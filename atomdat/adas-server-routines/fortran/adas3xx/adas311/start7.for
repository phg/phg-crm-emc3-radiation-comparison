       SUBROUTINE START7(IUTMP,IUPS1,IUPS2,STITLE,DSLPATH,
     &                   NBENG,NTEMP,NDENS, lbndl, lproj)
       IMPLICIT REAL*8(A-H,O-Z)
C-----------------------------------------------------------------------
C
C       *********  FORTRAN 77 ROUTINE : START7.F **********
C
C       PURPOSE : CALCULATION OF THE RESOLVED-NL POPULATION
C                 STRUCTURE.
C
C       NOTE    : THE RELEVANT QUANTUM NUMBERS ASSOCIATED WITH
C                 THE POPULATION STRUCTURE CALCULATION ARE
C                 STORED IN INTEGER ARRAYS WERE EACH ELEMENT
C                 IS 4 BYTES. THE FIRST 10 BITS OF THE 4 BYTE
C                 INTEGER ELEMENT OF THE ARRAY IS USED TO
C                 STORE THE TOTAL ANGULAR MOMENTUM QUANTUM
C                 NUMBER,L. THE NEXT TEN BITS IS USED TO STORE
C                 THE ORBITAL ANGULAR MOMENTUM QUANTUM
C                 NUMBER,l. THE LAST 12 BITS ARE USED TO STORE
C                 THE PRINCIPAL QUANTUM NUMBER,N.
C
C                 |<---------4 BYTE INTEGER--------->|
C                 |<---------32 BIT INTEGER--------->|
C                 |<----N----->|<----l--->|<---L---->|
C
C               [ |.....N......|.....l....|.....L....|  ]
C
C                 BIT OPERATORS ARE THEN EMPLOYED TO
C                 INTEROGATE ARRAYS,E.G IAND,ISHFR,
C                 USING HEXIDECIMAL MASKS.
C               
C
C       INPUT   :
C               
C       (CHR)   TITLE()    : NAME OF THE NEUTRAL BEAM SPECIES.
C       (I*4)   MN         :
C       (R*8)   RX3        :
C       (R*8)   DPT        :
C       (R*8)   EHCT       : CRITICAL ENERGY ?.
C       (I*4)   NHCT       : CRITICAL PRINCIPAL QUANTUM
C                            NUMBER ?.
C       (I*4)   LHCT       : CRITICAL ORBITAL QUANTUM
C                            NUMBER ?.
C       (I*4)   NIP        : RANGE OF DELTA N FOR IMPACT
C                            PARAMETER XSECTS. (LE.4)
C       (I*4)   NEX        :
C       (I*4)   IPRT       :
C       (I*4)   NDEL       :
C       (I*4)   INTD       : ORDER OF MAXWELL QUADRATURE
C                            FOR XSECTS. (LE.3)
C       (I*4)   IPRS       : 0 DEFAULT TO VAN REGEMORTER XSECTS.
C                              BEYOND NIP RANGE
C                            1 USE PERCIVAL-RICHARDS XSECTS.
C                              BEYOND NIP RANGE
C       (I*4)   ILOW       : 0 NO SPECIAL LOW LEVEL DATA ACCESSED.
C                            1 SPECIAL LOW LEVEL DATA ACCESSED.
C       (I*4)   IONIP      : 0 NO ION IMPACT COLLISIONS INCLUDED.
C                            1 ION IMPACT EXCITATION AND IONISATION
C                              INCLUDED.
C
C       (I*4)   NIONIP     : RANGE OF DELTA N FOR ION IMPACT
C                            EXCITATION XSECTS.
C       (I*4)   ILPRS      : 0 DEFAULT TO VAINSHTEIN ION IMPACT
C                              EXCITATION XSECTS.
C                            1 USE LODGE-PERCIVAL-RICHARDS ION
C                              IMPACT EXCITATION XSECTS.
C       (I*4)   IVDISP     : 0 ION IMPACT AT THERMAL MAXWELLIAN
C                              ENERGIES
C                            1 ION IMPACT AT DISPLACED THERMAL
C                              ENERGIES ACCORDING TO THE NEUTRAL
C                              BEAM ENERGY PARAMETER. IF(IVDISP=0 THEN
C                              SPECIAL LOW LEVEL DATA FOR ION IMPACT
C                              IS NOT SUBSTITUTED - ONLY VAINSHTEIN
C                              AND LODGE ET AL. OPTIONS ARE OPEN.
C                              ELECTRON IMPACT DATA SUBSTITUTION
C                              DOES OCCUR.
C       (R*4)   ZEFF       : EFFECTIVE CHARGE OF THE PLASMA.
C       (R*8)   DEDEG      : CRITICAL TRANSITION ENERGY (RYDBERGS)
C                            USED IN THE NEARLY DEGENERATE LEVEL
C                            TREATMENT.
C                            IF DE<=DEDEG THEN ASSUME ZERO A-VALUE
C                            AND NO SUPPLEMENTARY DATA.
C                            IF DE> DEDEG THEN ASSUME A-VALUE
C                            CALCULABLE AND SEARCH FOR SUPPLEMENTARY
C                            DATA.
C                            N.B. APPLIES TO DELTA N TRANSITIONS
C                            ONLY.
C
C       (I*4)   NL1        : PRINCIPAL QUANTUM NUMBER FROM WHICH
C                            THE RESOLVED-NL POPULATION STRUCTURE
C                            CALCULATION STARTS FROM
C                       
C       (I*4)   NL2        : PRINCIPAL QUANTUM NUMBER WHICH MARKS
C                            THE END OF THE RESOLVED-NL TREATMENT
C                            WITHIN THE POPULATION STRUCTURE
C                            CALCULATION AND INDICATES THE START
C                            OF THE BUNDLED-N APPROXIMATION.
C       (I*4)   NL3        : UPPER PRINCIPAL QUANTUM NUMBER OF
C                            THE BUNDLED-N APPROXIMATION.
C       (R*8)   Z0         : NUCLEAR CHARGE OF BEAM ATOM ?.
C       (R*8)   Z1         : ION CHARGE+1 OF BEAM ION ?.
C       (R*8)   ALF        : ADJUSTABLE PARAMETER ASSOCIATED
C                            WITH THE MODIFIED POTENTIAL USED
C                            WHEN SOLVING THE RADIAL WAVE
C                            EQUATION.
C       (R*8)   AMSZ0      :
C       (R*8)   AMSHYD     :
C       (I*4)   LP         :
C       (I*4)   ISP        :
C
C
C       OUTPUT  :
C       
C               (R*8)  ....ETC
C
C       GENERAL :
C
C       (I*4)   NLREP()    : ARRAY CONTAINING REPRESENTATIVE
C                            LEVELS.
C       (R*8)   ENL()      : EFFECTIVE PRINCIPAL QUANTUM
C                            NUMBER.
C       (R*8)   ENL2()     : RECIPROCAL OF THE EFFECTIVE PRINCIPAL
C                            QUANTUM NUMBER SQUARED.
C       (I*4)   KPF()      : ARRAY CONTAINING THE QUANTUM NUMBERS,
C                            N,l,L FROM NMIN TO NMAX, IN ORDER
C                            OF DECREASING BINDING ENERGY. SEE
C                            NOTE AT THE TOP OF PROGRAM.
C       (I*4)   KPB()      : ARRAY CONTAINING THE INDEX OF THE
C                            CORRESPONDING LEVEL IN KPF().
C       (R*8)   EGY        : IONISATION POTENTIAL ( RYDBERGS ).
C       (I*4)   IR         : COUNTER TO REFERENCE REPRESENTATIVE
C                            LEVELS.
C       (I*4)   I          : GENERAL COUNTER.
C       (R*8)   V          : EFFECTIVE PRINCIPAL QUANTUM NUMBER.
C       (R*8)   E          : RECIPROCAL OF THE EFFECTIVE PRINCIPAL
C                            QUANTUM NUMBER SQUARED.
C       (R*8)   EXE        : VARIABLE USED TO ASSIGN THE VALUE
C                            OF EXP(I/k*Te).
C       (R*8)   EXS        : VARIABLE USED TO ASSIGN THE VALUE
C                            OF EXP(I/k*TS)
C       (I*4)   K          : GENERAL COUNTER.
C       (R*8)   C1()       : COEFFICIENT OF THE QUANTUM DEFECT
C                            EXPANSION.
C       (R*8)   C2()       : COEFFICIENT OF THE QUANTUM DEFECT
C                            EXPANSION.
C       (R*8)   C3()       : COEFFICIENT OF THE QUANTUM DEFECT
C                            EXPANSION.
C
C
C
C
C       
C       ROUTINES:
C
C               ROUTINE        SOURCE     BRIEF DESCRIPTION
C               -----------------------------------------------
c               SUPPHE1         ADAS    OBTAINS FUNDAMENTAL DATA
C                                       FROM APPROPRIATE DATABASES.
C               OVLP            ADAS    ?????????????????????????
C               SETUP3          ADAS    ?????????????????????????
C               SPIJ            ADAS    ?????????????????????????
C               CCNST7          ADAS    ASSEMBLES ARRAYS USED TO
C                                       CONSTRUCT THE COLLISIONAL-
C                                       RADIATIVE MATRIX.
C               CCNSE4          ADAS    APPLIES MATRIX CONDENSATION
C                                       SCHEME TO ARRAYS USED TO
C                                       ASSEMBLE THE COLLISIONAL-
C                                       RADIATIVE MATRIX.
C               HYSCL           ADAS    ?????????????????????????
C
C
C       HISTORY : ORIGINALLY WRITTEN BY H.P.SUMMERS.
C
C       NOTE    : THE RESOLVED-NL CALCULATION WAS STRUCTURED
C                 IN SUCH A MANNER THAT THE CALCULATION WAS
C                 PERFORMED IN TWO STAGES. THIS TWO STAGE
C                 PROCCESS HAS BEEN REMOVED. CUBIC SPLINE
C                 INTERPLOATION IN L. THE DIMENSIONALITIES
C                 FOR EACH SPIN SYSTEM ARE AS FOLLOWS :
C
C                 NUMBER OF LEVELS <1000.
C                 NUMBER OF PRINCIPAL QUANTUM LEVELS<300.
C                 NUMBER OF RESOLVED PRINCIPAL QUANTUM LEVELS<40.
C                 NUMBER OF RESOLVED LEVELS <800
C                 NUMBER OF RESOLVED REPR.PRINC.QUANTUM LEVELS<11.
C                 NUMBER OF REPRESENTATIVE LEVELS<80.
C                 NUMBER OF PRINCIPAL QUANTUM REPRESENTATIVE LEVELS<30.
C
C
C       CONTACT : HARVEY ANDERSON
C                 UNIVERSITY OF STRATHCLYDE
C                 ANDERSON@PHYS.STRATH.AC.UK
C
C       DATE    : 2/2/98
C
C       NOTE :
C                 THE C-R MATRIX IS PASSED TO THE ROUTINE FINISH5.F
C                 VIA A SCRTACH FILE ON STREAM 12. IDEALLY THE VARIBLES
C                 SHOULD BE PASSED DIRECTLY TO THE ROUTINE.
C
C       
C       MODIFIED
C
C VERSION:      1.2                                     DATE:   21-10-99
C MODIFIED: RICHARD MARTIN
C               CHANGED HEXADECIMAL CONSTANTS TO Z'FFF00000' FORM.
C
C VERSION : 1.3
C MODIFIED: Martin O'Mullane      
C DATE    : 08-11-2004
C           Alter nmax in gamaf() from 200 to 500.        
C
C VERSION : 1.4
C DATE    : 18-11-2004
C MODIFIED: Martin O'Mullane      
C            - Align with Harvey Anderson's last version.
C            - Add lproj if projection output is requested.
C            - Add lbndl if adf36 output file is requested.
C            - The dsnps1 variable is replaced by iups2 in the 
C              parameter list.
C
C VERSION : 1.5
C DATE    : 16-05-07
C MODIFIED: Allan Whiteford    
C            - Moved parameter statement to below comment block
C              as part of subroutine documentation procedure.
C
C-----------------------------------------------------------------------
       PARAMETER(NCDIM=50,NDLOW=10,NLDIM=19)
       DIMENSION EEIJA(10),EEIJSA(10),EIJA(10),FIJA(10)
       DIMENSION EIJSA(10),FIJSA(10),CORA(10,6),CORSA(10,6)
       DIMENSION JCA(10),JCSA(10)
       DIMENSION ENL(1000),ENL2(1000),EXPTE(1000),EXPTS(1000)
       DIMENSION ENLS(1000),ENL2S(1000),EXTES(1000),EXTSS(1000)
       DIMENSION KPFS(1000),KPBS(1000)
       DIMENSION N2S(300),KPF(1000),KPB(1000),INTER(300)
       DIMENSION LMP(10),LTMP(5),NLREP(80),NREP(31)
       DIMENSION GE(2),LTG(5),LTGS(5),FPG(5),FPGS(5)
       DIMENSION ILTXRS(10,10,5),NLREPS(80),NLIVS(31)
       DIMENSION CGBBA(800,3),C1(5,5,3),C2(5,5,3),C3(5,5,3)
       DIMENSION ARED(160),A(1000),C1S(5,5,3),C2S(5,5,3),C3S(5,5,3)
       DIMENSION CARED(160),CA(1000)
       DIMENSION LXR(10),LTXR(10,10),ILTXR(10,10,5)
       DIMENSION RLR(10,10),RLTR(10,10,5),NLIV(31)
       DIMENSION AGRL(300,3),FLAG(300),IRA(20),TITLE(10)
       DIMENSION CLT1(10,10,3,2),CLT2(10,10,3,2),CLT3(10,10,3,2)
       DIMENSION CL1(10,10,9),CL2(10,10,9),CL3(10,10,9),CL4(10,10,9)
       DIMENSION YL(10),YLT(10),ANS(3),CGBBAS(800,3),SPA(10,10)
       DIMENSION ARL(80),ARLS(80),ISARL(80),ISARLS(80),SCLA(240,8)
       DIMENSION IREF(10),JREF(10),SREF(10),WREF(10),CREF(10)
       DIMENSION WIREF(10),WCREF(10)
C
C       VARIABLES FOR HANDLIING IMPURITIES.
C       HARVEY ANDERSON. 2/2/98
C
       DIMENSION AMIMPA(10) , ZIMPA(10), FRIMPA(10) , DNIMPA(10)
C
        CHARACTER DSLPATH*80 , STITLE*80
        CHARACTER DSNPS1*80
        
        logical   lbndl, lproj

C
C-----------------------------------------------------------------------
       COMMON /ATSP/Z0,Z1,ALF,LP,ISP,AMSZ0,AMSHYD
       COMMON /ACCSET/RX3,DPT,GE,EHCT,MN,NHCT,LHCT,NIP,NEX,IPRT,NDEL
       COMMON /PLSP/TE,TP,TS,DENS,DENSP,W,ATE,ATP,ATS,RHO,RHOP,
     &              ZIMP,DENIMP,RHOIMP,AMSIMP,VDISP
       COMMON /INTSP/AGRL,CL1,CL2,CL3,CL4,CLT1,CLT2,CLT3,RLR,RLTR,LXR,
     &               LTXR,INTER,INREP,NREP,NL1,NL2,NL3
       COMMON /DIEL/EIJA,EEIJA,FIJA,EIJSA,EEIJSA,FIJSA,CORA,CORSA,JCA,
     &              JCSA,IDIEL,LMAX,NTAIL,LSWCH,IGHNL,IRNDGV,ZP,EMP
       COMMON /CHEX/BMENER,DENSH,FLUX,MULTIP,MULTN,MULTN1
       COMMON /PARS1/INTD,IPRS,ILOW,IONIP,NIONIP,ILPRS,IVDISP,ZEFF,
     &              DEDEG

       COMMON /SUPPL/ATBE(NLDIM,NLDIM),XTBE(NLDIM,NLDIM),WTA(NLDIM),
     &               ENA(NLDIM),XTBP(NDLOW,NDLOW,2),XTBZ(NDLOW,NDLOW,2),
     &               PXTBP(NDLOW,2),PXTBZ(NDLOW,2),PSTBE(2),PSTBP(2),
     &               PSTBZ(2),STBE(NDLOW,2),STBP(NDLOW,2),STBZ(NDLOW,2),
     &               NNA(NLDIM),ISA(NLDIM),ILA(NLDIM),
     &               LXTBE(NLDIM,NLDIM),LXTBP(NDLOW,NDLOW,2),
     &               LXTBZ(NDLOW,NDLOW,2),LPXTBP(NDLOW,2),
     &               LPXTBZ(NDLOW,2),LSTBE(NDLOW,2),LSTBP(NDLOW,2),
     &               LSTBZ(NDLOW,2),LPSTBE(2),LPSTBP(2),LPSTBZ(2),
     &               ITYP1 ,ITYP2 ,ITYP3 ,ITYP4 ,ITYP5 ,ITYP6 ,
     &               NSYS  ,ISYSA(2) , NLEV  , IXREF(500),IXREFS(500)
C-----------------------------------------------------------------------

       DATA NLIV,NLIVS/62*0/
       DATA ILTXR,ILTXRS/1000*0/
       DATA ISARL,ISARLS/160*0/
       DATA MASK1,MASK2,MASK3,MASK4/Z'FFF00000',Z'000FFC00',Z'000003FF',
     &                              Z'FFFFFC00'/

C-----------------------------------------------------------------------
C       REWIND THE SCRATCH FILE TO ALLOW INPUT PARAMETERS TO BE READ
C-----------------------------------------------------------------------

        REWIND(IUTMP)

C-----------------------------------------------------------------------
C             OPEN SCRATCH FILE TO PASS THE C-R MATRIX
C             TO THE ROUTINE FINISH5 ON STREAM 12.
C-----------------------------------------------------------------------


         OPEN( UNIT=12 ,STATUS='SCRATCH',FORM='UNFORMATTED' )



C-----------------------------------------------------------------------
C               ZERO INITIAL ARRAYS AND SET SOME PARAMETERS
C-----------------------------------------------------------------------

       DO 16 I=1,10
        LXR(I)=0
        DO 15 J=1,10
         LTXR(I,J)=0
         RLR(I,J)=0.0
         DO 14 K=1,5
          RLTR(I,J,K)=0.0
   14    CONTINUE
   15   CONTINUE
   16  CONTINUE

       CUT=170.0

       CALL GAMAF(500)


C-----------------------------------------------------------------------
C  INPUT TITLE FOR SPECIES
C               THE TITLE IS TRANSFERED TO PASSING FILE ON STREAM 12
C               IT IS REQUIRED FOR FINISH5 AND FINISH6.
C               IT WILL NOT UPSET FINISH4.
C-----------------------------------------------------------------------


       READ(IUTMP,1006) ( TITLE(I),I=1,6 )
       WRITE(12) TITLE

C-----------------------------------------------------------------------
C               READING INPUT ACCURACY SETTING DATA
C-----------------------------------------------------------------------

       READ(IUTMP,1000)MN,RX3,DPT,EHCT,NHCT,LHCT,NIP,NEX,IPRT,NDEL

C-----------------------------------------------------------------------
C               READING INPUT SETTINGS
C-----------------------------------------------------------------------

       READ(IUTMP,2003)INTD,IPRS,ILOW,IONIP,NIONIP,ILPRS,IVDISP,
     &                 ZEFF,DEDEG

       READ(IUTMP,111)NL1,NL2,NL3
       READ(IUTMP,115)Z0,Z1,ALF,AMSZ0,AMSHYD
       IZ0=Z0+0.0001
       IZ1=Z1+0.0001
       ZZ=Z1*Z1
       WRITE(12)Z0
       READ(IUTMP,111)LP,ISP
       READ(IUTMP,111)LMAX,NTAIL,LSWCH,IGHNL,IRNDGV

C-----------------------------------------------------------------------
C >>>>
C ......... IGHNL  = 1 SWITCH ON GHNLV/E ROUTINE IN CCNST7   ELSE DON'T
C ......... IRNDGV = 1    "    " RNDEGV     "     "    "       "    "
C >>>>
C-----------------------------------------------------------------------
C               INPUT DIELECTRONIC RECOMBINATION DATA
C-----------------------------------------------------------------------
C       
       READ(IUTMP,111)IDIEL
       IF(IDIEL.EQ.0)GOTO 29

       DO 24 I=1,IDIEL
       READ(IUTMP,115)EIJA(I),FIJA(I),(CORA(I,J),J=1,6)
       JC=0
   21  IF(CORA(I,JC+1).EQ.0.0D+00)GOTO 22
       JC=JC+1
       GOTO 21
   22  JCA(I)=JC
   24  continue
       DO 28 I=1,IDIEL
       READ(IUTMP,115)EIJSA(I),FIJSA(I),(CORSA(I,J),J=1,6)
       JC=0
   25  IF(CORSA(I,JC+1).EQ.0.00D+00)GOTO 26
       JC=JC+1
       GOTO 25
   26  JCSA(I)=JC
   28  CONTINUE
   29  CONTINUE
       IF(ISP.LE.1)GO TO 2
       CALL OVLP(N,L,N1,L1,1,NL2,OVL)
    2  XLP=LP

C-----------------------------------------------------------------------
C  SET UP REFERENCING ARRAYS N2S,KPF AND KPFS.
C-----------------------------------------------------------------------

       S=0
       N2S(1)=0
       I=1
    1  U=N2S(I)
       IF(LP.LT.I)GO TO 3
       U=U+2*(LP-I+1)
    3  N2S(I)=S
       IM1=I
       I=I+1
       N2S(I)=U
       S=S+U
       N2S(IM1)=-N2S(IM1)+(IM1*(IM1-1)*(2*LP+1))/2+1
       IF(I.LE.NL2+1)GO TO 1
    4  N2S(I)=N2S(IM1)+1
       IM1=I
       I=I+1
       IF(I.LE.NL3)GO TO 4
       NMAX=N2S(I-1)
       N2S(I)=NMAX+1
       DO 8 N=1,NL3
       I0=N2S(N)
       I1=0
       X=N
       FLAG(N)=FINT1(X)
       IF(NL2.LT.N)GO TO 7
       DO 5 L1=1,N
       L=L1-1
       I1=2*L*(LP+1)-LP*LP+(LP-L+1)*MAX0(LP-L,0)
       LT3=LP+L+1
       LT2=IABS(LP-L)+1
       DO 6 LT1=LT2,LT3
       LT=LT1-1
       I=I0+I1-LT
       KPF(I)=ISHFL(N,20)+ISHFL(L,10)+LT
       ENL(I)=X
       IF(ISP.LE.1)GO TO 6
       KPFS(I)=KPF(I)
       ENLS(I)=ENL(I)
    6  CONTINUE
    5  CONTINUE
       GO TO 8
    7  KPF(I0)=ISHFL(N,20)
       ENL(I0)=X
       IF(ISP.LE.1)GO TO 8
       KPFS(I0)=KPF(I0)
       ENLS(I0)=ENL(I0)
    8  CONTINUE
       NMAX=I0

C-----------------------------------------------------------------------
C
C       READING INFORMATION REGARDING THE METASTABLES AND THE
C       EXTERNAL RADIATION FIELD.
C
C-----------------------------------------------------------------------

       READ(IUTMP,'(1I5)') MJ
       WRITE(12) MJ

       IF (MJ.EQ.0) GOTO 1997

       DO K=1,MJ
        READ(IUTMP,'(2I5,1P,5E12.4)') IREF(k),JREF(k),SREF(k),WREF(k),
     &  CREF(k),WIREF(k),WCREF(k)
        WRITE(12) IREF(k),JREF(k),SREF(k),WREF(k),CREF(k),
     &  WIREF(k),WCREF(k)

       ENDDO
 1997  CONTINUE

       READ(IUTMP,'(1I5)') MG
       WRITE(12) MG

       IF (MG.EQ.O) GOTO 1998

        READ(IUTMP,'(20I5)') (IRA(I),I=1,MG)
        WRITE(12) (IRA(I),I=1,MG)

 1998  CONTINUE

       READ(IUTMP,1011)TE,TP,TS,DENS,DENSP,W,BMENER,DENSH
       WRITE(12)TE,TP,TS,DENSP,W,BMENER,DENSH


C       READ IMPURITY PARAMETERS

       IF ( DENS .GT. DENSP ) THEN
           READ(IUTMP,'(1I5)') NIMP
                DO I=1,NIMP
                  READ(IUTMP,1012) ZIMPA(I),AMIMPA(I),FRIMPA(I)
                ENDDO
       ENDIF


       TEV=TE/11605.4
       TIEV=TP/11605.4
       X=157890.0*ZZ
       ATE=X/TE
       ATP=X/TP
       ATS=X/TS
       FLUX=1.38317498E+06*DSQRT(BMENER)*DENSH

C-----------------------------------------------------------------------
C
C    COMPUTE DISPLACED SPEED FOR ION IMPACT MAXWELLIAN AVERAGING AND
C    EVALUATE THE PLASMA ZEFF FROM KNOWLEDGE OF IMPURITIES AND THERE
C    FRACTIONAL CONCENTRATION.
C
C-----------------------------------------------------------------------

       IF(DENS.LE.DENSP.OR.ZEFF.LE.0.0D0)THEN
           NIMP       =   0.0D0
           ZIMPA(1)   =   0.0D0
           DNIMPA(1)  =   0.0D0
           DENIMP     =   DNIMPA(1)
           FRIMPA(1)  =   0.0D0
           AMIMPA(1)  =   1.0D0
           ZEFF       =   1.0D0
       ELSE IF ( NIMP .EQ. 1 ) THEN
           FRIMPA(1)  =   1.0D0
           DENIMP     = ( DENS-DENSP)/( ZIMPA(1)*FRIMPA(1) )
           ZEFF = (DENSP+DENIMP*(ZIMPA(1)**2*FRIMPA(1)))/DENS
           ZIMPA(1)=(ZEFF*DENS-DENSP)/(DENS-DENSP)
           DENIMP=(DENS-DENSP)/ZIMPA(1)
           DNIMPA(1)=DENIMP*FRIMPA(1)
        ELSE IF ( NIMP .GT. 1 ) THEN
           ZSUM1=0.0
           ZSUM2=0.0
               DO IMP=1,NIMP
                  ZSUM1=ZSUM1+ZIMPA(IMP)*FRIMPA(IMP)
                  ZSUM2=ZSUM2+( ZIMPA(IMP)**2*FRIMPA(IMP) )
               END DO
           DENIMP=( DENS-DENSP ) / ZSUM1
           ZEFF=( DENSP + (DENIMP*ZSUM2) ) / DENS
           DO IMP=1,NIMP
                DNIMPA(IMP)=DENIMP*FRIMPA(IMP)
           END DO
        ENDIF

       IF(IVDISP.EQ.1)THEN
           VDISP=1.38317498E6*DSQRT(BMENER)
       ELSE
           VDISP=0.0D0
       ENDIF

C-----------------------------------------------------------------------
C               PREPARING TO WORK WITH THE SCALED DENSITY.
C-----------------------------------------------------------------------

       RHO   = 3.9230144D-17*DENS*DSQRT(ATE)/Z1**7
       RHOP  = 3.9230144D-17*DENSP*DSQRT(ATP)/Z1**7
       RHOIMP= 3.9230144D-17*DENIMP*DSQRT(ATP)/Z1**7

C-----------------------------------------------------------------------
C
C  PREPARE SUPPLEMENTARY LOW LEVEL DATA FROM ADAS DATABASE IF REQUIRED
C
C       ITYP1   :       ELECTRON IMPACT EXCITATION DATA.
C       ITYP2   :       ELECTRON IMPACT IONISATION DATA.                
C       ITYP3   :       H+ IMPACT EXCITATION DATA.              
C       ITYP4   :       H+ IMPACT IONISATION AND CHARGE EXCHANGE DATA.                              C
C       ITYP5   :       ZIMP ION IMPACT EXCITATION DATA.
C       ITYP6   :       ZIMP ION IMPACT IONISATION CHARGE EXCHANGE DATA.
C
C-----------------------------------------------------------------------

       IF(ILOW.GE.1)THEN
           ITYP1=1
           ITYP2=1
           ITYP3=1
           ITYP4=1
           ITYP5=0
           ITYP6=1

C-----------------------------------------------------------------------
C
c       THE ROUTINE SUPPHE1 ACCESSES A COLLECTION OF DATABASES
C       CONTAINING CROSS-SECTION DESCRIBING THE BEHAVIOUR OF
C       THE ATOMIC PROCESSES WHICH CONTRIBUTE TO POPULATING
C       AND DEPOPULATING EACH LEVEL
C       
C
C-----------------------------------------------------------------------


         CALL SUPPHE1(TEV , BMENER , TIEV , NIMP, ZIMPA,
     &                FRIMPA, AMIMPA , DSLPATH)

       ENDIF

C-----------------------------------------------------------------------
C
C       NOTE :
C               ZP  = CHARGE OF COLLIDING +VE ION
C               EMP = REDUCED MASS OF FOR COLLIDING +VE ION
C                     (ELECTRON MASSES)
C
C-----------------------------------------------------------------------

       READ(IUTMP,1973)ZP,EMP

       DO 35 I=1,IDIEL
        IF(FIJA(I).GT.0.0D0)THEN
            X=Z1+1.0D0
            X=X*X*EIJA(I)*1.5789D5/TE
            EEIJA(I)=DEXPT(-X)
        ELSE
            EEIJA(I)=0.0D0
        ENDIF
   35  CONTINUE

C-----------------------------------------------------------------------
C  CALL MAIN INITIAL SET UP ROUTINE FOR EACH SPIN SYSTEM AND
C  CROSS-REFERENCING TO SUPPLEMENTARY LOW LEVEL ELECTRON DATA IF PRESENT
C-----------------------------------------------------------------------

       CALL SETUP3(NMIN,NMAX,N2S,ENL,ENL2,KPF,KPB,
     &             EXPTE,EXPTS,NG,LG,LTG,ISG,FPG,NGL,
     &             CGBBA,C1,C2,C3,1,CGBBAS,KPBS,IUTMP)


       IF(ILOW.EQ.1.AND.ITYP1.EQ.1)THEN
           DO 37 I=NMIN,NMAX
            K=KPF(I)
            N=ISHFR(K,20)
            IF(N.GT.NL2)GO TO 38
            L=IAND(K,MASK2)
            L=ISHFR(L,10)
            DO 36 ILEV=1,NLEV
             IF(ISA(ILEV).EQ.ISG.AND.NNA(ILEV).EQ.N
     &                          .AND.ILA(ILEV).EQ.L) THEN
                 IXREF(I)=ILEV
                 GO TO 37
             ENDIF
   36       CONTINUE
            IXREF(I)=0
   37      CONTINUE
   38      CONTINUE
       ENDIF

       IF(ISP.LE.1)GO TO 300
       CALL SETUP3(NMINS,NMAX,N2S,ENLS,ENL2S,KPFS,KPBS,
     &             EXTES,EXTSS,NGS,LGS,LTGS,ISGS,FPGS,NGLS,
     &             CGBBAS,C1S,C2S,C3S,2,CGBBA,KPB,IUTMP)

       IF(ILOW.EQ.1.AND.ITYP1.EQ.1)THEN
           DO 39 I=NMINS,NMAX
            K=KPFS(I)
            N=ISHFR(K,20)
            IF(N.GT.NL2)GO TO 40
            L=IAND(K,MASK2)
            L=ISHFR(L,10)
            DO 34 ILEV=1,NLEV
             IF(ISA(ILEV).EQ.ISGS.AND.NNA(ILEV).EQ.N
     &                          .AND.ILA(ILEV).EQ.L) THEN
                 IXREFS(I)=ILEV
                 GO TO 39
             ENDIF
   34       CONTINUE
            IXREFS(I)=0
   39      CONTINUE
   40      CONTINUE
       ENDIF

  300  CONTINUE

C-----------------------------------------------------------------------
C  INITIALISE RD2FS RRAYS FOR PHOTO PROCESSES IN RESOLVED
C  HYDROGENIC CASE
C-----------------------------------------------------------------------

       X=NL3
       ENMAX2=1.0/((X+0.5)*(X+0.5))

C       TEMPORARY SET ENMAX2 TO 0.0 HARVEY ANDERSON 27/5/98
C
C       ENMAX2=0.0


C-----------------------------------------------------------------------
C  SET UP REPRESENTATIVE LEVELS GIVEN INREP,ILREP,ILTREP,AND NREP ARRAY.
C  REQUIRE ILREP>6 AND ILTREP>4.
C  KPF CONTAINS PARAMETERS OF LEVELS N,L,LT IN FORM ISHFL(N,20)+
C  ISHFL(L,10)+LT WITH LEVELS INDEXED FROM NMIN TO NMAX IN ORDER OF
C  DECREASING BINDING ENERGY.
C  FOR REP. LEVEL I WHERE 1.LE.I.LE.IMAX, NLREP(I) GIVES THE INDEX OF
C  THIS LEVEL IN KPF ARRAY.
C  GIVEN N,L,LT, SET J=N2S(N)+2*L*(LP+1)-LP*LP+(LP-L+1)*MAX0(LP-L,0)-LT
C  THEN KPB(J) GIVES INDEX OF CORRESPONDING LEVEL IN KPF ARRAY.
C-----------------------------------------------------------------------

       I0=1
       I0S=1
       READ(IUTMP,111)INREP,ILREP,ILTREP
       READ(IUTMP,111)(NREP(I),I=1,INREP)
       DO 60 I=1,INREP
       N=NREP(I)
       J0=N2S(N)
       XM=MAX0(N-1,1)
       XM=1.0/XM
       IF(N.LE.NL2)GO TO 41
       NLREP(I0)=KPB(J0)
       NLIV(I)=I0
       I0=I0+1
       IF(ISP.LE.1)GO TO 60
       NLREPS(I0S)=KPBS(J0)
       NLIVS(I)=I0S
       I0S=I0S+1
       GO TO 60
   41  READ(IUTMP,111)LREP1
       READ(IUTMP,111)(LMP(L1),L1=1,LREP1)
       LXR(I)=LREP1
   45  DO 55 L1=1,LREP1
       L=LMP(L1)
       YL(L1)=DFLOAT(L)*XM
       RLR(I,L1)=YL(L1)
       J1=2*L*(LP+1)-LP*LP+(LP-L+1)*MAX0(LP-L,0)
       LTREP1=IABS(LP-L)
       Y1=LTREP1
       LTREP1=LP+L-LTREP1+1
       YM=MAX0(LTREP1-1,1)
       YM=1.0/YM
       IF(LTREP1.GT.ILTREP)GO TO 47
       LTXR(I,L1)=LTREP1
       DO 46 LT1=1,LTREP1
   46  LTMP(LT1)=LP+L-LT1+1
       GO TO 49
   47  LTMP(1)=LP+L
       LTMP(2)=LP+L-1
       LTMP(ILTREP)=IABS(LP-L)
       LTMP(ILTREP-1)=LTMP(ILTREP)+1
       LTXR(I,L1)=ILTREP
       LTREP1=ILTREP-4
       DO 48 LT1=1,LTREP1
   48  LTMP(LT1+2)=LP+L-2-(LT1*(L-3))/(LTREP1+1)
       LTREP1=ILTREP
   49  DO 50 LT1=1,LTREP1
       LT=LTMP(LT1)
       YLT(LT1)=(DFLOAT(LT)-Y1)*YM
       RLTR(I,L1,LT1)=YLT(LT1)
       J=J0+J1-LT
       K=KPB(J)
       IF(K.LT.NMIN)GO TO 310
       ILTXR(I,L1,LT1)=I0
       NLREP(I0)=K
       I0=I0+1
  310  IF(ISP.LE.1)GO TO 50
       K1=KPBS(J)
       IF(K1.LT.NMINS)GO TO 50
       ILTXRS(I,L1,LT1)=I0S
       NLREPS(I0S)=K1
       I0S=I0S+1
   50  CONTINUE
   51  IF(LTREP1-2)80,81,82
   80  CLT1(I,L1,1,1)=1.0
       CLT2(I,L1,1,1)=0.0
       CLT3(I,L1,1,1)=0.0
       GO TO 54
   81  U=1.0/(YLT(1)-YLT(2))
       CLT1(I,L1,1,1)=0.5
       CLT2(I,L1,1,1)=U
       CLT3(I,L1,1,1)=0.0
       CLT1(I,L1,2,1)=0.5
       CLT2(I,L1,2,1)=-U
       CLT3(I,L1,2,1)=0.0
       GO TO 54
   82  DO 83 INT=2,LTREP1
   83  YLT(INT-1)=YLT(INT)-YLT(INT-1)
       DO 53 LT1=1,LTREP1
       DO 52 INT=2,LTREP1
       INT1=INT-1
       CALL SPIJ(LTREP1-1,YLT,INT1,LT1,ANS)
       CLT1(I,L1,LT1,INT1)=ANS(1)
       CLT2(I,L1,LT1,INT1)=ANS(2)
   52  CLT3(I,L1,LT1,INT1)=ANS(3)
   53  CONTINUE
   54  CONTINUE
   55  CONTINUE
       IF(LREP1-2)84,86,86
   84  CL1(I,1,1)=1.0
       CL2(I,1,1)=0.0
       CL3(I,1,1)=0.0
       CL4(I,1,1)=0.0
       GO TO 60
   86  DO 87 INT=2,LREP1
   87  YL(INT-1)=YL(INT)-YL(INT-1)
       DO 56 INT=2,LREP1
       INT1=INT-1
       CALL SPIJ3(LREP1,YL,SPA)
       H=YL(INT1)
       H1=1.0/H
       H2=H1*H1
       H3=H1*H2
       DO 57 L1=1,LREP1
       U1=H*(SPA(INT1,L1)+SPA(INT,L1))
       U2=H*(SPA(INT1,L1)-SPA(INT,L1))
       X=0.0
       IF(INT1.EQ.L1)X=1.0
       IF(INT.EQ.L1)X=-1.0
       CL1(I,L1,INT1)=0.5*X*X+0.125*U2
       CL2(I,L1,INT1)=-H1*(1.5*X+0.25*U1)
       CL3(I,L1,INT1)=-0.5*H2*U2
   57  CL4(I,L1,INT1)=H3*(2.0*X+U1)
   56  CONTINUE
   60  CONTINUE
       IMAX=I0-1
       DO 260 I=1,IMAX
       J=NLREP(I)
       K=KPF(J)
       N=ISHFR(K,20)
       L=IAND(K,MASK2)
       L=ISHFR(L,10)
       LT=IAND(K,MASK3)
  260  CONTINUE
       IF(ISP.LE.1)GO TO 325
       IMAXS=I0S-1
       DO 320 I=1,IMAXS
       J=NLREPS(I)
       K=KPFS(J)
       N=ISHFR(K,20)
       L=IAND(K,MASK2)
       L=ISHFR(L,10)
       LT=IAND(K,MASK3)
  320  CONTINUE
  325  CONTINUE

C-----------------------------------------------------------------------
C  SET UP INTER ARRAY. FOR PRINCIPAL QUANTUM NO. N AND I=INTER(N),
C  NREP(I) GIVES LOWEST OF THREE REP. LEVELS FOR INTERPOLATI0N W.R.T. N.
C-----------------------------------------------------------------------

       NREP(INREP+1)=NL3+NL3
       I=3
       N1=NL1+2
       DO 70 N=N1,NL3
       IF(N+N-NREP(I)-NREP(I+1))73,73,72
   72  IF(I-INREP)74,73,73
   74  I=I+1
   73  INTER(N)=I-1
   70  CONTINUE
       INTER(NL1)=1
       INTER(NL1+1)=1

       DO 90 N=NL1,NL3
       F=FLAG(N)
       I=INTER(N)
       I1=NREP(I)
       F1=FLAG(I1)
       I2=NREP(I+1)
       F2=FLAG(I2)
       IF(INREP.LE.I+1)GO TO 91
       I3=NREP(I+2)
       F3=FLAG(I3)
       GO TO 92
   91  F3=0.0
   92  AGRL(N,1)=(F-F2)*(F-F3)/((F1-F2)*(F1-F3))
       AGRL(N,2)=(F-F1)*(F-F3)/((F2-F1)*(F2-F3))
   90  AGRL(N,3)=(F-F1)*(F-F2)/((F3-F1)*(F3-F2))

       ELLP=2*LP+1
       WRITE(12)Z1,ZZ,DENS,RHO,ATE,ELLP,ISG,ISGS,ISP,NL1,NL2,NL3,IMAX,
     1IMAXS
       WRITE(12)NLREP,NLREPS,KPF,KPFS
       WRITE(12)ENL2,ENL2S,EXPTE,EXTES
       CALL HYSCL(NL2,NMIN,NMAX,N2S,KPF,KPB,ENL,ENL2,LP,C1,C2,C3,SCLA)
       MULTIP=ISG
       MULTN=MULTIP

       DO 160 IR=1,IMAX
       I=NLREP(IR)
       V=ENL(I)
       E=ENL2(I)
       K=KPF(I)
       EXE=EXPTE(I)
       EXS=EXPTS(I)
       MULTN1=MULTN

C-----------------------------------------------------------------------
C
C       CCNST7 EVALUATES THE POPULATION STRUCTURE OF THE SINGLETS
C       EXCLUDING SPIN CHANGING CROSS SECTIONS.
C
C       HARVEY ANDERSON
C       6/3/98
C
C-----------------------------------------------------------------------

       CALL CCNST7(NLREP,IR,I,V,E,EXE,EXS,K,ISG,CGBBA,ENL,ENL2,EXPTE,
     &EXPTS,KPF,ISG,NMIN,NMAX,IMAX,A0,CA0,A,CA,RH,CRH,1,NG,LG,NGL,LTG,
     &FPG,NG,LG,NGL,LTG,FPG,ARL,ISARL,AC1,RHSC1,C1,C2,C3,SCLA,NIMP,
     &ZIMPA,FRIMPA,AMIMPA,EXTES)
       DO 157 JR=1,IMAX
       CARED(JR)=0.0
  157  ARED(JR)=0.0
       RHS=0.0
       CRHS=0.0

C-----------------------------------------------------------------------
C
C      CCNSE4 PERFORMS THAE MATRIX CONDENSATION SCHEME TO REDUCE
C      THE SIZE OF THE COLLISIONAL-RADIATIVE MATRIX
C
C       HARVEY ANDERSON
C       6/3/98
C
C-----------------------------------------------------------------------

       CALL CCNSE4(A0,CA0,A,CA,RH,CRH,NMIN,NMAX,ARED,CARED,RHS,CRHS,IR,
     &             0,0,KPF,NLIV,ILTXR)
       IF(ISP.LE.1)GO TO 160
       MULTN1=ISGS

C-----------------------------------------------------------------------
C
C       CCNST7 EVALUATES THE SPIN CHANGING CROSS SECTIONS FOR
C       TRANSITIONS FROM THE TRIPLET TO THE SINGLET SIDE
C
C       HARVEY ANDERSON
C       6/3/98
C
C-----------------------------------------------------------------------

       CALL CCNST7(NLREP,IR,I,V,E,EXE,EXS,K,ISG,CGBBAS,ENLS,ENL2S,
     &EXTES,EXTSS,KPFS,ISGS,NMINS,NMAX,IMAX,A0,CA0,A,CA,RH,CRH,2,NG,LG,
     &NGL,LTG,FPG,NGS,LGS,NGLS,LTGS,FPGS,ARL,ISARL,AC11,RHSC11,C1,C2,C3,
     &SCLA,NIMP,ZIMPA,FRIMPA,AMIMPA,EXPTE)
       DO 158 JR=1,IMAXS
       CARED(JR+IMAX)=0.0
  158  ARED(JR+IMAX)=0.0

C-----------------------------------------------------------------------
C
C      CCNSE4 PERFORMS THE MATRIX CONDENSATION SCHEME TO REDUCE
C      THE SIZE OF THE COLLISIONAL-RADIATIVE MATRIX
C
C       HARVEY ANDERSON
C       6/3/98
C
C-----------------------------------------------------------------------
C
c       write(6,*) 'nmins', nmins
c       DO Jf=1,IMAXS+1
c       write(6,*) 'BEFORE ROUTINE', CA(Jf)
c        ENDDO
C
C       MODIFIED : HARVEY ANDERSON
C                  9/9/98
C
C       CHANGED PARAMETER IN THE CALL TO THE ROUTINE CCNSE4 FROM
C       NMAX TO IMAXS.
C
C
       CALL CCNSE4(A0,CA0,A,CA,RH,CRH,NMINS,NMAX,ARED,CARED,RHS,CRHS,
     &             IR,0,IMAX,KPFS,NLIVS,ILTXRS)

  160   WRITE(12)ARED,CARED,RHS,CRHS,AC1,RHSC1
        IMAXT=IMAX

       IF(ISP.LE.1)GO TO 171
       CALL HYSCL(NL2,NMINS,NMAX,N2S,KPFS,KPBS,ENLS,ENL2S,LP,
     &            C1S,C2S,C3S,SCLA)
       MULTIP=ISGS
       MULTN=MULTIP
       DO 170 IR=1,IMAXS
       I=NLREPS(IR)
       V=ENLS(I)
       E=ENL2S(I)
       K=KPFS(I)
       EXE=EXTES(I)
       EXS=EXTSS(I)
       MULTN1=MULTN

C-----------------------------------------------------------------------
C
C       CCNST7 EVALUATES THE POPULATION STRUCTURE OF THE TRIPLET
C       EXCLUDING SPIN CHANGING CROSS SECTIONS.
C
C       HARVEY ANDERSON
C       6/3/98
C
C-----------------------------------------------------------------------

       CALL CCNST7(NLREPS,IR,I,V,E,EXE,EXS,K,ISGS,CGBBAS,ENLS,ENL2S,
     &   EXTES,EXTSS,KPFS,ISGS,NMINS,NMAX,IMAXS,A0,CA0,A,CA,RH,CRH,1,
     &   NGS,LGS,NGLS,LTGS,FPGS,NGS,LGS,NGLS,LTGS,FPGS,ARLS,ISARLS,AC1,
     &   RHSC1,C1S,C2S,C3S,SCLA,NIMP,ZIMPA,FRIMPA,AMIMPA,EXPTE)
       IRS=IR+IMAX
       DO 168 JR=1,IMAXS
       CARED(IMAX+JR)=0.0
  168  ARED(IMAX+JR)=0.0
       RHS=0.0
       CRHS=0.0

C-----------------------------------------------------------------------
C
C      CCNSE4 PERFORMS THAE MATRIX CONDENSATION SCHEME TO REDUCE
C      THE SIZE OF THE COLLISIONAL-RADIATIVE MATRIX
C
C       HARVEY ANDERSON
C       6/3/98
C
C-----------------------------------------------------------------------

       CALL CCNSE4(A0,CA0,A,CA,RH,CRH,NMINS,NMAX,ARED,CARED,RHS,CRHS,IR,
     &             IMAX,IMAX,KPFS,NLIVS,ILTXRS)
       MULTN1=ISG

C-----------------------------------------------------------------------
C
C       CCNST7 EVALUATES THE SPIN CHANGING CROSS SECTIONS FOR
C       TRANSITIONS FROM THE SINGLET TO THE TRIPLET SIDE
C
C       HARVEY ANDERSON
C       6/3/98
C
C-----------------------------------------------------------------------

       CALL CCNST7(NLREPS,IR,I,V,E,EXE,EXS,K,ISGS,CGBBAS,ENL,ENL2,EXPTE,
     &  EXPTS,KPF,ISG,NMIN,NMAX,IMAXS,A0,CA0,A,CA,RH,CRH,2,NGS,LGS,NGLS,
     &  LTGS,FPGS,NG,LG,NGL,LTG,FPG,ARLS,ISARLS,AC11,RHSC11,C1S,C2S,C3S,
     &  SCLA,NIMP,ZIMPA,FRIMPA,AMIMPA,EXTES)
       DO 167 JR=1,IMAX
       CARED(JR)=0.0
  167  ARED(JR)=0.0

C-----------------------------------------------------------------------
C
C      CCNSE4 PERFORMS THAE MATRIX CONDENSATION SCHEME TO REDUCE
C      THE SIZE OF THE COLLISIONAL-RADIATIVE MATRIX
C
C       HARVEY ANDERSON
C       6/3/98
C
C-----------------------------------------------------------------------

      CALL CCNSE4(A0,CA0,A,CA,RH,CRH,NMIN,NMAX,ARED,CARED,RHS,CRHS,IR,
     &            IMAX,0,KPF,NLIV,ILTXR)
       WRITE(12)ARED,CARED,RHS,CRHS,AC1,RHSC1
  170  CONTINUE
       IMAXT=IMAXT+IMAXS
  171  WRITE(12)IMAXT
       DO 341 IR=1,160
       CARED(IR)=0.0
  341  ARED(IR)=0.0
       DO 340 I=1,1000
       CA(I)=1.0
  340  A(I)=1.0
       A0=0.0
       CA0=0.0
       CALL CCNSE4(A0,CA0,A,CA,RH,CRH,NMIN,NMAX,ARED,CARED,RHS,CRHS,1,0,
     &             0,KPF,NLIV,ILTXR)
       IF(ISP.LE.1)GO TO 345
       A0=0.0
       CA0=0.0
       CALL CCNSE4(A0,CA0,A,CA,RH,CRH,NMINS,NMAX,ARED,CARED,RHS,CRHS,1,
     &             0,IMAX,KPFS,NLIVS,ILTXRS)
  345  WRITE(12)ARED

C-----------------------------------------------------------------------
C
C       TEMPORARY CALL TO STAGE TWO OF THE PROGRAM.
C       
C       NOTE : THE C-R MATRIX IS PASSED TO THE ROUTINE FINISH5 VIA
C              THE SCRATCH FILE ON STREAM 12.   
C
C       HARVEY ANDERSON
C       28/1/98
C
C
C-----------------------------------------------------------------------

        CALL finish5(NIP,INTD,IPRS,ILOW,IONIP,NIONIP,ILPRS,IVDISP,
     &    ZEFF,TS,W,CION,CPY,W1,ZIMPA,DNIMPA,NIMP,IUPS1,IUPS2,STITLE,
     &    NBENG,NTEMP,NDENS, lbndl, lproj)

       CLOSE(12)

       RETURN

C-----------------------------------------------------------------------
  100  FORMAT(20I5)
  102  FORMAT(1H0,8X,'IR',9X,'I',11X,'A0(I)',10X,'CA0(I)',
     &7X,'RH(IR)',8X,'CRH(IR)',8X,'AC(IR)',7X,'RHSC1(IR)')
  103  FORMAT(2I10,5X,1P,7E15.7)
  104  FORMAT(1H0,4X,'I',3X,'I1',7X,'G',9X,'PY',8X,'PYP',9X,'A1',
     &9X,'A2',9X,'A3',9X,'X3',9X,'AI',9X,'UE',9X,'UP',9X,'DNL')
  105  FORMAT(4I10,5X,1P,4E15.7)
  106  FORMAT(10E12.4)
  107  FORMAT(10I10)
  108  FORMAT(1H0)
  109  FORMAT(27I4)
  110  FORMAT(9E12.3)
  111  FORMAT(16I5)
  112  FORMAT(2I5,1P,5E12.4)
  113  FORMAT(2I5,F15.10,F10.5)
  114  FORMAT(2I10,F15.10,F15.5)
  115  FORMAT(8F10.5)
  116  FORMAT(1H1,19X,'Z0',8X,'Z1',
     &8X,'ALF',5X,'AMSZ0',5X,'AMSHYD'/1H ,5A3,5F10.5)
  117  FORMAT(1H0,'DETAILS OF REPRESENTATIVE LEVELS'//7X,'INREP',5X,
     &'ILREP',5X,'ILTREP'/3I10)
  118  FORMAT(1H0,8X,'NL1',7X,'NL2',7X,'NL3'/3I10)
  119  FORMAT(1H0,7X,'NREP(I),I=1,INREP'/5X,24I5)
  120  FORMAT(1H0,8X,'NMIN',6X,'NMAX',6X,'IMAX',3X,'MULTIPLET'/4I10)
  121  FORMAT(3I10,10X,6F10.5)
  122  FORMAT(2I5,1P,6E15.7)
  123  FORMAT(1P,6E15.7)
  125  FORMAT(1H0,8X,'I',5X,'NLREP(I)',4X,'KPF',9X,'N',9X,'L',8X,'LT',
     &        7X,'XREF')
  126  FORMAT(1H0,'PLASMA DATA'//7X,'TE',8X,'TP',8X,'TS',6X,'DENS',
     &5X,'DENSP',5X,'DENIMP',6X,'ZIMP',6X,'ZEFF',8X,'W'
     &       /1X,1P,9E10.2//8X,'ATE',12X,'ATP',12X,'ATS',
     &12X,'RHO',12X,'RHOP',12X,'RHOIMP'/1P,6E15.6)
  127  FORMAT(2I10,5X,1P,7E15.7)
 1000  FORMAT(I5,3F10.5,6I5)
 1001  FORMAT(4(3F7.2,5X))
 1002  FORMAT(I10,3F10.5)
 1003  FORMAT(1H0,5X,'PARENT L',4X,'MULTI'/2I10)
 1004  FORMAT(1H0,'LEVELS OF MULTIPLICITY',I3)
 1006  FORMAT(10A1)
 1007  FORMAT(4(4F8.2))
 1008  FORMAT(1H0,'NUMBER OF DIELECTRONIC CORE TRANSITIONS'
     &/I5/1H ,'PRIMARY TRANSITIONS '/
     &1H ,4X,'EIJ',7X,'FIJ',  7X,'COR(J),J=1,JCOR')
 1009  FORMAT(1H ,'SECONDARY TRANSITIONS'/
     &1H ,4X,'EIJS',6X,'FIJS',6X,'CORS(J),J=1,JCORS')
 1010  FORMAT(1H0,4X,'BEAM ENER.(EV)',10X,'H DENSITY(CM-3)',10X,
     &'FLUX(CM-2 SEC-1)'/1H ,5X,1PE10.2,15X,1PE10.2,15X,1PE10.2)
 1011  FORMAT(6E12.4)
 1012  FORMAT(3E12.4)
 1973  FORMAT(2E12.4)
 2000  FORMAT(I5,5X,I5,5X,I5,5X,I5,5X,I5)
 2002  FORMAT('   LMAX     NTAIL     LSWCH     GHNL      RNDEGV')
 2003  FORMAT(7I5,F5.1,F10.5)
 2004  FORMAT(1H0,'  NIP       INTD      IPRS      ILOW     IONIP    NIO
     &NIP   ILPRS     IVDISP     ZEFF       DEDEG/IH')
 2005 FORMAT(I5,7(5X,I5),5X,F5.1,5X,F10.5)
C-----------------------------------------------------------------------

      END
