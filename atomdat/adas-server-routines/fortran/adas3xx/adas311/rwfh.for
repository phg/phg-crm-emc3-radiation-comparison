CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas311/rwfh.for,v 1.3 2005/02/22 19:14:20 mog Exp $ Date $Date: 2005/02/22 19:14:20 $
CX
C-------------------------------------------------------------------------
C	VERSION : 1.2
C	MODIFIED: Martin O'Mullane      
C       DATE    : 08-11-2004
C		  Alter dimensions from 200 to 500. 	
C
C-------------------------------------------------------------------------
       SUBROUTINE RWFH(A,NA,IPA,UA,N,L)
       IMPLICIT REAL*8(A-H,O-Z)
       DIMENSION A(500),GAM(500),JGAM(500)
       COMMON /FAC/GAM,JGAM
       EN=N
       X=2.0/EN
       T=GAM(N+L+1)/(EN*EN*GAM(N-L))
       T=T**0.5*X**(L+1)/GAM(2*L+2)
       T=T*8.0**(JGAM(N+L+1)-JGAM(N-L)-2*JGAM(2*L+2))
       A(1)=T
       J=N-L-1
       IF(J)10,10,5
    5  DO 8 I=1,J
       U=I*(2*L+I+1)
       T=DFLOAT(L+I-N)*T*X/U
    8  A(I+1)=T
   10  CONTINUE
       UA=-0.5*X
       IPA=L+1
       NA=J+1
       RETURN
       END
