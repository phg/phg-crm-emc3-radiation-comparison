CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas311/psint.for,v 1.3 2005/02/22 19:14:20 mog Exp $ Date $Date: 2005/02/22 19:14:20 $
CX
C-------------------------------------------------------------------------
C	VERSION : 1.2
C	MODIFIED: Martin O'Mullane      
C       DATE    : 08-11-2004
C		  Alter dimensions from 200 to 500. 	
C
C-------------------------------------------------------------------------
       SUBROUTINE PSINT(C,NC,IPC,UC,D,ND,IPD,UD)
       IMPLICIT REAL*8(A-H,O-Z)
C  INTEGRATES X**IPC*EXP(UC*X)*(C(1)+X*C(2)+...NC TERMS...) TO GIVE
C  -X**IPD*EXP(UD*X)*(D(1)+X*D(2)+...ND TERMS...).NOTE SIGN IN FORMULA.
       DIMENSION C(500),D(500)
       UD=UC
       IF(UC)2,15,2
    2  ND=NC+IPC
       IPD=0
       T=0.0
       RUC=-1.0/UC
       DO 3 I=1,NC
       IC=NC-I+1
       XI=IC+IPC
       T1=T
       T=C(IC)+RUC*XI*T
       IF(1.0D12*DABS(T)-DABS(T1))17,17,3
   17  T=0.0
    3  CONTINUE
       IF(IPC)20,6,4
    4  DO 5 I=1,IPC
       IC=IPC-I+1
       XIC=IC
    5  T=RUC*XIC*T
    6  D(1)=RUC*T
       IF(IPC)20,9,7
    7  DO 8 I=1,IPC
       XI=I
    8  D(I+1)=-UC*D(I)/XI
    9  IF(NC-1)20,20,10
   10  DO 11 I=2,NC
       XI=IPC+I-1
   11  D(I+IPC)=(-UC*D(I+IPC-1)-C(I-1))/XI
       GO TO 20
   15  IPD=IPC+1
       ND=NC
       DO 16 I=1,NC
       XI=I
   16  D(I)=-C(I)/XI
   20  RETURN
      END
