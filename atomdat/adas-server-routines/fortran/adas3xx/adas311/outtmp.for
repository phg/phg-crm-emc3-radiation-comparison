CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas311/outtmp.for,v 1.4 2005/02/23 19:54:57 mog Exp $ Date $Date: 2005/02/23 19:54:57 $
CX
        SUBROUTINE OUTTMP(TITLE,MN,RX3,DPT,EHCT,NHCT,LHCT,NIP,NEX,IPRT,
     &  NDEL,INTD,IPRS,ILOW,IONIP,NIONIP,ILPRS,IVDISP,ZEFF,
     &  DEDEG,NL1,NL2,NL3,Z0,Z1,ALF,AMSZ0,AMSHYD,LP,ISP,LMAX,
     &  NTAIL,LSWCH,IGHNL,IRNDGV,MJ,IREF,JREF,SREF,WREF,CREF,
     &  WIREF,WCREF,MG,IRA,TE,TP,TS,DENS,DENSP,W,BMENER,DENSH,
     &  NIMP,ZIMPA,AMIMPA,FRIMPA,ZP,EMP,NGA,LGA,ISGA,NGLA,INAL,
     &  INALTG,ANAEGY,ANAFPG,NSERA,LAA,LTAA,INBL,INBLT,INBIN0,
     &  INBN1,ANBE1,INBN2,ANBE2,INBN3,ANBE3,INREP,ILREP,ILTREP,
     &  NREP,INLREP1,INLMP,IUTMP,IDIEL,EIJA,FIJA,CORA,EIJSA,
     &  FIJSA,CORSA )
C-------------------------------------------------------------------------
C
C  ********************* FORTRAN 77 ROUTINE : OUTTMP *********************
C
C  PURPOSE : ROUTINE TO WRITE INPUT TO TEMPORARY FILE.
C          
C
C  CONTACT : HARVEY ANDERSON
C            UNIVERSITY OF STRATHCLYDE
C            ANDERSON@BARWANI.PHYS.STRATH.AC.UK
C
C  DATE    : 04/21/98
C
C
C  VERSION : 1.2
C  MODIFIED: RICHARD MARTIN  
C  DATE    : 21-03-2000
C            DECLARED LOGICAL VARIABLE LOPEN.
C
C  VERSION : 1.3
C  MODIFIED: Martin O'Mullane      
C  DATE    : 3-6-2000
C            Tidied code to be within column 72. 
C            TITLE was passed as c*6 but here was c*1 title(10).
C            Change to make them compatible and alter writing of
C            TITLE to file.        
C
C  VERSION : 1.4
C  MODIFIED: Martin O'Mullane      
C  DATE    : 20-01-2004
C            Further code tidying.
C
C-------------------------------------------------------------------------

      REAL*8  RX3     ,       DPT     ,       EHCT    ,       ZEFF    ,
     &        DEDEG   ,       Z0      ,       Z1      ,       ALF     ,
     &        AMSZ0   ,       AMSHYD  ,       TE      ,       TP      ,
     &        TS      ,       DENS    ,       DENSP   ,       W       ,
     &        BMENER  ,       DENSH   ,       ZP      ,       EMP
C
      REAL*8  SREF(10) ,   WREF(10)   ,       CREF(10),      WIREF(10),
     &        WCREF(10),  ZIMPA(10)   ,     AMIMPA(10),     FRIMPA(10),
     &        ANAEGY(2,10),ANAFPG(2,10),   ANBE1(2,10),    ANBE2(2,10),
     &        ANBE3(2,10), EIJA(10),FIJA(10),CORA(10,6),
     &        EIJSA(10), FIJSA(10) , CORSA(10,6)

C
      INTEGER MN      ,       NHCT    ,       LHCT    ,       NIP     ,
     &        NEX     ,       IPRT    ,       NDEL    ,       INTD    ,
     &        IPRS    ,       ILOW    ,       IONIP   ,       NIONIP  ,
     &        ILPRS   ,       IVDISP  ,       NL1     ,       NL2     ,
     &        NL3     ,       LP      ,       ISP     ,       LMAX    ,
     &        NTAIL   ,       LSWCH   ,       IGHNL   ,       IRNDGV  ,
     &        MJ      ,       NIMP

C
      INTEGER INREP   ,       ILREP   ,       ILTREP  ,       IUTMP
C
      INTEGER IREF(10),       JREF(10),       IRA(20) ,   INAL(2,10),
     &        INALTG(2,10),  INBL(2,10),    INBLT(2,10),INBIN0(2,10),
     &        INBN1(2,10), INBN2(2,10),    INBN3(2,10),NREP(31),
     &        INLREP1(10),INLMP(10,10),        NGA(2) ,LGA(2) ,
     &        ISGA(2) ,       NGLA(2),        NSERA(2),LAA(2) ,
     &        LTAA(2)

      CHARACTER TITLE*6
        LOGICAL       LOPEN
C


C-----------------------------------------------------------------------
C OPEN TEMPORARY OUTPUT FILE.
C-----------------------------------------------------------------------

      INQUIRE( IUTMP , OPENED=LOPEN )
      IF ( LOPEN ) THEN
         REWIND( UNIT=IUTMP )
      ELSE
         OPEN( UNIT=IUTMP , STATUS='SCRATCH' , FORM='FORMATTED' ,
     &         ACCESS='SEQUENTIAL' )
      ENDIF

C-----------------------------------------------------------------------
C WRITE PARAMETERS TO TEMPORARY FILE.
C-----------------------------------------------------------------------

        
      WRITE(IUTMP,'(a)')  TITLE
      
      WRITE(IUTMP,101) MN,RX3,DPT,EHCT,NHCT,LHCT,NIP,NEX,IPRT,NDEL
        WRITE(IUTMP,102)  INTD,IPRS,ILOW,IONIP,NIONIP,ILPRS,IVDISP,
     &                   ZEFF,DEDEG
        WRITE(IUTMP,103)  NL1,NL2,NL3
        WRITE(IUTMP,112)  Z0,Z1,ALF,AMSZ0,AMSHYD
        WRITE(IUTMP,103)  LP,ISP
        WRITE(IUTMP,103)  LMAX,NTAIL,LSWCH,IGHNL,IRNDGV
        WRITE(IUTMP,103)  IDIEL
        DO I=1,IDIEL
          WRITE(IUTMP,113) EIJA(I),FIJA(I),(CORA(I,J),J=1,6)
        ENDDO
        DO I=1,IDIEL
          WRITE(IUTMP,113) EIJSA(I),FIJSA(I),(CORSA(I,J),J=1,6)
        ENDDO
        WRITE(IUTMP,104)  MJ
        DO K=1,MJ
                WRITE(IUTMP,105) IREF(k),JREF(k),SREF(k),WREF(k),
     &          CREF(k),WIREF(k),WCREF(k)

        ENDDO
        WRITE(IUTMP,104) MG
        WRITE(IUTMP,106) (IRA(I),I=1,MG)
        WRITE(IUTMP,107)TE,TP,TS,DENS,DENSP,W,BMENER,DENSH
        IF ( DENS .GT. DENSP ) THEN
           WRITE(IUTMP,104) NIMP
                DO I=1,NIMP
                  WRITE(IUTMP,108) ZIMPA(I),AMIMPA(I),FRIMPA(I)
                ENDDO
        ENDIF
        WRITE(IUTMP,109)ZP,EMP

        WRITE(IUTMP,106) NGA(1),LGA(1),ISGA(1),NGLA(1)

        DO  I=1,NGLA(1)
           WRITE(IUTMP,110) INAL(1,I), INALTG(1,I),ANAEGY(1,I),
     &                 ANAFPG(1,I)
        ENDDO
        WRITE(IUTMP,111) NSERA(1),LAA(1),LTAA(1)

        DO I=1,NSERA(1)
          WRITE(IUTMP,111) INBL(1,I),INBLT(1,I),INBIN0(1,I),INBN1(1,I),
     &                ANBE1(1,I),INBN2(1,I),ANBE2(1,I),INBN3(1,I),
     &                ANBE3(1,I)
        ENDDO
        IF ( ISP.GT.1 ) THEN
           WRITE(IUTMP,106) NGA(2),LGA(2),ISGA(2),NGLA(2)
             DO  I=1,NGLA(2)
                WRITE(IUTMP,110) INAL(2,I), INALTG(2,I),ANAEGY(2,I),
     &                      ANAFPG(2,I)
             ENDDO
           WRITE(IUTMP,111) NSERA(2),LAA(2),LTAA(2)

          DO I=1,NSERA(2)
           WRITE(IUTMP,111) INBL(2,I),INBLT(2,I),INBIN0(2,I),INBN1(2,I),
     &     ANBE1(2,I), INBN2(2,I),ANBE2(2,I),INBN3(2,I),ANBE3(2,I)

          ENDDO
        ENDIF
        WRITE(IUTMP,103)INREP,ILREP,ILTREP

        WRITE(IUTMP,103)(NREP(I),I=1,INREP)

        DO I=1,NL2
         WRITE(IUTMP,103) INLREP1(I)
         WRITE(IUTMP,103) (INLMP(I,L1),L1=1,INLREP1(I) )
        ENDDO


      RETURN
 100  FORMAT(10A1)
 101  FORMAT(I5,3F10.5,6I5)
 102  FORMAT(7I5,F5.1,F10.5)
 103  FORMAT(16I5)
 104  FORMAT(1I5)
 105  FORMAT(2I5,1P,5E12.4)
 106  FORMAT(20I5)
 107  FORMAT(6E12.4)
 108  FORMAT(3E12.4)
 109  FORMAT(2E12.4)
 110  FORMAT(2I5,F15.10,F10.5)
 111  FORMAT(3I5,3(I5,F15.10))
 112  FORMAT(8F10.5)
 113  FORMAT(8F10.5)
      END
