CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas311/supphe1.for,v 1.7 2007/05/17 17:04:12 allan Exp $ Date $Date: 2007/05/17 17:04:12 $
CX
        SUBROUTINE SUPPHE1(TEV,EBEAM,TIEV,NIMP,ZIMPA,FRIMPA, AMIMPA,
     &                     DSLPATH )
       IMPLICIT REAL*8(A-H,O-Z)
C-----------------------------------------------------------------------
C
C       ******  FORTRAN 77 ROUTINE : SUPPHE1.F  ******
C
C       PURPOSE : ACCESS FUNDAMENTAL CROSS SECTION DATA FOR THE
C                 BUNDLED NL CALCULATION.
C
C       INPUT   :
C
C       (R*8)   TEV            : ELECTRON TEMPERATURE (eV)
C       (R*8)   EBEAM          : NEUTRAL BEAM ENERGY (eV/AMU)
C       (R*8)   TIEV           : ION TEMPERATURE (eV)
C       (R*8)   ZIMP           : Z OF EFFECTIVE IMPURITY FOR ION
C                                COLLISIONS ( EXC H+ ).
C       (I*4)   ITYP1          : 1  OBTAIN ELECTRON IMPACT EXCITATION
C                                   DATA FROM SPECIFIC ION FILE.
C                                0  DO NOT OBTAIN ELECTRON IMPACT
C                                   EXCITATION DATA FROM SPECIFIC ION.
C       (I*4)   ITYP2          : 1  OBTAIN ELECTRON IMPACT IONISATION
C                                   DATA FROM SZD TYPE FILE.
C                                0  DO NOT OBTAIN ELECTRON IMPACT
C                                   IONISATION DATA FROM SZD FILE.
C       (I*4)   ITYP3          : 1  OBTAIN H+ IMPACT EXCITATION
C                                   DATA FROM ADF02 TYPE FILE.
C                                0  DO NOT OBTAIN H+ IMPACT EXCITATION
C                                   DATA FROM ADF02 TYPE FILE.
C       (I*4)   ITYP4          : 1  OBTAIN H+ IMPACT IONISATION AND
C                                   CHARGE EXCHANGE FROM ADF02 TYPE FILE.
C                                0  DO NOT OBTAIN H+ IMPACT IONISATION
C                                   AND CHARGE EXCHANGE FROM ADF02 TYPE FILE.
C       (I*4)   ITYP5          : 1  OBTAIN ZIMP ION IMPACT EXCITATION FROM
C                                   ADF02 TYPE FILE.
C                                0  DO NOT OBTAIN ZIMP ION IMPACT EXCITATION
C                                   FROM ADF02 TYPE FILE.
C       (I*4)   ITYP6          : 1  OBTAIN ZIMP ION IMPACT IONISATION AND
C                                   CHARGE EXCHANGE FROM ADF02 TYPE FILE.
C                                0  DO NOT OBTAIN ZIMP ION IMPACT IONISATION
C                                   AND CHARGE EXCHANGE FROM ADF02 TYPE FILE.
C       (CHR)   DSLPATH        : CHARCTER STRING CONTAINING THE USER NAME.
C                                INFORMATION REQUIRED TO OPEN UP LOW LEVEL
C                                DATA FILES (OBTAINED FROM IDL).
C       (C*120) TITLX          : CHARACTER STRING SPECIFYING THE SOURCE
C                                OF IONISATION DATA
C
C       OUTPUT  :
C
C       (I*4)   NSYS           : NUMBER OF SPIN SYSTEMS (=2)
C       (I*4)   ISYSA(IS)      : MULTIPLICITY OF SPIN SYSTEM
C       (I*4)   NNA(IR)        : N-SHELL     FOR COPDAT FILE LEVEL INDEX IR
C       (I*4)   ISA(IR)        : MULTIPLICITY
C       (I*4)   ILA(IR)        : TOTAL ORBITAL ANGULAR MOMENTUM
C       (R*8)   WTA(IR)        : STATISTICAL WEIGHT
C       (R*8)   ATBE(IR,IR'')  : EINSTEIN A-COEFFICIENT
C       (R*8)   XTBE(IR,IR'')  : ELECTRON IMPACT EXCITATION RATE COEFFICIENT.
C       (I*4)   LXTBE(IR,IR'') : ELECTRON IMPACT EXCITATION TYPE MARKER
C                                (0 =NO VALUE, 1=VALUE)
C       (R*8)   XTBP(I,I'',IS) : H+ IMPACT EXCITATION RATE COEFFICIENT.
C       (R*8)   XTBZ(I,I'',IS) : ZIMP ION IMPACT EXCITATION RATE COEFFICIENT
C       (R*8)   STBE(I,IS)     : ELECTRON IMPACT IONISATION RATE COEFFICIENT
C       (R*8)   STBP(I,IS)     : H+ ION IMPACT IONISATION AND CHARGE
C                                EXCHANGE RATE COEFFICIENT.
C       (R*8)   STBZ(I,IS)     : ZIMP ION IMPACT IONSATION AND CHARGE
C                                EXCHANGE RATE COEFFICIENT
C       (I*4)   LXTBP(I,I'',IS): H+ IMPACT EXCITATION TYPE  MARKER
C                                (0 =NO VALUE, 1=VALUE)
C       (I*4)   LXTBZ(I,I'',IS): ZIMP ION IMPACT EXCITATION TYPE MARKER
C       (I*4)   LSTBE(I,IS)    : ELECTRON IMPACT IONISATION TYPE MARKER
C       (I*4)   LSTBP(I,IS)    : H+ IMPACT IONISATION & CHARGE EXCHANGE
C                                TYPE MARKER
C       (I*4)   LSTBZ(I,IS)    : ZIMP ION IMPACT IONISATION AND CHARGE EXCHANGE
C                                TYPE MARKER
C       (R*8)   PXTBP(I,IS)    : H+ IMPACT EXCITATION TYPE PROJECTION MULTIPLIER
C       (R*8)   PXTBZ(I,IS)    : ZIMP ION IMPACT EXCITATION TYPE PROJECTION
C                                MULTIPLIER
C       (R*8)   PSTBE(IS)      : ELECTRON IMPACT IONISATION TYPE PROJECTION
C                                MULTIPLIER
C       (R*8)   PSTBP(IS)      : H+ IONISATION & CHARGE EXCHANGE TYPE
C                                PROJECTION MULTIPLIER
C       (R*8)   PSTBZ(IS)      : ZIMP ION IMPACT IONISATION AND CHARGE EXCHANGE
C                                TYPE PROJECTION MULTIPLIER
C       (I*4)   LPXTBP(I,IS)   : H+ IMPACT EXCITAION TYPE PROJECTION MULTIPLIER
C                                USED ABOVE THIS N'
C       (I*4)   LPXTBZ(I,IS)   : ZIMP ION IMPACT EXCITATION TYPE 5 PROJECTION
C                                MULTIPLIER USED ABOVE THIS N'
C       (I*4)   LPSTBP(IS)     : H+ IMPACT IONISATION & CHARGE EXCHANGE TYPE
C                                PROJECTION MULTIPLIER USED ABOVE THIS N
C       (I*4)   LPSTBZ (IS)    : ZIMP ION IMPACT IONISATION AND CHARGE EXCHANGE
C                                TYPE PROJECTION MULTIPLIER USED ABOBE THIS N
C
C       ROUTINES:
C
C               ROUTINE         SOURCE          DESCRIPTION
C             -----------------------------------------------
C                XXSLEN          ADAS           IDENTIFY THE FIRST & LAST
C                                               NON-BLANK CHARACTER IN A
C                                               STRING.
C                XXWORD          ADAS           MANIPULATES STRINGS.
C                SZD             ADAS           RETURNS ELECTRON IMPACT
C                                               IONISATION RATES WHICH
C                                               ARE OBTAINED FROM ADF07
C                                               TYPE FILE.
C                QHE             ADAS           RETURNS BEAM/THERMAL MAXWELL
C                                               AVERAGED RATE COEFFICIENTS.
C
C
C       CONTACT : HARVEY ANDERSON
C                 UNIVERSITY OF STRATHCLYDE
C                 ANDERSON@PHYS.STRATH.AC.UK
C
C       DATE    : 26/11/97
C
C       MODIFICATIONS   :  REPLACED NAG ROUTINES WITH THE NEAR
C                          ADAS EQUIVALENT ROUTINES.
C                          HARVEY ANDERSON
C       DATE            :  10/3/99
C
C VERSION: 1.2                          DATE: 15-10-99
C MODIFIED: RICHARD MARTIN
C               REMOVED 'ACTION' FROM OPEN STATEMENT.
C
C VERSION: 1.3                          DATE: 07-07-2004
C MODIFIED: ALLAN WHITEFORD
C               -CHANGED CALLS FROM DXNB{A,B}F TO XXNB{A,B}F
C
C VERSION: 1.4                          DATE: 04-11-2004
C MODIFIED: ALLAN WHITEFORD
C               -ADDED DECLARATION OF TITLX VARIABLE
C
C VERSION : 1.5                         
C DATE    : 22-02-2005
C MODIFIED: Martin O'Mullane
C            - Declare ltrng as a logical.
C            - Replace TITLF with DSLPATH in calls to qhe for 
C              itypes 4 and 5.
C
C VERSION : 1.6
C DATE    : 16-05-07
C MODIFIED: Allan Whiteford    
C            - Moved parameter statement to below comment block
C              as part of subroutine documentation procedure.
C
C-----------------------------------------------------------------------
       PARAMETER (NCDIM=50,NDLOW=10,NLDIM=19)
       CHARACTER DSNAME*80,TITLF*80,STRING*80, DSLPATH*80,DSTEMP2*80
       CHARACTER TITLED*3 , BUFFER*128 , CDELIM*7  , CHR*1 , TITLX*120
C
       INTEGER   IFIRST(1) , ILAST(1)
C
       DIMENSION NA(NCDIM),N1A(NCDIM),ICODEA(NCDIM),IMULTI(NCDIM)
       DIMENSION COEFFA(NCDIM),COEFFB(NCDIM)
       DIMENSION EA(24),OA(24),SZDA(1),TVAL(1)
       DIMENSION TEA(24),TEALG(24),GAMA(24)
       DIMENSION XKA(28),C(28),WRK(160)
       DIMENSION WTS(24),B(24),AB(28,4),DIAG(28)
       logical   ltrng(1)
C
C       VARIABLES REQUIRED TO HANDLE IMPURITIES.
C
       DIMENSION ZIMPA(10) , FRIMPA(10) , AMIMPA(10)

       COMMON /SUPPL/ATBE(NLDIM,NLDIM),XTBE(NLDIM,NLDIM),WTA(NLDIM),
     &               ENA(NLDIM),XTBP(NDLOW,NDLOW,2),XTBZ(NDLOW,NDLOW,2),
     &               PXTBP(NDLOW,2),PXTBZ(NDLOW,2),PSTBE(2),PSTBP(2),
     &               PSTBZ(2),STBE(NDLOW,2),STBP(NDLOW,2),STBZ(NDLOW,2),
     &               NNA(NLDIM),ISA(NLDIM),ILA(NLDIM),
     &               LXTBE(NLDIM,NLDIM),LXTBP(NDLOW,NDLOW,2),
     &               LXTBZ(NDLOW,NDLOW,2),LPXTBP(NDLOW,2),
     &               LPXTBZ(NDLOW,2),LSTBE(NDLOW,2),LSTBP(NDLOW,2),
     &               LSTBZ(NDLOW,2),LPSTBE(2),LPSTBP(2),LPSTBZ(2),
     &               ITYP1 ,ITYP2 ,ITYP3 ,ITYP4 ,ITYP5 ,ITYP6 ,
     &               NSYS  ,ISYSA(2) , NLEV
C
C-----------------------------------------------------------------------
C
       DATA      LCK,LWRK/28,160/
       DATA      CDELIM / ' ()<>{}' /
C
C-----------------------------------------------------------------------
C  SET UP REFERENCING FUNCTION
C-----------------------------------------------------------------------
C
        
        IREF(N,L)=(N*(N-1))/2+L+1
C
C-----------------------------------------------------------------------
C        RESET PARAMETERS FOR MULTIPLE LOOP CONFIGURATION
C-----------------------------------------------------------------------
C
        DO I=1,NDLOW
          DO J=1,2
              STBZ(I,J) = 0.0D0 
          ENDDO
        ENDDO   

C        write(0,*) '********** values inside supphe1.f *********'
C       write(0,*) 'Ebeam=', EBEAM
C        WRITE(0,*) 'TEV = ', TEV

C
C-----------------------------------------------------------------------
C  ASSUME  HELIUM      MASS=4
C          DEUTERIUM   MASS=2
C          DEUTERONS   MASS=2
C
C          ZIMP IONS   MASS=2*ZIMP
C
C  COMPUTE CENTRE OF MASS ENERGIES FOR THRESHOLDS AND 'DETAILED BALANCE'
C-----------------------------------------------------------------------
C

       AMSSRH=1.333D0
       ECMH=AMSSRH*EBEAM
C
       NSYS=2
       ISYSA(1)=1
       ISYSA(2)=3
C
       IPASS=0
       IPASSE=0
       NCOEFF=0
       TE=11605.4D0*TEV
       ATE=157890.0D0/TE
       TELG=DLOG(TE)

C
C-----------------------------------------------------------------------
C
C  DATA TYPE (1): ELECTRON IMPACT EXCITATION DATA IS OBTAINED
C                 BY ACCESSING A SPECIFIC ION FILE. A POINT
C                 TO NOTE IS THAT THE SPECIFIC ION FILES ARE
C                 SPLIT UP INTO A LOW AND HIGH TEMPERATURE
C                 DATASET.
C
C       NOTES :   ADF04 FILE CONTAINS ELECTRON IMPACT EXCITATION
C                 RATES. EACH RATE IS CHARACTERISED BY TWO
C                 LEVEL. ALOWER AND UPPER LEVEL. I1 IS THE INDEX
C                 TO THE UPPER LEVEL CONTAINED WITHIN THE ADF04
C                 TYPE FILE. I IS THE INDEX OF THE LOWER LEVEL
C                 CONTAINED WITHIN THE ADF04 TYPE FILE.
C
C
C       HARVEY ANDERSON 28/1/98
C
C-----------------------------------------------------------------------
C
        CALL XXSLEN( DSLPATH , IFIRST, ILAST )
       IF(ITYP1.EQ.1) THEN
           IF(TE.LT.1.5D6)THEN
             DSTEMP2='/adf04/helike/helike_kvi97#he0.dat'
             DSNAME=DSLPATH(IFIRST(1):ILAST(1))//DSTEMP2
           ELSE
             DSTEMP2='/adf04/helike/helike_kvi97#he0.dat'
             DSNAME=DSLPATH(IFIRST(1):ILAST(1))//DSTEMP2
           ENDIF

C       write(0,'(A)') DSNAME
C
           OPEN(UNIT=15,FILE=DSNAME)

           READ(15,1008) TITLED, IZ, IZ0, IZ1, BUFFER(1:55)
C
           J     = 1
           IWORD = 1
           CALL XXWORD( BUFFER(1:55) , CDELIM   , IWORD  ,
     &                  J            ,
     &                  IFIRST(1)    , ILAST(1) , NWORDS )


          READ(BUFFER(IFIRST(1):ILAST(1)),'(F7.0)') BWNO
           NLEV=0
    3      READ(15,1000)STRING
           IF(STRING(1:5).NE.'   -1')THEN
               NLEV=NLEV+1
               READ(STRING,1006)NNA(NLEV),ISA(NLEV),ILA(NLEV),
     &                          WTA(NLEV),ENA(NLEV)
              WTA(NLEV)=2.0D0*WTA(NLEV)+1.0D0
              ENA(NLEV)=(BWNO-ENA(NLEV))/109737.3
               GO TO 3
           ENDIF
C
C------------------------------------------------------------
C
C       WARNING : THE VARIABLE NTEMP INDICATES THE NOS.
C                 OF TEMPERATURES AVAILABLE IN THE
C                 ADF04 TYPE FILE. THIS VARIABLE SHOULD
C                 BE APPROPRIATELY MODIFIED IF A NEW
C                 ADF04 TYPE FILE IS USED AND THE NOS.
C                 OF TEMPERATURES IS DIFFERENT.
C
C       HARVEY ANDERSON
C       18/9/98
C
C------------------------------------------------------------
C
           NTEMP=14
           READ(15,1001)(TEA(IT),IT=1,NTEMP)
           DO 1 IT=1,NTEMP
            TEALG(IT)=DLOG(TEA(IT))
            WTS(IT)=1.0
    1      CONTINUE

    2      READ(15,1005)CHR,I1,I,A,(GAMA(IT),IT=1,NTEMP)
           IF(I1.GT.0.AND.CHR.EQ.' ') THEN
               DE=ENA(I)-ENA(I1)
               WN=WTA(I)
               WN1=WTA(I1)
               X=1.57890D5*DE/TE
               IF(TE.GE.TEA(NTEMP))THEN
                   GAM=GAMA(NTEMP)*EEI(X)/EEI(TE*X/TEA(NTEMP))
               ELSE
                   IFAIL=0
C
C------------------------------------------------------------
C
C   ADDED DO LOOP LABELLED 176 TO SATISFY THE ROUTINE DXNBAF
C
C   HARVEY ANDERSON 10/3/99
C
C------------------------------------------------------------
C
C
                   DO 176, IT=3,NTEMP-2
176                    XKA(IT+2) = TEALG(IT)

CX                   CALL E01BAF(NTEMP,TEALG,GAMA,XKA,C,LCK,
CX     &                         WRK,LWRK,IFAIL)

            CALL XXNBAF(NTEMP,NTEMP+4,TEALG,GAMA,WTS,XKA,B,AB,
     &                  DIAG,C,SS,IFAIL)

            CALL XXNBBF(NTEMP+4,XKA,C,TELG,GAM,IFAIL)

CX                  CALL E02BBF(NTEMP+4,XKA,C,TELG,GAM,IFAIL)

               ENDIF
               LXTBE(I,I1)=1
               XTBE(I,I1)=2.17161D-8*DSQRT(X/DE)*DEXP(-X)*GAM/WN

CX             write(3,*) '[I:I1:XTBE(I,I1)]', I,I1,XTBE(I,I1)
               ATBE(I,I1)=0.0D0
               LXTBE(I1,I)=1
               XTBE(I1,I)=2.17161D-8*DSQRT(X/DE)*GAM/WN1
CX             write(3,*) '[I1:I:XTBE(I1,I)]', I,I1,XTBE(I1,I)
               ATBE(I1,I)=A
               GO TO 2
           ENDIF
            CLOSE(15)
C
       ENDIF
C
C-----------------------------------------------------------------------
C
C  DATA TYPE (2): ELECTRON IMPACT IONISATION DATA IS OBTAINED AT THIS
C                 POINT BY ACCESSING AN ADF07 TYPE FILE. THAT IS AN
C                 SZD TYPE FILE.A POINT TO NOTE IS THAT THE DATA
C                 FILE CONTAINS RATES FROM THE GROUND AS WELL AS
C                 EXCITED SINGLET AND TRIPLET STATES.
C
C
C         ATOMIC PROCESS: ELECTRON IMPACT IONISATION
C          INITIAL   ION: HE(1s2 1S) HE(1s2s 3S)  He(1s2s 1S) HE(1S2S )
C            FINAL   ION: HE(1s  2S) HE(1S   2S)  HE(1S   2S) HE(*)
C                (CODE) :      1           2           3          4
C
C
C
C             MODIFIED  :
C                         HARVEY ANDERSON
C                         UNIVERSITY OF STRATHCLYDE
C                         27/1/98
C
C-----------------------------------------------------------------------
C
       IF(ITYP2.EQ.1)THEN
        
           IBSEL=1
           IZ0IN=2
           ITVAL=1
           TVAL(1)=TEV
           NCOEFF=NCOEFF+1
           ICODEA(NCOEFF)=2
           IMULTI(NCOEFF)=1
           I=IREF(1,0)
           NA(NCOEFF)=I
           N1A(NCOEFF)=0
           LSTBE(I,1)=1
           CALL CASSZD( IBSEL  , IZ0IN  ,
     &             ITVAL  , TVAL   ,
     &             BWNO   , IZ     , IZ1  ,
     &             METI   , METF   ,
     &             SZDA   , LTRNG  ,
     &             TITLX  , IRCODE , DSLPATH )
C
           STBE(I,1)=SZDA(1)
           COEFFA(NCOEFF)=STBE(I,1)
           COEFFB(NCOEFF)=0.0D0
C
           IBSEL=3
           IZ0IN=2
           ITVAL=1
           TVAL(1)=TEV
           NCOEFF=NCOEFF+1
           ICODEA(NCOEFF)=2
           IMULTI(NCOEFF)=1
           I=IREF(2,0)
           NA(NCOEFF)=I
           N1A(NCOEFF)=0
           LSTBE(I,1)=1
           CALL CASSZD( IBSEL  , IZ0IN  ,
     &             ITVAL  , TVAL   ,
     &             BWNO   , IZ     , IZ1  ,
     &             METI   , METF   ,
     &             SZDA   , LTRNG  ,
     &             TITLX  , IRCODE , DSLPATH )
C
           STBE(I,1)=SZDA(1)
           COEFFA(NCOEFF)=STBE(I,1)
           COEFFB(NCOEFF)=0.0D0
C
           IBSEL=2
           IZ0IN=2
           ITVAL=1
           TVAL(1)=TEV
           NCOEFF=NCOEFF+1
           ICODEA(NCOEFF)=2
           IMULTI(NCOEFF)=3
           I=IREF(2,0)
           NA(NCOEFF)=I
           N1A(NCOEFF)=0
           LSTBE(I,2)=1
           CALL CASSZD( IBSEL  , IZ0IN  ,
     &             ITVAL  , TVAL   ,
     &             BWNO   , IZ     , IZ1  ,
     &             METI   , METF   ,
     &             SZDA   , LTRNG  ,
     &             TITLX  , IRCODE , DSLPATH )
C
           STBE(I,2)=SZDA(1)
           COEFFA(NCOEFF)=STBE(I,2)
           COEFFB(NCOEFF)=0.0D0
       ENDIF
C
C-----------------------------------------------------------------------
c
C  DATA TYPE (3): H+ IMPACT EXCITATION DATA IS OBTAINED BY ACCESSING
C                 AN ADF02 TYPE FILE. THAT IS AN IONATOM DATA SET.THE
C                 IONATOM DATA FILE FOR WHICH THE SELECT NUMBERS HAVE
C                 BEEN CONFIGURED TO IS SIA#HE_J91#HE.DAT.
C                 ( H+ ION ONLY EXCITES THE SINGLETS )
c               
C               SELECT NUMBERS :
C
C            TRANSITIONS:1S-2S,1S-3S,1S-4S,1S-2P,1S-3P,1S-4P,1S-3D,1S-4D
C                (CODE) :  53    54    55    56    57    58    59   60
C
C                 NOTE  :
C                         THE ABOVE TRANSITIONS ARE SINGLETS. CHECKED
C                         STATISTICAL WEIGHTS.
C                       
C
C             MODIFIED  :
C                         HARVEY ANDERSON
C                         UNIVERSITY OF STRATHCLYDE
C                         27/1/98
C
C       
C-----------------------------------------------------------------------

       IF(ITYP3.EQ.1)THEN
C
           ISEL=53
           NCOEFF=NCOEFF+1
           ICODEA(NCOEFF)=3
           IMULTI(NCOEFF)=1
           I=IREF(1,0)
           I1=IREF(2,0)
           NA(NCOEFF)=I
           N1A(NCOEFF)=I1
           DE=1.515179D0
           WN=1.0D0
           WN1=1.0D0
C       
           LXTBP(I,I1,1)=1
           XTBP(I,I1,1)=QHE(EBEAM,TIEV,ISEL,ZSEL,NSEL,1,EA,OA,N,
     &     IPASS,DSLPATH)
           COEFFA(NCOEFF)=XTBP(I,I1,1)
           LXTBP(I1,I,1)=1
           XTBP(I1,I,1)=XTBP(I,I1,1)*WN*DEXP(13.6048*DE/(TIEV+ECMH))/WN1
           COEFFB(NCOEFF)=XTBP(I1,I,1)
C
           ISEL=54
           NCOEFF=NCOEFF+1
           ICODEA(NCOEFF)=3
           IMULTI(NCOEFF)=1
           I=IREF(1,0)
           I1=IREF(3,0)
           NA(NCOEFF)=I
           N1A(NCOEFF)=I1
           DE=1.684560D0
           WN=1.0D0
           WN1=1.0D0
           LXTBP(I,I1,1)=1
           XTBP(I,I1,1)=QHE(EBEAM,TIEV,ISEL,ZSEL,NSEL,1,EA,OA,N,
     &     IPASS,DSLPATH)
           COEFFA(NCOEFF)=XTBP(I,I1,1)
           LXTBP(I1,I,1)=1
           XTBP(I1,I,1)=XTBP(I,I1,1)*WN*DEXP(13.6048*DE/(TIEV+ECMH))/WN1
           COEFFB(NCOEFF)=XTBP(I1,I,1)
C
           ISEL=55
           NCOEFF=NCOEFF+1
           ICODEA(NCOEFF)=3
           IMULTI(NCOEFF)=1
           I=IREF(1,0)
           I1=IREF(4,0)
           NA(NCOEFF)=I
           N1A(NCOEFF)=I1
           DE=1.739923D0
           WN=1.0D0
           WN1=1.0D0
           LXTBP(I,I1,1)=1
           XTBP(I,I1,1)=QHE(EBEAM,TIEV,ISEL,ZSEL,NSEL,1,EA,OA,N,
     &     IPASS,DSLPATH)
           COEFFA(NCOEFF)=XTBP(I,I1,1)
           LXTBP(I1,I,1)=1
           XTBP(I1,I,1)=XTBP(I,I1,1)*WN*DEXP(13.6048*DE/(TIEV+ECMH))/WN1
           COEFFB(NCOEFF)=XTBP(I1,I,1)
C
           ISEL=56
           NCOEFF=NCOEFF+1
           ICODEA(NCOEFF)=3
           IMULTI(NCOEFF)=1
           I=IREF(1,0)
           I1=IREF(2,1)
           NA(NCOEFF)=I
           N1A(NCOEFF)=I1
           DE=1.559444D0
           WN=1.0D0
           WN1=3.0D0
           LXTBP(I,I1,1)=1
           XTBP(I,I1,1)=QHE(EBEAM,TIEV,ISEL,ZSEL,NSEL,1,EA,OA,N,
     &     IPASS,DSLPATH)
           COEFFA(NCOEFF)=XTBP(I,I1,1)
           LXTBP(I1,I,1)=1
           XTBP(I1,I,1)=XTBP(I,I1,1)*WN*DEXP(13.6048*DE/(TIEV+ECMH))/WN1
           COEFFB(NCOEFF)=XTBP(I1,I,1)
C
           ISEL=57
           NCOEFF=NCOEFF+1
           ICODEA(NCOEFF)=3
           IMULTI(NCOEFF)=1
           I=IREF(1,0)
           I1=IREF(3,1)
           NA(NCOEFF)=I
           N1A(NCOEFF)=I1
           DE=1.696812D0
           WN=1.0D0
           WN1=3.0D0
           LXTBP(I,I1,1)=1
           XTBP(I,I1,1)=QHE(EBEAM,TIEV,ISEL,ZSEL,NSEL,1,EA,OA,N,
     &     IPASS,DSLPATH)
           COEFFA(NCOEFF)=XTBP(I,I1,1)
           LXTBP(I1,I,1)=1
           XTBP(I1,I,1)=XTBP(I,I1,1)*WN*DEXP(13.6048*DE/(TIEV+ECMH))/WN1
           COEFFB(NCOEFF)=XTBP(I1,I,1)
C
           ISEL=58
           NCOEFF=NCOEFF+1
           ICODEA(NCOEFF)=3
           IMULTI(NCOEFF)=1
           I=IREF(1,0)
           I1=IREF(4,1)
           NA(NCOEFF)=I
           N1A(NCOEFF)=I1
           DE=1.744958D0
           WN=1.0D0
           WN1=3.0D0
           LXTBP(I,I1,1)=1
           XTBP(I,I1,1)=QHE(EBEAM,TIEV,ISEL,ZSEL,NSEL,1,EA,OA,N,
     &     IPASS,DSLPATH)
           COEFFA(NCOEFF)=XTBP(I,I1,1)
           LXTBP(I1,I,1)=1
           XTBP(I1,I,1)=XTBP(I,I1,1)*WN*DEXP(13.6048*DE/(TIEV+ECMH))/WN1
           COEFFB(NCOEFF)=XTBP(I1,I,1)
C
           ISEL=59
           NCOEFF=NCOEFF+1
           ICODEA(NCOEFF)=3
           IMULTI(NCOEFF)=1
           I=IREF(1,0)
           I1=IREF(3,2)
           NA(NCOEFF)=I
           N1A(NCOEFF)=I1
           DE=1.695861D0
           WN=1.0D0
           WN1=5.0D0
           LXTBP(I,I1,1)=1
           XTBP(I,I1,1)=QHE(EBEAM,TIEV,ISEL,ZSEL,NSEL,1,EA,OA,N,
     &     IPASS,DSLPATH)
           COEFFA(NCOEFF)=XTBP(I,I1,1)
           LXTBP(I1,I,1)=1
           XTBP(I1,I,1)=XTBP(I,I1,1)*WN*DEXP(13.6048*DE/(TIEV+ECMH))/WN1
           COEFFB(NCOEFF)=XTBP(I1,I,1)
C
           ISEL=60
           NCOEFF=NCOEFF+1
           ICODEA(NCOEFF)=3
           IMULTI(NCOEFF)=1
           I=IREF(1,0)
           I1=IREF(4,2)
           NA(NCOEFF)=I
           N1A(NCOEFF)=I1
           DE=1.744536D0
           WN=1.0D0
           WN1=5.0D0
           LXTBP(I,I1,1)=1
           XTBP(I,I1,1)=QHE(EBEAM,TIEV,ISEL,ZSEL,NSEL,1,EA,OA,N,
     &     IPASS,DSLPATH)
           COEFFA(NCOEFF)=XTBP(I,I1,1)
           LXTBP(I1,I,1)=1
           XTBP(I1,I,1)=XTBP(I,I1,1)*WN*DEXP(13.6048*DE/(TIEV+ECMH))/WN1
           COEFFB(NCOEFF)=XTBP(I1,I,1)
C
       ENDIF
C
C-----------------------------------------------------------------------
C
C  DATA TYPE (4): H+ IMPACT IONISATION AND CHARGE EXCHANGE DATA
C                 IS OBTAINED AT THIS POINT BY ACCESSING THE
C                 DATA CONTAINED WITHIN AN ADF02 TYPE FILE.
C                 SELECT NUMBERS HAVE BEEN CONFIGURED FOR
C                 SIA#HE_J91#HE.DAT.
C
C               SINGLETS:  N=1
C         ATOMIC PROCESS:  SION   SCX   DION
C                (CODE) :   24     1     43
C
C               TRIPLETS: N=2 ( 2(3)S METASTABLE )
C         ATOMIC PROCESS:  SION   SCX
C                (CODE) :   27     4
C
C
C         NOMENCLATURE  :
C                       
C                         SION  : SINGLE ION IMPACT IONISATION
C                         SCX   : SINGLE CHARGE EXCHANGE
C                         DION  : DOUBLE IONISATION.
C
C             MODIFIED  :
C                         HARVEY ANDERSON
C                         UNIVERSITY OF STRATHCLYDE
C                         27/1/98
C
C
C
C-----------------------------------------------------------------------
C
       IF(ITYP4.EQ.1)THEN
C

           ISEL=24
           T1=QHE(EBEAM,TIEV,ISEL,ZIMP,NSEL,1,EA,OA,N,
     &     IPASS, DSLPATH)
C
           ISEL=1
           T2=QHE(EBEAM,TIEV,ISEL,ZIMP,NSEL,1,EA,OA,N,
     &     IPASS, DSLPATH)
C
           ISEL=43
           T3=QHE(EBEAM,TIEV,ISEL,ZIMP,NSEL,1,EA,OA,N,
     &     IPASS, DSLPATH) 
C
           NCOEFF=NCOEFF+1
           ICODEA(NCOEFF)=4
           IMULTI(NCOEFF)=1
           I=IREF(1,0)
           NA(NCOEFF)=I
           N1A(NCOEFF)=0
           LSTBP(I,1)=1
           STBP(I,1)=T1+T2+T3
           COEFFA(NCOEFF)=STBP(I,1)
C
           ISEL=27
           T1=QHE(EBEAM,TIEV,ISEL,ZIMP,NSEL,1,EA,OA,N,
     &     IPASS, DSLPATH)
C
           ISEL=4
           T2=QHE(EBEAM,TIEV,ISEL,ZIMP,NSEL,1,EA,OA,N,
     &     IPASS, DSLPATH)

           I=IREF(2,0)
           LSTBP(I,2)=1
           STBP(I,2) =T1+T1
        
C
       ENDIF
C
C-----------------------------------------------------------------------
C
C  DATA TYPE (5): ZIMP ION IMPACT EXCITATION IS OBTAINED AT THIS
C                 POINT BY ACCESSING THE DATA CONTAINED WITHIN
C                 AN ADF02 TYPE FILE.THE SELECT NUMBERS HAVE BEEN
C                 CONFIGURED FOR  SIA#HE_J91#HE.DAT.
C
C               SINGLETS:  N=1
C         ATOMIC PROCESS:  SION   SCX   DION
C                (CODE) :   *** no data ***
C
C         NOMENCLATURE  :
C                       
C                         SION  : SINGLE ION IMPACT IONISATION
C                         SCX   : SINGLE CHARGE EXCHANGE
C                         DION  : DOUBLE IONISATION.
C                 SELECT NUMBERS :
C
C-----------------------------------------------------------------------
C
C       INSERT CODE AT THIS POINT TO HANDLE THE STATISTICAL
C       WEIGHTS TO ALLOW DETAILED BALANCE CALCULATION
C       OF THE DE-EXCITATION RATE.
C
C       DO NOT HAVE ENOUGH QUALITY DATA YET TO INCLUDE
C       ZIMP ION IMPACT EXCITATION.
C
C       HARVEY ANDERSON
C       23/9/98
C
       IF(ITYP5.EQ.1.AND.NIMP.GT.0.0D0)THEN
          DO 51 IMP=1,NIMP
             IF(FRIMPA(IMP).EQ.0.0D0)GO TO 51
             ZIMP=ZIMPA(IMP)
             AMSSRZ=4.0D0*AMIMPA(IMP)/(AMIMPA(IMP)+4.0D0)
             ECMZ=AMSSRZ*EBEAM

             IF (DABS(ZIMP-2.0D0).LE.0.2D0)THEN
C                 IBSEL=????????
             ELSEIF(DABS(ZIMP-3.0D0).LE.0.2D0)THEN
C                 IBSEL=????????
             ELSEIF(DABS(ZIMP-4.0D0).LE.0.2D0)THEN
C                IBSEL=????????
             ELSEIF(DABS(ZIMP-5.0D0).LE.0.2D0)THEN
C                 IBSEL=????????
             ELSEIF(DABS(ZIMP-6.0D0).LE.0.2D0)THEN
C                 IBSEL=????????
             ELSEIF(DABS(ZIMP-7.0D0).LE.0.2D0)THEN
C                 IBSEL=????????
             ELSEIF(DABS(ZIMP-8.0D0).LE.0.2D0)THEN
C                 IBSEL=????????
             ELSEIF(DABS(ZIMP-9.0D0).LE.0.2D0)THEN
C                 IBSEL=????????
             ELSEIF(DABS(ZIMP-10.0D0).LE.0.2D0)THEN
C                 IBSEL=????????
             ELSE
c                 IBSEL=????????
             ENDIF
C             ISEL=IBSEL
C
C           VAL12=FRIMPA(IMP)*QHE(EBEAM,TIEV,ISEL,ZIMP,NSEL,1,EA,OA,N,
C     &      IPASS,TITLF,DSLPATH)
C           VAL21=VAL12*WN*DEXP(13.6048*DE/(TIEV+ECMZ))/WN1
C
C           XTBZ(1,2,1)=XTBZ(1,2,1)+VAL12
C            XTBZ(2,1,1)=XTBZ(2,1,1)+VAL21
  51     CONTINUE
       ENDIF
C
C-----------------------------------------------------------------------
C
C  DATA TYPE (6): ZIMP ION IMPACT IONISATION AND CHARGE EXCHANGE
C                 DATA IS OBTAINED AT THIS POINT BY ACCESSING
C                 THE DATA CONTAINED WITHIN AN ADF02 TYPE FILE.
C                 THE SELECT NUMBERS HAVE BEEN CONFIGURED FOR
C                 SIA#HE_J91#HE.DAT.
C
C               SINGLETS:  N=1
C         ATOMIC PROCESS:  SION   SCX   DION   DCX   CHARGE
C                       
C                (CODE) :   28      5     46    20      2.0
C                          N/A    N/A    N/A   N/A      3.0
C                           31      8     48    21      4.0
C                          N/A    N/A    N/A   N/A      5.0
C                           34     12     50    22      6.0
C                          N/A    N/A    N/A   N/A      7.0
C                           37     16     52    23      8.0
C                          N/A    N/A    N/A   N/A      9.0
C                          N/A    N/A    N/A   N/A     10.0
C
C               TRIPLETS:  N=2 ( 2(3)S METASTABLE LEVEL )
C         ATOMIC PROCESS:  SION   SCX    CHARGE
C                       
C                (CODE) :  30       7     2.0
C                          N/A    N/A     3.0
C                          33      10     4.0
C                          N/A    N/A     5.0
C                          36      14     6.0
C                          N/A    N/A     7.0
C                          39     18      8.0
C                          N/A    N/A     9.0
C                          N/A    N/A    10.0
C         NOMENCLATURE  :
C                       
C                         SION  : SINGLE ION IMPACT IONISATION
C                         SCX   : SINGLE CHARGE EXCHANGE
C                         DION  : DOUBLE IONISATION.
C                         DCX   : DOUBLE CHARGE EXCHANGE.
C                      CHARGE   : NUCLEAR CHARGE OF THE IMPURITY.
C                         N/A   : NO ACCESS TO FUNDAMENTAL DATA.
C
C
C             MODIFIED  :
C                         HARVEY ANDERSON
C                         UNIVERSITY OF STRATHCLYDE
C                         27/1/98
C
C
C-----------------------------------------------------------------------
C

       IF(ITYP6.EQ.1.AND.NIMP.GT.0 ) THEN
           NSEL=1
C
          DO 52 IMP=1,NIMP
           IF(FRIMPA(IMP).EQ.0.0D0)GO TO 52
               ZIMP=ZIMPA(IMP)

           IF    (DABS(ZIMP-2.0D0).LE.0.2D0)THEN
                  ISEL=28
               T1=QHE(EBEAM,TIEV,ISEL,ZIMP,NSEL,1,EA,OA,N,
     &            IPASS, DSLPATH)
                  ISEL=5
               T2=QHE(EBEAM,TIEV,ISEL,ZIMP,NSEL,1,EA,OA,N,
     &            IPASS, DSLPATH)
                  ISEL=46
               T3=QHE(EBEAM,TIEV,ISEL,ZIMP,NSEL,1,EA,OA,N,
     &            IPASS, DSLPATH)
                  ISEL=20
               T4=QHE(EBEAM,TIEV,ISEL,ZIMP,NSEL,1,EA,OA,N,
     &            IPASS, DSLPATH)
           ELSEIF(DABS(ZIMP-4.0D0).LE.0.2D0)THEN
                  ISEL=31
               T1=QHE(EBEAM,TIEV,ISEL,ZIMP,NSEL,1,EA,OA,N,
     &            IPASS, DSLPATH)
                  ISEL=8
               T2=QHE(EBEAM,TIEV,ISEL,ZIMP,NSEL,1,EA,OA,N,
     &            IPASS, DSLPATH)
                  ISEL=48
               T3=QHE(EBEAM,TIEV,ISEL,ZIMP,NSEL,1,EA,OA,N,
     &            IPASS, DSLPATH)
                  ISEL=21
               T4=QHE(EBEAM,TIEV,ISEL,ZIMP,NSEL,1,EA,OA,N,
     &            IPASS, DSLPATH)
           ELSEIF(DABS(ZIMP-6.0D0).LE.0.2D0)THEN
                  ISEL=34
               T1=QHE(EBEAM,TIEV,ISEL,ZIMP,NSEL,1,EA,OA,N,
     &            IPASS, DSLPATH)
                  ISEL=12
               T2=QHE(EBEAM,TIEV,ISEL,ZIMP,NSEL,1,EA,OA,N,
     &            IPASS, DSLPATH)
                  ISEL=50
               T3=QHE(EBEAM,TIEV,ISEL,ZIMP,NSEL,1,EA,OA,N,
     &            IPASS, DSLPATH)
                  ISEL=22
               T4=QHE(EBEAM,TIEV,ISEL,ZIMP,NSEL,1,EA,OA,N,
     &            IPASS, DSLPATH)
           ELSEIF(DABS(ZIMP-8.0D0).LE.0.2D0)THEN
                  ISEL=37
               T1=QHE(EBEAM,TIEV,ISEL,ZIMP,NSEL,1,EA,OA,N,
     &            IPASS, DSLPATH)
                  ISEL=16
               T2=QHE(EBEAM,TIEV,ISEL,ZIMP,NSEL,1,EA,OA,N,
     &            IPASS, DSLPATH)
                  ISEL=52
               T3=QHE(EBEAM,TIEV,ISEL,ZIMP,NSEL,1,EA,OA,N,
     &            IPASS, DSLPATH)
                  ISEL=23
               T4=QHE(EBEAM,TIEV,ISEL,ZIMP,NSEL,1,EA,OA,N,
     &            IPASS, DSLPATH)
           ELSE
C               IBSEL=34???????GENERAL SCALING
           ENDIF
C

           I=IREF(1,0)
           NA(NCOEFF)=I
           N1A(NCOEFF)=0
           LSTBZ(I,1)=1
           STBZ(I,1)=STBZ(I,1)+FRIMPA(IMP)*(T1+T2+T3+T4)
           COEFFA(NCOEFF)=STBZ(I,1)
   52     CONTINUE
C

          DO 53 IMP=1,NIMP
           IF(FRIMPA(IMP).EQ.0.0D0)GO TO 53
               ZIMP=ZIMPA(IMP)

           IF    (DABS(ZIMP-2.0D0).LE.0.2D0)THEN
                  ISEL=30
               T1=QHE(EBEAM,TIEV,ISEL,ZIMP,NSEL,1,EA,OA,N,
     &            IPASS, DSLPATH)
                  ISEL=7
               T2=QHE(EBEAM,TIEV,ISEL,ZIMP,NSEL,1,EA,OA,N,
     &            IPASS, DSLPATH)
           ELSEIF(DABS(ZIMP-4.0D0).LE.0.2D0)THEN
                  ISEL=33
               T1=QHE(EBEAM,TIEV,ISEL,ZIMP,NSEL,1,EA,OA,N,
     &            IPASS, DSLPATH)
                  ISEL=10
               T2=QHE(EBEAM,TIEV,ISEL,ZIMP,NSEL,1,EA,OA,N,
     &            IPASS, DSLPATH)
           ELSEIF(DABS(ZIMP-6.0D0).LE.0.2D0)THEN
                  ISEL=36
               T1=QHE(EBEAM,TIEV,ISEL,ZIMP,NSEL,1,EA,OA,N,
     &            IPASS, DSLPATH)
                  ISEL=14
               T2=QHE(EBEAM,TIEV,ISEL,ZIMP,NSEL,1,EA,OA,N,
     &            IPASS, DSLPATH)
           ELSEIF(DABS(ZIMP-8.0D0).LE.0.2D0)THEN
                  ISEL=39
               T1=QHE(EBEAM,TIEV,ISEL,ZIMP,NSEL,1,EA,OA,N,
     &            IPASS, DSLPATH)
                  ISEL=18
               T2=QHE(EBEAM,TIEV,ISEL,ZIMP,NSEL,1,EA,OA,N,
     &            IPASS, DSLPATH)
           ELSE
C               IBSEL=34???????GENERAL SCALING
           ENDIF
           I=IREF(2,0)
           LSTBZ(I,2)=1
           STBZ(I,2)=STBZ(I,2)+FRIMPA(IMP)*(T1+T2)
           COEFFA(NCOEFF)=STBZ(I,2)
   53     CONTINUE
C
       ENDIF
C
C-----------------------------------------------------------------------
C
       RETURN
 1000  FORMAT(1A80)
 1001  FORMAT(16X,1P,14D8.2)
 1002  FORMAT(1H0,1A80)
 1003  FORMAT(1H0,'   IND ICODE  I   I1  2S+1   COEFFA(CM**3 SEC-1)
     & COEFFB(CM**3 SEC-1)')
 1004  FORMAT(1H ,5I5,5X,1P,D12.4,13X,1P,D12.4)
 1005  FORMAT(1A1,I3,I4,1P,15D8.2)
 1006  FORMAT(10X,I1,14X,I1,1X,I1,1X,F4.1,2X,F15.0)
 1007  FORMAT(30X,F15.0)
 1008 FORMAT(1A3,I2,2I10,A55)
      END
