CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas311/spij.for,v 1.2 2004/07/06 15:22:07 whitefor Exp $ Date $Date: 2004/07/06 15:22:07 $
CX
      SUBROUTINE SPIJ(N,H,I,J,CIJ)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION H(10),CIJ(3)
      CIJ(1) = 0.0D0
      CIJ(2) = 0.0D0
      EN = N
      J4 = -2*J + 1
      J3 = J4 + 2
      S = -1.0D0
      K = I + J
      IF(2*(K/2)-K)2,1,2
1     S = 1.0D0
2     IF(J-1)3,3,4
3     T1 = 0.0D0
      GO TO 5
4     T1 = S/H(J-1)
5     IF(N-J)6,7,7
6     T2 = 0.0D0
      GO TO 8
7     T2 = S/H(J)
8     IF(I-J+1)9,10,11
9     J3 = N + N + J3
      J4 = N + N + J4
      GO TO 13
10    J3 = N + J3
      J4 = N + N + J4
      CIJ(1) = 5.0D-1
      CIJ(2) = 1.0D0/H(I)
      GO TO 13
11    IF(I-J)12,12,13
12    J4 = N + J4
      CIJ(1) = 5.0D-1
      CIJ(2) = -1.0D0/H(I)
13    T3 = J3
      T4 = J4
      T = (T3*T1 + T4*T2)/EN
      CIJ(1) = CIJ(1) - H(I)*T/4.0D0
      CIJ(3) = T/H(I)
      RETURN
      END
