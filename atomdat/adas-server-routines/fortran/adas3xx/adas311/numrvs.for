CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas311/numrvs.for,v 1.2 2004/07/06 14:25:07 whitefor Exp $ Date $Date: 2004/07/06 14:25:07 $
CX
       SUBROUTINE NUMRVS(Z0,Z1,ALF,E,ELL,I1,I2)
       IMPLICIT REAL*8(A-H,O-Z)
       DIMENSION E(2),ELL(2),S(2),T(2),C(2),Y(2)
       COMMON /NUM/T,C,S,Y,X,H,HH,H1
       XR2=1.0/(X*X)
       V=VPOT(Z0,Z1,ALF,X)
       DO 4 I=I1,I2
       S(I)=S(I)+T(I)*Y(I)
       T(I)=HH*(-E(I)+V-ELL(I)*XR2)
       C(I)=C(I)-S(I)
    4  Y(I)=C(I)/(1.0+H1*T(I))
       RETURN
      END
