CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas311/qbip.for,v 1.2 2004/07/06 14:38:35 whitefor Exp $ Date $Date: 2004/07/06 14:38:35 $
CX
       FUNCTION QBIP(Z1,E1,ZP,ATMSSP)
       IMPLICIT REAL*8(A-H,O-Z)
C-----------------------------------------------------------------------
C  BURGESS IMPACT PARAMETER ION IMPACT EXCITATION CROSS-SECTIONS
C
C  EXCITATION CROSS-SECTION IS EVALUATED AND DE-EXCITATION CROSS-SECTION
C  OBTAINED BY DETAILED BALANCE
C
C
C
C  INPUT
C      Z1=TARGET ION CHARGE +1
C      E1=ENERGY OF EQUIVALENT ELECTRON IN RYDBERGS
C         (CORRESPONDS TO ACTUAL PROJECTILE ENERGY/25KEV)
C      ZP=PROJECTILE CHARGE
C      ATMSSP= PROJECTILE MASS IN PROTON UNITS
C  OUTPUT
C      QBIP=CROSS-SECTION IN PI*A0**2 UNITS
C
C
C  ***********  H.P.SUMMERS, JET             6/ 8/91 ***************
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C********   TEMPORARY FUNCTION AT THIS STAGE  *************
C-----------------------------------------------------------------------
       QBIP=1.0D-40
C-----------------------------------------------------------------------
       RETURN
      END
