CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas311/sum.for,v 1.1 2004/07/06 15:23:43 whitefor Exp $ Date $Date: 2004/07/06 15:23:43 $
CX
      PROGRAM SUM
C
      INTEGER IMAXT  , IMAX , IMAXS
      REAL*8 ARED(500,500) , DARED(500) , SARED(500)
C
      READ(3,*) IMAX , IMAXS      
      IMAXT = IMAX + IMAXS 
      WRITE(6,*) IMAXT   
      DO I=1,IMAXT
         READ(3,*) ( ARED(I,J),J=1,IMAXT )
         WRITE(4,'(500(1P,1E22.13))') ( ARED(I,J),J=1,IMAXT )
      ENDDO
      DO J=1,IMAXT
         SUM = 0.0
           DO I=1, IMAXT
             SUM = SUM+ARED(I,J)
           ENDDO
         SARED(J)=SUM
      ENDDO
      WRITE(4,*)
      WRITE(4,'(500(1P,1E22.13))') ( SARED(J),J=1,IMAXT )
      WRITE(4,*)
	      
      STOP
      END
