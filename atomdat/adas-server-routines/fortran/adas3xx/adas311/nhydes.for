CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas311/nhydes.for,v 1.3 2007/05/17 17:04:12 allan Exp $ Date $Date: 2007/05/17 17:04:12 $
CX
       SUBROUTINE NHYDES(Z0,ZEFF,N,L,E0)
       IMPLICIT REAL*8(A-H,O-Z)
C-----------------------------------------------------------------------
C
C  PURPOSE: CALULATES LOWEST ORDER NON-RELATIVISTIC, RELATIVISTIC AND
C           QUANTUM-ELECTRODYNAMIC ENERGIES FOR HYDROGENIC IONS
C
C  BINDING ENERGY FOR CENTRE OF TERM IS PRODUCED.  FINE STRUCTURE FOR
C  L>0 MUST BE ADDED EXTERNALLY. FORMULAE ARE FROM ERIKSON (1977) J.PHY
C  CHEM. REF.DATA,6,831.
C  QED EFFECTS FOR L>0 OMITTED.
C-----------------------------------------------------------------------
C VERSION  : 1.1
C DATE     : 18-03-1999
C MODIFIED : ???
C
C VERSION  : 1.2
C DATE     : 05-10-2000
C MODIFIED : ???
C             - Removed junk from columns > 72
C
C VERSION  : 1.3
C DATE     : 16-05-2007
C MODIFIED : Allan Whiteford
C             - Updated comments as part of subroutine documentation
C               procedure.
C
C-----------------------------------------------------------------------
      DIMENSION BLNA(4)
       DATA BLNA/-2.984128D0,2.811768D0,-2.767699D0,-2.749859D0/
       XN=N
       XL=L
       E0=(Z0/XN)**2
       IF(L-1)10,20,20
   10  RMC=5.32504D-5*(ZEFF/XN)**2*(XN/(XL+1.0D0)-0.75D0)
       BLN=-2.71631D0-2.68551D-1/XN**1.5D0
       IF(N.LT.5)BLN=BLNA(N)
       QED=3.2984D-7*ZEFF*(ZEFF/XN)**3*(DLOG(1.8779D4/(ZEFF*ZEFF))
     &+6.333D-1+BLN)
       E0=E0*(1.0D0+(ZEFF/Z0)**2*RMC)-QED
       RETURN
   20  RMC=5.32504D-5*(ZEFF/XN)**2*(XN/(XL+0.5D0)-0.75D0)
       E0=E0*(1.0D0+(ZEFF/Z0)**2*RMC)
       RETURN
      END
