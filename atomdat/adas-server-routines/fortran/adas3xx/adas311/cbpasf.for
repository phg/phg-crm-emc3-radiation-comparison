CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas311/cbpasf.for,v 1.2 2004/07/06 12:00:03 whitefor Exp $ Date $Date: 2004/07/06 12:00:03 $
CX
      SUBROUTINE CBPASF( IUPS1  , IUPS2  , IURUN , DSNPS1 , DSNPS2 ,
     &                   DSNRUN , LPASS1 , LPASS2 , LRUN  , IRCODE  )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: CBPASF *********************
C
C  PURPOSE: HANDLES OPENING OF OUTPUT PASSING FILES.
C
C  CALLING PROGRAM: ADAS311
C
C  SUBROUTINE:
C
C  INPUT : (I*4)   IUPS1    = UNIT NO. FOR FIRST PASSING FILE.
C  INPUT : (I*4)   IUPS2    = UNIT NO. FOR SECOND PASSING FILE.
C  INPUT : (I*4)   IURUN    = UNIT NO. FOR RUN SUMMARY FILE.
C  INPUT : (C*80)  DSNPS1   = FIRST PASSING FILE DATA SET NAME.
C  INPUT : (C*80)  DSNPS2   = SECOND PASSING FILE DATA SET NAME.
C  INPUT : (C*80)  DSNRUN   = RUN SUMMARY FILE DATA SET NAME.
C  INPUT : (L*4)   LPASS1   = FLAG FOR WHETHER OR NOT 1ST PASSING FILE
C                             HAS BEEN REQUESTED
C  INPUT : (L*4)   LPASS2   = FLAG FOR WHETHER OR NOT 2ND PASSING FILE
C                             HAS BEEN REQUESTED
C
C  OUTPUT: (I*4)   IRCODE   = RETURN CODE AFTER ATTEMPTING TO OPEN FILES.
C
C
C  CONTACT : HARVEY ANDERSON
C	     UNIVERSITY OF STRATHCLYDE
C	     ANDERSON@PHYS.STRATH.AC.UK
C
C  NOTE   : ROUTINE BASED ON THE STRUCTURE OF CAPASF.F, WHICH IS
C           USED BY ADAS310.
C
C  DATE   :  02/15/94
C
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER     IUPS1   , IUPS2   ,  IURUN
      INTEGER     IRCODE
C-----------------------------------------------------------------------
      CHARACTER   DSNPS1*80  , DSNPS2*80  , DSNRUN*80
C-----------------------------------------------------------------------
      LOGICAL    LPASS1  , LPASS2  ,   LRUN
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C OPEN PASSING FILES.
C-----------------------------------------------------------------------
C
      IRCODE = 0
      IF (LPASS1) THEN
          OPEN( UNIT=IUPS1 , FILE=DSNPS1 , STATUS='UNKNOWN' , ERR=9999)
      ELSE
          OPEN( UNIT=IUPS1 ,               STATUS='SCRATCH' , ERR=9999)
      ENDIF
      IF (LPASS2) THEN
          OPEN( UNIT=IUPS2 , FILE=DSNPS2 , STATUS='UNKNOWN' , ERR=9999)
      ELSE
          OPEN( UNIT=IUPS2 ,               STATUS='SCRATCH' , ERR=9999)
      ENDIF
      IF (LRUN) THEN
          OPEN( UNIT=IURUN , FILE=DSNRUN , STATUS='UNKNOWN' , ERR=9999)
      ELSE
          OPEN( UNIT=IURUN ,               STATUS='SCRATCH' , ERR=9999)
      ENDIF
      RETURN
C
C-----------------------------------------------------------------------
C PROGRAM RETURNS FROM THIS POINT IF THERE WAS AN ERROR
C-----------------------------------------------------------------------
C

9999  IRCODE = 1
      RETURN

      END
