CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas311/rad2.for,v 1.2 2004/07/06 14:43:12 whitefor Exp $ Date $Date: 2004/07/06 14:43:12 $
CX
       SUBROUTINE RAD2(E,N,L,D,NS,ANS)
       IMPLICIT REAL*8(A-H,O-Z)
       DIMENSION A(20),E(2),N(2),L(2),D(2),ELL(2),X1(2),X2(2),X3(2)
       DIMENSION Y(2),Y1(2),G(5,2),W(5),AN(2),T(2),C(2),S(2),GE(2)
       DIMENSION FS(100,2)
       COMMON /ATSP/Z0,Z1,ALF,LP,ISP,AMSZ0,AMSHYD
       COMMON /NUM/T,C,S,Y,X,H,HH,H1
       COMMON /ACCSET/RX3,DPT,GE,EHCT,MN,NHCT,LHCT,NIP,NEX
       NS4=NS/2
       ZZ=Z1*Z1
       DO 6 I=1,2
       ELL(I)=L(I)*(L(I)+1)
       TS=ZZ-E(I)*ELL(I)
       XQ=0.0
       IF(TS)1,1,2
    1  XP=ELL(I)/E(I)
       XP=DSQRT(XP)
       XQ=XP
       GO TO 18
    2  TS=DSQRT(TS)
       XQ=(Z1+TS)/E(I)
       XP=(Z1-TS)/E(I)
       IF(XP)5,5,18
   18  Z2=0.5*XP*VPOT(Z0,Z1,ALF,XP)
       TS=Z2*Z2-E(I)*ELL(I)
       IF(TS)4,4,3
    3  X=XP
       XP=(Z2-DSQRT(TS))/E(I)
       IF(DABS(X-XP)-0.1*X)5,5,18
    4  XP=Z2/E(I)
       XQ=DMAX1(XP,XQ)
    5  EN=N(I)
       X2(I)=XQ
       X=RX3*EN/Z1
       IF(E(1)*E(2).LE.0.0)XQ=XQ+X
       X3(I)=XQ+X
    6  X1(I)=XP
       IF(X1(1)-X1(2))50,50,51
   50  JL=1
       JU=2
       GO TO 52
   51  JL=2
       JU=1
   52  IF(X3(1)-X3(2))53,53,54
   53  IL=1
       IU=2
       GO TO 55
   54  IL=2
       IU=1
   55  NS4=NS4+NS4
       H=NS4
       H=X3(IU)/H
       NS1=X1(JL)/H
       IF(X1(JL).LE.0.0)GO TO 46
       IF(NS1.LE.MN)GO TO 55
       GO TO 56
   46  NS1=NS1+1
   56  IF(X1(JU).LE.0.0)GO TO 47
       NS2=X1(JU)/H
       IF(NS2.LE.MN)GO TO 55
       GO TO 59
   47  NS2=NS2+1
   59  X=NS2
       X1(JU)=H*X
       X=NS1
       X1(JL)=H*X
   60  X=E(IL)
       IF(X.GT.0.0)GO TO 57
       NS3=NS4
       GO TO 58
   57  X=170.0/DSQRT(X)
       XP=X3(IU)
       NS3=DMIN1(X,XP)/H
       NS3=((NS3+3)/4)*4
   58  X=NS3
       X3(IL)=X*H
       HH=H*H
       H1=0.083333333
       H2=0.044444444*H
       X=H
       XR2=1.0/(X*X)
       V=VPOT(Z0,Z1,ALF,X)
       DO 15 I=1,2
       Y(I)=1.0
       T(I)=HH*(-E(I)+V-ELL(I)*XR2)
       C(I)=(1.0+H1*T(I))*Y(I)
       S(I)=-C(I)
       IF(L(I)-1)12,13,14
   12  S(I)=S(I)+2.0*H1*H*Z0
       GO TO 14
   13  S(I)=S(I)-H1*ELL(I)
   14  FS(1,I)=0.0
   15  FS(2,I)=Y(I)
       IF(NS1.LE.0)GO TO 21
       DO 20 NC=1,NS1
       X=X+H
       CALL NUMRVS(Z0,Z1,ALF,E,ELL,1,2)
       FS(NC+2,1)=Y(1)
   20  FS(NC+2,2)=Y(2)
   21  NL=NS1+1
       IF(NS2.LE.0)GO TO 26
       DO 25 NC=NL,NS2
       X=X+H
       CALL NUMRVS(Z0,Z1,ALF,E,ELL,JU,JU)
   25  FS(NC+2,JU)=Y(JU)
   26  ANS=0.0
       NK=NS2+2
       DO 10 I=1,2
       AN(I)=0.0
       X=X3(I)
       XR2=1.0/(X*X)
       XH=X+H
       XHR2=1.0/(XH*XH)
       EP=-E(I)
       ELLP=ELL(I)
       LPP=L(I)
       IF(EP)9,8,8
    8  P=PHASEC(EP,ELLP,LPP,-Z1,XH)+D(I)
       CALL DNAMP(A0,A,EP,ELLP,-Z1,XH,10,5)
       Y1(I)=A0*DSIN(P)
       P=PHASEC(EP,ELLP,LPP,-Z1,X)+D(I)
       CALL DNAMP(A0,A,EP,ELLP,-Z1,X,10,5)
       Y(I)=A0*DSIN(P)
       GO TO 63
    9  CALL BDCF32(F,EP,ELLP,-Z1,XH)
       Y1(I)=F
       CALL BDCF32(F,EP,ELLP,-Z1,X)
       Y(I)=F
   63  V=VPOT(Z0,Z1,ALF,X)
       T(I)=HH*(EP+V-ELLP*XR2)
       C(I)=(1.0+H1*T(I))*Y(I)
       S(I)=-C(I)+(1.0+H1*HH*(EP+VPOT(Z0,Z1,ALF,XH)-ELLP*XHR2))*Y1(I)
   10  G(1,I)=Y(I)*Y(I)
       X=X3(IU)
       H=-H
       K=1
       IS=1
       DO 28 NC=1,NS4
       N1=NS4-NC+1
       X=X+H
       K=K+1
       GO TO (30,32,35,37),IS
   30  IF(N1.LE.NS3)GO TO 31
       CALL NUMRVS(Z0,Z1,ALF,E,ELL,IU,IU)
       G(K,IU)=Y(IU)*Y(IU)
       GO TO 38
   31  IS=2
       W(1)=Y(1)*Y(2)*(X+H)
   32  IF(N1-NS2-1)34,48,49
   48  F11=Y(JU)/FS(N1+1,JU)
       F22=Y(JL)/FS(NS1+2,JL)
   49  CALL NUMRVS(Z0,Z1,ALF,E,ELL,1,2)
       G(K,1)=Y(1)*Y(1)
       G(K,2)=Y(2)*Y(2)
       W(K)=Y(1)*Y(2)*X
       GO TO 38
   34  IS=3
       F1=Y(JU)/FS(N1+1,JU)
   35  IF(N1-NS1-1)36,61,62
   61  F22=Y(JL)/FS(N1+1,JL)
   62  CALL NUMRVS(Z0,Z1,ALF,E,ELL,JL,JL)
       G(K,JL)=Y(JL)*Y(JL)
       XP=FS(N1,JU)*F1
       G(K,JU)=XP*XP
       W(K)=XP*Y(JL)*X
       GO TO 38
   36  IS=4
       F2=Y(JL)/FS(N1+1,JL)
   37  XQ=FS(N1,JL)*F2
       XP=FS(N1,JU)*F1
       G(K,JL)=XQ*XQ
       G(K,JU)=XP*XP
       W(K)=XQ*XP*X
   38  IF(K.LT.5)GO TO 28
       GO TO (40,39,39,39),IS
   39  AN(IL)=AN(IL)+H2*(7.0*(G(1,IL)+G(5,IL))+32.0*(G(2,IL)+G(4,IL))+12
     1.0*G(3,IL))
       G(1,IL)=G(5,IL)
       ANS=ANS+H2*(7.0*(W(1)+W(5))+32.0*(W(2)+W(4))+12.0*W(3))
       W(1)=W(5)
   40  AN(IU)=AN(IU)+H2*(7.0*(G(1,IU)+G(5,IU))+32.0*(G(2,IU)+G(4,IU))+12
     1.0*G(3,IU))
       G(1,IU)=G(5,IU)
       K=1
   28  CONTINUE
       GE(JU)=(F11-F1)/H
       GE(JL)=(F22-F2)/H
       ANS=ANS*ANS
       DO 45 I=1,2
       IF(E(I))45,45,42
   42  ANS=ANS/AN(I)
   45  CONTINUE
       RETURN
      END
