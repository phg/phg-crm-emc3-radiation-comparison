CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas311/gbfr.for,v 1.2 2004/07/06 13:58:21 whitefor Exp $ Date $Date: 2004/07/06 13:58:21 $
CX
       FUNCTION GBFR(N1,L1,LT1,E1,V1,L2,E2,J,C1,C2,C3)
       IMPLICIT REAL*8(A-H,O-Z)
       DIMENSION E(2),L(2),N(2),D(2),GE(2)
       DIMENSION C1(5,5,3),C2(5,5,3),C3(5,5,3)
       COMMON /ATSP/Z0,Z1,ALF,LP,ISP,AMSZ0,AMSHYD
C  CALCULATES G(N1,L1;K2,L2) BASED ON ENERGIES OF LEVEL N1,L1,LT1
C  AND CONTINUUM NON-COULOMB PHASE FROM AVERAGE OVER ALLOWED LT2.
C  NOTE THAT E2 IS TAKEN AS NEGATIVE.
       COMMON /ACCSET/RX3,DPT,GE,EHCT,MN,NHCT,LHCT,NIP,NEX
       XLP=LP
       ZZ=Z1*Z1
       GBFR=0.0
       NS4=DPT
       NS4=NS4*N1*N1
       D(1)=0.0
       D(2)=0.0
       E(1)=E1*ZZ
       N(1)=N1
       L(1)=L1
       E(2)=E2*ZZ
       ES=DSQRT(-E(2))
       NS5=ES*DPT
       NS5=NS5*N1*N1
       NS4=MAX0(NS4,NS5)
       N(2)=0
       L(2)=L2
       LM=MIN0(L1,L2)
       W2=AMAX0(L1,L2)
       W=0.10825318*W2*(E1-E2)**4*V1**3
       IF(L2-L1+1)20,1,2
    1  I=1
       GO TO 4
    2  IF(L2-L1-1)20,7,20
    7  I=3
    4  IF(LHCT.LE.LM)GO TO 8
       IF(EHCT.LE.E2)GO TO 8
       IF(NHCT.LE.N1)GO TO 8
       LL1=L1+1
       LLT1=LT1+1-IABS(LP-L1)
       D(2)=3.141592654*(C1(LL1,LLT1,I)+E2*(C2(LL1,LLT1,I)+E2*
     1C3(LL1,LLT1,I)))
    6  CALL RAD2(E,N,L,D,NS4,ANS)
       GBFR=W*ANS*ZZ*ZZ
   20  RETURN
    8  GBFR=W*RD2FS(N1,L1,L2,E2)
       GO TO 20
      END
