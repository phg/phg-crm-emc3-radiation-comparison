       SUBROUTINE CCNST7(NLREP,IR,I,V,E,EXE,EXS,K,ISG,CGBBA,ENL,ENL2,
     &                   EXPTE,EXPTS,KPF,ISG1,NMIN,NMAX,IMAX,
     &                   A0,CA0,A,CA,RH,CRH,
     &                   IOPT,NG,LG,NGL,LTG,FPG,NG1,LG1,
     &                   NGL1,LTG1,FPG1,ARL,ISARL,AC1,RHSC1,
     &                   C1,C2,C3,SCLA,NIMP,ZIMPA,FRIMPA,AMIMPA,
     &                   EXMTE )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C       **********  FORTRAN 77 ROUTINE : CCNST7 **********
C
C       PURPOSE : ASSEMBLES ARRAYS USED TO CONSTRUCT THE
C                 COLLISIONAL-RADIATIVE MATRIX.
C
C       HISTORY : ROUTINE WAS ORIGINALLY WRITTEN BY H.P.SUMMERS.
C                 RESTRUCURED AND MODIFIED BY H.ANDERSON.
C
C       INPUT   :
C
C       (I*4) NLREP()   : ARRAY CONTAINING ALL OF THE REPRESENTATIVE
C                         LEVELS.
C       (I*4) IR        : COUNTER GIVEN AS INPUT WHICH RANGES FROM
C                         NMIN TO NMAX AND IS USED TO REFERENCE THE
C                         REPRESENTATIVE LEVEL.
C       (I*4) I         : REPRESENTATIVE LEVEL WHICH IS GIVEN BY
C                         NLREP(IR)
C       (R*8) V         : EFFECTIVE PRINCIPAL QUANTUM NUMBER FOR THE
C                         REPRESENTATIVE LEVEL I.
C       (R*8) E         : RECIPROCAL OF THE EFFECTIVE PRINCIPAL QUANTUM
C                         NUMBER SQUARED FOR THE REPRESENTATIVE LEVEL I.
C       (R*8) EXE       : VARIABLE ASSIGNED THE VALUE OF EXP(I/k*Te)
C                         ASSOCIATED WITH THE REPRESENTATIVE LEVEL I.
C       (R*8) EXS       : VARIABLE ASSIGNED THE VALUE OF EXP(I/k*TS)
C                         ASSOCIATED WITH THE REPRESENTATIVE LEVEL I.
C       (I*4) K         : VARIABLE ASSIGNED THE QUANTUM NUMBERS FOR
C                         THE REPRESENTATIVE LEVEL I. STORAGE OF THE
C                         NUMBERS ARE OF THE SAME FORMAT AS KPF().
C       (I*4) ISG       : MULTIPLICITY ?.
C
C       (R*8) CGBBA     : **** UNKNOWN ****
C       (R*8) ENL()     : ARRAY CONTAINING THE EFFECTIVE PRINCIPAL
C                         QUANTUM NUMBERS FOR EACH REPRESENTATIVE
C                         LEVEL.
C       (R*8) ENL2()    : ARRAY CONTAINING THE RECIPROCAL OF THE
C                         EFFECTIVE PRINCIPAL QUANTUM NUMBER
C                         SQUARED FOR EACH REPRESENTATIVE LEVEL.
C       (R*8) EXPTE()   : ARRAY CONTAINING THE VALUE OF EXP(I/k*Te)
C                         FOR EACH REPRESENTATIVE LEVEL FOR THE
C                         TRIPLETS.
C
C       (R*8) EXMTE()   : ARRAY CONTAINING THE VALUE OF EXP(I/k*Te)
C                         FOR EACH REPRESENTATIVE LEVEL FOR THE
C                         SINGLETS.
C       (R*8) EXPTS()   : ARRAY CONTAINING THE VALUE OF EXP(I/k*TS)
C                         FOR EACH REPRESENTATIVE LEVEL.
C       (I*4) KPF()     : ARRAY CONTAINING THE QUANTUM NUMBERS,
C                         N,l,L FROM NMIN TO NMAX, IN ORDER
C                         OF DECREASING BINDING ENERGY. EACH 32 BIT
C                         ELEMENT OF THE ARRAY IS USED TO STORE N,l
C                         AND L FOR EACH REPRESENTATIVE LEVEL. THE
C                         FIRST 10 BITS ARE USED TO STORE THE TOTAL
C                         ANGULAR MOMENTUM QUANTUM NUMBER. THE NEXT
C                         10 BITS ARE USED TO STORE THE ORBITAL
C                         QUANTUM NUMBER. THE LAST 12 BITS ARE USED
C                         TO STORE THE PRINCIPAL QUANTUM NUMBER.
C
C                         |<---------4 BYTE INTEGER--------->|
C                         |<---------32 BIT INTEGER--------->|
C                         |<----N----->|<----l--->|<---L---->|
C
C                       [ |.....N......|.....l....|.....L....|  ]
C
C                         BIT OPERATORS ARE THEN EMPLOYED TO
C                         INTEROGATE ARRAYS,E.G IAND,ISHFR,
C                         USING HEXIDECIMAL MASKS.
C
C       (I*4) ISG1      : *****UNKNOWN*****
C       (I*4) NMIN      : MINIMUM PRINCIPAL QUANTUM NUMBER OF THE
C                         RANGE WHICH CONTAINS THE REPRESENTATIVE
C                         LEVELS.
C
C       (I*4) NMAX      : MAXIMUM PRINCIPAL QUANTUM NUMBER OF THE
C                         RANGE WHICH CONTAINS THE REPRESENTATIVE
C                         LEVELS.
C       (I*4) IMAX      : THE MAXIMUM NUMBER OF REPRESENTATIVE
C                         LEVELS.
C
C       OUTPUT :
C
C       (R*8) A0        :
C       (R*8) CA0       :
C       (R*8) A         :
C       (R*8) CA        :
C       (R*8) RH        :
C       (R*8) CRH       :
C       (I*4) IOPT      : SWITCH USED TO DETERMINE IF CCNST7
C                         SHOULD ASSEMBLE ARRAYS USED TO
C                         CONSTRUCT THE COLLISIONAL-RADIATIVE
C                         MATRIX EXCLUDING SPIN CHANGING
C                         CROSS SECTIONS ( IOPT = 1 ) OR IF
C                         CCNST7 SHOULD ONLY CONTRUCT THE
C                         ARRAYS CONTAINING SPIN CHANGING
C                         CROSS SECTIONS ( IOPT GT 1 ).
C       (I*4) NG        :
C       (I*4) LG        :
C       (I*4) NGL       :
C       (I*4) LTG       :
C       (R*8) FPG       :
C       (I*4) NG1       :
C       (I*4) LG1       :
C       (I*4) NGL1      :
C       (I*4) iLTG1     :
C       (R*8) FPG1      :
C       (R*8) ARL       :
C       (I*4) ISARL     :
C       (R*8) AC1       :
C       (R*8) RHSC1     :
C       (R*8) C1()      : COEFFICIENTS OF THE QUANTUM DEFECT
C                         EXPANSION.
C       (R*8) C2()      : COEFFICIENTS OF THE QUANTUM DEFECT
C                         EXPANSION.
C       (R*8) C3()      : COEFFICIENTS OF THE QUANTUM DEFECT
C                         EXPANSION.
C       (R*8) SCLA      :
C       (I*4) NIMP      : NUMBER OF IMPURITIES IN THE PLASMA.
C       (R*8) ZIMPA     : ARRAY CONTAINING THE NUCLEAR CHARGE
C                         OF THE IMPURITIES IN THE PLASMA.
C       (R*8) FRIMPA    : IMPURITY FRACTIONS.
C       (R**) AMIMPA    : THE ATOMIC MASS OF EACH IMPURITY
C                                 WITHIN THE PLASMA.
C
C
C
C       ROUTINES:
C
C           ROUTINE        SOURCE         DESCRIPTION
C        -----------------------------------------------
C           GBBR            ADAS
C           GBB             ADAS
C           PYVR            ADAS
C           PYPR            ADAS
C           PYIPHE          ADAS
C           RQVNEW          ADAS
C           RQLNEW          ADAS
C           RQBNEW          ADAS
C           GHNLV           ADAS
C           GHNLE           ADAS
C           COLINT          ADAS
C           RQINEW          ADAS
C           PHOTO2          ADAS
C           NDIEL           ADAS
C           RNDEGV          ADAS
C           WIG6J           ADAS
C           OVLP            ADAS
C           COLEXC          ADAS
C           .....
C           .....
C           .....
C
C
C       CONTACT : HARVEY ANDERSON
C                 UNIVERSITY OF STRATHCLYDE
C                 ANDERSON@BARWANI.PHYS.STRATH.AC.UK
C
C       DATE    : 2/2/98
C
C
C
C       WARNING!!! : CODE IS UNDER DEVELOPMENT
C
C                    HARVEY ANDERSON
C                    UNIVERSITY OF STRATHCLYDE
C
C VERSION:      1.2                                     DATE:   21-10-99
C MODIFIED: RICHARD MARTIN
C               CHANGED HEXADECIMAL CONSTANTS TO Z'FFF00000' FORM.
C               CORRECTED ARRAY INDEXING PROBLEM
C
C VERSION : 1.3
C MODIFIED: Martin O'Mullane
C DATE    : 3-6-2000
C           iLTG1 is a scalar here but is passed from start7
C           as an integer array. It should be LTG1 not iLTG1.
C           COR should be 20 not 6 - seel NDIEL subroutine.
C           CORS should be an array not a scalar.
C
C VERSION : 1.4
C DATE    : 18-11-2004
C MODIFIED: Martin O'Mullane
C            - Align with Harvey Anderson's last version.
C            - Make implicit none and remove unecessary code.
C
C VERSION : 1.5
C DATE    : 24-02-2005
C MODIFIED: Martin O'Mullane
C            - Make implicit none and remove unecessary code.
C
C-----------------------------------------------------------------------
       integer   NCDIM ,NDLOW,NLDIM
C-----------------------------------------------------------------------
       PARAMETER (NCDIM=50,NDLOW=10,NLDIM=19)
C-----------------------------------------------------------------------
       integer     I ,           I1 ,          I11 ,           I2 ,
     &            ID ,        IDIEL ,        IGHNL ,           II ,
     &           II0 ,          II1 ,          IIS ,         IISP ,
     &         IIWCH ,           IL ,          ILA ,         ILOW ,
     &         ILPRS ,         IMAX ,          IMP ,          IND ,
     &         INREP ,         INTD ,        IONIP ,         IOPT ,
     &          IPRS ,         IPRT ,           IR ,         IREF ,
     &        IRNDGV ,           IS ,          IS1 ,          ISA ,
     &           ISG ,         ISG1 ,        ISHFR ,          ISP ,
     &          ISUP ,        ISUP1 ,       ISUP11 ,        ISUPE ,
     &        ISUPE1 ,      ISUPE11 ,        ISWCH ,         ISYS ,
     &         ISYSA ,        ITYP1 ,        ITYP2 ,        ITYP3 ,
     &         ITYP4 ,        ITYP5 ,        ITYP6 ,           IU ,
     &        IVDISP ,        IXREF ,       IXREFS ,            J ,
     &            JC ,         JCOR ,        JCORS ,           JJ ,
     &            JR ,            K ,           K1 ,          K11 ,
     &             L ,           L1 ,          L11 ,           LG ,
     &           LG1 ,         LHCT ,           LL ,          LLT ,
     &            LM ,         LMAX ,           LP ,       LPSTBE ,
     &        LPSTBP ,       LPSTBZ ,       LPXTBP ,       LPXTBZ ,
     &         LSTBE ,        LSTBP ,        LSTBZ ,        LSWCH ,
     &            LT ,          LT1 ,         LT11 ,           LU ,
     &         LXTBE ,        LXTBP ,        LXTBZ ,        MASK1 ,
     &         MASK2 ,        MASK3 ,        MASK4 ,           MN ,
     &        MULTIP ,        MULTN ,       MULTN1 ,            N ,
     &        N0CALL ,           N1 ,          N11 ,         NDEL ,
     &           NEX ,           NG ,          NG1 ,          NGL ,
     &          NGL1 ,         NHCT ,         NIMP ,       NIONIP ,
     &           NIP ,           NL ,          NL1 ,          NL2 ,
     &           NL3 ,         NLEV ,         NMAX ,         NMIN ,
     &           NNA ,         NSYS ,        NTAIL ,        NTEST ,
     &            NU ,          NVV
C-----------------------------------------------------------------------
       real*8     A0 ,           A1 ,          A1E ,        A1WIG ,
     &            A2 ,           A3 ,          A3D ,         A3ED ,
     &          A3EU ,        A3INT ,         A3PD ,         A3PU ,
     &           A3U ,         A3ZD ,         A3ZU ,          AC1 ,
     &            AD ,          ADS ,           AI ,           AL ,
     &           ALF ,       AMSHYD ,       AMSIMP ,        AMSZ0 ,
     &          ATBE ,          ATE ,          ATP ,          ATS ,
     &        BMENER ,            C ,          CA0 ,        CA0LD ,
     &         CAOLD ,        CONST ,       CONST1 ,       CONST2 ,
     &          CREC ,        CRECH ,          CRH ,          CUT ,
     &          CVAL ,            D ,           D2 ,        DEDEG ,
     &        DENIMP ,         DENS ,        DENSH ,        DENSP ,
     &           DFV ,          DNL ,          DPT ,
     &             E ,           E1 ,          E11 ,           EC ,
     &          EHCT ,       EICALL ,          EIJ ,       EJCALL ,
     &            EL ,          EL1 ,          ELL ,         ELL1 ,
     &          ELLP ,         ELLT ,        ELLT1 ,       ELLT11 ,
     &            EM ,          EMP ,           EN ,          EN1 ,
     &          EN11 ,          EN2 ,          ENA ,          ENC ,
     &        ENMAX2 ,           ES ,          EX1 ,          EXE ,
     &         EXPON ,          EXS ,          EXU ,          FIJ ,
     &          FLUX ,           FP ,          FP1 ,           FS ,
     &             G ,          GAE ,       GAMTOT ,          GAP ,
     &           GBB ,         GBBR ,          OVL ,          PHI ,
     &          PION ,        PIONH ,         PREC ,        PRECH ,
     &         PSTBE ,        PSTBP ,        PSTBZ ,        PSTIM ,
     &        PSTIMH ,        PXTBP ,        PXTBZ ,           PY ,
     &           PYD ,       PYDVAL ,       PYIMPD ,       PYIMPU ,
     &          PYPD ,         PYPU ,          PYU ,       PYUVAL ,
     &             R ,        RDEXC ,           RH ,          RHO ,
     &        RHOIMP ,         RHOO ,         RHOP ,        RHSC1 ,
     &        RQBNEW ,       RQINEW ,       RQLNEW ,       RQVNEW ,
     &           RS1 ,          RS2 ,          RS3 ,          RX3 ,
     &           SCL ,         SCLF ,        SECIP ,         SION ,
     &         SIONP ,      SIONVAL ,        SIONZ ,         STBE ,
     &          STBP ,         STBZ ,        STMIX ,        TCONV ,
     &            TE ,          TEV ,           TP ,          TPV ,
     &            TS ,        TSMIX ,            U ,           UE ,
     &            UP ,            V ,           V1 ,          V11 ,
     &         VDISP ,           VL ,           VU ,           VV ,
     &             W ,           W1 ,           W2 ,           WF ,
     &            WI ,       WICALL ,        WIG6J ,           WJ ,
     &        WJCALL ,           WS ,          WS1 ,           WT ,
     &           WTA ,          WTF ,         WTF1 ,        WTF11 ,
     &             X ,           X1 ,           X2 ,           X3 ,
     &            X4 ,           XL ,          XL1 ,          XLG ,
     &           XLP ,          XLT ,         XLT1 ,           XS ,
     &           XS1 ,          XSP ,         XTBE ,         XTBP ,
     &          XTBZ ,            Y ,           YE ,           YS ,
     &            Z0 ,           Z1 ,        ZCALL ,         ZEFF ,
     &         ZEFF1 ,        ZEFF2 ,         ZIMP ,           ZP ,
     &            ZZ
C-----------------------------------------------------------------------
       integer   KPF(1000),NLREP(80),ISARL(80),IDEGRA(800),
     &           LTG(5),LTG1(5),LXR(10),LTXR(10,10),INTER(300),
     &           NREP(31), JCA(10),JCSA(10)
C-----------------------------------------------------------------------
       real*8   ENL(1000),ENL2(1000),EXPTE(1000),EXPTS(1000),
     &          A(1000),CGBBA(800,3),ARL(80),
     &          FPG(5),FPG1(5),GE(2),
     &          CA(1000),C1(5,5,3),C2(5,5,3),C3(5,5,3),
     &          AGRL(300,3),CL1(10,10,9),CL2(10,10,9),CL3(10,10,9),
     &          CL4(10,10,9),CLT1(10,10,3,2),CLT2(10,10,3,2),
     &          CLT3(10,10,3,2),RLR(10,10),RLTR(10,10,5),
     &          SCLFA(3,6),
     &          CEXA(300),SCLA(240,8),
     &          RATEL(500),ADLA(500),EIJA(10),FIJA(10),
     &          EIJSA(10),FIJSA(10),CORA(10,6),CORSA(10,6),
     &          EEIJA(10),EEIJSA(10),
     &          COR(20),GAMA(20), cors(20),
     &          QTHREP(10), EXMTE(1000)
       real*8   AMIMPA(10) ,ZIMPA(10),FRIMPA(10)
C-----------------------------------------------------------------------
       COMMON /ACCSET/RX3,DPT,GE,EHCT,MN,NHCT,LHCT,NIP,NEX,IPRT,NDEL
       COMMON /ATSP/Z0,Z1,ALF,LP,ISP,AMSZ0,AMSHYD
       COMMON /PLSP/TE,TP,TS,DENS,DENSP,W,ATE,ATP,ATS,RHO,RHOP,
     &              ZIMP,DENIMP,RHOIMP,AMSIMP,VDISP
       COMMON /INTSP/AGRL,CL1,CL2,CL3,CL4,CLT1,CLT2,CLT3,RLR,RLTR,
     &               LXR,LTXR,INTER,INREP,NREP,NL1,NL2,NL3
       COMMON /DIEL/EIJA,EEIJA,FIJA,EIJSA,EEIJSA,FIJSA,
     &              CORA,CORSA,JCA,JCSA,IDIEL,
     &              LMAX,NTAIL,LSWCH,IGHNL,IRNDGV,ZP,EMP
       COMMON /CHEX/BMENER,DENSH,FLUX,MULTIP,MULTN,MULTN1
       COMMON /PARS1/INTD,IPRS,ILOW,IONIP,NIONIP,ILPRS,IVDISP,ZEFF,
     &               DEDEG

       COMMON /SUPPL/ATBE(NLDIM,NLDIM),XTBE(NLDIM,NLDIM),WTA(NLDIM),
     &               ENA(NLDIM),XTBP(NDLOW,NDLOW,2),XTBZ(NDLOW,NDLOW,2),
     &               PXTBP(NDLOW,2),PXTBZ(NDLOW,2),PSTBE(2),PSTBP(2),
     &               PSTBZ(2),STBE(NDLOW,2),STBP(NDLOW,2),STBZ(NDLOW,2),
     &               NNA(NLDIM),ISA(NLDIM),ILA(NLDIM),
     &               LXTBE(NLDIM,NLDIM),LXTBP(NDLOW,NDLOW,2),
     &               LXTBZ(NDLOW,NDLOW,2),LPXTBP(NDLOW,2),
     &               LPXTBZ(NDLOW,2),LSTBE(NDLOW,2),LSTBP(NDLOW,2),
     &               LSTBZ(NDLOW,2),LPSTBE(2),LPSTBP(2),LPSTBZ(2),
     &               ITYP1 ,ITYP2 ,ITYP3 ,ITYP4 ,ITYP5 ,ITYP6 ,
     &               NSYS  ,ISYSA(2) , NLEV   , IXREF(500),IXREFS(500)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C SET UP REFERENCING FUNCTION
C-----------------------------------------------------------------------

       IREF(N,L)=(N*(N-1))/2+L+1

C-----------------------------------------------------------------------
C Initialise values and constants
C
C     CONST1=3*SQRT(3)*PI*A0/(16*ALF**4*C*Z1**4)
C     CONST2=Z1**3/((2**9/9)*SQRT(PI)*ALF*C*A0**2)
C-----------------------------------------------------------------------
       
       MASK1 = Z'FFF00000'
       MASK2 = Z'000FFC00'
       MASK3 = Z'000003FF'
       MASK4 = Z'FFFFFC00'
       
       NVV = 0

       CONST1 = 6.3509817D-11/(Z1**4)
       CONST2 = 1.6189034D6*Z1**3

C-----------------------------------------------------------------------
C SET REDUCED MASS FOR PROTON HELIUM COLLISIONS.
C-----------------------------------------------------------------------

       EM = EMP

C-----------------------------------------------------------------------
C RESET PARAMETERS FOR MULTIPLE LOOP CONFIGURATION
C-----------------------------------------------------------------------

       PYIMPD  = 0.0D0
       PYIMPU  = 0.0D0
       SIONVAL = 0.0D0
       PYDVAL  = 0.0D0
       PYUVAL  = 0.0D0
       SIONZ   = 0.0D0
       PYPU    = 0.0D0
       PYPD    = 0.0D0


C-----------------------------------------------------------------------
C SET INITIAL PARAMETERS
C-----------------------------------------------------------------------

       XSP=ISP
       XS=ISG
       XS1=ISG1
       IF(ILOW.EQ.1)THEN
           IF(ISG.EQ.ISYSA(1))THEN
               ISYS=1
           ELSEIF(ISG.EQ.ISYSA(2))THEN
               ISYS=2
           ELSE
               WRITE(0,*)'***  MISMATCH OF SPIN SYSTEMS IN CCNST7'
               STOP
           ENDIF
       ENDIF
       WT=0.5D+00*XS/XSP
       ELLP=2*LP+1
       XLP=LP
       X=NL3

       ENMAX2=1.0D+00/((X+0.5D+00)*(X+0.5D+00))

       A(I)  = 0.0D+00
       CA(I) = 0.0D+00
       AC1   = 0.0D+00
       RHSC1 = 0.0D+00
       CUT   = 170.0D+00
       ZZ    = Z1*Z1


C-----------------------------------------------------------------------
       IS=0
       FP=1.0D+00
       N=ISHFR(K,20)

C-----------------------------------------------------------------------
C  SET SWITCHES:
C
C               ISWCH  -  ACTIVATES GHNLV AND GHNLE ROUTINES
C               IIWCH  -  ACTIVATES RNDEGV ROUTINE
C-----------------------------------------------------------------------
       IF (IGHNL.NE.1) THEN
           ISWCH = 0
       ELSE
           IF( (Z0.EQ.Z1.AND.N.EQ.1)                        .OR.
     &         (Z0.EQ.Z1+1.0D+00.AND.N.LE.2.AND.MULTIP.EQ.1).OR.
     &         (Z0.EQ.Z1+1.0D+00.AND.N.EQ.2.AND.MULTIP.EQ.3).OR.
     &         (Z0.EQ.Z1+2.0D+00.AND.N.EQ.2.AND.NREP(1).EQ.2)) THEN
               ISWCH=1
           ELSE
               ISWCH=0
           ENDIF
       ENDIF

       IF (IRNDGV.NE.1) THEN
           IIWCH=0
       ELSE
           IF((Z0.EQ.Z1)        .OR.
     &        (Z0.EQ.Z1+1.0D+00).OR.
     &        (Z0.EQ.Z1+2.0D+00.AND.NREP(1).EQ.2))THEN
              IIWCH=1
           ELSE
              IIWCH=0
           ENDIF
       ENDIF

C-----------------------------------------------------------------------
C  DETERMINE IF RESOLVED OR UNRESOLVED LEVEL AND FIND PARAMETERS
C-----------------------------------------------------------------------
       EN=N
       EN2=EN*EN
       IF(N.GT.NL2)THEN
           WS=XS/XSP
           ISUP=0
           ISUPE=0
           IS=1
       ELSE
           L=IAND(K,MASK2)
           L=ISHFR(L,10)

           IF(ILOW.EQ.1.AND.ITYP1.EQ.1)THEN
               IF(ISYS.EQ.1)THEN
                   ISUPE=IXREF(I)
               ELSEIF(ISYS.EQ.2)THEN
                   ISUPE=IXREFS(I)
               ELSE
                   ISUPE=0
               ENDIF
           ENDIF

           ISUP=IREF(N,L)
           IF(ISUP.GT.NDLOW)ISUP=0
           ELL=2*L+1
           XL=L
           LT=IAND(K,MASK3)
           II1=(2*LP+1)*(NL2*L+N-1)+LT-IABS(L-LP)+1
           II0=II1+(2*LP+1)*(NHCT-N-1)
           ELLT=2*LT+1
           WTF=WT*ELLT/ELLP

           XLT=LT
           WF=ELLT/(ELL*ELLP)
           WS=(ELLT*XS)/(ELLP*XSP)
           IF(N.EQ.NG.AND.L.EQ.LG)THEN
               DO J=1,NGL
                  IF(LT.EQ.LTG(J)) FP=FPG(J)
               END DO
           ENDIF
       
       ENDIF

C-----------------------------------------------------------------------
       RH=0.0D+00
       CRH=0.0D+00
       A0=0.0D+00
       CA0=0.0D+00

C-----------------------------------------------------------------------
C     BRANCH TO SPIN CHANGING PART IF IOPT.GT.1
C-----------------------------------------------------------------------

       IF(IOPT.GT.1)GO TO 156
       IU=I
       IL=I
       IF(I.GE.NMAX)GO TO 262
       I1=I+1

C-----------------------------------------------------------------------
C   1ST MAIN LOOP - THROUGH LEVELS I1 > I.  BYPASS IF NO LEVEL >I.
C-----------------------------------------------------------------------

       DO 1460 I11=I1,NMAX
       II=II1
       IF(NHCT.LE.MIN0(ISUP,ISUP11))II=II0
       A(I11)=0.0D+00
       CA(I11)=0.0D+00
       CONST=0.0D+00
       IS1=0
       FP1=1.0D+00
       V11=ENL(I11)
       E11=ENL2(I11)
       K11=KPF(I11)
       N11=ISHFR(K11,20)
       EN11=N11
       D=E-E11
       IF(N11.GT.NL2)THEN
           IS1=1
           ISUP11=0
           ISUPE11=0
       ELSE
           L11=IAND(K11,MASK2)
           L11=ISHFR(L11,10)

           IF(ILOW.EQ.1.AND.ITYP1.EQ.1)THEN
               IF(ISYS.EQ.1)THEN
                   ISUPE11=IXREF(I11)
               ELSEIF(ISYS.EQ.2)THEN
                   ISUPE11=IXREFS(I11)
               ELSE
                   ISUPE11=0
               ENDIF
           ENDIF

           ISUP11=IREF(N11,L11)
           IF(ISUP11.GT.NDLOW)ISUP11=0
           LT11=IAND(K11,MASK3)
           ELLT11=2*LT11+1

C       CHANGED THE LINE BEGINING WTF11=0.5*WT*ELLT11/ELLP
C       TO WTF11=WT*ELLT11/ELLP.
C
C       HARVEY ANDERSON 10/9/98

           WTF11=WT*ELLT11/ELLP

           IF(N11.EQ.NG1.AND.L11.EQ.LG1)THEN
               DO 384 J=1,NGL1
                IF(LT11.EQ.LTG1(J))THEN
                     FP1=FPG1(J)
                ENDIF
  384          CONTINUE
           ENDIF

           IF(L11.NE.L+1.AND.L11.NE.L-1) THEN
C-----------------------------------------------------------------------
C
C       ********        NON-DIPOLE TRANSITIONS          ********
C                       ----------------------
C
C       SUBSTITUTION OF LOW LEVEL DATA : XTBP(INDEX1,INDEX2,SPIN)
C
C
C       XTBP(,) : ARRAY CONTAINING H+ ION IMPACT EXITATION
C                 RATE COEFFICIENTS.THIS ARRAY IS ASSIGNED
C                 ITS VALUE IN THE ROUTINE SUPPHE1.F. THE
C                 RATE COEFFICIENTS ARE OBTAINED BY DIRECTLY
C                 ACCESSING CROSS-SECTIONS CONTAINED WITHIN
C                 AN ADF02 TYPE FILE. BEAM/THERMAL MAXWELL
C                 AVERAGED RATES ARE OBTAINED VIA THE
C                 ROUTINE QHE.F
C
C
C
C       NOTE    : IN THE CONTEXT OF ION  IMPACT EXCITATION,
C                 ISUPE IS THE INDEX REFERENCING THE LOWER
C                 LEVEL AND ISUPE11 REFERENCES THE UPPER LEVEL.
C                 THE BEAM ATOM IS EXCITED FROM STATE ISUPE TO
C                 ISUPHE11. EXCITATION AND DE-EXCITATION RATES
C                 ARE SUPPLIED SEPARATELY.
C
C
C       MODIFIED: HARVEY ANDERSON
C                 UNIVERSITY OF STRATHCLYDE
C                 28/1/97
C
C-----------------------------------------------------------------------

               IF(ILOW.EQ.1) THEN
                   A1E=0.0D0
                   A3ED=0.0D0
                   A3EU=0.0D0
                   A3PD=0.0D0
                   A3PU=0.0D0
                   A3ZD=0.0D0
                   A3ZU=0.0D0
                   IF(IONIP.EQ.1)THEN
                       IF(ISUP.GT.0.AND.ISUP.LE.NDLOW.AND.
     &                        ISUP11.GT.0.AND.ISUP11.LE.NDLOW
     &                            .AND.IVDISP.EQ.1)THEN
                           IF(DENSP.GT.0.0D0)THEN
                              IF(LXTBP(ISUP,ISUP11,ISYS).EQ.1)THEN
                                 A3PD=(CONST2*RHOP*WTF11*EXPTE(I11)/
     &                                DSQRT(ATP))*XTBP(ISUP11,ISUP,ISYS)

                                 A3PU=(CONST2*RHOP*WTF*EXE/
     &                                DSQRT(ATP))*XTBP(ISUP,ISUP11,ISYS)
                              ENDIF
                           ENDIF
                           IF(DENIMP.GT.0.0D0)THEN
                              IF(LXTBZ(ISUP,ISUP11,ISYS).EQ.1)THEN
                                 A3ZD=(CONST2*RHOIMP*WTF11*EXPTE(I11)/
     &                                DSQRT(ATP))*XTBZ(ISUP11,ISUP,ISYS)
                                 A3ZU=(CONST2*RHOIMP*WTF*EXE/
     &                                DSQRT(ATP))*XTBZ(ISUP,ISUP11,ISYS)
                              ENDIF
                           ENDIF
                       ENDIF
                   ENDIF



C	TEMPORARY STATEMENT TO SUPRESS ION IMPACT EXCITATION
C
C	H. ANDERSON 26/10/99
C
C 	A3PD = 0.0
C 	A3PU = 0.0
C 	A3ZD = 0.0
C 	A3ZU = 0.0


C-----------------------------------------------------------------------
C
C       ********        NON-DIPOLE TRANSITIONS          ********
C                       ----------------------
C
C       SUBSTITUTION OF LOW LEVEL DATA : XTBE(INDEX1,INDEX2)
C                                        ATBE(INDEX1,INDEX2)
C
C       XTBE(,) : ARRAY CONTAINING ELECTRON IMPACT EXCITATION
C                 RATE COEFFICIENTS.THIS ARRAY IS ASSIGNED
C                 ITS VALUE IN THE ROUTINE SUPPHE1.F. THE
C                 RATE COEFFICEINT IS OBTAINED BY DIRECTLY
C                 ACCESSING AN ADF04 TYPE FILE FROM WHICH
C                 EFFECTIVE COLLISION STRENGTHS ARE EXTRACTED.
C                 DE-EXCITATION RATES ARE OBTAINED FROM
C                 DETAILED BALANCE.
C
C       ATBE(,) : ARRAY CONTAINS TRANSITION PROBABILITIES.
C                 THIS ARRAY IS ASSIGNED ITS VALUES IN THE
C                 ROUTINE SUPPHE1.F.THE TRANSITION PROBABILITIES
C                 ARE DIRECTLY OBTAINED BY ACCESSING AN ADF04
C                 TYPE FILE.
C
C       NOTE    : IN THE CONTEXT OF ELECTRON IMPACT EXCITATION,
C                 ISUPE IS THE INDEX REFERENCING THE LOWER
C                 LEVEL AND ISUPE11 REFERENCES THE UPPER LEVEL.
C                 THE BEAM ATOM IS EXCITED FROM STATE ISUPE TO
C                 ISUPHE11. THE GENERAL FORMULAE IN THE CODE
C                 ONLY DEALS WITH DIPOLED ALLOWED TRANSITION
C                 THEREFORE THE ONLY MECHANISM TO INCORPORATE
C                 NON-DIPOLE TRANSITIONS IS VIA DATA CONTAINED
C                 WITHIN THE ADF04 TYPE FILE. IF THE ADF04
C                 TYPE FILE DOES NOT CONTAIN ALL THE SELECTED
C                 NON-DIPOLE TRANSITIONS THEN THE APPROPRIATE
C                 ELEMENT IN THE C-R MATRIX HAS A VALUE OF 0.0 .
C
C       MODIFIED: HARVEY ANDERSON
C                 UNIVERSITY OF STRATHCLYDE
C                 28/1/97
C
C-----------------------------------------------------------------------

                   IF(ISWCH.EQ.1)GO TO 145
                   IF(ISUPE.GT.0.AND.ISUPE.LE.NLDIM.AND.
     &                    ISUPE11.GT.0.AND.ISUPE11.LE.NLDIM) THEN
                       IF(LXTBE(ISUPE,ISUPE11).EQ.1)THEN
                           A1E =CONST1*WTF11*EXPTE(I11)*
     &                          ATBE(ISUPE11,ISUPE)
                           A3ED=(CONST2*RHO*WTF11*EXPTE(I11)/
     &                          DSQRT(ATE))*XTBE(ISUPE11,ISUPE)
                           A3EU=(CONST2*RHO*WTF*EXE/
     &                          DSQRT(ATE))*XTBE(ISUPE,ISUPE11)
                       ENDIF
                   ENDIF
               ENDIF

C-----------------------------------------------------------------------
C
C
C           EFFECTIVE GP-FACTORS
C           --------------------
C
C           A3ED : DE-EXCITATION BY ELECTRON COLLISIONS.
C           A3EU : EXCITATION BY ELECTRON COLLISIONS.
C           A3PD : DE-EXCITATION BY PROTONS.
C           A3PU : EXCITATION BY PROTONS.
C           A3ZD : DE-EXCITATION BY IMPURITY IONS.
C           A3ZU : EXCITATION BY IMPURITY IONS.
C
C
C   MODIFIED : HARVEY ANDERSON
C              09/08/98
C
C-----------------------------------------------------------------------

               A3D=A3ED+A3PD+A3ZD
               A3U=A3EU+A3PU+A3ZU

C-----------------------------------------------------------------------

               A(I11)=-A1E
               CA(I11)=-A3D

               tconv = (1.0D+00/const1)/(EXPTE(I11)*WTF11)

               CA0=CA0+A3U
               RH=RH+A1E
               CRH=CRH+A3PD-A3PU+A3ZD-A3ZU

               GO TO 1460
           ENDIF

           IF(IABS(LT11-LT).GT.1)THEN
               IF(ISWCH.EQ.1)THEN
                write(0,*) '****WARNING JUMPING TO LINE 145 ***'
                   GO TO 145
                              ELSE
                write(0,*) '****WARNING JUMPING TO LINE 1460 ***'
                   GO TO 1460
               ENDIF
           ENDIF

C-----------------------------------------------------------------------
C  CHECK IF DEGENERATE TREATMENT REQUIRED - IF SO SET MARKER IN IDEGRA,
C                                           THE INDEX SPAN IN IU AND
C                                           JUMP TO SECTION END AT 1460
C-----------------------------------------------------------------------

           IF(N.EQ.N11.AND.ZZ*D.LE.DEDEG)THEN
               IDEGRA(I11)=1
               IU=MAX0(IU,I11)
               GO TO 1460
           ELSE
               IDEGRA(I11)=0
           ENDIF
       ENDIF

       Y=ATE*D
       YS=ATS*D
       D=1.0D+00/D

C       TEMPORARILY SET 'IS =1' TO ALLOW A DIRECT COMPARISON
C       WITH ADAS310. IF 'IS=0' GII IS EVALUATED USING NON
C       HYDROGENIC WAVEFUNCTIONS. JUST INSERTED STATEMENT IN
C       THE CODE SHOULD BE EVENTUALLY REMOVED.
C       HARVEY ANDERSON
C       6/2/98
C
C       IS=1
C       REMOVED TEMPORARY SWITCH RUNNING NL H-BEAM
C
C       ADDED TEMPORARY LINE 'IS=1' TO ALLOW DIRECT COMPARISON
C       WITH ADAS310. H.A 10/8/98
C
C       IS=1

       IF(IS.LE.0)THEN
           IF(IS1.LE.0)THEN

C-----------------------------------------------------------------------
C
C
C       ********        DIPOLE ALLOWED TRANSITIONS         ********
C                       --------------------------
C
C       SUBSTITUTION OF LOW LEVEL DATA : ATBE(INDEX1,INDEX2)
C
C
C       ATBE(,) : ARRAY CONTAINS TRANSITION PROBABILITIES.
C                 THIS ARRAY IS ASSIGNED ITS VALUES IN THE
C                 ROUTINE SUPPHE1.F.THE TRANSITION PROBABILITIES
C                 ARE DIRECTLY OBTAINED BY ACCESSING AN ADF04
C                 TYPE FILE.
C
C       NOTE    : ISUPE IS THE INDEX REFERENCING THE LOWER
C                 LEVEL AND ISUPE11 REFERENCES THE UPPER LEVEL.
C
C
C
C       MODIFIED: HARVEY ANDERSON
C                 UNIVERSITY OF STRATHCLYDE
C                 28/1/97
C
C-----------------------------------------------------------------------

               IF(ILOW.EQ.1.AND.ITYP1.EQ.1.AND.
     &                    ISUPE.GT.0.AND.ISUPE.LE.NLDIM.AND.
     &                        ISUPE11.GT.0.AND.ISUPE11.LE.NLDIM.AND.
     &                            LXTBE(ISUPE,ISUPE11).EQ.1)THEN
                G=CONST1*ELLT11*V*V11*ATBE(ISUPE11,ISUPE)/
     &            (D*E*E11*FP*FP1)
               ELSE
                   IF(LHCT.LE.MIN0(L,L11))THEN
                       SCL=1.0D+00
                   ELSE
                       JJ=2*(L11-L+1)+LT11-LT+3
                       SCL=SCLA(II,JJ)
                   ENDIF
                   G=GBBR(N,L,LT,E,V,N11,L11,LT11,E11,V11,0,
     &                    C1,C2,C3,SCL)
               ENDIF
           ELSE
               IF(N11.GT.N+NEX)THEN
                   DFV=1.0D+00/(V11-V)
                   G=CGBBA(I,1)+DFV*(CGBBA(I,2)+DFV*CGBBA(I,3))
               ELSE
                   IF(LHCT.LE.L)THEN
                       SCL=1.0D+00
                   ELSE
                       SCL=SCLA(II,5)
                   ENDIF
                   G=GBBR(N,L,LT,E,V,N11,L+1,0,E11,V11,1,C1,C2,C3,SCL)
                   IF(L.GT.0)THEN
                       IF(LHCT.LE.L-1)THEN
                           SCL=1.0D+00
                       ELSE
                           SCL=SCLA(II,1)
                       ENDIF
                   ENDIF
                  G=G+GBBR(N,L,LT,E,V,N11,L-1,0,E11,V11,1,C1,C2,C3,SCL)
               ENDIF
               G=G*WF
           ENDIF
       ELSE
           X=N11-1
           X1=N*(N11-N)
           X=(X/X1)**0.6666667D+00
           G=GBB(V11,V,E11,X)
       ENDIF

       A1=FP*FP1*WT*E*E11*D*G/(V*V11)
       IF(IS.LE.0)THEN
           WI=ELLT
           EL=L
           R=0.5D+00*(3.0D+00*V*V-EL*(EL+1.0D+00))/Z1
           IF(IS1.LE.0)THEN
               WJ=2*LT11+1
               C=ELLP/WI
           ELSE
               WJ=ELLP*EN11*EN11
               C=1.0D+00/ELL
           ENDIF
       ELSE
           WI=ELLP*EN2
           WJ=ELLP*EN11*EN11
           R=0.25D+00*(5.0D+00*V*V+1.0D+00)/Z1
           C=1.0D+00/EN2
       ENDIF
       D2=D*D
       PHI=1.96D+00*C*E*E11*D2*D2*G/(V*V11*Z1*Z1)

C-----------------------------------------------------------------------
C
C       ********        DIPOLE ALLOWED TRANSITIONS         ********
C                       --------------------------
C
C       ATOMIC PROCESS : ELECTRON IMPACT EXCITATION.
C               METHOD : VAN REGEMORTER.
C                        PERCIVAL-RICHARDS.
C
C-----------------------------------------------------------------------

       NTEST=N11-N
       IF(IPRS.LE.0.OR.NTEST.EQ.0)THEN
           CALL PYVR(Y,Z1,PY)
       ELSE
           CALL PYPR(E,E11,N,N11,1.0D0,Z1,PHI,WI,WJ,TE,INTD,PY,RDEXC)
       ENDIF

C-----------------------------------------------------------------------
C               SUBSTITUTE IMPACT PARAMETER IF REQUESTED
C-----------------------------------------------------------------------

       IF(NTEST.LE.NIP.OR.NTEST.EQ.0)THEN
           CALL PYIPHE(E,E11,1.0D0,Z1,PHI,1.0D0,WI,WJ,R,TE,INTD,PY)
       ENDIF
       PYD=PY
       PYU=PY

C-----------------------------------------------------------------------
C
C       ********        DIPOLE ALLOWED TRANSITIONS      ********
C                       --------------------------
C
C       SUBSTITUTION OF LOW LEVEL DATA : XTBE(INDEX1,INDEX2)
C                                        ATBE(INDEX1,INDEX2)
C
C       XTBE(,) : ARRAY CONTAINING ELECTRON IMPACT EXCITATION
C                 RATE COEFFICIENTS.THIS ARRAY IS ASSIGNED
C                 ITS VALUE IN THE ROUTINE SUPPHE1.F. THE
C                 RATE COEFFICEINT IS OBTAINED BY DIRECTLY
C                 ACCESSING AN ADF04 TYPE FILE FROM WHICH
C                 EFFECTIVE COLLISION STRENGTHS ARE EXTRACTED.
C                 DE-EXCITATION RATES ARE OBTAINED FROM
C                 DETAILED BALANCE.
C
C
C       NOTE    : IN THE CONTEXT OF ELECTRON IMPACT EXCITATION,
C                 ISUPE IS THE INDEX REFERENCING THE LOWER
C                 LEVEL AND ISUPE11 REFERENCES THE UPPER LEVEL.
C                 THE BEAM ATOM IS EXCITED FROM STATE ISUPE TO
C                 ISUPHE11. ATTENTION SHOULD BE DRAWN TO THE
C                 FACT THAT THE EXCITATION AND DE-EXCITATION
C                 RATE COEFFICIENTS ARE SUPPLIED SEPARATELY.
C
C
C       MODIFIED: HARVEY ANDERSON
C                 UNIVERSITY OF STRATHCLYDE
C                 28/1/97
C
C-----------------------------------------------------------------------

       IF(ILOW.GT.0.AND.ISUPE.GT.0.AND.ISUPE.LE.NLDIM.AND.
     &            ISUPE11.GT.0.AND.ISUPE11.LE.NLDIM)THEN
           IF(LXTBE(ISUPE,ISUPE11).EQ.1)THEN
               PYD=XTBE(ISUPE11,ISUPE)/
     &             (3.15D-7*DSQRT(1.57890D5/TE)*PHI*
     &             (WI/WJ))
               PYU=XTBE(ISUPE,ISUPE11)/
     &            (3.15D-7*DSQRT(1.57890D5/TE)*PHI*
     &            EXPTE(I11)/EXPTE(I))
            ENDIF
       ENDIF

C-----------------------------------------------------------------------
C
C      ATOMIC PROCESS : ION IMPACT EXCITATION/DE-EXCITATION.
C               METHOD: VAINSHTEIN.
C                       LODGE-PERCIVAL-RICHARDS.
C                       BURGESS IMPACT PARAMETER.
C
C                 NOTE: EXCITATION AND DE-EXCITATION COEFFICIENTS
C                       MUST BE SUPPLIED SEPARATELY.
C
C             MODIFIED: HARVEY ANDERSON
C                       2/2/98
C
C-----------------------------------------------------------------------

       PYPD=0.0D0
       PYPU=0.0D0
       PYIMPD=0.0D0
       PYIMPU=0.0D0
       IF(IONIP.EQ.1)THEN
           IF(ILPRS.LE.0.AND.NTEST.NE.0)THEN
C
C       METHOD : VAINSHTEIN.
C
               IF(DENSP.GT.0.0D0.AND.NTEST.LE.NIONIP)THEN
                   PYPD=RQVNEW(Z1,N11,N,PHI,1.0D0,2.0D0,TP,VDISP)/
     &                  (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                  (WI/WJ))
                   PYPU=RQVNEW(Z1,N,N11,PHI,1.0D0,2.0D0,TP,VDISP)/
     &                  (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                  EXPTE(I11)/EXPTE(I))
               ELSE
                   PYPD=0.0D0
                   PYPU=0.0D0
               ENDIF
C
C       METHOD : VAINSHTEIN.
C
         IF(DENIMP.GT.0.0D0.AND.NTEST.LE.NIONIP)THEN
           DO 41 IMP=1,NIMP
              IF ( FRIMPA(IMP) .EQ. 0.0D0 ) GO TO 41
                  PYIMPD=PYIMPD+FRIMPA(IMP)*RQVNEW(Z1,N11,N,
     &                   PHI,ZIMPA(IMP),AMIMPA(IMP),TP,VDISP)/
     &                   (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                   (WI/WJ))
                  PYIMPU=PYIMPU+FRIMPA(IMP)*RQVNEW(Z1,N,N11,
     &                   PHI,ZIMPA(IMP),AMIMPA(IMP),TP,VDISP)/
     &                   (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                   EXPTE(I11)/EXPTE(I))
   41          CONTINUE
              ELSE
                   PYIMPD=0.0D0
                   PYIMPU=0.0D0
              ENDIF
           ELSEIF(NTEST.NE.0) THEN
C
C       METHOD : LODGE-PERCIVAL-RICHARDS.
C
               IF(DENSP.GT.0.0D0.AND.NTEST.LE.NIONIP)THEN
                   PYPD=RQLNEW(Z1,N11,N,PHI,1.0D0,2.0D0,TP,VDISP)
                   PYPD=PYPD/
     &                  (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                  (WI/WJ))
                   PYPU=RQLNEW(Z1,N,N11,PHI,1.0D0,2.0D0,TP,VDISP)
                   PYPU=PYPU/
     &                  (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                  EXPTE(I11)/EXPTE(I))
               ELSE
                   PYPD=0.0D0
                   PYPU=0.0D0
               ENDIF
C
C       METHOD : LODGE-PERCIVAL-RICHARDS.
C
             IF(DENIMP.GT.0.0D0.AND.NTEST.LE.NIONIP)THEN
                DO 42 IMP=1,NIMP
                 IF ( FRIMPA(IMP) .EQ. 0.0D0 ) GO TO 42
                   PYDVAL=FRIMPA(IMP)*RQLNEW(Z1,N11,N,
     &                  PHI,ZIMPA(IMP),AMIMPA(IMP),TP,VDISP)
                   PYIMPD=PYIMPD+PYDVAL/
     &                    (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                    (WI/WJ))
                   PYUVAL=FRIMPA(IMP)*RQLNEW(Z1,N,N11,
     &                    PHI,ZIMPA(IMP),AMIMPA(IMP),TP,VDISP)
                   PYIMPU=PYIMPU+PYUVAL/
     &                    (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                    EXPTE(I11)/EXPTE(I))
  42             CONTINUE
               ELSE
                   PYIMPD=0.0D0
                   PYIMPU=0.0D0
               ENDIF
           ELSEIF(NTEST.EQ.0)THEN
C
C       METHOD : BURGESS IMPACT PARAMETER.
C

               IF(DENSP.GT.0.0D0.AND.NTEST.LE.NIONIP)THEN

                   PYPD=RQBNEW(Z1,E11,E,PHI,1.0D0,2.0D0,TP,VDISP)
                   PYPD=PYPD/
     &                  (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                  (WI/WJ))
                   PYPU=RQBNEW(Z1,E,E11,PHI,1.0D0,2.0D0,TP,VDISP)
                   PYPU=PYPU/
     &                  (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                  EXPTE(I11)/EXPTE(I))

               ELSE
                   PYPD=0.0D0
                   PYPU=0.0D0
               ENDIF
C
C       METHOD : BURGESS IMPACT PARAMETER.
C
               IF(DENIMP.GT.0.0D0.AND.NTEST.LE.NIONIP)THEN
                 DO 43 IMP=1,NIMP
                  IF ( FRIMPA(IMP) .EQ. 0.0D0 ) GO TO 43
                   PYDVAL=FRIMPA(IMP)*RQBNEW(Z1,E11,E,PHI,
     &                    ZIMPA(IMP),AMIMPA(IMP),TP,VDISP)
                   PYIMPD=PYIMPD+PYDVAL/
     &                    (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                    (WI/WJ))
                   PYUVAL=FRIMPA(IMP)*RQBNEW(Z1,E,E11,PHI,
     &                    ZIMPA(IMP),AMIMPA(IMP),TP,VDISP)
                   PYIMPU=PYIMPU+PYUVAL/
     &                    (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                    EXPTE(I11)/EXPTE(I))
   43          CONTINUE
               ELSE
                   PYIMPD=0.0D0
                   PYIMPU=0.0D0
               ENDIF
           ENDIF

C-----------------------------------------------------------------------
C
C       ********        DIPOLE ALLOWED TRANSITIONS      ********
C                       --------------------------
C
C       SUBSTITUTION OF LOW LEVEL DATA : XTBP(INDEX1,INDEX2,SPIN)
C
C
C       XTBP(,) : ARRAY CONTAINING H+ ION IMPACT EXITATION
C                 RATE COEFFICIENTS.THIS ARRAY IS ASSIGNED
C                 ITS VALUE IN THE ROUTINE SUPPHE1.F. THE
C                 RATE COEFFICIENTS ARE OBTAINED BY DIRECTLY
C                 ACCESSING CROSS-SECTIONS CONTAINED WITHIN
C                 AN ADF02 TYPE FILE. BEAM/THERMAL MAXWELL
C                 AVERAGED RATES ARE OBTAINED VIA THE
C                 ROUTINE QHE.F
C
C
C
C       NOTE    : IN THE CONTEXT OF ION  IMPACT EXCITATION,
C                 ISUPE IS THE INDEX REFERENCING THE LOWER
C                 LEVEL AND ISUPE11 REFERENCES THE UPPER LEVEL.
C                 THE BEAM ATOM IS EXCITED FROM STATE ISUPE TO
C                 ISUPHE11. THE EXCITATION AND DE-EXCITATION RATES
C                 ARE SUPPLIED SEPARATELY.
C
C
C       MODIFIED: HARVEY ANDERSON
C                 UNIVERSITY OF STRATHCLYDE
C                 28/1/97
C
C-----------------------------------------------------------------------

           IF(ILOW.GT.0.AND.ISUP.GT.0.AND.ISUP.LE.NDLOW.AND.
     &        ISUP11.GT.0.AND.ISUP11.LE.NDLOW.AND.IVDISP.EQ.1)THEN
               IF(DENSP.GT.0.0D0)THEN
                   IF(LXTBP(ISUP,ISUP11,ISYS).EQ.1)THEN
                       PYPD=XTBP(ISUP11,ISUP,ISYS)/
     &                      (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                      (WI/WJ))

                       PYPU=XTBP(ISUP,ISUP11,ISYS)/
     &                      (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                      EXPTE(I11)/EXPTE(I))
                   ENDIF
               ENDIF
               IF(DENIMP.GT.0.0D0)THEN
                   IF(LXTBZ(ISUP,ISUP11,ISYS).EQ.1)THEN
                       PYIMPD=XTBZ(ISUP11,ISUP,ISYS)/
     &                        (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                        (WI/WJ))

                       PYIMPU=XTBZ(ISUP,ISUP11,ISYS)/
     &                        (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                        EXPTE(I11)/EXPTE(I))
                   ENDIF
                ENDIF
           ENDIF
       ENDIF

C	INSERTED TEMPORARY CODE TO SUPRESS ION IMPACT EXCITATION
C	H. ANDERSON 26/10/99
C
C 	PYPD = 0.0
C 	PYPU = 0.0
C 	PYUVAL = 0.0
C 	PYDVAL = 0.0
C       PYIMPD = 0.0
C 	PYIMPU = 0.0

       PYD=PYD*RHO+PYPD*RHOP+PYIMPD*RHOIMP
       PYU=PYU*RHO+PYPU*RHOP+PYIMPU*RHOIMP

C-----------------------------------------------------------------------

       IF(YS.LE.0.001D+00)THEN
           A2=W/(YS+0.5D+00*YS*YS)
       ELSE
           IF(EXS.LE.0.0D+00)THEN
               IF(YS.GE.CUT)THEN
                   A2=0.0D+00
               ELSE
                   A2=W/(DEXP(YS)-1.0D+00)
               ENDIF
           ELSE
               A2=W/(EXS/EXPTS(I11)-1.0D+00)
           ENDIF
       ENDIF

C       modified A3 to become density
C       dependent.
C       Harvey Anderson 15/11/97
C       -changed PY to PYD
C
       A3=D*D*D*PYD
       A(I11)=-A1*(1.0D+00+A2)*EXPTE(I11)


C       CONVERSION FOR DIPOLE ALLOWED DE-EXCITATION

        tconv = ( 1.0D+00/(ELLT11*WT) )/
     &          (EXpte(i11)*const1)


C       CONVERSION FOR DIPOLE ALLOWED EXCITATION

C        tconv = ( 1.0/(ELLT*WT) )/
C     & (EXpte(i)*const1)



       CA(I11)=-A1*A3*EXPTE(I11)
       A0=A0+A1*A2*EXE
       CA0LD=CA0
       CA0=CA0+A1*A3*EXPTE(I11)

       IF(ISWCH.EQ.1)THEN
           CA0=CA0LD
           CA(I11)=0.0D+00
       ENDIF

       RH=RH+A1*(EXPTE(I11)+A2*(EXPTE(I11)-EXE))


C-----------------------------------------------------------------------
C   FETCH SPECIAL LOW LEVEL DATA VIA ROUTINES GHNLV AND GHNLE
C-----------------------------------------------------------------------

  145  CONTINUE
       IF(ISWCH.EQ.1)THEN
           IF(Z0.EQ.Z1)ZEFF=Z0
           IF(Z0.EQ.Z1+1.0D+00)ZEFF=Z1
           IF(Z0.EQ.Z1+2.0D+00.AND.N.EQ.2)ZEFF=Z0-1.4D+00
           IF(Z0.EQ.Z1+2.0D+00.AND.N.NE.2)ZEFF=Z1
           TEV=TE/(1.16054D+04)
           IF(Z0.NE.Z1+1.0D+00)THEN
               CALL GHNLV(Z0,Z1,ZEFF,N,L,N11,NL2,TEV,GAMA,GAMTOT)
           ELSE
               CALL GHNLE(Z0,Z1,ZEFF,N,L,MULTN,N11,MULTN1,NL2,TEV,
     &                    GAMA,GAMTOT)
           ENDIF
           IF(N11.LE.NL2)THEN
               IND=L11+1
               CONST=1.7578125D-02*Z1*Z1/(XSP*(2.0D+00*LP+1.0D+00))
               CA(I11)=-CONST*RHO*GAMA(IND)*EXPTE(I11)
               CA0=CA0-CA(I11)
               GAMA(IND)=0.0D+00
           ELSE
               CONST=1.7578125D-02*Z1*Z1
               CA(I11)=-CONST*RHO*GAMTOT*EXPTE(I11)
               CA0=CA0-CA(I11)

           ENDIF
       ENDIF
 1460  CONTINUE

C-----------------------------------------------------------------------
C   EVALUATION OF DIAGONAL ELEMENTS OF COLLISIONAL RADIATIVE MATRIX
C-----------------------------------------------------------------------

  262  CONTINUE

C-----------------------------------------------------------------------
C
C      BASIC ELECTRON DATA  - ECIP
C       ATOMIC PROCESS : ELECTRON IMPACT IONISATION.
C               METHOD : ECIP.
C
C-----------------------------------------------------------------------

       EC=ENL2(I)-ENMAX2
       YE=ATE*EC
       YS=ATS*EC
       ENC=1.0D+00/DSQRT(EC)
       CALL COLINT(YE,Z1,ENC,AI)
       W2=W
       IF(I.LE.NMIN)THEN
           W2=W1
       ENDIF

C       replaced 0.43171 by 0.140625 to compensate for the
C       correct definition of rho.
C       cf. adas310 and how secip is implmented
C       Harvey Anderson
C       23/10/97
C
       SECIP=0.140625D+00*RHO*AI*ENC*ENC*1.57456D10*Z1**4/
     &           (DENS*EXPTE(I))
       SION=SECIP

C-----------------------------------------------------------------------
C
C       SUBSTITUTION OF LOW LEVEL DATA : STBE(INDEX,SPIN)
C
C       STBE(,) : ARRAY CONTAINING ELECTRON IMPACT IONISATION
C                 RATE COEFFICIENTS.THIS ARRAY IS ASSIGNED
C                 ITS VALUE IN THE ROUTINE SUPPHE1.F. THE
C                 RATE COEFFICEINT IS OBTAINED BY DIRRECTLY
C                 ACCESSING AN ADF07 TYPE FILE. THE FIRST
C                 INDEXING ELEMENT OF THE ARRAY IS OBTAINED
C                 FROM THE REFERENCING FUNCTION.
C
C       MODIFIED: HARVEY ANDERSON
C                 UNIVERSITY OF STRATHCLYDE
C                 28/1/97
C
C-----------------------------------------------------------------------

       IF(ILOW.GT.0.AND.ISUP.GT.0.AND.ISUP.LE.NDLOW.AND.
     &    LSTBE(ISUP,ISYS).EQ.1)THEN
           SION=STBE(ISUP,ISYS)
       ENDIF

C-----------------------------------------------------------------------
C
C      ATOMIC PROCESS : ION IMPACT IONISATION
C               METHOD: PERCIVAL-RICHARDS.
C
C
C             MODIFIED: HARVEY ANDERSON
C                       2/2/98
C
C
C-----------------------------------------------------------------------

       SIONP=0.0D0
       SIONZ=0.0D0
       IF(IONIP.GT.0)THEN
           IF(DENSP.GT.0.0D0)THEN
C
C       METHOD : PERCIVAL-RICHARDS
C
               SIONP=RQINEW(Z1,N,1.0D0,2.0D0,TP,VDISP)

           ELSE
               SIONP=0.0D0
           ENDIF
           IF(DENIMP.GT.0.0D0)THEN
C
C       METHOD : PERCIVAL-RICHARDS
C
                SIONVAL=0.0D0
                DO 44 IMP=1,NIMP
                IF ( FRIMPA(IMP) .EQ. 0.0D0 ) GO TO 44
                SIONVAL=SIONVAL+FRIMPA(IMP)*RQINEW(Z1,N,
     &                  ZIMPA(IMP),AMIMPA(IMP),TP,VDISP)
  44     CONTINUE
               SIONZ=SIONVAL
           ELSE
               SIONZ=0.0D0
           ENDIF

C-----------------------------------------------------------------------
C
C       SUBSTITUTION OF LOW LEVEL DATA : STBP(INDEX,SPIN)
C
C       STBP(,) : ARRAY CONTAINING COMBINED RATE COEFFICIENTS
C                 FOR SINGLE & DOUBLE ION IMPACT IONISATION.
C                 ALSO INCLUDES THE RATE FOR SINGLE CHARGE
C                 EXCHANGE.THIS ARRAY IS ASSIGNED ITS VALUE
C                 IN THE ROUTINE SUPPHE1.F . THE TARGET ION
C                 IS H+. THE INDEX IN THE FIRST ELEMENT OF
C                 THE ARRAY IS OBTAINED FROM THE REFERENCING
C                 FUNCTION.
C
C       MODIFIED: HARVEY ANDERSON
C                 UNIVERSITY OF STRATHCLYDE
C                 28/1/97
C
C-----------------------------------------------------------------------

           IF(ILOW.GT.0.AND.IVDISP.GT.0.AND.ISUP.NE.0)THEN
               IF(DENSP.GT.0.0D0.AND.LSTBP(ISUP,ISYS).EQ.1)THEN
                   SIONP=STBP(ISUP,ISYS)
               ENDIF

C-----------------------------------------------------------------------
C
C       SUBSTITUTION OF LOW LEVEL DATA : STBZ(INDEX,SPIN)
C
C       STBP(,) : ARRAY CONTAINING COMBINED RATE COEFFICIENTS
C                 FOR SINGLE & DOUBLE ION IMPACT IONISATION.
C                 ALSO INCLUDES THE RATE FOR SINGLE CHARGE
C                 EXCHANGE.THIS ARRAY IS ASSIGNED ITS VALUE
C                 IN THE ROUTINE SUPPHE1.F .THE TARGET ION
C                 IS Z+
C
C       MODIFIED: HARVEY ANDERSON
C                 28/1/98
C
C-----------------------------------------------------------------------

               IF(DENIMP.GT.0.0D0.AND.LSTBZ(ISUP,ISYS).EQ.1)THEN
                   SIONZ=STBZ(ISUP,ISYS)
               ENDIF
           ENDIF
       ENDIF

       AI=AI*(SION+(DENSP*SIONP+DENIMP*SIONZ)/DENS)/SECIP


C-----------------------------------------------------------------------
C
C
C       IS.EQ.0     : THE NL RESOLVED TREATMENT IS IMPLEMENTED AND
C                     THE CONTENTS OF THE VARIABLE AI IS STATISTICALLY
C                     DISTRIBUTED OVER THE L-SUBSTATES.
C       IS.EQ.1     : STANDARD BUNDLED-N TREATMENT.
C
C       HARVEY ANDERSON
C       23/2/98
C
C-----------------------------------------------------------------------
C
C       TEMPORARY INSERTED THE STATEMENT 'IS = 1'
C
C       IS = 1

       IF(IS.LE.0)THEN
           AI=AI*ELLT/ELLP
       ELSE
           AI=AI*EN*EN
       ENDIF

C-----------------------------------------------------------------------

       IF(W.LE.0.0D+00)THEN
           X1=0.0D+00
           X2=0.0D+00
       ELSE

           IF(YS.LE.CUT)THEN
               IF(IS.LE.0)THEN

C-----------------------------------------------------------------------
C
C       ATOMIC PROCESS : PHOTO IONISATION.
C                        STIMULATED RECOMBINATION.
C               METHOD : STANDARD INTEGRALS INVOLVING THE
C                        BOUND-FREE GAUNT FACTOR. THE
C                        ROUTINE PHOTO2 EVALUATES THE
C                        INTEGRALS USING A COMBINATION OF
C                        LAGUERRE AND GUASSIAN QUADRATURE.
C
C                 NOTE : THE VARIABLE 'IS' ACTS AS A SWITCH.
C                        WHEN N < NL2 THE BOUND-FREE GAUNT FACTOR
C                        IS EVALUATED BY INTEGRATING OVER THE
C                        RADIAL WAVEFUNCTIONS. FOR N > NL2 THE
C                        BOUND-FREE GAUNT FACTOR IS OBTAINED USING
C                        THE USUAL ANALYTICAL APPROXIMATION. THE
C                        SETTING USED TO DETERMINE WHICH METHOD
C                        IS EMPLOYED TO EVALUATE THE GII IS PASSED
C                        AS A PARAMETER IN THE CALLING ARGUMENTS
C                        OF THE ROUTINE PHOTO2.
C
C             MODIFIED : HARVEY ANDERSON
C                        5/2/98
C
C-----------------------------------------------------------------------
C
C TEMPORARILY  CHANGED IOPT FROM 1 TO 0
C H.A 5/2/98
C RETURNED IOPT BACK TO IOPT=1
C H.A 16/2/98

                   CALL PHOTO2(PION,PREC,PSTIM,CREC,ZZ,TE,TS,N,L,LT,EC,
     &                         ENC,1,0,1,CUT,1,C1,C2,C3)
                   PION=PION*WF
                   PSTIM=PSTIM*WF
               ELSE

C-----------------------------------------------------------------------
C
C       ATOMIC PROCESS : PHOTO IONISATION.
C                        STIMULATED RECOMBINATION.
C               METHOD : STANDARD INTEGRALS INVOLVING THE
C                        BOUND-FREE GAUNT FACTOR. THE
C                        ROUTINE PHOTO2 EVALUATES THE
C                        INTEGRALS USING A COMBINATION OF
C                        LAGUERRE AND GUASSIAN QUADRATURE.
C
C                 NOTE : THE VARIABLE 'IS' ACTS AS A SWITCH.
C                        WHEN N < NL2 THE BOUND-FREE GAUNT FACTOR
C                        IS EVALUATED BY INTEGRATING OVER THE
C                        RADIAL WAVEFUNCTIONS. FOR N > NL2 THE
C                        BOUND-FREE GAUNT FACTOR IS OBTAINED USING
C                        THE USUAL ANALYTICAL APPROXIMATION. THE
C                        SETTING USED TO DETERMINE WHICH METHOD
C                        IS EMPLOYED TO EVALUATE THE GII IS PASSED
C                        AS A PARAMETER IN THE CALLING ARGUMENTS
C                        OF THE ROUTINE PHOTO2.
C
C             MODIFIED : HARVEY ANDERSON
C                        5/2/98
C
C-----------------------------------------------------------------------

                   CALL PHOTO2(PION,PREC,PSTIM,CREC,ZZ,TE,TS,N,0,0,EC,
     &                         ENC,1,0,1,CUT,0,C1,C2,C3)
               ENDIF
               X1=DEXP(YE)*PION
               X2=PSTIM
           ELSE
               X=YS-ATE*ENL2(I)
               IF(X.GT.CUT)THEN
                   X1=0.0D+00
                   X2=0.0D+00
               ELSE
                   X1=DEXP(-X)/(YS+1.0D+00)
                   X2=X1*DEXP(-YE)
              ENDIF
           ENDIF
       ENDIF

C-----------------------------------------------------------------------

       A0=A0+0.5D+00*FP*WT*W*ENL2(I)/ENL(I)*X1
       CA0=CA0+0.140625D+00*FP*WT*RHO*AI*ENC*ENC
       CVAL=0.140625D+00*FP*WT*RHO*AI*ENC*ENC


C-----------------------------------------------------------------------

       IF(IS.LE.0)THEN

C-----------------------------------------------------------------------
C
C       ATOMIC PROCESS : RADIATIVE RECOMBINATION.
C
C               METHOD : STANDARD INTEGRALS INVOLVING THE
C                        BOUND-FREE GAUNT FACTOR. THE
C                        ROUTINE PHOTO2 EVALUATES THE
C                        INTEGRALS USING A COMBINATION OF
C                        LAGUERRE AND GUASSIAN QUADRATURE.
C
C                 NOTE : THE VARIABLE 'IS' ACTS AS A SWITCH.
C                        WHEN N < NL2 THE BOUND-FREE GAUNT FACTOR
C                        IS EVALUATED BY INTEGRATING OVER THE
C                        RADIAL WAVEFUNCTIONS. FOR N > NL2 THE
C                        BOUND-FREE GAUNT FACTOR IS OBTAINED USING
C                        THE USUAL ANALYTICAL APPROXIMATION. THE
C                        SETTING USED TO DETERMINE WHICH METHOD
C                        IS EMPLOYED TO EVALUATE THE GII IS PASSED
C                        AS A PARAMETER IN THE CALLING ARGUMENTS
C                        OF THE ROUTINE PHOTO2.
C
C             MODIFIED : HARVEY ANDERSON
C                        5/2/98
C
C-----------------------------------------------------------------------
C
C TEMPORARILY  CHANGED IOPT FROM 1 TO 0
C H.A 5/2/98
C RETURNED IOPT BACK TO IOPT=1
C H.A 16/2/98

                   CALL PHOTO2(PION,PREC,PSTIM,CREC,ZZ,TE,TS,N,L,LT,EC,
     &                 ENC,0,1,0,CUT,1,C1,C2,C3)

           SCLF=1.0D+00
           LL=L+1
           LLT=LT-IABS(L-LP)+1
           IF(L.LE.LHCT)THEN
               IF(N.GT.NHCT)THEN
                   SCLF=SCLFA(LL,LLT)
               ELSEIF(N.EQ.NHCT)THEN
                   NHCT=NHCT+1

C-----------------------------------------------------------------------
C
C       ATOMIC PROCESS : RADIATIVE RECOMBINATION.
C
C               METHOD : STANDARD INTEGRALS INVOLVING THE
C                        BOUND-FREE GAUNT FACTOR. THE
C                        ROUTINE PHOTO2 EVALUATES THE
C                        INTEGRALS USING A COMBINATION OF
C                        LAGUERRE AND GUASSIAN QUADRATURE.
C
C                 NOTE : THE VARIABLE 'IS' ACTS AS A SWITCH.
C                        WHEN N < NL2 THE BOUND-FREE GAUNT FACTOR
C                        IS EVALUATED BY INTEGRATING OVER THE
C                        RADIAL WAVEFUNCTIONS. FOR N > NL2 THE
C                        BOUND-FREE GAUNT FACTOR IS OBTAINED USING
C                        THE USUAL ANALYTICAL APPROXIMATION. THE
C                        SETTING USED TO DETERMINE WHICH METHOD
C                        IS EMPLOYED TO EVALUATE THE GII IS PASSED
C                        AS A PARAMETER IN THE CALLING ARGUMENTS
C                        OF THE ROUTINE PHOTO2.
C
C             MODIFIED : HARVEY ANDERSON
C                        5/2/98
C
C-----------------------------------------------------------------------
C
C TEMPORARILY  CHANGED IOPT FROM 1 TO 0
C H.A 5/2/98
C RETURNED IOPT BACK TO IOPT = 1
C H.A. 16/2/98

                   CALL PHOTO2(PIONH,PRECH,PSTIMH,CRECH,ZZ,TE,TS,N,L,
     &                         LT,EC,ENC,0,1,0,CUT,1,C1,C2,C3)
                   SCLF=PRECH/PREC
                   SCLFA(LL,LLT)=SCLF
                   NHCT=NHCT-1
               ENDIF
               PREC=PREC*WF*SCLF
           ENDIF
       ELSE

C-----------------------------------------------------------------------
C
C       ATOMIC PROCESS : RADIATIVE RECOMBINATION.
C
C               METHOD : STANDARD INTEGRALS INVOLVING THE
C                        BOUND-FREE GAUNT FACTOR. THE
C                        ROUTINE PHOTO2 EVALUATES THE
C                        INTEGRALS USING A COMBINATION OF
C                        LAGUERRE AND GUASSIAN QUADRATURE.
C
C                 NOTE : THE VARIABLE 'IS' ACTS AS A SWITCH.
C                        WHEN N < NL2 THE BOUND-FREE GAUNT FACTOR
C                        IS EVALUATED BY INTEGRATING OVER THE
C                        RADIAL WAVEFUNCTIONS. FOR N > NL2 THE
C                        BOUND-FREE GAUNT FACTOR IS OBTAINED USING
C                        THE USUAL ANALYTICAL APPROXIMATION. THE
C                        SETTING USED TO DETERMINE WHICH METHOD
C                        IS EMPLOYED TO EVALUATE THE GII IS PASSED
C                        AS A PARAMETER IN THE CALLING ARGUMENTS
C                        OF THE ROUTINE PHOTO2.
C
C             MODIFIED : HARVEY ANDERSON
C                        5/2/98
C
C-----------------------------------------------------------------------


           CALL PHOTO2(PION,PREC,PSTIM,CREC,ZZ,TE,TS,N,0,0,EC,
     &                 ENC,0,1,0,CUT,0,C1,C2,C3)

       ENDIF
       X3=PREC

       ADS=0.00D+00
       IF(N.GT.NL1)THEN
           DO 205 ID=1,IDIEL
            FIJ=FIJA(ID)
            IF(FIJ.LE.0.00D+00)GOTO 205
            EIJ=EIJA(ID)
            JCOR=JCA(ID)
            DO 204 JC=1,JCOR
             COR(JC)=CORA(ID,JC)
  204       CONTINUE
            CALL NDIEL(Z1,EIJ,FIJ,0.0D0,COR,JCOR,ES,FS,
     &                 CORS,JCORS,N,AD,ADLA)

                IF(IS.LE.0)THEN
                ADS=ADS+ADLA(L+1)*EEIJA(ID)*EXPTE(I)
                  ELSE
                ADS=ADS+AD*EEIJA(ID)*EXPTE(I)
                ENDIF
  205      CONTINUE
       ENDIF
       IF(DENSH.GT.0.00D+00)THEN

C-----------------------------------------------------------------------
C                       CHARGE EXCHANGE SECTION
C-----------------------------------------------------------------------
C
C
C          X4=QTHREP(I)
C           X4=9.62134D12*DENSH*X4/(DENS*Z*ATE**1.5D0)
C
C       END OF CODE INSERTED BY HARVEY ANDERSON 11/2/98

           X4=0.0D0
C-----------------------------------------------------------------------
C          IF(IS.LE.0)THEN
C              X4=9.62134D12*DENSH*RATEL(L+1)/(DENS*Z1*ATE**1.5D0)
C              IF(Z1+1.0.NE.Z0)FACT=1.0
C              IF(MULTIP.EQ.1.AND.Z1+1.EQ.Z0)FACT=1.0/4.0
C              IF(MULTIP.EQ.3.AND.Z1+1.EQ.Z0)FACT=3.0/4.0
C              X4=X4*FACT
C          ELSE
C              X4=9.62134D12*DENSH*RATEN/(DENS*Z1*ATE**1.5D0)
C              IF(Z1+1.NE.Z0)FACT=1.0
C              IF(MULTIP.EQ.1.AND.Z1+1.EQ.Z0)FACT=1.0/4.0
C              IF(MULTIP.EQ.3.AND.Z1+1.EQ.Z0)FACT=3.0/4.0
C              X4=X4*FACT
C          ENDIF
C-----------------------------------------------------------------------
       ELSE
           X4=0.0D0
       ENDIF

       RH=RH+0.5D+00*FP*WT*ENL2(I)*(W*X2-W*X1+X3)/ENL(I)+WT*(ADS+X4)
       RHSC1=0.5D+00*ZZ*FP*WT*ENL2(I)*CREC/(ENL(I)*ATE)

       IF(I.LE.NMIN)GO TO 241
       I2=I-1
C-----------------------------------------------------------------------
C   2ND MAIN LOOP - THROUGH LEVELS I1 < I.  BYPASS IF NO LEVEL <I.
C-----------------------------------------------------------------------
       DO 157 I1=NMIN,I2
       A(I1)=0.0D+00
       CA(I1)=0.0D+00
       CONST=0.0D+00

C       TEMPORARILY CHANGED IS1=0 TO IS1=1 TO ALLOW THE
C       CODE TO BE DIRECTLY COMPARED TO ADAS310. IF
C       IS1=0 THE GI IS EVALUATED USING RECURSIVE-RELATIONS.
C       H.A 10/8/98
C       RETURNED VARIABLE BACK TO 'IS1=0'
C
       IS1=0

       FP1=1.0D+00
       V1=ENL(I1)
       E1=ENL2(I1)
       K1=KPF(I1)
       N1=ISHFR(K1,20)
C-----------------------------------------------------------------------
C   SET SWITCH :
C                ISWCH
C-----------------------------------------------------------------------
       IF(IGHNL.NE.1)THEN
           ISWCH=0
       ELSE
           IF((Z0.EQ.Z1.AND.N1.EQ.1)                          .OR.
     &        (Z0.EQ.Z1+1.0D+00.AND.N1.LE.2.AND.MULTIP.EQ.1)  .OR.
     &        (Z0.EQ.Z1+1.0D+00.AND.N1.EQ.2.AND.MULTIP.EQ.3)  .OR.
     &        (Z0.EQ.Z1+2.0D+00.AND.N1.EQ.2.AND.NREP(1).EQ.2))THEN
              ISWCH=1
           ELSE
              ISWCH=0
           ENDIF
       ENDIF
       EN=N
       EN1=N1
       D=E1-E
       IF(N1.GT.NL2)THEN
           ISUP1=0
           ISUPE1=0
           IS1=1
       ELSE
           L1=IAND(K1,MASK2)
           L1=ISHFR(L1,10)

           IF(ILOW.EQ.1.AND.ITYP1.EQ.1)THEN
               IF(ISYS.EQ.1)THEN
                   ISUPE1=IXREF(I1)
               ELSEIF(ISYS.EQ.2)THEN
                   ISUPE1=IXREFS(I1)
               ELSE
                   ISUPE1=0
               ENDIF
           ENDIF

           ISUP1=IREF(N1,L1)
           IF(ISUP1.GT.NDLOW)ISUP1=0
           ELL1=2*L1+1
           LT1=IAND(K1,MASK3)
           NL=MIN0(N1,NHCT-1)
           II=(2*LP+1)*(NL2*L1+NL-1)+LT1-IABS(L1-LP)+1
           ELLT1=2*LT1+1

C       CHANGE THE LINE BEGINING WTF1=0.5*WT*ELLT1/ELLP
C       TO WTF1=WT*ELLT1/ELLP
C
C       HARVEY ANDERSON 10/9/98

           WTF1=WT*ELLT1/ELLP
           WF=ELLT1/(ELLP*ELL1)
           IF(N1.EQ.NG1.AND.L1.EQ.LG1)THEN
               DO 381 J=1,NGL1
                IF(LT1.EQ.LTG1(J))THEN
                    FP1=FPG1(J)
                ENDIF
  381          CONTINUE
           ENDIF
           IF(IS.LE.0)THEN

               IF(L1.NE.L+1.AND.L1.NE.L-1) THEN

C-----------------------------------------------------------------------
C
C       ********        NON-DIPOLE TRANSITIONS          ********
C                       ----------------------
C
C       SUBSTITUTION OF LOW LEVEL DATA : XTBP(INDEX1,INDEX2,SPIN)
C
C
C       XTBP(,) : ARRAY CONTAINING H+ ION IMPACT EXITATION
C                 RATE COEFFICIENTS.THIS ARRAY IS ASSIGNED
C                 ITS VALUE IN THE ROUTINE SUPPHE1.F. THE
C                 RATE COEFFICIENTS ARE OBTAINED BY DIRECTLY
C                 ACCESSING CROSS-SECTIONS CONTAINED WITHIN
C                 AN ADF02 TYPE FILE. BEAM/THERMAL MAXWELL
C                 AVERAGED RATES ARE OBTAINED VIA THE
C                 ROUTINE QHE.F
C
C
C
C       NOTE    : IN THE CONTEXT OF ION  IMPACT EXCITATION,
C                 ISUPE IS THE INDEX REFERENCING THE LOWER
C                 LEVEL AND ISUPE11 REFERENCES THE UPPER LEVEL.
C                 THE BEAM ATOM IS EXCITED FROM STATE ISUPE TO
C                 ISUPHE11. EXCITATION AND DE-EXCITATION RATES
C                 ARE SUPPLIED SEPARATELY.
C
C
C       MODIFIED: HARVEY ANDERSON
C                 UNIVERSITY OF STRATHCLYDE
C                 28/1/97
C
C-----------------------------------------------------------------------

                   IF(ILOW.EQ.1) THEN
                       A1E=0.0D0
                       A3ED=0.0D0
                       A3EU=0.0D0
                       A3PD=0.0D0
                       A3PU=0.0D0
                       A3ZD=0.0D0
                       A3ZU=0.0D0
C---------------------------------
C                      ION IMPACT DATA
C---------------------------------
                IF ( IONIP.EQ.1 ) THEN
                       IF(ISUP.GT.0.AND.ISUP.LE.NDLOW.AND.
     &                        ISUP1.GT.0.AND.ISUP1.LE.NDLOW
     &                            .AND.IVDISP.EQ.1)THEN
                           IF(DENSP.GT.0.0D0)THEN
                               IF(LXTBP(ISUP1,ISUP,ISYS).EQ.1)THEN
                                  A3PD=(CONST2*RHOP*WTF*EXE/
     &                                 DSQRT(ATP))*XTBP(ISUP,ISUP1,ISYS)
                                  A3PU=(CONST2*RHOP*WTF1*EXPTE(I1)/
     &                                 DSQRT(ATP))*XTBP(ISUP1,ISUP,ISYS)
                               ENDIF
                           ENDIF
                           IF(DENIMP.GT.0.0D0)THEN
                               IF(LXTBZ(ISUP1,ISUP,ISYS).EQ.1)THEN
                                  A3ZD=(CONST2*RHOIMP*WTF*EXE/
     &                                 DSQRT(ATP))*XTBZ(ISUP,ISUP1,ISYS)
                                  A3ZU=(CONST2*RHOIMP*WTF1*EXPTE(I1)/
     &                                 DSQRT(ATP))*XTBZ(ISUP1,ISUP,ISYS)
                               ENDIF
                           ENDIF
                       ENDIF
                ENDIF

C-----------------------------------------------------------------------
C	INSERTED TEMPORARY STATEMENTS TO SUPRESS ION IMPACT EXCITATION
C
C
C 	A3PD=0.0
C 	A3PU=0.0
C 	A3ZD=0.0
C 	A3ZU=0.0
C
C-----------------------------------------------------------------------
C
C       ********        NON-DIPOLE TRANSITIONS          ********
C                       ----------------------
C
C       SUBSTITUTION OF LOW LEVEL DATA : XTBE(INDEX1,INDEX2)
C                                        ATBE(INDEX1,INDEX2)
C
C       XTBE(,) : ARRAY CONTAINING ELECTRON IMPACT EXCITATION
C                 RATE COEFFICIENTS.THIS ARRAY IS ASSIGNED
C                 ITS VALUE IN THE ROUTINE SUPPHE1.F. THE
C                 RATE COEFFICEINT IS OBTAINED BY DIRECTLY
C                 ACCESSING AN ADF04 TYPE FILE FROM WHICH
C                 EFFECTIVE COLLISION STRENGTHS ARE EXTRACTED.
C                 DE-EXCITATION RATES ARE OBTAINED FROM
C                 DETAILED BALANCE.
C
C       ATBE(,) : ARRAY CONTAINS TRANSITION PROBABILITIES.
C                 THIS ARRAY IS ASSIGNED ITS VALUES IN THE
C                 ROUTINE SUPPHE1.F.THE TRANSITION PROBABILITIES
C                 ARE DIRECTLY OBTAINED BY ACCESSING AN ADF04
C                 TYPE FILE.
C
C       NOTE    : IN THE CONTEXT OF ELECTRON IMPACT EXCITATION,
C                 ISUPE IS THE INDEX REFERENCING THE LOWER
C                 LEVEL AND ISUPE11 REFERENCES THE UPPER LEVEL.
C                 THE BEAM ATOM IS EXCITED FROM STATE ISUPE TO
C                 ISUPHE11.
C
C       MODIFIED: HARVEY ANDERSON
C                 UNIVERSITY OF STRATHCLYDE
C                 28/1/97
C
C-----------------------------------------------------------------------

                       IF(ISWCH.EQ.1)GO TO 155
                       IF(ISUPE.GT.0.AND.ISUPE.LE.NLDIM.AND.
     &                        ISUPE1.GT.0.AND.ISUPE1.LE.NLDIM) THEN
                            IF(LXTBE(ISUPE1,ISUPE).EQ.1)THEN
                                A1E=CONST1*WTF*EXE*ATBE(ISUPE,ISUPE1)
                                A3ED=(CONST2*RHO*WTF*EXE/
     &                               DSQRT(ATE))*XTBE(ISUPE,ISUPE1)
                                A3EU=(CONST2*RHO*WTF1*EXPTE(I1)/
     &                              DSQRT(ATE))*XTBE(ISUPE1,ISUPE)
                            ENDIF
                       ENDIF
                   ENDIF

C-----------------------------------------------------------------------
C
C
C           EFFECTIVE GP-FACTORS
C           --------------------
C
C           A3ED : DE-EXCITATION BY ELECTRON COLLISIONS.
C           A3EU : EXCITATION BY ELECTRON COLLISIONS.
C           A3PD : DE-EXCITATION BY PROTONS.
C           A3PU : EXCITATION BY PROTONS.
C           A3ZD : DE-EXCITATION BY IMPURITY IONS.
C           A3ZU : EXCITATION BY IMPURITY IONS.
C
C
C   MODIFIED : HARVEY ANDERSON
C              09/08/98
C
C-----------------------------------------------------------------------

                   A3D=A3ED+A3PD+A3ZD
                   A3U=A3EU+A3PU+A3ZU
                   CA(I1)=-A3U

C-----------------------------------------------------------------------
C
C        tconv = (1.0/(const1))/(EXPTE(I1)*WTF1)

                   A0=A0+A1E
                   CA0=CA0+A3D
                   RH=RH-A1E
                   CRH=CRH+A3PU-A3PD+A3ZU-A3ZD
                   GO TO 157
               ENDIF

               IF(IABS(LT1-LT).GT.1)THEN
                   IF(ISWCH.NE.1)THEN
                       GO TO 157
                   ELSE
                       GO TO 155
                   ENDIF
               ENDIF
           ENDIF

C-----------------------------------------------------------------------
C
C                    DIPOLE ALLOWED TRANSITION
C
C  CHECK IF DEGENERATE TREATMENT REQUIRED - IF SO SET MARKER IN IDEGRA,
C                                           THE INDEX SPAN IN IU AND
C                                           JUMP TO SECTION END AT 1460
C-----------------------------------------------------------------------

           IF(N.EQ.N1.AND.ZZ*D.LE.DEDEG)THEN
               IDEGRA(I1)=1
               IL=MIN0(IL,I1)
               GO TO 157
           ELSE
               IDEGRA(I1)=0
           ENDIF
       ENDIF
       Y=ATE*D
       YS=ATS*D
       D=1.0D+00/D

C       TEMPORARILY SET 'IS =1' TO ALLOW A DIRECT COMPARISON
C       WITH ADAS310. IF 'IS=0' GII IS EVALUATED USING NON
C       HYDROGENIC WAVEFUNCTIONS. JUST ENTERED STATEMENT.
C
C       HARVEY ANDERSON
C       10/8/98
C
C
C       IS=1
C       REMOVED TEMP LINE 108/98 H.A.


       IF(IS.LE.0)THEN

C-----------------------------------------------------------------------
C
C
C       ********        DIPOLE ALLOWED TRANSITIONS         ********
C                       --------------------------
C
C       SUBSTITUTION OF LOW LEVEL DATA : ATBE(INDEX1,INDEX2)
C
C
C       ATBE(,) : ARRAY CONTAINS TRANSITION PROBABILITIES.
C                 THIS ARRAY IS ASSIGNED ITS VALUES IN THE
C                 ROUTINE SUPPHE1.F.THE TRANSITION PROBABILITIES
C                 ARE DIRECTLY OBTAINED BY ACCESSING AN ADF04
C                 TYPE FILE.
C
C       NOTE    : ISUPE IS THE INDEX REFERENCING THE LOWER
C                 LEVEL AND ISUPE1 REFERENCES THE UPPER LEVEL.
C
C
C
C       MODIFIED: HARVEY ANDERSON
C                 UNIVERSITY OF STRATHCLYDE
C                 28/1/97
C
C-----------------------------------------------------------------------

           IF(ILOW.EQ.1.AND.ITYP1.EQ.1.AND.
     &                ISUPE.GT.0.AND.ISUPE.LE.NLDIM.AND.
     &                    ISUPE1.GT.0.AND.ISUPE1.LE.NLDIM.AND.
     &                        LXTBE(ISUPE1,ISUPE).EQ.1)THEN
                G=CONST1*ELLT*V*V1*ATBE(ISUPE,ISUPE1)/(D*E*E1)
           ELSE
               IF(LHCT.LE.MIN0(L,L1))THEN
                   SCL=1.0D+00
               ELSE
                   JJ=2*(L-L1+1)+LT-LT1+3
                   SCL=SCLA(II,JJ)
               ENDIF
               G=GBBR(N1,L1,LT1,E1,V1,N,L,LT,E,V,0,C1,C2,C3,SCL)
           ENDIF
       ELSE
           IF(IS1.LE.0)THEN
               IF(N.GT.N1+NEX)THEN
                   DFV=1.0D+00/(V-V1)
                   G=CGBBA(I1,1)+DFV*(CGBBA(I1,2)+DFV*CGBBA(I1,3))
               ELSE
                   SCL=1.0D+00
                   IF(LHCT.GT.L1)THEN
                       SCL=SCLA(II,5)
                   ENDIF
                   G=GBBR(N1,L1,LT1,E1,V1,N,L1+1,0,E,V,1,C1,C2,C3,SCL)
                   IF(L1.GT.0)THEN
                       IF(LHCT.LE.L1-1)THEN
                           SCL=1.0D+00
                       ELSE
                           SCL=SCLA(II,1)
                       ENDIF
                       G=G+GBBR(N1,L1,LT1,E1,V1,N,L1-1,0,E,V,1,
     &                          C1,C2,C3,SCL)
                   ENDIF
               ENDIF
               G=G*WF
           ELSE
               X=N-1
               X1=N1*(N-N1)
               X=(X/X1)**0.6666667D+00
               G=GBB(V,V1,E,X)
           ENDIF
       ENDIF
       A1=FP*FP1*WT*E*E1*D*G/(V*V1)
       IF(IS1.LE.0)THEN
           WI=ELLT1
           EL1=L1
           R=0.5D+00*(3.0D+00*V1*V1-EL1*(EL1+1.0D+00))/Z1
           IF(IS.LE.0)THEN
               WJ=ELLT
               C=ELLP/WI
           ELSE
               WJ=ELLP*EN2
               C=1.0D+00/ELL1
           ENDIF
       ELSE
           WI=ELLP*EN1*EN1
           WJ=ELLP*EN2
           R=0.25D+00*(5.0D+00*V1*V1+1.0D+00)/Z1
           C=1.0D+00/(EN1*EN1)
       ENDIF
       D2=D*D
       PHI=1.96D+00*C*E*E1*D2*D2*G/(V*V1*Z1*Z1)

C-----------------------------------------------------------------------
C
C       ATOMIC PROCESS : ELECTRON IMPACT EXCITATION
C               METHOD : VAN REGEMORTER.
C                        PERCIVAL-RICHARDS.
C
C             MODIFIED : HARVEY ANDERSON
C                        2/2/98
C
C-----------------------------------------------------------------------

       NTEST=N-N1
       IF(IPRS.LE.0.OR.NTEST.EQ.0)THEN
           CALL PYVR(Y,Z1,PY)
       ELSE
           CALL PYPR(E1,E,N1,N,1.0D0,Z1,PHI,WI,WJ,TE,INTD,PY,RDEXC)
       ENDIF

C-----------------------------------------------------------------------
C              SUBSTITUTE IMPACT PARAMETER IF REQUESTED
C-----------------------------------------------------------------------

       IF(NTEST.LE.NIP.OR.NTEST.EQ.0)THEN
           CALL PYIPHE(E1,E,1.0D0,Z1,PHI,1.0D0,WI,WJ,R,TE,INTD,PY)
       ENDIF

       PYD=PY
       PYU=PY


C-----------------------------------------------------------------------
C
C       ********        DIPOLE ALLOWED TRANSITIONS      ********
C                       --------------------------
C
C       SUBSTITUTION OF LOW LEVEL DATA : XTBE(INDEX1,INDEX2)
C
C
C       XTBE(,) : ARRAY CONTAINING ELECTRON IMPACT EXCITATION
C                 RATE COEFFICIENTS.THIS ARRAY IS ASSIGNED
C                 ITS VALUE IN THE ROUTINE SUPPHE1.F. THE
C                 RATE COEFFICEINT IS OBTAINED BY DIRECTLY
C                 ACCESSING AN ADF04 TYPE FILE FROM WHICH
C                 EFFECTIVE COLLISION STRENGTHS ARE EXTRACTED.
C                 DE-EXCITATION RATES ARE OBTAINED FROM
C                 DETAILED BALANCE.
C
C
C       NOTE    : IN THE CONTEXT OF ELECTRON IMPACT EXCITATION,
C                 ISUPE IS THE INDEX REFERENCING THE LOWER
C                 LEVEL AND ISUPE11 REFERENCES THE UPPER LEVEL.
C                 THE BEAM ATOM IS EXCITED FROM STATE ISUPE TO
C                 ISUPHE11. ATTENTION SHOULD BE DRAWN TO THE
C                 FACT THAT THE EXCITATION AND DE-EXCITATION
C                 RATE COEFFICIENTS ARE SUPPLIED SEPARATELY.
C
C
C       MODIFIED: HARVEY ANDERSON
C                 UNIVERSITY OF STRATHCLYDE
C                 28/1/97
C
C-----------------------------------------------------------------------

       IF(ILOW.GT.0.AND.ISUPE1.GT.0.AND.ISUPE1.LE.NLDIM.AND.
     &            ISUPE.GT.0.AND.ISUPE.LE.NLDIM)THEN
           IF(LXTBE(ISUPE1,ISUPE).EQ.1)THEN
               PYU=XTBE(ISUPE1,ISUPE)/
     &             (3.15D-7*DSQRT(1.57890D5/TE)*PHI*
     &             EXPTE(I)/EXPTE(I1))

               PYD=XTBE(ISUPE,ISUPE1)/
     &             (3.15D-7*DSQRT(1.57890D5/TE)*PHI*
     &             (WI/WJ))
            ENDIF
       ENDIF

C-----------------------------------------------------------------------
C
C
C       ********        DIPOLE ALLOWED TRANSITIONS      ********
C                       --------------------------
C
C      ATOMIC PROCESS : ION IMPACT EXCITATION/DE-EXCITATION.
C               METHOD: VAINSHTEIN.
C                       LODGE-PERCIVAL-RICHARDS.
C                       BURGESS IMPACT PARAMETER.
C
C                 NOTE: EXCITATION AND DE-EXCITATION COEFFICIENTS
C                       MUST BE SUPPLIED SEPARATELY.
C
C             MODIFIED: HARVEY ANDERSON
C                       2/2/98
C
C-----------------------------------------------------------------------

       PYPU=0.0D0
       PYPD=0.0D0
       PYIMPU=0.0D0
       PYIMPD=0.0D0
       IF(IONIP.EQ.1)THEN
           IF(ILPRS.LE.0.AND.NTEST.NE.0)THEN
C
C       METHOD : VAINSHTEIN.
C
               IF(DENSP.GT.0.0D0.AND.NTEST.LE.NIONIP)THEN
                   PYPU=RQVNEW(Z1,N1,N,PHI,1.0D0,2.0D0,TP,VDISP)/
     &                  (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                  EXPTE(I)/EXPTE(I1))

                   PYPD=RQVNEW(Z1,N,N1,PHI,1.0D0,2.0D0,TP,VDISP)/
     &                  (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                  (WI/WJ))
               ELSE
                   PYPU=0.0D0
                   PYPD=0.0D0
               ENDIF
C
C       METHOD : VAINSHTEIN.
C

            IF(DENIMP.GT.0.0D0.AND.NTEST.LE.NIONIP)THEN
                DO 46 IMP=1,NIMP
                   IF ( FRIMPA(IMP) .EQ. 0.0D0 ) GO TO 46
                   PYIMPU=PYIMPU+FRIMPA(IMP)*RQVNEW(Z1,N1,N,PHI,
     &                    ZIMPA(IMP),AMIMPA(IMP),TP,VDISP)/
     &                    (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                    EXPTE(I)/EXPTE(I1))

                   PYIMPD=PYIMPD+FRIMPA(IMP)*RQVNEW(Z1,N,N1,PHI,
     &                    ZIMPA(IMP),AMIMPA(IMP),TP,VDISP)/
     &                    (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                    (WI/WJ))
   46          CONTINUE
               ELSE
                   PYIMPU=0.0D0
                   PYIMPD=0.0D0
               ENDIF

           ELSEIF(ILPRS.EQ.1.AND.NTEST.NE.0)THEN
C
C       METHOD : LODGE-PERCIVAL-RICHARDS.
C
               IF(DENSP.GT.0.0D0.AND.NTEST.LE.NIONIP)THEN
                   PYPU=RQLNEW(Z1,N1,N,PHI,1.0D0,2.0D0,TP,VDISP)/
     &                  (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                  EXPTE(I)/EXPTE(I1))

                   PYPD=RQLNEW(Z1,N,N1,PHI,1.0D0,2.0D0,TP,VDISP)/
     &                  (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                  (WI/WJ))
               ELSE
                   PYPU=0.0D0
                   PYPD=0.0D0
               ENDIF
C
C       METHOD : LODGE-PERCIVAL-RICHARDS.
C

             IF(DENIMP.GT.0.0D0.AND.NTEST.LE.NIONIP)THEN
                DO 47 IMP=1,NIMP
                   IF( FRIMPA(IMP) .EQ. 0.0D0 ) GO TO 47
                      PYDVAL=FRIMPA(IMP)*RQLNEW(Z1,N,N1,PHI,
     &                       ZIMPA(IMP),AMIMPA(IMP),TP,VDISP)
                      PYIMPD=PYIMPD+PYDVAL/
     &                    (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                    (WI/WJ))

                   PYUVAL=FRIMPA(IMP)*RQLNEW(Z1,N1,N,PHI,
     &                    ZIMPA(IMP),AMIMPA(IMP),TP,VDISP)
                   PYIMPU=PYIMPU+PYUVAL/
     &                    (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                    EXPTE(I)/EXPTE(I1))
   47           CONTINUE
               ELSE
                   PYIMPU=0.0D0
                   PYIMPD=0.0D0
               ENDIF
           ELSEIF(NTEST.EQ.0)THEN
C
C       METHOD: BURGESS IMPACT PARAMETER.
C
               IF(DENSP.GT.0.0D0)THEN
                   PYPU=RQBNEW(Z1,E1,E,PHI,1.0D0,2.0D0,TP,VDISP)/
     &                  (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                  EXPTE(I)/EXPTE(I1))
                   PYPD=RQBNEW(Z1,E,E1,PHI,1.0D0,2.0D0,TP,VDISP)/
     &                  (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                  (WI/WJ))
               ELSE
                   PYPU=0.0D0
                   PYPD=0.0D0
               ENDIF
C
C       METHOD: BURGESS IMPACT PARAMETER.
C
               IF(DENIMP.GT.0.0D0)THEN
                  DO 48 IMP=1,NIMP
                    IF (FRIMPA(IMP).EQ.0.0D0) GO TO 48

                   PYDVAL=FRIMPA(IMP)*RQBNEW(Z1,E,E1,PHI,
     &                    ZIMPA(IMP),AMIMPA(IMP),TP,VDISP)
                   PYIMPD = PYIMPD + PYDVAL/
     &                    (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                    (WI/WJ))

                   PYUVAL=FRIMPA(IMP)*RQBNEW(Z1,E1,E,PHI,
     &                    ZIMPA(IMP),AMIMPA(IMP),TP,VDISP)
                   PYIMPU=PYIMPU + PYUVAL/
     &                    (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                    EXPTE(I)/EXPTE(I1))
  48        CONTINUE
               ELSE
                   PYIMPU=0.0D0
                   PYIMPD=0.0D0
               ENDIF
           ENDIF

C-----------------------------------------------------------------------
C
C       ********        DIPOLE ALLOWED TRANSITIONS      ********
C                       --------------------------
C
C       SUBSTITUTION OF LOW LEVEL DATA : XTBP(INDEX1,INDEX2,SPIN)
C
C
C       XTBP(,) : ARRAY CONTAINING H+ ION IMPACT EXCITATION
C                 RATE COEFFICIENTS.THIS ARRAY IS ASSIGNED
C                 ITS VALUE IN THE ROUTINE SUPPHE1.F. THE
C                 RATE COEFFICIENTS ARE OBTAINED BY DIRECTLY
C                 ACCESSING CROSS-SECTIONS CONTAINED WITHIN
C                 AN ADF02 TYPE FILE. BEAM/THERMAL MAXWELL
C                 AVERAGED RATES ARE OBTAINED VIA THE
C                 ROUTINE QHE.F
C
C
C
C       NOTE    : IN THE CONTEXT OF ION  IMPACT EXCITATION,
C                 ISUPE IS THE INDEX REFERENCING THE LOWER
C                 LEVEL AND ISUPE11 REFERENCES THE UPPER LEVEL.
C                 THE BEAM ATOM IS EXCITED FROM STATE ISUPE TO
C                 ISUPHE11. EXCITATION AND DE-EXCITATION RATES
C                 ARE SUPPLIED SEPARATELY.
C
C
C       MODIFIED: HARVEY ANDERSON
C                 UNIVERSITY OF STRATHCLYDE
C                 28/1/97
C
C-----------------------------------------------------------------------

           IF(ILOW.GT.0.AND.ISUP1.GT.0.AND.ISUP1.LE.NDLOW.AND.
     &        ISUP.GT.0.AND.ISUP.LE.NDLOW.AND.IVDISP.EQ.1) THEN
               IF(DENSP.GT.0.0D0)THEN
                   IF(LXTBP(ISUP1,ISUP,ISYS).EQ.1)THEN
                       PYPU=XTBP(ISUP1,ISUP,ISYS)/
     &                      (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                      EXPTE(I)/EXPTE(I1))
                       PYPD=XTBP(ISUP,ISUP1,ISYS)/
     &                      (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                      (WI/WJ))
                   ENDIF
               ENDIF
               IF(DENIMP.GT.0.0D0)THEN
                   IF(LXTBZ(ISUP1,ISUP,ISYS).EQ.1)THEN
                       PYIMPU=XTBZ(ISUP1,ISUP,ISYS)/
     &                        (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                        EXPTE(I)/EXPTE(I1))
                       PYIMPD=XTBZ(ISUP,ISUP1,ISYS)/
     &                        (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                        (WI/WJ))
                   ENDIF
               ENDIF
           ENDIF
       ENDIF


C-----------------------------------------------------------------------
C	INSERTED TEMPORARY STATEMENTS TO SUPRESS ION IMPACT
C	EXCITATION.
C	H. ANDERSON 26/10/99
C
C
C 	PYPD = 0.0
C 	PYPU = 0.0
C 	PYUVAL = 0.0
C 	PYDVAL = 0.0
C       PYIMPD = 0.0
C 	PYIMPU = 0.0
C-----------------------------------------------------------------------

       PYU=PYU*RHO+PYPU*RHOP+PYIMPU*RHOIMP
       PYD=PYD*RHO+PYPD*RHOP+PYIMPD*RHOIMP

C-----------------------------------------------------------------------

       IF(YS.LE.0.001D+00)THEN
           A2=W/(YS+0.5D+00*YS*YS)
       ELSE
           IF(EXPTS(I1).LE.0.0D0)THEN
               IF(YS.GE.CUT)THEN
                   A2=0.0D+00
               ELSE
                   A2=W/(DEXP(YS)-1.0D+00)
               ENDIF
           ELSE
               A2=W/(EXPTS(I1)/EXS-1.0D+00)
           ENDIF
       ENDIF


C       modified A3 to become density
C       dependent.
C       Harvey Anderson 15/11/97
C       -changed PY to PYD

       A3=D*D*D*PYD

       A(I1)=-A1*A2*EXPTE(I1)
       CA(I1)=-A1*A3*EXE

       AC1=AC1+ZZ*A1*EXE/D
       A0=A0+A1*(1.0D+00+A2)*EXE
       CA0LD=CA0
       CA0=CA0+A1*A3*EXE
       IF(ISWCH.EQ.1)THEN
           CA0=CA0LD
           CA(I1)=0.0D+00
       ENDIF

       RH=RH-A1*(EXE+A2*(EXE-EXPTE(I1)))

C-----------------------------------------------------------------------
C   FETCH SPECIAL LOW LEVEL DATA VIA ROUTINES GHNLV AND GHNLE
C-----------------------------------------------------------------------

  155  CONTINUE
       IF(ISWCH.EQ.1)THEN
           IF(Z0.EQ.Z1)ZEFF=Z0
           IF(Z0.EQ.Z1+1.0D+00)ZEFF=Z1
           IF(Z0.EQ.Z1+2.0D+00.AND.N1.EQ.2)ZEFF=Z0-1.4D+00
           IF(Z0.EQ.Z1+2.0D+00.AND.N1.NE.2)ZEFF=Z1
           TEV=TE/(1.16054D+04)
           IF(Z0.NE.Z1+1.0D+00)THEN
               CALL GHNLV(Z0,Z1,ZEFF,N1,L1,N,NL2,TEV,GAMA,GAMTOT)
           ELSE
               CALL GHNLE(Z0,Z1,ZEFF,N1,L1,MULTN1,N,MULTN,
     &                    NL2,TEV,GAMA,GAMTOT)
           ENDIF

           IF(N.LE.NL2)THEN
               IND=L+1
               CONST=1.7578125D-02*Z1*Z1/(XSP*(2.0D+00*LP+1.0D+00))
               CA(I1)=-CONST*RHO*GAMA(IND)*EXE
               CA0=CA0-CA(I1)
               GAMA(IND)=0.0D+00
           ELSE
               CONST=1.7578125D-02*Z1*Z1
               CA(I1)=-CONST*RHO*GAMTOT*EXE
               CA0=CA0-CA(I1)
           ENDIF
       ENDIF
  157  CONTINUE
C-----------------------------------------------------------------------
C   CALCULATE NEARLY DEGENERATE COLLISION RATES HERE
C      N.B. ONLY IF IL<I OR IU>I AND IDEGRA(I1)=1
C-----------------------------------------------------------------------
  241  CONTINUE
       IF(IL.EQ.IU)RETURN

       ARL(IR)=A0+CA0
       ISARL(IR)=1
       DO 197 I1=IL,IU
        IF(I1.EQ.I)GO TO 197

        D=ENL2(I)-ENL2(I1)
        D=D*D*Z1**4
        K1=KPF(I1)
        L1=IAND(K1,MASK2)
        L1=ISHFR(L1,10)
        LT1=IAND(K1,MASK3)
        ELLT1=2*LT1+1
        IF(L.EQ.L1-1)GO TO 251
        IF(L.NE.L1+1)GO TO 197
  251   IF(IABS(LT1-LT).GT.1)GO TO 197
        AL=ARL(IR)*ELLP/(ELLT*EXE*WT)
        DO 270 JR=1,IMAX
         IF(I1.EQ.NLREP(JR))THEN
             IF(ISARL(JR).LE.0)GO TO 271
             AL=ARL(JR)*ELLP/(ELLT1*EXE*WT)
             GO TO 271
         ENDIF
  270   CONTINUE
  271  XL1=L1
       XLT1=LT1
       XLG=AMAX0(L,L1)
       A1=WIG6J(XL,XL1,1.0D0,XLT1,XLT,XLP)
       A1WIG=A1
       A1=0.105469D+00*WT*A1*A1*XLG*ELLT*ELLT1*EN2*(EN2-XLG*XLG)/ELLP
       DNL=6.0D+00*EN2*(EN2-XLG*XLG-XLG-1.0D+00)
       RS1=3.59485D-10*TE/(Z1**8*AL*AL)
       RS2=47.86301D+00*TE/DENS
       IF(D.LE.1.0D-14)THEN
           RS3=RS1
       ELSE
           RS3=5.0474D-22*TE/D
       ENDIF
       U=ZZ*TE*DMIN1(RS1,RS2,RS3)/DNL
       UE=U
       A3=RHO*(26.57D+00+DLOG(U))
       IF(A3.LT.0.00D+00)A3=0.00D+00
       IF(RHOP.LE.0.0D+00)THEN
           UP=0.0D+00
       ELSE
           RS1=RS1*TP/(TE*EM)
           RS3=RS3*TP/(TE*EM)
           U=ZZ*TP*DMIN1(RS1,RS2,RS3)/((ZP*ZP)*DNL*EM)
           UP=U
           A3INT=0.00D+00
           A3INT=RHOP*(ZP*ZP)*DSQRT(EM)*(26.57D+00+DLOG(U))
           IF(A3INT.LT.0.00D+00)A3INT=0.00D+00
           A3=A3+A3INT
        ENDIF
       CA(I1)=-A1*A3*EXE
       CAOLD=CA(I1)
       IF(IIWCH.EQ.1)THEN
           CA(I1)=0.00D+00
       ELSE
           CA0=CA0+A1*A3*EXE
C-----------------------------------------------------------------------
C   RNDEGV SUBROUTINE ACTIVATED HERE
C-----------------------------------------------------------------------
       ENDIF

       IF(IIWCH.EQ.1)THEN
           IF(Z0.EQ.Z1)ZEFF1=Z0
           IF(Z0.EQ.Z1)ZEFF2=Z0
           IF(Z0.EQ.Z1)N0CALL=1
           IF(Z0.EQ.Z1+1.0D+00)ZEFF1=Z0
           IF(Z0.EQ.Z1+1.0D+00)ZEFF2=Z0-1.0D+00
           IF(Z0.EQ.Z1+1.0D+00)N0CALL=1
           IF(Z0.EQ.Z1+2.0D+00)ZEFF1=Z0
           IF(Z0.EQ.Z1+2.0D+00)ZEFF2=Z0-2.0D+00
           IF(Z0.EQ.Z1+2.0D+00)N0CALL=2
           TEV=TE/1.16054D+04
           TPV=TP/1.16054D+04
           ZCALL=Z1-1.0D+00
           EICALL=E*Z1*Z1
           EJCALL=ENL2(I1)*Z1*Z1
           WICALL=XS*(2.0D+00*L+1.0D+00)
           WJCALL=XS*(2.0D+00*L1+1.0D+00)
           IISP=XSP
           IIS=XS
           IF(L1.GE.L)THEN
               CALL RNDEGV(Z0,ZCALL,ZEFF1,ZEFF2,N,L,LT,EICALL,WICALL,
     &                     N,L1,LT1,EJCALL,WJCALL,IIS,IISP,LP,N0CALL,
     &                     TEV,DENS,TPV,ZP,EMP,GAE,GAP)
               EXPON=EXE
           ELSE
               CALL RNDEGV(Z0,ZCALL,ZEFF1,ZEFF2,N,L1,LT1,EJCALL,WJCALL,
     &                     N,L,LT,EICALL,WICALL,IIS,IISP,LP,N0CALL,
     &                     TEV,DENS,TPV,ZP,EMP,GAE,GAP)
               EXPON=EXPTE(I1)
           ENDIF
           CONST=1.7578125D-02*Z1*Z1/(XSP*(2.0D+00*LP+1.0D+00))
           RHOO=RHO*GAE
           IF(RHOP.GT.0.0D+00)THEN
               RHOO=RHOO+RHOP*GAP
           ENDIF
           CA(I1)=-CONST*RHOO*EXPON
           CA0=CA0-CA(I1)
       ENDIF
  197  CONTINUE
       RETURN
C-----------------------------------------------------------------------
C   COMPUTE SPIN SYSTEM CHANGING CROSS-SECTIONS
C-----------------------------------------------------------------------
  156  CONTINUE

       IF(N.NE.NVV)THEN
           VV=N
           IL=MAX0(1,N-NDEL)
           IU=MIN0(NL3,N+NDEL)
           DO 393 I1=IL,IU
            V1=I1
            VU=DMAX1(VV,V1)
            VL=DMIN1(VV,V1)
            CALL COLEXC(ATE,VL,VU,AI)
            CEXA(I1)=AI
  393      CONTINUE
           NVV=N
       ENDIF

C       GIVEN REPRESENTATIVE LEVEL ONE IS COLLECTING ALL
C       THE CONTRIBUTIONS RESULTING FROM THE LOSS FROM
C       THE SINGLETS TO THE TRIPLETS.

        DO 350 I1=NMIN,NMAX
           A(I1)=0.0D+00
           CA(I1)=0.0D+00
           FP1=1.0D+00
           K1=KPF(I1)
           EX1=EXPTE(I1)
           N1=ISHFR(K1,20)


C-----------------------------------------------------------------------
C
C       MAPPING LOW LEVEL DATA INTO THE CORRECT ELEMENTS OF THE
C       ARRAYS WHICH ARE USED TO CONSTRUCT THE C-R MATRIX.
C
C
C       IXREF() : CONTAINS THE INDEX LEVELS FOR THE SINGLETS IN
C                 THE ADF04 TYPE FILE
C       IXREFS(): CONTAINS THE INDEX LEVELS FOR THE TRIPLETS IN
C                 THE ADF04 TYPE FILE.
C
C
C       NOTE :  THE SPIN CHANGING CONTRIBUTION IS FIRST CALULATED
C               FOR THE TRIPLETS CONTRIBUTING TO THE POPULATION
C               STRUCTURE OF THE SINGLETS. THE FIRST CALL TO
C               CCNST7 IS FOR THE SINGLET POPULATION STRUCTURE
C               FOR WHICH ISG.EQ.1 . IN THIS CASE ISUPE REFERENCES
C               THE SINGLETS IN THE ADFO4 FILE AND ISUPE11
C               REFERENCES THE TRIPLETS. THE CODE BELOW LOOPS
C               AROUND EACH REPRESENTATIVE LEVEL GATHERING THE
C               CONTRIBUTIONS FROM THE LEVELS OF THE OTHER
C               SPIN SYSTEM I.E. IF ROUTINE IS SETTING UP THE
C               C-R MATRIX FOR THE SINGLETS, THE SPIN CHANGING
C               CONTRIBUTING FROM THE TRIPLETS TO THE
C               SINGLETS IS EVALUATED AND VICE VERSA. FOR THE
C               CASE OF SETTING UP THE C-R MATRIX FOR THE TRIPLETS I.E
C               ISG.EQ.3 THEN ISUPE REFERENCES THE TRIPLETS AND ISUPE11
C               REFERENCES THE SINGLETS CONTAINED IN THE ADF04 TYPE FILE
C
C
C               WHEN ISG .EQ. 1 THEN THE ARRAY EXPTE() CONTAINS
C               exp(i/k*Te) FOR THE SINGLETS AND THE ARRAY
C               EXMTE() CONTAINS exp(i/k*Te) FOR THE TRIPLETS.
C
C               WHEN ISG .EQ. 3 THEN THE ARRAY EXPTE() CONTAINS
C               exp(i/k*Te) FOR THE TRIPLETS AND THE ARRAY
C               EXMTE() CONTAINS exp(i/k*Te) FOR THE SINGLETS.
C
C       HARVEY ANDERSON
C       31/3/98
C
C-----------------------------------------------------------------------

           IF(ILOW.EQ.1.AND.ITYP1.EQ.1.D+00) THEN
               IF( ISG.EQ.1 ) THEN
                   ISUPE11=IXREFS(I1)
               ELSE
                   ISUPE11=IXREF(I1)
               ENDIF
           ENDIF

        IF(NDEL.GT.IABS(N-N1))THEN
            NU=MAX0(N,N1)
            NL=MIN0(N,N1)
            X=NL*NL
            EXU=DMIN1(EXE,EX1)
            AI=CEXA(N1)
            IF(N1.GT.NL2)THEN
                WS1=XS1/XSP
                OVL=1.0D+00
                IF(IS.GT.0)OVL=OVL*X
            ELSE
                L1=IAND(K1,MASK2)
                L1=ISHFR(L1,10)
                OVL=1.0D+00
                IF(IS.LE.0)THEN
                    IF(N.LT.N1)THEN
                        LL=L
                        LU=L1
                    ELSEIF(N.EQ.N1)THEN
                        LL=MIN0(L,L1)
                        LU=MAX0(L,L1)
                    ELSE
                        LL=L1
                        LU=L
                    ENDIF

                    CALL OVLP(NL,LL,NU,LU,2,NL2,OVL)
                    LM=MIN0(L,L1)
                    IF(LHCT.GT.LM)THEN
                        V1=ENL(I1)
                        VU=DMAX1(V,V1)
                        VL=DMIN1(V,V1)
                       CALL COLEXC(ATE,VL,VU,AI)
                    ENDIF
                ENDIF
                LT1=IAND(K1,MASK3)
                ELLT1=2*LT1+1
                WS1=(ELLT1*XS1)/(ELLP*XSP)
                IF(N1.EQ.NG1.AND.L1.EQ.LG1)THEN
                    DO 387 J=1,NGL1
                     IF(LT1.EQ.LTG1(J))THEN
                        FP1=FPG1(J)
                     ENDIF
  387               CONTINUE
                ENDIF
            ENDIF
            A1=0.0703125D+00*FP*FP1*RHO*WS*WS1*AI*OVL*EXU

C
C       SET A1 TO ZERO TO ALLOW COMPARISON WITH
C       ADAS208. A1 CONTAINS FUNDAMENTAL DATA OBRAINED FROM
C       OVERLAP INTEGRAL AND IS USED WERE THERE IS NO
C       FUNDAMENTAL DATA. ADAS208 DOES NOT HAVE THIS
C       ABILITY.
C
C       HARVEY ANDERSON
C       16/9/98
C
C           A1=0.0
C
C       REMOVED TEMPORARY LINE WHICH SET A1=0.0
C       H.A. 19/9/98
C
C       CHANGE LINE  WTF1=0.5*WT*(ELLT1/ELLP) TO
C       WTF1=WT*(ELLT1/ELLP)
C
C       HARVEY ANDERSON 10/9/98
C

            WTF1=0.5D+00*(ISG1/XSP)*(ELLT1/ELLP)

C-----------------------------------------------------------------------
C
C       ********        SPIN CHANGING TRANSITIONS       ********
C                       --------------------------
C
C       SUBSTITUTION OF LOW LEVEL DATA : XTBE(INDEX1,INDEX2)
C
C
C       XTBE(,) : ARRAY CONTAINING ELECTRON IMPACT EXCITATION
C                 RATE COEFFICIENTS.THIS ARRAY IS ASSIGNED
C                 ITS VALUE IN THE ROUTINE SUPPHE1.F. THE
C                 RATE COEFFICIENTS ARE OBTAINED BY DIRECTLY
C                 ACCESSING CROSS-SECTIONS CONTAINED WITHIN
C                 AN ADF04 TYPE FILE.
C
C       NOTE    : WHEN ISG.EQ.1 THEN ISUPE REFERENCES THE
C                 SINGLETS CONTAINED WITHIN THE ADF04 TYPE
C                 FILE AND ISUPE11 REFERENCES THE TRIPLETS.
C                 THEREFORE XTBE(ISUPE,ISUPE11) CONTAINS
C                 THE SPIN CHANGING RATE COEFFICIENT FOR
C                 THE PROCESS FROM A SINGLET TO A TRIPLET
C                 AND XTBE(ISUPE11,ISUPE) CONTAINS THE RATE
C                 FOR THE PROCESS FROM A TRIPLET TO A
C                 SINGLET.XTBE(ISUPE,ISUPE11) IS USED FOR DIAG.
C                 ELEMENTS OF C-R MATRIX AND XTBE(ISUPE11,ISUPE)
C                 IS USED FOR OFF DIAG. ELEMENTS OF THE C-R
C                 MATRIX WHICH REPRESENTS THE CONTRIBUTION
C                 TO THE SINGLETS FROM THE TRIPLETS.
C
C                 WHEN ISG.EQ.3 THEN ISUPE REFERENCES THE
C                 THE TRIPLETS AND ISUPE11 REFERENCES THE
C                 SINGLETS CONTAINED WITHIN THE ADF04 TYPE
C                 FILE. XTBE(ISUPE,ISUPE11) IS USED FOR DIAG
C                 ELEMENTS AND XTBE(ISUPE11,ISUPE) IS USED FOR
C                 THE OFF DIAG ELEMENTS.
C
C                 IF IS.EQ.0 NL RESOLVED BOUND BOUND GAUNT
C                 FACTORS ARE EVALUATED USING RADIAL
C                 WAVEFUNCTIONS. IF IS.EQ.1 BUNDLED-N BOUND
C                 BOUND GAUNT FACTORS ARE EVALUATED USING
C                 AN ANALYTICAL FIT.
C
C       MODIFIED: HARVEY ANDERSON
C                 UNIVERSITY OF STRATHCLYDE
C                 28/1/97
C
C
C-----------------------------------------------------------------------
C
C       SUBSTITUTING FUNDAMENTAL DATA VIA RATE COEFFICIENTS
C       FOR THE CONTRIBUTION TO THE SINGLETS FROM THE
C       TRIPLETS ( OFF DIAG ELEMENTS ) ALSO INCLUDED IS THE LOSS
C       FROM THE SINGLETS TO THE TRIPLETS ( DIAG ELEMENTS ).
C
C-----------------------------------------------------------------------

           IF ( ISG.EQ.1.AND.ISUPE.GT.0.AND.ISUPE11.GT.0 ) THEN
               IF ( LXTBE(ISUPE,ISUPE11).EQ.1) THEN

C-----------------------------------------------------------------------
C
C             IF ISUPE11 IS .LT. THAN ISUPE THE TRANSITION
C             FROM THE TRIPLET TO SINGLET IS EXCITATION
C             PROCESS. IF THIS IS NOT THE CASE THEN THE
C             TRANSITION IS AN EXCITATION PROCESSES
C
C       N.B   EXE CONTAINS THE VALUE OF exp(i/k*te) FOR THE
C             SINGLETS CORRESPONDING TO LEVEL I=NLREP(IR).
C             ETE CONTAINS THE VALUE OF exp(i/k*Te)
C
C
C                isupe11(triplet)---->isupe(Singlet)
C
C-----------------------------------------------------------------------


C
C       IF ISG.EQ.1 .AND. ISUPE11 .GT. ISUPE THEN
C          WTF1 SHOULD BE STATISTICAL WEIGHT OF TRIPLETS
C          WTF  SHOULD BE STATISTICAL WEIGHT OF SINGLETS
C       ENDIF

                IF ( ISUPE11 .GT. ISUPE ) THEN
                      TSMIX = (CONST2*RHO*WTF1*EXMTE(I1) /
     &                        DSQRT(ATE))*XTBE(ISUPE11,ISUPE)
                ELSE
                      TSMIX = (CONST2*RHO*WTF*EXE /
     &                        DSQRT(ATE))*XTBE(ISUPE,ISUPE11)
                ENDIF

C-----------------------------------------------------------------------
C
C             IF ISUPE IS .GT. THAN ISUPE11 THE TRANSITION
C             FROM THE SINGLET TO TRIPLET IS DE-EXCITATION
C             PROCESS. IF THIS IS NOT THE CASE THEN THE
C             TRANSITION IS AN EXCITATION PROCESSES
C
C               isupe(singlet)--->isupe11(triplet)
C
C
C-----------------------------------------------------------------------
C
C       NOTE : VALIDATED CONTRIBUTION FROM THE DIAGONAL ELEMENTS
C              OF THE SINGLETS TO THE TRIPLETS
C              HARVEY ANDERSON 16/9/98
C-----------------------------------------------------------------------


                IF ( ISUPE .GT. ISUPE11 ) THEN
                    STMIX = (CONST2*RHO*WTF*EXE /
     &                      DSQRT(ATE))*XTBE(ISUPE,ISUPE11)

                ELSE
                    STMIX = (CONST2*RHO*WTF*EXE /
     &                      DSQRT(ATE))*XTBE(ISUPE,ISUPE11)
                ENDIF

               ENDIF
           ENDIF


C-----------------------------------------------------------------------
C
C       SUBSTITUTING FUNDAMENTAL DATA VIA RATE COEFFICIENTS
C       FOR THE CONTRIBUTION TO THE TRIPLETS FROM THE
C       SINGLETS ( OFF DIAG ELEMENTS )ALSO INCLUDED IS THE LOSS
C       FROM THE TRIPLETS TO THE SINGLETS ( DIAG ELEMENTS ).
C
C-----------------------------------------------------------------------


           IF ( ISG.EQ.3.AND.ISUPE.GT.0.AND.ISUPE11.GT.0 ) THEN
               IF ( LXTBE(ISUPE,ISUPE11).EQ.1) THEN

C-----------------------------------------------------------------------
C
C              isupe11(singlet)--->isupe(triplet)
C
C-----------------------------------------------------------------------

                IF ( ISUPE11 .GT. ISUPE ) THEN
                      TSMIX = (CONST2*RHO*WTF1*EXMTE(I1) /
     &                        DSQRT(ATE))*XTBE(ISUPE11,ISUPE)
                ELSE
                      TSMIX = (CONST2*RHO*WTF*EXE /
     &                        DSQRT(ATE))*XTBE(ISUPE,ISUPE11)
                ENDIF

C-----------------------------------------------------------------------
C
C             IF ISUPE IS .GT. THAN ISUPE11 THE TRANSITION
C             FROM THE TRIPLET TO SINGLET IS  A DE-EXCITATION
C             PROCESS. IF THIS IS NOT THE CASE THEN THE
C             TRANSITION IS AN EXCITATION PROCESSES
C
C
C              isupe(triplet)--->isupe11(singlet)
C
C
C-----------------------------------------------------------------------

                IF ( ISUPE .GT. ISUPE11 ) THEN

                    STMIX = (CONST2*RHO*WTF*EXE /
     &                      DSQRT(ATE))*XTBE(ISUPE,ISUPE11)

                ELSE
                    STMIX = (CONST2*RHO*WTF*EXE /
     &                      DSQRT(ATE))*XTBE(ISUPE,ISUPE11)


                ENDIF

              ENDIF
           ENDIF


      IF (ISUPE.GT.0.AND.ISUPE11.GT.0) THEN
          IF ( ILOW.EQ.1.AND.ITYP1.EQ.1.AND.LXTBE(ISUPE,ISUPE11).EQ.1
     &        .AND.ISUPE.GT.0.AND.ISUPE11.GT.0 ) THEN
                CA0=CA0
                CA0=CA0+STMIX
                CA(I1)=-TSMIX

          ELSE
                CA0=CA0
                CA0=CA0+A1
                CA(I1)=-A1
        ENDIF
       ELSE
           CA0=CA0
           CA0=CA0+A1
           CA(I1)=-A1
       ENDIF

C
C       CONVERSION FOR DE-EXCITATION RATES
C
          IF ( ISG.EQ.1 ) THEN
              IF ( ISUPE11 .GT. ISUPE ) THEN
                  tconv = (1.0D+00/const1)/(EXMTE(I1)*WTF1)
                ELSE
                  tconv = (1.0D+00/const1)/(EXE*WTF)
              ENDIF
          ENDIF

         IF ( ISG.EQ.3 ) THEN
             IF ( ISUPE .LT. ISUPE11 ) THEN
               tconv = (1.0D+00/const1)/(EXMTE(I1)*WTF1)
               ELSE
               tconv = (1.0D+00/const1)/(EXE*WTF)
             ENDIF
         ENDIF


C-----------------------------------------------------------------------
C
C
C       RETAIN WRITE STATEMENT FOR DIAGNOSTIC PURPOSES
C
C       HARVEY ANDERSON
C       1/4/98
C
C       WRITE(23,122) I,I1,ISUPE,ISUPE11,
C     & XTBE(ISUPE11,ISUPE) , CA(I1) , ca(i1)*tconv
C-----------------------------------------------------------------------

        ENDIF

  350  CONTINUE
       RETURN

      END
