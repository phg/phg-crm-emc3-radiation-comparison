CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas311/bdcf32.for,v 1.2 2004/07/06 11:37:24 whitefor Exp $ Date $Date: 2004/07/06 11:37:24 $
CX
       SUBROUTINE BDCF32(F,E,ELL1,Z,X)
       IMPLICIT REAL*8(A-H,O-Z)
      B=DSQRT(-E)
      GNU=-Z/B
      T=2.0*X*B
      R=1.0/T
      T3=GNU*(GNU+1.0)-ELL1
       S=1.0
       A=1.0
       K0=GNU+GNU+1.5+T
       TH=K0
       TH=GNU+GNU+1.0+T-TH
       DO 1 K=1,K0
       TK=K
      T1=GNU-TK
      A=A*(ELL1-T1*(T1+1.0))*R/TK
       T1=DABS(A)
       T2=DABS(S)
       IF(T2-T1*1.0E7)1,1,2
    1  S=S+A
       C=-0.5+0.125*R*((2.0*TH-1.0)+(TH*TH-1.5*TH+0.25-2.0*T3)*R)
       S=S+C*A
    2  F=(T**GNU)*S*DEXP(-0.5*T)
      T3=DABS(T3)
      IF(T*T-T3)3,3,4
3     X1=DSQRT(T3)/(2.0*B)
       WRITE(6,100)E,ELL1,Z,X,X1
4     RETURN
100   FORMAT('0INACCURACY IN BDCF32 FOR E=',1PE10.2,3X,'ELL1=',
     11PE10.2,3X,'Z=',1PE10.2,3X,'X=',1PE10.2,5X,'X SHOULD BE AT LEAST',
     21PE10.2)
      END
