CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas311/gbbr.for,v 1.2 2004/07/06 13:58:12 whitefor Exp $ Date $Date: 2004/07/06 13:58:12 $
CX
       FUNCTION GBBR(N1,L1,LT1,E1,V1,N2,L2,LT2,E2,V2,IOPT,C1,C2,C3,SCL)
       IMPLICIT REAL*8(A-H,O-Z)
       DIMENSION E(2),L(2),N(2),D(2),GE(2)
       DIMENSION C1(5,5,3),C2(5,5,3),C3(5,5,3)
       COMMON /ATSP/Z0,Z1,ALF,LP,ISP,AMSZ0,AMSHYD
       COMMON /ACCSET/RX3,DPT,GE,EHCT,MN,NHCT,LHCT,NIP,NEX,IPRT,NDEL
C  IOPT=0, THEN CALCULATE G(N1,L1,LT1;N2,L2,LT2)
C  IOPT=1, THEN CALCULATE G(N1,L1;N2,L2) BASED ON ENERGIES OF LEVELS
C  N1,L1,LT1 AND AVERAGED N2,L2,LT2 OVER ALLOWED LT2.
C  E1 AND E2 ARE ENERGIES/(Z1*Z1); V1 AND V2 ARE EFFECTIVE PRINCIPAL
C  QUANTUM NUMBERS.
C  REQUIRE N1 LESS THAN OR EQUAL TO N2.
       XLP=LP
       ZZ=Z1*Z1
       E4=E2
       V4=V2
       GBBR=0.0
       E(1)=E1*ZZ
       N(1)=N1
       L(1)=L1
       XL1=L1
       E(2)=E2*ZZ
       N(2)=N2
       L(2)=L2
       XL2=L2
       W=1.0
       LM=MIN0(L1,L2)
       IF(IOPT)3,3,5
    3  IF(IABS(L1-L2).NE.1)GO TO 20
       XLT1=LT1
       XLT2=LT2
       W1=WIG6J(XL1,XL2,1.0D0,XLT2,XLT1,XLP)
       W1=W1*W1
       W=W1*(2.0*XLT1+1.0)*(2.0*XLT2+1.0)/(2.0*XLP+1.0)
       GO TO 6
    5  IF(L2-L1+1)20,1,2
    1  I=1
       GO TO 4
    2  IF(L2-L1-1)20,7,20
    7  I=3
    4  IF(LHCT.LE.LM)GO TO 6
       XN2=N2
       LL1=L1+1
       LLT1=LT1+1-IABS(LP-L1)
       T=5.0D-6*E2
    8  E3=E2
       U=C1(LL1,LLT1,I)+E3*(C2(LL1,LLT1,I)+E3*C3(LL1,LLT1,I))
       E2=1.0/(XN2-U)
       E2=E2*E2
       IF(DABS(E2-E3)-T)9,9,8
    9  V2=XN2-U
       E(2)=E2*ZZ
    6  W1=E1-E2
       W2=AMAX0(L1,L2)
       W=0.17004369*W*W1**4*W2*(V1*V2)**3
       D(1)=0.0
       D(2)=0.0
       IF(W.LE.1.0D-28.OR.W1.LE.1.0D-6)GO TO 20
       IF(NHCT.LE.MIN0(N1,N2))GO TO 10
       IF(LHCT.LE.LM)GO TO 10
       IF(IABS(N1-N2).GT.NDEL)GO TO 10
       NS4=MAX0(N1,N2)
       NS5=DPT
       NS4=NS5*NS4*NS4
       CALL RAD2(E,N,L,D,NS4,ANS)
       GBBR=W*ANS*ZZ
       IF(IABS(N1-N2).LT.NDEL)GO TO 20
       SCL=ZZ*ANS/RD2BS(N1,L1,N2,L2)
   20  E2=E4
       V2=V4
       RETURN
   10  IF(N1-N2)11,12,13
   11  GBBR=W*SCL*RD2BS(N1,L1,N2,L2)
       GO TO 20
   12  GBBR=2.25*W*SCL*V1*V2*(V1*V2-W2*W2)
       GO TO 20
   13  GBBR=W*SCL*RD2BS(N2,L2,N1,L1)
       GO TO 20
      END
