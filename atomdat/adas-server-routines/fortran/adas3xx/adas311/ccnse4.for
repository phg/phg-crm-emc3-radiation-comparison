CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas311/ccnse4.for,v 1.3 2004/07/06 12:01:06 whitefor Exp $ Date $Date: 2004/07/06 12:01:06 $
CX
       SUBROUTINE CCNSE4(A0,CA0,A,CA,RH,CRH,NMIN,NMAX,ARED,CARED,RHS,
     1CRHS,IR,ISH,JSH,KPF,NLIV,ILTXR)
C
C-----------------------------------------------------------------------
C
C	********** FORTRAN 77 ROUTINE : CCNSE4.F  **********
C
C	PURPOSE : APPLIES MATRIX CONDENSATION TREATEMENT TO
C		  THE ARRAYS WHICH ARE USED TO CONSTRUCT
C		  THE COLLISIONAL RADIATIVE MATRIX.
C
C	HISTORY : ROUTINE WAS ORIGINALLY WRITTEN BY H.P.SUMMERS.
C
C
C	INPUT   :
C
C	(R*8)	A0	:
C	(R*8)	CAO	:
C	(R*8)	A	:
C	(R*8)	CA	:
C	(R*8)	RH	:
C	(R*8)	CRH	:
C	(I*4)   IR	:  INDEX CORRESPONDING TO THE REPRESENTATIVE
C			   LEVEL OF INTEREST.
C	(I*4)   IRS	:  SWITCH USED TO LOCATE THE DIAGONAL ELEMENTS
C			   OF THE COLLISIONAL RADIATIVE MATRIX. THIS
C			   ROUTINE DOES NOT CONTAIN THE C-R MATRIX BUT
C			   ARRAYS WHICH ARE USED TO ASSEMBLE IT, FOR
C			   A SINGLE SPIN SYSTEM WITH NO METASTABLES
C			   IRS=0 .
C
C
C
C	CONTACT : HARVEY ANDERSON
C		  UNIVERSITY OF STRATHCLYDE
C		  ANDERSON@PHYS.STRATH.AC.UK
C
C	DATE	: 26/02/98
C
C VERSION:      1.2                                     DATE:   21-10-99
C MODIFIED: RICHARD MARTIN
C               CHANGED HEXADECIMAL CONSTANTS TO Z'FFF00000' FORM.
C
C-----------------------------------------------------------------------
C
	IMPLICIT REAL*8(A-H,O-Z)
C
       DIMENSION A(1000),ARED(160),KPF(1000),NREP(31),NLIV(31)
       DIMENSION CA(1000),CARED(160)
       DIMENSION LXR(10),LTXR(10,10),ILTXR(10,10,5),INTER(300)
       DIMENSION AGRL(300,3),RLR(10,10),RLTR(10,10,5)
       DIMENSION CL1(10,10,9),CL2(10,10,9),CL3(10,10,9),CL4(10,10,9)
       DIMENSION CLT1(10,10,3,2),CLT2(10,10,3,2),CLT3(10,10,3,2)
       COMMON /ATSP/Z0,Z1,ALF,LP,ISP,AMSZ0,AMSHYD
       COMMON /INTSP/AGRL,CL1,CL2,CL3,CL4,CLT1,CLT2,CLT3,RLR,RLTR,
     1LXR,LTXR,INTER,INREP,NREP,NL1,NL2,NL3
       DATA MASK1,MASK2,MASK3,MASK4/Z'FFF00000',Z'000FFC00',Z'000003FF',
     1Z'FFFFFC00'/
       IRS=IR+ISH
       ARED(IRS)=ARED(IRS)+A0
       CARED(IRS)=CARED(IRS)+CA0
       RHS=RHS+RH
       CRHS=CRHS+CRH
       DO 240 I=NMIN,NMAX
       I0=KPF(I)
       N=ISHFR(I0,20)
       NRL=INTER(N)
       NRU=MIN0(NRL+2,INREP)
       DO 235 NR=NRL,NRU
       A1=AGRL(N,NR-NRL+1)
       CA1=A1
       IF(DABS(A1).LE.1.0D-10)GO TO 235
       CONTINUE
       CONTINUE
       CONTINUE
       A1=A1*A(I)
       CA1=CA1*CA(I)
c       write(6,*) 'The value of CA(i)', CA(I) , I   , ca1
       IF(NREP(NR)-NL2)202,202,201
  201  JR=NLIV(NR)
       JRS=JR+JSH
c       write(6,*) 'CARED(JRS) BEFORE ', CARED(JRS)  , JRS
       ARED(JRS)=ARED(JRS)+A1
       CARED(JRS)=CARED(JRS)+CA1
       GO TO 235
  202  L=IAND(I0,MASK2)
       L=ISHFR(L,10)
       LT=IAND(I0,MASK3)
       XL=MAX0(N-1,1)
       XL=DFLOAT(L)/XL
       LLP=IABS(L-LP)
       XLT=L+LP-LLP
       XLT=DMAX1(XLT,1.0D0)
       XLT=DFLOAT(LT-LLP)/XLT
       LU=LXR(NR)
       IF(LU.GT.1)GO TO 207
       XL=0.0
       LN=1
       GO TO 206
  207  DO 204 LL=2,LU
       IF(XL-RLR(NR,LL))203,204,204
  203  LN=LL-1
       GO TO 205
  204  CONTINUE
       LN=LU-1
  205  XL=XL-0.5*(RLR(NR,LN)+RLR(NR,LN+1))
  206  DO 230 LL=1,LU
       A2=CL1(NR,LL,LN)+XL*(CL2(NR,LL,LN)+XL*(CL3(NR,LL,LN)+XL*
     1CL4(NR,LL,LN)))
       CA2=A2
       IF(DABS(A2).LE.1.0D-10)GO TO 230
       CA2=CA1*CA2
       A2=A1*A2
       LTU=LTXR(NR,LL)
C
       IF(LTU.GT.1)GO TO 209
       XLT=0.0
       LTN=1
       GO TO 213
  209  DO 210 LLT=2,LTU
       IF(RLTR(NR,LL,LLT)-XLT)208,210,210
  208  LTN=LLT-1
       GO TO 212
  210  CONTINUE
       LTN=LTU-1
  212  XLT=XLT-0.5*(RLTR(NR,LL,LTN)+RLTR(NR,LL,LTN+1))
  213  DO 225 LLT=1,LTU
       A3=CLT1(NR,LL,LLT,LTN)+XLT*(CLT2(NR,LL,LLT,LTN)+XLT*CLT3(NR,LL,LL
     1T,LTN))
       CA3=A3
C
C	WHAT IS NR ?
C	WHAT IS LL ?
C	WHAT IS LLT ?
C       WHAT IS ILTXR ?
C
       IF(DABS(A3).LE.1.0D-10)GO TO 225
       JR=ILTXR(NR,LL,LLT)
       JRS=JR+JSH
c       write(6,*) JR , JSH , JRS
       ARED(JRS)=ARED(JRS)+A3*A2
       CARED(JRS)=CARED(JRS)+CA3*CA2
C
c      WRITE(6,*) 'CARED(JRS) AFTER MATRIX CONDENSATION   ',CARED(JRs) ,
c     &'     JRS =', JRS , '   I= ', I

  225  CONTINUE
  230  CONTINUE
  235  CONTINUE
  240  CONTINUE
       RETURN
      END
