CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas311/omgrc2.for,v 1.3 2007/05/17 17:04:12 allan Exp $ Date $Date: 2007/05/17 17:04:12 $
CX
       SUBROUTINE OMGRC2(EI,EJ,WI,WJ,X,OMEG,ISW,M,ATE,QI,QJ)
       IMPLICIT REAL*8(A-H,O-Z)
C-----------------------------------------------------------------------
C  PURPOSE: GIVEN A SET OF VALUES OF COLLISION STRENGTHS AND INCIDENT
C           ELECTRON ENERGIES(IN THRESHOLD UNITS), THE ROUTINE
C           EVALUATES THE COLLISIONAL EXCITATION AND DEEXCITATION RATE
C           COEFFICIENTS.
C
C   EXTRAPOLATION PROCEDURE MAY BE SELECTED TO BE CONSTANT (ISW=1),
C   LOGARITHMIC (ISW=2) OR EXPONENTIAL (ISW=3).
C   
C-----------------------------------------------------------------------
C VERSION  : 1.1
C DATE     : 18-03-1999
C MODIFIED : ???
C
C VERSION  : 1.2
C DATE     : 05-10-2000
C MODIFIED : ???
C             - Removed junk from columns > 72
C
C VERSION  : 1.3
C DATE     : 16-05-2007
C MODIFIED : Allan Whiteford
C             - Updated comments as part of subroutine documentation
C               procedure.
C
C-----------------------------------------------------------------------
       DIMENSION X(20),OMEG(20)
       EIJ=EI-EJ
       M1=M-1
       S=OMEG(1)
       IF(M1)10,10,1
    1  U1=EIJ*ATE*(X(1)-1.0D0)
       G1=1.0D0-U1
       DO 5 K=1,M1
       U2=EIJ*ATE*(X(K+1)-1.0D0)
       G2=DEXP(-U2)
       B=(OMEG(K+1)-OMEG(K))/(U2-U1)
       S=S+B*(G1-G2)
       U1=U2
    5  G1=G2
       IF(ISW-2)10,7,8
    7  S=S+OMEG(M)*G1*EEI(U1+EIJ*ATE)
       GO TO 10
    8  S=S-OMEG(M)*G1/(1.0D0+ATE)
   10  QJ=2.171609D-8*DSQRT(ATE)*S/WJ
       QI=QJ*WJ*DEXP(-ATE*EIJ)/WI
       RETURN
      END
