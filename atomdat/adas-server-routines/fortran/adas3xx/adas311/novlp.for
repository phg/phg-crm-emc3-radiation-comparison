CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas311/novlp.for,v 1.3 2005/02/22 19:14:20 mog Exp $ Date $Date: 2005/02/22 19:14:20 $
CX
C-------------------------------------------------------------------------
C	VERSION : 1.2
C	MODIFIED: Martin O'Mullane      
C       DATE    : 08-11-2004
C		  Alter dimensions from 200 to 500. 	
C
C-------------------------------------------------------------------------
       SUBROUTINE NOVLP(N,L,N1,L1,IOPT,NMAX,OVL)
       IMPLICIT REAL*8(A-H,O-Z)
       DIMENSION SOVL(20),A(500),B(500),C(500)
       COMMON /SOVLP/SOVL
C  ALTERATIONS TO PRODUCE COMMON PASSING OF SOVL FOR USE IN X-SECT
C  INVESTIGATION ONLY, 31 OCT 1984.  RENAMED TO NOVLP WITH ALTERED
C  STORAGE FOR SINGLE N,L,N1 OPERATION, WITH EXTENDED EXPLICIT
C  EVALUATION RANGE.
C  *********** H.P. SUMMERS, JET  17 JAN 1985  ******************
       M=MIN0(NMAX,20)
       GO TO (1,10),IOPT
    1  NN=N
       LL=L
       K1=0
       CALL RWFH(A,NA,IPA,UA,NN,LL)
       CALL PSPROD(A,NA,IPA,A,NA,IPA,B,NB,IPB)
       S=UA+UA
       NN1=N1
       SUM=0.0
       DO 5 J=1,NN1
       LL1=J-1
       W=2*LL1+1
       CALL RWFH(A,NA,IPA,UA,NN1,LL1)
       CALL PSPROD(A,NA,IPA,A,NA,IPA,C,NC,IPC)
       CALL PSPROD(B,NB,IPB,C,NC,IPC,A,NA,IPA)
       UA=UA+UA+S
       IPA=IPA-2
       CALL PSINT(A,NA,IPA,UA,C,NC,IPC,UC)
    5  SUM=SUM+W*C(1)
       SOVL(K1+NN1)=SUM
C      WRITE(6,100)NN,LL,(SOVL(K1+NN1),NN1=NN,M)
    9  RETURN
   10  IF(MAX0(N,N1).GT.M)GO TO 20
       CALL RWFH(A,NA,IPA,UA,N,L)
       CALL PSPROD(A,NA,IPA,A,NA,IPA,B,NB,IPB)
       S=UA+UA
       CALL RWFH(A,NA,IPA,UA,N1,L1)
       CALL PSPROD(A,NA,IPA,A,NA,IPA,C,NC,IPC)
       CALL PSPROD(B,NB,IPB,C,NC,IPC,A,NA,IPA)
       UA=UA+UA+S
       IPA=IPA-2
       CALL PSINT(A,NA,IPA,UA,C,NC,IPC,UC)
       K=N1
       OVL=C(1)/SOVL(K)
   15  RETURN
   20  CC=0.3
       G=2.0
       XN=N
       XL=L
       XN1=N1
       XL1=L1
       M=MIN0(N1-1,N+1)
       XM=M
       D=1.0
    2  BT=XN1*XN1/((XM+1.0)*(XM+1.0))
    4  X1=DMAX1(XL-XM,0.0D0)
       X2=DMAX1(XM-XL,0.0D0)
       XK=BT*(XM+1.0)*(XM+1.0)/(2.0*XN1*XN1*(-XL+DABS(XM-XL)+
     1(D+XL+0.5)*DLOG((D+XL)/(D+X1))-(D-XL-0.5)*DLOG((D+X2)/D)))
       IF(L1-M)16,16,17
   16  OVL=BT*(1.0-CC)/(XN1*XN1)+CC*XK/(DABS(XL1-XL)+D)
       GO TO 15
   17  OVL=BT*(2.0*XM+1.0)*DEXP(-G*(XL1-XM))/((2.0*XL1+1.0)*
     1XN1*XN1)
       GO TO 15
  100  FORMAT(1H ,2I5,10F10.5/10F10.5)
      END
