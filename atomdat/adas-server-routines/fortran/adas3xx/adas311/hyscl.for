CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas311/hyscl.for,v 1.3 2004/07/06 14:06:15 whitefor Exp $ Date $Date: 2004/07/06 14:06:15 $
CX
C
C VERSION:      1.2                                     DATE:   21-10-99
C MODIFIED: RICHARD MARTIN
C               CHANGED HEXADECIMAL CONSTANTS TO Z'FFF00000' FORM.
C
C
C---------------------------------------------------------------------
       SUBROUTINE HYSCL(NL2,NMIN,NMAX,N2S,KPF,KPB,ENL,ENL2,LP,C1,C2,C3,
     1SCLA)
       IMPLICIT REAL*8(A-H,O-Z)
C.......... FOR NON - CRAY RUNNING      11/22/85
C............ ANY CHANGES MADE HERE MUST BE MADE TO HYSCLCR FOR
C............ CRAY RUNNING.
       DIMENSION N2S(300),KPF(1000),KPB(1000),ENL(1000),ENL2(1000)
       DIMENSION C1(5,5,3),C2(5,5,3),C3(5,5,3),SCLA(240,8),GE(2)
       COMMON /ACCSET/RX3,DPT,GE,EHCT,MN,NHCT,LHCT,NIP,NEX,IPRT,NDEL
       DATA MASK1,MASK2,MASK3,MASK4/Z'FFF00000',Z'000FFC00',Z'000003FF',
     1Z'FFFFFC00'/
       DO 600 I=NMIN,NMAX
       K=KPF(I)
       N=ISHFR(K,20)
       IF(N.GT.NL2)GO TO 600
       L=IAND(K,MASK2)
       L=ISHFR(L,10)
       IF(LHCT.LT.L)GO TO 600
       LT=IAND(K,MASK3)
       E=ENL2(I)
       V=ENL(I)
       II=(2*LP+1)*(NL2*L+N-1)+LT-IABS(L-LP)+1
       N1=N+NDEL
       DO 610 LL1=1,3,2
       L1=L+LL1-2
       IF(L1.LT.0)GO TO 610
       IF(LHCT.LE.MIN0(L,L1))GO TO 610
       IF(NHCT.LE.MIN0(N,N1))GO TO 610
       IF(N1.LE.NL2)GO TO 615
       LT1=0
       V1=N1
       E1=1.0/(V1*V1)
       G=GBBR(N,L,LT,E,V,N1,L1,0,E1,V1,1,C1,C2,C3,SCL)
       JJ=2*(L1-L+1)+1
       SCLA(II,JJ)=SCL
c       WRITE(7,100)N,L,LT,N1,L1,LT1,II,JJ,SCL,G
       GO TO 610
  615  IL=IABS(L1-LP)+1
       IU=L1+LP+1
       DO 620 LLT1=IL,IU
       LT1=LLT1-1
       IF(IABS(LT-LT1).GT.1)GO TO 620
       J1=N2S(N1)+2*L1*(LP+1)-LP*LP+(LP-L1+1)*MAX0(LP-L1,0)-LT1
       I1=KPB(J1)
       V1=ENL(I1)
       E1=ENL2(I1)
       G=GBBR(N,L,LT,E,V,N1,L1,LT1,E1,V1,0,C1,C2,C3,SCL)
       JJ=2*(L1-L+1)+LT1-LT+3
       SCLA(II,JJ)=SCL
  620  CONTINUE
c  620  WRITE(7,100)N,L,LT,N1,L1,LT1,II,JJ,SCL,G
  610  CONTINUE
  600  CONTINUE
       RETURN
  100  FORMAT(8I5,1P2E12.4)
      END
