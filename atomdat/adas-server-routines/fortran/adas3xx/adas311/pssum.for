CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas311/pssum.for,v 1.3 2005/02/22 19:14:20 mog Exp $ Date $Date: 2005/02/22 19:14:20 $
CX
C-------------------------------------------------------------------------
C	VERSION : 1.2
C	MODIFIED: Martin O'Mullane      
C       DATE    : 3-6-2000
C		  A, B and C should be of size 200 not 50. 	
C
C	VERSION : 1.3
C	MODIFIED: Martin O'Mullane      
C       DATE    : 08-11-2004
C		  Alter dimensions from 200 to 500. 	
C
C-------------------------------------------------------------------------
       SUBROUTINE PSSUM(CA,A,NA,IPA,CB,B,NB,IPB,C,NC,IPC)
       IMPLICIT REAL*8(A-H,O-Z)
C  FINDS SUM OF SERIES OF FORM CA*(A(1)+X*A(2)+...NA TERMS...)*X**IPA
       DIMENSION A(500),B(500),C(500)
       DO 5 I=1,500
    5  C(I)=0.0
       IPC=MIN0(IPA,IPB)
       IA1=IPA-IPC
       NA1=MIN0(NA+IA1,500)
       IB1=IPB-IPC
       NB1=MIN0(NB+IB1,500)
       NC=MAX0(NA1,NB1)
       NA1=NA1-IA1
       NB1=NB1-IB1
       DO 10 I=1,NA1
       IC=I+IA1
   10  C(IC)=A(I)*CA
       DO 15 I=1,NB1
       IC=I+IB1
       T=C(IC)+B(I)*CB
       IF(1.0D12*DABS(T)-DABS(C(IC)))13,13,14
   13  C(IC)=0.0
       GO TO 15
   14  C(IC)=T
   15  CONTINUE
   20  NC1=NC
       DO 22 I=1,NC
       IF(DABS(C(I))-1.0D-70)21,21,23
   21  IPC=IPC+1
   22  NC1=NC1-1
   23  DO 24 I=1,NC1
       IS=NC-NC1
   24  C(I)=C(I+IS)
       NC=NC1
   30  IF(DABS(C(NC))-1.0D-70)31,31,32
   31  NC=NC-1
       GO TO 30
   32  RETURN
      END
