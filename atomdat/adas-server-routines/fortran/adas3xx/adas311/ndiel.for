CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas311/ndiel.for,v 1.2 2004/07/06 14:23:19 whitefor Exp $ Date $Date: 2004/07/06 14:23:19 $
CX
       SUBROUTINE NDIEL(Z,EIJ,F,T,COR,JCOR,ES,FS,CORS,JCORS,N,AD,ADLA)
       IMPLICIT REAL*8 (A-H,O-Z)
       DIMENSION THETA(1000),COR(20),ADLA(500),CORS(20)
       AD=0.0
       DO 11 J=1,N
   11  ADLA(J)=0.00D+00
       ZZ=Z*Z
       Z1=(Z+1.0)*(Z+1.0)
       EN=N
       T1=Z1*EIJ/ZZ-1.0/(EN*EN)
       E=T1*ZZ
       IF(T1)4,4,1
    1  A=DSQRT(T1)
       CALL BF(THETA,N,A)
       B=0.777135E-6*ZZ*ZZ
       J1=JCOR
       IF(N-JCOR)5,6,6
    5  J1=N
    6  DO 2 J=1,J1
    2  THETA(J)=COR(J)*THETA(J)
       C3=0.0
        Z2=Z1*EIJ/ZZ
        C4=0.51013*Z2*Z2*F
       DO 3 J=1,N
       TJ=J
       TH=THETA(J)*TJ*EN*EN
       TJ=TJ+TJ-1.0
       T3=TJ*TH/(B*TJ*(1.0+T)+TH)
       THETA(J)=TH/(B*TJ*(1.0+T))
        ADLA(J)=C4*T3
    3  C3=C3+ADLA(J)
       AD=C3
       IF(N-10)7,8,8
    7  NP=N
       GO TO 9
    8  NP=10
    9  CONTINUE
C      WRITE(6,100)(THETA(J),J=1,NP)
C 100  FORMAT(1P10E12.2)
    4  RETURN
      END
