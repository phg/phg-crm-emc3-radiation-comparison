CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas311/photo2.for,v 1.2 2004/07/06 14:28:03 whitefor Exp $ Date $Date: 2004/07/06 14:28:03 $
CX
      SUBROUTINE PHOTO2(PION,PREC,PSTIM,CREC,ZZ,TE,TP,N1,L1,LT1,E1,V1,
     1KPION,KPREC,KPSTIM,CUT,IOPT,C1,C2,C3)
C---------------------------------------------------------------------
C
C	********** FORTRAN 77 ROUTINE : PHOTO2 **********
C
C	PURPOSE : EVALUATES PHOTO IONISATION, STIMULATED
C		  AND RADIATIVE RECOMBINATION RATES.
C
C	NOTE    : THE INTEGRALS INVOLVING THE BOUND-FREE
C		  GAUNT FACTOR ARE EVALUATED USING A
C		  COMBINATION OF LAGUERRE AND GUASSIAN
C		  QUADRATURE.
C
C	HISTORY : ROUTINE WAS ORIGINALLY WRITTEN BY
C		  H.P.SUMMERS.
C
C	CONTACT : HARVEY ANDERSON
C		  UNIVERSITY OF STRATHCLYDE
C		  ANDERSON@PHYS.STRATH.AC.UK
C
C	DATE    : 5/2/98
C
C---------------------------------------------------------------------
C
C   CHANGE TO PHOTO2 ON 17/11/83 AND GPRT=GPRT1=0
       IMPLICIT REAL*8(A-H,O-Z)
       DIMENSION A(10),B(10),U(10),V(10),R(10),S(10),D(10),WA(10),WB(10)
       DIMENSION GPRT1(12),GPRT(12),UPRT(12),DC(10),RC(10)
       DIMENSION C1(5,5,3),C2(5,5,3),C3(5,5,3)
C  IF IOPT=0 THEN USE GBF
C  IF IOPT=1 THEN USE GBFR
C  CALCULATES RECOMBINATION COOLING INTEGRAL
       XNT=157890.0*E1*ZZ/TE
      XNTP=XNT*TE/TP
       UXNT=DLOG(XNT)
       UXNTP=DLOG(XNTP)
       EN=N1
C TEMPORARILY ADDED THE FOLLOWING LINE
C H.A 5/2/98
       EN=V1
      MAX=6
      A(1)=0.2228466
      A(2)=1.1889321
      A(3)=2.9927363
      A(4)=5.7751436
      A(5)=9.8374674
      A(6)=15.9828740
      WA(1)=0.45896467
      WA(2)=0.41700083
      WA(3)=0.11337338
      WA(4)=0.010399197
      WA(5)=0.0002610172
      WA(6)=0.00000089855
      B(1)=-0.9324695
      B(2)=-0.6612094
      B(3)=-0.2386192
      B(4)=0.2386192
      B(5)=0.6612094
      B(6)=0.9324695
      WB(1)=0.1713245
      WB(2)=0.3607616
      WB(3)=0.4679139
      WB(4)=0.4679139
      WB(5)=0.3607616
      WB(6)=0.1713245
       GO TO 20
    1 T1=0.0
       TC1=0.0
      DO 204 I=1,MAX
      UI=(U(I)-COR)/COR
       JRD2=JRD2+1
       IF(IOPT-1)2,3,3
    2  G=GBF(EN,UI)
       GPRT1(I)=0.0D0
       GO TO 4
    3  G=0.0
       E2=-E1*UI
       IF(L1.LE.0)GO TO 6
       G=GBFR(N1,L1,LT1,E1,V1,L1-1,E2,JRD2,C1,C2,C3)
    6  GPRT1(I)=G
       G=G+GBFR(N1,L1,LT1,E1,V1,L1+1,E2,JRD2,C1,C2,C3)
    4  T1=FACTOR*G*WA(I)/D(I)+T1
       IF(K.GT.1)GO TO 220
       TC1=FACTOR*G*WA(I)/DC(I)+TC1
  220  UPRT(I)=UI
  204  GPRT(I)=G-GPRT1(I)
      IF(SWIT3-0.5)5,10,10
    5 T2=0.0
       TC2=0.0
      DO 212 J=1,MAX
      VJ=(V(J)-COR)/COR
       JRD2=JRD2+1
       IF(IOPT-1)8,9,9
    8  G=GBF(EN,VJ)
       GPRT1(J+MAX)=0.0D0
       GO TO 12
    9  G=0.0
       E2=-E1*VJ
       IF(L1.LE.0)GO TO 11
       G=GBFR(N1,L1,LT1,E1,V1,L1-1,E2,JRD2,C1,C2,C3)
   11  GPRT1(J+MAX)=G
       G=G+GBFR(N1,L1,LT1,E1,V1,L1+1,E2,JRD2,C1,C2,C3)
   12  T2=COEFFT*G*WB(J)/R(J)+T2
       IF(K.GT.1)GO TO 225
       TC2=COEFFT*G*WB(J)/RC(J)+TC2
  225  UPRT(J+MAX)=VJ
  212  GPRT(J+MAX)=G-GPRT1(J+MAX)
      T=T1+T2
       TC=TC1+TC2
       GO TO (29,39,49),K
   10 T=T1
       TC=TC1
       GO TO (29,39,49),K
   20 SWIT=XNT
       SWIT3=157890.0*ZZ/(EN*EN*TE)
      COR=XNT
       K=1
       JRD2=0
       IF(KPREC)28,28,21
   21 IF(SWIT3-0.5)24,22,22
   22 DO 23 I=1,MAX
      D(I)=A(I)+XNT
       DC(I)=1.0
   23 U(I)=D(I)
       FACTOR=1.0
      GO TO 1
   24 P=-UXNT
      Q=-P
      COEFFT=0.5*P*DEXPT(SWIT)
      DO 25 I=1,MAX
      D(I)=A(I)+1.0
       DC(I)=1.0
      U(I)=D(I)
       BI=B(I)
      S(I)=DEXPT(0.5*(P*BI+Q))
      V(I)=S(I)
       SI=S(I)
      R(I)=DEXPT(SI)
   25  RC(I)=R(I)/SI
      FACTOR=0.367879441*DEXPT(SWIT)
      GO TO 1
   28 PREC=0.0
       CREC=0.0
      GO TO 30
   29 PREC=T
       CREC=TC
   30 SWIT=XNTP
       SWIT3=157890.0*ZZ/(EN*EN*TP)
      COR=XNTP
       K=2
       JRD2=12
       IF(KPION)38,38,31
   31 IF(SWIT3-0.5)34,32,32
   32 DO 33 I=1,MAX
      X=A(I)+XNTP
      D(I)=X*(1.0-DEXPT(-X))
   33 U(I)=X
      FACTOR=DEXPT(-SWIT)
      GO TO 1
   34 AP=1.0
      BP=1.0/XNTP
      P=BP-AP
      Q=BP+AP
       COEFFT=0.5*P
      DO 35 I=1,MAX
      U(I)=(A(I)+1.0)
      AI=A(I)
      D(I)=(A(I)+1.0)*(1.0-DEXPT(-AI)*0.367879441)
      X=2.0/(P*B(I)+Q)
      V(I)=X
      R(I)=(1.0+0.5*X*(1.0+0.33333*X*(1.0+0.25*X*(1.0+0.2*X*
     1(1.0+0.1666667*X)))))
   35 CONTINUE
       FACTOR=0.367879441
      GO TO 1
   38 PION=0.0
      GO TO 40
   39 PION=T
   40 SWIT=XNTP
       SWIT3=157890.0*ZZ/(EN*EN*TP)
      COR=XNTP
       K=3
       JRD2=24
      IF(KPSTIM)48,48,41
   41 IF(TP-TE)42,42,60
   42 IF(SWIT3-0.5)45,43,43
   43 DO  44 I=1,MAX
      U(I)=(A(I)+XNTP)
       UI=U(I)
       D(I)=UI*(1.0-DEXPT(-UI))*DEXPT(TP*A(I)/TE)
   44  CONTINUE
      FACTOR=DEXPT(-SWIT)
      GO TO 1
   45 AP = 1.0
      BP=1.0/XNTP
      P=BP-AP
      Q=BP+AP
      COEFFT=0.5*P*DEXPT(XNT)
      DO 46 I=1,MAX
      U(I)=(A(I)+1.0)
      UI=U(I)
      D(I)=(A(I)+1.0)*(1.0-DEXPT(-UI))*DEXPT( TP*UI/TE)
      X=2.0/(P*B(I)+Q)
      V(I)=X
      Y=TP*X/TE
      R(I)=(1.0+0.5*X*(1.0+0.333333*X*(1.0+0.25*X*(1.0+0.2*X*(1.0
     1 +0.1666667*X)))))
       DENOM=(1.0-Y*(1.0-0.5*Y*(1.0-0.33333*Y*(1.0-0.25*Y*(1.0-0.2*Y
     1 )))))
       R(I)=R(I)/DENOM
   46  CONTINUE
      FACTOR=0.367879441*DEXPT(XNT)
      GO TO 1
   60 SWIT=XNT
       IF(SWIT3-0.5)93,91,91
   91 DO 92 I=1,MAX
      U(I)=TE*(A(I)+XNT)/TP
      UI=U(I)
       IF(UI-CUT)97,96,96
   96  D(I)=DEXPT(CUT)
       GO TO 92
   97  D(I)=(A(I)+XNT)*(DEXPT(UI)-1.0)
   92  CONTINUE
       FACTOR=1.0
      SWIT=1.0
      GO TO 1
   93 AP=TE/TP
      BP=1.0/XNT
      P=BP-AP
      Q=BP+AP
      COEFFT=0.5*P
      DO 94 I=1,MAX
      X=2.0/(P*B(I)+Q)
      Y=TE*X/TP
      V(I)=Y
       IF(X-XNT-CUT)101,100,100
  100  R(I)=DEXPT(CUT)
       GO TO 102
  101  R(I)=(TE/TP)*(1.0+0.5*Y*(1.0+0.33333*Y*(1.0+0.25*Y*
     1 (1.0+0.2*Y))))*DEXPT(X-XNT)
  102  FACTOR=DEXPT(TP*(XNTP-1)/TE)
      U(I)=TE*A(I)/TP+1.0
      UI=U(I)
   94 D(I)=(TP/TE)*UI*(DEXPT(UI)-1.0)
       SWIT=0.0
      GO TO 1
   48 PSTIM=0.0
      GO TO 50
   49 PSTIM=T
   50 RETURN
  200  FORMAT(F9.4,1P12E10.2)
  201  FORMAT(3I3,1P12E10.2)
      END
