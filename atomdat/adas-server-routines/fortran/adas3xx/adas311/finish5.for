        SUBROUTINE FINISH5(NIP,INTD,IPRS,ILOW,IONIP,NIONIP,ILPRS,IVDISP,
     &      ZEFF,TS,W,CION,CPY,W1,ZIMPA,DNIMPA,NIMP,IUPS1,IUPS2,STITLE,
     &      NBENG,NTEMP,NDENS, lbndl, lproj)
     
       IMPLICIT REAL*8(A-H,O-Z)
C----------------------------------------------------------------------
C
C       ********** FORTRAN 77 ROUTINE : FINISH5.F **********
C
C   PURPOSE : ASSEMBLES AND SOLVES THE COLLISIONAL RADIATIVE
C             FOR THE RELATIVE POPULATIONS, Bnl FACTORS AND THE
C             COLLISIONAL-RADIATIVE IONISATION AND RECOMBINATION
C             COEFFICIENTS.     
C
C   INPUT :
C
C           ROUTINE SET TO READ STREAM 12 AS A TEMPORARY MEASURE
C
C   OUTPUT:
C
C
C   HISTORY : ROUTINE WAS ORIGINALLY WRITTEN BY H.P. SUMMERS
C
C
C   NOTE    :
C
C             IPOSNT .EQ. 1 EXTERNAL RADIATION FIELD IS .NE. 0.0
C             THE C-R MATRIX IS MODIFIED AND THE Bnl SOLUTION IS
C             OBTAINED. THE F2 COLUMN REPRESENTING THE RECOMBINATION
C             IS THEN EQUAL TO THE Bnl SOLUTION.
C
C             IPOSNT .EQ. 2 EXTERNAL RADIATION FIELD IS SET TO 0.0
C             PROVIDING THAT IR.EQ.2 AND JR.EQ.1. THE C-R MATRIX IS
C             MODIFIED AND THE Bnl SOLUTION IS OBTAINED. THE F1I
C             COLUMN REPRESENTING THE EXCITATION CONTRIBUTION FROM
C             THE FIRST METASTABLE IS EVALUATED BY MANIPULATING
C             THE F2 COLUMN, Bnl SOLUTION AND THE RELATIVE POPULATION
C             OF THE GROUND STATE.
C
C             IPOSNT .EQ.3 EXTERNAL RADIATION FIELD IS SET TO 0.0
C             PROVIDING THAT IR.EQ.2 AND JR.EQ.1. THE C-R MATRIX IS
C             MODIFIED AND THE Bnl SOLUTION IS OBTAINED. THE F1II
C             COLUMN REPRESENTING THE EXCITATION CONTRIBUTION FROM
C             THE SECOND METASTABLE IS EVALUATED BY MANIPULATING
C             THE F2 COLUMN, Bnl SOLUTION AND THE RELATIVE POPULATION
C             OF THE SECOND METASTABLE STATE.
C
C             IPOSNT .EQ.4 EXTERNAL RADIATION FIELD IS SET TO 0.0
C             PROVIDING THAT IR.EQ.2 AND JR.EQ.IMAX+1. THE C-R MATRIX
C             IS MODIFIED AND THE Bnl SOLUTION IS OBTAINED.THE F1III
C             COLUMN REPRESENTING THE EXCITATION CONTRIBUTION FROM
C             THE SECOND METASTABLE IS EVALUATED BY MANIPULATING
C             THE F2 COLUMN, Bnl SOLUTION AND THE RELATIVE POPULATION
C             OF THE SECOND METASTABLE STATE.
C
C
C
C   CONTACT : HARVEY ANDERSON
C             UNIVERSITY OF STRATHCLYDE
C             ANDERSON@PHYS.STRATH.AC.UK
C
C   DATE    : 4/3/98
C
C
C VERSION : 1.2                                     
C DATE    : 21-10-99
C MODIFIED: RICHARD MARTIN
C            - CHANGED HEXADECIMAL CONSTANTS TO Z'FFF00000' FORM.
C
C VERSION : 1.3
C DATE    : 3-6-2000
C MODIFIED: Martin O'Mullane      
C            - Removed call to errset.       
C
C VERSION : 1.4
C DATE    : 18-11-2004
C MODIFIED: Martin O'Mullane      
C            - Align with Harvey Anderson's last version.
C            - Add lbndl if adf26 files output is requested.
C            - The dsnps1 variable is replaced by iups2 in the 
C              parameter list.
C            - Outputs projection matrices for Vienna codes 
C              if lproj is set.      
C
C----------------------------------------------------------------------


       DIMENSION AT(160),ARED(160,160),RHS(160),AC(160),RHSC(160)
       DIMENSION ENL2(1000),ENL2S(1000),EXPTE(1000),EXTES(1000)
       DIMENSION NLREP(80),NLREPS(80),KPF(1000),KPFS(1000)
       DIMENSION CAT(160),BREP(160),EBREP(160),POP(160),IRA(20)
       DIMENSION IREF(10),JREF(10),SREF(10),WREF(10),CREF(10)
       DIMENSION WIREF(10),WCREF(10),PARED(160,160), PRHS(160)
       DIMENSION QAC(160),QARED(160,160),QRHS(160),QRHSC(160)
       DIMENSION F2(160),FPOP(10),F1(160),F1I(160),F1II(160),F1III(160)
       DIMENSION BACT(160),FPOP1(10),TITLE(10),DNIMPA(10),ZIMPA(10)
       DIMENSION INRES(100),NQUAN(100),LQUAN(100),LTQUAN(100)
       DIMENSION NMULTI(100), ALFA(100)

       CHARACTER STITLE*80
       CHARACTER SPECNT(6)*1

       logical   lbndl, lproj

C----------------------------------------------------------------------
       DATA MASK1,MASK2,MASK3,MASK4/Z'FFF00000',Z'000FFC00',
     &                              Z'000003FF',Z'FFFFFC00'/
       DATA AT,CAT/320*0.0D0/
       DATA SPECNT/'S','P','D','F','G','H'/

C----------------------------------------------------------------------

       IMAX=0
       IMAXS=0
       DO 11 I=1,160
       QRHS(I)=0.00D+00
       QAC(I)=0.00D+00
       QRHSC(I)=0.00D+00
       DO 12 J=1,160
       QARED(I,J)=0.00D+00
   12  CONTINUE
   11  CONTINUE


       REWIND(12)
        
       READ(12) TITLE
       READ(12)Z0
       READ(12) MJ
       IF(MJ.NE.0) THEN
         DO K=1,MJ
            READ(12) IREF(k),JREF(k),SREF(k),WREF(k),CREF(k),
     &      WIREF(k),WCREF(k)
         ENDDO
       ENDIF

       READ(12) MG
       IF(MG.NE.0) THEN
          READ(12) (IRA(k),k=1,MG)
       ENDIF

       READ(12)TE,TP,TS,DENSP,W,BMENER,DENSH

  508  READ(12)Z1,ZZ,DENS,RHO,ATE,ELLP,ISG,ISGS,ISP,NL1,NL2,NL3,IMAX,
     &         IMAXS

       IF(Z1.EQ.Z0.OR.Z1+2.EQ.Z0)IMAXS=0
       IF(Z1.EQ.Z0.OR.Z1+2.EQ.Z0)IPMAX=3
       IF(Z1.EQ.Z0.OR.Z1+2.EQ.Z0)GOTO 3
       IF(Z1+1.EQ.Z0)IPMAX=6
       IF(Z1+1.EQ.Z0)GOTO 3
       if (lbndl) write(iups1,2000)
       STOP
    3  IMAXF=IMAX
       IMAXSF=IMAXS

       DENSN=DENS

       X=DENSN/DENS

       READ(12)NLREP,NLREPS,KPF,KPFS
       READ(12)ENL2,ENL2S,EXPTE,EXTES

       JTOT=IMAX
       IF(ISP.GT.1) JTOT=JTOT+IMAXS
       DO 10 IR=1,IMAX
           READ(12)AT,CAT,RHT,CRHT,AC1,RHSC1
        DO 5 JR=1,JTOT
           ARED(IR,JR)=AT(JR)+X*CAT(JR)
    5   QARED(IR,JR)=ARED(IR,JR)
           RHS(IR)=RHT+X*CRHT
           QRHS(IR)=RHS(IR)
           AC(IR)=AC1
           QAC(IR)=AC(IR)
           RHSC(IR)=RHSC1
   10  QRHSC(IR)=RHSC(IR)
       IF(ISP.LE.1)GO TO 25
       DO 20 IR=1,IMAXS
       IRS=IR+IMAX
       READ(12)AT,CAT,RHT,CRHT,AC1,RHSC1
       DO 15 JR=1,JTOT
       ARED(IRS,JR)=AT(JR)+X*CAT(JR)
   15  QARED(IRS,JR)=ARED(IRS,JR)
       RHS(IRS)=RHT+X*CRHT
       QRHS(IRS)=RHS(IRS)
       AC(IRS)=AC1
       QAC(IRS)=AC(IRS)
       RHSC(IRS)=RHSC1
   20  QRHSC(IRS)=RHSC(IRS)
   25  READ(12)IMAXT

       RHO=RHO*DENSN/DENS
       DENS=DENSN
       IPOSNT=0
    8  IPOSNT=IPOSNT+1
       IF(IPOSNT.EQ.IPMAX)GOTO 500
       IF(IPOSNT.GT.1)BACKSPACE 12
    7  DO 1 I=1,160
          RHS(I)=QRHS(I)
          AC(I)=QAC(I)
          RHSC(I)=QRHSC(I)
        DO 2 J=1,160
          ARED(I,J)=QARED(I,J)
    2   CONTINUE
    1  CONTINUE

       DENSN = DENS

       DO 390 JJ=1,MJ
         IR=IREF(JJ)
         JR=JREF(JJ)
         SR=SREF(JJ)
         WR=WREF(JJ)
         CR=CREF(JJ)
         WCI=WIREF(JJ)
         WCJ=WCREF(JJ)
         IF(Z1+1.EQ.Z0)GOTO 4
         IF(IPOSNT.EQ.2)WCI=0.00D+00
         IF(IPOSNT.EQ.2)WCJ=0.00D+00
            GOTO 6
    4    IF(IPOSNT.EQ.2.AND.IR.EQ.2.AND.JR.EQ.1)WCJ=0.00D+00
         IF(IPOSNT.EQ.3.AND.IR.EQ.2.AND.JR.EQ.1)WCI=0.00D+00
         IF(IPOSNT.EQ.3.AND.IR.EQ.2.AND.JR.EQ.IMAX+1)WCI=0.00D+00
         IF(IPOSNT.EQ.4.AND.IR.EQ.2.AND.JR.EQ.IMAX+1)WCJ=0.00D+00
         IF(IPOSNT.EQ.5)WCJ=0.00D+00
         IF(IPOSNT.EQ.5)WCI=0.00D+00
    6    IF(IR-IMAX)381,381,382
  381    I=NLREP(IR)
         EXI=EXPTE(I)
         D=ENL2(I)
           GOTO 383
  382    I=NLREPS(IR-IMAX)
         EXI=EXTES(I)
         D=ENL2S(I)
  383    IF(JR-IMAX)384,384,385
  384    J=NLREP(JR)
         EXJ=EXPTE(J)
         D=D-ENL2(J)
           GO TO 386
  385    J=NLREPS(JR-IMAX)
         EXJ=EXTES(J)
         D=D-ENL2S(J)
  386    IF(EXJ.GT.EXI)GO TO 387
         ISWAP=IR
         IR=JR
         JR=ISWAP
         SWAP=EXI
         EXI=EXJ
         EXJ=SWAP
         SWAP=WCI
         WCI=WCJ
         WCJ=SWAP
  387    A1=6.3509817D-11/(ZZ*ZZ)
         A3=1.6189034D6*Z1*ZZ*RHO*CR/DSQRT(ATE)
         T1=A1*EXI*(SR+WR)+A3
         T2=A1*EXJ*WR+A3
         T3=A1*(EXI*(SR+WR)-EXJ*WR)
         ARED(IR,IR)=ARED(IR,IR)+T1+A1*WCI*EXI
         ARED(IR,JR)=ARED(IR,JR)-T2
         ARED(JR,IR)=ARED(JR,IR)-T1
         ARED(JR,JR)=ARED(JR,JR)+T2+A1*WCJ*EXJ
         AC(IR)=AC(IR)+A1*EXI*SR*ZZ*DABS(D)
         RHS(IR)=RHS(IR)-T3-A1*WCI*EXI
         RHS(JR)=RHS(JR)+T3-A1*WCJ*EXJ

  390    CONTINUE


         CALL MATINL(ARED,IMAXT,RHS,2,DETERM)


       C=6.60074E-24*DENS*(ATE/ZZ)**1.5


       XSP=ISP
       XS=ISG
       WT=0.5*XS/XSP

       DO 265 IR=1,IMAX
       I=NLREP(IR)
       K=KPF(I)
       N=IAND(K,MASK1)
       N=ISHFR(N,20)
       L=IAND(K,MASK2)
       L=ISHFR(L,10)
       LT=IAND(K,MASK3)
       BREP(IR)=1.0+RHS(IR)
       EBREP(IR)=EXPTE(I)*BREP(IR)
       IF(N.LE.NL2)GO TO 263
       WF=N*N
       GO TO 264
  263  ELLT=2*LT+1
       WF=ELLT/ELLP
  264  POP(IR)=C*WF*WT*EBREP(IR)
       IF(Z1+1.EQ.Z0)GOTO 501
       IF(IPOSNT.EQ.1)F2(IR)=BREP(IR)
       IF(IPOSNT.EQ.2)F1(IR)=(BREP(IR)-F2(IR))/POP(1)
       IF(IPOSNT.EQ.2)FPOP(1)=POP(1)
       IF(IPOSNT.EQ.2)BACT(IR)=BREP(IR)
       GOTO 265
  501  IF(IPOSNT.EQ.1)F2(IR)=BREP(IR)
       IF(IPOSNT.EQ.2)F1I(IR)=(BREP(IR)-F2(IR))
       IF(IPOSNT.EQ.3)F1II(IR)=(BREP(IR)-F2(IR))
       IF(IPOSNT.EQ.4)F1III(IR)=(BREP(IR)-F2(IR))
       IF(IPOSNT.EQ.2.AND.IR.EQ.1)FPOP1(1)=POP(1)
       IF(IPOSNT.EQ.3.AND.IR.EQ.2)FPOP1(2)=POP(2)
       IF(IPOSNT.EQ.4.AND.IR.EQ.IMAX+1)FPOP1(3)=POP(IMAX+1)
       IF(IPOSNT.EQ.5)FPOP(1)=POP(1)
       IF(IPOSNT.EQ.5.AND.IR.GE.2)FPOP(2)=POP(2)
       IF(IPOSNT.EQ.5.AND.IR.GE.IMAX+1)FPOP(3)=POP(IMAX+1)
       IF(IPOSNT.EQ.5)BACT(IR)=BREP(IR)
  265  CONTINUE

       IF(ISP.LE.1)GO TO 321
       XSS=ISGS
       WTS=0.5*XSS/XSP

       DO 330 IR=1,IMAXS
       I=NLREPS(IR)
       K=KPFS(I)
       N=IAND(K,MASK1)
       N=ISHFR(N,20)
       L=IAND(K,MASK2)
       L=ISHFR(L,10)
       LT=IAND(K,MASK3)
       IRS=IMAX+IR
       BREP(IRS)=1.0+RHS(IRS)
       EBREP(IRS)=EXTES(I)*BREP(IRS)
       IF(N.LE.NL2)GO TO 318
       WF=N*N
       GO TO 319
  318  ELLT=2*LT+1
       WF=ELLT/ELLP
  319  POP(IRS)=C*WF*WTS*EBREP(IRS)
       IF(Z1+1.EQ.Z0)GOTO 510
       IF(IPOSNT.EQ.1)F2(IRS)=BREP(IRS)
       IF(IPOSNT.EQ.2)F1(IRS)=(BREP(IRS)-F2(IRS))/POP(1)
       IF(IPOSNT.EQ.2)FPOP(1)=POP(1)
       IF(IPOSNT.EQ.2)BACT(IRS)=BREP(IRS)
       GOTO 330
  510  IF(IPOSNT.EQ.1)F2(IRS)=BREP(IRS)
       IF(IPOSNT.EQ.2)F1I(IRS)=(BREP(IRS)-F2(IRS))
       IF(IPOSNT.EQ.3)F1II(IRS)=(BREP(IRS)-F2(IRS))
       IF(IPOSNT.EQ.4)F1III(IRS)=(BREP(IRS)-F2(IRS))
       IF(IPOSNT.EQ.4.AND.IRS.EQ.IMAX+1)FPOP1(3)=POP(IMAX+1)
       IF(IPOSNT.NE.5)GOTO 330
       FPOP(1)=POP(1)
       FPOP(2)=POP(2)
       FPOP(3)=POP(IMAX+1)
       BACT(IRS)=BREP(IRS)

  330  CONTINUE


  321  CONTINUE
       CA=1.57456E10*ELLP*ZZ*ZZ
       GOTO 8
  500  CONTINUE
       if (lbndl) write(iups1,2001)
       if (lbndl) write(iups1,2012)
       if (lbndl) write(iups1,2045)
       if (lbndl) write(iups1,2013) STITLE
       if (lbndl) write(iups1,2045)
       if (lbndl) write(iups1,2045)
       if (lbndl) write(iups1,2020) ( TITLE(I),I=1,6) ,Z0,Z1
       if (lbndl) write(iups1,2045)
       if (lbndl) write(iups1,2023)TS,TE,TP
       if (lbndl) write(iups1,2030)W,DENSN,DENSP
       XXDENS=DENSH/DENSN
       XFLUX=1.3831749E+06*DSQRT(BMENER)*DENSH
       if (lbndl) write(iups1,2045)
       if (lbndl) write(iups1,2033)BMENER,DENSH,XXDENS,XFLUX
       if (lbndl) write(iups1,2045)

C--------------------------------------------------------------------
C
C       MODIFIED : HARVEY ANDERSON
C       DATE     : 28/5/99
C
C       INTRODUCTION OF THE PROJECTION MATRICES.
C
C       1. IMAXT - NUMBER OF TOTAL LEVELS CONTAINED IN C-R MATRIX
C       2. PARED - COPY OF CONTENTS OF ARED ( BEFORE ARED IS MOD.)
C       3. NRES  - NUMBER OF RESOLVED LEVELS
C       4. PRHS  - COPY OF THE CONTENTS OF RHS BEFORE MODIFICATION.
C
C--------------------------------------------------------------------

       if (lproj) then

C       TEMPORARY VARIABLES

        NMAXEB = NBENG
        NMAXTE = NTEMP
        NMAXNE = NDENS

        DO I=1, IMAXT
          DO J=1,IMAXT
            PARED(I,J)=ARED(I,J)
          ENDDO
          PRHS(I) = RHS(I)
        ENDDO

C--------------------------------------------------------------------
C       REDUCING C-R MATRIX TO INCLUDE ONLY THE ELEMENTS
C       ASSOCIATED WITH THE LS-RESOLVED LEVELS
C--------------------------------------------------------------------

         NRES=0
         DO IR=1, IMAXT
           IF(IR.LE.IMAX) THEN
             I=NLREP(IR)
             K=KPF(I)
           ELSE
             I=NLREPS(IR-IMAX)
             K=KPFS(I)
           ENDIF
             N=IAND(K,MASK1)
             N=ISHFR(N,20)
             L=IAND(K,MASK2)
             L=ISHFR(L,10)
             LT=IAND(K,MASK3)
           IF (N.LE.NL2 .AND.N.GT.0 ) THEN
              NRES=NRES+1
              INRES(NRES)=IR
              NQUAN(NRES)=N
              LQUAN(NRES)=L
              LTQUAN(NRES)=LT
           ENDIF
         ENDDO

C--------------------------------------------------------------------
C       PREPARING OUTPUT FOR THE PROJECTION MATRICES
C
C       MODIFIED : HARVEY ANDERSON
C       DATE     : 28/5/99
C--------------------------------------------------------------------

       IF(NDONE.NE.1) THEN      
       write(iups2,200) NRES, NMAXEB , NMAXTE , NMAXNE , '30/08/99'

       write(iups2,109)
       write(iups2,*)

        DO I=1,NRES
           J=INRES(I)
          IF(J.LE.IMAX) THEN
              NMULTI(I)=1
            ELSE
              NMULTI(I)=3
          ENDIF
        ENDDO

        write(iups2,202) (I, NQUAN(I)  , SPECNT(LQUAN(I)+1),
     &  NMULTI(I), SPECNT(LTQUAN(I)+1), I=1,NRES)

  202   FORMAT(2(2X,I4,3X,'1S',I1,A,3X,'(',I1,')',A))

       write(iups2,*)
       write(iups2,109)
       NDONE=1
       ENDIF

       write(iups2,*)
       write(iups2,110)  BMENER  ,  TE  ,  TP
       write(iups2,111)  DENSN   , DENSP , DENSH
       write(iups2,*)
       write(iups2,109)

        DO 870 IR =1, NRES
              KR=INRES(IR)
              IF(IR-KR)855,870,855
  855     DO 860 I=1,IMAXT
              SWAP=PARED(IR,I)
              PARED(IR,I)=PARED(KR,I)
  860         PARED(KR,I)=SWAP
            DO 865 I=1,IMAXT
              SWAP=PARED(I,IR)
              PARED(I,IR)=PARED(I,KR)
  865         PARED(I,KR)=SWAP
  870   CONTINUE


C--------------------------------------------------------------------
C        END OF REDUCING C-R MATRIX AND START OF EVALUATING
C        THE ELEMENTS OF THE PROJECTION MATRIX.
C--------------------------------------------------------------------

       CALL MATINL(PARED,NRES,PRHS,2,DETERM)

       DO 878 IR=1,NRES
          ALF=0.0
            DO 876 JR=1,NRES
              I1=INRES(JR)
              IF(I1-IMAX)873,873,874
  873         J=NLREP(I1)
              K=KPF(J)
              EXE=EXPTE(J)*WT
                GO TO 875
  874         J=NLREPS(I1-IMAX)
              K=KPFS(J)
              EXE=EXTES(J)*WTS
  875         LT=IAND(K,MASK3)
              ELLT=2*LT+1
              PARED(IR,JR)=CA*PARED(IR,JR)/(ELLT*EXE*DENS)
  876         ALF=ALF+PARED(IR,JR)*POP(I1)

              ALFA(IR)=ALF

              write(iups2,128) (PARED(IR,JR),JR=1,NRES)

  878  CONTINUE
  128  FORMAT(5(1PE17.10,1X))
              write(iups2,109)
              write(iups2,128)( ALFA(I),I=1,NRES )


C--------------------------------------------------------------------
C       END OF INTRODUCING PROJECTION MATRICES
C
C       MODIFIED : HARVEY ANDERSON
C       DATE     : 14/6/99
C--------------------------------------------------------------------

       endif


C--------------------------------------------------------------------
C
C       EVALUATING THE EFFECTIVE IONISATION RATES AND CROSS
C       COUPLING COEFFICENTS.
C--------------------------------------------------------------------


       DO 370 IR=1,MG
          KR=IRA(IR)
          IF(IR-KR)355,370,355
  355     DO 360 I=1,IMAXT
          SWAP=ARED(IR,I)
          ARED(IR,I)=ARED(KR,I)
  360     ARED(KR,I)=SWAP
            DO 365 I=1,IMAXT
             SWAP=ARED(I,IR)
             ARED(I,IR)=ARED(I,KR)
  365       ARED(I,KR)=SWAP
  370  CONTINUE



       CALL MATINL(ARED,MG,RHS,2,DETERM)

       if (lbndl) write(iups1,101)(IRA(IN),IN=1,3)
       DO 378 IR=1,MG
          ALF=0.0
          I=IRA(IR)
            DO 376 JR=1,MG
              I1=IRA(JR)
              IF(I1-IMAX)373,373,374
  373         J=NLREP(I1)
              K=KPF(J)
              EXE=EXPTE(J)*WT
                GO TO 375
  374         J=NLREPS(I1-IMAX)
              K=KPFS(J)
              EXE=EXTES(J)*WTS
  375         LT=IAND(K,MASK3)
              ELLT=2*LT+1
              ARED(IR,JR)=CA*ARED(IR,JR)/(ELLT*EXE*DENS)
  376       ALF=ALF+ARED(IR,JR)*POP(I1)
            if (lbndl) write(iups1,103)IR,I,ALF,(ARED(IR,JR),JR=1,MG)
  378  CONTINUE
            if (lbndl) write(iups1,2045)

C       commented out read'at' 10/8/98
        READ(12)AT
C
c       COOL=0.0
c       DO 342 IR=1,IMAX
c  342  COOL=COOL+AT(IR)*(BREP(IR)*AC(IR)+RHSC(IR))
c       IF(ISP.LE.1)GO TO 345
c       DO 344 IR=1,IMAXS
c       IRS=IR+IMAX
c  344  COOL=COOL+AT(IRS)*(BREP(IRS)*AC(IRS)+RHSC(IRS))
c  345  COOL=1.57456E10*ZZ*ZZ*C*COOL
c
C       WRITE(7,1008)COOL
C       GOTO 8
       IF(Z1.EQ.Z0.OR.Z1+2.EQ.Z0)GOTO 547
       DO 541 IR=1,IMAX
       F1I(IR)=F1I(IR)/FPOP1(1)
       F1II(IR)=F1II(IR)/FPOP1(2)
       F1III(IR)=F1III(IR)/FPOP1(3)
  541  CONTINUE
       DO  542 IR=1,IMAXS
       IRS=IR+IMAX
       F1I(IRS)=F1I(IRS)/FPOP1(1)
       F1II(IRS)=F1II(IRS)/FPOP1(2)
       F1III(IRS)=F1III(IRS)/FPOP1(3)
  542  CONTINUE
  547  CONTINUE



       IF(Z1.EQ.Z0.OR.Z1+2.EQ.Z0)IMMM=1
       IF(Z1+1.EQ.Z0)IMMM=3
       DO 507 I=1,IMMM
       IMM=I
       IF(I.EQ.3)IMM=IMAX+1
  507  CONTINUE
       GOTO 590
  590  if (lbndl) write(iups1,1004)ISG
       IF(Z1.EQ.Z0.OR.Z1+2.EQ.Z0) then
          if (lbndl) write(iups1,3000)
       ENDIF
       IF(Z1+1.EQ.Z0) then
          if (lbndl) write(iups1,3001)
       ENDIF
       DO 530 IR=1,IMAX
       I=NLREP(IR)
       K=KPF(I)
       N=IAND(K,MASK1)
       N=ISHFR(N,20)
       L=IAND(K,MASK2)
       L=ISHFR(L,10)
       LT=IAND(K,MASK3)
       IF(Z1+1.EQ.Z0)GOTO 525
       B=F1(IR)*FPOP(1)+F2(IR)
       if (lbndl) write(iups1,2060)IR,N,L,LT,F1(IR),F2(IR),B,BACT(IR),
     &                  POP(IR)*(1.0/BACT(IR))
       GOTO 530
  525  B=F1I(IR)*FPOP(1)+F1II(IR)*FPOP(2)+F1III(IR)*FPOP(3)+F2(IR)
       if (lbndl) write(iups1,2065)IR,N,L,LT,F1I(IR),F1II(IR),F1III(IR),
     &                  F2(IR),B,BACT(IR),POP(IR)*(1.0/BACT(IR))
  530  CONTINUE
       IF(ISP.LE.1)GOTO 550
       if (lbndl) write(iups1,1004)ISGS
       IF(Z1.EQ.Z0.OR.Z1+2.EQ.Z0)then
          if (lbndl) write(iups1,3000)
       endif
       IF(Z1+1.EQ.Z0)then
          if (lbndl) write(iups1,3001)
       endif
       DO 540 IR=1,IMAXS
       I=NLREPS(IR)
       K=KPFS(I)
       N=IAND(K,MASK1)
       N=ISHFR(N,20)
       L=IAND(K,MASK2)
       L=ISHFR(L,10)
       LT=IAND(K,MASK3)
       IRS=IMAX+IR
       IF(Z1+1.EQ.Z0) GOTO 535
       B=F1(IRS)*FPOP(1)+F2(IRS)
       if (lbndl) write(iups1,2060)IRS,N,L,LT,F1(IRS),F2(IRS),
     &                             B,BACT(IRS)

       GOTO 540
  535  B=F1I(IRS)*FPOP(1)+F1II(IRS)*FPOP(2)+F1III(IRS)*FPOP(3)+F2(IRS)
       if (lbndl) write(iups1,2065)IRS,N,L,LT,F1I(IRS),F1II(IRS),
     &                   F1III(IRS),F2(IRS),B,BACT(IRS),
     &                   POP(IRS)*(1.0/BACT(IRS))
  540  CONTINUE
  550  CONTINUE
       if (lbndl) write(iups1,2045)
       if (lbndl) write(iups1,2045)
       IF(Z1+1.EQ.Z0) then
          if (lbndl) write(iups1,2075)
       endif
       IF(Z1.EQ.Z0.OR.Z1+2.EQ.Z0) then
          if (lbndl) write(iups1,2076)
       endif
       if (lbndl) write(iups1,2045)
       if (lbndl) write(iups1,2045)

       if (lbndl) write(iups1,3003)NIP,INTD,IPRS,ILOW,IONIP,NIONIP,
     &                  ILPRS,IVDISP,ZEFF,TS,W,CION,CPY,W1,ZIMPA(1),
     &                  DNIMPA(1)
C
       IF(NIMP.GT.1)THEN
           DO  IMP=2,NIMP
             if (lbndl) write(iups1,3004)ZIMPA(IMP),DNIMPA(IMP)
           ENDDO
       ENDIF

C--------------------------------------------------------------------
  599  RETURN
  
C--------------------------------------------------------------------
  100  FORMAT('The value of imax    ',20I5)
  101  FORMAT(1H0,'COLLISIONAL DIELECTRONIC RATES'//8X,'IG',9X,'I',13X,
     &'ALF',5X,'EFFECTIVE IONISATION AND CROSS COUPLING RATES'/35X,6I15)
  103  FORMAT(2I10,5X,1P,7E15.7)
  104  FORMAT(1H0,7X,'IR',9X,'N',9X,'L',8X,'LT',13X,'C',14X,'B',10X,
     &'DEXP(X)B',10X,'POP')
  105  FORMAT(4I10,5X,1P,5E15.7)
  106  FORMAT(10E12.4)
  107  FORMAT(1H0,7X,'DENS'/1PE10.2)
  108  FORMAT(1H0)
  109  FORMAT(89('-'))
  110  FORMAT(6X,'EB = ',2X,1PE9.3,' EV/AMU ',1X,' TE = ',2X, 1PE9.3,
     & ' K ',1X,'    TP = ',1PE9.3,' K ')
  111  FORMAT(6X,'NE = ',2X,1PE9.3,' CM-3  ',1X,'  NP = ',2X, 1PE9.3,
     & ' CM-3 ',1X,' NB = ',1PE9.3,' CM-3 ')
  112  FORMAT(2I5,1P,5E12.4)
  122  FORMAT(2I5,1P,6E15.7)
  127  FORMAT(2I10,5X,1P,5E15.7)
  200  FORMAT(6X,'/NRES=',I4,1X,'/MAXEB=',I4,1X,'/MAXTE=',I4,1X,
     & '/MAXNE=',I4,1X,'/DATE=',A8,1X,'/CODE=ADAS311')
 1004  FORMAT(1H0,'LEVELS OF MULTIPLICITY',I3)
 1005  FORMAT(1H0,'FURTHER RATE INCLUSION'//8X,'IR',8X,'JR',8X,
     &'SPONT. RATE',3X,'INDUCED RATE',5X,'COLL. RATE'
     &,4X,'IR ION. RATE',3X,'JR ION. RATE')
 1008  FORMAT(1H0,'COOLING RATE'/1PE15.7)
 2000  FORMAT(' NOT A H. , HE. OR LI - LIKE ATOM')
 2001  FORMAT('1')
 2002  FORMAT(' NEUTRAL H. BEAM DENSITY IS NOT OFF  NH = ',1PE8.2)
 2010  FORMAT(' HYDROGEN - LIKE ATOM OR LITHIUM - LIKE ATOM')
 2011  FORMAT(' HELIUM - LIKE ATOM ')
 2012  FORMAT(' EFFECTIVE CONTRIBUTION TABLE FOR ION PRINCIPAL QUANTUM S
     &HELL POPULATIONS IN THERMAL PLASMA')
 2013  FORMAT(' TITLE : ', A )
 2020  FORMAT(1H0,3X,6A1,18X,' Z0 = ',1PE8.2,15X,' Z1 = ',1PE8.2)
 2023  FORMAT('    TRAD = ',1PE8.2,' K        TE = ',1PE8.2,' K
     &     TP = ',1PE8.2,' K')
 2030  FORMAT('       W = ',1PE8.2,'          NE = ',1PE8.2,' CM-3
     &     NP = ',1PE8.2,' CM-3')
 2033  FORMAT('      EH = ',1PE8.2,' EV/AMU   NH = ',1PE8.2,' CM-3
     &  NH/NE = ',1PE8.2,'   FLUX = ',1PE8.2,' CM-2 SEC-1')
 2040  FORMAT(1H0,'POPULATION FOR LEVEL ',I3,'  =  ',1PE15.7)
 2045  FORMAT('   ')
 2050  FORMAT(' ZERO DEPLETION OF LEVELS CHECK')
 2060  FORMAT(4I5,7X,1P,7E15.7)
 2065  FORMAT(4I5,7X,1P,7E15.7)
 2075  FORMAT(75X,'B   = F1(I)*(N1/N+) F1(II)*(N2/N+) +F1(III)*(N3/N+) +
     &F2 '/
     &75X,'N1  = POPULATION OF THE 1s2 1S METASTABLE'/
     &75X,'N2  = POPULATION OF THE 2s  1S METASTABLE'/
     &75X,'N3  = POPULATION OF THE 2s  3S METASTABLE'/
     &75X,'N+  = POPULATION OF GROUND STATE OF NEXT IONISATION STAGE'/
     &75X,'NL  = POPULATION OF RESOLVED NL QUANTUM SHELL OF ION'/
     &75X,'B   = SAHA-BOLTZMANN FACTOR FOR RESOLVED NL QUANTUM SHELL'/
     &75X,'EH  = NEUTRAL HELIUM BEAM ENERGY'/
     &75X,' W  = RADIATION DILUTION FACTOR'/
     &75X,'Z0  = NUCLEAR CHARGE'/
     &75X,'Z1  = ION CHARGE+1')
 2076  FORMAT(47X,'BN = F1*(N1/N+) + F2 '/
     &47X,'N1 = POPULATION OF GROUND STATE OF ION'/
     &47X,'N+ = POPULATION OF GROUND STATE OF NEXT IONISATION STAGE'/
     &47X,'NN = POPULATION OF PRINCIPAL QUANTUM SHELL N OF ION'/
     &47X,'BN = SAHA-BOLTZMANN FACTOR FOR PRINCIPAL QUANTUM SHELL N '/
     &47X,'EH = NEUTRAL HYDROGEN BEAM ENERGY'/
     &47X,' W = RADIATION DILUTION FACTOR'/
     &47X,'Z0 = NUCLEAR CHARGE'/
     &47X,'Z1 = ION CHARGE+1')

 3000  FORMAT(1H0,2X,'IR',4X,'N',4X,'L',3X,'LT ',13X,'F1',13X,'F2',10X,
     &'B(CHECK)',7X,'B(ACTUAL)',7X,'NN/(BN*N+)')
 3001  FORMAT(1H0,2X,'IR',4X,'N',4X,'L',3X,'LT ',13X,'F1(I)',9X,
     &'F1(II) ',7X,'F1(III)  ',8X,'F2',10X,'B(CHECK)',7X,'B(ACTUAL)',
     &6X,'NL/(B*N+)')
 3003  FORMAT(1H ,'NIP   =',I3,5X,'INTD  =',I3,5X,'IPRS  =',I3,5X,
     &            'ILOW  =',I3,5X,
     &            'IONIP =',I3,5X,'NIONIP=',I3,5X,'ILPRS =',I3,5X,
     &            'IVDISP=',I3/
     &        1H ,'ZEFF  =',0P,F4.1,4X,'TS  =',1PD10.2,5X,
     &            'W   =',1PD10.2,5X,
     &            'CION  =',0P,F3.1,5X,'CPY =',0P,F3.1,5X,
     &            'W1  =',1PD10.2,5X,'ZIMP  =',0P,F4.1,1X,'(', 1PD10.2,
     &            ')')
 3004  FORMAT(1H ,110X,F4.1,' (',1PD10.2,')')
C--------------------------------------------------------------------

      END
