CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas311/psprod.for,v 1.3 2005/02/22 19:14:20 mog Exp $ Date $Date: 2005/02/22 19:14:20 $
CX
C-------------------------------------------------------------------------
C	VERSION : 1.2
C	MODIFIED: Martin O'Mullane      
C       DATE    : 08-11-2004
C		  Alter dimensions from 200 to 500. 	
C
C-------------------------------------------------------------------------
       SUBROUTINE PSPROD(A,NA,IPA,B,NB,IPB,C,NC,IPC)
       IMPLICIT REAL*8(A-H,O-Z)
C  FINDS PRODUCT OF SERIES OF FORM (A(1)+X*A(2)+...NA TERMS...)*X**IPA
       DIMENSION A(500),B(500),C(500)
       IPC=IPA+IPB
       NC=MIN0(NA+NB-1,500)
       DO 6 I1=1,NC
    2  I2=MIN0(NA,I1)
       I3=MAX0(1,I1-NB+1)
       X=0.0
       DO 5 I=I3,I2
    5  X=X+A(I)*B(I1-I+1)
    6  C(I1)=X
   10  RETURN
      END
