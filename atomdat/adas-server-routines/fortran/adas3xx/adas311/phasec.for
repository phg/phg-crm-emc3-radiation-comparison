CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas311/phasec.for,v 1.2 2004/07/06 14:27:51 whitefor Exp $ Date $Date: 2004/07/06 14:27:51 $
CX
       FUNCTION PHASEC(E,C,L,Z,X)
       IMPLICIT REAL*8(A-H,O-Z)
      PI=3.1415926535898
      ZZ=Z*Z
      CK=DSQRT(E)
      XK=X*CK
      XZ=X*Z
      C1=XK*XK-XZ-XZ-C
      IF(C1-1.0E-10)20,20,21
20    WRITE(6,100)E,C,L,Z,X
      PHASEC=0.25*PI
      RETURN
21    CHI=DSQRT(C1)
      IF(CHI-5.0)22,23,23
22    WRITE(6,101)E,C,L,Z,X,CHI
23    CONTINUE
      C1=1.0/CHI
      P=CHI-0.125*C1
      IF(DABS(Z)+DABS(C)-1.0D-15)18,17,17
17    B=E*C+ZZ
      A=ZZ*(CHI-XK)-CK*C*Z
      P=P-(A/(24.0*B*(CHI+XK))+5.0*(XZ+C)*C1*C1/24.0)*C1
18    IF(E-1.0E-10)1,2,2
1     P=P+CHI
      GO TO 3
2     A=Z/CK
      P=P+A*(1.0-DLOG(CHI+XK-A))
3     IF(L)7,4,4
4     EL=L
      IF(E-1.0E-10)5,6,6
5     P=P-(EL+0.25)*PI
      GO TO 7
6     P=P-0.5*EL*PI+ARGAM(L,A)
7     IF(C+1.0E-10)8,9,9
8     C1=DSQRT(-C)
      T1=C1*CHI-C-XZ
      T2=C1*XK-XZ
      PHASEC=P+((C+0.125)/C1)*DLOG(T1/T2)
      RETURN
9     IF(C-1.0E-10)10,10,11
10    PHASEC=P+1.0/(4.0*(CHI+XK))
      RETURN
11    T=CK*C*CHI+ZZ*X+C*Z
      T1=DABS(T)
      IF(T1-1.0E-15)12,12,13
12    TH=0.5*PI
      GO TO 16
13    ARG=DSQRT(C*(ZZ+E*C)*(CK*C*CHI+E*C*X+2.*ZZ*X+C*Z)/(CK*CHI+E*X-
     1Z))/T
      IF(T)14,14,15
14    TH=PI-DATAN(-ARG)
      GO TO 16
15    TH=DATAN(ARG)
16    PHASEC=P+TH*(C+0.125)/DSQRT(C)
      RETURN
100   FORMAT('0FAILED IN PHASEC,  E=',1PE10.2,' C=',1PE10.2,' L=',
     1I3,' Z=',1PE10.2,' X=',1PE10.2)
101   FORMAT('0INACCURACY IN PHASEC FOR E=',1PE10.2,' C=',1PE10.2,
     1' L=',I3,' Z=',1PE10.2,' X=',1PE10.2,' CHI=',1PE10.2)
      END
