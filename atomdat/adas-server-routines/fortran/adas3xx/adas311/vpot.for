CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas311/vpot.for,v 1.2 2004/07/06 15:28:23 whitefor Exp $ Date $Date: 2004/07/06 15:28:23 $
CX
       FUNCTION VPOT(Z0,Z1,ALF,X)
       IMPLICIT REAL*8(A-H,O-Z)
       X1=1.0/X
       X2=ALF*X
       VPOT=2.0*X1*(Z1+(Z0-Z1)*DEXP(-0.2075*X2)/(1.0+1.19*X2))
       RETURN
      END
