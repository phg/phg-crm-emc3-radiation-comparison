CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas311/qhe.for,v 1.4 2004/07/07 17:03:23 allan Exp $ Date $Date: 2004/07/07 17:03:23 $
CX
       FUNCTION QHE(EPRO,TTAR,ISEL,ZSEL,NSEL,IORD,EA,OA,N,IPASS,
     &              DSLPATH)
C
       IMPLICIT REAL*8(A-H,O-Z)
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 FUNCTION: QHE ***************************
C
C  PURPOSE : TO EVALUATE MAXWELL AVERAGED TOTAL IONISATION, EXCITATION
C	     AND CHARGE EXCHANGE RATE COEFFICIENTS. FUNCTION ALSO
C	     RETURNS THE RAW CROSS SECTION DATA FOR VERIFICATION AND
C	     GRAPHING PURPOSES.
C            THE INCIDENT PARTICLE IS A MONOENERGETIC
C	     BEAM AND THE TARGET IS A MAXWELL DISTRIBUTION. THE TARGET AND
C	     PROJECTILE ROLES MAY BE REVERSED. ARBITRARY RELATIVE SPEEDS
C            ARE ALLOWED.
C
C  HISTORY : ROUTINE WAS ORIGINALLY WRITTEN BY H.P.SUMMERS AND MODIFIED
C	     AND RESTRUCTURED BY HARVEY ANDERSON.	
C
C  DATA   :  ROUTINE UTILISES THE ION/ATOM COLLISION DATABASE ASSEMBLED FOR
C	     HELIUM IN INTERACTION WITH IMPURITIES UPTO THE SECOND
C	     PERIOD. THE DATA BASE IS OF ADAS ADF02 TYPE FORMAT :
C
C		/ADAS/ADF02/SIA#HE/SIA#HE_J91#HE.DAT
C
C  INPUT  :
C
C	     (R*8)	EPRO	:	INCIDENT PARTICLE ENERGY (EV/AMU).
C	     (R*8)	TTAR	:	MAXWELL TEMPERATURE OF TARGET
C					PARTICLES (EV) IF (TTAR.LE.0) THEN
C					ONLY RAW SOURCE VALUES ARE RETURNED
C              				IN ARRAYS (EA(I),OA(I), I=1,N).
C	     (R*8)      ZSEL	:	NUCLEAR CHARGE (REQUIRED ONLY FOR
C					PARTICULAR ISEL).
C	     (I*4)	ISEL	:	SELECTOR FOR PARTICULAR RATE COEFFT.
C					CHOSEN FROM TABLE BELOW (SEE ALSO
C					NOTES ON DATA).
C	     (I*4)	NSEL	:	PRINC. QUANTUM NO. (REQUIRED ONLY FOR
C					PARTICULAR ISEL NB. NSEL SHOULD BE
C					ZERO ON ENTRY OTHERWISE).
C	     (I*4)      IORD    :	1 FOR 1ST PARTICLE INCIDENT AND
C					MONOENERGETIC = 2 FOR 2ND PARTICLE
C					INCIDENT AND MONOENERGETIC.
C	     (I*4)	IPASS	:	0 IF DATA FILE TO BE READ IN AFRESH
C            				1 IF DATA FILE IS NOT TO BE READ IN
C					AGAIN.
C	
C
C  OUTPUT :
C      	     (R*8)	QHE     :	RATE COEFFICIENT (CM3 SEC-1).
C            (R*8)      EA	: 	SET OF ENERGIES (EV/AMU) FOR SELECTED
C					SOURCE DATA.
C            (R*8)      OA      :       CROSS-SECTIONS (CM**2) FOR SELECTED
C					SOURCE DATA.
C      	     (I*4)      N       :       NUMBER OF SOURCE DATA VALUES
C      	     (CHR)	TITLF   :	INFORMATION STRING.
C
C
C
C  CONTACT : HARVEY ANDERSON
C	     UNIVERSITY OF STRATHCLYDE.
C	     ANDERSON@CHAMBA.PHYS.STRATH.AC.UK
C
C  DATE    : 05/10/97
C
C
C VERSION: 1.2                          DATE: 15-10-99
C MODIFIED: RICHARD MARTIN
C  		REMOVED 'ACTION' FROM OPEN STATEMENT.
C
C VERSION: 1.3				DATE: 07-07-2004
C MODIFIED: ALLAN WHITEFORD
C 		-CHANGED CALLS FROM DXNB{A,B}F TO XXNB{A,B}F
C
C----------------------------------------------------------------------
       PARAMETER (NSTORE=80)
       DIMENSION XA(9),WXA(9),NA(NSTORE)
       DIMENSION SIGA(24,NSTORE),ERELA(24,NSTORE)
       DIMENSION EM1A(NSTORE),EM2A(NSTORE),ALFA(NSTORE)
       DIMENSION ETHRA(NSTORE),EA(24),OA(24),YA(24),FA(24)
       DIMENSION XSA(24),YSA(24),XKA(28),C(28),WTS(24)
CX     DIMENSION WRK(160)
       CHARACTER DSNAME*80, STRING*80, TITLF*80, DSLPATH*80
       DIMENSION B(24),A(28,4),DIAG(28)
       INTEGER   IFIRST(1) , ILAST(1)
C
C----------------------------------------------------------------------
C  GAUSS-LAGUERRE DATA (9PT)
C----------------------------------------------------------------------
C
       DATA XA/0.1523222277D0,0.8072200227D0,2.0051351556D0,
     &         3.7834739733D0,6.2049567778D0,9.3729852516D0,
     &         13.4662369110D0,18.8335977889D0,26.3740718909D0/
       DATA WXA/3.36126421798D-1,4.11213980424D-1,1.99287525371D-1,
     &          4.74605627657D-2,5.59962661079D-3,3.05249767093D-4,
     &          6.59212302608D-6,4.11076933035D-8,3.29087403035D-11/
C----------------------------------------------------------------------
C  XSECT STORAGE - NA(ISEL),EM1A(ISEL),EM2A(ISEL),ALFA(ISEL),
C                           ETHRA(ISEL),ISEL
C                  ERELA(IVAL,ISEL), SIGA(IVAL,ISEL)  WHERE
C                         NA=NUMBER OF ENERGIES FOR CROSS-SECTION
C                         EM1=1ST PARTICLE MASS (AT.MASS UNITS)
C                         EM2=2ND PARTICLE MASS (AT.MASS UNITS)
C                         ALF=HIGH ENERGY SLOPE (AS EREL**-ALF)
C                         ETHR=THRESHOLD ENERGY FOR REACTION (EV)
C                         EREL=RELATIVE ENERGY (EV/AMU)
C                         SIG=X-SECT (CM2)
C  WHERE THERE ARE UP TO 24 VALUES FOR EACH X-SECT
C
C  N.B. IT ASSUMED FOR POWER LAW INTERPOLATION PURPOSES THAT THE
C  CROSS-SECTION AT ERELA(1) IS FINITE.
C----------------------------------------------------------------------
       DATA LCK,LWRK/28,160/
C
       IF(IPASS.LE.0)THEN
C----------------------------------------------------------------------
C  OPEN FILE AND READ IN
C----------------------------------------------------------------------

           DSNAME='/adf02/sia#he/sia#he_j91#he.dat'
           CALL XXSLEN(DSLPATH,IFIRST(1),ILAST(1))
           DSNAME=DSLPATH(IFIRST(1):ILAST(1))//DSNAME
           OPEN(UNIT=15,FILE=DSNAME)
C
           READ (15,1000)NTRANS
           DO 150 ISTORE=1,NTRANS
            READ (15,1001)NA(ISTORE),EM1A(ISTORE),EM2A(ISTORE),
     &                    ALFA(ISTORE),ETHRA(ISTORE)
            READ (15,1002)(ERELA(J,ISTORE),J=1,NA(ISTORE) )
            READ (15,1002)(SIGA(J,ISTORE),J=1, NA(ISTORE) )
  150      CONTINUE
C
           IF(ISEL.LE.0.OR.ISEL.GT.NTRANS)THEN
    7          READ(15,1004,END=8)STRING
               GO TO 7
    8          IPASS=0
               CLOSE(15)
               N=0
               QHE=0.0D0
               ISEL=0
               RETURN
           ELSE
               IPASS=1
               CLOSE(15)
           ENDIF
       ENDIF
C-----------------------------------------------------------------------
C  EXTRACT DATA SELECTED BY ISEL AND CONVERT IN SCALED DATA CASES
C-----------------------------------------------------------------------
       TITLF='
     &                      '
       IZSEL=ZSEL+0.001
       N=NA(ISEL)
       DO 5 I=1,N
       OA(I)=SIGA(I,ISEL)
       EA(I)=ERELA(I,ISEL)
       YA(I)=ERELA(I,ISEL)
    5  CONTINUE
       QHE=0.0D0
       IF(TTAR.LE.0.0D0)THEN
           V=1.38377D6*DSQRT(EPRO)
C-----------------------------------------------------------------------
C  INTERPOLATE USING SPLINES (NAG ALGORITHM)
C-----------------------------------------------------------------------
           IF(EPRO.LE.EA(1))THEN
               QHE=V*OA(1)
           ELSEIF(EPRO.GE.EA(N))THEN
               QHE=V*OA(N)*(EA(N)/EPRO)**ALFA(ISEL)
           ELSE
               DO 175 I=1,N
                   WTS(I)=1.0
                   XSA(I)=DLOG(EA(I))
  175              YSA(I)=DLOG(OA(I))
               IFAIL=0
CA
CA---------------------------------------------------------------------
CA REPLACED CALL TO NAG ROUTINE E01BAF WITH DXNBAF INSTEAD
CA - VARIABLE WTS REPRESENTS WEIGHTS - SET TO 1.0 TO GIVE ALL DATA
CA   SAME WEIGHT
CA - VARIABLE XKA REPRESENTS THE KNOTS - THESE ARE SET AS INDICATED
CA   IN THE NAG DOCUMENTATION FOR THE ORIGINAL ROUTINE E01BAF (NOTE
CA   THAT THE ROUTINE DXNBAF IS A DIRECT REPLACEMENT FOR E02BAF NOT
CA   E01BAF AND THEREFORE THE KNOTS HAVE TO BE PRESET BEFORE THE
CA   ROUTINE IS CALLED).
CA - SS CONTAINS THE RESIDUAL SUM OF SQUARES WHEN THE ROUTINE COMPLETES
CA - B,A AND DIAG ARE (I BELIEVE) VARIABLES WHICH SHOULD ACTUALLY ONLY
CA   BE INTERIOR TO THAT ROUTINE, BUT ARE INCLUDED HERE IN ORDER TO
CA   ALLOW THE CORRECT NUMBER OF ARGUMENTS FOR THE ROUTINE TO BE USED.
CA---------------------------------------------------------------------
CA
               DO 176, I=3,N-2
176                XKA(I+2) = XSA(I)
CX             CALL E01BAF(N,XSA,YSA,XKA,C,LCK,WRK,LWRK,IFAIL)
               CALL XXNBAF(N,N+4,XSA,YSA,WTS,XKA,B,A,DIAG,C,SS,IFAIL)
               XVAL=DLOG(EPRO)
CX             CALL E02BBF(N+4,XKA,C,XVAL,SVAL,IFAIL)
               CALL XXNBBF(N+4,XKA,C,XVAL,SVAL,IFAIL)
               QHE=V*DEXP(SVAL)
           ENDIF
           RETURN
       ENDIF
C-----------------------------------------------------------------------
C  COMPUTE ALPHAS AND REDUCED SPEEDS
C-----------------------------------------------------------------------
       IF(IORD.EQ.1)EMT=EM2A(ISEL)
       IF(IORD.EQ.2)EMT=EM1A(ISEL)
       V=1.38377D6*DSQRT(EPRO)
       U=1.38377D6*DSQRT(TTAR/EMT)
       VTHR=1.38377D6*DSQRT(ETHRA(ISEL)*(EM1A(ISEL)+EM2A(ISEL))/
     &(EM1A(ISEL)*EM2A(ISEL)))
       X=V/U
       XRMIN=VTHR/U
       FA(N)=ALFA(ISEL)
       DO 10 I=1,N-1
       FA(I)=-DLOG(OA(I)/OA(I+1))/DLOG(YA(I)/YA(I+1))
   10  YA(I)=1.38377D6*DSQRT(YA(I))/U
       YA(N)=1.38377D6*DSQRT(YA(N))/U
       SUM=0.0D0
       DO 100 I=1,9
       XI=XA(I)
       SXI=DSQRT(XI)
       ISWIT=1
       IF(X.LT.XRMIN)ISWIT=2
       IF(X.EQ.0.0D0)ISWIT=3
       GO TO (30,50,70),ISWIT
   30  IF(SXI.GE.0.0D0.AND.SXI.LE.X-XRMIN)F=ABI(OA,YA,FA,X-SXI,X+SXI,N)
       IF(SXI.GT.X-XRMIN.AND.SXI.LE.X+XRMIN)F=ABI(OA,YA,FA,XRMIN,X+SXI,
     & N)
       IF(SXI.GT.X+XRMIN)F=ABI(OA,YA,FA,SXI-X,X+SXI,N)
       GO TO 100
C---------------------CHANGE 31/7/90------------------------------------
C  50  XXI=XI+XRMIN**2
C-----------------------------------------------------------------------
   50  XXI=XI+(XRMIN-X)**2
       SXXI=DSQRT(XXI)
C---------------------CHANGE 31/7/90------------------------------------
C      IF(SXXI.GE.XRMIN.AND.SXXI.LE.X+XRMIN)F=ABI(OA,YA,FA,XRMIN,SXXI+X,
C    & N)
C-----------------------------------------------------------------------
       IF(SXXI.GE.XRMIN-X.AND.SXXI.LE.X+XRMIN)F=ABI(OA,YA,FA,XRMIN-X,
     & SXXI+X,N)
       IF(SXXI.GT.X+XRMIN)F=ABI(OA,YA,FA,SXXI-X,X+SXXI,N)
       GO TO 100
   70  XXI=XI+XRMIN**2
       SXXI=DSQRT(XXI)
       K=0
   75  K=K+1
       IF(K.EQ.N+1)GO TO 80
       IF(SXXI.GT.YA(K))GO TO 75
   80  K=K-1
       IF(K.EQ.0)K=1
       F=XXI*OA(K)*(YA(K)/SXXI)**(2.0D0*FA(K))
       GO TO 100
  100  SUM=SUM+F*WXA(I)
       GO TO (110,120,130),ISWIT
  110  QHE=5.64190D-1*U*SUM/X
       RETURN
C---------------------CHANGE 31/7/90------------------------------------
C 120  QHE=5.64190D-1*U*DEXP(-XRMIN*XRMIN)*SUM/X
C-----------------------------------------------------------------------
  120  QHE=5.64190D-1*U*DEXP(-(XRMIN-X)**2)*SUM/X
       RETURN
  130  QHE=1.12838D0*U*DEXP(-XRMIN*XRMIN)*SUM
       RETURN
C
 1000  FORMAT(I5)
 1001  FORMAT(13X,I2,3X,1E9.0,3X,1E9.0,3X,1E9.0,3X,1E9.0)
 1002  FORMAT(1P,6D10.3)
 1004  FORMAT(1A80)
 1005  FORMAT(1H ,1A80)
 1006  FORMAT(10I3)
 1007  FORMAT('ISEL=',I2,'  DATA SCALED FOR EXCITATION TO H(N=',
     &I1,') BY H+')
 1008  FORMAT('ISEL=',I2,'  DATA SCALED FOR EXCITATION TO H(N=',
     &I1,') BY C+6')
 1009  FORMAT('ISEL=',I2,'   DATA SCALED FOR EXCITATION TO H(N=',
     &I1,') BY Z0= ',I2)
 1010  FORMAT('ISEL=',I2,'  DATA SCALED FOR TRANSFER TO Z0=',
     &I2,'FROM H(N=',I2,')')
 1011  FORMAT('ISEL=',I2,'  DATA SCALED FOR TRANSFER TO H+  FROM H(N=',
     &I2,')')
 1012  FORMAT('ISEL=',I2,' DATA SCALED FOR TRANSFER TO C+6  FROM H(N=',
     &I2,')')
 1013  FORMAT('ISEL=',I2,'  DATA SCALED FOR TRANSFER TO Z0=',
     &I2,' FROM H(N=',I2,')')
 1015  FORMAT('ISEL=',I2,'  DATA SCALED FOR EXCITATION BY HE+2 TO H(N=',
     &         I2,')')
 1016  FORMAT('ISEL=',I2,'  DATA SCALED FOR EXCITATION BY BE+4 TO H(N=',
     &         I2,')')
 1017  FORMAT('ISEL=',I2,'  DATA SCALED FOR EXCITATION BY  C+6 TO H(N=',
     &         I2,')')
 1018  FORMAT('ISEL=',I2,'  DATA SCALED FOR EXCITATION BY  O+8 TO H(N=',
     &         I2,')')
 1019  FORMAT('ISEL=',I2,'  DATA SCALED FOR EXCITATION BY  Z0=',
     &         I2)
 1020  FORMAT('ISEL=',I2,'  DATA SCALED FOR EXCITATION BY  Z0=',
     &         I2,' TO H(N=',I2,')')
 1021  FORMAT('ISEL=',I2)
      END
