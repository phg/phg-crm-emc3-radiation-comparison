         PROGRAM ADAS311
         IMPLICIT NONE
C-------------------------------------------------------------------------
C
C ********************** FORTRAN 77 PROGRAM : ADAS311 ********************
C
C PURPOSE : CALCULATES THE QUASI-STATIC EXCITED POPULATION
C           STRUCTURE OF FAST HELIUM BEAM ATOMS. THE GROUND STATE
C           AND THE HE(2 1S), HE(2 3S) METASTABLES ARE TREATED
C           AS NON-EQUILIBRIUM LEVELS. COLLISIONAL-RADIATIVE CROSS
C           COUPLING COEFFICIENTS AND RECOMBINATION COEFFICIENTS
C           ARE ALSO CALCULATED.
C
C INPUT   :
C
C       (CHR)   TITLE           :  TYPE OF BEAM SPECIES
C       ()      MN              :  *****UNKNOWN*****
C       ()      RX3             :  *****UNKNOWN*****
C       ()      DPT             :  *****UNKNOWN*****
C       ()      EHCT            :  CRITICAL ENERGY.
C       ()      NHCT            :  CRITICAL PRINCIPAL QUANTUM NUMBER.
C       ()      LHCT            :  CRITICAL L QUANTUM NUMBER.
C       ()      NIP             : RANGE OF DELTA N FOR IMPACT PARAMETER
C                                 XSECTS. (.LE. 4)
C       ()      NEX             :  *****UNKNOWN*****
C       ()      IPRT            :  *****UNKNOWN*****
C       ()      NDEL            :  *****UNKNOWN*****
C       ()      INTD            : ORDER OF MAXWELL QUADRATURE FOR XSECTS.
C       ()      IPRS            : CONTROLS XSECTS BEYOND NIP RANGE.
C                                 0 => DEFAULT TO VAN REGEMORTER
C       ()      ILOW            : CONTROLS ACCESS OF SPECIAL LOW LEVEL DATA.
C       ()      IONIP           : CONTROLS INCLUSION OF ION IMPACT
C                                 COLLISIONS.
C                                 0 => NO ION IMPACT COLLISIONS INCLUDED.
C                                 1 => ION IMPACT EXCITATION AND IONISATION
C                                 INCLUDED.
C       ()      NIONIP          : RANGE OF DELTA N FOR ION IMPACT EXCITATION
C       ()      ILPRS           : CONTROLS USE OF LODGE-PERCIVAL-RICHARDS
C                                 XSECTS.
C                                 0 => DEFAULT TO VAINSHTEIN XSECTS.
C                                 1 => USE LODGE-PERCIVAL-RICHARDS XSECTS.
C       ()      IVDISP          : CONTROLS USE OF BEAM ENERGY IN CALCULATION
C                                 OF XSECTS.
C                                 0 => ION IMPACT AT THERMAL MAXWELLIAN
C                                      ENERGIES.
C                                1 => ION IMPACT AT DISPLACED THERMAL
C                                     ENERGIES ACCORDING TO THE NEUTRAL
C                                     BEAM ENERGY PARAMETER.
C                           NB:  IF IVDISP=0 THEN SPECIAL LOW LEVEL
C                                DATA FOR ION IMPACT IS NOT
C                                SUBSTITUTED - ONLY VAINSHTEIN AND
C                                LODGE ET AL. OPTIONS ARE OPEN.
C                                ELECTRON IMPACT DATA SUBSTITUTION
C                                DOES OCCURS.
C       ()      ZEFF            : NUCLEAR CHARGE OF IMPURITY.
C       ()      DEDEG           :  *****UNKNOWN*****
C       ()      NL1             : LOWEST N-SHELL.                       
C       ()      NL2             : MIDDLE N-SHELL.       
C       ()      NL3             : HIGHEST N-SHELL.
C       ()      Z0              : NUCLEAR CHARGE OF THE BEAM ATOMS.
C       ()      Z1              : NUCLEAR CHARGE+1
C       ()      ALF             : SCREENING PARAMETER
C       ()      AMSZ0           :  *****UNKNOWN*****
C       ()      AMSHYD          :  *****UNKNOWN*****
C       ()      LP              :  *****UNKNOWN*****
C       ()      ISP             :  *****UNKNOWN*****
C       ()      LMAX            :  *****UNKNOWN*****
C       ()      NTAIL           :  *****UNKNOWN*****
C       ()      LSWCH           :  *****UNKNOWN*****
C       ()      IGHNL           :  *****UNKNOWN*****
C       ()      IRNDGV          :  *****UNKNOWN*****
C       ()      IDIEL           :  *****UNKNOWN*****
C       ()      EIJA()          :  *****UNKNOWN*****
C       ()      FIJA()          :  *****UNKNOWN*****
C       ()      CORA()          :  *****UNKNOWN*****
C       ()      EIJSA()         :  *****UNKNOWN*****
C       ()      FIJSA()         :  *****UNKNOWN*****
C       ()      CORSA()         :  *****UNKNOWN*****
C       ()      MJ              :  NUMBER OF LEVELS TO BE MODIFIED
C                                  WITH SYTHETIC RADIATION FIELD.
C       ()      IREF()          :  REFERENCE TO LEVEL WHICH IS TO BE
C                                  MODIFIED WITH RADIATION FIELD
C       ()      JREF()          :  REFERENCE TO LEVEL WHICH IS TO BE
C                                  MODIFIED WITH RADIATION FIELD.
C       ()      SREF()          :  *****UNKNOWN*****
C       ()      WREF()          :  *****UNKNOWN*****
C       ()      CREF()          :  *****UNKNOWN*****
C       ()      WIREF()         : EXTERNAL RADIATION FIELD DILUTION FACTOR
C                                 FOR PHOTO-IONISATION FROM THE GROUND
C                                 LEVEL.
C       ()      WCREF()         : EXTERNAL RADIATION FIELD DILUTION FACTOR
C                                 FOR PHOTO-IONISATION FROM THE GROUND
C                                 LEVEL.
C       ()      MG              : NUMBER OF NON-EQUILIBRIUM LEVELS
C       ()      IRA()           : INDEX OF TEH NON-EQUILIBRIUM LEVELS.
C       ()      TS              : EXTERNAL RADIATION FIELD TEMPERATURE.
C                                 UNITS: K
C       ()      W               : EXTERNAL RADIATION FIELD DILUTION FACTOR
C                                 (HIGHER LEVELS).
C       ()      DENSH           : BEAM DENSITY.
C                                 UNITS: CM-3
C       ()      NTEMP           : NUMBER OF TEMPERATURES.
C       ()      TEA()           : ELECTRON TEMPERATURES.
C                                 UNITS: K
C       ()      TPA()           : PROTON TEMPERATURES.
C                                 UNITS: K
C       ()      TEREF           : INDEX FOR REFERENCE TEMPERATURE.
C       ()      NDENS           : NUMBER OF DENSITIES IN THE SCAN.
C       ()      DENSA()         : ELECTRON DENSITY (cm-3).
C       ()      DENSPA()        : PROTON DENSITY (cm-3)
C       ()      TDREF           : INDEX FOR REFERENCE DENSITY
C       ()      NOSCAN          : NOSCAN => 0 NO IMPURITIES
C                                 NOSCAN => 1 IMPURITIES CONSIDERED.
C       ()      NIMP            : NUMBER OF IMPURITY SPECIES.
C       ()      ZIMPA()         : NUCLEAR CHARGE OF IMPURITIES.
C       ()      AMIMPA()        : ATOMIC MASS NUMBERS OF IMPURITIES.    
C       ()      FRIMPA()        : IMPURITY FRACTIONS.
C       ()      NBENG           : NUMBER OF BEAM ENERGIES IN SCAN.
C       ()      BMENGA()        : BEAM ENERGY.
C                                 UNITS: EV/AMU
C       ()      BEREF           : REFERENCE BEAM ENERGY.
C                                 UNITS: EV/AMU
C       ()      ZP              : CHARGE OF PROTON.
C       ()      EMP             : REDUCED MASS OF AN ELECTRON.
C       ()      NGA()           :  *****UNKNOWN*****
C       ()      LGA()           :  *****UNKNOWN*****
C       ()      ISGA()          :  *****UNKNOWN*****
C       ()      NGLA()          :  *****UNKNOWN*****
C       ()      INAL()          :  *****UNKNOWN*****
C       ()      INALTG()        :  *****UNKNOWN*****
C       ()      ANAEGY()        :  *****UNKNOWN*****
C       ()      ANAFPG()        :  *****UNKNOWN*****
C       ()      NSERA()         :  *****UNKNOWN*****
C       ()      LAA()           :  *****UNKNOWN*****
C       ()      LTAA()          :  *****UNKNOWN*****
C       ()      INREP           :  *****UNKNOWN*****
C       ()      ILREP           :  *****UNKNOWN*****
C       ()      ILTREP          :  *****UNKNOWN*****
C       ()      NREP()          :  REPRESENTATIVE PRINCIPLE QUANTUM SHELLS.
C       ()      INLREP1()       :  REPRESENTATIVE L SUB-STATES.
C       ()      INLMP()         :  REPRESENTATIVE L SUB-STATES.
C
C       (CHR)   DATE            : DATE.
C       (CHR)   DSNRUN          : RUN SUMMARY DATA SET NAME.
C       (CHR)   DSNPS1          : FIRST PASSING FILE DATA SET NAME.
C       (CHR)   DSNPS2          : SECOND PASSING FILE DATA SET NAME.
C       (L*4)   LPASS1          : FLAG FOR WHETHER OR NOT 1ST PASSING FILE
C                               : HAS BEEN REQUESTED
C       (L*4)   LPASS2          : FLAG FOR WHETHER OR NOT 2ND PASSING FILE
C                                 HAS BEEN REQUESTED
C       (L*4)   LRUN            : FLAG FOR WHETHER OR NOT RUN SUMMARY FILE
C                                 HAS BEEN REQUESTED
C
C COMMUNICATION :
C
C      UNIT  DATA SET NAME           CONTENT               ROUTINE
C      ----  ----------------------  -------------------   -------
C        5   THROUGH UNIX PIPE       PRIMARY INPUT DATA
C                                    (FROM IDL SCREENS)
C        ?   SCRATCH FILE            INPUT PARAMETERS      ??????
C
C
C ROUTINES :
C
C         NAME           SOURCE        DESCRIPTION
C         ----------------------------------------
C         OUTTMP          ADAS
C         START7          ADAS
C
C NOTE : MOST OF THE ROUTINES ARE NOT YET IN AN OFFICIAL
C        ADAS FORMAT AND LACK THE APPROPRIATE DOCUMENTATION.
C
C
C AUTHOR  : HARVEY ANDERSON
C           UNIVERSITY OF STRATHCLYDE
C           ANDERSON@BARWANI.PHYS.STRATH.AC.UK
C
C VERSION : 1.2
C MODIFIED: RICHARD MARTIN & MARTIN O'MULLANE     
C DATE    : 21-03-2000
C             CHANGED ANALTG TO INALTG IN CALLS TO OUTTMP 
C
C VERSION : 1.3
C MODIFIED: Martin O'Mullane      
C DATE    : 3-6-2000
C           Tidied code to be within column 72. Call to outtmp
C           fixed, INAL was not followed by a comma.      
C
C
C VERSION : 1.4
C MODIFIED: Martin O'Mullane      
C DATE    : 18-12-2004
C             - Align with Harvey Anderson's last version.
C             - Output the input driver file to unit 2 if 
C               ldriver is set to TRUE.
C             - Some code tidying and implicit none.
C             - Add projection matrix output if requested - send 
C               to the unused pass2 output file.
C            -  The dsnps1 variable is call to start7 is replaced
C               by iups2.
C 
C-----------------------------------------------------------------------
      INTEGER     IUPS1   , IUPS2       , IURUN         , ONE          ,
     &            ZERO    , IUTMP       , IUDBG         ,
     &            PIPEIN  , PIPEOU
C-----------------------------------------------------------------------
      PARAMETER( IUPS1 = 7, IUPS2 = 10  , IURUN = 52    , ONE = 1      ,
     &           ZERO  = 0, IUTMP = 51  , IUDBG = 2     )
      PARAMETER( PIPEIN=5 , PIPEOU=6    )
C-----------------------------------------------------------------------
      INTEGER MN          , NHCT        , LHCT          , NIP          ,
     &        NEX         , IPRT        , NDEL          , INTD         ,
     &        IPRS        , ILOW        , IONIP         , NIONIP       ,
     &        ILPRS       , IVDISP      , NL1           , NL2          ,
     &        NL3         , LP          , ISP           , LMAX         ,
     &        NTAIL       , LSWCH       , IGHNL         , IRNDGV       ,
     &        MJ          , NIMP        , NBE           , NBENG        
      INTEGER INREP       , ILREP       , ILTREP        ,
     &        IDIEL       , NDT         , NDENS         , NTP          ,
     &        NTEMP       , TEREF       , TDREF         , BEREF        ,
     &        IRCODE      , NLOOPS      , NOSCAN        , ILOGIC
      integer i           , j           , k             , L1           ,
     &        mg          , ntd
C-------------------------------------------------------------------------
      LOGICAL LRUN        , LPASS1      , LPASS2        , ldriver      ,
     &        lbndl       , lproj
C-------------------------------------------------------------------------
      REAL*8  RX3         ,  DPT        , EHCT          , ZEFF         ,
     &        DEDEG       ,  Z0         , Z1            , ALF          ,
     &        AMSZ0       ,  AMSHYD     , TE            , TP           ,
     &        TS          ,  DENS       , DENSP         , W            ,
     &        BMENER      ,  DENSH      , ZP            , EMP          
C-------------------------------------------------------------------------
      CHARACTER TITLE*6   , DSLPATH*80  , DATE*8        , STITLE*80
      CHARACTER DSNRUN*80 , DSNPS1*80   , DSNPS2*80
C-------------------------------------------------------------------------
      INTEGER IREF(10)    , JREF(10)    , IRA(20)       , INAL(2,10)   ,
     &        INALTG(2,10), INBL(2,10)  , INBLT(2,10)   , INBIN0(2,10) ,
     &        INBN1(2,10) , INBN2(2,10) , INBN3(2,10)   , NREP(31)     ,
     &        INLREP1(10) , INLMP(10,10), NGA(2)        , LGA(2)       ,
     &        ISGA(2)     , NGLA(2)     , NSERA(2)      , LAA(2)       ,
     &        LTAA(2)
C-------------------------------------------------------------------------
      REAL*8  SREF(10)    , WREF(10)    , CREF(10)      , WIREF(10)    ,
     &        WCREF(10)   , ZIMPA(10)   , AMIMPA(10)    , FRIMPA(10)   ,
     &        ANAEGY(2,10), ANAFPG(2,10), ANBE1(2,10)   , ANBE2(2,10)  ,
     &        ANBE3(2,10) , EIJA(10)    , FIJA(10)      , CORA(10,6)   ,
     &        EIJSA(10)   , FIJSA(10)   , CORSA(10,6)   , BMENGA(25)   ,
     &        DENSA(25)   , DENSPA(25)  , TEA(25)       , TPA(25)
C-------------------------------------------------------------------------


C-------------------------------------------------------------------------
C GET CURRENT DATE.
C-----------------------------------------------------------------------

        CALL XXDATE(DATE)

C-----------------------------------------------------------------------
C READ GENERAL INPUT PARAMETERS.
C-----------------------------------------------------------------------

        READ(PIPEIN,'(A)') DSLPATH      

        TITLE='HELIUM'
        READ(PIPEIN,*) MN
        READ(PIPEIN,*) RX3
        READ(PIPEIN,*) DPT
        READ(PIPEIN,*) EHCT
        READ(PIPEIN,*) NHCT
        READ(PIPEIN,*) LHCT
        READ(PIPEIN,*) NIP
        READ(PIPEIN,*) NEX
        READ(PIPEIN,*) IPRT
        READ(PIPEIN,*) NDEL
       
        READ(PIPEIN,*) INTD
        READ(PIPEIN,*) IPRS
        READ(PIPEIN,*) ILOW
        READ(PIPEIN,*) IONIP
        READ(PIPEIN,*) NIONIP
        READ(PIPEIN,*) ILPRS
        READ(PIPEIN,*) IVDISP
        READ(PIPEIN,*) ZEFF
        READ(PIPEIN,*) DEDEG

        READ(PIPEIN,*) NL1              !MINIMUM N
        READ(PIPEIN,*) NL2              !MIDDLE  N
        READ(PIPEIN,*) NL3              !MAXIMUM N

        READ(PIPEIN,*) Z0
        READ(PIPEIN,*) Z1
        READ(PIPEIN,*) ALF
        READ(PIPEIN,*) AMSZ0
        READ(PIPEIN,*) AMSHYD   

        READ(PIPEIN,*) LP
        READ(PIPEIN,*) ISP

        READ(PIPEIN,*) LMAX
        READ(PIPEIN,*) NTAIL
        READ(PIPEIN,*) LSWCH
        READ(PIPEIN,*) IGHNL
        READ(PIPEIN,*) IRNDGV

        READ(PIPEIN,*) IDIEL

        DO I=1,IDIEL

           READ(PIPEIN,*)  EIJA(I)
           READ(PIPEIN,*)  FIJA(I)
           READ(PIPEIN,*)  ( CORA(I,J),J=1,6 )

        ENDDO

        DO I=1,IDIEL
           READ(PIPEIN,*) EIJSA(I)
           READ(PIPEIN,*) FIJSA(I)
           READ(PIPEIN,*) ( CORSA(I,J),J=1,6 )
        ENDDO


        READ(PIPEIN,*)  MJ


        DO K=1,MJ
           READ(PIPEIN,*) IREF(k),JREF(k),SREF(k),WREF(k),
     &     CREF(k),WIREF(k),WCREF(k)
        ENDDO


        READ(PIPEIN,*) MG
        READ(PIPEIN,*) (IRA(I),I=1,MG)

        READ(PIPEIN,*) TS
        READ(PIPEIN,*) W
        READ(PIPEIN,*) DENSH
        READ(PIPEIN,*) NTEMP

        READ(PIPEIN,*) ( TEA(NTP),NTP=1,NTEMP )
        READ(PIPEIN,*) ( TPA(NTP),NTP=1,NTEMP )

        READ(PIPEIN,*) TEREF
        
        READ(PIPEIN,*) NDENS

        READ(PIPEIN,*) ( DENSA(NDT),NDT=1,NDENS )
        READ(PIPEIN,*) ( DENSPA(NDT),NDT=1,NDENS )
        READ(PIPEIN,*) TDREF

        READ(PIPEIN,*) NOSCAN
        IF ( NOSCAN .EQ. 1 ) THEN
           READ(PIPEIN,*) NIMP
           DO I=1,NIMP
             READ(PIPEIN,*) ZIMPA(I),AMIMPA(I),FRIMPA(I)
           ENDDO
        ELSE
           NIMP = 0
        ENDIF   

        READ(PIPEIN,*) NBENG
        READ(PIPEIN,*) ( BMENGA(NBE), NBE=1,NBENG )
        READ(PIPEIN,*) BEREF

        READ(PIPEIN,*) ZP
        READ(PIPEIN,*) EMP

        READ(PIPEIN,*) NGA(1)
        READ(PIPEIN,*) LGA(1)
        READ(PIPEIN,*) ISGA(1)
        READ(PIPEIN,*) NGLA(1)

        DO I=1,NGLA(1)
           READ(PIPEIN,*) INAL(1,I), INALTG(1,I),ANAEGY(1,I),
     &                    ANAFPG(1,I)
        ENDDO

        READ(PIPEIN,*) NSERA(1)
        READ(PIPEIN,*) LAA(1)
        READ(PIPEIN,*) LTAA(1)

        DO I=1,NSERA(1)
          READ(PIPEIN,*) INBL(1,I),INBLT(1,I),INBIN0(1,I),INBN1(1,I),
     &                ANBE1(1,I),INBN2(1,I),ANBE2(1,I),INBN3(1,I),
     &                ANBE3(1,I)
        ENDDO

        IF ( ISP.GT.1 ) THEN
           READ(PIPEIN,*) NGA(2)
           READ(PIPEIN,*) LGA(2)
           READ(PIPEIN,*) ISGA(2)
           READ(PIPEIN,*) NGLA(2)

             DO  I=1,NGLA(2)
                READ(PIPEIN,*) INAL(2,I), INALTG(2,I),ANAEGY(2,I),
     &                         ANAFPG(2,I)
             ENDDO

           READ(PIPEIN,*) NSERA(2)
           READ(PIPEIN,*) LAA(2)
           READ(PIPEIN,*) LTAA(2)

           DO I=1,NSERA(2)
              READ(PIPEIN,*) INBL(2,I),INBLT(2,I),INBIN0(2,I),
     &        INBN1(2,I),ANBE1(2,I), INBN2(2,I),ANBE2(2,I),
     &        INBN3(2,I),ANBE3(2,I)
           ENDDO
        ENDIF

        READ(PIPEIN,*) INREP
        READ(PIPEIN,*) ILREP
        READ(PIPEIN,*) ILTREP

        READ(PIPEIN,*) (NREP(I),I=1,INREP)

        DO I=1,NL2
            READ(PIPEIN,*) INLREP1(I)
            READ(PIPEIN,*) ( INLMP(I,L1),L1=1,INLREP1(I) )
        ENDDO



C-------------------------------------------------------------------------
C Write input file for offline version
C-------------------------------------------------------------------------

        ldriver = .FALSE.

        if (ldriver) then
           write(iudbg,'(a)') title
           write(iudbg,101)  mn,rx3,dpt,ehct,nhct,lhct,
     &                       nip,nex,iprt,ndel
           write(iudbg,102)  intd,iprs,ilow,ionip,nionip,ilprs,ivdisp,
     &                  zeff,dedeg
           write(iudbg,103)  nl1,nl2,nl3
           write(iudbg,112)  z0,z1,alf,amsz0,amshyd
           write(iudbg,103)  lp,isp
           write(iudbg,103)  lmax,ntail,lswch,ighnl,irndgv
           write(iudbg,103)  idiel
           do i=1,idiel
             write(iudbg,112) eija(i),fija(i),(cora(i,j),j=1,6)
           enddo
           do i=1,idiel
             write(iudbg,112) eijsa(i),fijsa(i),(corsa(i,j),j=1,6)
           enddo
           write(iudbg,104)  mj
           do k=1,mj
                   write(iudbg,105) iref(k),jref(k),sref(k),wref(k),
     &             cref(k),wiref(k),wcref(k)
     
           enddo
           write(iudbg,104) mg
           write(iudbg,106) (ira(i),i=1,mg)
           write(iudbg,107) ts,w,densh
           write(iudbg,103) ntemp
           write(iudbg,107) ( tea(ntp),ntp=1,ntemp )
           write(iudbg,107) ( tpa(ntp),ntp=1,ntemp )
           write(iudbg,103) teref
           write(iudbg,103) ndens
           write(iudbg,107) ( densa(ndt),ndt=1,ndens )
           write(iudbg,107) ( denspa(ndt),ndt=1,ndens )
           write(iudbg,103) tdref
           if ( densa(1) .gt. denspa(1) ) then
              write(iudbg,104) nimp
                   do i=1,nimp
                     write(iudbg,108) zimpa(i),amimpa(i),frimpa(i)
                   enddo
           endif
           write(iudbg,103) nbeng
           write(iudbg,107) ( bmenga(nbe), nbe=1,nbeng )
           write(iudbg,103) beref
           write(iudbg,109)zp,emp
           write(iudbg,106) nga(1),lga(1),isga(1),ngla(1)
           do  i=1,ngla(1)                                                
              write(iudbg,110) inal(1,i), inaltg(1,i),anaegy(1,i), 
     &                    anafpg(1,i)
           enddo
           write(iudbg,111) nsera(1),laa(1),ltaa(1)
           do i=1,nsera(1)                                               
             write(iudbg,111) inbl(1,i),inblt(1,i),inbin0(1,i),
     &                   inbn1(1,i),anbe1(1,i),inbn2(1,i),anbe2(1,i),
     &                   inbn3(1,i),anbe3(1,i)
           enddo
           if ( isp.gt.1 ) then
              write(iudbg,106) nga(2),lga(2),isga(2),ngla(2)
                do  i=1,ngla(2)                                                
                   write(iudbg,110) inal(2,i), inaltg(2,i),anaegy(2,i), 
     &                         anafpg(2,i)
                enddo
              write(iudbg,111) nsera(2),laa(2),ltaa(2)
              do i=1,nsera(2)                                               
              write(iudbg,111) inbl(2,i),inblt(2,i),inbin0(2,i),
     &                         inbn1(2,i),anbe1(2,i), inbn2(2,i),
     &                         anbe2(2,i),inbn3(2,i),anbe3(2,i)
              enddo
           endif
           write(iudbg,103)inrep,ilrep,iltrep
           write(iudbg,103)(nrep(i),i=1,inrep)
           do i=1,nl2
            write(iudbg,103) inlrep1(i)
            write(iudbg,103) (inlmp(i,l1),l1=1,inlrep1(i) )
           enddo

        endif


C-------------------------------------------------------------------------
C GET VALUES FROM THE IDL OUTPUT SCREEN
C-------------------------------------------------------------------------

      READ(PIPEIN,'(A)') STITLE
      READ(PIPEIN,*) ILOGIC
      IF (ILOGIC.EQ.1) THEN
          LRUN = .TRUE.
      ELSE
          LRUN = .FALSE.
      ENDIF
      
      READ(PIPEIN,*) ILOGIC
      IF (ILOGIC.EQ.1) THEN
          LPASS1 = .TRUE.
      ELSE
          LPASS1 = .FALSE.
      ENDIF
      
      READ(PIPEIN,*) ILOGIC
      IF (ILOGIC.EQ.1) THEN
          LPASS2 = .TRUE.
      ELSE
          LPASS2 = .FALSE.
      ENDIF

      READ(PIPEIN,'(A)') DSNRUN
      READ(PIPEIN,'(A)') DSNPS1
      READ(PIPEIN,'(A)') DSNPS2
       
C-------------------------------------------------------------------------
C OPEN OUTPUT DATA SETS
C-------------------------------------------------------------------------


      CALL CBPASF( IUPS1  , IUPS2  , IURUN , DSNPS1 , DSNPS2 ,
     &             DSNRUN , LPASS1 , LPASS2, LRUN   , IRCODE   )


C-----------------------------------------------------------------------
C SEND SIGNAL TO IDL TO CONTINUE OR STOP
C-----------------------------------------------------------------------

      IF (IRCODE.EQ.1) THEN
          WRITE( PIPEOU, *) ONE
          CALL XXFLSH( PIPEOU )
      ELSE
          WRITE( PIPEOU, *) ZERO
          CALL XXFLSH( PIPEOU )
      ENDIF


C-----------------------------------------------------------------------
C COMMUNICATE TO IDL NO. OF LOOPS
C-----------------------------------------------------------------------

      WRITE( PIPEOU, *) NDENS
      CALL XXFLSH( PIPEOU )
      WRITE( PIPEOU, *) NTEMP
      CALL XXFLSH( PIPEOU )
      WRITE( PIPEOU, *) NBENG
      CALL XXFLSH( PIPEOU )

        
C-----------------------------------------------------------------------
C START OF MAIN LOOP
C
C       1. AT REFERENCE TEMPERATURE LOOP AROUND EACH DENSITY
C          FOR A FIXED BEAM ENERGY.
C
C       2. AT REFERENCE DENSITY AND BEAM ENERGY LOOP AROUND
C          EACH TEMPERATURE
C
C-----------------------------------------------------------------------

      if (lpass1) then
      
        lbndl = .TRUE.
        lproj = .FALSE.
        
        DO NBE = 1, NBENG
          DO NTD = 1, NDENS
             
             DENS   = DENSA(NTD)
             DENSP  = DENSPA(NTD)
             BMENER = BMENGA(NBE)
             TE     = TEA(TEREF)
             TP     = TPA(TEREF)

             WRITE( PIPEOU, *) ONE
             CALL XXFLSH( PIPEOU )

             CALL OUTTMP(TITLE,MN,RX3,DPT,EHCT,NHCT,LHCT,NIP,NEX,IPRT,
     &       NDEL,INTD,IPRS,ILOW,IONIP,NIONIP,ILPRS,IVDISP,ZEFF,
     &       DEDEG,NL1,NL2,NL3,Z0,Z1,ALF,AMSZ0,AMSHYD,LP,ISP,LMAX,
     &       NTAIL,LSWCH,IGHNL,IRNDGV,MJ,IREF,JREF,SREF,WREF,CREF,
     &       WIREF,WCREF,MG,IRA,TE,TP,TS,DENS,DENSP,W,BMENER,DENSH,
     &       NIMP,ZIMPA,AMIMPA,FRIMPA,ZP,EMP,NGA,LGA,ISGA,NGLA,INAL,
     &       INALTG,ANAEGY,ANAFPG,NSERA,LAA,LTAA,INBL,INBLT,INBIN0,
     &       INBN1,ANBE1,INBN2,ANBE2,INBN3,ANBE3,INREP,ILREP,ILTREP,
     &       NREP,INLREP1,INLMP,IUTMP,IDIEL,EIJA,FIJA,CORA,EIJSA,
     &       FIJSA,CORSA)


             CALL START7(IUTMP,IUPS1,IUPS2,STITLE,DSLPATH,
     &                   NBENG,NTEMP,NDENS, lbndl, lproj)

          ENDDO
        ENDDO

        IF ( NTEMP .GT. 1.AND.NBENG.GE.1.OR.NDENS.GE.1 ) THEN
           
           DO NTP = 1, NTEMP
             DENS   = DENSA(TDREF)
             DENSP  = DENSPA(TDREF)
             BMENER = BMENGA(BEREF)
             TE     = TEA(NTP)
             TP     = TPA(NTP)

             WRITE( PIPEOU, *) ONE
             CALL XXFLSH( PIPEOU )

             CALL OUTTMP(TITLE,MN,RX3,DPT,EHCT,NHCT,LHCT,NIP,NEX,IPRT,
     &       NDEL,INTD,IPRS,ILOW,IONIP,NIONIP,ILPRS,IVDISP,ZEFF,
     &       DEDEG,NL1,NL2,NL3,Z0,Z1,ALF,AMSZ0,AMSHYD,LP,ISP,LMAX,
     &       NTAIL,LSWCH,IGHNL,IRNDGV,MJ,IREF,JREF,SREF,WREF,CREF,
     &       WIREF,WCREF,MG,IRA,TE,TP,TS,DENS,DENSP,W,BMENER,DENSH,
     &       NIMP,ZIMPA,AMIMPA,FRIMPA,ZP,EMP,NGA,LGA,ISGA,NGLA,INAL,
     &       INALTG,ANAEGY,ANAFPG,NSERA,LAA,LTAA,INBL,INBLT,INBIN0,
     &       INBN1,ANBE1,INBN2,ANBE2,INBN3,ANBE3,INREP,ILREP,ILTREP,
     &       NREP,INLREP1,INLMP,IUTMP,IDIEL,EIJA,FIJA,CORA,EIJSA,
     &       FIJSA,CORSA)

             CALL START7(IUTMP,IUPS1,IUPS2,STITLE,DSLPATH,
     &                   NBENG,NTEMP,NDENS, lbndl, lproj)

           ENDDO
        
        ENDIF

      endif
     
C-----------------------------------------------------------------------
C Write projection file if requested.
C
C The loops over Te/Ne/Energy are not the same as in the adf26 file.
C-----------------------------------------------------------------------

      if ( lpass2 ) then
      
        lbndl = .FALSE.
        lproj = .TRUE.

        DO NTP = 1, NTEMP    
          DO NTD = 1, NDENS
         
            DENS   = DENSA(NTD)
            DENSP  = DENSPA(NTD)
            BMENER = BMENGA(BEREF)
            TE     = TEA(NTP)
            TP     = TPA(NTP)

            WRITE( PIPEOU, *) ONE
            CALL XXFLSH( PIPEOU )
             
            CALL OUTTMP(TITLE,MN,RX3,DPT,EHCT,NHCT,LHCT,NIP,NEX,IPRT,
     &         NDEL,INTD,IPRS,ILOW,IONIP,NIONIP,ILPRS,IVDISP,ZEFF,
     &         DEDEG,NL1,NL2,NL3,Z0,Z1,ALF,AMSZ0,AMSHYD,LP,ISP,LMAX,
     &         NTAIL,LSWCH,IGHNL,IRNDGV,MJ,IREF,JREF,SREF,WREF,CREF,
     &         WIREF,WCREF,MG,IRA,TE,TP,TS,DENS,DENSP,W,BMENER,DENSH,
     &         NIMP,ZIMPA,AMIMPA,FRIMPA,ZP,EMP,NGA,LGA,ISGA,NGLA,INAL,
     &         INALTG,ANAEGY,ANAFPG,NSERA,LAA,LTAA,INBL,INBLT,INBIN0,
     &         INBN1,ANBE1,INBN2,ANBE2,INBN3,ANBE3,INREP,ILREP,ILTREP,
     &         NREP,INLREP1,INLMP,IUTMP,IDIEL,EIJA,FIJA,CORA,EIJSA,
     &         FIJSA,CORSA)

            CALL START7(IUTMP,IUPS2,IUPS2,STITLE,DSLPATH,
     &                  NBENG,NTEMP,NDENS, lbndl, lproj)

          END DO
        END DO
   
        IF ( NBENG .GT. 1.AND.NTEMP.GE.1.AND.NDENS.GE.1 ) THEN
   
           DO NBE = 1, NBENG
              
             DENS   = DENSA(TDREF)
             DENSP  = DENSPA(TDREF)
             BMENER = BMENGA(NBE)
             TE     = TEA(TEREF)
             TP     = TPA(TEREF)

             WRITE( PIPEOU, *) ONE
             CALL XXFLSH( PIPEOU )

             CALL OUTTMP(TITLE,MN,RX3,DPT,EHCT,NHCT,LHCT,NIP,NEX,IPRT,
     &          NDEL,INTD,IPRS,ILOW,IONIP,NIONIP,ILPRS,IVDISP,ZEFF,
     &          DEDEG,NL1,NL2,NL3,Z0,Z1,ALF,AMSZ0,AMSHYD,LP,ISP,LMAX,
     &          NTAIL,LSWCH,IGHNL,IRNDGV,MJ,IREF,JREF,SREF,WREF,CREF,
     &          WIREF,WCREF,MG,IRA,TE,TP,TS,DENS,DENSP,W,BMENER,DENSH,
     &          NIMP,ZIMPA,AMIMPA,FRIMPA,ZP,EMP,NGA,LGA,ISGA,NGLA,INAL,
     &          INALTG,ANAEGY,ANAFPG,NSERA,LAA,LTAA,INBL,INBLT,INBIN0,
     &          INBN1,ANBE1,INBN2,ANBE2,INBN3,ANBE3,INREP,ILREP,ILTREP,
     &          NREP,INLREP1,INLMP,IUTMP,IDIEL,EIJA,FIJA,CORA,EIJSA,
     &          FIJSA,CORSA)

             CALL START7(IUTMP,IUPS2,IUPS2,STITLE,DSLPATH,
     &                   NBENG,NTEMP,NDENS, lbndl, lproj)

    
           END DO
           
        ENDIF    
      
      endif

C-----------------------------------------------------------------------
C WRITE SUMMARY OUTPUT FILE IF REQUESTED
C-----------------------------------------------------------------------


      IF ( LRUN ) THEN
          WRITE(IURUN,4001) DSNRUN
          WRITE(IURUN,*) ' '
          WRITE(IURUN,4002) DSNPS1
          WRITE(IURUN,4003) DSNPS2
          WRITE(IURUN,*) ' '
          WRITE(IURUN,*) '   NUMBER OF BEAM ENERGIES'
          WRITE(IURUN,*) NBENG
          WRITE(IURUN,*) '   NUMBER OF DENSITIES'
          WRITE(IURUN,*) NDENS
          WRITE(IURUN,*) '   NUMBER OF TEMPERATURES '
          WRITE(IURUN,*) NTEMP
      ENDIF


C-----------------------------------------------------------------------
C CLOSE I/O FILES.
C-----------------------------------------------------------------------

      CLOSE( IUPS1 )
      CLOSE( IUPS2 )
      CLOSE( IURUN )

C-------------------------------------------------------------------------
C COMMUNICATE WITH IDL : FORTRAN IS COMPLETE
C-------------------------------------------------------------------------
     
      WRITE( PIPEOU, *) ONE
      CALL XXFLSH( PIPEOU )

C-------------------------------------------------------------------------
 101  FORMAT(I5,3F10.5,6I5)
 102  FORMAT(7I5,F5.1,F10.5)
 103  FORMAT(16I5)
 104  FORMAT(1I5)
 105  FORMAT(2I5,1P,5E12.4)
 106  FORMAT(20I5)
 107  FORMAT(6E12.4)
 108  FORMAT(3E12.4)
 109  FORMAT(2E12.4)
 110  FORMAT(2I5,F15.10,F10.5)
 111  FORMAT(3I5,3(I5,F15.10))
 112  FORMAT(8F10.5)
4001  FORMAT(1H ,4X,'RUN SUMMARY DATA SET NAME    : ',A80)
4002  FORMAT(1H ,4X,'PASSING FILES DATA SET NAMES : ',A80)
4003  FORMAT(1H ,34X,A80)
C-------------------------------------------------------------------------
      END
