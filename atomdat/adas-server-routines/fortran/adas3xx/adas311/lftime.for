CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas311/lftime.for,v 1.3 2007/05/17 17:04:12 allan Exp $ Date $Date: 2007/05/17 17:04:12 $
CX
       SUBROUTINE LFTIME(ZEFF,N,L,NMIN,TAU)
       IMPLICIT REAL*8(A-H,O-Z)
C-----------------------------------------------------------------------
C
C  PURPOSE: EVALUATES RADIATIVE LIFETIME OF AN NL LEVEL OF A H-LIKE ION.
C
C  **************** H.P. SUMMERS, JET    22 JAN 1985  ******************
C  *** CORRECTIONS 13/5/85
C  *** CORRECTIONS 22/5/85
C  INPUT
C      ZEFF=EFFECTIVE CHARGE FOR OUTER ELECTRON
C      N=PRINCIPAL QUANTUM NUMBER FOR OUTER ELECTRON
C      L=ORBITAL ANGULAR MOMENTUM QUANTUM NUMBER FOR OUTER ELECTRON
C      NMIN=LOWEST ACCESSIBLE PRINCIPAL QUANTUM NUMBER BY DIPOLE
C           TRANSITION
C  OUTPUT
C      TAU=LIFETIME (SEC)
C
C VERSION  : 1.2
C DATE     : 16-05-2007
C MODIFIED : Allan Whiteford
C             - Updated comments as part of subroutine documentation
C               procedure.
C
C-----------------------------------------------------------------------
       IF(N-NMIN)5,5,10
    5  TAU=1.0D10
       RETURN
   10  NMAX=N-1
       XN=N
       XL=L
       T1=2.67744D9*ZEFF**4/(2.0D0*XL+1.0D0)
       SUM=0.0D0
       DO 20 N1=NMIN,NMAX
       XN1=N1
       DE=1.0D0/XN1**2-1.0D0/XN**2
       DE3=DE**3
       L1=L+1
       XL1=L1
       IF(L1.GE.N1)GO TO 15
       SUM=SUM+XL1*T1*DE3*RD2BS(N1,L1,N,L)
   15  L1=L-1
       XL1=L1
       IF(L1.LT.0.OR.L1.GE.N1)GO TO 20
       SUM=SUM+XL*T1*DE3*RD2BS(N1,L1,N,L)
   20  CONTINUE
       IF(SUM.LE.1.0D-10)GOTO 5
       TAU=1.0D0/SUM
       RETURN
      END
