CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas311/nwripv.for,v 1.3 2007/05/17 17:04:12 allan Exp $ Date $Date: 2007/05/17 17:04:12 $
CX
       SUBROUTINE NWRIPV(IZ,WI,EI,WJ,EJ,M,PHI,EPS,OMEG,N,TVA,EM,IZC,RAT,
     &QI,QJ,GA)
C-----------------------------------------------------------------------
C   PURPOSE: CALCULATES ELECTRON & POSITIVE ION COLL. EXCITATION AND
C            DEEXCITATION RATE COEFFICIENTS FOR DIPOLE TRANSITIONS
C            IN THE IMPACT PARAMETER APPROXIMATION
C
C   (BURGESS AND SUMMERS,1976,MON.NOT.R.AST.SOC.,174,345)
C
C   OPTIONALLY A SET OF INCIDENT PARTICLE ENERGIES AND COLLISION
C   STRENGTHS MAY BE PROVIDED, IN WHICH CASE THE IMPACT PARAMETER THEORY
C   IS USED TO CALCULATE THE COLLISION STRENGTHS AT HIGH ENERGY
C   WITH VALUES SCALED TO THE HIGHEST ENERGY INPUT COLLISION STRENGTH.
C  **************** H.P. SUMMERS, JET   22 JAN 1985  *******************
C
C  ****************   ORIGINAL VERSION WITHOUT THE   *******************
C  ****************  STATISTICAL WEIGHT (WT) CHANGE  *******************
C
C  INPUT
C       IZ=ION CHARGE
C       WI=STATISTICAL WEIGHT OF STATE I
C       EI=BINDING ENERGY OF STATE I (RYD)
C       WJ=STATISTICAL WEIGHT OF STATE J
C       EJ=BINDING ENERGY OF STATE J (RYD)
C       M=NUMBER OF TABULAR VALUES OF COLLISION STRENGTH
C       PHI=FIJ/EIJ WITH FIJ ABSORPTION OSCILLATOR STRENGTH
C                        EIJ=EI-EJ THE TRANSITION ENERGY (RYD)
C       EPS(K)=INCIDENT ELECTRON ENERGIES (RYDBERGS)
C       OMEG(K)=COLLISION STRENGTHS
C       N=NUMBER OF TEMPERATURES
C       TVA(I)=TEMPERATURES (EV) (INCIDENT PARTICLE DISTRIBUTION)
C       EM=REDUCED MASS FOR COLLIDING PARTICLE (ELECTRON MASSES)
C       IZC=CHARGE OF COLLIDING PARTICLE
C  OUTPUT
C       RAT=RATIO OF OMEG(M) TO I.P. OMEGA.
C       QI(I)=COLLISIONAL EXCITATION RATE COEFFICIENTS  (CM**3 SEC-1)
C       QJ(I)=COLLISIONAL DEEXCITATION RATE COEFFICIENTS.
C       GA(I)=GAMMA RATE PARAMETERS
C-----------------------------------------------------------------------
C  AUTHOR
C   HUGH SUMMERS     1977/5/20
C  UPDATES
C       1983/9/1, 1984/6/25
C  COMMENTS
C   I IS THE LOWER LEVEL OF THE TRANSITION.
C   M MAY BE ZERO, IN WHICH CASE NO EPS AND OMEG VALUES ARE REQUIRED.
C   UNDERFLOW IS NOT TRAPPED. THIS MAY BE ACHEIVED IN IBM FORTRAN WITH T
C   MODIFIED IPRATE TO ALLOW PROTON RATES. EM TAKEN AS INPUT AND
C   PHI ACCEPTED INPLACE OF AJI ON INPUT.
C-----------------------------------------------------------------------
C VERSION  : 1.1
C DATE     : 18-03-1999
C MODIFIED : ???
C
C VERSION  : 1.2
C DATE     : 05-10-2000
C MODIFIED : ???
C             - Removed junk from columns > 72
C
C VERSION  : 1.3
C DATE     : 16-05-2007
C MODIFIED : Allan Whiteford
C             - Updated comments as part of subroutine documentation
C               procedure.
C
C-----------------------------------------------------------------------
       IMPLICIT REAL*8(A-H,O-Z)
       DIMENSION A(40),EPS(20),OMEG(20),T(40),TVA(40)
       DIMENSION X(4),H(4)
       DIMENSION GA(40),QI(40),QJ(40)
C-----------------------------------------------------------------------
       X(1)=0.322547689619D0
       X(2)=1.745761101158D0
       X(3)=4.536620296921D0
       X(4)=9.395070912301D0
       H(1)=6.03154104342D-1
       H(2)=3.57418692438D-1
       H(3)=3.88879085150D-2
       H(4)=5.39294705561D-4
       Z1=IZ+1
       ZC=IZC
       ZC=DABS(ZC)
       EIJ=EI-EJ
       IF(EIJ.GT.1.0D-10)GO TO 24
       DO 23 I=1,N
       QI(I)=0.0D0
       QJ(I)=0.0D0
   23  GA(I)=0.0D0
c       WRITE(6,8895)                                                   0
 8895  FORMAT(' CHECK ON NWRIPV :- ')
c       WRITE(6,8890)                                                   0
c       WRITE(6,8883)IZ,N,IZC,M                                         0
c       WRITE(6,8880)                                                   0
c       WRITE(6,8881)WI,EI,WJ,EJ,PHI                                    0
c       WRITE(6,8885)                                                   0
c       WRITE(6,8882)EM,RAT,QI(1),QJ(1),GA(1),TVA(1)                    0
 8890  FORMAT('   IZ    N   IZC   M')
 8880  FORMAT('      WI          EI          WJ          EJ         PHI'
     *)
 8885  FORMAT('      EM          RAT        QI(1)       QJ(1)      GA(1)
     *       TVA(1)')
 8883  FORMAT(4I5)
 8881  FORMAT(1P13E12.4)
 8882  FORMAT(1P13E12.4)
       RETURN
   24  ELAM=9.11763D2/EIJ
       ZCOL=ZC*(Z1-1.0D0)
       Z2PHI=ZC*ZC*PHI
       SC=1.0D0
       T2=1.0D0
       EPSM=EIJ
       R=1.25D0*Z1/EI+0.25/Z1
       IF(M)4,4,3
    3  CONTINUE
       M1=M-1
       EPSM=EPS(M)
       CALL EIQIP(EPSM,EIJ,EM,ZCOL,Z2PHI,SC,WI,WJ,R,EIQ,FLAG)
       T2=WI*EIQ
       IF(M1)7,7,8
    7  EPSM=EIJ
       GO TO 9
    8  CONTINUE
    9  RAT=OMEG(M)/T2
       T2=WI*OMEG(M)/T2
    4  CONTINUE
       DO 20 I=1,N
       T(I)=1.16054D4*TVA(I)
       QI(I)=0.0D0
       ATE=157890.0D0/T(I)
       T1=T2*DEXP(-ATE*(EPSM-EIJ))
       S=0.0D0
       M1=M-2
       IF(M1)10,1,1
    1  S=OMEG(1)
       U1=ATE*(EPS(1)-EIJ)
       G1=1.0D0-U1
       U2=ATE*(EPS(2)-EIJ)
       G2=DEXP(-U2)
       B=(OMEG(2)-OMEG(1))/(U2-U1)
       IF(M1)6,6,2
    2  DO 5 K=1,M1
       S=S+B*(G1-G2)
       U1=U2
       G1=G2
       U2=ATE*(EPS(K+2)-EIJ)
       G2=DEXP(-U2)
    5  B=(OMEG(K+2)-OMEG(K+1))/(U2-U1)
    6  S=S+B*(G1-G2)-OMEG(M)*G2
   10  S1=0.0D0
       DO 15 K=1,4
       E=X(K)/ATE+EPSM
       CALL EIQIP(E,EIJ,EM,ZCOL,Z2PHI,SC,WI,WJ,R,EIQ,FLAG)
   15  S1=S1+H(K)*EIQ
       S=S+T1*S1
       P=6.8916111D-2*S/(WI*PHI)
       QJ(I)=2.171609D-8*DSQRT(ATE/EM)*S/WJ
       GA(I)=4.604D7*WJ*QJ(I)/DSQRT(ATE)
       IF(ATE*EIJ.LE.-70.0D0)GO TO 20
       QI(I)=QJ(I)*WJ*DEXP(-ATE*EIJ)/WI
   20  CONTINUE
c       WRITE(6,8895)
c       WRITE(6,8890)
c       WRITE(6,8883)IZ,N,IZC,M
c       WRITE(6,8880)
c       WRITE(6,8881)WI,EI,WJ,EJ,PHI
c       WRITE(6,8885)
c       WRITE(6,8882)EM,RAT,QI(1),QJ(1),GA(1),TVA(1)
       RETURN
      END
