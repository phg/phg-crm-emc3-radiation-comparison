CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas311/abcdh.for,v 1.3 2005/02/22 19:14:20 mog Exp $ Date $Date: 2005/02/22 19:14:20 $
CX
C-------------------------------------------------------------------------
C	VERSION : 1.2
C	MODIFIED: Martin O'Mullane      
C       DATE    : 3-6-2000
C		  One too many parameters for rwfh - remove last one. 	
C
C	VERSION : 1.3
C	MODIFIED: Martin O'Mullane      
C       DATE    : 08-11-2004
C		  Alter dimensions from 200 to 500. 	
C
C-------------------------------------------------------------------------
       FUNCTION ABCDH(NI1,LI1,NI2,LI2,NJ1,LJ1,NJ2,LJ2,LAM,ZZ1,ZZ2)
       IMPLICIT REAL*8(A-H,O-Z)
       DIMENSION A(500),B(500),C(500),D(500),X1(500),X2(500)
       DIMENSION Y1(500),Y2(500),Z1(500),Z2(500)
       CALL RWFH(A,NA,IPA,UA,NI1,LI1)
C      WRITE(6,*)NA,IPA,UA,(A(I),I=1,NA)
       CALL RWFH(B,NB,IPB,UB,NI2,LI2)
C      WRITE(6,*)NB,IPB,UB,(B(I),I=1,NB)
       CALL RWFH(C,NC,IPC,UC,NJ1,LJ1)
C      WRITE(6,*)NC,IPC,UC,(C(I),I=1,NC)
       CALL RWFH(D,ND,IPD,UD,NJ2,LJ2)
C      WRITE(6,*)ND,IPD,UD,(D(I),I=1,ND)
       CALL PSPROD(B,NB,IPB,D,ND,IPD,X2,NX2,IPX2)
       UX2=UB+UD
       IPX2=IPX2-LAM-1
C      WRITE(6,*)NX2,IPX2,UX2,(X2(I),I=1,NX2)
       CALL PSINT(X2,NX2,IPX2,UX2,X1,NX1,IPX1,UX1)
C      WRITE(6,*)NX1,IPX1,UX1,(X1(I),I=1,NX1)
       IPX1=IPX1+LAM+LAM+1
       IPX2=IPX2+LAM+LAM+1
       CALL PSINT(X2,NX2,IPX2,UX2,Y1,NY1,IPY1,UY1)
C      WRITE(6,*)NY1,IPY1,UY1,(Y1(I),I=1,NY1)
       T1=Y1(1)
       CALL PSSUM(1.0D0,Y1,NY1,IPY1,-1.0D0,X1,NX1,IPX1,X2,NX2,IPX2)
C      WRITE(6,*)NX2,IPX2,UX2,(X2(I),I=1,NX2)
       CALL PSPROD(A,NA,IPA,C,NC,IPC,Z2,NZ2,IPZ2)
       UZ2=UA+UC
       IPZ2=IPZ2-LAM-1
C      WRITE(6,*)NZ2,IPZ2,UZ2,(Z2(I),I=1,NZ2)
       CALL PSPROD(Z2,NZ2,IPZ2,X2,NX2,IPX2,X1,NX1,IPX1)
       UX1=UX2+UZ2
C      WRITE(6,*)NX1,IPX1,UX1,(X1(I),I=1,NX1)
       CALL PSINT(X1,NX1,IPX1,UX1,X2,NX2,IPX2,UX2)
C      WRITE(6,*)NX2,IPX2,UX2,(X2(I),I=1,NX2)
       CALL PSINT(Z2,NZ2,IPZ2,UZ2,Z1,NZ1,IPZ1,UZ1)
C      WRITE(6,*)NZ1,IPZ1,UZ1,(Z1(I),I=1,NZ1)
       ABCDH=-X2(1)+T1*Z1(1)
       RETURN
      END
