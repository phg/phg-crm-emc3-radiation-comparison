CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas311/gamaf.for,v 1.3 2004/07/06 13:57:59 whitefor Exp $ Date $Date: 2004/07/06 13:57:59 $
CX
C
C	Version:  1.3
C	Modified: Richard Martin			18-03-03	
C			Increased dimensions to 500.
C
C
       SUBROUTINE GAMAF(NMAX)
       IMPLICIT REAL*8(A-H,O-Z)
       DIMENSION GAM(500),JGAM(500)
       COMMON /FAC/ GAM,JGAM
       JGAM(1)=0
       GAM(1)=1.0
       DO 4 N=2,NMAX
       N1=N-1
       X=N1
       X1=X*GAM(N1)
       J1=JGAM(N1)
    1  IF(X1-64.0)3,2,2
    2  X1=0.015625*X1
       J1=J1+1
       GO TO 1
    3  GAM(N)=X1
       JGAM(N)=J1
    4  CONTINUE
       RETURN
      END
