CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas301/c1bsig.for,v 1.3 2007/05/24 15:27:21 mog Exp $ Data $Date: 2007/05/24 15:27:21 $
CX
      SUBROUTINE C1BSIG( NENRGY , NSHELL , NENER ,
     &                   INSEL  , ILSEL  ,
     &                   IEDATA , NDATA  ,
     &                   XTOT   , XSIGN  , XSIGL ,
     &                   ALPHAA ,
     &                   SIGA
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C1BSIG *********************
C
C  PURPOSE:  TO EXTRACT VALID CROSS-SECTIONS FROM INPUT CHARGE-EXCHANGE
C            FILE FOR A GIVEN SUB-BLOCK.
C
C  CALLING PROGRAM: ADAS301
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  NENRGY   = NUMBER OF TABULATED INPUT ENERGIES.
C  INPUT : (I*4)  NSHELL   = NO. OF TABULATED PRINCIPAL QUANTUM N-SHELLS
C
C  INPUT : (I*4)  NENER    = NUMBER OF VALID ENERGIES/VEL. FOR SUB-BLOCK
C  INPUT : (I*4)  INSEL    = SELECTED INPUT PRINCIPAL QUANTUM N-SHELL
C                            ( 0 => TOTAL CROSS SECTION).
C  INPUT : (I*4)  ILSEL    = SELECTED INPUT DATA L QUANTUM SHELL
C                            ( -1   => TOTAL CROSS-SECTION FOR N SHELL)
C  INPUT : (I*4)  IMSEL    = SELECTED INPUT DATA M QUANTUM SHELL
C                            ( -1   => TOTAL CROSS-SECTION FOR NL SHELL)
C
C  INPUT : (I*4)  IEDATA() = INDEX RANGE FOR VALID QUANTUM NUMBERS IN
C                            'XSIGN(,)'.
C                             DIMENSION: 1 => LOWER INDEX BOUND
C                                        2 => UPPER INDEX BOUND
C  INPUT : (I*4)  NDATA()  = INDEX RANGE FOR VALID INPUT ENERGIES/VELS.
C                            IN 'XTOT()', 'XSIGN(,)','ALPHAA()'
C                             DIMENSION: 1 => LOWER INDEX BOUND
C                                        2 => UPPER INDEX BOUND
C
C  INPUT : (R*8)  XTOT()   = TOTAL CROSS SECTIONS (UNITS: cm**2)
C                            1st DIMENSION: ENERGY INDEX
C  INPUT : (R*8)  XSIGN(,) = CROSS SECTIONS FOR EACH N-SHELL
C                                   (UNITS: cm**2)
C                            1st DIMENSION: ENERGY INDEX
C                            2nd DIMENSION: QUANTUM N-SHELL INDEX
C  INPUT : (R*8)  XSIGL(,,)=INPUT DATA FILE: L-RESOLVED CROSS-SECTIONS.
C                           1st DIMENSION: ENERGY INDEX
C                           2ND DIMENSION: INDEXED BY FUNCTION I4IDFL.
C                           3rd DIMENSION: DATA SUB-BLOCK INDEX
C  INPUT : (R*8)  XSIGM(,,)=INPUT DATA FILE: M-RESOLVED CROSS-SECTIONS.
C                           1st DIMENSION: ENERGY INDEX
C                           2ND DIMENSION: INDEXED BY FUNCTION I4IDFM.
C                           3rd DIMENSION: DATA SUB-BLOCK INDEX
C  INPUT : (R*8)  ALPHAA() =
C                            1st DIMENSION: ENERGY INDEX
C
C  OUTPUT: (R*8)  SIGA()   = VALID CROSS SECTIONS READ FROM INPUT FILE
C                            FOR PRINCIPAL QUATUM NUMBER 'INSEL' AND
C                            GIVEN SUB-BLOCK. (UNITS: cm**2)
C                            1st DIMENSION: ENERGY INDEX
C
C          (I*4)  IE       = ARRAY INDEX: ENERGY INDEX
C          (I*4)  I1ST     = ARRAY INDEX: FIRST VALID ENERGY INDEX - 1
C          (I*4)  IN       = 'NDATA(2)' - IF 'INSEL > NDATA(2)'
C
C          (R*8)  AVAL     = 'NDATA(2)/INSEL' -  IF 'INSEL > NDATA(2)'
C          (R*8)  ZERO     = PARAMETER = EFFECTIVE ZERO (1.0D-72)
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          I4IDFL     ADAS      PROVIDES UNIQUE INDEX GIVEN N AND L
C          I4IDFM     ADAS      PROVIDES UNIQUE INDEX GIVEN N, L AND M
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    07/02/91
C
C UPDATE:  19/04/95  H P SUMMERS - EXTENSION TO INCLUDE L AND M
C                                  SUB-SHELL CROSS-SECTIONS
C
C VERSION  : 1.2
C DATE     : 22-05-2007
C MODIFIED : Martin O'Mullane
C              - Remove unused m-subshell data possibility.
C
C-----------------------------------------------------------------------
      REAL*8    ZERO
C-----------------------------------------------------------------------
      PARAMETER ( ZERO = 1.0D-72 )
C-----------------------------------------------------------------------
      INTEGER   NENRGY               , NSHELL               ,
     &          NENER                , INSEL                ,
     &          ILSEL
      INTEGER   IE                   , I1ST                 , IN
      INTEGER   I4IDFL
C-----------------------------------------------------------------------
      REAL*8    AVAL
C-----------------------------------------------------------------------
      INTEGER   IEDATA(2)            , NDATA(2)
C-----------------------------------------------------------------------
      REAL*8    XTOT(NENRGY)         , ALPHAA(NENRGY)         ,
     &          SIGA(NENRGY)         , XSIGN(NENRGY,NSHELL)   ,
     &          XSIGL(NENRGY,(NSHELL*(NSHELL+1))/2)
C-----------------------------------------------------------------------
C
C **********************************************************************
C
      I1ST  = IEDATA(1) - 1
C-----------------------------------------------------------------------
         IF (INSEL.EQ.0) THEN
C
               DO 1 IE=1,NENER
                  SIGA(IE) = XTOT(I1ST+IE)
    1          CONTINUE
C-----------------------------------------------------------------------
         ELSEIF (INSEL.LT.NDATA(1)) THEN
C
               DO 2 IE=1,NENER
                  SIGA(IE) = ZERO
    2          CONTINUE
C-----------------------------------------------------------------------
         ELSEIF (INSEL.GT.NDATA(2)) THEN
C
            IN   = NDATA(2)
            AVAL = DBLE( IN ) / DBLE( INSEL )
C
               DO 3 IE=1,NENER
                  IF(ILSEL.LT.0)THEN
                      SIGA(IE) = XSIGN(I1ST+IE,IN) *
     &                           (AVAL**ALPHAA(I1ST+IE))
                  ELSEIF(ILSEL.GE.IN)THEN
                      SIGA(IE) = ZERO
                  ELSEIF(ILSEL.GE.0)THEN
                      SIGA(IE) = XSIGL(I1ST+IE,I4IDFL(IN,ILSEL)) *
     &                           (AVAL**ALPHAA(I1ST+IE))
                  ENDIF
    3          CONTINUE
C-----------------------------------------------------------------------
         ELSE
C
               DO 4 IE=1,NENER
                  IF(ILSEL.LT.0)THEN
                      SIGA(IE) = XSIGN(I1ST+IE,INSEL)
                  ELSE
                      SIGA(IE) = XSIGL(I1ST+IE,I4IDFL(INSEL,ILSEL))
                  ENDIF
    4          CONTINUE
C
         ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN
      END
