CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas301/c1out0.for,v 1.2 2004/07/06 11:47:38 whitefor Exp $ Data $Date: 2004/07/06 11:47:38 $
CX
       SUBROUTINE C1OUT0( IWRITE   , LSETL  , LSETM ,
     &                    LOSEL    , LFSEL  , TITLF ,
     &                    TITLE    , TITLX  , TITLM , DATE  ,
     &                    INSEL    , ILSEL  , IMSEL ,
     &                    IEVAL    , LERNG  , EOA   , SIGOA ,
     &                    KPLUS1   , COEF
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C1OUT0 *********************
C
C  PURPOSE:  TO  PRINT DATA CONCERNING THE SELECTED CHEXDATA SUB-BLOCK
C            AND SELECTED PRINCIPAL QUANTUM NUMBER.
C
C  CALLING PROGRAM: ADAS301
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IWRITE  = OUTPUT UNIT FOR RESULTS
C  INPUT : (L*4)  LSETL() = .TRUE.  => L-RESOLVED DATA READ
C                           .FALSE. => NO L-RESOLVED DATA READ
C  INPUT : (L*4)  LSETM() = .TRUE.  => M-RESOLVED DATA READ
C                           .FALSE. => NO M-RESOLVED DATA READ
C  INPUT : (L*4)  LOSEL   = .TRUE.  => INTERPOLATED   VALUES  FOR   ISPF
C                                      PANEL INPUT VALUES CALCULATED
C                           .FALSE  => INTERPOLATED   VALUES  FOR   ISPF
C                                      PANEL INPUT VALUES NOT CALCULATED
C  INPUT : (L*4)  LFSEL   = .TRUE.  => MINIMAX POLYNOMIAL FITTED.
C                           .FALSE. => MINIMAX POLYNOMIAL NOT FITTED
C  INPUT : (C*80) TITLF   = INFORMATION STRING READ FROM CHEXDATA FILE
C                           FOR SELECTED SUB-BLOCK.
C
C  INPUT : (C*40) TITLE   = TITLE OF RUN (READ FROM ISPF PANEL)
C  INPUT : (C*80) TITLX   = INFORMATION STRING CONTAINING: INPUT DATA
C                           FILE-NAME, SELECTED SUB-BLOCK AND N-SHELL.
C  INPUT : (C*80) TITLM   = DIAGNOSTIC LINE INFORMATION FROM 'MINIMAX'
C  INPUT : (C*8)  DATE    = CURRENT DATE (AS 'DD/MM/YY')
C
C  INPUT : (I*4)  INSEL   = SELECTED PRINCIPAL QUANTUM NUMBER.
C                           (0 => TOTAL CROSS-SECTION).
C  INPUT : (I*4)  ILSEL   = SELECTED INPUT DATA L QUANTUM SHELL
C                           ( -1    => TOTAL CROSS-SECTION FOR N SHELL)
C  INPUT : (I*4)  IMSEL   = SELECTED INPUT DATA M QUANTUM SHELL
C                           ( -1    => TOTAL CROSS-SECTION FOR NL SHELL)
C
C  INPUT : (I*4)  IEVAL   = NUMBER OF ISPF ENTERED ENERGY VALUES.
C  INPUT : (L*4)  LERNG() = .TRUE.  => ENERGY 'EOA()' IN RANGE
C                           .FALSE. => ENERGY 'EOA()' OUT OF RANGE
C                           (RANGE = INPUT ENERGY RANGE FOR SUB-BLOCK)
C  INPUT : (R*8)  EOA()   = ISPF ENTERED ENERGIES (eV/amu)
C  INPUT : (R*8)  SIGOA() = SPLINE INTEROPLATED X-SEC VALUE AT 'EOA()'
C
C  INPUT : (I*4)  KPLUS1  = NUMBER OF MINIMAX COEFFICIENTS
C  INPUT : (R*8)  COEF()  = COEFFICIENTS OF FITTED MINIMAX POLYNOMIAL
C
C          (I*4)  L1      = PARAMETER = 1
C          (I*4)  L2      = PARAMETER = 2
C          (I*4)  L3      = PARAMETER = 3
C
C          (I*4)  I       = GENERAL ARRAY ELEMENT INDEX
C
C          (R*8)  R8ECON  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (R*8)  R8SCON  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (R*8)  VAUNIT  = VELOCITY (UNITS: at.units)
C          (R*8)  VCMS    = VELOCITY (UNITS: cm/sec  )
C          (R*8)  SIGAO2  = CROSS-SECTION (UNITS: pi*(a0**2) )
C          (R*8)  RCOEF   = RATE-COEFFICIENT (UNITS: cm**3/sec )
C
C          (C*80) CADAS   = ADAS HEADER: INCLUDES RELEASE, PROGRAM, TIME
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXADAS     ADAS      GATHERS ADAS HEADER INFORMATION
C          R8ECON     ADAS      REAL*8 FUNCTION: CONVERT ENGY/VEL. FORM
C          R8SCON     ADAS      REAL*8 FUNCTION: CONVERT X-SECTION FORM
C
C AUTHOR  : PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C           K1/0/81
C           JET EXT. 4569
C
C DATE:    07/02/91
C
C UPDATE:  22/09/93 - J NASH    - ADAS91:
C          REMOVED PRINTING OF DATA SUB-BLOCK TITLE (COMMENTED OUT).
C
C UPDATE:  18/04/95   H P SUMMERS - ADDED L AND M RESOLUTION
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 24-04-95
C MODIFIED: TIM HAMMOND(TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C VERSION: 1.2                          DATE: 13-05-96
C MODIFIED: TIM HAMMOND
C               - REMOVED HOLLERITH CONSTANTS FROM OUTPUT AND TIDIED UP
C                 HEADER
C----------------------------------------------------------------------
      INTEGER    L1           , L2           , L3
C----------------------------------------------------------------------
      PARAMETER( L1 = 1       , L2 = 2       , L3 = 3     )
C----------------------------------------------------------------------
      INTEGER    IWRITE       , INSEL        , ILSEL     , IMSEL     ,
     &           IEVAL        , KPLUS1
      INTEGER    I
C----------------------------------------------------------------------
      REAL*8     R8ECON       , R8SCON
      REAL*8     VAUNIT       , VCMS         , SIGAO2     , RCOEF
C----------------------------------------------------------------------
      LOGICAL    LSETL        , LSETM        , LOSEL      , LFSEL
C----------------------------------------------------------------------
      CHARACTER  TITLF*(*)    , TITLE*(*)    , TITLX*(*)  , TITLM*(*) ,
     &           DATE*8
      CHARACTER  CADAS*80
C----------------------------------------------------------------------
      REAL*8     EOA(IEVAL)   , SIGOA(IEVAL) ,
     &           COEF(KPLUS1)
C----------------------------------------------------------------------
      LOGICAL    LERNG(IEVAL)
C----------------------------------------------------------------------
      SAVE       CADAS
C----------------------------------------------------------------------
      DATA       CADAS/' '/
C----------------------------------------------------------------------
C
C**********************************************************************
C
C---------------------------------------------------------------------
C GATHER ADAS HEADER
C---------------------------------------------------------------------
C
      CALL XXADAS( CADAS )
C
C---------------------------------------------------------------------
C OUTPUT HEADINGS
C---------------------------------------------------------------------
C
      WRITE(IWRITE,1000) CADAS(2:80)
      WRITE(IWRITE,1001) 'CHARGE EXCHANGE CROSS-SECTION INTERROGATION',
     &                   'ADAS301' , DATE
      WRITE(IWRITE,1002) TITLE
      WRITE(IWRITE,1003) TITLX
C     WRITE(IWRITE,1004) TITLF
C
         IF(LSETL.AND.(.NOT.LSETM)) THEN
            WRITE(IWRITE,1016)
         ELSEIF(LSETL.AND.LSETM) THEN
            WRITE(IWRITE,1017)
         ENDIF
C
         IF (INSEL.EQ.0) THEN
            WRITE(IWRITE,1005)
         ELSEIF(INSEL.GT.0.AND.ILSEL.LT.0) THEN
            WRITE(IWRITE,1006) INSEL
         ELSEIF(INSEL.GT.0.AND.ILSEL.GE.0.AND.IMSEL.LT.0)THEN
            WRITE(IWRITE,1018) INSEL , ILSEL
         ELSEIF(INSEL.GT.0.AND.ILSEL.GE.0.AND.IMSEL.GE.0) THEN
            WRITE(IWRITE,1019) INSEL , ILSEL , IMSEL
         ENDIF
C
C---------------------------------------------------------------------
C OUTPUT ENERGIES, CROSS-SECTIONS AND RATE COEFFICIENTS - IF REQD.
C---------------------------------------------------------------------
C
         IF (LOSEL) THEN
            WRITE (IWRITE,1007)
               DO 1 I=1,IEVAL
                  VAUNIT = R8ECON( L3 , L1 , EOA(I)   )
                  VCMS   = R8ECON( L3 , L2 , EOA(I)   )
                     IF (LERNG(I)) THEN
                        SIGAO2 = R8SCON( L1 , L2 , SIGOA(I) )
                        RCOEF  = VCMS * SIGOA(I)
                        WRITE(IWRITE,1008) EOA(I)   , VAUNIT , VCMS ,
     &                                     SIGOA(I) , SIGAO2 ,
     &                                     RCOEF
                     ELSE
                        WRITE(IWRITE,1009) EOA(I)   , VAUNIT , VCMS
                     ENDIF
    1          CONTINUE
            WRITE (IWRITE,1010)
            WRITE (IWRITE,1011)
            WRITE (IWRITE,1010)
         ENDIF
C
C---------------------------------------------------------------------
C OUTPUT THE MINIMAX COEFFICIENTS (IF REQUESTED)
C---------------------------------------------------------------------
C
         IF (LFSEL) THEN
            WRITE (IWRITE , 1012 )
              DO 2 I=1,KPLUS1,2
                 IF (I.EQ.KPLUS1) THEN
                    WRITE (IWRITE , 1013 ) I , COEF(I)
                 ELSE
                    WRITE (IWRITE , 1014 ) I , COEF(I) , I+1 , COEF(I+1)
                 ENDIF
    2         CONTINUE
            WRITE (IWRITE , 1015 ) TITLM
         ENDIF
C
C---------------------------------------------------------------------
C
 1000 FORMAT(1H ,A79)
 1001 FORMAT(1H ,15('*'),' TABULAR OUTPUT FROM ',A43,' PROGRAM: ',
     & A7,1X,'- DATE: ',A8,1X,16('*')/)
 1002 FORMAT(1H ,19('-'),1X,A40,1X,19('-')//12X,
     & 'CHARGE EXCHANGE CROSS-SECTIONS  AS A FUNCTION OF ENERGY'/
     & 12X,55('-')/21X,
     & 'DATA GENERATED USING PROGRAM: ADAS301'/21X,37('-'))
 1003 FORMAT(1H /1H ,A80)
 1004 FORMAT(1H ,'SUB-BLOCK TITLE: ',A80)
 1005 FORMAT(1H /1H ,'TOTAL CHARGE-EXCHANGE CROSS-SECTION')
 1006 FORMAT(1H /1H ,'PRINCIPAL QUANTUM N-SHELL = ',I2)
 1007 FORMAT(1H ,2('-'), ' ENERGY ' ,2('-'),1X,
     &           8('-'),' VELOCITY ',8('-'),2X,
     &           5('-'),' CROSS-SECTION ',5('-'),2X,'RATE COEFFT'/
     &       1H ,4X,'eV/amu',6X,'at.units',6X,'cm/sec',9X,'cm**2',5X,
     &           'pi*(a0**2)',4X,'cm**3/sec'/
     &       1H ,79('-'))
 1008 FORMAT(1P,2X,D10.3,2(3X,D10.3),2X,3(3X,D10.3))
 1009 FORMAT(1P,2X,D10.3,2(3X,D10.3),
     &          5X,'----- ENERGY VALUE OUT OF RANGE -----')
 1010 FORMAT(1H ,79('-'))
 1011 FORMAT(1H ,'KEY: a0 => Bohr radius = 5.29177D-11 m')
C
 1012 FORMAT (1H /1H ,'MINIMAX POLYNOMIAL - TAYLOR COEFTS:',
     &                ' LOG10(X-SEC<cm**2>) vs. LOG10(ENGY<eV/amu>)'/
     &            1H ,79('-'))
 1013 FORMAT ( 12X,'A(',I2,') = ',1P,D17.9)
 1014 FORMAT ( 2(12X,'A(',I2,') = ',1P,D17.9))
 1015 FORMAT (/1X,A79/1X,79('-'))
 1016 FORMAT (1H ,'FILE IS L RESOLVED')
 1017 FORMAT (1H ,'FILE IS L AND M RESOLVED')
 1018 FORMAT(1H /1H ,'PRINCIPAL QUANTUM N-SHELL = ',I2/
     &           1H ,'ORBITAL   QUANTUM L-SHELL = ',I2)
 1019 FORMAT(1H /1H ,'PRINCIPAL QUANTUM N-SHELL = ',I2/
     &           1H ,'ORBITAL   QUANTUM L-SHELL = ',I2/
     &           1H ,'AZIMUTHAL QUANTUM L-SHELL = ',I2)
C
C---------------------------------------------------------------------
C
      RETURN
      END
