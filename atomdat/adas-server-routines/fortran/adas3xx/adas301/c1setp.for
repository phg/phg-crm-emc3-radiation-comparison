CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas301/c1setp.for,v 1.2 2004/07/06 11:47:52 whitefor Exp $ Data $Date: 2004/07/06 11:47:52 $
CX
       SUBROUTINE C1SETP( NBLOCK , NBLKS , TITLF ,
     &                    LSETL  , LSETM
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C1SETP *********************
C
C  PURPOSE:  TO SET UP PARAMETERS IN THE SHARED POOLED FOR PANEL DISPLAY
C
C  CALLING PROGRAM: ADAS301
C
C  DATA:
C           DATA IS OBTAINED VIA SUBROUTINE 'C1DATA'
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  NBLOCK  = (MAXIMUM) NUMBER OF INPUT DATA SUB-BLOCKS.
C  INPUT : (I*4)  NBLKS   = NUMBER OF DATA SUB-BLOCKS READ IN.
C  INPUT : (C*80) TITLF() = TITLES FOR EACH DATA SUB-BLOCK.
C                           DIMENSION: DATA SUB-BLOCK INDEX
C  INPUT : (L*4)  LSETL() = .TRUE.  => L-RESOLVED DATA READ
C                           .FALSE. => NO L-RESOLVED DATA READ
C  INPUT : (L*4)  LSETM() = .TRUE.  => M-RESOLVED DATA READ
C                           .FALSE. => NO M-RESOLVED DATA READ
C
C          (I*4)  ILEN    = LENGTH, IN BYTES, OF ISPF DIALOG VARIABLES
C          (I*4)  IBLK    = ARRAY INDEX: SUB-BLOCK INDEX
C
C          (C*1)  CBLNK   = ' '
C          (C*8)  CHA     = FUNCTION POOL NAME SKELETON: TITLE
C
C          (I*4)  PIPEIN  = PARAMETER = UNIT NUMBER FOR INPUT FROM PIPE
C          (I*4)  PIPEOU  = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C
C ROUTINES:
C           ROUTINE     SOURCE      BRIEF DESCRIPTION
C           ------------------------------------------------------------
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    07/02/91
C
C UPDATE:  10/04/95 TIM HAMMOND - UNIX PORT, PIPE COMMUNICATION WITH IDL
C
C UPDATE:  18/04/95  H P SUMMERS - INCLUDE LSETL AND LSETM AND SET POOL
C                                  VARIABLES SETL AND SETM TO YES OR NO
C                                  ACCORDINGLY
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER    NBLOCK             , NBLKS
      INTEGER    ILEN               , IBLK
      INTEGER    PIPEIN             , PIPEOU
      PARAMETER( PIPEIN=5           , PIPEOU=6)
C-----------------------------------------------------------------------
      LOGICAL    LSETL              , LSETM
C-----------------------------------------------------------------------
      CHARACTER  CBLNK*1            , CYES*3          , CNO*3         ,
     &           CHA*8              , CHB(2)*8
      CHARACTER  TITLF(NBLOCK)*80
C-----------------------------------------------------------------------
      SAVE       CBLNK              , CHA
C-----------------------------------------------------------------------
      DATA CBLNK , CYES , CNO / ' ' , 'YES' , 'NO ' /
      DATA CHA   / '(TITLF*)' /
C-----------------------------------------------------------------------
C
C **********************************************************************
C
C-----------------------------------------------------------------------
C SEND STRINGS TO ISPF FUNCTION POOL
C-----------------------------------------------------------------------
C
          WRITE( PIPEOU, * ) NBLKS
          CALL XXFLSH( PIPEOU )
          WRITE( PIPEOU, * ) NBLOCK
          CALL XXFLSH( PIPEOU )

CX        DO 1 IBLK=1,NBLKS
CX           WRITE(CHA(7:7),1000) IBLK
CX           ILEN=69
CX           CALL ISPLNK( F6 , CHA , ILEN , TITLF(IBLK)(1:69) )
CX           WRITE( PIPEOU, '(A69)' ) TITLF(IBLK)(1:69) 
CX           CALL XXFLSH( PIPEOU )
CX  1     CONTINUE
C
CX        DO 2 IBLK=NBLKS+1,NBLOCK
CX           WRITE(CHA(7:7),1000) IBLK
CX           ILEN=1
CX           CALL ISPLNK( F6 , CHA , ILEN , CBLNK             )
CX  2     CONTINUE
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(I1)
C
C-----------------------------------------------------------------------
C
      RETURN
      END
