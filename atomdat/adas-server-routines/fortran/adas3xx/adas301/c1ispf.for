CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas301/c1ispf.for,v 1.4 2004/07/06 11:47:22 whitefor Exp $ Data $Date: 2004/07/06 11:47:22 $
CX
      SUBROUTINE C1ISPF( IPAN   , LPEND  ,
     &                   NDEIN  , NBLOCK ,
     &                   NENRGY , NENER  , EVALS ,
     &                   LSETL  , LSETM  ,
     &                   TITLE  ,
     &                   IBSEL  , INSEL  , ILSEL , IMSEL ,
     &                   IETYP  , IEVAL  , EIN   ,
     &                   LOSEL  , LFSEL  , LGRD1 , LDEF1 ,
     &                   TOLVAL ,
     &                   XL1    , XU1    , YL1   , YU1   ,
     &                   NMIN   , NMAX
     &                 )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C1ISPF *********************
C
C  PURPOSE: PIPE COMMUNICATIONS WITH IDL AND TO RETURN USER SELECTED
C           OPTIONS AND VALUES
C
C  CALLING PROGRAM: ADAS301
C
C  SUBROUTINE:
C
C  I/O   : (L*4)   LPEND    = .TRUE.  => END ANALYSIS OF CURRENT DATA
C                                        SETS
C                           = .FALSE. => CONTINUE PANALYSIS WITH CURRENT
C                                        DATA SETS
C
C  INPUT : (I*4)   NDEIN    = MAX. NUMBER OF VELOCITIES/ENERGIES ALLOWED
C  INPUT : (I*4)   NBLOCK   = MAX. NUMBER OF INPUT DATA FILE SUB-BLOCKS.
C
C  INPUT : (I*4)   NENRGY   = INPUT DATA FILE: MAX. NO. OF VEL/ENERGIES
C  INPUT : (I*4)   NENER()  = INPUT DATA FILE: NUMBER   OF VEL/ENERGIES
C                             DIMENSION: DATA SUB-BLOCK INDEX
C  INPUT : (R*8)   EVALS(,,)= INPUT DATA FILE: ELECTRON VEL./ENERGIES
C                             2ND DIMENSION: 1 => AT.UNITS (IETYP=1)
C                                            2 => CM/SEC   (IETYP=2)
C                                            3 => EV/AMU   (IETYP=3)
C                             3RD DIMENSION: DATA SUB-BLOCK INDEX
C  INPUT : (L*4)   LSETL    = .TRUE.  => L-RESOLVED DATA READ
C                             .FALSE. => NO L-RESOLVED DATA READ
C  INPUT : (L*4)   LSETM    = .TRUE.  => M-RESOLVED DATA READ
C                             .FALSE. => NO M-RESOLVED DATA READ
C  INPUT : (I*4)   NMIN      LOWEST N-SHELL FOR WHICH DATA READ
C  INPUT : (I*4)   NMAX      HIGHEST N-SHELL FOR WHICH DATA READ
C
C  OUTPUT: (C*40)  TITLE    = USER ENTERED GENERAL TITLE FOR PROGRAM RUN
C
C  OUTPUT: (I*4)   IBSEL    = SELECTED INPUT DATA SUB-BLOCK FOR ANALYSIS
C  OUTPUT: (I*4)   INSEL    = SELECTED INPUT DATA PRINC. QUANTUM SHELL
C                             ( 0 => TOTAL CROSS-SECTION)
C  OUTPUT: (I*4)   ILSEL    = SELECTED INPUT DATA L QUANTUM SHELL
C                             ( -1    => TOTAL CROSS-SECT. FOR N SHELL)
C  OUTPUT: (I*4)   IMSEL    = SELECTED INPUT DATA M QUANTUM SHELL
C                             ( -1    => TOTAL CROSS-SECT. FOR NL SHELL)
C
C  OUTPUT: (I*4)   IETYP    = 1 => INPUT VELOCITIES IN AT. UNITS
C                           = 2 => INPUT VELOCITIES IN CM/SEC
C                           = 3 => INPUT ENERGIES   IN EV/AMU
C  OUTPUT: (I*4)   IEVAL    = NUMBER OF INPUT VEL/ENERGIES ( 1 -> 20)
C  OUTPUT: (R*8)   EIN()    = ELECTRON VEL/ENERGIES (UNITS: SEE 'IETYP')
C
C  OUTPUT: (L*4)   LOSEL    = .TRUE.  => CALCULATE INTERPOLATED VALUES
C                                        FOR OUTPUT.
C                           = .FALSE. => - DO NOT DO THE ABOVE -
C  OUTPUT: (L*4)   LFSEL    = .TRUE.  => CARRY OUT MINIMAX POLYNOMIAL
C                                        FITTING
C                           = .FALSE. => - DO NOT DO THE ABOVE -
C  OUTPUT: (L*4)   LGRD1    = .TRUE.  => PUT GRAPH IN GRID FILE
C                           = .FALSE. => DO NOT PUT GRAPH IN GRID FILE
CX UNIX PORT - LGRD1 ONLY USED TO KEEP ARGUMENT LIST THE SAME
C  OUTPUT: (L*4)   LDEF1    = .TRUE.  => USE GRAPH DEFAULT SCALING
C                           = .FALSE. => DO NOT USE DEFAULT SCALING
CX UNIX PORT - AXES SCALING CHOSEN LATER UNDER IDL-ADAS
C
C  OUTPUT: (R*8)   TOLVAL   = FRACTIONAL TOLERANCE FOR MINIMAX FIT
C                             (=0 IF MINIMAX FIT NOT SELECTED)
C
C  OUTPUT: (R*8)   XL1      = LOWER LIMIT FOR X-AXIS OF GRAPH
C  OUTPUT: (R*8)   XU1      = UPPER LIMIT FOR X-AXIS OF GRAPH
C  OUTPUT: (R*8)   YL1      = LOWER LIMIT FOR Y-AXIS OF GRAPH
C  OUTPUT: (R*8)   YU1      = UPPER LIMIT FOR Y-AXIS OF GRAPH
C
C          (I*4)   PIPEIN - PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)   PIPEOU - PARAMETER = UNIT NUMBER FOR INPUT FROM PIPE
C          (I*4)   LOGIC  - USED TO PIPE LOGICAL VALUES
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXFLSH     IDL_ADAS  calls 'flush' to clear the pipe.
C
C
C AUTHOR:  Tim Hammond (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    11/04/95
C
C Updates: 11/04/95 - Created UNIX conversion version.
C
C          24/04/95 - Added l and m shell capability. In line with
C                     changes made to JET code by H P Summers.
C          01/05/95 - Added extra writes to the IDL code so that
C                     the l and m resolution can be determined.
C	   30/05/95 - Added pipeou parameter which was missing from
C                     some calls to xxflsh, causes problems on Suns.
C
C-----------------------------------------------------------------------
      INTEGER    ILEN
      INTEGER    IPAN           ,
     &           NDEIN          , NBLOCK        , NENRGY      ,
     &           IBSEL          , INSEL         , ILSEL       , IMSEL  ,
     &           IETYP          , IEVAL
      INTEGER    IABT           , IPANRC        , IELAST
C-----------------------------------------------------------------------
      REAL*8     TOLVAL         ,
     &           XL1            , XU1           , YL1         , YU1
C-----------------------------------------------------------------------
      CHARACTER  TITLE*40
      CHARACTER  P*2            , PLAST*2       , KEY*4
      CHARACTER  CURPOS*8       , MSGTXT*8      , PNAME*8
C-----------------------------------------------------------------------
      LOGICAL    LPEND
      LOGICAL    LFSEL          , LOSEL         , LGRD1       , LDEF1
      LOGICAL    LSETL          , LSETM
C-----------------------------------------------------------------------
      INTEGER    NENER(NBLOCK)
      INTEGER    I              , J             , ILOGIC
      INTEGER    LOGIC          
      INTEGER    NMIN           , NMAX
C-----------------------------------------------------------------------
      REAL*8     EVALS(NENRGY,3,NBLOCK)         , EIN(NDEIN)
C-----------------------------------------------------------------------
      INTEGER    PIPEOU         , PIPEIN
      PARAMETER (PIPEOU=6       , PIPEIN=5)
C-----------------------------------------------------------------------
C WRITE VARIABLES OUT TO IDL VIA PIPE
C-----------------------------------------------------------------------
      IBSEL=1
      WRITE(PIPEOU, *) NDEIN
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU, *) NBLOCK
      WRITE(PIPEOU, *) NENRGY
      WRITE(PIPEOU, *) NENER(IBSEL)
      CALL XXFLSH(PIPEOU)
      DO J = 1, 3
          DO I = 1, NENER(IBSEL)
              WRITE(PIPEOU, *)EVALS(I,J,IBSEL)
          ENDDO
      ENDDO 
      CALL XXFLSH(PIPEOU)
      IF (LSETL) THEN
          WRITE(PIPEOU,*) 1
      ELSE
          WRITE(PIPEOU,*) 0
      ENDIF
      IF (LSETM) THEN
          WRITE(PIPEOU,*) 1
      ELSE
          WRITE(PIPEOU,*) 0
      ENDIF
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) NMIN
      WRITE(PIPEOU,*) NMAX
      CALL XXFLSH(PIPEOU)
      
C-----------------------------------------------------------------------
C     READ OUPUTS FROM PIPE
C-----------------------------------------------------------------------

      READ(PIPEIN, *) LOGIC
      IF (LOGIC.EQ.1) THEN
          LPEND = .TRUE.
      ELSE
          LPEND = .FALSE.
      ENDIF
      READ(PIPEIN,'(A)') TITLE
      READ(PIPEIN,*) IETYP
      READ(PIPEIN,*) IEVAL
      READ(PIPEIN,*) (EIN(I), I=1, IEVAL)
      READ(PIPEIN, *) LOGIC
      IF (LOGIC.EQ.1) THEN
          LFSEL = .TRUE.
         READ(PIPEIN,*) TOLVAL
         TOLVAL = TOLVAL /100.0
      ELSE
         LFSEL = .FALSE.
      ENDIF

      READ(PIPEIN, *) INSEL
      READ(PIPEIN, *) ILSEL
      READ(PIPEIN, *) IMSEL

      READ(PIPEIN,*) ILOGIC
      IF (ILOGIC .EQ. 1) THEN
         LOSEL = .TRUE.
      ELSE
         LOSEL = .FALSE.
      ENDIF


C-----------------------------------------------------------------------
      RETURN
      END
