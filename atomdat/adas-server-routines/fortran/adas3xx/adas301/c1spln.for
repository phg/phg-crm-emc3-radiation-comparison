CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas301/c1spln.for,v 1.2 2004/07/06 11:48:16 whitefor Exp $ Data $Date: 2004/07/06 11:48:16 $
CX
      SUBROUTINE C1SPLN(         LOSEL ,
     &                   NENER , IEVAL , NPSPL  ,
     &                   ENERA , EOA   , EOSA   ,
     &                   SIGA  , SIGOA , SIGOSA ,
     &                   LERNG
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C1SPLN *********************
C
C  (IDENTICAL TO: B1SPLN (EXCEPT SOME VARIABLE NAMES ARE CHANGED))
C
C  PURPOSE:
C         1) PERFORMS CUBIC SPLINE ON LOG(ENERGY) VERSUS LOG(X-SECTION)
C            INPUT DATA, ('ENERA' VERSUS 'SIGA' , NENER DATA PAIRS),
C            FOR A GIVEN SUB-BLOCK.
C
C         2) INTERPOLATES 'IEVAL' X-SECT. VALUES USING  ABOVE  SPLINES
C            AT ENERGIES READ IN FROM ISPF PANELS FOR TABULAR  OUTPUT.
C            (ANY ENERGIES VALUES WHICH REQUIRED EXTRAPOLATION ARE SET
C             TO ZERO).
C                 - THIS STEP ONLY TAKES PLACE IF 'LOSEL=.TRUE.' -
C
C         3) INTERPOLATES 'NPSPL' X-SECT VALUES USING ABOVE SPLINES AT
C            ENERGIES EQUI-DISTANCE  ON RANGE OF LOG(ENERGIES)  STORED
C            IN INPUT 'ENERA' ARRAY.
C
C  CALLING PROGRAM: ADAS301
C
C
C  SUBROUTINE:
C
C  INPUT : (L*4)  LOSEL   = .TRUE.  => CALCULATE X-SECS FOR INPUT ENGYS.
C                                      READ FROM ISPF PANEL.
C                           .FALSE. => - DO NOT DO THE ABOVE -
C
C  INPUT : (I*4)  NENER   = INPUT DATA FILE: NO. OF VALID ENERGY/X-SECT.
C                           PAIRS READ FOR THE SUB-BLOCK BEING ASSESSED
C  INPUT : (I*4)  IEVAL   = NUMBER OF  ISPF  ENTERED  ENERGY  VALUES  AT
C                           WHICH INTERPOLATED X-SEC VALUES ARE REQUIRED
C                           FOR TABULAR OUTPUT.
C  INPUT : (I*4)  NPSPL   = NUMBER OF  SPLINE  INTERPOLATED  ENGY/X-SECT
C                           REQUIRED FOR GRAPHICAL DISPLAY.
C
C  INPUT : (I*4)  ENERA() = INPUT DATA FILE: ENERGIES (EV/AMU)
C  INPUT : (I*4)  EOA()   = ISPF PANEL ENTERED ENERGIES (EV/AMU)
C  OUTPUT: (I*4)  EOSA()  = 'NPSPL' ENERGIES FOR GRAPHICAL OUTPUT
C                           (EV/AMU).
C
C  INPUT : (R*8)  SIGA()  = INPUT DATA FILE: SELECTED SUB-BLOCK -
C                           X-SECTION VALUES AT 'ENERA()'. (CM**2)
C  OUTPUT: (I*4)  SIGOA() = SPLINE INTERPOLATED X-SEC VALUES AT 'EOA()'
C                           (EXTRAPOLATED VALUES = 0.0).
C  OUTPUT: (R*8)  SIGOSA()= SPLINE INTERPOLATED X-SEC VALUES AT 'EOSA()'
C
C  OUTPUT: (L*4)  LERNG() = .TRUE.  => OUTPUT   SPLINE    VALUE    WAS
C                                      INTERPOLATED FOR 'DLOG(EOA()'.
C                           .FALSE. => OUTPUT   SPLINE    VALUE    WAS
C                                      EXTRAPOLATED FOR 'DLOG(EOA()'.
C                                      (NOTE: 'YOUT()=0' AS 'IOPT < 0').
C
C          (I*4)  NIN     = PARAMETER = MAX. NO. OF  INPUT  ENGY/X-SEC
C                                      PAIRS MUST BE >= 'NENER'
C          (I*4)  NOUT    = PARAMETER = MAX. NO. OF 'OUTPUT ENGY/X-SEC
C                                      PAIRS MUST BE >= 'IEVAL' &'NPSPL'
C          (R*8)  ZERO    = PARAMETER = EFFECTIVE ZERO (1.0D-72)
C          (R*8)  ZEROL   = PARAMETER = LN(ZERO) APPROX.  = -165.7
C
C          (I*4)  IARR    = ARRAY SUBSCRIPT USED FOR ENGY/X-SEC PAIRS
C          (I*4)  IOPT    = DEFINES THE BOUNDARY DERIVATIVES FOR THE
C                           SPLINE ROUTINE 'XXSPLE', SEE 'XXSPLE'.
C                           (VALID VALUES = <0, 0, 1, 2, 3, 4)
C
C          (R*8)  ESTEP   = THE SIZE OF STEP BETWEEN 'XOUT()' VALUES FOR
C                           GRAPHICAL  OUTPUT  ENGY/X-SEC  PAIRS  TO  BE
C                           CALCULATED USING SPLINES.
C
C          (L*4)  LSETX   = .TRUE.  => SET UP SPLINE PARAMETERS RELATING
C                                      TO 'XIN' AXIS.
C                           .FALSE. => DO NOT SET UP SPLINE PARAMETERS
C                                      RELATING TO 'XIN' AXIS.
C                                      (I.E. THEY WERE SET IN A PREVIOUS
C                                            CALL )
C                           (VALUE SET TO .FALSE. BY 'XXSPLE')
C
C          (R*8)  XIN()   = LOG( 'ENERA()' )
C          (R*8)  YIN()   = LOG( 'SIGA()' )
C          (R*8)  XOUT()  = LOG(ENERGIES AT WHICH SPLINES REQUIRED)
C          (R*8)  YOUT()  = LOG(OUTPUT SPLINE INTERPOLATED X-SEC VALUES)
C          (R*8)  DF()    = SPLINE INTERPOLATED DERIVATIVES
C
C          (L*4)  LDUMP() = .TRUE.  => OUTPUT SPLINE VALUE INTRPOLATED
C                                      FOR 'YOUT()'.
C                           .FALSE. => OUTPUT SPLINE VALUE EXTRAPOLATED
C                                      FOR 'YOUT()'.
C                                      (NOTE: USED AS A DUMMY ARGUMENT.
C                                             ALL VALUES WILL BE TRUE.)
C
C NOTE:
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXSPLE     ADAS      SPLINE SUBROUTINE (EXTENDED DIAGNOSTICS)
C          R8FUN1     ADAS      REAL*8 FUNCTION: ( X -> X )
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    07/02/91
C
C UPDATE:  30/11/93 - J NASH    - ADAS91:
C          INCREASED MAX NUMBER OF INPUT ENERGIES ('NIN') FROM 24 TO 30.
C
C UPDATE:  19/04/95   H P SUMMERS - ADDED TRAP FOR ZERO INPUTS
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    NIN          , NOUT
C-----------------------------------------------------------------------
      REAL*8     ZERO         , ZEROL
C-----------------------------------------------------------------------
      PARAMETER( NIN = 30     , NOUT = 100    )
      PARAMETER( ZERO = 1.0D-72  , ZEROL = -165.7D0 )
C-----------------------------------------------------------------------
      INTEGER    NENER        , IEVAL         , NPSPL
      INTEGER    IOPT         , IARR
C-----------------------------------------------------------------------
      REAL*8     R8FUN1       , ESTEP
C-----------------------------------------------------------------------
      LOGICAL    LOSEL        , LSETX
C-----------------------------------------------------------------------
      REAL*8     ENERA(NENER) , SIGA(NENER)   ,
     &           EOA(IEVAL)   , SIGOA(IEVAL)  ,
     &           EOSA(NPSPL)  , SIGOSA(NPSPL)
      REAL*8     XIN(NIN)     , YIN(NIN)      ,
     &           XOUT(NOUT)   , YOUT(NOUT)
      REAL*8     DF(NIN)
C-----------------------------------------------------------------------
      LOGICAL    LERNG(IEVAL) , LDUMP(NOUT)
C-----------------------------------------------------------------------
      EXTERNAL   R8FUN1
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      IF (NIN.LT.NENER)
     &             STOP ' C1SPLN ERROR: NIN < NENER - INCREASE NIN'
      IF (NOUT.LT.IEVAL)
     &             STOP ' C1SPLN ERROR: NOUT < IEVAL - INCREASE NTOUT'
      IF (NOUT.LT.NPSPL)
     &             STOP ' C1SPLN ERROR: NOUT < NPSPL - INCREASE NGOUT'
C
C-----------------------------------------------------------------------
C SET UP SPLINE BOUNDARY CONDITIONS - SWITCH OFF EXTRAPOLATION.
C-----------------------------------------------------------------------
C
      LSETX = .TRUE.
      IOPT  = -1
C
C-----------------------------------------------------------------------
C SET UP ARRAYS CONTAINING LOG VALUES OF INPUT ENERGY/X-SECTION PAIRS
C-----------------------------------------------------------------------
C
         DO 1 IARR=1,NENER
            XIN(IARR) = DLOG( ENERA(IARR)  )
            IF(SIGA(IARR).LE.ZERO) THEN
                YIN(IARR) = DLOG( ZERO )
            ELSE
                YIN(IARR) = DLOG( SIGA(IARR) )
            ENDIF
    1    CONTINUE
C
C-----------------------------------------------------------------------
C INTERPOLATE SPLINED ENGY/X-SEC PAIRS FOR TABULAR OUTPUT - IF REQUESTED
C-----------------------------------------------------------------------
C
         IF (LOSEL) THEN
C
               DO 2 IARR=1,IEVAL
                  XOUT(IARR) = DLOG(EOA(IARR))
    2          CONTINUE
C
            CALL XXSPLE( LSETX , IOPT   , R8FUN1 ,
     &                   NENER , XIN    , YIN    ,
     &                   IEVAL , XOUT   , YOUT   ,
     &                   DF    , LERNG
     &                 )
C
               DO 3 IARR=1,IEVAL
                  IF (LERNG(IARR)) THEN
                     IF(YOUT(IARR).LE.ZEROL) THEN
                         SIGOA(IARR) = 0.0D0
                     ELSE
                         SIGOA(IARR) = DEXP( YOUT(IARR) )
                     ENDIF
                  ELSE
                     SIGOA(IARR) = 0.0
                  ENDIF
    3          CONTINUE
C
         ENDIF
C
C-----------------------------------------------------------------------
C INTERPOLATE SPLINED ENGY/X-SEC PAIRS FOR GRAPH OUTPUT- 'NPSPL' VALUES
C-----------------------------------------------------------------------
C
      ESTEP = ( XIN(NENER)-XIN(1) ) / DBLE( NPSPL-1 )
C
         DO 4 IARR=1,NPSPL
            XOUT(IARR) = ( DBLE(IARR-1)*ESTEP ) + XIN(1)
    4    CONTINUE
C
      CALL XXSPLE( LSETX , IOPT   , R8FUN1 ,
     &             NENER , XIN    , YIN    ,
     &             NPSPL , XOUT   , YOUT   ,
     &             DF    , LDUMP
     &           )
C
         DO 5 IARR=1,NPSPL
            EOSA(IARR)   = DEXP( XOUT(IARR) )
            SIGOSA(IARR) = DEXP( YOUT(IARR) )
    5    CONTINUE
C
C-----------------------------------------------------------------------
C
      RETURN
      END
