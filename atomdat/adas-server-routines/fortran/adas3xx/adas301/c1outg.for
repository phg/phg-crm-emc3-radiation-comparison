CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas301/c1outg.for,v 1.1 2004/07/06 11:47:41 whitefor Exp $ Date $Date: 2004/07/06 11:47:41 $    
CX      
       SUBROUTINE C1OUTG( LGHOST , TITLF  ,
     &                   TITLE  , TITLX  , TITLM , DATE ,
     &                   ENERA  , SIGA   , NENER ,
     &                   EOMA   , SIGOMA , NMX   ,
     &                   EOSA   , SIGOSA , NPSPL ,
     &                   LGRD1  , LDEF1  , LFSEL ,
     &                   XMIN   , XMAX   , YMIN  , YMAX
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C1OUTG *********************
C
C  PURPOSE:  GRAPHIC ROUTINE FOR SELECTED TRANSITION USING GHOST80.
C            (IDENTICAL TO: B1OUTG - EXCEPT SOME VARIABLES HAVE BEEN
C             RENAMED AS HAVE THE MAIN TITLE 'ISPEC(1:48)' AND AXES.
C             - ALSO ARGUMENT 'TITLF(C*80)' INTRODUCED.)
C
C            PROVIDES COMPARATIVE GRAPH OF:
C                                ORIGINAL CHEXDATA.DATA    (CROSSES    )
C                                SPLINE INTERPOLATED DATA  (FULL CURVE)
C                                MINIMAX FIT TO DATA       (DASH CURVE )
C
C            PLOT IS LOG10(X-SECT.(CM**2)) VERSUS LOG10(ENERGY(EV/AMU))
C
C  CALLING PROGRAM: ADAS301
C
C  SUBROUTINE:
C
C  INPUT : (L*4)  LGHOST  = .TRUE.  => GHOST80 INITIALISED
C                           .FALSE. => GHOST80 NOT INITIALISED
C  INPUT : (C*80) TITLF   = INFORMATION STRING READ FROM CHEXDATA FILE
C                           FOR SELECTED SUB-BLOCK.
C
C  INPUT : (C*40) TITLE   = TITLE OF RUN (READ FROM ISPF PANEL)
C  INPUT : (C*120)TITLX   = INFORMATION STRING CONTAINING: INPUT DATA
C                           FILE-NAME, SELECTED BLOCK and QUANTUM SHELL
C  INPUT : (C*80) TITLM   = DIAGNOSTIC LINE INFORMATION FROM 'MINIMAX'
C  INPUT : (C*8)  DATE    = CURRENT DATE (AS 'DD/MM/YY')
C
C  INPUT : (R*8)  ENERA() = INPUT DATA FILE: SELECTED SUB-BLOCK -
C                           VALID ENERGIES (eV/amu)
C  INPUT : (R*8)  SIGA()  = INPUT DATA FILE: SELECTED SUB-BLOCK/N-SHELL
C                           CROSS-SECTIONS (cm**2) AT 'ENERA()'
C  INPUT : (I*4)  NENER   = INPUT DATA FILE: NUMBER OF ENERGY/X-SECTION
C                           PAIRS FOR THE SELECTED SUB-BLOCK.
C
C  INPUT : (R*8)  EOMA()  = MINIMAX: SELECTED ENERGIES (eV/amu)
C  INPUT : (R*8)  SIGOMA()= N-SHELL CROSS-SECTIONS (cm**2) AT 'EOMA()'
C  INPUT : (I*4)  NMX     = NUMBER OF MINIMAX  GENERATED ENERGY/X-SECT.
C                           PAIRS FOR GRAPHICAL DISPLAY.
C
C  INPUT : (R*8)  EOSA()  = SPLINE: SELECTED ENERGIES (eV/amu)
C  INPUT : (R*8)  SIGOSA()= N-SHELL CROSS-SECTIONS (cm**2) AT 'EOSA()'
C  INPUT : (I*4)  NPSPL   = NUMBER  OF SPLINE INTERPOLATED ENGY/X-SECT.
C                           PAIRS FOR GRAPHICAL DISPLAY.
C
C  INPUT : (L*4)  LGRD1   = .TRUE.  => PUT GRAPH IN GRID FILE
C                           .FALSE. => DO NOT PUT GRAPH IN GRID FILE
C  INPUT : (L*4)  LDEF1   = .TRUE.  => USE DEFAULT GRAPH SCALING
C                           .FALSE. => DO NOT USE DEFAULT GRAPH SCALING
C  INPUT : (L*4)  LFSEL   = .TRUE.  => CARRY OUT MINIMAX POLYNOMIAL
C                                      FITTING
C                           .FALSE. => - DO NOT DO THE ABOVE -
C
C  INPUT : (R*8)  XMIN    = GRAPH: LOWER LIMIT FOR ENERGY (eV/amu)
C  INPUT : (R*8)  XMAX    = GRAPH: UPPER LIMIT FOR ENERGY (eV/amu)
C  INPUT : (R*8)  YMIN    = GRAPH: LOWER LIMIT FOR CROSS-SECTION (cm**2)
C  INPUT : (R*8)  YMAX    = GRAPH: UPPER LIMIT FOR CROSS-SECTION (cm**2)
C
C
C          (R*4)  GHZERO  = PARAMETER = VALUE BELOW WHICH GHOST80
C                           TAKES NUMBERS AS BEING ZERO = 1.0E-36
C          (R*4)  YDMIN   = PARAMETER = MINIMUM ALLOWED Y-VALUE FOR
C                           PLOTTING. (USED FOR DEFAULT GRAPH SCALING)
C                           (SET TO 'GHZERO'/ZERO TO REMOVE)
C
C          (I*4)  I4UNIT  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (I*4)  I       = GENERAL USE - ARRAY INDEX
C          (I*4)  IKEY    = NUMBER OF 'KEY()' VALUES TO BE OUTPUT
C          (I*4)  ICOUNT  = NUMBER OF POINTS PLOTTED FOR GRAPH CURVE
C
C          (R*4)  XHIGH   = UPPER X-AXIS LIMIT FOR USE WITH GHOST80
C          (R*4)  XLOW    = LOWER X-AXIS LIMIT FOR USE WITH GHOST80
C          (R*4)  YHIGH   = UPPER Y-AXIS LIMIT FOR USE WITH GHOST80
C          (R*4)  YLOW    = LOWER Y-AXIS LIMIT FOR USE WITH GHOST80
C          (R*4)  XA4()   = X-AXIS CO0ORDINATES FOR USE WITH GHOST80
C          (R*4)  YA4()   = Y-AXIS CO0ORDINATES FOR USE WITH GHOST80
C
C          (C*88) ISPEC   = GRAPH TITLE (INCORPORATES 'TITLE')
C          (C*80) CADAS   = ADAS HEADER: INCLUDES RELEASE, PROGRAM, TIME
C          (C*12) DNAME   = '      DATE: '
C          (C*23) XTIT    = X-AXIS UNITS/TITLE
C          (C*23) YTIT    = Y-AXIS UNITS/TITLE
C          (C*9)  KEY0    = '    KEY: '
C          (C*9)  MNMX0   = 'MINIMAX: '
C          (C*8)  CINFO   = 'TITLE  : '
C          (C*8)  ADAS0   = 'ADAS   :'
C          (C*22) KEY()   = DESCRIPTIVE KEY FOR GRAPH (3 TYPES)
C
C          (C*1)  GRID    = DUMMY NAME VARIABLE FOR USE WITH GHOST80
C          (C*1)  PIC     = DUMMY NAME VARIABLE FOR USE WITH GHOST80
C          (C*3)  C3BLNK  = BLANK 3 BYTE STRING
C          (C*7)  C7      =  7 BYTE STRING = 'TITLX(1:4)'//'C3BLNK'
C
C
C ROUTINES:
C          ROUTINE     SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          XXADAS      ADAS      GATHERS ADAS HEADER INFORMATION
C          XXLIM8      ADAS      SETTING UP OF DEFAULT GRAPH AXES
C          XXGSEL      ADAS      SELECTS POINTS WHICH WILL FIT ON GRAPH
C          I4UNIT      ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C                      GHOST80   NUMEROUS SUBROUTINES
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    07/02/91
C
C UPDATE:  07/08/91 - PE BRIDEN: INTRODUCED SUBROUTINE 'XXGSEL'
C
C UPDATE:  25/11/91 - PE BRIDEN: MADE FILNAM/PICSAV ARGUMENT LIST
C                                COMPATIBLE WITH GHOST VERSION 8.
C
C UPDATE:  23/04/93 - PE BRIDEN - ADAS91: ADDED I4UNIT FUNCTION TO WRITE
C                                         STATEMENTS FOR SCREEN MESSAGES
C
C UPDATE:  24/05/93 - PE BRIDEN - ADAS91: CHANGED I4UNIT(0)-> I4UNIT(-1)
C
C UPDATE:  22/09/93 - J NASH    - ADAS91:
C          REMOVED PRINTING OF DATA SUB-BLOCK TITLE (COMMENTED OUT).
C
C UPDATE:  01/05/95 - Tim Hammond - IDL-ADAS:
C                     UNIX conversion
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      REAL*4     GHZERO           , YDMIN
C-----------------------------------------------------------------------
      PARAMETER( GHZERO = 1.0E-36 , YDMIN = 1.0E-20 )
C-----------------------------------------------------------------------
      INTEGER    I4UNIT
      INTEGER    NENER           , NMX              , NPSPL
      INTEGER    I               , IKEY             , ICOUNT
      INTEGER    PIPEOU          , ONE              , ZERO
      PARAMETER (PIPEOU = 6      , ONE=1            , ZERO=0)
C-----------------------------------------------------------------------
      REAL*8     ENERA(NENER)    , SIGA(NENER)      ,
     &           EOMA(NMX)       , SIGOMA(NMX)      ,
     &           EOSA(NPSPL)     , SIGOSA(NPSPL)    ,
     &           XMIN            , XMAX             ,
     &           YMIN            , YMAX
C-----------------------------------------------------------------------
      REAL*4     XHIGH           , XLOW             ,
     &           YHIGH           , YLOW
      REAL*4     XA4(100)        , YA4(100)
C-----------------------------------------------------------------------
      CHARACTER  TITLE*40 , TITLF*80    , TITLX*120  , TITLM*80  ,
     &           DATE*8
      CHARACTER  CADAS*80 ,
     &           ISPEC*88 , DNAME*12    , XTIT*23   , YTIT*23   ,
     &           MNMX0*9     , KEY0*9   , ADAS0*8   , CINFO*8   ,
     &           KEY(3)*22
      CHARACTER  GRID*1   , PIC*1       , C3BLNK*3  , C7*7
C-----------------------------------------------------------------------
      LOGICAL    LGHOST   , LGRD1       , LDEF1     , LFSEL
C-----------------------------------------------------------------------
      SAVE       ISPEC    , CADAS
C-----------------------------------------------------------------------
      DATA ISPEC(1:48)
     & /'CHARGE-EXCHANGE  CROSS-SECTION  VERSUS  ENERGY: '/
      DATA DNAME/'      DATE: '/
      DATA XTIT/'    ENERGY (eV/amu)    '/
      DATA YTIT/' CROSS-SECTION (cm**2) '/
      DATA ADAS0/'ADAS   :'/
     &     CINFO/'TITLE  :'/,
     &     MNMX0/'MINIMAX: '/,
     &     KEY0 /'KEY    : '/,
     &     KEY(1)/'(CROSSES - INPUT DATA)'/,
     &     KEY(2)/' (FULL LINE - SPLINE) '/,
     &     KEY(3)/'(DASH LINE - MINIMAX) '/
      DATA GRID  /' '/   ,
     &     PIC   /' '/   ,
     &     C3BLNK/'   '/ ,
     &     CADAS /' '/
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C SET UP GRAPH HEADINGS
C-----------------------------------------------------------------------
C
CX    ISPEC(49:88)=TITLE
CX    CALL XXADAS( CADAS )
CX    WRITE OUT TITLES AND DATE INFORMATION
         WRITE(PIPEOU,'(A120)') TITLX
         WRITE(PIPEOU,'(A80)') TITLM
         WRITE(PIPEOU,'(A80)') TITLF
         WRITE(PIPEOU,'(A8)') DATE
         CALL XXFLSH(PIPEOU)
C
CX    WRITE OUT ARRAY SIZES
         WRITE(PIPEOU,*) NENER
         WRITE(PIPEOU,*) NPSPL
         CALL XXFLSH(PIPEOU)

CX    DATA FROM DATA FILE
         DO 1,I=1, NENER
             WRITE(PIPEOU,*) ENERA(I)
1        CONTINUE
         CALL XXFLSH(PIPEOU)
         DO 2,I=1,NENER
             WRITE(PIPEOU,*) SIGA(I)
2        CONTINUE
         CALL XXFLSH(PIPEOU)

CX     WRITE OUT INTERPOLATED SPLINE VALUES
         WRITE(PIPEOU,*) ONE
         DO 3,I = 1, NPSPL      
             WRITE(PIPEOU,*) EOSA(I)
3        CONTINUE
         CALL XXFLSH(PIPEOU)
         DO 4,I = 1, NPSPL
             WRITE(PIPEOU,*) SIGOSA(I)
4        CONTINUE
         CALL XXFLSH(PIPEOU)

CX     WRITE OUT USER SELECTED AXES RANGES.
         IF (.NOT. LDEF1 ) THEN
             WRITE(PIPEOU,*) ZERO
         ELSE
             WRITE(PIPEOU,*) ONE
             WRITE(PIPEOU,*) XMIN
             WRITE(PIPEOU,*) XMAX
             WRITE(PIPEOU,*) YMIN
             WRITE(PIPEOU,*) YMAX
         ENDIF
         CALL XXFLSH(PIPEOU)

CX     IF SELECTED WRITE OUT MINMAX FIT VALUES
         IF (LFSEL) THEN
             WRITE(PIPEOU,*) ONE
             WRITE(PIPEOU,*) NMX
             CALL XXFLSH(PIPEOU)
             DO 5 I = 1, NMX
                 WRITE(PIPEOU,*) EOMA(I)
 5           CONTINUE
             CALL XXFLSH(PIPEOU)
             DO 6 I = 1, NMX
                 WRITE(PIPEOU,*) SIGOMA(I)
 6           CONTINUE
             CALL XXFLSH(PIPEOU)
         ELSE
             WRITE(PIPEOU,*) ZERO
             CALL XXFLSH(PIPEOU)
         ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN
C-----------------------------------------------------------------------
      END
