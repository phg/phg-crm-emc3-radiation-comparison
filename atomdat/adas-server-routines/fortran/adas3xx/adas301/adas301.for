CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas301/adas301.for,v 1.7 2007/05/24 15:27:21 mog Exp $ Date $Date: 2007/05/24 15:27:21 $
CX
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 PROGRAM: ADAS301 **********************
C
C  ORIGINAL NAME: SPRTGRF1
C
C  VERSION:  1.2 (ADAS91)
C
C  PURPOSE:  TO GRAPH AND INTERPOLATE SELECTED DATA FROM COMPILATIONS
C            OF SHELL  RESOLVED  CHARGE  EXCHANGE  CROSS-SECTIONS FOR
C            CAPTURE FROM HYDROGEN BY BARE NUCLEI.
C
C            THE SELECTED INPUT DATA SET CONTAINS A NUMBER OF SEPARATE
C            BLOCKS  OF DATA GENERALLY FROM DIFFERENT  SOURCES.   THIS
C            PROGRAM  ALLOWS THE EXAMINATION AND DISPLAY OF A SELECTED
C            DATA BLOCK AS FOLLOWS:
C
C            TABULATED VALUES FOR THE SELECTED DATA BLOCK ARE PLOTTED
C            & FITTED USING A CUBIC SPLINE ROUTINE, FROM WHICH INTER-
C            POLATIONS ARE MADE IF REQUIRED.   IF REQUESTED A MINIMAX
C            POLYNOMIAL  IS FITTED THROUGH THE  DATA  AND GRAPHICALLY
C            DISPLAYED. THE MINIMAX COEFFICIENTS AND THE ACCURACY  OF
C            THE FIT ARE PRINTED OUT WITH THE GRAPH.
C
C            IF REQUESTED THE GRAPH AND A SUMMARY TEXT FILE ARE OUTPUT
C            TO HARDCOPY.
C
C
CX DATA:     THE SOURCE DATA IS CONTAINED AS MEMBERS IN A PARTITIONED
CX           DATA SET AS FOLLOWS:-
CX
CX                   'JETSHP.QCX#<DONOR>.DATA(<MEMBER>)'
CX
CX           WHERE
CX             <DONOR>    = <DONOR ELEMENT SYMBOL><DONOR CHARGE STATE>
CX             <MEMBER>   = <ID>#<RECEIVER>
CX
CX             <ID>       = 3 CHARACTER PREFIX IDENTIFYING THE SOURCE
CX                          OF THE DATA.
CX             <RECEIVER> = <RECVR ELEMENT SYMBOL><RECVR CHARGE STATE>
CX
CX
CX             E.G. 'JETSHP.QCX#H0.DATA(OLD#C6)'
C
C           THE UNITS USED IN THE DATA FILE ARE TAKEN AS FOLLOWS:
C
C           ENERGIES            : KEV / AMU
C           CROSS SECTIONS      : CM**2
C
C
C
C  PROGRAM:
C
C          (I*4)  NENRGY  = PARAMETER = NO. OF TABULATED INPUT ENERGIES.
C          (I*4)  NSHELL  = PARAMETER = NUMBER  OF  TABULATED  PRINCIPAL
C                                       QUANTUM N-SHELLS.
C          (I*4)  NBLOCK  = PARAMETER = (MAXIMUM) NUMBER OF  INPUT  DATA
C                                       SUB-BLOCKS.
C          (I*4)  NDEIN   = PARAMETER = MAX. NO. OF ISPF ENTRED ENERGIES
C          (I*4)  IUNT07  = PARAMETER = OUTPUT UNIT FOR RESULTS
C          (I*4)  IUNT10  = PARAMETER = INPUT UNIT FOR DATA
C
C          (I*4)  NPSPL   = PARAMETER =
C                           NUMBER  OF SPLINE INTERPOLATED ENERGY/X-SECT
C                           PAIRS FOR GRAPHICAL DISPLAY.
C          (I*4)  NMX     = PARAMETER =
C                           NUMBER OF MINIMAX  GENERATED   ENERGY/X-SECT
C                           PAIRS FOR GRAPHICAL DISPLAY.
C          (I*4)  MAXDEG  = PARAMETER = MAX. NO. OF DEGREES FOR MINIMAX
C          (I*4)  L3      = PARAMETER = 3
C
C          (L*4)  LMLOG   = PARAMETER = .TRUE. => FIT MINIMAX POLYNOMIAL
C                           TO LOG10 TRANSFORMATION OF INPUT DATA.
C
C          (I*4)  I4UNIT  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (I*4)  NBLKS   = NUMBER OF DATA SUB-BLOCKS READ IN.
C          (I*4)  IBLK    = DATA SUB-BLOCK ELEMENT INDEX
C          (I*4)  IFORM   = ENERGY/VELOCITY FORM INDEX
C          (I*4)  IBSEL   = SELECTED INPUT DATA SUB-BLOCK FOR ANALYSIS
C          (I*4)  INSEL   = SELECTED INPUT DATA n QUANTUM SHELL
C                           ( 0 => TOTAL CROSS-SECTION)
C          (I*4)  ILSEL   = SELECTED INPUT DATA L QUANTUM SHELL
C                           ( -1    => TOTAL CROSS-SECTION FOR N SHELL)
C          (I*4)  IMSEL   = SELECTED INPUT DATA M QUANTUM SHELL
C                           ( -1    => TOTAL CROSS-SECTION FOR NL SHELL)
C          (I*4)  KPLUS1  = NUMBER OF MINIMAX COEFFICIENTS
C          (I*4)  IEVAL   = NO. OF ISPF ENTERED ENERGY/VELOCITY VALUES
C          (I*4)  IETYP   = 1 => 'EIN(array)' UNITS: AT.UNITS (VELOCITY)
C                         = 2 => 'EIN(array)' UNITS: CM/SEC   (VELOCITY)
C                         = 3 => 'EIN(array)' UNITS: EV/AMU   (ENERGY)
C          (I*4)  IPAN    = ISPF PANEL NUMBER
C          (I*4)  IE      = ENERGY INDEX
C
C          (R*8)  R8ECON  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (R*8)  XMIN    = GRAPH: LOWER LIMIT FOR VELOCITY/ENERGY
C                                  UNITS: SEE 'IETYP'
C          (R*8)  XMAX    = GRAPH: UPPER LIMIT FOR VELOCITY/ENERGY
C                                  UNITS: SEE 'IETYP'
C          (R*8)  XMINE   = GRAPH: LOWER LIMIT FOR ENERGY
C                                  UNITS: EV/AMU
C          (R*8)  XMAXE   = GRAPH: UPPER LIMIT FOR ENERGY
C                                  UNITS: EV/AMU
C          (R*8)  YMIN    = GRAPH: LOWER LIMIT FOR X-SECTION (cm**2)
C          (R*8)  YMAX    = GRAPH: UPPER LIMIT FOR X-SECTION (cm**2)
C          (R*8)  TOLVAL  = FRACTIONAL  TOLERANCE  FOR  ACCEPTANCE  OF
C                           MINIMAX POLYNOMIAL FIT TO DATA (IF SELECTED)
C
C          (L*4)  OPEN10  = .TRUE.  => FILE ALLOCATED TO UNIT 10.
C                         = .FALSE. => NO FILE ALLOCATED TO UNIT 10.
C          (L*4)  LGHOST  = .TRUE.  => GHOST80 OUTPUT WRITTEN
C                         = .FALSE. => NO GHOST80 OUTPUT WRITTEN
C          (L*4)  LPEND   = .TRUE.  => END ANALYSIS OF CURRENT DATA SET
C                         = .FALSE. => CONTINUE ANALYSIS WITH CURRENT
C                                      DATA SET.
C          (L*4)  LOSEL   = .TRUE.  => CALCULATE INTERPOLATED VALUES FOR
C                                      OUTPUT
C                           .FALSE. => - DO NOT DO THE ABOVE -
C          (L*4)  LFSEL   = .TRUE.  => CARRY OUT MINIMAX POLYNOMIAL
C                                      FITTING
C                           .FALSE. => - DO NOT DO THE ABOVE -
C          (L*4)  LDSEL   = .TRUE.  => DISPLAY INPUT FILE DESCRIPTIVE
C                                      TEXT
C                           .FALSE. => - DO NOT DO THE ABOVE -
C          (L*4)  LGRD1   = .TRUE.  => GRAPH OUTPUT TO BE STORED IN
C                                      GHOST80 GRID FILE.
C                           .FALSE. => DO NOT STORE GRAPH IN GRID FILE.
C          (L*4)  LDEF1   = .TRUE.  => USE GRAPH DEFAULT SCALING
C                           .FALSE. => DO NOT USE DEFAULT SCALING
C
C          (C*3)  REP     = 'YES' => TERMINATE PROGRAM EXECUTION
C                           'NO'  => CONTINUE PROGRAM EXECUTION
C          (C*8)  DATE    = CURRENT DATE (AS 'DD/MM/YY')
C          (C*80) DSFULL  = FULL INPUT DATA SET NAME (READ FROM ISPF)
C                           (INCORPORATED INTO 'TITLX')
C          (C*40) TITLE   = TITLE OF RUN (READ FROM ISPF PANEL)
C          (C*80) TITLM   = DIAGNOSTIC LINE INFORMATION FROM 'MINIMAX'
C          (C*120)TITLX   = INFORMATION STRING CONTAINING: INPUT DATA
C                           FILE-NAME, DATA BLOCK , N, L AND M VALUES
C
C          (I*4)  NENER() = NUMBER OF ENERGIES IN 'ENERA(,)' FOR EACH
C                           DATA SUB-BLOCK.
C                           DIMENSION: DATA SUB-BLOCK INDEX
C          (I*4)  IZR()   = ION CHARGE OF RECEIVER.
C                           DIMENSION: DATA SUB-BLOCK INDEX
C          (I*4)  IZD()   = ION CHARGE OF DONOR.
C                           DIMENSION: DATA SUB-BLOCK INDEX
C          (I*4)  INDD()  = DONOR STATE INDEX.
C                           DIMENSION: DATA SUB-BLOCK INDEX
C
C          (I*4)  NDATA(,)= INPUT DATA FILE: VALID RANGE FOR N QUANTUM
C                                            NO. IN PARTIAL CROSS-SEC.
C                           1st DIMENSION: 1 => LOWER INDEX BOUND
C                                          2 => UPPER INDEX BOUND
C                           2nd DIMENSION: DATA SUB-BLOCK INDEX
C          (I*4)  IEDATA(,)=VALID RANGE FOR ENERGY INDEX
C                           1st DIMENSION: 1 => LOWER INDEX BOUND
C                                          2 => UPPER INDEX BOUND
C                           2nd DIMENSION: DATA SUB-BLOCK INDEX
C          (I*4)  LFORMA(,)= PARAMETERS FOR CALCULATING L-RES X-SEC.
C                            1ST DIMENSION: ENERGY INDEX
C                            2ND DIMENSION: DATA SUB-BLOCK INDEX
C
C          (R*8)  COEF()  = COEFFICIENTS OF FITTED MINIMAX POLYNOMIAL
C          (R*8)  SIGA()  = VALID CROSS SECTIONS FROM INPUT FILE FOR
C                           PRINCIPAL QUANTUM NUMBER 'INSEL' AND DATA
C                           SUB-BLOCK 'IBSEL'. (UNITS: CM**2)
C                           DIMENSION: VALID ENERGY INDEX.
C          (R*8)  EIN()   = ISPF ENTERED ELECTRON VELOCITY/ENERGIES
C                           (NOTE: UNITS ARE GIVEN BY 'IETYP')
C          (R*8)  EOA()   = ISPF ENTERED ELECTRON ENERGIES
C                           (UNITS: EV/AMU)
C          (R*8)  SIGOA() = SPLINE INTEROPLATED X-SEC VALUE AT 'EOA()'
C                           (UNITS: CM**2)
C                           (EXTRAPOLATED VALUES = 0.0 )
C
C          (R*8)  EOSA()  = SPLINE: SELECTED ENERGIES (EV/AMU)
C          (R*8)  SIGOSA()= SPLINE INTEROPLATED X-SEC VALUE AT 'EOSA()'
C                           (UNITS: CM**2)
C
C          (R*8)  EOMA()  = MINIMAX: SELECTED ENERGIES (EV/AMU)
C          (R*8)  SIGOMA()= MINIMAX GENERATED X-SEC VALUE AT 'EOMA()'
C                           (UNITS: CM**2)
C
C          (R*8)  ENERA(,) = INPUT DATA FILE: TABULATED ENERGIES (EV)
C                            1ST DIMENSION: ENERGY INDEX
C                            2ND DIMENSION: DATA SUB-BLOCK INDEX
C          (R*8)  ALPHAA(,)= INPUT DATA FILE: EXTRAPOLATION PARAMETERS
C                            1ST DIMENSION: ENERGY INDEX
C                            2ND DIMENSION: DATA SUB-BLOCK INDEX
C          (R*8)  XLCUTA(,)= PARAMETERS FOR CALCULATING L-RES X-SEC.
C                            1ST DIMENSION: ENERGY INDEX
C                            2ND DIMENSION: DATA SUB-BLOCK INDEX
C          (R*8)  PL2A(,)  = PARAMETERS FOR CALCULATING L-RES X-SEC.
C                            1ST DIMENSION: ENERGY INDEX
C                            2ND DIMENSION: DATA SUB-BLOCK INDEX
C          (R*8)  PL3A(,)  = PARAMETERS FOR CALCULATING L-RES X-SEC.
C                            1ST DIMENSION: ENERGY INDEX
C                            2ND DIMENSION: DATA SUB-BLOCK INDEX
C          (R*8)  XTOT(,) = INPUT DATA FILE: TOTAL CROSS-SECTIONS(CM**2)
C                           1st DIMENSION: ENERGY INDEX
C                           2nd DIMENSION: DATA SUB-BLOCK INDEX
C
C          (R*8)  EVALS(,,)= VALID INPUT DATA FILE ENERGIES/VELOCITIES
C                            1ST DIMENSION: VALID ENERGY INDEX
C                                           ( SEE 'ENERA(,)' )
C                            2ND DIMENSION: 1 => AT.UNITS (IETYP=1) VEL.
C                                           2 => CM/SEC   (IETYP=2) VEL.
C                                           3 => EV/AMU   (IETYP=3) ENGY
C                            3RD DIMENSION: DATA SUB-BLOCK INDEX
C          (R*8)  XSIGN(,,)=INPUT DATA FILE: N-RESOLVED CROSS-SECTIONS.
C                           1st DIMENSION: ENERGY INDEX
C                           2nd DIMENSION: QUANTUM N-SHELL INDEX
C                           3rd DIMENSION: DATA SUB-BLOCK INDEX
C          (R*8)  XSIGL(,,)=INPUT DATA FILE: L-RESOLVED CROSS-SECTIONS.
C                           1st DIMENSION: ENERGY INDEX
C                           2ND DIMENSION: INDEXED BY FUNCTION I4IDFL.
C                           3rd DIMENSION: DATA SUB-BLOCK INDEX
C          (R*8)  XSIGM(,,)=INPUT DATA FILE: M-RESOLVED CROSS-SECTIONS.
C                           1st DIMENSION: ENERGY INDEX
C                           2ND DIMENSION: INDEXED BY FUNCTION I4IDFM.
C                           3rd DIMENSION: DATA SUB-BLOCK INDEX
C
C          (L*4)  LERNG() = .TRUE.  => ENERGY 'EOA()' IN RANGE
C                           .FALSE. => ENERGY 'EOA()' OUT OF RANGE
C                           (RANGE = INPUT ENERGY RANGE)
C          (L*4)  LPARMS()= FLAGS IF L-SPLITTING PARAMETERS PRESENT.
C                           .TRUE.  => L-SPLITTING PARAMETERS PRESENT.
C                           .FALSE  => L-SPLITTING PARAMETERS ABSENT.
C          (L*4)  LSETL() = .TRUE.  => L-RESOLVED DATA READ
C                           .FALSE. => NO L-RESOLVED DATA READ
C          (L*4)  LSETM() = .TRUE.  => M-RESOLVED DATA READ
C                           .FALSE. => NO M-RESOLVED DATA READ
C
C          (C*80) TITLF() = INPUT DATA FILE: SUB-BLOCK HEADINGS
C                           DIMENSION: DATA SUB-BLOCK INDEX.
C          (C*2)  SYMBR() = INPUT DATA FILE: RECEIVER ION ELEMENT SYMBOL
C                           DIMENSION: DATA SUB-BLOCK INDEX.
C          (C*2)  SYMBD() = INPUT DATA FILE: DONOR ION ELEMENT SYMBOL
C                           DIMENSION: DATA SUB-BLOCK INDEX.
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          C1SPF0     ADAS      GATHERS INPUT FILE NAMES VIA ISPF PANELS
C          C1BENG     ADAS      SELECTS VALID INPUT ENERGIES FROM C1DATA
C          C1SETP     ADAS      SETS UP ISPF NON-READABLE VARIABLES
C          C1ISPF     ADAS      GATHERS USERS VALUES VIA ISPF PANELS
C          C1BSIG     ADAS      SETS UP X-SECTIONS FOR SELECTED N-SHELL
C          C1SPLN     ADAS      SPLINE CURVE FITTING/INTERPOLATION.
C          C1OUTG     ADAS      GRAPHICAL OUTPUT ROUTINE USING GHOST80
C          C1OUT0     ADAS      RESULTS OUTPUT ROUTINE
C          CXDATA     ADAS      READS INPUT DATA SET IN ADF01 FORMAT.
C          XX0000     ADAS      SET MACHINE DEPENDANT ADAS CONFIGURATION
C          XXDATE     ADAS      PROVIDES CURRENT DATE
C          XXENDG     ADAS      CLOSE GHOST80 GRIDFILE & OUTPUT CHANNEL
C          XXMNMX     ADAS      MINIMAX POLYNOMIAL FIT ROUTINE
C          XXTEXT     ADAS      DISPLAYS DESCRIPTIVE TEXT FROM FILE
C          XXECON     ADAS      CONVERTS ISPF ENTERED ENERGY/VEL. FORM
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          R8ECON     ADAS      REAL*8 FUNCTION: CONVERT ENRGY/VEL. FORM
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 4569
C
C DATE:    20/02/91
C
C UPDATE:  11/06/91 PE BRIDEN: ADAS91 - 'LDOUT' REMOVED - NOT USED
C
C UPDATE:  23/04/93 - PE BRIDEN - ADAS91: ADDED I4UNIT FUNCTION TO WRITE
C                                         STATEMENTS FOR SCREEN MESSAGES
C
C UPDATE:  26/04/93 - PE BRIDEN - ADAS91: ADDED CALL TO XX0000
C
C UPDATE:  24/05/93 - PE BRIDEN - ADAS91: CHANGED I4UNIT(0)-> I4UNIT(-1)
C
C UPDATE:  22/09/93 - J NASH    - ADAS91:
C          UPDATED TO READ NEW ADF01 DATA FORMAT. REPLACED CALL TO
C          'C1DATA' WITH 'CXDATA'. SINCE NEW FORMAT FILES CONTAIN ONLY
C          ONE BLOCK OF DATA, DATA NOW LOADED INTO FIRST SUB-BLOCK.
C          REMOVED CALL TO C1BENG (NOT NEEDED).
C
C UPDATE:  18/10/93 - J NASH    - ADAS91:
C          UPDATED TO NEW VERSION OF CXDATA, WHICH NOW READS L-SPLITTING
C          PARAMETERS.
C
C UNIX-IDL PORT:
C
C AUTHOR:  TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    10/04/95
C
C VERSION: 1.1				DATE: 10/04/95
C MODIFIED: TIM HAMMOND
C	    - FIRST CONVERTED
C 
C UPDATE:  18/04/95 - H P SUMMMERS - ADAS91:
C          UPDATED TO INCLUDE SELECTION OF TOTAL, N, NL OR NLM CROSS-
C          CROSS-SECTIONS IF AVAILABLE.
C          - VERSION NUMBER CHANGED FROM 1.1 TO 1.2
C          - VERSION NUMBER SUPERCEDED BY SCCS VERSION NUMBER
C
C UPDATE:  26/04/95 - TIM HAMMOND - UNIX PORT
C          ADDED NDATA IN CALL TO C1ISPF SO THE VALUES CAN
C          BE TRANSFERRED TO IDL.
C
C UPDATE:  01/05/95 - TIM HAMMOND - UNIX PORT
C	   MINOR CHANGES TO FORMATTING OF TITLES.
C
C UPDATE:  04/05/95 - TIM HAMMOND - UNIX PORT
C          REMOVED LDSEL FROM CALL TO C1SPF0
C
C VERSION: 1.6				DATE: 05/06/96
C MODIFIED: WILLIAM OSBORN
C	    - ADDED MENU BUTTON CODE
C           - ADDED 'IF (LGRAPH) THEN ... ENDIF' AROUND C1OUTG
C 
C VERSION  : 1.7
C DATE     : 22-05-2007
C MODIFIED : Martin O'Mullane
C              - Use xxdata_01 to access adf01 data.
C              - Remove unused m-subshell data possibility.
C
C-----------------------------------------------------------------------
      INTEGER    NENRGY       , NSHELL       , NBLOCK     , NDEIN      ,
     &           MAXDEG       , ISTOP        , PIPEIN
      INTEGER    IUNT07       , IUNT10
      INTEGER    NPSPL        , NMX
      INTEGER    L3
C-----------------------------------------------------------------------
      LOGICAL    LMLOG
C-----------------------------------------------------------------------
      PARAMETER( NENRGY =  30 , NSHELL =  20 , NBLOCK = 1 , NDEIN = 30 ,
     &           MAXDEG =  19 )
      PARAMETER( IUNT07 =   7 , IUNT10 =  10 )
      PARAMETER( NPSPL  = 100 , NMX    = 100 )
      PARAMETER( L3     =   3 , PIPEIN = 5)
C-----------------------------------------------------------------------
      PARAMETER( LMLOG  = .TRUE. )
C-----------------------------------------------------------------------
      INTEGER    I4UNIT
      INTEGER    NBLKS        , IBLK         , IFORM      ,
     &           IBSEL        , INSEL        , ILSEL      , IMSEL      ,
     &           KPLUS1     ,
     &           IEVAL        , IETYP        , IPAN       ,
     &           IE
C-----------------------------------------------------------------------
      REAL*8     R8ECON
      REAL*8     XMIN         , XMAX         , XMINE      , XMAXE      ,
     &           YMIN         , YMAX
      REAL*8     TOLVAL
C-----------------------------------------------------------------------
      LOGICAL    OPEN10       , LGHOST       , LPEND      , LOSEL      ,
     &           LFSEL
      LOGICAL    LDSEL        , LGRD1        , LDEF1
C-----------------------------------------------------------------------
CX    UNIX PORT - Following variables added for output selection options
      LOGICAL    LGRAPH       , L2FILE
C-----------------------------------------------------------------------
      CHARACTER  REP*3        , DATE*8       , DSFULL*80
      CHARACTER  TITLE*40     , TITLM*80     , TITLX*120
CX    UNIX PORT - Following is name of output text file
      CHARACTER  SAVFIL*80
C-----------------------------------------------------------------------
      INTEGER    NENER(NBLOCK)          , IZR(NBLOCK)           ,
     &           IZD(NBLOCK)            , INDD(NBLOCK)
      INTEGER    NDATA(2,NBLOCK)        , IEDATA(2,NBLOCK)      ,
     &           LFORMA(NENRGY,NBLOCK)
C-----------------------------------------------------------------------
      REAL*8     COEF(MAXDEG+1)        , SIGA(NENRGY)          ,
     &           EIN(NDEIN)            ,
     &           EOA(NDEIN)            , SIGOA(NDEIN)          ,
     &           EOSA(NPSPL)           , SIGOSA(NPSPL)         ,
     &           EOMA(NMX)             , SIGOMA(NMX)
      REAL*8     XTOT(NENRGY,NBLOCK)    , ALPHAA(NENRGY,NBLOCK)  ,
     &           XLCUTA(NENRGY,NBLOCK)  , PL2A(NENRGY,NBLOCK)    ,
     &           PL3A(NENRGY,NBLOCK)    , ENERA(NENRGY,NBLOCK)
      REAL*8     EVALS(NENRGY,3,NBLOCK)                       ,
     &           XSIGN(NENRGY,NSHELL,NBLOCK)                  ,
     &           XSIGL(NENRGY,(NSHELL*(NSHELL+1))/2,NBLOCK)
C-----------------------------------------------------------------------
      LOGICAL    LERNG(NDEIN)          , LPARMS(NBLOCK)       ,
     &           LSETL(NBLOCK)         , LSETM(NBLOCK)
C-----------------------------------------------------------------------
      CHARACTER  TITLF(NBLOCK)*80      , SYMBR(NBLOCK)*2      ,
     &           SYMBD(NBLOCK)*2
C-----------------------------------------------------------------------
      DATA IPAN   /0/       , IEVAL  /0/
      DATA XMIN   /0/       , XMAX   /0/
      DATA OPEN10 /.FALSE./ , LGHOST /.FALSE./
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C *************************** MAIN PROGRAM ****************************
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C SET MACHINE DEPENDENT ADAS CONFIGURATION VALUES
C-----------------------------------------------------------------------
C
      CALL XX0000
C
C-----------------------------------------------------------------------
C GATHER CURRENT DATE
C-----------------------------------------------------------------------
C
      CALL XXDATE( DATE )
C-----------------------------------------------------------------------
  100 IPAN=0
C
C-----------------------------------------------------------------------
C IF FILE IS ACTIVE ON UNIT 10 - CLOSE THE UNIT
C-----------------------------------------------------------------------
C
         IF (OPEN10) THEN
            CLOSE(10)
            OPEN10=.FALSE.
         ENDIF
C
C-----------------------------------------------------------------------
C GET INPUT DATA SET NAME (FROM ISPF PANELS)
C-----------------------------------------------------------------------
C
      CALL C1SPF0( REP , DSFULL  )
C
C-----------------------------------------------------------------------
C IF PROGRAM RUN IS COMPLETED: CLOSE GHOST80 GRIDFILE/CHANNEL & END RUN
C-----------------------------------------------------------------------
C
         IF (REP.EQ.'YES') THEN
CX          IF (LGHOST) CALL XXENDG
            GOTO 9999
         ENDIF
C
C-----------------------------------------------------------------------
C OPEN INPUT DATA FILE - DSFULL
C-----------------------------------------------------------------------
C
CX    OPEN( UNIT=IUNT10 , FILE=DSFULL , ACTION='READ' )
      OPEN( UNIT=IUNT10 , FILE=DSFULL , STATUS='OLD'  )
      OPEN10=.TRUE.
CX
CX-----------------------------------------------------------------------
CX IF DESCRIPTIVE TEXT REQUESTED - SCAN MEMBER AND OUTPUT TEXT
CX-----------------------------------------------------------------------
CX THIS PART IS NOW HANDLED BY THE IDL SUBROUTINE XXTEXT.PRO
CX-----------------------------------------------------------------------
CX        IF (LDSEL) THEN
CX           CALL XXTEXT(IUNT10)
CX           GOTO 100
CX        ENDIF
C
C-----------------------------------------------------------------------
C FETCH DATA FROM SELECTED DATASET
C-----------------------------------------------------------------------

      call xxdata_01(iunt10     , nenrgy     , nshell     ,
     &               symbr(1)   , symbd(1)   , izr(1)     , izd(1)    ,
     &               indd(1)    , nener(1)   , ndata(1,1) , ndata(2,1),
     &               lparms(1)  , lsetl(1)   , enera      ,
     &               alphaa     , lforma     , xlcuta     , pl2a      ,
     &               pl3a       , xtot       , xsign      , xsigl
     &              )
C
      NBLKS = 1
      IEDATA(1,1) = 1
      IEDATA(2,1) = NENER(1)
C
C-----------------------------------------------------------------------
C SET UP ARRAY 'EVALS(,)' - CONTAINS INPUT VEL/ENERGIES IN THREE FORMS.
C-----------------------------------------------------------------------
C
         DO 1 IBLK=1,NBLKS
            DO 2 IFORM=1,3
               CALL XXECON( L3          , IFORM               ,
     &                      NENER(IBLK) , ENERA(1,IBLK)       ,
     &                                    EVALS(1,IFORM,IBLK)
     &                    )
    2       CONTINUE
    1    CONTINUE
C
C-----------------------------------------------------------------------
C SET UP ISPF FUNCTION POOL PARAMETERS (NON-READABLE VARIABLES)
C
C Set LSTEM to false for IDL code.
C-----------------------------------------------------------------------
C
      lsetm(1) = .FALSE.
      CALL C1SETP( NBLOCK , NBLKS , TITLF , LSETL(1) , LSETM(1) )
C
C-----------------------------------------------------------------------
C GET USER-DEFINED VALUES (FROM IDL PANELS)
C-----------------------------------------------------------------------
C
  200 CALL C1ISPF( IPAN     , LPEND    ,
     &             NDEIN    , NBLOCK   ,
     &             NENRGY   , NENER    , EVALS ,
     &             LSETL(1) , LSETM(1) ,
     &             TITLE    ,
     &             IBSEL    , INSEL    , ILSEL , IMSEL ,
     &             IETYP    , IEVAL    , EIN   ,
     &             LOSEL    , LFSEL    , LGRD1 , LDEF1 ,
     &             TOLVAL   ,
     &             XMIN     , XMAX     , YMIN  , YMAX  ,
     &             NDATA(1,1)          , NDATA(2,1)
     &           )
      IPAN=-1
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) ISTOP
      IF(ISTOP.EQ.1) GOTO 9999
C
C-----------------------------------------------------------------------
C IF END OF CURRENT FILE REQUESTED GO BACK TO INPUT FILE PANEL.
C-----------------------------------------------------------------------
C
      IF (LPEND) GOTO 100
C
C-----------------------------------------------------------------------
C IDENTIFY REQUESTED SUB-BLOCK CROSS-SECTIONS
C-----------------------------------------------------------------------
C
      CALL C1BSIG( NENRGY           , NSHELL           , NENER(IBSEL)  ,
     &             INSEL            , ILSEL            ,
     &             IEDATA(1,IBSEL)  , NDATA(1,IBSEL)   ,
     &             XTOT(1,IBSEL)    , XSIGN(1,1,IBSEL) ,
     &             XSIGL(1,1,IBSEL) ,
     &             ALPHAA(1,IBSEL)  ,
     &             SIGA
     &           )
C
C-----------------------------------------------------------------------
C CONVERT INPUT ENERGIES/VELOCITIES INTO EV/AMU (EIN -> EOA)
C-----------------------------------------------------------------------
C
      CALL XXECON( IETYP , L3 , IEVAL , EIN , EOA   )
C
C-----------------------------------------------------------------------
C CONVERT XMIN & XMAX PLOTTING UNITS INTO EV/AMU.
C-----------------------------------------------------------------------
C
      XMINE = R8ECON( IETYP , L3 , XMIN )
      XMAXE = R8ECON( IETYP , L3 , XMAX )
C
C-----------------------------------------------------------------------
C FIT CUBIC SPLINE AND INTERPOLATE X-SECS FOR GRAPHICAL & TABULAR OUTPUT
C-----------------------------------------------------------------------
C
      CALL C1SPLN(                  LOSEL ,
     &             NENER(IBSEL)   , IEVAL , NPSPL  ,
     &             ENERA(1,IBSEL) , EOA   , EOSA   ,
     &             SIGA           , SIGOA , SIGOSA ,
     &             LERNG
     &           )
C
C-----------------------------------------------------------------------
C  FIT MINIMAX POLYNOMIAL IF REQUIRED
C-----------------------------------------------------------------------
C
         IF (LFSEL) THEN
            CALL XXMNMX( LMLOG         , MAXDEG          , TOLVAL ,
     &                   NENER(IBSEL)  , ENERA(1,IBSEL)  , SIGA   ,
     &                   NMX           , EOMA            , SIGOMA ,
     &                   KPLUS1        , COEF            ,
     &                   TITLM
     &                 )
CX          WRITE(I4UNIT(-1),1000) TITLM(1:79)
         ENDIF
C
C-----------------------------------------------------------------------
C  CONSTRUCT DESCRIPTIVE TITLE FOR OUTPUT
C-----------------------------------------------------------------------
C
CX UNIX PORT - Modified construction of titlx because of variable filename
CX             lengths
CX
         IF (INSEL.EQ.0) THEN
            WRITE (TITLX,1001) DSFULL(1:70) , IBSEL
         ELSEIF(INSEL.GT.0.AND.ILSEL.LT.0) THEN
            WRITE (TITLX,1002) DSFULL(1:70) , IBSEL , INSEL
         ELSEIF(INSEL.GT.0.AND.ILSEL.GE.0.AND.IMSEL.LT.0) THEN
            WRITE (TITLX,1003) DSFULL(1:70) , IBSEL , INSEL , ILSEL
         ELSE
            WRITE (TITLX,1004) DSFULL(1:70), IBSEL , INSEL , ILSEL ,
     &                         IMSEL
         ENDIF
C
C-----------------------------------------------------------------------
C  GENERATE GRAPH
CX UNIX PORT - Added new call to c1spf1 routine which communicates with idl
CX             via a pipe to get user selected output options. Note that if
CX             user selects graph x-axis ranges these are assumed to be
CX             in ev/amu so no further conversion is carried out.
C-----------------------------------------------------------------------
C
300   CONTINUE
      CALL C1SPF1( DSFULL         , LFSEL        , LDEF1       ,
     &             LGRAPH         , L2FILE       , SAVFIL      ,
     &             XMINE          , XMAXE        , YMIN        ,
     &             YMAX           , LPEND )

C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) ISTOP
      IF(ISTOP.EQ.1) GOTO 9999

C-----------------------------------------------------------------------
CX    IF USER HAS SELECTED 'CANCEL' RETURN TO PROCESSING OPTIONS WINDOW.
C-----------------------------------------------------------------------
      IF (LPEND) GOTO 200

C-----------------------------------------------------------------------
C GRAPHICAL OUTPUT
C-----------------------------------------------------------------------
      IF (LGRAPH) THEN
         CALL C1OUTG( LGHOST         , TITLF(IBSEL) ,
     &             TITLE          , TITLX        , TITLM        , DATE ,
     &             ENERA(1,IBSEL) , SIGA         , NENER(IBSEL) ,
     &             EOMA           , SIGOMA       , NMX          ,
     &             EOSA           , SIGOSA       , NPSPL        ,
     &             LGRD1          , LDEF1        , LFSEL        ,
     &             XMINE          , XMAXE        , YMIN         , YMAX
     &           )
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
         READ(PIPEIN,*) ISTOP
         IF(ISTOP.EQ.1) GOTO 9999
C
      ENDIF
C
C----------------------------------------------------------------------
C OUTPUT RESULTS
C----------------------------------------------------------------------
C
      IF (L2FILE) THEN
         OPEN(UNIT=IUNT07, FILE=SAVFIL, STATUS='UNKNOWN')
      CALL C1OUT0( IUNT07   , LSETL(1) , LSETM(1)          ,
     &             LOSEL    , LFSEL    , TITLF(IBSEL)      ,
     &             TITLE    , TITLX    , TITLM    , DATE   ,
     &             INSEL    , ILSEL    , IMSEL    ,
     &             IEVAL    , LERNG    , EOA      , SIGOA  ,
     &             KPLUS1   , COEF
     &           )
        CLOSE(UNIT=IUNT07)
      ENDIF
C
C-----------------------------------------------------------------------
C  RETURN TO ISPF PANELS. (ON RETURN UNIT 10 IS CLOSED)
C-----------------------------------------------------------------------
C
CX    UNIX PORT - CONTROL RETURNS TO OUTPUT SELECTION SCREEN
CX    GOTO 200
      GOTO 300
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(/1X,'MINIMAX INFORMATION:'/1X,20('-')/1X,1A79/)
 1001 FORMAT(A70,3X,'- SUB-BLOCK:',1X,I1,3X,
     &                       '- TOTAL CROSS-SECTION',3X)
 1002 FORMAT(A70,3X,'- SUB-BLOCK:',1X,I1,3X,
     &                       '  N:',1X,I2,17X)
 1003 FORMAT(A70,3X,'- SUB-BLOCK:',1X,I1,3X,
     &                       '  N:',1X,I2,3X,'L:',1X,I2,9X)
 1004 FORMAT(A70,3X,'- SUB-BLOCK:',1X,I1,3X,
     &                       '  N:',1X,I2,3X,'L:',1X,I2,3X,'M:',1X,I2,1X
     &      )
C
C-----------------------------------------------------------------------
C
 9999 END
