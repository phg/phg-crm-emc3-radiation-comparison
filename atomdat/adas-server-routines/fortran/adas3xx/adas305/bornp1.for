       SUBROUTINE BORNP1(NMIN,NMAX,NLOW,LLOW,NUP,LUP,LAM,ANS,QA,IOPT,
     &IMAX1)
       IMPLICIT REAL*8(A-H,O-Z)
C-----------------------------------------------------------------------
C  PURPOSE: FIRST STAGE OF BORN X-SECT CALCULATION WITH DIRECTIONAL
C  BEAMS CAUSING TRANSITIONS BETWEEN HYDROGEN STARK/ZEEMAN STATES.
C
C  RADIAL INTEGRALS OF FORM P(N1,L1)*P(N2,L2)*J(LAM,Q*R) ARE EVALUATED
C  FOR ALL HYDROGENIC ORBITALS WITH NMIN<=NMAX AND N1<N2 FOR A SET
C  OF Q'S SPANNING THE EXTENT OF THE GENERALISED OSCILLATOR STRENGTH
C
C  CALLING ROUTINE WITH IOPT=1 PREPARES LOOKUP TABLES. CALL WITH IOPT=2
C  RETURNS VALUES.
C
C  MATRIX ELEMENT EVALUATION USES POWER SERIES ROUTINES.
C
C  STACK INTEGRALS OVER A GRID OF MOMENTUM CHANGE FROM 10**-2 TO 10**2
C  EQUALLY SPACED IN THE LOGARITHM (IMAX INTERVALS, IMAX+1 VALUES)
C
C  EVALUATE MULTIPOLES UP TO LAMMX=2
C
C  USE POINTER VECTORS FOR RAPID LOOKUP.
C
C  THE MAIN CALLING ROUTINE MUST HAVE THE LINE
C      CALL GAMAF(200)
C  BEFORE CALL TO BORNP1
C
C  ******** H.P. SUMMERS, JET          17 OCT 1988   *************
C  INPUT
C      NMIN=LOWEST N-SHELL
C      NMAX=HIGHEST N-SHELL
C      NLOW=LOWER N FOR SELECTED TRANSITION
C      LLOW=LOWER L FOR SELECTED TRANSITION
C      NUP=UPPER N FOR SELECTED TRANSITION
C      LUP=UPPER L FOR SELECTED TRANSITION
C      LAM=REQUIRED MULTIPOLE (0<=LAM<=LAMMX)
C      IOPT=1  PREPARE LOOKUP STACKS (ONLY NMIN,NMAX PARAMETERS USED)
C          =2  SUPPLY ANSWERS FOR SPECIFIED NLOW,LLOW,NUP,LUP,LAM CASE
C  OUTPUT
C      ANS(I)=BORN APPROXIMATION RESULT FOR I=1,IMAX+1
C      QA(I)=TRANSFERED MOMENTUM VECTOR
C      IMAX1=IMAX+1
C
C-----------------------------------------------------------------------
C
C ADAS305 version. Developed from JETSHP.STARK.FORT (H P Summers).
C
C VERSION  : 1.1
C DATE     : 24-02-2005
C MODIFIED : Martin O'Mullane
C             - First version.
C             - Change dimensions from 200 to 500.
C
C VERSION  : 1.2
C DATE     : 16-05-2007
C MODIFIED : Allan Whiteford
C             - Updated comments as part of subroutine documentation
C               procedure.
C
C VERSION  : 1.3
C DATE     : 09-12-2017
C MODIFIED : Martin O'Mullane
C             - Add save statement to remove need for special
C               compiler options.
C             - Do not reuse input variable LAM as DO loop variable.
C             - Add spacing to show the different RETURN paths. 
C
C----------------------------------------------------------------------
       DIMENSION GARR(100,41),QA(41),ANS(41)
       DIMENSION NMARK(20),ITPAR(100)
       DIMENSION A(500),B(500),C(500)
       DIMENSION GAM(500),JGAM(500)
       COMMON /FAC/GAM,JGAM
       
       save
       
       IF(IOPT.NE.1)GOTO 300
       IMAX=40
       IMAX1=IMAX+1
       Q0=1.0D-3
       Q1=1.0D1
       LAMMX=2
       Q0L=DLOG10(Q0)
       Q1L=DLOG10(Q1)
       DQL=(Q1L-Q0L)/IMAX
       DQ=10.0D0**DQL
       QA(1)=Q0
       DO 5 I=1,IMAX
    5  QA(I+1)=QA(I)*DQ
       IN=1
       NMARK(1)=1
       NT=1
       DO 200 N1=NMIN,NMAX-1
       DO 190 N2=N1+1,NMAX
       IND=((NMAX-NMIN)*(NMAX-NMIN+1))/2-
     &((NMAX-N1)*(NMAX-N1+1))/2+N2-N1
       DO 180 L1=0,N1-1
       DO 170 L2=0,N2-1
       DO 160 iLAM=0,LAMMX
       IF(iLAM.LT.IABS(L1-L2).OR.
     &    iLAM.GT.L1+L2.OR.
     &    MOD(L1+L2+iLAM,2).NE.0)
     & GO TO 160
       ITPAR(NT)=L1+16*(L2+16*iLAM)
C      WRITE(6,1000)IN,NMARK(IN),N1,L1,N2,L2,iLAM,NT,ITPAR(NT)
C      WRITE(7,1000)IN,NMARK(IN),N1,L1,N2,L2,iLAM,NT,ITPAR(NT)
       CALL RWFH(A,NA,IPA,UA,N1,L1)
       CALL RWFH(B,NB,IPB,UB,N2,L2)
       CALL PSPROD(A,NA,IPA,B,NB,IPB,C,NC,IPC)
       UC=UA+UB
       UCM=-UC
       DO 20 I=1,IMAX+1
       Q=QA(I)
       RES=0.0D0
       DO 50 J=IPC,IPC+NC-1
       NN=J
       W=WATINT(NN,iLAM,UCM,Q)
       RES=RES+C(J-IPC+1)*W
   50  CONTINUE
       GARR(NT,I)=RES
   20  CONTINUE
C      WRITE(6,1002)(GARR(NT,I),I=1,IMAX+1,10)
C      WRITE(7,1002)(GARR(NT,I),I=1,IMAX+1,10)
       NT=NT+1
  160  CONTINUE
  170  CONTINUE
  180  CONTINUE
C      WRITE(6,1001)IND
C      WRITE(7,1001)IND
       IN=IN+1
       NMARK(IN)=NT
  190  CONTINUE
  200  CONTINUE
       RETURN

  300  CONTINUE
       IF(LAM.LT.IABS(LLOW-LUP).OR.LAM.GT.LLOW+LUP.OR.
     &MOD(LLOW+LUP+LAM,2).NE.0)GO TO 400
       IND=((NMAX-NMIN)*(NMAX-NMIN+1))/2-
     &((NMAX-NLOW)*(NMAX-NLOW+1))/2+NUP-NLOW
       IPARM=LLOW+16*(LUP+16*LAM)
       NT1=NMARK(IND)
       NT2=NMARK(IND+1)-1
       DO 350 NT=NT1,NT2
       IF(IPARM.NE.ITPAR(NT))GO TO 350
       DO 320 I=1,IMAX+1
  320  ANS(I)=GARR(NT,I)
  350  CONTINUE
       RETURN

  400  CONTINUE
       DO 210 I=1,IMAX+1
  210  ANS(I)=0.0D0
       RETURN

 1000  FORMAT(1H0,'IN =',I3,2X,'NMARK(IN) =',I3,2X,'N1 =',I3,2X,'L1 ='
     &,I3,2X,'N2 =',I3,2X,'L2 =',I3,2X,'LAM =',I3,2X,'NT =',I3,2X,
     &'ITPAR(NT) =',I7)
 1001  FORMAT(1H0,'IND =',I3)
 1002  FORMAT(1H0,'GARR =',1P,5D12.4)
      END
