      SUBROUTINE DRVSPL(N,F,H,B,GAMMA)
      IMPLICIT REAL*8 (A-H,O-Z)
C-----------------------------------------------------------------------
C
C ADAS305 version. Developed from JETSHP.STARK.FORT (H P Summers).
C
C VERSION  : 1.1
C DATE     : 24-02-2005
C MODIFIED : Martin O'Mullane
C             - First version.
C
C----------------------------------------------------------------------
      DIMENSION F(N),B(N),H(N),GAMMA(N)

      DO 2 I=2,N-1
2     B(I)=(F(I+1)-F(I))/H(I)-(F(I)-F(I-1))/H(I-1)
      B(2)=B(2)-H(1)*B(1)/6.
      B(N-1)=B(N-1)-H(N-1)*B(N)/6.
      GAMMA(1)=(H(1)+H(2))/3.
      B(2)=B(2)/GAMMA(1)
      DO 4 I=3,N-1
      GAMMA(I-1)=(H(I-1)+H(I))/3.-H(I-1)**2/(36.*GAMMA(I-2))
4     B(I)=(B(I)-H(I-1)*B(I-1)/6.)/GAMMA(I-1)
      DO 5 I=N-2,2,-1
5     B(I)=B(I)-B(I+1)*H(I)/(6.*GAMMA(I-1))
      RETURN
      END
