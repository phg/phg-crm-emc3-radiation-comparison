       SUBROUTINE BORNP2(NMAX,LLOW,LAM,LUP,MLLOW,NU,MLUP,ANS,IOPT)
       IMPLICIT REAL*8(A-H,O-Z)
C-----------------------------------------------------------------------
C  PURPOSE: SECOND STAGE OF BORN X-SECT CALCULATION WITH DIRECTIONAL
C  BEAMS CAUSING TRANSITIONS BETWEEN HYDROGEN STARK/ZEEMAN STATES.
C
C  ANGULAR INTEGRALS OF FORM ???????????????????????????? ARE EVALUATED
C  FOR ALL ORBITAL ANGULAR MOMENTA  0<=L<=NMAX-1
C
C  CALLING ROUTINE WITH IOPT=1 PREPARES LOOKUP TABLES. CALL WITH IOPT=2
C  RETURNS VALUES.
C
C  EVALUATE MULTIPOLES UP TO LAMMX=2
C
C  USE POINTER VECTORS FOR RAPID LOOKUP.
C
C  THE MAIN CALLING ROUTINE MUST HAVE THE LINE
C      CALL GAMAF(200)
C  BEFORE CALL TO BORNP2
C
C  ******** H.P. SUMMERS, JET          17 OCT 1988   *************
C  INPUT
C      NMAX=HIGHEST N-SHELL FOR TABLE PREPARATION
C      LLOW=LOWER L FOR SELECTED TRANSITION
C      LAM=MULTIPOLE (0<=LAM<=LAMMX)
C      LUP=UPPER L FOR SELECTED TRANSITION
C      MLLOW=AZIMUTHAL QUANTUM NUMBER FOR LOWER L
C      NU=MULTIPOLE AZIMUTHAL COMPONENT
C      MLUP=AZIMUTHAL QUANTUM NUMBER FOR UPPER L
C      IOPT=1  PREPARE LOOKUP STACKS (ONLY NMAX PARAMETER USED)
C          =2  SUPPLY ANSWERS FOR SPECIFIED LLOW,LAM,LUP,MLLOW,NU,MLUP
C  OUTPUT
C      ANS(I)=RESULT
C-----------------------------------------------------------------------
C
C ADAS305 version. Developed from JETSHP.STARK.FORT (H P Summers).
C
C VERSION  : 1.1
C DATE     : 24-02-2005
C MODIFIED : Martin O'Mullane
C             - First version.
C             - Change dimensions from 200 to 500.
C
C VERSION  : 1.2
C DATE     : 16-05-2007
C MODIFIED : Allan Whiteford
C             - Updated comments as part of subroutine documentation
C               procedure.
C
C VERSION  : 1.3
C DATE     : 09-12-2017
C MODIFIED : Martin O'Mullane
C             - Add save statement to remove need for special
C               compiler options.
C             - Add spacing to show the different RETURN paths. 
C
C----------------------------------------------------------------------
       DIMENSION AS(500),IDS(500),IDL(40),IPT2(40),IPT1(6)
       
       save
       
       IF(IOPT.NE.1)GOTO 300
       LAMMX=2
       NCS=1
       NCL=1
       IPT1(1)=1
C      WRITE(6,1000)
C      WRITE(7,1000)
       DO 200 L1=0,NMAX-1
       IPT1(L1+1)=NCL
       DO 190 L2=0,NMAX-1
       DO 180 LM=0,LAMMX
       IF(LM.LT.IABS(L1-L2).OR.LM.GT.L1+L2.OR.MOD(L1+L2+LM,2).NE.0)
     &GO TO 180
       IDL(NCL)=L1+16*(L2+16*LM)
       IPT2(NCL)=NCS
       XL1=L1
       XL2=L2
       XLM=LM
       W1=WIG3J(XL1,XLM,XL2,0.0D0,0.0D0,0.0D0)
       DO 170 ML1=-L1,L1,1
       DO 160 NUU=-LM,LM,1
       ML2=ML1+NUU
       IF(ML2.LT.-L2.OR.ML2.GT.L2)GO TO 160
       XML1=ML1
       XML2=ML2
       XNUU=NUU
       W2=WIG3J(XL1,XLM,XL2,XML1,XNUU,-XML2)
       AS(NCS)=0.28209D0*DSQRT((2.0D0*XLM+1.0D0)*(2.0D0*XL1+1.0D0)*
     &(2.0D0*XL2+1.0D0))*(-1.0D0)**(-ML1)*W1*W2
       IDS(NCS)=ML1+16*(ML2+16*NUU)
C      WRITE(6,1001)L1,LM,L2,ML1,NUU,ML2,IPT1(L1+1),NCL,IPT2(NCL),
C    &IDL(NCL),NCS,IDS(NCS),AS(NCS)
C      WRITE(7,1001)L1,LM,L2,ML1,NUU,ML2,IPT1(L1+1),NCL,IPT2(NCL),
C    &IDL(NCL),NCS,IDS(NCS),AS(NCS)
       NCS=NCS+1
  160  CONTINUE
  170  CONTINUE
       NCL=NCL+1
  180  CONTINUE
  190  CONTINUE
  200  CONTINUE
       IPT2(NCL)=NCS
       IPT1(NMAX+1)=NCL
       RETURN

  300  IF(LAM.LT.IABS(LLOW-LUP).OR.LAM.GT.(LLOW+LUP).OR.MOD(LLOW+LUP
     &+LAM,2).NE.0)GO TO 400
       IF(MLLOW.LT.-LLOW.OR.MLLOW.GT.LLOW)GO TO 400
       IF(MLUP.LT.-LUP.OR.MLUP.GT.LUP)GO TO 400
       IF(MLLOW+NU.NE.MLUP)GO TO 400
       IDLL=LLOW+16*(LUP+16*LAM)
       ICL1=IPT1(LLOW+1)
       ICL2=IPT1(LLOW+2)-1
C      WRITE(6,1002)IDLL,ICL1,ICL2
C      WRITE(7,1002)IDLL,ICL1,ICL2
       DO 350 ICL=ICL1,ICL2
       IF(IDLL.NE.IDL(ICL))GO TO 350
       ICS1=IPT2(ICL)
       ICS2=IPT2(ICL+1)-1
       IDSS=MLLOW+16*(MLUP+16*NU)
C      WRITE(6,1003)IDSS,ICS1,ICS2
C      WRITE(7,1003)IDSS,ICS1,ICS2
       DO 320 ICS=ICS1,ICS2
       IF(IDSS.NE.IDS(ICS))GO TO 320
       ANS=AS(ICS)
       RETURN

  320  CONTINUE
  350  CONTINUE
  400  ANS=0.0D0
       RETURN

 1000  FORMAT(1H0,' L1  LM  L2  ML1  NUU  ML2       IPT1       NCL
     &IPT2      IDL       NCS      IDS       AS')
 1001  FORMAT(1H ,6I4,6I10,1P,D15.4)
 1002  FORMAT(1H0,'IDLL =',I10,3X,'ICL1 =',I10,3X,'ICL2 =',I10)
 1003  FORMAT(1H0,'IDSS =',I10,3X,'ICS1 =',I10,3X,'ICS2 =',I10)
      END
