      SUBROUTINE SFI2

      IMPLICIT REAL*8 (A-H,O-Z)
C-----------------------------------------------------------------------
C     PURPOSE: This subroutine find the value ln(SIGMA(XNEW))
C-----------------------------------------------------------------------
C
C ADAS305 version. Developed from JETSHP.STARK.FORT (H P Summers).
C
C VERSION  : 1.1
C DATE     : 24-02-2005
C MODIFIED : Martin O'Mullane
C             - First version.
C
C VERSION  : 1.2
C DATE     : 16-05-07
C MODIFIED : Allan Whiteford
C             - Updated comments as part of subroutine documentation
C               procedure.
C----------------------------------------------------------------------

      COMMON/APRX/F(10),B(10),H(10),ABETA(10)
      COMMON/B/FSPL(1),XNEW(1)
      DIMENSION X(10)
      IF(XNEW(1).LT..02)GOTO 22
      IF(XNEW(1).GT.10.24)GOTO 23
      DO 110 I=1,10
110   X(I)=ABETA(I)
      CALL FSPLIN(10,X,F,B,H,1,XNEW,FSPL)
      GOTO 24
22    FSPL(1)=DLOG(3.14**2*DLOG(1./XNEW(1))*0.56)
      GOTO 24
23    FSPL(1)=DLOG((2.*DSQRT(XNEW(1))+1.)/(8.*XNEW(1)**(2./3.)))
24    CONTINUE
      RETURN
      END
