      SUBROUTINE STARK( AMDEUT , AMSS   ,
     &                  BENER  , DV1    , DV2    , DV3   , DENSB ,
     &                  BMAG   , DB1    , DB2    , DB3   ,
     &                  EMAG   , DE1    , DE2    , DE3   ,
     &                  DO1    , DO2    , DO3    ,
     &                  POLO   , POLP   ,
     &                  DENS   , TE     , ZEFF   ,
     &                  NU     , NL     , POPU   ,
     &                  NDCOMP , NCOMP  , WVCOMP , EMCOMP
     &                )


       IMPLICIT NONE
C----------------------------------------------------------------------
C
C  ******************** FORTRAN77 SUBROUTINE: STARK *******************
C
C  PURPOSE: Code for modelling of emission from neutral hydrogen
C           in beams.
C
C  CALLING PROGRAM: ADAS305
C
C  NOTES: Developed from JETSHP.STARK.FORT(EMIS7)
C
C  STEPS: Evaluate Stark/Zeeman shifted hydrogenic energy levels and
C         evaluate dipole matrix elements.
C
C         Calculate directional positive ion impact born cross-sections
C         for Stark/Zeeman states.
C
C         Calculate populations of excited states.
C
C         Calculate polar distribution of emitted radiation for selected
C         lines and its polarisation for the charge exchange spectroscopy
C         multichord viewing lines.
C
C         Initial basis wave functions  -  n l s ml ms
C
C         Stark field is from particle motion across the magnetic induction
C         and a separate pure electric field.
C
C         General geometry specification is by direction cosines
C         dv1,dv2,dv3          : direction cosines of beam particle velocity
C         db1,db2,db3          : direction cosines of magnetic induction.
C         delec1,delec2,delec3 : direction cosines of pure electric filed
C         do1,do2,do3          : direction cosines of observation viewing line
C
C         Specific geometry
C         viewing direction defines the   -i direction
C         i-k plane is that of viewing line and beam direction
C         normal to i-k plane defines the  j direction
C         thetv= angle of beam to i direction (deg)
C         ebeam=beam speed  (kev/amu)
C         b=magnetic induction  (tesla)
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  NU      = UPPER PRINCIPAL QUANTUM NUMBER LINE
C          (I*4)  NL      = LOWER PRINCIPAL QUANTUM NUMBER LINE
C          (I*4)  POPU    = RELATIVE POPULATION OF UPPER (NU) LEVEL
C
C          (R*8)  AMDEUT  = ATOMIC MASS OF HYDROGEN IN BEAM
C          (R*8)  AMSS    = ATOMIC MASS OF HYDROGEN IN PLASMA
C          (R*8)  BENERA  = ENERGY OF ITH BEAM COMPONENT (EV/AMU)
C          (R*8)  DV1     = D.C. FOR X-CPT OF BEAM VELOCITY
C          (R*8)  DV2     = D.C. FOR Y-CPT OF BEAM VELOCITY
C          (R*8)  DV3     = D.C. FOR Z-CPT OF BEAM VELOCITY
C          (R*8)  DENSB   = SPECIFIC NEUTRAL BEAM DENSITY (CM-3)
C          (R*8)  BMAG    = SPECIFIC MAGNETIC FIELD INDUCTION (TESLA)
C          (R*8)  DB1     = D.C. FOR X-CPT OF BMAG
C          (R*8)  DB2     = D.C. FOR Y-CPT OF BMAG
C          (R*8)  DB3     = D.C. FOR Z-CPT OF BMAG
C          (R*8)  EMAG    = SPECIFIC ELECTRIC FIELD STRENGTH (VOLTS)
C          (R*8)  DE1     = D.C. FOR X-CPT OF EMAG
C          (R*8)  DE2     = D.C. FOR Y-CPT OF EMAG
C          (R*8)  DE3     = D.C. FOR Z-CPT OF EMAG
C          (R*8)  DO1     = D.C. FOR X-CPT OF SPECIFIC VIEWING LINE
C          (R*8)  DO2     = D.C. FOR Y-CPT OF SPECIFIC VIEWING LINE
C          (R*8)  DO3     = D.C. FOR Z-CPT OF SPECIFIC VIEWING LINE
C          (R*8)  POLO    = SPECIFIC SIGMA POLARISATION INTENSITY MULTIPLIER
C          (R*8)  POLP    = SPECIFIC  PI   POLARISATION INTENSITY MULTIPLIER
C          (R*8)  DENS    = SPECIFIC PLASMA ELECTRON DENSITY (CM-3)
C          (R*8)  TE      = SPECIFIC PLASMA ELECTRON TEMPERATURE (EV)
C          (R*8)  ZEFF    = SPECIFIC PLASMA EFFECTIVE Z
C
C          (L)    LPASS   = IF TRUE OUTPUT A LOG FILE
C
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          BORNP1     ADAS      Stage 1 Born cross-section calculation
C          BORNP2     ADAS      Stage 2
C          DIPOL      ADAS      H Dipole length radial matrix elements
C          GAMAF      ADAS      Stack vector of factorial function
C          STARK2     ADAS      Calc. Stark perturb. matrix elements
C          UNBUN2     ADAS      Extract indiv. set qu. nos. from integer
C          ZEEMN2     ADAS      Calc. Zeeman perturb. matrix elements
C          C5RLSP     ADAS      Calc. rel.+s.o. energy matrix elements 
C          HYDEMI     ADAS      Collisional mixing of H excited levels
C          ZHPEV      LAPACK    Compute eigenvectors of complex 
C                               Hermitian matrix
C
C
C AUTHOR:  H.P.SUMMERS, JET
C          14 SEPT 1989
C
C----------------------------------------------------------------------
C
C
C ADAS305 version - originally SPSTRK.
C
C VERSION  : 1.1
C DATE     : 24-02-2005
C MODIFIED : Martin O'Mullane
C             - First version. Restrict to a single track.
C
C VERSION  : 1.2
C DATE     : 24-01-2006
C MODIFIED : Hugh Summers
C             - introduced relativistic +spin-orbit fine structure
C             - placed beam velocity vector direction cosines in call
C               parameters and made general
C             - corrected AMSS to AMDEUT for beam atom energy levels
C
C VERSION  : 1.3
C DATE     : 28-09-2006
C MODIFIED : Martin O'Mullane
C             - Add missing blockdata for WF, XF, ABETA and F
C               (prefixed variable with bd_ to avoid name clashes).
C             - INDW3A initialised to zero for all 726 values.
C             - Some more details sent to pass file.
C
C VERSION  : 1.4
C DATE     : 28-09-2006
C MODIFIED : Martin O'Mullane
C             - Setup infrastructure for matching upper level to adf22
C               data but don't make correction yet.
C
C VERSION  : 1.5
C DATE     : 16-10-2009
C MODIFIED : Martin O'Mullane
C             - Correct C4 term in vacuum to air conversion which
C               introduced a ~0.3A shift at H-alpha.
C
C VERSION  : 1.6
C DATE     : 04-06-2017
C MODIFIED : Martin O'Mullane
C             - Add save statement to remove need for special
C               compiler options.
C
C----------------------------------------------------------------------
       integer   ndmx         , mwave    , iunt
C----------------------------------------------------------------------
       parameter (ndmx = 64   , iunt = 7 , mwave = 300)
C----------------------------------------------------------------------
       integer   NU       , NL
       integer   I        , I1           ,
     &           IBMAX1   , IC           ,
     &           ICPT     , ICT          , IFAIL      ,
     &           II       , IK           ,
     &           ILMU     , IMAX         , IMAXB      ,
     &           IMS      , IMS1         , INDW       ,
     &           IOPT     ,
     &           IPSWCH   , IQ           , IQ0        ,
     &           IQ1      , IS           , IV         ,
     &           IVAL     ,
     &           IVSTK    , J            , J1         ,
     &           JJ       , JSW          ,
     &           JV       , JVAL         , K          ,
     &           L        , L1           , LAM        ,
     &           LAMMX    , LAMUP        , LG         ,
     &           LLOW     , LM           , LMAX       ,
     &           LMU      , LUP          ,
     &           ML       , ML1          , MLLOW      ,
     &           MLUP     , MU           , N          ,
     &           N1       , NDIM         ,
     &           NDIML    , NDIMU        ,
     &           NLOW     , NMAX         , NMAXB      ,
     &           NMIN     , NN           ,
     &           NUP      , JNU
       integer   NCPTS    , ndcomp       , ncomp
C----------------------------------------------------------------------
       logical   lpass
C----------------------------------------------------------------------
       real*8    BMAG     , DB1          , DB2        , DB3    ,
     &           EMAG     , DE1          , DE2        , DE3    ,
     &           DO1      , DO2          , DO3        , POLO   ,
     &           POLP     , DENSB        , DENS       , TE     ,
     &           ZEFF
       real*8    C1       , C2           , C3         , C4     , C5
       real*8    ACOEF    , AK1          , AKJ        ,
     &           AKMAX    , AKMIN        , AMDEUT     ,
     &           AMSS     , ANSB         ,
     &           B        , BENER        ,
     &           BX       , BY           , BZ         ,
     &           CII      , CIR          , CJI        ,
     &           CJR      , CTH          ,
     &           DELEC1   ,
     &           DELEC2   , DELEC3       , DENS_C     ,
     &           DLAMF    , DV1          , DV2        ,
     &           DV3      , E2           , EBEAM      ,
     &           EKEV     , EMIS         , ESI        ,
     &           ESR      , EX           , EY         ,
     &           EZ       , EZI          , EZR        ,
     &           POL      ,
     &           RBORN    , RD           , RF         ,
     &           SIG2     , SIGMA        , SS         ,
     &           STH      , SUM2         , SUMXI      ,
     &           SUMXR    , SUMYI        , SUMYR      ,
     &           SUMZI    , SUMZR        , S          ,
     &           TE_C     , U            , V          ,
     &           W3       , XI           , XL         ,
     &           XL1      , XLG          , XML        ,
     &           XML1     , XMS          , XMS1       ,
     &           XN       , XN1          ,
     &           XNN      , XR           , XSECT      ,
     &           Y        , YI           , YR         ,
     &           ZEFF_C   , ZI           , ZR         ,
     &           SUML     , ER           , EI         , 
     &           POPU     , SUMN
       real*8    wig3j , sum_1
C----------------------------------------------------------------------
       integer   NPT(11)        , IPAR(400)    , JC(10)        ,
     &           NVSTK(9)       , NA(66)       , NUINDA(mwave) ,
     &           INDW3A(726)
C----------------------------------------------------------------------
       real*8    CMATR(32,32)   , CMATI(32,32) , R(32)         ,
     &           VR(32,32)      , VI(32,32)    , VUPR(32,32)   ,
     &           VUPI(32,32)    , VLOWR(32,32) , VLOWI(32,32)  ,
     &           CP(10)         , CM(10)       , RLOW(32)      ,
     &           RUP(32)        , CEMIS(mwave) , WAVLN(mwave)  ,
     &           VSTKR(1416)    , VSTKI(1416)  , ESTK(66)      ,
     &           AK(66,66)      , BK(66)       , W3A(726)      ,
     &           RDPA(10)       , RDMA(10)     , BORNR(9,41)   ,
     &           BORNI(9,41)    , ANSA(41)     , XBORN(60,60)  ,
     &           GARR(41)       , ALFA(42)     , QA(41)        ,
     &           CEMIS2(mwave)  , SSA(45)      , wk2(3*ndmx-2)
        real*8   wvcomp(ndcomp) , emcomp(ndcomp)
	real*8   WF(10)         , XF(10)       ,
     &           bd_F(10)       , bd_B(10)     , bd_H(10)      ,
     &           bd_ABETA(10)
C----------------------------------------------------------------------
       complex*16 ap(ndmx*(ndmx+1)/2), zap(ndmx,ndmx), wk1(2*ndmx-1)
C----------------------------------------------------------------------
       COMMON /ADAT/ESTK,AK,XBORN,BK,NA,IMAX,IMAXB
       COMMON /PLDAT/EBEAM,TE_c,DENS_c,ZEFF_c,MU
       COMMON /AP/WF,XF
       COMMON /APRX/bd_F,bd_B,bd_H,bd_ABETA
C-----------------------------------------------------------------------
C  HYDROGENIC LINE STRENGTHS FOR BUNDLED-N LEVELS: N -> N' , N <= 10.
C-----------------------------------------------------------------------
       DATA SSA/3.330D0 ,5.339D-1,1.107D2,1.855D-1,1.527D1 ,9.354D2 ,
     &          8.711D-2,5.105D0 ,1.143D2,4.428D3 ,4.813D-2,2.386D0 ,
     &          3.618D1 ,4.956D2 ,1.511D4,2.948D-2,1.328D0 ,1.648D1 ,
     &          1.494D2 ,1.584D3 ,4.173D4,1.940D-2,8.228D-1,9.069D0 ,
     &          6.614D1 ,4.583D2 ,4.160D3,9.931D4 ,1.346D-2,5.482D-1,
     &          5.595D0 ,3.579D1 ,1.977D2,1.164D3 ,9.514D3 ,2.116D5 ,
     &          9.729D-3,3.851D-1,3.728D0,2.187D1 ,1.052D2 ,4.906D2 ,
     &          2.588D3 ,1.964D4 ,4.141D5/
C-----------------------------------------------------------------------
C  BLOCKDATA for integration and F(BETA)
C-----------------------------------------------------------------------
      DATA WF, XF/3.08441D-1, 4.01119D-1 , 2.18068D-1, 6.20874D-2,
     &            9.50151D-3, 7.53008D-4 , 2.82592D-5, 4.24931D-7,
     &            1.83956D-9, 9.91182D-13,
     &            0.13779   , 0.72945    ,  1.80834  ,  3.40143  ,
     &            5.55249   , 8.33015    , 11.84378  , 16.27925  ,
     &            21.99658  , 29.92069/
      
      DATA bd_ABETA/0.02,  0.04, 0.08, 0.16, 0.32, 0.64, 1.28, 2.56 ,
     &              5.12, 10.24/
      DATA bd_F/3.0727, 2.7663, 2.3702, 1.8326, 1.0784, 9.95E-3,
     &         -1.3168,-2.49,  -3.601, -4.73/
C-----------------------------------------------------------------------
      external wig3j
C-----------------------------------------------------------------------
      save
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
C  Preliminary setup tasks.
C  Setup Gamma function and zero look up arrays for wig3j coefficients.
C  Make this 500 to be consistent with central ADAS routines.
C
C  C1...C5 : Precision air wavelength correction parameters.
C-----------------------------------------------------------------------


       C1 = 6432.8D0
       C2 = 2949810.0D0
       C3 = 25540.0D0
       C4 = 146.0D0
       C5 = 41.0D0

       CALL GAMAF(500)
       DO I = 1, 726
         INDW3A(I)=0
       END DO

       lpass = .FALSE.
       if (lpass) open(unit=iunt, file='stark.pass', status='UNKNOWN')

       NMIN  = 1
       NMAX  = 4
       NMAXB = 10

       if (ndcomp.GT.mwave) then
          write(0,1001)'(ndcomp.gt.mwave) ', ndcomp, ' vs 300'
          write(0,1002)
          stop
       endif
       
C-----------------------------------------------------------------------
C  STAGE O.
C         Move input data into subroutine
C-----------------------------------------------------------------------

       EBEAM  = 1.0D-3*BENER
       V      = DSQRT(EBEAM/24.98082D0)

       B      = BMAG
       EKEV   = 1.0D-3*EMAG
       DELEC1 = DE1
       DELEC2 = DE2
       DELEC3 = DE3
       BX     = B*DB1
       BY     = B*DB2
       BZ     = B*DB3
       EX     = V*B*(DV2*DB3-DV3*DB2)+EKEV*DELEC1/21.8771D0
       EY     = V*B*(DV3*DB1-DV1*DB3)+EKEV*DELEC2/21.8771D0
       EZ     = V*B*(DV1*DB2-DV2*DB1)+EKEV*DELEC3/21.8771D0
       DLAMF  = 7.3430D-3*(DO1*DV1+DO2*DV2+DO3*DV3)*V

       TE_c   =  TE
       DENS_c =  DENS
       ZEFF_c =  ZEFF
C---------------------------------------------------------------------
C  STAGE 1.
C         Evaluate eigenvalues for principal quantum shells NMIN to NMAX
C---------------------------------------------------------------------

       NPT(NMIN)=1
       IVSTK=1
       IC=0
       DO N=NMIN,NMAX
          DO L1=1,N
             L=L1-1
             DO ML=-L,L
                XML=ML
                DO IMS=-1,1,2
                   XMS=0.5D0*IMS
                   IC=IC+1
                   IPAR(IC)=((IMS+1)+32*((ML+L)+16*(L+16*N)))
                end do
             end do
          end do
          NPT(N+1)=IC+1
       end do

C BUNDLE-N EXTENSION

       DO N=NMAX+1,NMAXB
          IC=IC+1
          NPT(N+1)=IC+1
          IPAR(IC)=8192*N
       end do

       DO 100 NN=NMIN,NMAX

       DO 50 I=NPT(NN),NPT(NN+1)-1

       IVAL=IPAR(I)
       CALL UNBUN2(IVAL,N,L,ML,IMS,XN,XL,XML,XMS)
       I1=I-NPT(NN)+1

C TEMPORARY FIX NO RELATIVISTIC
C      CMATR(I1,I1)=-(1.0D0+5.32504D-5*(XN/(XJ+0.5D0)-0.75D0)/(XN*XN))/
C    &(XN*XN)
C  cmatr(i1,i1)=-1.0d0/(xn*xn)-1.0d-10*i1     
       
       CMATR(I1,I1) = 0.0D0
       CMATI(I1,I1) = 0.0D0
       DO 40 J=NPT(NN),NPT(NN+1)-1
       JVAL=IPAR(J)
       CALL UNBUN2(JVAL,N1,L1,ML1,IMS1,XN1,XL1,XML1,XMS1)
       J1=J-NPT(NN)+1
       IF(I1.NE.J1)THEN
          CMATR(I1,J1)=0.0D0
          CMATI(I1,J1)=0.0D0
       ENDIF
       ER =0.0D0
       EI =0.0D0
       EZR=0.0D0
       EZI=0.0D0
       ESR=0.0D0
       ESI=0.0D0
       CALL C5RLSP(XN,XL,XML,XMS,XN1,XL1,XML1,XMS1,ER,EI)
       CALL ZEEMN2(BX,BY,BZ,XN,XL,XML,XMS,XN1,XL1,XML1,XMS1,EZR,EZI)
       CALL STARK2(EX,EY,EZ,XN,XL,XML,XMS,XN1,XL1,XML1,XMS1,ESR,ESI)
       CMATR(I1,J1)=CMATR(I1,J1)+ER+EZR-ESR
       CMATI(I1,J1)=CMATI(I1,J1)+EI+EZI-ESI
   40  CONTINUE

   50  CONTINUE

       NDIM=NPT(NN+1)-NPT(NN)

       do i = 1, ndim
         do j = 1, ndim
           if (j.LE.i.AND.i.LE.ndim) then
              ap(i + (j-1)*(2*ndim-j)/2) = cmplx(cmatr(i,j),cmati(i,j))
           end if
         end do
       end do

       IFAIL=0
       CALL ZHPEV('V','L',NDIM,AP,R,ZAP,ndmx,WK1,WK2,IFAIL)

       if (ifail.NE.0) then
          write (0,1010)ifail
          stop
       else
         do i = 1, ndim
           do j = 1, ndim
             vr(i,j) = dble(zap(i,j))
             vi(i,j) = imag(zap(i,j))
           end do
         end do
       end if

       NVSTK(N)=IVSTK

       if (lpass) write(iunt,1025)N,NDIM,NPT(N),NVSTK(N)
       DO I=1,NDIM
          II=NPT(NN)+I-1
          ESTK(II)=R(I)
          NA(II)=NN
          DO J=1,NDIM
             VSTKR(IVSTK)=VR(I,J)
             VSTKI(IVSTK)=VI(I,J)
             IVSTK=IVSTK+1
          end do
       end do

       if (lpass) write(iunt,1007)
       if (lpass) write(iunt,1005)NN
       DO K=1,NDIM
          DE1=-1.0D0/NN**2-R(K)
          DE2=(109737.3D0-58.5D0/AMDEUT)*DE1
          if (lpass) write(iunt,1006)R(K),DE1,DE2
       end do

  100  CONTINUE


C BUNDLE-N EXTENSION

       DO NN=NMAX+1,NMAXB
          II=NPT(NN)
          XNN=NN
          ESTK(II)=-1.0D0/(XNN*XNN)
          NA(II)=NN
          if (lpass) write(iunt,1048)NN,NPT(NN),NA(II),ESTK(II)
       end do
       IMAX=NPT(NMAX+1)-1
       IMAXB=NPT(NMAXB+1)-1


C-----------------------------------------------------------------------
C  STAGE 2.
C         Evaluate born matrix elements between all basis states and
C         stack data in look-up tables
C-----------------------------------------------------------------------

       LAMMX=2
       IOPT=1
       CALL BORNP1(NMIN,NMAX,NLOW,LLOW,NUP,LUP,LAM,ANSA,QA,IOPT,IBMAX1)
       CALL BORNP2(NMAX,LLOW,LAM,LUP,MLLOW,NU,MLUP,ANSB,IOPT)


C-----------------------------------------------------------------------
C  STAGE 3.
C         Start main loop through all Stark/Zeeman states.
C-----------------------------------------------------------------------

       DO 220 NLOW=NMIN,NMAX
       NDIML=NPT(NLOW+1)-NPT(NLOW)
       if (lpass) write(iunt,1022)
       DO 52 II=1,NDIML
C**  BUNDLE-N EXTENSION
       DO 57 NN=NMAX+1,NMAXB
       IS=((NN-1)*(NN-2))/2+NLOW
       SS=SSA(IS)/NDIML
   57  AK(NPT(NLOW)+II-1,NPT(NN))=SS
       RLOW(II)=ESTK(NPT(NLOW)+II-1)
       DO 51 JJ=1,NDIML
       IVSTK=NVSTK(NLOW)+NDIML*(II-1)+JJ-1
       VLOWR(II,JJ)=VSTKR(IVSTK)
   51  VLOWI(II,JJ)=VSTKI(IVSTK)
   52  CONTINUE
       DO 210 NUP=NLOW,NMAX
       if (lpass) write(iunt,1046)NLOW,NUP
       if (lpass) write(iunt,1015)
       IPSWCH=0
       if (nlow.eq.nl.and.nup.eq.nu) ipswch=1
       NDIMU=NPT(NUP+1)-NPT(NUP)
       DO 54 II=1,NDIMU
       RUP(II)=ESTK(NPT(NUP)+II-1)
       DO 53 JJ=1,NDIMU
       IVSTK=NVSTK(NUP)+NDIMU*(II-1)+JJ-1
       VUPR(II,JJ)=VSTKR(IVSTK)
   53  VUPI(II,JJ)=VSTKI(IVSTK)
   54  CONTINUE


C----------------------------------------------------------------------
C  STAGE 4.
C         Evaluate dipole matrix elements for pair of principal quantum
C         shells. stack data in look-up table.
C----------------------------------------------------------------------

       JSW=-1
       LMAX=NLOW
       CALL DIPOL(JSW,NLOW,NUP,E2,LMAX,CP,CM,JC)
       DO 56 L=1,LMAX+1
       RDPA(L)=DSQRT(CP(L)*10.0D0**JC(L))
   56  RDMA(L)=DSQRT(CM(L)*10.0D0**JC(L))
       ICT=0
       DO 200 II=1,NDIML
       DO 190 JJ=1,NDIMU
       DE1=-RLOW(II)+RUP(JJ)
       DE2=(109737.3D0-58.5D0/AMDEUT)*DE1
       SUMXR=0.0D0
       SUMXI=0.0D0
       SUMYR=0.0D0
       SUMYI=0.0D0
       SUMZR=0.0D0
       SUMZI=0.0D0
C **  BORN PART INITIALISING
       LMU=(LAMMX+1)**2
       DO 65 LM=1,LMU
       DO 60 IK=1,IBMAX1
       BORNR(LM,IK)=0.0D0
   60  BORNI(LM,IK)=0.0D0
   65  CONTINUE
       DO 180 I=1,NDIML
       CIR=VLOWR(I,II)
       CII=VLOWI(I,II)
       IV=NPT(NLOW)+I-1
       IVAL=IPAR(IV)
       CALL UNBUN2(IVAL,N,L,ML,IMS,XN,XL,XML,XMS)
       DO 170 J=1,NDIMU
       CJR=VUPR(J,JJ)
       CJI=VUPI(J,JJ)
       JV=NPT(NUP)+J-1
       JVAL=IPAR(JV)
       CALL UNBUN2(JVAL,N1,L1,ML1,IMS1,XN1,XL1,XML1,XMS1)


C-------------------------------------------------------------------
C  STAGE 5.
C         Calculate dipole radiation terms and accumulate
C-------------------------------------------------------------------

       XR=0.0D0
       XI=0.0D0
       YR=0.0D0
       YI=0.0D0
       ZR=0.0D0
       ZI=0.0D0
       IF(IABS(L-L1).EQ.1.AND.IABS(ML-ML1).LE.1.AND.
     &    IABS(IMS-IMS1).EQ.0)THEN
C  **ACCELERATED LOOK UP SECTION
          IF(L.EQ.L1-1)RD=RDPA(L1)
          IF(L.EQ.L1+1)RD=RDMA(L)
          LG=MAX0(L1,L)
          XLG=LG
          U=XML-XML1
          K=NINT(-XML)
          INDW=6*L1*L1+6*(L1+ML1)+3*(L1-L+1)/2+(ML1-ML+1)+1
          IF(INDW3A(INDW).EQ.1)THEN
             W3=W3A(INDW)
          ELSE
             INDW3A(INDW)=1
             W3=WIG3J(XL1,1.0D0,XL,XML1,U,-XML)
             W3A(INDW)=W3
          ENDIF
          S=RD*(-1.0D0)**(K+LG)*W3*DSQRT(XLG)
          IF(U.GE.0.999D0)THEN
             XR=-0.70711D0*S
             YI=0.70711D0*S
          ELSE IF(U.LE.-0.999D0)THEN
             XR=0.70711D0*S
             YI=0.70711D0*S
          ELSE IF(DABS(U).LE.1.0D-3)THEN
             ZR=S
          ENDIF
       ENDIF
       SUMXR=SUMXR+(CIR*CJR+CJI*CII)*XR+(CII*CJR-CJI*CIR)*XI
       SUMXI=SUMXI+(-CJR*CII+CJI*CIR)*XR+(CJR*CIR+CJI*CII)*XI
       SUMYR=SUMYR+(CIR*CJR+CJI*CII)*YR+(CII*CJR-CJI*CIR)*YI
       SUMYI=SUMYI+(-CJR*CII+CJI*CIR)*YR+(CJR*CIR+CJI*CII)*YI
       SUMZR=SUMZR+(CIR*CJR+CJI*CII)*ZR+(CII*CJR-CJI*CIR)*ZI
       SUMZI=SUMZI+(-CJR*CII+CJI*CIR)*ZR+(CJR*CIR+CJI*CII)*ZI


C----------------------------------------------------------------------
C  STAGE 6.
C         Calculate born x-sect terms and accumulate-bypass if nup=nlow
C----------------------------------------------------------------------

       IF(NUP.LE.NLOW)GO TO 170
       IOPT=2
       IF(IMS.EQ.IMS1) THEN
C           TTR=CIR*CJR+CII*CJI
C           TTI=-CJR*CII+CJI*CIR
          LAMUP=MIN0(LAMMX,L+L1)
          DO 85 LAM=0,LAMUP
          IF(MOD(L+L1+LAM,2).NE.0)GO TO 85
          CALL BORNP1(NMIN,NMAX,N,L,N1,L1,LAM,ANSA,QA,IOPT,IBMAX1)
          DO 80 jnu=-LAM,LAM
          IF(ML+jnu.NE.ML1)GO TO 80
          ILMU=LAM*LAM+1+LAM+jnu
          CALL BORNP2(NMAX,L,LAM,L1,ML,jnu,ML1,ANSB,IOPT)
          DO 75 IK=1,IBMAX1
          BORNR(ILMU,IK)=BORNR(ILMU,IK)+ANSA(IK)*ANSB*(CIR*CJR+CII*CJI)
          BORNI(ILMU,IK)=BORNI(ILMU,IK)+ANSA(IK)*ANSB
     &    *(-CJR*CII+CJI*CIR)
   75     CONTINUE
   80     CONTINUE
   85     CONTINUE
       ENDIF
  170  CONTINUE
  180  CONTINUE



C----------------------------------------------------------------------
C  STAGE 7.
C         Final a-value and radiation polar pattern evaluation completed
C         here for particular initial and final Stark/Zeeman states.
C----------------------------------------------------------------------

       SUM2=SUMXR**2+SUMXI**2+SUMYR**2+SUMYI**2+SUMZR**2+SUMZI**2
       IF(SUM2.GT.1.0D-12)THEN
           POL=DSQRT((SUMYR**2+SUMYI**2)/SUM2)
       ELSE
           POL=0.0D0
       ENDIF
       AK(NPT(NLOW)+II-1,NPT(NUP)+JJ-1)=SUM2
       IF(IPSWCH.NE.1)GO TO 191
       DE3=1.0D8/DE2
       ACOEF=2.67744D9*DE1**3*SUM2
       IF(SUM2.GE.1.0D-2)THEN
          EMIS=3.19596D8*DE1**3*
     &    (0.5D0*(1.0D0-DO1**2)*(SUMXR**2+SUMXI**2)+
     &    0.5D0*(1.0D0-DO2**2)*(SUMYR**2+SUMYI**2)+
     &    0.5D0*(1.0D0-DO3**2)*(SUMZR**2+SUMZI**2)-
     &    DO1*DO2*(SUMXR*SUMYR+SUMXI*SUMYI) -
     &    DO2*DO3*(SUMYR*SUMZR+SUMYI*SUMZI) -
     &    DO3*DO1*(SUMZR*SUMXR+SUMZI*SUMXI))
          ICT=ICT+1
          SIGMA=DE2*1.0D-4
          SIG2=SIGMA*SIGMA
          RF=1.0D0+1.0D-8*(C1+(C2/(C4-SIG2))+(C3/(C5-SIG2)))
          WAVLN(ICT)=DE3*(1.0D0-DLAMF)/RF
          if (lpass) write(iunt,1026)ICT,NU,NL,
     &                 NPT(NUP)+JJ-1,WAVLN(ICT),EMIS
          IF(POL.GT.0.5)THEN
              CEMIS(ICT)=EMIS*POLP
          ELSE
              CEMIS(ICT)=EMIS*POLO
          ENDIF
          NUINDA(ICT)=NPT(NUP)+JJ-1
       ENDIF


C----------------------------------------------------------------------
C  STAGE 8.
C         Final born cross-section evaluation completed here for
C         particular initial and final Stark/Zeeman states.
C----------------------------------------------------------------------

  191  CONTINUE

       RBORN=0.0D0
       IF(NUP.LE.NLOW)GO TO 192
       AK1=3.673710D2*AMDEUT*DSQRT(EBEAM)
       AKJ=1.34961452D5*AMDEUT**2.0D0*EBEAM-AMDEUT*1836.12D0*DE1
       IF(AKJ.LT.0.0D0)THEN
           XSECT=0.0D0
           GO TO 187
       ENDIF
       AKJ=DSQRT(AKJ)
       AKMIN=AK1-AKJ
       AKMAX=AMDEUT*1836.12D0*DE1/AKMIN
       IQ0=1
  181  IF(AKMIN.GT.QA(IQ0).AND.IQ0.LT.IBMAX1)THEN
           IQ0=IQ0+1
           GO TO 181
       ENDIF
       IQ1=IBMAX1
  182  IF(AKMAX.LT.QA(IQ1).AND.IQ1.GT.1)THEN
           IQ1=IQ1-1
           GO TO 182
       ENDIF
       IF(IQ0.GT.IQ1-1)THEN
           XSECT=0.0D0
C           WRITE(0,'(A23)')'TOO CLOSE TO THRESHOLD'
           GO TO 187
       ENDIF

       DO 185 IQ=IQ0,IQ1
       CTH=(QA(IQ)**2+AMDEUT*1836.12D0*DE1)/(2.0D0*QA(IQ)*AK1)
       STH=DSQRT(1.0D0-CTH*CTH)
       GARR(IQ)=0.0D0
       DO 184 LAM=0,LAMMX
       DO 183 JNU=-LAM,LAM
       ILMU=LAM*LAM+1+LAM+JNU
       GO TO (90,91,92,93,94,95,96,97,98),ILMU
   90  Y=2.82095D-1
       GO TO 99
   91  Y=3.45494D-1*STH
       GO TO 99
   92  Y=4.88603D-1*CTH
       GO TO 99
   93  Y=-3.45494D-1*STH
       GO TO 99
   94  Y=3.86274D-1*STH*STH
       GO TO 99
   95  Y=7.72548D-1*CTH*CTH
       GO TO 99
   96  Y=3.15392D-1*(2.0D0*CTH*CTH-STH*STH)
       GO TO 99
   97  Y=-7.72548D-1*CTH*STH
       GO TO 99
   98  Y=3.86274D-1*STH*STH
   99  CONTINUE
       GARR(IQ)=GARR(IQ)+Y*Y*(BORNR(ILMU,IQ)**2+BORNI(ILMU,IQ)**2)/
     &QA(IQ)**3

  183  CONTINUE
  184  CONTINUE

       IF(IQ.NE.IQ0)THEN
           IF(GARR(IQ-1).LE.0.0D0)THEN
               ALFA(IQ)=0.0D0
           ELSE
               ALFA(IQ)=DLOG(GARR(IQ)/GARR(IQ-1))/DLOG(QA(IQ)/QA(IQ-1))
           ENDIF
       ENDIF
  185  CONTINUE
       ALFA(IQ0)=ALFA(IQ0+1)
       ALFA(IQ1+1)=ALFA(IQ1)
       XSECT=0.0D0
       XSECT=XSECT+GARR(IQ0)*(QA(IQ0)-AKMIN*(AKMIN/QA(IQ0))**ALFA(IQ0))/
     &(ALFA(IQ0)+1)+GARR(IQ1)*(AKMAX*(AKMAX/QA(IQ1))**ALFA(IQ1+1)-
     &QA(IQ1))/(ALFA(IQ1+1)+1)

       IF(IQ0.NE.IQ1)THEN
           DO 186 IQ=IQ0,IQ1-1
           XSECT=XSECT+GARR(IQ)*(QA(IQ+1)*(QA(IQ+1)/QA(IQ))**ALFA(IQ+1)-
     &     QA(IQ))/(ALFA(IQ+1)+1)
  186      CONTINUE
       ENDIF

  187  RBORN=4.86449D-6*DSQRT(EBEAM)*XSECT*
     &       (AMDEUT*1836.12D0/AK1)**2
       IF(IPSWCH.NE.1)GO TO 192
       IF(SUM2.GE.1.0D-2.AND.RBORN.GE.1.0D-11) THEN
          if (lpass) write(iunt,1016)II,JJ,DE1,DE2,DE3,SUMXR,SUMXI,
     &    SUMYR,SUMYI,SUMZR,SUMZI,ACOEF,RBORN,POL
       ELSEIF(SUM2.GE.1.0D-2) THEN
          if (lpass) write(iunt,1016)II,JJ,DE1,DE2,DE3,SUMXR,SUMXI,
     &    SUMYR,SUMYI,SUMZR,SUMZI,ACOEF,RBORN,POL
       ELSEIF(RBORN.GE.1.0D-11) THEN
          if (lpass) write(iunt,1044)II,JJ,DE1,DE2,DE3,RBORN
       ENDIF
  192  XBORN(NPT(NLOW)+II-1,NPT(NUP)+JJ-1)=RBORN
       XBORN(NPT(NUP)+JJ-1,NPT(NLOW)+II-1)=DSQRT(2.940D3*AMDEUT*EBEAM/
     &      (2.940D3*AMDEUT*EBEAM-DE1))*RBORN
  190  CONTINUE
  200  CONTINUE
       IF(IPSWCH.EQ.1)NCPTS=ICT
  210  CONTINUE
  220  CONTINUE

C  BUNDLE-N EXTENSION
       DO 230 NLOW=NMAX+1,NMAXB-1
       AK(NPT(NLOW),NPT(NLOW))=0.0D0
       DO 225 NUP=NLOW+1,NMAXB
       IS=((NUP-1)*(NUP-2))/2+NLOW
  225  AK(NPT(NLOW),NPT(NUP))=SSA(IS)
  230  CONTINUE
  
       if (lpass) then
          write(iunt,1047)(AK(1,K),K=1,66)
          write(iunt,1047)(AK(2,K),K=1,66)
          write(iunt,1047)(AK(3,K),K=1,66)
          write(iunt,1047)(AK(10,K),K=1,66)
          write(iunt,1047)(AK(11,K),K=1,66)
          write(iunt,1047)(AK(28,K),K=1,66)
          write(iunt,1047)(AK(29,K),K=1,66)
          write(iunt,1047)(AK(60,K),K=1,66)
          write(iunt,1047)(AK(65,K),K=1,66)
       endif

C----------------------------------------------------------------------
C  STAGE 9.
C         Calculation of excited state populations is now performed by
C         call to HYDEMI. Radiation emitted is then altered to include
C         population dependence.
C----------------------------------------------------------------------


C  Note : Set BK(i)=0 to remove the effect of populations on intensities.

       if (lpass) write(iunt,1051)TE,DENS,ZEFF,DENSB
       CALL HYDEMI(lpass, iunt)

 
C Ratio of NU population to POPU input value

       sumn = 0.0D0
       do i = npt(nu),npt(nu+1)-1
          sumn = sumn + bk(i)
       end do
                   
       SUML=0.0D0
       sum_1 = 0.0D0
       DO ICT=1,NCPTS
         if (lpass) write(iunt,1026)ICT,NU,NL,
     &     NUINDA(ICT),WAVLN(ICT),CEMIS(ICT),BK(NUINDA(ICT))
         CEMIS2(ICT)=CEMIS(ICT)*BK(NUINDA(ICT))*DENSB
         SUML=SUML+CEMIS2(ICT)
         sum_1 = sum_1 + CEMIS(ICT)
       end do
       if (lpass) write(iunt,1052)SUML
      
       if (lpass) then 
          do ict=1,ncpts
            write(iunt,1027) nuinda(ict), wavln(ict), bk(nuinda(ict))
          end do
       endif

C----------------------------------------------------------------------
C  STAGE 10.
C         Output the component wavelengths and emissivities.
C         Correct for correct NU population.
C         Wait until we know how to calculate the upper level population
C         from this code.
C----------------------------------------------------------------------

       ncomp = ncpts
       do icpt=1,ncomp
          wvcomp(icpt) = wavln(icpt)
C           emcomp(icpt) = cemis2(icpt) * popu / sumn
          emcomp(icpt) = cemis2(icpt)
      end do

       if (lpass) then
          do icpt=1,ncomp
             write(iunt,1028) icpt, wavln(icpt), cemis2(icpt)
          end do
       endif

       if (lpass) close(iunt)
       
C----------------------------------------------------------------------

 1001 format(1x,31('*'),' stark error ',30('*')//
     &       1x,'Fault in input parameters: ',a,i5,a)
 1002 format(/1x,29('*'),' program terminated ',29('*'))
 1005  FORMAT(1H ,'EIGENVALUES FOR N = ',I5/1H ,'  ENER/IH     DENER/IH
     &     DENER/CM-1')
 1006  FORMAT(1H ,1P,2D12.4,0P,F17.5)
 1007  FORMAT(1H )
 1010  format('Error in ZHPEV. IFAIL = ', i3)
 1015  FORMAT(1H ,'  II  JJ     DE1        DE2        DE3      SUMXR
     &  SUMXI      SUMYR      SUMYI      SUMZR      SUMZI      ACOEF
     &  RBORN  POL')
 1016  FORMAT(1H ,2I4,1PD11.3,0P,2F10.1,1P,8D11.3,0PF4.1)
 1022  FORMAT(1H )
 1025  FORMAT(1H ,'N = ',I3,3X,'NDIM =',I3,3X,'NPT(N) =',I3,3X,'NVSTK(N)
     & =',I3)
 1026  FORMAT(1H ,4I5,1P,3D12.4)
 1027  FORMAT(1H ,'I = ',I5,3X,'WAVE = ',D14.6,3X,'BK = ',D14.6)
 1028  FORMAT(1H ,'J = ',I5,3X,'WAVE = ',D14.6,3X,'EMIS = ',D14.6)
 1044  FORMAT(1H ,2I4,1PD11.3,0P,2F10.1,77X,1PD11.3)
 1046  FORMAT(1H ,'NLOW =',I3,5X,'NUP =',I3)
 1047  FORMAT(1H ,1P,8D12.4)
 1048  FORMAT(1H ,3I5,F10.5)
 1051  FORMAT(1H ,'TEV =',1PD12.4,3X,'DENS =',1PD12.4,3X,'ZEFF =',
     &        1PD12.4,3X,'DENSB =',1PD12.4)
 1052  FORMAT(1H ,31X,'SUML: ', 1PD12.4)

C----------------------------------------------------------------------

      END
