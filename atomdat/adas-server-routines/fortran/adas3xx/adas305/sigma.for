      SUBROUTINE SIGMA

      IMPLICIT REAL*8 (A-H,O-Z)
C-----------------------------------------------------------------------
C
C     PURPOSE: Calculates <SIGMA*V> or SIGMA*V rates for collisional
C              excitation by proton or electron impact
C
C-----------------------------------------------------------------------
C
C ADAS305 version. Developed from JETSHP.STARK.FORT (H P Summers).
C
C VERSION  : 1.1
C DATE     : 24-02-2005
C MODIFIED : Martin O'Mullane
C             - First version.
C
C VERSION  : 1.2
C DATE     : 24-10-2005
C MODIFIED : Martin O'Mullane
C             - Change MK(66) to BK(66) to maintain maning and
C               size of ADAT common block.
C
C VERSION  : 1.3
C DATE     : 16-05-2007
C MODIFIED : Allan Whiteford
C             - Updated comments as part of subroutine documentation
C               procedure.
C
C----------------------------------------------------------------------
      INTEGER IMAX,IMAXB,NA(66)
      DIMENSION SIGEX(9),A1(66,66),A(65,65),B(65),WR(66),SIG(66)

      COMMON/ADAT/ESTK(66),AK(66,66),XBORN(60,60),BK(66),NA,IMAX,IMAXB
      COMMON/PLDAT/EBEAM,T,DENS,ZEFF,MU
      COMMON/F/ K1,K2,N1,N2
      COMMON/S/CLAM,SIGEL,SIGIN
      COMMON/AP/WF(10),XF(10)
      COMMON/D/DELTAE,WR
      COMMON/B/FSPL(1),XNEW(1)
      COMMON/Selex/SEL(9)

C     The marker KK
      KK=1
      N1=NA(K1)
      N2=NA(K2)
      CLAM=AK(K1,K2)/3
C     WRITE(6,*) 'K1=',K1,'K2=',K2,'N1=',N1,'N2=',N2,'CLAM=',CLAM

C  If CLAM=O it is not necessary to calculate cross sections
      IF(CLAM.EQ.0.0D0)THEN
      SIGEL=0.0D0
      SIGIN=0.0D0
      GOTO 4000
      END IF

C  Separation the cases when (|N1-N2|>0)
      IF(N1.NE.N2) GOTO 2002
C  For transition when N1-N2=0, electron contribution is not important
      SIGEL=0.0D0
      ASUM=(WR(K1)+WR(K2))*6.588D-16
C  If the difference of the level energies is less then radiation widths
C
      IF(DELTAE.LT.ASUM) GOTO 2003
C  I am still keeeping the part of programm for calculation of
C  Maxwellian integral (See Loop 111)
      IF(KK.NE.1) GOTO 16

      XNEW(1)=0.919*DELTAE*DSQRT(CLAM)/EBEAM
      CALL SFI2
      SIGIN=7.8D-08*CLAM*DEXP(FSPL(1))*DEXP(-2.*DSQRT(XNEW(1)))/
     *DSQRT(EBEAM)*ZEFF
      GOTO 4000

16    FINT=0.0D0
      DO 111 I=1,10
      XNEW(1)=MU*DELTAE*DSQRT(CLAM)/(2.*T*(XF(I)+DELTAE/T))
      CALL SFI2
      FINT=FINT+WF(I)*EXP(-(2.*DSQRT(XNEW(1))+DELTAE/T))*DEXP(FSPL(1))
  111 CONTINUE
      SIGIN=2.*DSQRT(2.*3.14*MU*27.2/T)*CLAM*FINT*6.12D-9*ZEFF*0.406
      GOTO 4000

C     STLAM2=6.*N**2*(N**2-L**2-L-1.)/Z**2
C Here it is nesessary to introduce SIGMA*V
C but now we calculate only <SIGMA*V> for transition with DELTAE=0
2003  T1=EBEAM
      IF(CLAM.LE.0.0D0)THEN
      SIGIN=0.0D0
      GOTO 4000
      END IF
      SIGIN=9.22D-8*CLAM*DSQRT(MU/T1)*(21.35+DLOG10(T1**2/(MU*CLAM
     &*DENS*ZEFF)))*ZEFF
      GOTO 4000

C Here the rate coff. for |N1-N2|>0 transitions are calculated
C
 2002 CONTINUE
      CALL SIGMEL
      CALL SIGMIN

C     IF(N.EQ.1.AND.M2.LE.9)THEN
C     I54=M2
C     SEL(M2)=SIGEL
C     END IF
C     IF(M1.EQ.1)THEN
C     IF(M2.LE.9)THEN
C     A20=SIGEL
C     A21=SIGIN
C     WRITE(14,*)'SEXEL=',A20,'SEXIN=',A21
C     WRITE(14,*)'PROBAB=',A(M1,M2),'STRF=',STRF
C     WRITE(14,*)'M1=',M1,'M2=',M2,'T=',T
C     WRITE(14,*)'N=',N,'L=',L,'CJ=',CJ
C     WRITE(14,*)'NK=',NK,'LF=',LF,'CJF=',CJF
C     END IF
C     END IF

 4000 CONTINUE
      RETURN
      END
