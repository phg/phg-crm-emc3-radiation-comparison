       subroutine c5dplr( ndpix  , npix   , wvmin  , wvmax  ,
     &                    ndcomp , ncomp  , wvcomp , emcomp ,
     &                    tev    , amss   ,
     &                    total
     &                  )

       implicit none
C-----------------------------------------------------------------------
C
C  ****************** fortran77 subroutine: c5pixv *********************
C
C  Purpose:  Distribute Doppler broadened line emission into pixel range
C
C  Calling program: adas305, stark
C
C
C  Subroutine:
C
C  input : (i*4)  ndpix   = maximum number of pixels
C  input : (i*4)  npix    = number of pixels assigned to wavelength interval
C  input : (r*8)  wvmin   = lower limit of wavelength interval (ang)
C  input : (r*8)  wvmax   = upper limit of wavelength interval (ang)
C
C  input : (i*4)  ndcomp  = maximum number of components in feature
C  input : (i*4)  ncomp   = number of components in feature
C  input : (r*8)  wvcomp  = wavelenghts of components (ang)
C  input : (r*8)  emcomp  = emissivity of component
C
C  input : (r*8)  tev     = electron temperature (eV)
C  input : (r*8)  amss    = atomic mass number
C
C  output: (r*8)  total() = Doppler broadened emission in wavelength interval
C
C Routines:
C          Routine    Source    Brief Description
C          -------------------------------------------------------------
C
C
C Author:  Martin O'Mullane
C Date:    18-02-2005
C
C
C VERSION  : 1.1
C DATE     : 18-02-2005
C MODIFIED : Martin O'Mullane
C              - First version.
C
C VERSION  : 1.2
C DATE     : 24-05-2014
C MODIFIED : Martin O'Mullane
C              - Restrict filling the TOTAL array to npix rather 
C                than ndpix.
C
C-----------------------------------------------------------------------
      integer   ndpix       , npix      , ndcomp   , ncomp
      integer   j           , ipix      , ind1     , ind2
C-----------------------------------------------------------------------
      real*8    wvmin       , wvmax     , tev      , amss      
      real*8    cpixmx
C-----------------------------------------------------------------------
      real*8    wvcomp(ndcomp)          , emcomp(ndcomp)   , 
     &          emiss(ndpix)            , total(ndpix)
C-----------------------------------------------------------------------
      
      
      do j = 1, ndpix
        total(j) = 0.0D0
      end do
      cpixmx = 0.0D0

      do j = 1, ncomp
      
        call c5pixv( ndpix  , npix   , wvmin     , wvmax      , 
     &               cpixmx ,
     &               tev    , amss   , wvcomp(j) , emcomp(j)  ,
     &               emiss  , ind1   , ind2   
     &              )

        if (ind1.GT.0.AND.ind2.LE.npix) then
           do ipix = ind1, ind2
             total(ipix) = total(ipix) + emiss(ipix)
             cpixmx      = max(cpixmx, emiss(ipix))
           end do
        endif
        
      end do



      end
