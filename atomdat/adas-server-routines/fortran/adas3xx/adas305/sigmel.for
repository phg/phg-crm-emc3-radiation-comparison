      SUBROUTINE SIGMEL

      IMPLICIT REAL*8 (A-H,O-Z)
C-----------------------------------------------------------------------
C
C  PURPOSE: Calculates rate coefficients for excitation by
C           electron impact
C
C-----------------------------------------------------------------------
C
C ADAS305 version. Developed from JETSHP.STARK.FORT (H P Summers).
C
C VERSION  : 1.1
C DATE     : 24-02-2005
C MODIFIED : Martin O'Mullane
C             - First version.
C
C VERSION  : 1.2
C DATE     : 16-05-2007
C MODIFIED : Allan Whiteford
C             - Updated comments as part of subroutine documentation
C               procedure.
C
C----------------------------------------------------------------------

      INTEGER IMAX,IMAXB,NA(66)
      DIMENSION SIGEX(9),A1(66,66),A(65,65),B(65),WR(66),SIG(66)

      COMMON/PLDAT/EBEAM,T,DENS,ZEFF,MU
      COMMON/F/ K1,K2,N,NK
      COMMON/S/CLAM,SIGEL,SIGIN
      COMMON/D/DELTAE,WR
      COMMON/Selex/SEL(9)

C     WRITE(6,*)'SIGMEL'
C     WRITE(6,*) 'K1=',K1,'K2=',K2,'N1=',N,'N2=',NK,'CLAM=',CLAM
      Z=1
      STRF= CLAM*DELTAE*7.353D-02
      IF(STRF.LE.0.0D0)THEN
      SIGEL=0.0D0
      GOTO 3002
      END IF
      XTR=DELTAE/T
      IF(XTR.LE.1.)GOTO 3000
      EX1=(XTR**2+2.335*XTR+0.25)*DEXP(-XTR)/
     &(XTR*(XTR**2+3.33*XTR+1.68))
      GOTO 3001
 3000 EX1=-0.577+XTR-0.25*XTR**2+0.055*XTR**3-
     &0.01*XTR**4+0.001*XTR**5-DLOG(XTR)
 3001 IF(N.GT.NK)NG2=N
      IF(N.LT.NK)NG2=NK
      IF(N.GT.NK)NG1=NK
      IF(N.LT.NK)NG1=N
      GAUNT=0.19*(1.+0.9*(1.+0.05*NG2*(NG2-NG1)*
     &(1.+(1.-2./Z)*XTR))*DEXP(XTR)*EX1)
      SIGEL=1.6D-5*GAUNT*STRF*DEXP(-XTR)/(DELTAE*DSQRT(T))
 3002 CONTINUE
      RETURN
      END
