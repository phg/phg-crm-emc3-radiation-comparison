      SUBROUTINE DRV
      IMPLICIT REAL*8 (A-H,O-Z)
C-----------------------------------------------------------------------
C
C     PURPOSE: Calculates coefficients for spline approximation
C              of tabulated function in 10 points together with
C              subroutine DRVSPL.
C
C-----------------------------------------------------------------------
C
C ADAS305 version. Developed from JETSHP.STARK.FORT (H P Summers).
C
C VERSION  : 1.1
C DATE     : 24-02-2005
C MODIFIED : Martin O'Mullane
C             - First version.
C
C VERSION  : 1.2
C DATE     : 16-05-2007
C MODIFIED : Allan Whiteford
C             - Updated comments as part of subroutine documentation
C               procedure.
C
C----------------------------------------------------------------------
      COMMON/APRX/F(10),B(10),H(10),ABETA(10)
      DIMENSION GAMMA(10)
      DO 113 I=1,9
113   H(I)=ABETA(I+1)-ABETA(I)
      B(1)=0.0
      B(10)=0.0
      CALL DRVSPL(10,F,H,B,GAMMA)
      RETURN
      END
