      SUBROUTINE FSPLIN(N,X,F,B,H,M,XNEW,FSPL)
      IMPLICIT REAL*8 (A-H,O-Z)
C-----------------------------------------------------------------------
C     PURPOSE: Calculates M values of function FSPL(XNEW)
C              using spline approximation
C-----------------------------------------------------------------------
C
C ADAS305 version. Developed from JETSHP.STARK.FORT (H P Summers).
C
C VERSION  : 1.1
C DATE     : 24-02-2005
C MODIFIED : Martin O'Mullane
C             - First version.
C
C VERSION  : 1.2
C DATE     : 16-05-2007
C MODIFIED : Allan Whiteford
C             - Updated comments as part of subroutine documentation
C               procedure.
C
C----------------------------------------------------------------------
      DIMENSION X(N),F(N),B(N),H(N),XNEW(M),FSPL(M)
      NJ=N-1
      DO 1 I=1,M
      K=0
      DO 2 J=1,NJ
      IF(XNEW(I).GE.X(J).AND.XNEW(I).LE.X(J+1))GOTO 5
      GOTO 2
 5    K=J+1
      GOTO 3
 2    CONTINUE
 3    CONTINUE
      IF(K.NE.0)GOTO 4
      GOTO 1
 4    FSPL(I)=((B(K-1)*(X(K)-XNEW(I))**3+B(K)*(XNEW(I)-
     &X(K-1))**3)/6.+(F(K-1)-B(K-1)*H(K-1)**2/6.)*(X(K)-
     &XNEW(I))+
     &(F(K)-B(K)*H(K-1)**2/6.)*(XNEW(I)-X(K-1)))/H(K-1)
 1    CONTINUE
      RETURN
      END
