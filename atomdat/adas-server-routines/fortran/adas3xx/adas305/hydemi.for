      SUBROUTINE HYDEMI(lpass, iunt)

      IMPLICIT REAL*8 (A-H,O-Z)
C----------------------------------------------------------------------
C     PURPOSE: Calculates the collisional mixing of excited levels of
C              hydrogen atoms in the beam.
C
C     IMAX - number of Stark resolved states
C     IMAXB - total number of included states
C     NA(I)- an array with principle quantum numbers of the states
C     ESTK(I)- the energies of the states in rydbergs
C     A(I,J) -|<r ij>|**2  - dipol matrix elements in at.units
C     BK(I) - the results of calculations
C     EBEAM - the energy of the beam atoms (keV/nucleon)
C     T - electron temperature of plasma in eV
C     DENS - electron density ( cm -3 )
C     ZEFF - effective charge of the plasma
C     MU - reduced mass(in mass of electron)
C-----------------------------------------------------------------------
C
C ADAS305 version. Developed from JETSHP.STARK.FORT (H P Summers).
C
C VERSION  : 1.1
C DATE     : 24-02-2005
C MODIFIED : Martin O'Mullane
C             - First version.
C
C VERSION  : 1.2
C DATE     : 16-03-2005
C MODIFIED : Martin O'Mullane
C             - Declare lpass as logical.
C
C VERSION  : 1.3
C DATE     : 16-05-2007
C MODIFIED : Allan Whiteford
C             - Updated comments as part of subroutine documentation
C               procedure.
C
C----------------------------------------------------------------------
C----- alterations to dimensions and common to allow bundle-n extension
C----- hps 6 dec 1988
C----------------------------------------------------------------------
       logical   lpass
C----------------------------------------------------------------------
      INTEGER IMAX,IMAXB,NA(66)
      DIMENSION SIGEX(9),A1(66,66),A(65,65),B(65),WR(66),SIG(66)
      DIMENSION ESTK(66),AK(66,66),BK(66),XBORN(60,60)

      COMMON/ADAT/ESTK,AK,XBORN,BK,NA,IMAX,IMAXB
      COMMON/PLDAT/EBEAM,T,DENS,ZEFF,MU
      COMMON/F/ K1,K2,N1,N2
      COMMON/S/CLAM,SIGEL,SIGIN
      COMMON/AP/WF(10),XF(10)
      COMMON/D/DELTAE,WR
      COMMON/B/FSPL(1),XNEW(1)
      COMMON/Selex/SEL(9)
      COMMON /CX/ A,B
C
C     CONTROL OF INPUT DATA
      MU=1837
      if (lpass) WRITE(iunt,*) 'ESTK(I)'
      if (lpass) WRITE(iunt,1000) (ESTK(I),I=1,12)
1000  FORMAT(/,8E10.3)

      if (lpass) WRITE(iunt,1006)' EBEAM=',EBEAM, ' TE=',T,' DENS=',DENS
1006  FORMAT(A6,E10.3,A4,E10.3,A6,E10.3)
      if (lpass) WRITE(iunt,*)'IMAX=',IMAX,'IMAXB=',IMAXB,
     &                        'ZEFF=',ZEFF,'MU=',MU
      if (lpass) WRITE(iunt,*)'NA='
      if (lpass) WRITE(iunt,1001) (NA(I),I=1,12)
1001  FORMAT(/,6I4)
C     Because Sigma(E) is tabulated in BD2 we need to calculate
C     the cooefficients for spline approximation
      CALL DRV

C     If we consider n,L,ML,MS case
C-----alteration for bundle-n extension  hps 6 dec 1988
      MK=IMAXB
C
      DO 300 I=1,MK
      DO 301 J=1,MK
 301  A1(I,J)=0.0D0
      WR(I)=0.0D0
      SIG(I)=0.0D0
C------- insertion by hps  4 nov 1988
      BK(I)=0.0D0
      IF(I.LE.MK-1)B(I)=0.0D0
 300  CONTINUE
      DO 500 I=1,MK
      DO 500 J=I,MK
      IF(I.EQ.J) AK(I,J)=0.0D0
 500  AK(J,I)=AK(I,J)

C
C     Calculation of total radiative probability of level decay WR(I)
C     and rate coefficient for collision induced transition into
C     all other states SIG(I)
C
      DO 302 I=1,MK
      K1=I
      DO 302 J=1,MK
      DELTAE=DABS(ESTK(I)-ESTK(J))*13.6
      K2=J
      IF(I.EQ.J) GOTO 302
      CALL SIGMA
C--------------  insertion by hps 4 nov 1988
C------alteration for bundle-n extension  hps 6 dec 1988
C     IF(NA(I).NE.NA(J).AND.I.LE.IMAX.AND.J.LE.IMAX)SIGIN=XBORN(I,J)
      IF(I.EQ.1.AND.J.LE.12)THEN
      if (lpass) WRITE(iunt,*) 'SIGEL=',SIGEL,' SIGIN=',SIGIN,' 
     &                          XBORN=',XBORN(1,J),' J=',J
      END IF
      SIG(I)=SIG(I)+SIGEL+SIGIN*ZEFF
      IF(ESTK(I).GT.ESTK(J)) THEN
      WR(I)=WR(I)+AK(I,J)*DELTAE**3*1.066D6
      END IF
 302  CONTINUE
      if (lpass) WRITE(iunt,*) 'WR'
      if (lpass) WRITE(iunt,2000) (WR(I),I=1,30)
2000  FORMAT(/,6D10.3)
      if (lpass) WRITE(iunt,*) 'SIG'
      if (lpass) WRITE(iunt,2000) (SIG(I),I=1,30)
C*  Here ionization rates are added
C
      DO 820 I=1,MK
      K1=I
      N=NA(I)
C----------- next line altered by hps 4 nov 1988
      POTI=13.6/N**2
C     POTI=13.6/N**2-13.6/(NA(IMAXB)+1)**2
      XTRI=POTI/T
      IF(XTRI.LE.1.)GOTO 801
      EXI1=(XTRI**2+2.335*XTRI+0.25)*DEXP(-XTRI)/
     &(XTRI*(XTRI**2+3.33*XTRI+1.68))
      GOTO 802
801   EXI1=-0.577+XTRI - 0.25*XTRI**2+0.055*XTRI**3
     &-0.01*XTRI**4+0.001*XTRI**5-DLOG(XTRI)
802   SIEL=2.68E-6*DSQRT(XTRI)*EXI1/(POTI*DSQRT(POTI))
      SIIN=2.D-9*DSQRT(MU/EBEAM)*DLOG(991.*EBEAM/(MU*POTI))/POTI
      IF(I.LE.12)THEN
      if (lpass) WRITE(iunt,*)'I=',I,' SIEL=',SIEL,' SIIN=',SIIN
      END IF
      SIG(I)=SIG(I)+SIEL+SIIN*ZEFF

C Calculation of coupling coefficients between states I and J
      DO 821 J=1,MK
      K2=J
      DELTAE=DABS(ESTK(I)-ESTK(J))*13.6
      IF(I.EQ.J)THEN
      A1(I,I)= -(SIG(I)*DENS+WR(I))
      ELSE

      IF(ESTK(J).GT.ESTK(I))THEN
      V= AK(I,J)*DELTAE**3*1.066D6
      ELSE
      V=0.0D0
      END IF

      CALL SIGMA
C------------ insertion by hps  4 nov 1988
C------alteration for bundle-n extension   hps 6 dec 1988
C     IF(NA(I).NE.NA(J).AND.I.LE.IMAX.AND.J.LE.IMAX)SIGIN=XBORN(I,J)
      A1(I,J)= V+(SIGEL+SIGIN*ZEFF)*DENS
C     IF(I.EQ.3.AND.J.EQ.5)THEN
C     WRITE(6,*) 'I=',I,'J=',J,'AK(I,J)=',AK(I,J)
C     WRITE(6,1005)' V=',V,' SIGEL=',SIGEL,'SIGIN=',SIGIN,'A1=',A1(I,J)
C1005  FORMAT(A3,E10.3,A6,E10.3,A5,E10.3,A3,E10.3)
C      END IF
C     IF(I.EQ.5.AND.J.EQ.3)THEN
C     WRITE(6,*) 'I=',I,'J=',J
C     WRITE(6,1005)'V=',V,'SIGEL=',SIGEL,'SIGIN=',SIGIN,'A1=',A1(I,J)
C     END IF
      END IF
821   CONTINUE


C Initial population is produced by excitation from ground state
      IF(I.GT.2) B(I-2)=-A1(I,1)-A1(I,2)
820   CONTINUE
C Here is it possible to introduce the experimental intial population
C      DO 9555 I=2,9
C      A1(I,1)=(SIGEX(I)*2.18E08*SQRT(EBEAM/25)*ZEFF+SEL(I))*ND
C9555  B(I-1)=A1(I,1)+A1(I,2)

C We exclude the equation for the ground state

      DO 1111 I=1,MK-2
      DO 1111 J=1,MK-2
1111  A(I,J)=A1(I+2,J+2)
C      DO 2222 J=1,MK
C      S=0.0D0

C     DO 2221 I=1,MK
C      IF(I.EQ.J)GOTO 2221
C      S=S+A1(I,J)
C2221  CONTINUE
C      WRITE(6,*)'A1(J,J)=',A1(J,J),'SUM=',S
C2222  CONTINUE

C Solution of system of linear equations

      CALL LUMSIS(MK-1)

C  Recalculation of effective rate coefficients

      BK(1)=1.0D0
      BK(2)=1.0D0
C
      DO 902 I=1,MK-2

 902  BK(2+I)=B(I)

      RETURN
      END
