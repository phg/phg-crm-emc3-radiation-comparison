       SUBROUTINE ZEEMN2(BX,BY,BZ,XN,XL,XML,XMS,XN1,XL1,XML1,XMS1,EZR,
     &                   EZI)
     
       IMPLICIT REAL*8(A-H,O-Z)
C-----------------------------------------------------------------------
C
C ADAS305 version. Developed from JETSHP.STARK.FORT (H P Summers).
C
C VERSION  : 1.1
C DATE     : 24-02-2005
C MODIFIED : Martin O'Mullane
C             - First version.
C
C----------------------------------------------------------------------
       EZR=0.0D0
       EZI=0.0D0
       CONST=4.25454D-6
       VLP=0.0D0
       VL0=0.0D0
       VLM=0.0D0
       VSP=0.0D0
       VS0=0.0D0
       VSM=0.0D0
       IF(DABS(XL-XL1).LE.1.0D-3.AND.DABS(XMS-XMS1).LE.1.0D-3)THEN
         IF(DABS(XML-XML1+1.0D0).LE.1.0D-3)VLM=0.5D0*DSQRT((XL1+XML1)*
     &   (XL1-XML1+1.0D0))
         IF(DABS(XML-XML1-1.0D0).LE.1.0D-3)VLP=0.5D0*DSQRT((XL1-XML1)*
     &   (XL1+XML1+1.0D0))
         IF(DABS(XML-XML1).LE.1.0D-3)VL0=XML1
       ENDIF
       IF(DABS(XL-XL1).LE.1.0D-3.AND.DABS(XML-XML1).LE.1.0D-3)THEN
         IF(DABS(XMS-XMS1+1.0D0).LE.1.0D-3)VSM=DSQRT((0.5+XMS1)*
     &   (0.5-XMS1+1.0D0))
         IF(DABS(XMS-XMS1-1.0D0).LE.1.0D-3)VSP=DSQRT((0.5-XMS1)*
     &   (0.5+XMS1+1.0D0))
         IF(DABS(XMS-XMS1).LE.1.0D-3)VS0=2.0*XMS1
       ENDIF
       EZR=CONST*(BX*(VLM+VSM+VLP+VSP)+BZ*(VL0+VS0))
       EZI=CONST*BY*(VLM+VSM-VLP-VSP)
       RETURN
      END
