       SUBROUTINE UNBUN2(IVAL,N,L,ML,IMS,XN,XL,XML,XMS)
       
       IMPLICIT REAL*8(A-H,O-Z)
C-----------------------------------------------------------------------
C
C ADAS305 version. Developed from JETSHP.STARK.FORT (H P Summers).
C
C VERSION  : 1.1
C DATE     : 24-02-2005
C MODIFIED : Martin O'Mullane
C             - First version.
C
C----------------------------------------------------------------------
       I=IVAL
       J=I/32
       I1=I-32*J
       I=J
       J=I/16
       I2=I-16*J
       I=J
       J=I/16
       L=I-16*J
       N=J
       IMS=I1-1
       ML=I2-L
       XN=N
       XL=L
       XML=ML
       XMS=0.5*IMS
       RETURN
      END
