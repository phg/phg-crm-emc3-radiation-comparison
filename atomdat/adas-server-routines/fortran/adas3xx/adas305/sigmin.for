      SUBROUTINE SIGMIN
      
      IMPLICIT REAL*8 (A-H,O-Z)
C-----------------------------------------------------------------------
C  PURPOSE: Calculates rate coefficients for excitation by ion impact
C-----------------------------------------------------------------------
C
C ADAS305 version. Developed from JETSHP.STARK.FORT (H P Summers).
C
C VERSION  : 1.1
C DATE     : 24-02-2005
C MODIFIED : Martin O'Mullane
C             - First version.
C
C VERSION  : 1.2
C DATE     : 16-05-2007
C MODIFIED : Allan Whiteford
C             - Updated comments as part of subroutine documentation
C               procedure.
C
C----------------------------------------------------------------------

      INTEGER IMAX,IMAXB,NA(66)
      DIMENSION SIGEX(9),A1(66,66),A(65,65),B(65),WR(66),SIG(66)

      COMMON/PLDAT/EBEAM,T,DENS,ZEFF,MU
      COMMON/S/CLAM,SIGEL,SIGIN
      COMMON/AP/WF(10),XF(10)
      COMMON/D/DELTAE,WR
      COMMON/B/FSPL(1),XNEW(1)

      KK=1
      IF(KK.NE.1)GOTO 3004
      XNEW(1)=0.919*DELTAE*DSQRT(CLAM)/EBEAM
      CALL SFI2
      SIGIN=7.80D-08*CLAM*DEXP(FSPL(1))*DEXP(-2.*
     &DSQRT(XNEW(1)))/DSQRT(EBEAM)
      GOTO 3005
 3004 FINT=0.0
      DO 3003 I=1,10
      XNEW(1)=MU*DELTAE*DSQRT(CLAM)/(2.*T*(XF(I)+DELTAE/T))
      CALL SFI2
      FINT=FINT+WF(I)*DEXP(-(2.*DSQRT(XNEW(1))+DELTAE/T))*
     &DEXP(FSPL(1))
 3003 CONTINUE
      SIGIN=2.*DSQRT(2.*3.14*MU*27.2/T)*CLAM*FINT*6.12D-9*0.406
 3005 CONTINUE
      RETURN
      END
