       subroutine c5rlsp( xn  , xl  , xml  , xms  ,
     &                    xn1 , xl1 , xml1 , xms1 ,
     &                    er  , ei
     &                  )

       implicit none
C-----------------------------------------------------------------------
C
C  ****************** fortran77 subroutine: c5rlsp *********************
C
C  PURPOSE:  Evaluates relativistic+spin orbit matrix elements  of the
C            form <phi|H|phi_1> for hydrogen in the nlm_lm_s individual
C            set basis.
C
C  CALLING PROGRAM: stark (adas305)
C
C  SUBROUTINE:
C
C  INPUT : (R*8)   xn    = principal quantum number (bra state)
C  INPUT : (R*8)   xl    = orbital angular momentum quantum number
C  INPUT : (R*8)   xml   = azimuthal orbital ang-mom quantum number
C  INPUT : (R*8)   xms   = azimuthal spin ang-mom quantum number
C  INPUT : (R*8)   xn1   = principal quantum number (ket state)
C  INPUT : (R*8)   xl1   = orbital angular momentum quantum number
C  INPUT : (R*8)   xml1  = azimuthal orbital ang-mom quantum number
C  INPUT : (R*8)   xms1  = azimuthal spin ang-mom quantum number
C
C  OUTPUT: (R*8)   er    = real part of rel-spin matrix elem (Ryd)
C  OUTPUT: (R*8)   ei    = imag part of rel-spin matrix elem (Ryd)
C
C
C ROUTINES:
C          none
C
C AUTHOR:  Hugh Summers,  University of Strathclyde
C          JA7.08
C          Tel. 0141-548-4196
C
C DATE  :  24/01/06
C
C
C VERSION  : 1.1
C DATE     : 24-01-2006
C MODIFIED : Hugh Summers
C             - First version.
C
C----------------------------------------------------------------------
       real*8    alpha
C----------------------------------------------------------------------
       parameter ( alpha = 7.2974d-3 )
C----------------------------------------------------------------------
       integer   i4unit
C----------------------------------------------------------------------
       real*8    xn  , xl  , xml  , xms  ,
     &           xn1 , xl1 , xml1 , xms1
       real*8    xm
       real*8    er  , ei
       real*8    c1  , c2
C----------------------------------------------------------------------

       er=0.0d0
       ei=0.0d0

       if((dabs(xn-xn1).gt.1.0d-3).or.
     &    (dabs(xl-xl1).gt.1.0d-3))return

       if(dabs(xml+xms-xml1-xms1).gt.1.0d-3) return

       c1=1.0d0/(xn*xn)
       c2=alpha*alpha/xn**2

       xm=xml+xms

       if(dabs(xl).le.1.0d-3) then
           if((dabs(xms-0.50d0).le.1.0d-3).and.
     &        (dabs(xml).le.1.0d-3).and.
     &        (dabs(xms-xms1).le.1.0d-3).and.
     &        (dabs(xml-xml1).le.1.0d-3))then
              er=-c1*(1.0d0+c2*(xn-0.75d0))
              return
           elseif((dabs(xms+0.50d0).le.1.0d-3).and.
     &        (dabs(xml).le.1.0d-3).and.
     &        (dabs(xms-xms1).le.1.0d-3).and.
     &        (dabs(xml-xml1).le.1.0d-3))then
              er=-c1*(1.0d0+c2*(xn-0.75d0))
              return
           else
              return
           endif
        endif


       if(dabs(xms-0.5d0).le.1.0d-3) then
           if((dabs(xml-xml1).le.1.0d-3).and.
     &        (dabs(xms-xms1).le.1.0d-3)) then
               er=-c1*(1.0d0-0.75d0*c2)-
     &             c2*(2.0d0*xl*(xl+1.0d0)-xm+0.5d0)/(xn*xl*(xl+1.0d0)*
     &             (2.0d0*xl+1.0d0))
           elseif((dabs(xml-xml1+1.0d0).le.1.0d-3).and.
     &        (dabs(xms-xms1-1.0d0).le.1.0d-3)) then
               er=c2*dsqrt((xl+xm+0.5d0)*(xl-xm+0.5d0))/
     &            (xn*xl*(xl+1.0d0)*
     &            (2.0d0*xl+1.0d0))
           else
               return
           endif
       elseif(dabs(xms+0.5d0).le.1.0d-3) then
           if((dabs(xml-xml1).le.1.0d-3).and.
     &        (dabs(xms-xms1).le.1.0d-3)) then
               er=-c1*(1.0d0-0.75d0*c2)-
     &             c2*(2.0d0*xl*(xl+1.0d0)+xm+0.5d0)/(xn*xl*(xl+1.0d0)*
     &             (2.0d0*xl+1.0d0))
           elseif((dabs(xml-xml1-1.0d0).le.1.0d-3).and.
     &        (dabs(xms-xms1+1.0d0).le.1.0d-3)) then
               er=c2*dsqrt((xl+xm+0.5d0)*(xl-xm+0.5d0))/
     &            (xn*xl*(xl+1.0d0)*
     &            (2.0d0*xl+1.0d0))
           else
               return
           endif
       else
            return
       endif

      end
