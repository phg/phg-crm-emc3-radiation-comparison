       SUBROUTINE STARK2(EX,EY,EZ,XN,XL,XML,XMS,XN1,XL1,XML1,XMS1,
     &                   ESR,ESI)

       IMPLICIT REAL*8(A-H,O-Z)
C-----------------------------------------------------------------------
C
C ADAS305 version. Developed from JETSHP.STARK.FORT (H P Summers).
C
C VERSION  : 1.1
C DATE     : 24-02-2005
C MODIFIED : Martin O'Mullane
C             - First version.
C
C----------------------------------------------------------------------
       ESR=0.0D0
       ESI=0.0D0
       CONST=8.50908D-6
       VRP=0.0D0
       VR0=0.0D0
       VRM=0.0D0
       IF(DABS(XMS-XMS1).LE.1.0D-3.AND.DABS(XL-XL1).LE.1.001D0.AND.
     &DABS(XML-XML1).LE.1.001D0)THEN
         XLG=DMAX1(XL,XL1)
         RD=1.5D0*XN*DSQRT(XN*XN-XLG*XLG)
         U=XML-XML1
         K=NINT(-XML)
         S=0.488603D0*RD*(-1.0D0)**K*DSQRT((2.0D0*XL+1.0D0)*(2.0D0*XL1+
     &   1.0D0))*
     &   WIG3J(XL1,1.0D0,XL,0.0D0,0.0D0,0.0D0)*
     &   WIG3J(XL1,1.0D0,XL,XML1,U,-XML)
         IF(DABS(S).LE.1.0D-8)S=0.0D0
         IF(U.GE.0.999D0)VRP=-1.44720D0*S
         IF(U.LE.-0.999D0)VRM=1.44720D0*S
         IF(DABS(U).LE.1.0D-3)VR0=2.04665D0*S
       ENDIF
       ESR=CONST*(EX*(VRM+VRP)+EZ*VR0)
       ESI=CONST*EY*(VRM-VRP)
       RETURN
      END
