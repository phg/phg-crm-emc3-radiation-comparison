       subroutine c5pixv( ndpix  , npix   , wvmin , wvmax ,
     &                    cpixmx ,
     &                    tev    , amssno , wvl   , pec   ,
     &                    cpixa  , ind1   , ind2
     &                  )

       implicit none
C-----------------------------------------------------------------------
C
C  ****************** fortran77 subroutine: c5pixv *********************
C
C  Purpose:  Distribute Doppler broadened line emission into pixel range
C
C  Calling program: adas305, stark
C
C
C  Subroutine:
C
C  input : (i*4)  ndpix   = maximum number of pixels
C
C  input : (i*4)  npix    = number of pixels assigned to wavelength interval
C  input : (r*8)  wvmin   = lower limit of wavelength interval (ang)
C  input : (r*8)  wvmax   = upper limit of wavelength interval (ang)
C
C  input : (r*8)  cpixmx  = largest pixel count currently found
C                           for the wavelength range
C
C  input : (r*8)  tev     = electron temperature (eV)
C  input : (r*8)  amssno  = atomic mass number
C  input : (r*8)  wvl     = input line wavelength for test(ang)
C  input : (r*8)  pec     = emissivity coefficient for component
C
C  output: (r*8)  cpixa() = counts in each pixel for the line
C  output: (r*8)  ind1    = first pixel with non-negligible count
C  output: (r*8)  ind2    = last pixel with non-negligible count
C
C          (r*8)  fcrit   = pixel counts for the selected line below
C                           this fraction of the largest pixel count are
C                           discounted.
C Routines:
C          Routine    Source    Brief Description
C          -------------------------------------------------------------
C          r8erfc     ADAS      returns erfc(x) function value
C
C
C Author:  Martin O'Mullane
C Date:    18-02-2005
C
C Notes:   Based on hapixv.for in adas810.
C
C
C
C VERSION  : 1.1
C DATE     : 18-02-2005
C MODIFIED : Martin O'Mullane
C              - First version.
C
C VERSION  : 1.2
C DATE     : 04-07-2007
C MODIFIED : Hugh Summers
C             - Corrected error in ind1 & ind2 return (see xxpixv).
C
C-----------------------------------------------------------------------
      real*8    fcrit
C-----------------------------------------------------------------------
      parameter (fcrit = 1.0D-6)
C-----------------------------------------------------------------------
      integer   ndpix       , npix      ,
     &          ind1        , ind2
      integer   i           , i0
C-----------------------------------------------------------------------
      real*8    wvmin       , wvmax    , cpixmx    ,
     &          tev         , amssno    , wvl      , pec
      real*8    sigma       , h         , dwvl     , cmax     ,
     &          xp          , xm        , erfcp    , erfcm    ,
     &          erfcp1      , erfcm1    , cnt
      real*8    r8erfc
C-----------------------------------------------------------------------
      real*8    cpixa(ndpix)
C-----------------------------------------------------------------------


C----------------------------------------------------------------------
C  Establish sigma and the pixel in which the line lies
C-----------------------------------------------------------------------

      sigma = 4.6344D-5 * wvl * dsqrt(tev/amssno)
      h     = ( wvmax - wvmin ) / ( npix - 1 )
      i0    = int( (wvl - wvmin) / h ) + 1
      dwvl  = wvl - h * dfloat(i0-1) - wvmin

C-----------------------------------------------------------------------
C fill the pixel bins working outwards from i0
C-----------------------------------------------------------------------
       xm    = dwvl
       xp    = h - dwvl
       erfcm = r8erfc(xm/sigma)
       erfcp = r8erfc(xp/sigma)

       if (i0.ge.1) then
          cpixa(i0) = pec*(1.0D0-0.5d0*(erfcm+erfcp))
          cmax      = dmax1(cpixmx,cpixa(i0))
       else
           write(0,*)'C5PIXV warning : No valid pixels'
           return
       endif


       ind1=i0
       ind2=i0

       if (i0.gt.1) then
           do i=i0-1,1,-1
             xm=xm+h
             erfcm1 = r8erfc(xm/sigma)
             cnt = 0.5d0*pec*(erfcm-erfcm1)
             if(cnt.gt.(fcrit*cmax))then
                 cpixa(i)=cnt
                 erfcm=erfcm1
             else
                 ind1 = i+1
                 go to 10
             endif
           enddo
           ind1 = 1
       endif

   10  if (i0.lt.npix) then
           do i=i0+1,npix
             xp=xp+h
             erfcp1 = r8erfc(xp/sigma)
             cnt = 0.5d0*pec*(erfcp-erfcp1)
             if(cnt.gt.(fcrit*cmax))then
                 cpixa(i)=cnt
                 erfcp=erfcp1
             else
                 ind2 = i-1
                 go to 20
             endif
           enddo
           ind2 = npix
        endif

   20   return


      end
