       FUNCTION WATINT(N,L,A,B)
       IMPLICIT REAL*8(A-H,O-Z)
C----------------------------------------------------------------------
C  EVALUATES INTEGRAL FROM 0 TO INF. OF
C       EXP(-A*T)*T**N*J(L,B*T)
C  WHERE J IS THE HALF-INTEGER SPHERICAL BESSEL FUNCTION
C  C.F. WATSON
C  REQUIRE    N.GE.L+1
C  INPUT
C      N
C      L
C      A
C      B
C  OUTPUT
C      WATINT=RESULTANT INTEGRAL
C  ******   H.P. SUMMERS, JET            20 JUNE 1988   **********
C----------------------------------------------------------------------
C
C ADAS305 version. Developed from JETSHP.STARK.FORT (H P Summers).
C
C VERSION  : 1.1
C DATE     : 24-02-2005
C MODIFIED : Martin O'Mullane
C             - First version.
C
C----------------------------------------------------------------------
       WATINT=0.0D0
       IF(N.LT.L+1)RETURN
       DA=1.0D0/A
       BDA=(B*DA)**2
       DEN=1.0D0/(1.0D0+BDA)
       F1=1.0D0
       DO 20 IN=1,N
   20  F1=F1*IN*DA*DEN
       F1=F1*DA
       IF(L.LE.0)GO TO 35
       DO 30 IL=2,L+1
   30  F1=F1*(N+IL-1)*DA*B/(2*IL-1)
   35  CONTINUE
       SUM=0.0D0
       T0=1.0D0
       I=-1
   40  SUM=SUM+T0
       I=I+1
       K=(L-N+1+2*I)*(L-N+2+2*I)
       IF(K.NE.0)THEN
          T0=T0*K*(-BDA)/((2*L+3+2*I)*(I+1)*2)
          GO TO 40
       ENDIF
       WATINT=F1*SUM
       RETURN
      END
