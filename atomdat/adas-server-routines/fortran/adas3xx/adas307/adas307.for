CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas307/adas307.for,v 1.2 2007/05/24 15:27:25 mog Exp $ Date $Date: 2007/05/24 15:27:25 $
CX
      PROGRAM ADAS307
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 PROGRAM: ADAS307 **********************
C
C  ORIGINAL NAME: SPSCLMSJ / SCLMSJ
C
C  VERSION:  1.0 (ADAS91)
C
C  PURPOSE:  SCANNING PROGRAM FOR J-RESOLVED EFFECTIVE EMISSIVITY RATE
C            COEFFICIENTS. FOR A GIVEN TRANSITION THE PROGRAM PERFORMS
C            ONE DIMENSIONAL SCANS IN BEAM ENERGY , ION TEMPERATURE ,
C            ION DENSITY , PLASMA EFFECTIVE Z , AND PLASMA MAGNETIC
C            INDUCTION. (AT PRESENT THE ELECTRON DENSITY IS FIXED.
C            STRICTLY IT SHOULD BE ADJUSTED WITH ION DENSITY.) THE
C            PROGRAM CAN BE RUN EITHER IN THE FOREGROUND OR IN BATCH.
C
C            THE EMISSIVITY CALCULATION IS APPLICABLE TO IMPURITIES IN
C            PLASMA TRAVERSED BY NEUTRAL BEAMS OF H OR HE. THE
C            RECOMBINED TARGET ION MAY BE H, LI OR NA-LIKE. THE MODEL
C            INCLUDES CAPTURE, N-N' LEVEL CASCADE, AND MIXING AMONG L,J
C            LEVELS OF SAME N BY COLLISIONS OR MAGNETIC FIELDS.
C            ELECTRON IMPACT IONISATION IS INCLUDED TO GIVE COLLISION
C            LIMIT EFFECT. AN INTERNAL EIKONAL APPROXIMATION IS USED
C            FOR CAPTURE FROM EXCITED H OR HE STATES, ALTHOUGH NORMALLY
C            THE EXTERNAL DATA SET SHOULD BE USED.
C
C  DATA:     THE SOURCE DATA IS CONTAINED AS MEMBERS IN A PARTITIONED
C            DATA SET AS FOLLOWS:-
C
C                    'JETSHP.QCX#<DONOR>.DATA(<MEMBER>)'
C
C            WHERE
C              <DONOR>    = <DONOR ELEMENT SYMBOL><DONOR CHARGE STATE>
C              <MEMBER>   = <ID>#<RECEIVER>
C
C              <ID>       = 3 CHARACTER PREFIX IDENTIFYING THE SOURCE
C                           OF THE DATA.
C              <RECEIVER> = <RECVR ELEMENT SYMBOL><RECVR CHARGE STATE>
C
C
C              E.G. 'JETSHP.QCX#H0.DATA(OLD#C6)'
C
C           THE UNITS USED IN THE DATA FILE ARE TAKEN AS FOLLOWS:
C
C           ENERGIES            : KEV / AMU
C           CROSS SECTIONS      : CM**2
C
C  PROGRAM:
C
C  PARAM : (I*4)  IUNT07    = OUTPUT UNIT FOR RUN SUMMARY.
C  PARAM : (I*4)  IUNT10    = INPUT UNIT FOR CHARGE EXCHANGE DATA SET.
C  PARAM : (I*4)  IUNT11    = OUTPUT UNIT FOR PASSING FILE.
C  PARAM : (I*4)  IUNT12    = OUTPUT UNIT FOR JCL FILE.
C  PARAM : (I*4)  MXNENG    = MAXIMUM NO. OF ENERGIES IN DATA SET.
C  PARAM : (I*4)  MXNSHL    = MAXIMUM NUMBER OF N SHELLS.
C  PARAM : (I*4)  MXSCAN    = MAXIMUM NUMBER OF VALUES IN A SCAN.
C
C          (I*4)  ISTOP     = VALUE READ FROM PIPE FOR MENU BUTTON STATE
C                             1 => PRESSED, 0 => NOT
C          (I*4)  IPSET     = INPUT PARAMETER SET NUMBER FOR CURRENT.
C          (I*4)  NGRND     = PRINCIPAL QUANTUM NUMBER OF GROUND STATE.
C          (I*4)  NTOT      = PRINCIPAL QUANTUM NUMBER OF HIGHEST BOUND
C                             STATE.
C          (I*4)  NBOT      = MINIMUM PRINCIPAL QUANTUM NUMBER FOR
C                             RATE TABLES.
C          (I*4)  NTOP      = MAXIMUM PRINCIPAL QUANTUM NUMBER FOR
C                             RATE TABLES.
C          (I*4)  IZR       = ION CHARGE OF RECEIVER.
C          (I*4)  IZD       = ION CHARGE OF DONOR.
C          (I*4)  INDD      = DONOR STATE INDEX.
C          (I*4)  NENRGY    = NUMBER OF ENERGIES READ FROM DATA SET.
C          (I*4)  NMINF     = LOWEST N-SHELL FOR WHICH DATA READ.
C          (I*4)  NMAXF     = HIGHEST N-SHELL FOR WHICH DATA READ.
C          (I*4)  IDZ0      = DONOR NUCLEAR CHARGE.
C          (I*4)  IRZ0      = RECEIVER NUCLEAR CHARGE.
C          (I*4)  IRZ1      = RECEIVER ION INITIAL CHARGE.
C          (I*4)  IRZ2      = RECEIVER ION FINAL CHARGE.
C          (I*4)  IPAN      = ISPF PANEL NUMBER.
C          (I*4)  NBEAM     = NUMBER OF BEAM ENERGIES.
C          (I*4)  ITHEOR    = CHARGE EXCHANGE MODEL OPTION.
C                             1 => USE INPUT DATA SET.
C                             2 => USE EIKONAL MODEL.
C          (I*4)  IBSTAT    = DONOR STATE FOR EIKONAL MODEL.
C                             1 => H(1S)
C                             2 => H(2S)
C                             3 => H(2P)
C                             4 => HE(1S2)
C                             5 => HE(1S2S)
C          (I*4)  IEMMS     = EMISSION MEASURE MODEL OPTION.
C                             1 => CHARGE EXCHANGE.
C                             2 => ELECTRON IMPACT EXCITATION.
C                             3 => RADIATIVE RECOMBINATION.
C          (I*4)  NTU       = UPPER PRINCIPAL QUANTUM NUMBER OF
C                             TRANSITION.
C          (I*4)  NTL       = LOWER PRINCIPAL QUANTUM NUMBER OF
C                             TRANSITION.
C          (I*4)  NBMENG    = NUMBER OF BEAM ENERGIES IN SCAN.
C          (I*4)  NTIEV     = NUMBER OF ION TEMPERATURES IN SCAN.
C          (I*4)  NDENSZ    = NUMBER OF ION DENSZTIES IN SCAN.
C          (I*4)  NZEFF     = NUMBER OF PLASMA EFFECTIVE Z IN SCAN.
C          (I*4)  NBMAG     = NUMBER OF PLASMA MAGNETIC FIELDS IN SCAN.
C          (I*4)  IRCODE    = RETURN CODE FROM IBM SUBROUTINE 'FILEINF'.
C          (I*4)  I         = LOOP INDEX.
C
C          (R*8)  RAMSNO    = RECEIVER ATOMIC MASS.
C          (R*8)  TEV       = ELECTRON TEMPERATURE.
C                             UNITS: EV
C          (R*8)  TIEV      = ION TEMPERATURE.
C                             UNITS: EV
C          (R*8)  DENS      = ELECTRON DENSITY.
C                             UNITS: CM-3
C          (R*8)  DENSZ     = PLASMA ION DENSITY.
C                             UNITS: CM-3
C          (R*8)  ZEFF      = EFFECTIVE ION CHARGE.
C          (R*8)  BMAG      = PLASMA MAGNETIC INDUCTION.
C                             UNITS: TESLA
C          (R*8)  BMENG     = BEAM ENERGY.
C                             UNITS: EV/AMU
C          (R*8)  ERATE     = EFFECTIVE EMISSIVITY RATE COEFFICIENT FOR
C                             REQUESTED TRANSITION.
C                             SPECTRUM LINE.
C                             UNITS: CM3 SEC-1
C          (R*8)  QEFREF    = REFERENCE VALUE FOR EFFECTIVE RATE COEFFT.
C                             UNITS: CM3 SEC-1
C          (R*8)  SCNVAL    = VALUE OF QUANTITY CURRENTLY BEING SCANNED.
C
C          (L*4)  LBATCH    = FLAS HOW PROGRAM IS BEING EXECUTED.
C                             .TRUE.  => PROGRAM RUNNING IN BATCH.
C                             .FALSE. => PROGRAM RUNNING IN FOREGROUND.
C          (L*4)  OPEN10    = FLAGS IF INPUT CX DATA SET OPEN.
C                             .TRUE.  => INPUT CX DATA SET OPEN.
C                             .FALSE. => INPUT CX DATA SET CLOSED.
C          (L*4)  OPEN11    = FLAGS IF PASSING FILE OPEN.
C                             .TRUE.  => PASSING FILE OPEN.
C                             .FALSE. => PASSING FILE CLOSED.
C          (L*4)  LPARMS    = FLAGS IF L-SPLITTING PARAMETERS PRESENT.
C                             .TRUE.  => L-SPLITTING PARAMETERS PRESENT.
C                             .FALSE  => L-SPLITTING PARAMETERS ABSENT.
C          (L*4)  LSETL     = FLAGS IF L-RESOLVED DATA PRESENT.
C                             .TRUE.  => L-RESOLVED DATA PRESENT.
C                             .FALSE  => L-RESOLVED DATA ABSENT.
C          (L*4)  LSETM     = FLAGS IF M-RESOLVED DATA PRESENT.
C                             .TRUE.  => M-RESOLVED DATA PRESENT.
C                             .FALSE  => M-RESOLVED DATA ABSENT.
C          (L*4)  LPEND     = FLAGS IF END OF ANALYSIS REQUESTED.
C                             .TRUE.  => END ANALYSIS OF CURRENT DATA
C                                        SETS
C                             .FALSE. => CONTINUE PANALYSIS WITH CURRENT
C                                        DATA SETS
C          (L*4)  LBTSEL    = FLAGS IF PROGRAM TO BE RUN IN BATCH.
C                             .TRUE.  => RUN PROGRAM IN BATCH.
C                             .FALSE. => RUN PROGRAM IN FOREGROUND.
C
C          (C*8)  DATE      = DATE.
C          (C*3)  REP       = REQUEST FOR PROGRAM TERMINATION.
C                             'YES' => TERMINATE PROGRAM EXECUTION.
C                             'NO'  => CONTINUE PROGRAM EXECUTION.
C          (C*80) DSNIN     = FILE NAME OF INPUT DATA SET.
C          (C*80) TITLEF    = NOT SET - TITLE FROM INPUT DATA SET.
C          (C*2)  SYMBR     = RECEIVER ELEMENT SYMBOL.
C          (C*2)  SYMBD     = DONOR ELEMENT SYMBOL.
C          (C*40) TITLE     = ISPF ENTERED GENERAL TITLE FOR RUN.
C          (C*80) DSNRUN    = RUN SUMMARY FILE DATA SET NAME.
C          (C*80) DSNPAS    = PASSING FILE DATA SET NAME.
C          (C*80) CADAS     = ADAS HEADER: INCLUDES RELEASE, PROGRAM,
C                             TIME.
C
C          (I*4)  LFORMA()  = PARAMETERS FOR CALCULATING L-RES X-SEC
C                             READ FROM INPUT DATA SET.
C                             DIMENSION: ENERGY INDEX
C
C          (R*8)  ENRGYA()  = COLLISION ENERGIES READ FROM INPUT DATA
C                             SET.
C                             UNITS: EV/AMU
C                             DIMENSION: ENERGY INDEX
C          (R*8)  ALPHAA()  = EXTRAPOLATION PARAMETER ALPHA READ FROM
C                             INPUT DATA SET.
C                             DIMENSION: ENERGY INDEX
C          (R*8)  XLCUTA()  = PARAMETERS FOR CALCULATING L-RES X-SEC
C                             READ FROM INPUT DATA SET.
C                             DIMENSION: ENERGY INDEX
C          (R*8)  PL2A()    = PARAMETERS FOR CALCULATING L-RES X-SEC
C                             READ FROM INPUT DATA SET.
C                             DIMENSION: ENERGY INDEX
C          (R*8)  PL3A()    = PARAMETERS FOR CALCULATING L-RES X-SEC
C                             READ FROM INPUT DATA SET.
C                             DIMENSION: ENERGY INDEX
C          (R*8)  XSECTA()  = TOTAL CHARGE EXCHANGE CROSS-SECTIONS READ
C                             FROM INPUT DATA SET.
C                             UNITS: CM2
C                             DIMENSION: ENERGY INDEX
C          (R*8)  BMENGA()  = BEAM ENERGIES FOR SCAN.
C                             UNITS: EV/AMU
C                             DIMENSION: MXSCAN
C          (R*8)  DENSZA()  = ION DENSZTIES FOR SCAN.
C                             UNITS: CM-3
C                             DIMENSION: MXSCAN
C          (R*8)  TIEVA()   = ION TEMPERATURES FOR SCAN.
C                             UNITS: EV
C                             DIMENSION: MXSCAN
C          (R*8)  ZEFFA()   = PLASMA EFFECTIVE Z FOR SCAN.
C                             DIMENSION: MXSCAN
C          (R*8)  BMAGA()   = PLASMA MAGNETIC FIELDS FOR SCAN.
C                             UNITS: TESLA
C                             DIMENSION: MXSCAN
C          (R*8)  QEFBEA()  = EFFECTIVE RATE COEFFICIENTS FOR BEAM
C                             ENERGY SCAN.
C                             UNITS: CM3 SEC-1
C                             DIMENSION: MXSCAN
C          (R*8)  QEFTIA()  = EFFECTIVE RATE COEFFICIENTS FOR ION
C                             TEMPERATURE SCAN.
C                             UNITS: CM3 SEC-1
C                             DIMENSION: MXSCAN
C          (R*8)  QEFDZA()  = EFFECTIVE RATE COEFFICIENTS FOR ION
C                             DENSITY SCAN.
C                             UNITS: CM3 SEC-1
C                             DIMENSION: MXSCAN
C          (R*8)  QEFZEA()  = EFFECTIVE RATE COEFFICIENTS FOR PLASMA
C                             EFFECTIVE Z SCAN.
C                             UNITS: CM3 SEC-1
C                             DIMENSION: MXSCAN
C          (R*8)  QEFBMA()  = EFFECTIVE RATE COEFFICIENTS FOR PLASMA
C                             MAGNETIC FIELD SCAN.
C                             UNITS: CM3 SEC-1
C                             DIMENSION: MXSCAN
C
C          (R*8)  XSECNA(,) = N-RESOLVED CHARGE EXCHANGE CROSS-SECTIONS
C                             READ FROM INPUT DATA SET.
C                             UNITS: CM2
C                             1ST DIMENSION: ENERGY INDEX
C                             2ND DIMENSION: N-SHELL
C          (R*8)  FRACLA(,) = L-RESOLVED CHARGE EXCHANGE CROSS-SECTIONS.
C                             AFTER CXDATA: ABSOLUTE VALUES (CM2).
C                             AFTER CXFRAC: FRACTION OF N-RESOLVED
C                                            DATA.
C                             1ST DIMENSION: ENERGY INDEX
C                             2ND DIMENSION: INDEXED BY I4IDFL(N,L)
C          (R*8)  FRACMA(,) = M-RESOLVED CHARGE EXCHANGE CROSS-SECTIONS.
C                             AFTER CXDATA: ABSOLUTE VALUES (CM2).
C                             AFTER CXFRAC: FRACTION OF L-RESOLVED
C                                            DATA.
C                             1ST DIMENSION: ENERGY INDEX
C                             2ND DIMENSION: INDEXED BY I4IDFM(N,L,M)
C                                            WITH M >= 0 ONLY
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          C7BCH0     ADAS      READS INPUT DATA SET NAME WHEN RUNNING
C                               IN BATCH.
C          C7SPF0     ADAS      HANDLES ISPF PANEL FOR DATA SET INPUT.
C          C6CHRG     ADAS      SETS UP CHARGES AND EXTREME N LEVELS FOR
C                               DONOR AND RECEIVER.
C          C7BCH0     ADAS      READS USER INPUT DATA FROM JCL FILE WHEN
C                               RUNNING IN BATCH.
C          C7ISPF     ADAS      HANDLES ISPF PANELS FOR USER INPUT.
C          C7SPF1     ADAS      HANDLES ISPF PANEL FOR SETTING UP JCL
C                               FILE.
C          C7CXEE     ADAS      CALCULATES CHARGE EXCHANGE EFFECTIVE
C                               EMISSIVITY COEFFICIENT.
C          C7OUT0     ADAS      WRITES RUN SUMMARY FOR ADAS307.
C          C7OUT1     ADAS      WRITES PASSING FILE FOR ADAS307.
C          CXDATA     ADAS      READS INPUT DATA SET IN ADF01 FORMAT.
C          CXFRAC     ADAS      CONVERTS L AND M RESOLVED CROSS-SECTIONS
C                               FROM ABSOLUTE VALUES TO FRACTIONS.
C          CXEXTR     ADAS      EXTRAPOLATES N AND L RESOLVED CROSS-
C                               SECTIONS FROM INPUT DATA SET.
C          CXSETP     ADAS      SETS UP PARAMETERS IN THE SHARED POOL
C                               FOR ISPF PANEL DISPLAY.
C          XX0000     ADAS      SETS MACHINE DEPENDENT ADAS
C                               CONFIGURATION.
C          XXDATE     ADAS      RETURNS CURRENT DATE.
C          XXTEXT     ADAS      DISPLAYS DESCRIPTIVE TEXT FROM INPUT
C                               DATA SET.
C          FILEINF    IBM       SETS UP DATASET ATTRIBUTES.
C          ZA06CS     HSL       READS INFO. FROM JCL //EXEC PARM.G FIELD
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/87
C          JET EXT. 5183
C
C DATE:    03/12/93
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    24TH MAY 1996
C
C VERSION: 1.1                          DATE: 24-05-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST VERSION. WRITTEN USING ADAS309.FOR
C
C VERSION  : 1.2
C DATE     : 22-05-2007
C MODIFIED : Martin O'Mullane
C              - Remove unused m-subshell data possibility.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    IUNT07       , PIPEOU       , IUNT10       ,
     &           MXNENG       , MXNSHL       , MXSCAN       ,
     &           PIPEIN
      PARAMETER( IUNT07 = 7   , IUNT10 = 10  , PIPEOU = 6   ,
     &           MXNENG = 30  , MXNSHL = 20  , MXSCAN = 24  ,
     &           PIPEIN = 5 )
C-----------------------------------------------------------------------
      INTEGER    IPSET   , NGRND   , NTOT    , NBOT    , NTOP    ,
     &           IZR     , IZD     , INDD    , NENRGY  , NMINF   ,
     &           NMAXF   , IDZ0    , IRZ0    , IRZ1    , IRZ2    ,
     &           I       , IBATCH  , J       ,
     &           ISTOP
C-----------------------------------------------------------------------
      LOGICAL    OPEN10  , OPEN11  , LPARMS  ,
     &           LSETL   , LSETM   , LPEND   , LBTSEL
C-----------------------------------------------------------------------
      CHARACTER  DATE*8     , REP*3      , DSNIN*80   , TITLEF*80  ,
     &           SYMBR*2    , SYMBD*2    ,
     &           DSNPAS*80  , CADAS*80
C-----------------------------------------------------------------------
      INTEGER    LFORMA(MXNENG)
C-----------------------------------------------------------------------
      REAL*8     ENRGYA(MXNENG)  , ALPHAA(MXNENG)  , XLCUTA(MXNENG)  ,
     &           PL2A(MXNENG)    , PL3A(MXNENG)    , XSECTA(MXNENG)
C-----------------------------------------------------------------------
      REAL*8     XSECNA(MXNENG,MXNSHL)                            ,
     &           FRACLA(MXNENG,(MXNSHL*(MXNSHL+1))/2)             ,
     &           FRACMA(MXNENG,(MXNSHL*(MXNSHL+1)*(MXNSHL+2))/6)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C *************************** MAIN PROGRAM ****************************
C-----------------------------------------------------------------------
C
C
C-----------------------------------------------------------------------
C SET MACHINE DEPENDANT ADAS CONFIGURATION VALUES.
C-----------------------------------------------------------------------
C
      CALL XX0000
C
C-----------------------------------------------------------------------
C GET CURRENT DATE.
C-----------------------------------------------------------------------
C
      CALL XXDATE( DATE )
C
C-----------------------------------------------------------------------
C INITIALIZE VALUES.
C-----------------------------------------------------------------------
C
      OPEN10 = .FALSE.
      OPEN11 = .FALSE.
      LBTSEL = .FALSE.
      REP = 'NO'
      DSNPAS = ' '
      CADAS = ' '
      IPSET = 0
      NTOT = MXNSHL
      NTOP = MXNSHL
      NGRND = 1
      NBOT = 2
C
C-----------------------------------------------------------------------
C START OF CONTROL LOOP.
C-----------------------------------------------------------------------
C
    1 CONTINUE
C
C-----------------------------------------------------------------------
C IF FILE IS ACTIVE ON UNIT 10 THEN CLOSE THE UNIT.
C-----------------------------------------------------------------------
C
      IF (OPEN10) THEN
         CLOSE( IUNT10 )
         OPEN10 = .FALSE.
      ENDIF
C
C-----------------------------------------------------------------------
C GET INPUT DATA SET NAME (FROM IDL). THE IDL ROUTINE PASSING THE
C DATA IS THE STANDARD C9SPF0.PRO
C-----------------------------------------------------------------------
C
      CALL C7SPF0( REP , DSNIN)
C
C-----------------------------------------------------------------------
C IF PROGRAM RUN IS COMPLETED: END RUN.
C-----------------------------------------------------------------------
C
      IF (REP .EQ. 'YES') THEN
         GOTO 9999
      ENDIF
C
C-----------------------------------------------------------------------
C OPEN INPUT DATA FILE - DSNIN
C-----------------------------------------------------------------------
C
      OPEN( UNIT=IUNT10 , FILE=DSNIN , STATUS='UNKNOWN' )
      OPEN10 = .TRUE.
C
C-----------------------------------------------------------------------
C FETCH DATA FROM SELECTED DATASET
C-----------------------------------------------------------------------

      CALL xxdata_01( IUNT10 , MXNENG , MXNSHL ,
     &                SYMBR  , SYMBD  , IZR    , IZD    ,
     &                INDD   , NENRGY , NMINF  , NMAXF  ,
     &                LPARMS , LSETL  , ENRGYA ,
     &                ALPHAA , LFORMA , XLCUTA , PL2A   ,
     &                PL3A   , XSECTA , XSECNA , FRACLA  
     &              )
C
C-----------------------------------------------------------------------
C SET UP TARGET CHARGES AND LOWER PRINCIPAL QUANTUM NUMBERS.
C-----------------------------------------------------------------------
C
      CALL C6CHRG( SYMBD  , IZD    , SYMBR  , IZR    , IDZ0   ,
     &             IRZ0   , IRZ1   , IRZ2   , NGRND  , NBOT     )
C
C-----------------------------------------------------------------------
C CONVERT L AND M RESOLVED CROSS-SECTIONS TO FRACTIONS.
C-----------------------------------------------------------------------
C
      CALL CXFRAC( MXNENG , MXNSHL , NENRGY , NMINF  , NMAXF  ,
     &             LSETL  , XSECNA , FRACLA 
     &           )
C
C-----------------------------------------------------------------------
C EXTRAPOLATE N AND L RESOLVED CROSS-SECTIONS BEYOND NMINF AND NMAXF.
C-----------------------------------------------------------------------
C
      CALL CXEXTR( MXNENG , MXNSHL , NMINF  , NMAXF  , NENRGY ,
     &             LPARMS , ALPHAA , LFORMA , XLCUTA , PL2A   ,
     &             PL3A   , XSECNA , FRACLA                     )
C
C-----------------------------------------------------------------------
C SET UP ISPF FUNCTION POOL PARAMETERS (NON-READABLE VARIABLES).
C-----------------------------------------------------------------------
C
      CALL CXSETP( 0      , 0      , SYMBD  , IDZ0   , SYMBR  ,
     &             IRZ0   , IRZ1   , IRZ2   , NGRND  , NTOT     )
C
C-----------------------------------------------------------------------
C
    2 CONTINUE
C
C-----------------------------------------------------------------------
C WRITE INFORMATION ABOUT DATA SET TO IDL
C-----------------------------------------------------------------------
C
      LPEND = .FALSE.

      CALL C7ISPF( LPEND, IBATCH)
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) ISTOP
      IF(ISTOP.EQ.1) GOTO 9999
C
C-----------------------------------------------------------------------
C IF PROGRAM RUN COMPLETE THEN 
C RETURN TO INPUT DATA SET SELECTION PANEL.
C-----------------------------------------------------------------------
C
      IF ((LPEND) .AND. (IBATCH.EQ.1) ) THEN
          GOTO 1
      ENDIF
C
C-----------------------------------------------------------------------
C  IF USER HAS MOVED ONTO OUTPUT SCREEN THEN WE GET TO HERE. IF 'CANCEL'
C  IS CLICKED THEN NO OUTPUT AND RETURN TO PROCESSING SCREEN (GOTO 2)
C  IF 'RUN NOW' IS CLICKED THEN WRITE ALL REQUIRED DATA (AND THERE IS
C  QUITE A BIT) TO IDL AND THEN RETURN TO PROCESSING SCREEN.
C-----------------------------------------------------------------------
C
3     CONTINUE
      CALL C7ISPF( LPEND, IBATCH )
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) ISTOP
      IF(ISTOP.EQ.1) GOTO 9999
      IF ((LPEND) .AND. (IBATCH.EQ.1) ) THEN
          GOTO 2
      ENDIF
C
C-----------------------------------------------------------------------
C  IF WE GET TO HERE THEN RUNNING HAS BEEN SELECTED. FIRST WRITE ALL
C  DATA TO IDL ROUTINE C7SPF2.PRO WHICH THEN SPAWNS A SECOND 
C  FORTRAN PROCESS TO COMPLETE THE CALCULATION. COMMUNICATIONS WITH
C  THIS FORTRAN ROUTINE RE-COMMENCE WHEN THE CALCULATION IS COMPLETED.
C-----------------------------------------------------------------------
C
      WRITE( PIPEOU, *) MXNENG
      CALL XXFLSH( PIPEOU )
      WRITE( PIPEOU, *) MXNSHL
      CALL XXFLSH( PIPEOU )
      DO 101, I = 1, MXNENG
          WRITE( PIPEOU, *) ENRGYA(I)
101   CONTINUE
      CALL XXFLSH( PIPEOU )
      DO 102, I = 1, MXNENG
          WRITE( PIPEOU, *) ALPHAA(I)
102   CONTINUE
      CALL XXFLSH( PIPEOU )
      DO 103, I = 1, MXNENG
          DO 104, J = 1, MXNSHL
              WRITE( PIPEOU, *) XSECNA(I,J)
104       CONTINUE
          CALL XXFLSH( PIPEOU )
103   CONTINUE
      CALL XXFLSH( PIPEOU )
      DO 105, I = 1, MXNENG
          DO 106, J = 1, (MXNSHL*(MXNSHL+1)/2)
              WRITE( PIPEOU, *) FRACLA(I,J)
106       CONTINUE
          CALL XXFLSH( PIPEOU )
105   CONTINUE
      CALL XXFLSH( PIPEOU )
      WRITE( PIPEOU, *) NMINF 
      CALL XXFLSH( PIPEOU )
      WRITE( PIPEOU, *) NMAXF 
      CALL XXFLSH( PIPEOU )
      WRITE( PIPEOU, *) NENRGY
      CALL XXFLSH( PIPEOU )

      GOTO 3
C
C-----------------------------------------------------------------------
C CLOSE I/O FILES.
C-----------------------------------------------------------------------
C
 9999 CONTINUE

      CLOSE( IUNT10 )
C
      STOP
C
C-----------------------------------------------------------------------
C
      END


