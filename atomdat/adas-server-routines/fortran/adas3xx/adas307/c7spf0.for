CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas307/c7spf0.for,v 1.1 2004/07/06 11:54:32 whitefor Exp $ Date $Date: 2004/07/06 11:54:32 $
CX
      SUBROUTINE C7SPF0( REP    , DSFULL)
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C7SPF0 *********************
C
C  PURPOSE: TO FETCH VALUES FROM IDL ROUTINE C7SPF0.PRO FOR 307 INPUT
C
C  CALLING PROGRAM: ADAS307
C
C  SUBROUTINE:
C
C  OUTPUT: (C*3)   REP     = 'YES' => TERMINATE PROGRAM EXECUTION.
C                          = 'NO ' => CONTINUE PROGRAM EXECUTION.
C
C  OUTPUT: (C*80)  DSFULL  = INPUT DATA SET NAME (FULL MVS DSN)
C                            (IN FORM SUITABLE FOR DYNAMIC ALLOCATION)
C
C
C          (I*4)  PIPEIN   = UNIT NUMBER FOR PIPE INPUT (5)
C          (I*4)  PIPEOU   = UNIT NUMBER FOR PIPE OUTPUT (6)
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    29/11/93
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    24TH MAY 1996
C
C VERSION: 1.1                          DATE: 24-05-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST VERSION
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      CHARACTER   REP*3         , DSFULL*80
C-----------------------------------------------------------------------
      INTEGER     PIPEIN        , PIPEOU
      PARAMETER ( PIPEIN=5      , PIPEOU=6)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C  READ OUTPUTS FROM PIPE
C-----------------------------------------------------------------------
C
      READ(PIPEIN,'(A)') REP
      READ(PIPEIN,'(A)') DSFULL
C
C-----------------------------------------------------------------------
C
      RETURN
      END
