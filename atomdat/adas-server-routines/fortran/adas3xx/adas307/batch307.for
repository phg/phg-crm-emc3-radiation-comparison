CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas307/batch307.for,v 1.2 2004/07/06 11:35:49 whitefor Exp $ Date $Date: 2004/07/06 11:35:49 $
CX
      SUBROUTINE BATCH307(IUNT07, IUNT14, DSBATCH)

      IMPLICIT NONE 
C----------------------------------------------------------------------- 
C 
C  ****************** FORTRAN77 SUBROUTINE: BATCH307 *******************
C
C  CALLING PROGRAM: ADAS307B
C
C  PURPOSE:  THIS IS THE TOP LEVEL SUBROUTINE FOR PART B OF ADAS307
C            WHEN IT IS RUN AS A BATCH JOB. 
C            IT OPENS ALL THE FILES AND CALLS ALL CALCULATION ROUTINES.
C            THERE IS NO PIPE COMMS WITH IDL - ALL DATA COMES FROM
C            THE BATCH FILE DSBATCH AND THROUGH THE STANDARD INPUT
C            FROM ANOTHER NAMED FILE (SEE c7spf3.pro FOR DETAILS).
C            (FOR SCIENTIFIC COMMENTS ON THE PURPOSE OF ADAS307
C            IN GENERAL THEN SEE ADAS307.FOR).
C
C  PROGRAM:
C
C  PARAM : (I*4)  IUNT07    = OUTPUT UNIT FOR RUN SUMMARY.
C  PARAM : (I*4)  IUNT14    = OUTPUT UNIT FOR PASSING FILE.
C  PARAM : (I*4)  MXNENG    = MAXIMUM NO. OF ENERGIES IN DATA SET.
C  PARAM : (I*4)  MXNSHL    = MAXIMUM NUMBER OF N SHELLS.
C  PARAM : (I*4)  MXSCAN    = MAXIMUM NUMBER OF VALUES IN A SCAN.
C
C          (C*8)  DATE      = DATE.
C          (C*80) DSNIN     = FILE NAME OF INPUT DATA SET.
C          (C*2)  SYMBR     = RECEIVER ELEMENT SYMBOL.
C          (C*2)  SYMBD     = DONOR ELEMENT SYMBOL.
C          (C*40) TITLE     = ISPF ENTERED GENERAL TITLE FOR RUN.
C          (C*80) DSNRUN    = RUN SUMMARY FILE DATA SET NAME.
C          (C*80) DSNPAS    = PASSING FILE DATA SET NAME.
C          (C*80) CADAS     = ADAS HEADER: INCLUDES RELEASE, PROGRAM,
C                             TIME.
C
C          (I*4)  IPSET     = INPUT PARAMETER SET NUMBER FOR CURRENT.
C          (I*4)  NGRND     = PRINCIPAL QUANTUM NUMBER OF GROUND STATE.
C          (I*4)  NTOT      = PRINCIPAL QUANTUM NUMBER OF HIGHEST BOUND
C                             STATE.
C          (I*4)  NBOT      = MINIMUM PRINCIPAL QUANTUM NUMBER FOR
C                             RATE TABLES.
C          (I*4)  NTOP      = MAXIMUM PRINCIPAL QUANTUM NUMBER FOR
C                             RATE TABLES.
C          (I*4)  NENRGY    = NUMBER OF ENERGIES READ FROM DATA SET.
C          (I*4)  NMINF     = LOWEST N-SHELL FOR WHICH DATA READ.
C          (I*4)  NMAXF     = HIGHEST N-SHELL FOR WHICH DATA READ.
C          (I*4)  IDZ0      = DONOR NUCLEAR CHARGE.
C          (I*4)  IRZ0      = RECEIVER NUCLEAR CHARGE.
C          (I*4)  IRZ1      = RECEIVER ION INITIAL CHARGE.
C          (I*4)  IRZ2      = RECEIVER ION FINAL CHARGE.
C          (I*4)  ITHEOR    = CHARGE EXCHANGE MODEL OPTION.
C                             1 => USE INPUT DATA SET.
C                             2 => USE EIKONAL MODEL.
C          (I*4)  IBSTAT    = DONOR STATE FOR EIKONAL MODEL.
C                             1 => H(1S)
C                             2 => H(2S)
C                             3 => H(2P)
C                             4 => HE(1S2)
C                             5 => HE(1S2S)
C          (I*4)  IEMMS     = EMISSION MEASURE MODEL OPTION.
C                             1 => CHARGE EXCHANGE.
C                             2 => ELECTRON IMPACT EXCITATION.
C          (I*4)  NTU       = UPPER PRINCIPAL QUANTUM NUMBER OF
C                             TRANSITION.
C          (I*4)  NTL       = LOWER PRINCIPAL QUANTUM NUMBER OF
C                             TRANSITION.
C          (I*4)  NBMENG    = NUMBER OF BEAM ENERGIES IN SCAN.
C          (I*4)  NTIEV     = NUMBER OF ION TEMPERATURES IN SCAN.
C          (I*4)  NDENSZ    = NUMBER OF ION DENSZTIES IN SCAN.
C          (I*4)  NZEFF     = NUMBER OF PLASMA EFFECTIVE Z IN SCAN.
C          (I*4)  NBMAG     = NUMBER OF PLASMA MAGNETIC FIELDS IN SCAN.
C          (I*4)  I         = LOOP INDEX.
C          (R*8)  RAMSNO    = RECEIVER ATOMIC MASS.
C          (R*8)  TEV       = ELECTRON TEMPERATURE.
C                             UNITS: EV
C          (R*8)  TIEV      = ION TEMPERATURE.
C                             UNITS: EV
C          (R*8)  DENS      = ELECTRON DENSITY.
C                             UNITS: CM-3
C          (R*8)  DENSZ     = PLASMA ION DENSITY.
C                             UNITS: CM-3
C          (R*8)  ZEFF      = EFFECTIVE ION CHARGE.
C          (R*8)  BMAG      = PLASMA MAGNETIC INDUCTION.
C                             UNITS: TESLA
C          (R*8)  BMENG     = BEAM ENERGY.
C                             UNITS: EV/AMU
C          (R*8)  ERATE     = EFFECTIVE EMISSIVITY RATE COEFFICIENT FOR
C                             REQUESTED TRANSITION.
C                             SPECTRUM LINE.
C                             UNITS: CM3 SEC-1
C          (R*8)  QEFREF    = REFERENCE VALUE FOR EFFECTIVE RATE COEFFT.
C                             UNITS: CM3 SEC-1
C          (R*8)  SCNVAL    = VALUE OF QUANTITY CURRENTLY BEING SCANNED.
C          (R*8)  ENRGYA()  = COLLISION ENERGIES READ FROM INPUT DATA
C                             SET.
C                             UNITS: EV/AMU
C                             DIMENSION: ENERGY INDEX
C          (R*8)  ALPHAA()  = EXTRAPOLATION PARAMETER ALPHA READ FROM
C                             INPUT DATA SET.
C                             DIMENSION: ENERGY INDEX
C          (R*8)  BMENGA()  = BEAM ENERGIES FOR SCAN.
C                             UNITS: EV/AMU
C                             DIMENSION: MXSCAN
C          (R*8)  DENSZA()  = ION DENSZTIES FOR SCAN.
C                             UNITS: CM-3
C                             DIMENSION: MXSCAN
C          (R*8)  TIEVA()   = ION TEMPERATURES FOR SCAN.
C                             UNITS: EV
C                             DIMENSION: MXSCAN
C          (R*8)  ZEFFA()   = PLASMA EFFECTIVE Z FOR SCAN.
C                             DIMENSION: MXSCAN
C          (R*8)  BMAGA()   = PLASMA MAGNETIC FIELDS FOR SCAN.
C                             UNITS: TESLA
C                             DIMENSION: MXSCAN
C          (R*8)  QEFBEA()  = EFFECTIVE RATE COEFFICIENTS FOR BEAM
C                             ENERGY SCAN.
C                             UNITS: CM3 SEC-1
C                             DIMENSION: MXSCAN
C          (R*8)  QEFTIA()  = EFFECTIVE RATE COEFFICIENTS FOR ION
C                             TEMPERATURE SCAN.
C                             UNITS: CM3 SEC-1
C                             DIMENSION: MXSCAN
C          (R*8)  QEFDZA()  = EFFECTIVE RATE COEFFICIENTS FOR ION
C                             DENSITY SCAN.
C                             UNITS: CM3 SEC-1
C                             DIMENSION: MXSCAN
C          (R*8)  QEFZEA()  = EFFECTIVE RATE COEFFICIENTS FOR PLASMA
C                             EFFECTIVE Z SCAN.
C                             UNITS: CM3 SEC-1
C                             DIMENSION: MXSCAN
C          (R*8)  QEFBMA()  = EFFECTIVE RATE COEFFICIENTS FOR PLASMA
C                             MAGNETIC FIELD SCAN.
C                             UNITS: CM3 SEC-1
C                             DIMENSION: MXSCAN
C          (R*8)  XSECNA(,) = N-RESOLVED CHARGE EXCHANGE CROSS-SECTIONS
C                             READ FROM INPUT DATA SET.
C                             UNITS: CM2
C                             1ST DIMENSION: ENERGY INDEX
C                             2ND DIMENSION: N-SHELL
C          (R*8)  FRACLA(,) = L-RESOLVED CHARGE EXCHANGE CROSS-SECTIONS.
C                             AFTER CXDATA: ABSOLUTE VALUES (CM2).
C                             AFTER CXFRAC: FRACTION OF N-RESOLVED
C                                            DATA.
C                             1ST DIMENSION: ENERGY INDEX
C                             2ND DIMENSION: INDEXED BY I4IDFL(N,L)
C
C            (I*4)   ONE      = PARAMETER = 1  : USED AS FLAG 
C            (I*4)   ZERO     = PARAMETER = 0  : USED AS FLAG 
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          C7CXEE     ADAS      CALCULATES CHARGE EXCHANGE EFFECTIVE
C                               EMISSIVITY COEFFICIENT.
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    24TH MAY 1996
C
C VERSION: 1.1                          DATE: 24-05-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST VERSION. WRITTEN USING BATCH309.FOR
C
C VERSION:  1.2                          DATE: 08/04/99
C MODIFIED: Martin O'Mullane
C            - Added XXRAMS to set the weight of the receiver for
C              subsequent use in C6PRSL.
C
C-----------------------------------------------------------------------
      INTEGER    IUNT07       , I            , J            ,
     &           IUNT14
      INTEGER    I4UNIT
      INTEGER    LOGIC        , MXNENG       , MXNSHL       , MXSCAN
C-----------------------------------------------------------------------
      INTEGER    NBMENG  , NDENSZ  , NTIEV   , NZEFF   , NBMAG,
     &           NTU     , NTL     , ITHEOR  , IBSTAT  , IEMMS,
     &           NGRND   , NTOT    , NBOT    , NTOP    , IRZ0    ,
     &           IRZ1    , NMINF   , NMAXF   , NENRGY  , IRZ2    ,
     &           IDZ0    , IPSET
      INTEGER    ONE          , ZERO
      PARAMETER( ONE=1        , ZERO=0)
      PARAMETER( MXNENG = 30  , MXNSHL = 20  , MXSCAN = 24    )
C-----------------------------------------------------------------------
      LOGICAL    LTEXT           , LTEXAP
      LOGICAL    LPASS           , LPASAP
C-----------------------------------------------------------------------
      REAL*8     BMENGA(MXSCAN)  , DENSZA(MXSCAN)  , TIEVA(MXSCAN)   ,
     &           ZEFFA(MXSCAN)   , BMAGA(MXSCAN)   , QEFBEA(MXSCAN)  ,
     &           QEFTIA(MXSCAN)  , QEFDZA(MXSCAN)  , QEFZEA(MXSCAN)  ,
     &           QEFBMA(MXSCAN)  , ENRGYA(MXNENG)  , ALPHAA(MXNENG)  ,
     &           XSECNA(MXNENG,MXNSHL)             ,
     &           FRACLA(MXNENG,(MXNSHL*(MXNSHL+1))/2)
      REAL*8     RAMSNO  , TEV     , TIEV    , DENS    , DENSZ   ,
     &           ZEFF    , BMAG    , BMENG   , QEFREF  , ERATE   ,
     &           SCNVAL
C-----------------------------------------------------------------------
      CHARACTER  DATE*8    , DSNRUN*80  , DSNPAS*80  , CADAS*80
      CHARACTER  TITLE*40  , SYMBR*2    , SYMBD*2    , DSNIN*80
      CHARACTER  DSBATCH*80
      CHARACTER  RMASS*7  
C-----------------------------------------------------------------------
C *************************** BEGIN ************************************
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C OPEN THE BATCH FILE FOR INPUT
C-----------------------------------------------------------------------
      OPEN( UNIT = 40, FILE=DSBATCH, STATUS='UNKNOWN')

80    CONTINUE

C
C-----------------------------------------------------------------------
C ZERO SCAN ARRAYS.
C-----------------------------------------------------------------------
C
      DO 1 I = 1 , MXSCAN
         BMENGA(I) = 0.0D0
         DENSZA(I) = 0.0D0
         TIEVA(I)  = 0.0D0
         ZEFFA(I)  = 0.0D0
         BMAGA(I)  = 0.0D0
         QEFBEA(I) = 0.0D0
         QEFTIA(I) = 0.0D0
         QEFDZA(I) = 0.0D0
         QEFZEA(I) = 0.0D0
         QEFBMA(I) = 0.0D0
    1 CONTINUE
C
C-----------------------------------------------------------------------
C GET USER-DEFINED VALUES FROM FILE
C-----------------------------------------------------------------------
C
      READ(   40  , '(A)', END=9999) DSNRUN
      READ(   40  , '(A)', END=9999) DSNPAS
      READ(   40  , '(A)', END=9999) CADAS
      READ(   40  , '(A)', END=9999) TITLE
      READ(   40  , *, END=9999) LOGIC
      IF (LOGIC.EQ.1) THEN
          LTEXT = .TRUE.
      ELSE
          LTEXT = .FALSE.
      ENDIF
      READ(   40  , *, END=9999) LOGIC
      IF (LOGIC.EQ.1) THEN
          LTEXAP = .TRUE.
      ELSE
          LTEXAP = .FALSE.
      ENDIF
      READ(   40  , *, END=9999) LOGIC
      IF (LOGIC.EQ.1) THEN
          LPASS = .TRUE.
      ELSE
          LPASS = .FALSE.
      ENDIF
      READ(   40  , *, END=9999) LOGIC
      IF (LOGIC.EQ.1) THEN
          LPASAP = .TRUE.
      ELSE
          LPASAP = .FALSE.
      ENDIF
      READ(   40  , *, END=9999) NBMENG
      READ(   40  , *, END=9999) NDENSZ
      READ(   40  , *, END=9999) NTIEV
      READ(   40  , *, END=9999) NZEFF
      READ(   40  , *, END=9999) NBMAG
      READ( 40, *) (BMENGA(I), I=1, NBMENG)
      READ( 40, *) (DENSZA(I), I=1, NDENSZ)
      READ( 40, *) (TIEVA(I), I=1, NTIEV)
      READ( 40, *) (ZEFFA(I), I=1, NZEFF)
      READ( 40, *) (BMAGA(I), I=1, NBMAG)
      READ( 40, *) (ENRGYA(I), I=1, MXNENG)
      READ( 40, *) (ALPHAA(I), I=1, MXNENG)
C     DO 108, I = 1, MXNENG
          DO 109, J = 1, MXNSHL
              READ(   40  , *) (XSECNA(I,J), I=1, MXNENG)
109       CONTINUE
C108   CONTINUE
C     DO 110, I = 1, MXNENG
          DO 111, J = 1, (MXNSHL*(MXNSHL+1))/2
          READ(   40  , *) (FRACLA(I,J), I=1, MXNENG)
111       CONTINUE
C110   CONTINUE
      READ(   40  , *) NTU
      READ(   40  , *) NTL
      READ(   40  , *) ITHEOR
      READ(   40  , *) IEMMS
      READ(   40  , *) IBSTAT
      READ(   40  , *) RAMSNO
      READ(   40  , *) TIEV
      READ(   40  , *) TEV
      READ(   40  , *) DENSZ
      READ(   40  , *) DENS
      READ(   40  , *) ZEFF
      READ(   40  , *) BMAG
      READ(   40  , *) BMENG
      READ(   40  , *) IDZ0
      READ(   40  , *) IRZ0
      READ(   40  , *) IRZ1
      READ(   40  , *) IRZ2
      READ(   40  , *) NMINF
      READ(   40  , *) NMAXF
      READ(   40  , *) NENRGY
      READ(   40  , '(A)') DSNIN
      READ(   40  , '(A)') DATE
      READ(   40  , '(A)') SYMBR
      READ(   40  , '(A)') SYMBD
      READ(   40  , *) IPSET

      NTOT = MXNSHL
      NTOP = MXNSHL
      NGRND = 1
      NBOT = 2

C-----------------------------------------------------------------------
C SET THE RECEIVER ATOMIC MASS.
C-----------------------------------------------------------------------
      
      WRITE(RMASS,'(F7.3)')RAMSNO
      CALL XXRAMS(RMASS)

C-----------------------------------------------------------------------
C ESTABLISH REFERENCE VALUE FOR EFFECTIVE COEFFICIENT.
C-----------------------------------------------------------------------
C
      CALL C7CXEE( MXNENG , MXNSHL , NGRND  , NTOT   , NBOT   ,
     &             NTOP   , IRZ0   , IRZ1   , RAMSNO , TEV    ,
     &             TIEV   , DENS   , DENSZ  , ZEFF   , BMAG   ,
     &             BMENG  , ITHEOR , IBSTAT , IEMMS  , NTU    ,
     &             NTL    , NMINF  , NMAXF  , NENRGY , ENRGYA ,
     &             ALPHAA , XSECNA , FRACLA , ERATE        )
      QEFREF = ERATE
C
C-----------------------------------------------------------------------
C ENERGY SCAN.
C-----------------------------------------------------------------------
C
      DO 2 I = 1 , NBMENG
         SCNVAL = BMENGA(I)
         CALL C7CXEE( MXNENG , MXNSHL , NGRND  , NTOT   , NBOT   ,
     &                NTOP   , IRZ0   , IRZ1   , RAMSNO , TEV    ,
     &                TIEV   , DENS   , DENSZ  , ZEFF   , BMAG   ,
     &                SCNVAL , ITHEOR , IBSTAT , IEMMS  , NTU    ,
     &                NTL    , NMINF  , NMAXF  , NENRGY , ENRGYA ,
     &                ALPHAA , XSECNA , FRACLA , ERATE             )
         QEFBEA(I) = ERATE
    2 CONTINUE
C
C-----------------------------------------------------------------------
C ION TEMPERATURE SCAN.
C-----------------------------------------------------------------------
C
      DO 3 I = 1 , NTIEV
         SCNVAL = TIEVA(I)
         CALL C7CXEE( MXNENG , MXNSHL , NGRND  , NTOT   , NBOT   ,
     &                NTOP   , IRZ0   , IRZ1   , RAMSNO , TEV    ,
     &                SCNVAL , DENS   , DENSZ  , ZEFF   , BMAG   ,
     &                BMENG  , ITHEOR , IBSTAT , IEMMS  , NTU    ,
     &                NTL    , NMINF  , NMAXF  , NENRGY , ENRGYA ,
     &                ALPHAA , XSECNA , FRACLA , ERATE             )
         QEFTIA(I) = ERATE
    3 CONTINUE
C
C-----------------------------------------------------------------------
C ION DENSITY SCAN.
C-----------------------------------------------------------------------
C
      DO 4 I = 1 , NDENSZ
         SCNVAL = DENSZA(I)
         CALL C7CXEE( MXNENG , MXNSHL , NGRND  , NTOT   , NBOT   ,
     &                NTOP   , IRZ0   , IRZ1   , RAMSNO , TEV    ,
     &                TIEV   , DENS   , SCNVAL , ZEFF   , BMAG   ,
     &                BMENG  , ITHEOR , IBSTAT , IEMMS  , NTU    ,
     &                NTL    , NMINF  , NMAXF  , NENRGY , ENRGYA ,
     &                ALPHAA , XSECNA , FRACLA , ERATE             )
         QEFDZA(I) = ERATE
    4 CONTINUE
C
C-----------------------------------------------------------------------
C PLASMA EFFECTIVE Z SCAN.
C-----------------------------------------------------------------------
C
      DO 5 I = 1 , NZEFF
         SCNVAL = ZEFFA(I)
         CALL C7CXEE( MXNENG , MXNSHL , NGRND  , NTOT   , NBOT   ,
     &                NTOP   , IRZ0   , IRZ1   , RAMSNO , TEV    ,
     &                TIEV   , DENS   , DENSZ  , SCNVAL , BMAG   ,
     &                BMENG  , ITHEOR , IBSTAT , IEMMS  , NTU    ,
     &                NTL    , NMINF  , NMAXF  , NENRGY , ENRGYA ,
     &                ALPHAA , XSECNA , FRACLA , ERATE             )
         QEFZEA(I) = ERATE
    5 CONTINUE
C
C-----------------------------------------------------------------------
C PLASMA MAGNETIC INDUCTION SCAN.
C-----------------------------------------------------------------------
C
      DO 6 I = 1 , NBMAG
         SCNVAL = BMAGA(I)
         CALL C7CXEE( MXNENG , MXNSHL , NGRND  , NTOT   , NBOT   ,
     &                NTOP   , IRZ0   , IRZ1   , RAMSNO , TEV    ,
     &                TIEV   , DENS   , DENSZ  , ZEFF   , SCNVAL ,
     &                BMENG  , ITHEOR , IBSTAT , IEMMS  , NTU    ,
     &                NTL    , NMINF  , NMAXF  , NENRGY , ENRGYA ,
     &                ALPHAA , XSECNA , FRACLA , ERATE             )
         QEFBMA(I) = ERATE
    6 CONTINUE
C
C-----------------------------------------------------------------------
C OPEN OUTPUT FILES
C-----------------------------------------------------------------------
C
      OPEN( UNIT = IUNT14 , FILE = DSNPAS , STATUS = 'UNKNOWN')
C
C-----------------------------------------------------------------------
C TEXT FILE OUTPUT.
C-----------------------------------------------------------------------
C
      IF (LTEXT) THEN
          OPEN( UNIT = IUNT07,  FILE = DSNRUN , STATUS = 'UNKNOWN')
          IF (LTEXAP) THEN
7             READ( IUNT07, *, END=8)
              GOTO 7
          ENDIF
8         CONTINUE
          CALL C7OUT0( IUNT07 , MXSCAN , DATE   , DSNIN ,  DSNPAS ,
     &                 IPSET  , TITLE  , SYMBD  , IDZ0   , SYMBR  ,
     &                 IRZ0   , IRZ1   , IRZ2   , RAMSNO , TEV    ,
     &                 TIEV   , DENS   , DENSZ  , ZEFF   , BMAG   ,
     &                 BMENG  , NTU    , NTL    , ITHEOR , IBSTAT ,
     &                 IEMMS  , NBMENG , BMENGA , NTIEV  , TIEVA  ,
     &                 NDENSZ , DENSZA , NZEFF  , ZEFFA  , NBMAG  ,
     &                 BMAGA  , CADAS                               )
      ENDIF
C
C-----------------------------------------------------------------------
C PASSING FILE OUTPUT.
C-----------------------------------------------------------------------
C
      IF (LPASS) THEN
          OPEN( UNIT = IUNT14 , FILE = DSNPAS , STATUS = 'UNKNOWN')
          IF (LPASAP) THEN
9             READ( IUNT14, *, END = 10)
              GOTO 9
          ENDIF
10        CONTINUE
          CALL C7OUT1( IUNT14 , MXSCAN , DATE   , DSNIN , IPSET   ,
     &                 SYMBD  , SYMBR  , IRZ1   , NTU    , NTL    ,
     &                 ITHEOR , IBSTAT , IEMMS  , BMENG  , TIEV   ,
     &                 DENSZ  , ZEFF   , BMAG   , QEFREF , NBMENG ,
     &                 BMENGA , QEFBEA , NTIEV  , TIEVA  , QEFTIA ,
     &                 NDENSZ , DENSZA , QEFDZA , NZEFF  , ZEFFA  ,
     &                 QEFZEA , NBMAG  , BMAGA  , QEFBMA            )
      ENDIF
C
C-----------------------------------------------------------------------
C GO BACK FOR MORE IF WE HAVEN'T REACHED THE END
C-----------------------------------------------------------------------
C
      GOTO 80
C
C-----------------------------------------------------------------------
C REACHED END - TERMINATE
C-----------------------------------------------------------------------
C

9999  CONTINUE

      IF (LPASS) THEN
          CLOSE( IUNT14 )
      ENDIF
      IF (LTEXT) THEN
          CLOSE( IUNT07 )
      ENDIF

      WRITE(I4UNIT(-1),*)"*** YOUR BATCH JOB ***"
      WRITE(I4UNIT(-1),*) DSBATCH
      WRITE(I4UNIT(-1),*)"*** HAS SUCCESSFULLY COMPLETED ***"


      RETURN

      END
