CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas307/c7out1.for,v 1.1 2004/07/06 11:54:30 whitefor Exp $ Date $Date: 2004/07/06 11:54:30 $
CX
      SUBROUTINE C7OUT1( IWRITE , MXSCAN , DATE   , DSNIN  ,
     &                   IPSET  , SYMBD  , SYMBR  , IRZ1   ,
     &                   NTU    , NTL    , ITHEOR , IBSTAT ,
     &                   IEMMS  , BMENG  , TIEV   , DENSZ  ,
     &                   ZEFF   , BMAG   , QEFREF , NBMENG ,
     &                   BMENGA , QEFBEA , NTIEV  , TIEVA  ,
     &                   QEFTIA , NDENSZ , DENSZA , QEFDZA ,
     &                   NZEFF  , ZEFFA  , QEFZEA , NBMAG  ,
     &                   BMAGA  , QEFBMA
     &                 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C7OUT1 *********************
C
C  PURPOSE: WRITES DATA OUTPUT TO FILE FOR ADAS307.
C
C  CALLING PROGRAM: ADAS307
C
C  INPUT : (I*4)  IWRITE    = UNIT NUMBER FOR OUTPUT.
C  INPUT : (I*4)  MXSCAN    = MAXIMUM NUMBER OF VALUES IN A SCAN.
C  INPUT : (C*8)  DATE      = DATE STRING.
C  INPUT : (C*80) DSNIN     = FULL INPUT DATA SET NAME.
C  INPUT : (I*4)  IPSET     = INPUT PARAMETER SET NUMBER FOR CURRENT
C                             RUN.
C  INPUT : (C*2)  SYMBD     = DONOR ELEMENT SYMBOL.
C  INPUT : (C*2)  SYMBR     = RECEIVER ELEMENT SYMBOL.
C  INPUT : (I*4)  IRZ1      = ION CHARGE OF RECEIVER.
C  INPUT : (I*4)  NTU       = UPPER PRINCIPAL QUANTUM NUMBER OF
C                             TRANSITION.
C  INPUT : (I*4)  NTL       = LOWER PRINCIPAL QUANTUM NUMBER OF
C                             TRANSITION.
C  INPUT : (I*4)  ITHEOR    = CHARGE EXCHANGE MODEL OPTION.
C                             1 => USE INPUT DATA SET.
C                             2 => USE EIKONAL MODEL.
C  INPUT : (I*4)  IBSTAT    = DONOR STATE FOR EIKONAL MODEL.
C                             1 => H(1S)
C                             2 => H(2S)
C                             3 => H(2P)
C                             4 => HE(1S2)
C                             5 => HE(1S2S)
C  INPUT : (I*4)  IEMMS     = EMISSION MEASURE MODEL OPTION.
C                             1 => CHARGE EXCHANGE.
C                             2 => ELECTRON IMPACT EXCITATION.
C                             3 => RADIATIVE RECOMBINATION.
C  INPUT : (R*8)  BMENG     = REFERNCE VALUE FOR BEAM ENERGY.
C                             UNITS: EV/AMU
C  INPUT : (R*8)  TIEV      = REFERENCE VALUE FOR ION TEMPERATURE.
C                             UNITS: EV
C  INPUT : (R*8)  DENSZ     = REFERENCE VALUE FOR PLASMA ION DENSITY.
C                             UNITS: CM-3
C  INPUT : (R*8)  ZEFF      = REFERENCE VALUE FOR EFFECTIVE ION CHARGE.
C  INPUT : (R*8)  BMAG      = REFERENCE VALUE FOR PLASMA MAGNETIC
C                             FIELD.
C                             UNITS: TESLA
C  INPUT : (R*8)  QEFREF    = REFERENCE VALUE FOR EFFECTIVE RATE COEFFT.
C                             UNITS: CM3 SEC-1
C  INPUT : (I*4)  NBMENG    = NUMBER OF BEAM ENERGIES IN SCAN.
C  INPUT : (R*8)  BMENGA()  = BEAM ENERGIES FOR SCAN.
C                             UNITS: EV/AMU
C                             DIMENSION: MXSCAN
C  INPUT : (R*8)  QEFBEA()  = EFFECTIVE RATE COEFFICIENTS FOR BEAM
C                             ENERGY SCAN.
C                             UNITS: CM3 SEC-1
C                             DIMENSION: MXSCAN
C  INPUT : (I*4)  NTIEV     = NUMBER OF ION TEMPERATURES IN SCAN.
C  INPUT : (R*8)  TIEVA()   = ION TEMPERATURES FOR SCAN.
C                             UNITS: EV
C                             DIMENSION: MXSCAN
C  INPUT : (R*8)  QEFTIA()  = EFFECTIVE RATE COEFFICIENTS FOR ION
C                             TEMPERATURE SCAN.
C                             UNITS: CM3 SEC-1
C                             DIMENSION: MXSCAN
C  INPUT : (I*4)  NDENSZ    = NUMBER OF ION DENSZTIES IN SCAN.
C  INPUT : (R*8)  DENSZA()  = ION DENSZTIES FOR SCAN.
C                             UNITS: CM-3
C                             DIMENSION: MXSCAN
C  INPUT : (R*8)  QEFDZA()  = EFFECTIVE RATE COEFFICIENTS FOR ION
C                             DENSITY SCAN.
C                             UNITS: CM3 SEC-1
C                             DIMENSION: MXSCAN
C  INPUT : (I*4)  NZEFF     = NUMBER OF PLASMA EFFECTIVE Z IN SCAN.
C  INPUT : (R*8)  ZEFFA()   = PLASMA EFFECTIVE Z FOR SCAN.
C                             DIMENSION: MXSCAN
C  INPUT : (R*8)  QEFZEA()  = EFFECTIVE RATE COEFFICIENTS FOR PLASMA
C                             EFFECTIVE Z SCAN.
C                             UNITS: CM3 SEC-1
C                             DIMENSION: MXSCAN
C  INPUT : (I*4)  NBMAG     = NUMBER OF PLASMA MAGNETIC FIELDS IN SCAN.
C  INPUT : (R*8)  BMAGA()   = PLASMA MAGNETIC FIELDS FOR SCAN.
C                             UNITS: TESLA
C                             DIMENSION: MXSCAN
C  INPUT : (R*8)  QEFBMA()  = EFFECTIVE RATE COEFFICIENTS FOR PLASMA
C                             MAGNETIC FIELD SCAN.
C                             UNITS: CM3 SEC-1
C                             DIMENSION: MXSCAN
C
C          (I*4)  I         = ARRAY INDEX.
C          (I*4)  I1        = ARRAY INDEX.
C          (I*4)  I2        = ARRAY INDEX.
C
C          (C*8)  DONOR     = DONOR SYMBOL AND ION CHARGE (OR STATE IF
C                             EIKONAL MODEL.
C          (C*5)  RECVR     = RECEIVER SYMBOL AND ION CHARGE.
C          (C*5)  TRANS     = N LEVELS OF TRANSITION.
C          (C*7)  FILE      = INPUT DATA SET MEMBER NAME.
C          (C*2)  MODEL     = EMISSION MEASURE MODEL.
C
C ROUTINES:  NONE
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/87
C          JET EXT. 5183
C
C DATE:    01/12/93
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    24TH MAY 1996
C
C VERSION: 1.1                          DATE: 24-05-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST VERSION
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    IWRITE  , MXSCAN  , IRZ1    , NTU     ,
     &           NTL     , ITHEOR  , IBSTAT  , IEMMS   , IPSET   ,
     &           NBMENG  , NTIEV   , NDENSZ  , NZEFF   , NBMAG
      INTEGER    I       , I1      , I2
C-----------------------------------------------------------------------
      REAL*8     BMENG   , TIEV    , DENSZ   , ZEFF    , BMAG    ,
     &           QEFREF
C-----------------------------------------------------------------------
      CHARACTER  DATE*8     , DSNIN*80   , SYMBD*2    , SYMBR*2
      CHARACTER  DONOR*8    , RECVR*5    , TRANS*5    , FILE*7     ,
     &           MODEL*2
C-----------------------------------------------------------------------
      REAL*8     BMENGA(MXSCAN)  , DENSZA(MXSCAN)  , TIEVA(MXSCAN)   ,
     &           ZEFFA(MXSCAN)   , BMAGA(MXSCAN)   , QEFBEA(MXSCAN)  ,
     &           QEFTIA(MXSCAN)  , QEFDZA(MXSCAN)  , QEFZEA(MXSCAN)  ,
     &           QEFBMA(MXSCAN)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C SET UP DONOR STRING.
C-----------------------------------------------------------------------
C
      IF ( ITHEOR .EQ. 1 ) THEN
         DONOR = '        '
         IF ( SYMBD(2:2) .EQ. ' ' ) THEN
            DONOR = SYMBD(1:1) // '(1S)   '
         ELSE
            DONOR = SYMBD // '(1S)  '
         ENDIF
      ELSE
         IF ( IBSTAT .EQ. 1 ) THEN
            DONOR = 'H(1S)   '
         ELSE IF ( IBSTAT .EQ. 2 ) THEN
            DONOR = 'H(2S)   '
         ELSE IF ( IBSTAT .EQ. 3 ) THEN
            DONOR = 'H(2P)   '
         ELSE IF ( IBSTAT .EQ. 4 ) THEN
            DONOR = 'HE(1S2) '
         ELSE IF ( IBSTAT .EQ. 5 ) THEN
            DONOR = 'HE(1S2S)'
         ENDIF
      ENDIF
C
C-----------------------------------------------------------------------
C SET UP RECEIVER STRING.
C-----------------------------------------------------------------------
C
      IF ( SYMBR(2:2) .EQ. ' ' ) THEN
         IF ( IRZ1 .LT. 10 ) THEN
            WRITE(RECVR,1001) SYMBR(1:1) , IRZ1
         ELSE
            WRITE(RECVR,1002) SYMBR(1:1) , IRZ1
         ENDIF
      ELSE
         IF ( IRZ1 .LT. 10 ) THEN
            WRITE(RECVR,1003) SYMBR , IRZ1
         ELSE
            WRITE(RECVR,1004) SYMBR , IRZ1
         ENDIF
      ENDIF
C
C-----------------------------------------------------------------------
C SET UP TRANSITION STRING.
C-----------------------------------------------------------------------
C
      IF ( NTL .LT. 10 ) THEN
         WRITE(TRANS,1005) NTU , NTL
      ELSE
         WRITE(TRANS,1006) NTU , NTL
      ENDIF
C
C-----------------------------------------------------------------------
C SET UP FILE NAME STRING.
C-----------------------------------------------------------------------
C
      IF ( ITHEOR .EQ. 1 ) THEN
         FILE = '        '
CX THIS HAS BEEN CHANGED TO LOOK FOR UNIX-STYLE FILENAMES
         I1 = INDEX( DSNIN , '_' ) + 1
         I2 = INDEX( DSNIN , '.dat' ) - 1
         FILE = DSNIN(I1:I2)
      ELSE
         FILE = 'EIKONAL '
      ENDIF
C
C-----------------------------------------------------------------------
C SET UP MODEL STRING.
C-----------------------------------------------------------------------
C
      IF ( IEMMS .EQ. 1 ) THEN
         MODEL = 'CX'
      ELSE IF ( IEMMS .EQ. 2 ) THEN
         MODEL = 'EX'
      ELSE IF ( IEMMS .EQ. 3 ) THEN
         MODEL = 'RD'
      ENDIF
C
C-----------------------------------------------------------------------
C WRITE HEADER STRING.
C-----------------------------------------------------------------------
C
      WRITE(IWRITE,1007) DATE , DONOR , RECVR , TRANS , FILE , MODEL ,
     &                   IPSET
C
C-----------------------------------------------------------------------
C WRITE EFFECTIVE RATE REFERENCE VALUE AND PARAMETERS.
C-----------------------------------------------------------------------
C
      WRITE(IWRITE,1008) QEFREF
      WRITE(IWRITE,1009) BMENG , TIEV , DENSZ , ZEFF , BMAG
C
C-----------------------------------------------------------------------
C WRITE TABLES GENERATED BY SCANS.
C-----------------------------------------------------------------------
C
      WRITE(IWRITE,1010) NBMENG , NTIEV , NDENSZ , NZEFF , NBMAG
      WRITE(IWRITE,1011) ( BMENGA(I) , I=1,24 )
      WRITE(IWRITE,1012) ( QEFBEA(I) , I=1,24 )
      WRITE(IWRITE,1013) ( TIEVA(I) , I=1,12 )
      WRITE(IWRITE,1014) ( QEFTIA(I) , I=1,12 )
      WRITE(IWRITE,1015) ( DENSZA(I) , I=1,24 )
      WRITE(IWRITE,1016) ( QEFDZA(I) , I=1,24 )
      WRITE(IWRITE,1017) ( ZEFFA(I) , I=1,12 )
      WRITE(IWRITE,1018) ( QEFZEA(I) , I=1,12 )
      WRITE(IWRITE,1019) ( BMAGA(I) , I=1,12 )
      WRITE(IWRITE,1020) ( QEFBMA(I) , I=1,12 )
C
C-----------------------------------------------------------------------
C
 1001 FORMAT( ' ' , A1 , '+' , I1 , ' ' )
 1002 FORMAT( ' ' , A1 , '+' , I2 )
 1003 FORMAT( A2 , '+' , I1 , ' ' )
 1004 FORMAT( A2 , '+' , I2 )
 1005 FORMAT( I2 , '-' , I1 , ' ' )
 1006 FORMAT( I2 , '-' , I2 )
 1007 FORMAT( 'ADAS307 ' , A8 , ' D=' , A8 , ' R=' , A5 , ' N=' ,
     &        A5 , ' F=' , A7 , ' M=' , A2 , '     ISEL=' , I2   )
 1008 FORMAT( 1P , 1D10.2 , 50X , '   QEFREF' )
 1009 FORMAT( 1P , 5D10.2 , 10X , '   PARMREF' )
 1010 FORMAT( 5I10 , 10X , '   NPARMSC' )
 1011 FORMAT( 1P , 6D10.2 , '   ENER   ' , 3( / 6D10.2 ) )
 1012 FORMAT( 1P , 6D10.2 , '   QENER  ' , 3( / 6D10.2 ) )
 1013 FORMAT( 1P , 6D10.2 , '   TIEV   ' , / 6D10.2 )
 1014 FORMAT( 1P , 6D10.2 , '   QTIEV  ' , / 6D10.2 )
 1015 FORMAT( 1P , 6D10.2 , '   DENSI  ' , 3( / 6D10.2 ) )
 1016 FORMAT( 1P , 6D10.2 , '   QDENSI ' , 3( / 6D10.2 ) )
 1017 FORMAT( 1P , 6D10.2 , '   ZEFF   ' , / 6D10.2 )
 1018 FORMAT( 1P , 6D10.2 , '   QZEFF  ' , / 6D10.2 )
 1019 FORMAT( 1P , 6D10.2 , '   BMAG   ' , / 6D10.2 )
 1020 FORMAT( 1P , 6D10.2 , '   QBMAG  ' , / 6D10.2 )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
