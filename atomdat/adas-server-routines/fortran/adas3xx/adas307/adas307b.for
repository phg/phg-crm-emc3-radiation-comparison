CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas307/adas307b.for,v 1.1 2004/07/06 10:31:20 whitefor Exp $ Date $Date: 2004/07/06 10:31:20 $
CX
      PROGRAM ADAS307B

      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 PROGRAM: ADAS307B **********************
C
C  CALLING PROGRAM: SPAWNED BY IDL ROUTINE   ADAS.PRO
C
C  PURPOSE:  THIS PROGRAM IS THE MAIN CONTROLLER OF THE SECOND PART OF
C            THE ADAS307 ROUTINE. THE FIRST PART HANDLES ALL THE INPUT
C            OF USER DATA, CHECKS IT AND GETS THE NAME(S) OF THE OUTPUT
C            FILE(S). IT ALSO GETS INFORMATION ON WHETHER THE ACTUAL
C            SCANNING CALCULATION(S) ARE TO BE DONE IN REAL TIME OR
C            SUBMITTED AS A BATCH JOB. WHATEVER THE OUTCOME, THE IDL
C            AT SOME POINT SPAWNS THIS PROGRAM IN ONE OF TWO POSSIBLE WAYS.
C            1 - INTERACTIVELY, THE PARAMETER LBATCH IS .FALSE. AND SO
C                THE CALCULATION OF THE SCANS AND OUTPUT OF DATA IS
C                PERFORMED WHILST THE USER WAITS. THIS INVOLVES SOME
C                PIPE COMMUNICATIONS WITH IDL. IN THIS CASE THE MAIN
C                SUBROUTINE CALLED IS RUN209.FOR.
C            2 - RUNNING IN BATCH. HERE THE IDL HAS BUILT UP A FILE OF
C                RUNS TO BE PERFORMED IN BATCH. THE CONSTRUCTION OF THE
C                'BATCH' FILES IS ENTIRELY AN IDL PROCESS AND NO PIPE
C                COMMUNICATIONS ARE NEEDED. WHEN THE USER EXITS THE 
C                ADAS307 PROCESSING OPTIONS SCREEN FOR THE FINAL TIME THEN
C                THE IDL SPAWNS THIS PROGRAM WITH THE PARAMETER LBATCH
C                SET TO TRUE. ALL INPUTS WHICH WOULD HAVE COME FROM IDL
C                ARE INSTEAD READ FROM A SET OF BATCH FILES.
C		 THE BATCH VERSION OF THE PROGRAM IS SET TO RUN LATER
C                USING THE UNIX 'at'  COMMAND - SEE WRITTEN NOTES
C                FOR MORE INFORMATION.
C
C  PROGRAM:
C
C  PARAM : (I*4)  IUNT07    = OUTPUT UNIT FOR RUN SUMMARY.
C  PARAM : (I*4)  IUNT14    = OUTPUT UNIT FOR PASSING FILE.
C
C          (L*4)  LBATCH    = FLAG FOR BATCH JOB OR NOT (TRUE = BATCH)
C          (L*4)  LTEXT     = FLAG FOR SUMMARY OUTPUT (TRUE = SUMMARY)
C          (L*4)  LTEXAP    = FLAG FOR APPENDING SUMMARY OUTPUT
C                             (TRUE = APPEND)
C	   (L*4)  LPASS     = FLAG FOR PASS FILE OUTPUT (TRUE = PASS FILE)
C          (L*4)  LPASAP    = FLAG FOR APPENDING PASS FILE OUTPUT
C                             (TRUE = APPEND)
C
C          (C*40) TITLE     = ISPF ENTERED GENERAL TITLE FOR RUN.
C          (C*80) DSNRUN    = RUN SUMMARY FILE DATA SET NAME.
C          (C*80) DSNPAS    = PASSING FILE DATA SET NAME.
C          (C*80) CADAS     = ADAS HEADER: INCLUDES RELEASE, PROGRAM,
C                             TIME.
C          (C*80) DSBATCH   = THE NAME OF THE BATCH FILE TO READ
C
C            (I*4)   PIPEIN   = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C            (I*4)   PIPEOU   = PARAMETER = UNIT NUMBER FOR INPUT FROM PIPE
C            (I*4)   ONE      = PARAMETER = 1  : USED AS FLAG TO IDL
C            (I*4)   ZERO     = PARAMETER = 0  : USED AS FLAG TO IDL
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          XX0000     ADAS      SETS MACHINE DEPENDENT ADAS
C                               CONFIGURATION.
C	   RUN307    IDL_ADAS   RUNS AN INTERACTIVE VERSION OF ADAS307
C	   BATCH307  IDL_ADAS   RUNS A BATCH VERSION OF ADAS307
C
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    24TH MAY 1996
C
C VERSION: 1.1                          DATE: 24-05-96
C MODIFIED: WILLIAM OSBORN
C               - COPIED FROM ADAS307B.FOR, WRITTEN BY TIM HAMMOND
C
C
C-----------------------------------------------------------------------
      INTEGER    IUNT07       ,
     &           IUNT14       
C      INTEGER    I4UNIT
      INTEGER    LOGIC
C     PARAMETER( IUNT07 = 17  ,
C    &           IUNT14 = 14  )
C-----------------------------------------------------------------------
      INTEGER    PIPEIN  , PIPEOU  , ONE     , ZERO
      PARAMETER( PIPEIN=5      , PIPEOU=6    , ONE=1       , ZERO=0)
C-----------------------------------------------------------------------
      LOGICAL    LBATCH    , LTEXT      , LTEXAP
      LOGICAL    LPASS     , LPASAP
C-----------------------------------------------------------------------
      CHARACTER  DSNRUN*80  , DSNPAS*80  , CADAS*80
      CHARACTER  TITLE*40  , DSBATCH*80
C-----------------------------------------------------------------------
C *************************** MAIN PROGRAM ****************************
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C SET MACHINE DEPENDENT ADAS CONFIGURATION VALUES.
C-----------------------------------------------------------------------
C
      CALL XX0000
C
C-----------------------------------------------------------------------
C GET PARAMETER LBATCH (IS THIS A BATCH RUN OR NOT?) FROM IDL
C-----------------------------------------------------------------------
C
      READ( PIPEIN, *) LOGIC
      IF (LOGIC.EQ.0) THEN
          LBATCH = .FALSE.

C
C-----------------------------------------------------------------------
C GET ALL RELEVANT FILENAMES FROM IDL
C-----------------------------------------------------------------------
C
          READ( PIPEIN, '(A)' ) DSNRUN
          READ( PIPEIN, '(A)' ) DSNPAS
C
C-----------------------------------------------------------------------
C GET ANY OTHER INFORMATION FROM IDL
C-----------------------------------------------------------------------
C
          READ( PIPEIN, '(A)' ) CADAS
          READ( PIPEIN, '(A)' ) TITLE
          READ( PIPEIN, *) LOGIC
          IF (LOGIC.EQ.1) THEN
              LTEXT = .TRUE.
          ELSE
              LTEXT = .FALSE.
          ENDIF
          READ( PIPEIN, *) LOGIC
          IF (LOGIC.EQ.1) THEN
              LTEXAP = .TRUE.
          ELSE
              LTEXAP = .FALSE.
          ENDIF
          READ( PIPEIN, *) LOGIC
          IF (LOGIC.EQ.1) THEN
              LPASS = .TRUE.
          ELSE
              LPASS = .FALSE.
          ENDIF
          READ( PIPEIN, *) LOGIC
          IF (LOGIC.EQ.1) THEN
              LPASAP = .TRUE.
          ELSE
                  LPASAP = .FALSE.
          ENDIF
    
      ELSE
C
C-----------------------------------------------------------------------
C  IT IS A BATCH RUN
C-----------------------------------------------------------------------
C

          LBATCH = .TRUE.
          READ( PIPEIN, '(A)') DSBATCH
      ENDIF
C
C-----------------------------------------------------------------------
C  READ IN UNIT NUMBERS TO USE FOR OUTPUT
C-----------------------------------------------------------------------
C

      READ( PIPEIN, *) IUNT07
      READ( PIPEIN, *) IUNT14
C
C-----------------------------------------------------------------------
C
      IF (LBATCH) THEN
          CALL BATCH307(IUNT07, IUNT14, DSBATCH)
      ELSE 
          CALL RUN307(IUNT07, IUNT14, DSNRUN, DSNPAS,
     &                 CADAS, TITLE, LTEXT, LTEXAP, LPASS, LPASAP )
      ENDIF
C
C-----------------------------------------------------------------------
C
      STOP

      END
