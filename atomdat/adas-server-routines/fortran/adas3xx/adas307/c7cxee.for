CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas307/c7cxee.for,v 1.1 2004/07/06 11:54:08 whitefor Exp $ Date $Date: 2004/07/06 11:54:08 $
CX
      SUBROUTINE C7CXEE( MXNENG , MXNSHL , NGRND  , NTOT   ,
     &                   NBOT   , NTOP   , IRZ0   , IRZ1   ,
     &                   RAMSNO , TEV    , TIEV   , DENS   ,
     &                   DENSZ  , ZEFF   , BMAG   , BMENG  ,
     &                   ITHEOR , IBSTAT , IEMMS  , NTU    ,
     &                   NTL    , NMINF  , NMAXF  , NENRGY ,
     &                   ENRGYA , ALPHAA , XSECNA , FRACLA ,
     &                   ERATE
     &                 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C7CXEE *********************
C
C  PURPOSE:  CALCULATES THE J-RESOLVED EFFECTIVE EMISSIVITY RATE
C            COEFFICIENT FOR THE GIVEN TRANSITION.
C
C            IT IS APPLICABLE TO IMPURITIES IN PLASMA TRAVERSED BY
C            NEUTRAL BEAMS OF H OR HE. THE RECOMBINED TARGET ION MAY BE
C            H, LI OR NA-LIKE.
C
C            THE MODEL INCLUDES CAPTURE, N-N' LEVEL CASCADE, AND MIXING
C            AMONG L,J LEVELS OF SAME N BY COLLISIONS OR MAGNETIC
C            FIELDS.
C
C            ELECTRON IMPACT IONISATION IS INCLUDED TO GIVE COLLISION
C            LIMIT EFFECT.
C
C            AN INTERNAL EIKONAL APPROXIMATION IS USED FOR CAPTURE FROM
C            EXCITED H OR HE STATES, ALTHOUGH NORMALLY THE EXTERNAL DATA
C            SET SHOULD BE USED.
C
C  CALLING PROGRAM: ADAS307
C
C  INPUT : (I*4)  MXNENG    = MAXIMUM NO. OF ENERGIES IN DATA SET.
C  INPUT : (I*4)  MXNSHL    = MAXIMUM NUMBER OF N SHELLS.
C  INPUT : (I*4)  NGRND     = PRINCIPAL QUANTUM NUMBER OF GROUND STATE.
C  INPUT : (I*4)  NTOT      = PRINCIPAL QUANTUM NUMBER OF HIGHEST BOUND
C                             STATE.
C  INPUT : (I*4)  NBOT      = MINIMUM PRINCIPAL QUANTUM NUMBER FOR
C                             RATE TABLES.
C  INPUT : (I*4)  NTOP      = MAXIMUM PRINCIPAL QUANTUM NUMBER FOR
C                             RATE TABLES.
C  INPUT : (I*4)  IRZ0      = RECEIVER NUCLEAR CHARGE.
C  INPUT : (I*4)  IRZ1      = RECEIVER ION INITIAL CHARGE.
C  INPUT : (R*8)  RAMSNO    = RECEIVER ATOMIC MASS.
C  INPUT : (R*8)  TEV       = ELECTRON TEMPERATURE.
C                             UNITS: EV
C  INPUT : (R*8)  TIEV      = ION TEMPERATURE.
C                             UNITS: EV
C  INPUT : (R*8)  DENS      = ELECTRON DENSITY.
C                             UNITS: CM-3
C  INPUT : (R*8)  DENSZ     = PLASMA ION DENSITY.
C                             UNITS: CM-3
C  INPUT : (R*8)  ZEFF      = EFFECTIVE ION CHARGE.
C  INPUT : (R*8)  BMAG      = PLASMA MAGNETIC INDUCTION.
C                             UNITS: TESLA
C  INPUT : (R*8)  BMENG     = BEAM ENERGY.
C                             UNITS: EV/AMU
C  INPUT : (I*4)  ITHEOR    = CHARGE EXCHANGE MODEL OPTION.
C                             1 => USE INPUT DATA SET.
C                             2 => USE EIKONAL MODEL.
C  INPUT : (I*4)  IBSTAT    = DONOR STATE FOR EIKONAL MODEL.
C                             1 => H(1S)
C                             2 => H(2S)
C                             3 => H(2P)
C                             4 => HE(1S2)
C                             5 => HE(1S2S)
C  INPUT : (I*4)  IEMMS     = EMISSION MEASURE MODEL OPTION.
C                             1 => CHARGE EXCHANGE.
C                             2 => ELECTRON IMPACT EXCITATION.
C                             3 => RADIATIVE RECOMBINATION.
C  INPUT : (I*4)  NTL       = LOWER PRINCIPAL QUANTUM NUMBER OF
C                             TRANSITION.
C  INPUT : (I*4)  NTU       = UPPER PRINCIPAL QUANTUM NUMBER OF
C                             TRANSITION.
C  INPUT : (I*4)  NMINF     = LOWEST N-SHELL FOR WHICH DATA READ.
C  INPUT : (I*4)  NMAXF     = HIGHEST N-SHELL FOR WHICH DATA READ.
C  INPUT : (I*4)  NENRGY    = NUMBER OF ENERGIES READ FROM DATA SET.
C  INPUT : (R*8)  ENRGYA()  = COLLISION ENERGIES READ FROM INPUT DATA
C                             SET.
C                             UNITS: EV/AMU
C                             DIMENSION: ENERGY INDEX
C  INPUT : (R*8)  ALPHAA()  = EXTRAPOLATION PARAMETER ALPHA READ FROM
C                             INPUT DATA SET.
C                             DIMENSION: ENERGY INDEX
C  INPUT : (R*8)  XSECNA(,) = N-RESOLVED CHARGE EXCHANGE CROSS-SECTIONS
C                             READ FROM INPUT DATA SET.
C                             UNITS: CM2
C                             1ST DIMENSION: ENERGY INDEX
C                             2ND DIMENSION: N-SHELL
C  INPUT : (R*8)  FRACLA(,) = L-RESOLVED CHARGE EXCHANGE CROSS-SECTIONS.
C                             AFTER CXDATA: ABSOLUTE VALUES (CM2).
C                             AFTER CXFRAC: FRACTION OF N-RESOLVED
C                                            DATA.
C                             1ST DIMENSION: ENERGY INDEX
C                             2ND DIMENSION: INDEXED BY I4IDFL(N,L)
C
C  OUTPUT: (R*8)  ERATE     = EFFECTIVE EMISSIVITY RATE COEFFICIENT FOR
C                             REQUESTED TRANSITION
C                             SPECTRUM LINE.
C                             UNITS: CM3 SEC-1
C
C  PARAM : (I*4)  MXN       = MXNSHL.
C  PARAM : (I*4)  MXJSHL    = MAXIMUM NUMBER OF J SUB-SHELLS.
C  PARAM : (I*4)  MXBEAM    = MAXIMUM NUMBER OF BEAM COMPONENTS.
C  PARAM : (I*4)  MXOBSL    = MAXIMUM NUMBER OF OBSERVED SPECTRUM
C                             LINES.
C  PARAM : (I*4)  MXPRSL    = MAXIMUM NUMBER OF SPECTRUM LINES TO
C                             PREDICT.
C
C  PARAM : (R*8)  EMP       = REDUCED MASS FOR POSITIVE ION.
C                             UNITS: ELECTRON MASSES
C
C          (I*4)  NBEAM     = NUMBER OF BEAM ENERGIES.
C          (I*4)  NOLINE    = NUMBER OF OBSERVED SPECTRUM LINES.
C          (I*4)  NPLINE    = NUMBER OF SPECTRUM LINES TO PREDICT.
C          (I*4)  NUMIN     = MINIMUM UPPER PRINCIPAL QUANTUM NUMBER
C                             FOR OBSERVED SPECTRUM LINES.
C          (I*4)  NUMAX     = MAXIMUM UPPER PRINCIPAL QUANTUM NUMBER
C                             FOR OBSERVED SPECTRUM LINES.
C
C          (R*8)  EM        = EMMISSION MEASURE.
C                             UNITS: CM-5
C
C          (I*4)  NL()      = LIST OF LOWER PRINCIPAL QUANTUM NUMBERS
C                             OF OBSERVED SPECTRUM LINES.
C                             DIMENSION: SPECTRUM LINE INDEX.
C          (I*4)  NU()      = LIST OF UPPER PRINCIPAL QUANTUM NUMBERS
C                             OF OBSERVED SPECTRUM LINES.
C                             DIMENSION: SPECTRUM LINE INDEX.
C          (I*4)  NPL()     = LIST OF LOWER PRINCIPAL QUANTUM NUMBERS
C                             OF SPECTRUM LINES TO PREDICT.
C                             DIMENSION: SPECTRUM LINE INDEX.
C          (I*4)  NPU()     = LIST OF UPPER PRINCIPAL QUANTUM NUMBERS
C                             OF SPECTRUM LINES TO PREDICT.
C                             DIMENSION: SPECTRUM LINE INDEX.
C
C          (R*8)  BMFRA()   = BEAM COMPONENT FRACTIONS.
C                             DIMENSION: COMPONENT INDEX.
C          (R*8)  BMENA()   = BEAM ENERGY COMPONENTS.
C                             UNITS: EV/AMU
C          (R*8)  EMISA()   = LIST OF EMISSIVITIES OF OBSERVED SPECTRUM
C                             LINES.
C                             UNITS: PH CM-2 SEC-1
C                             DIMENSION: SPECTRUM LINE INDEX.
C          (R*8)  TBLF()    = TABLE OF RADIATIVE LIFETIMES.
C                             UNITS: SECS
C                             DIMENSION: REFERENCED BY I4IDFL(N,L).
C          (R*8)  QTHIN()   = IONISATION RATE COEFFICIENT.
C                             UNITS: CM3 SEC-1
C                             DIMENSION: N SHELL INDEX.
C          (R*8)  QTHEX()   = MEAN EXCITATION RATE COEFFICIENTS FOR
C                             N-LEVELS AVERAGED OVER BEAM FRACTIONS.
C                             UNITS: CM3 SEC-1
C                             DIMENSION: N SHELL INDEX.
C          (R*8)  QTHCH()   = MEAN CHARGE EXCHANGE COEFFICIENTS FOR
C                             N-LEVELS AVERAGED OVER BEAM FRACTIONS.
C                             UNITS: CM3 SEC-1
C                             DIMENSION: N SHELL INDEX.
C          (R*8)  QTHRC()   = MEAN RECOMBINATION RATE COEFFICIENTS FOR
C                             N-LEVELS AVERAGED OVER BEAM FRACTIONS.
C                             UNITS: CM3 SEC-1
C                             DIMENSION: N SHELL INDEX.
C          (R*8)  QEX()     =
C                             DIMENSION: N SHELL INDEX.
C          (R*8)  TOTPOP()  = TOTAL COLLISION POP. FOR PREDICTED
C                             SPECTRUM LINE.
C                             UNITS: CM-2
C                             DIMENSION: PREDICTED LINE INDEX.
C          (R*8)  TOTEMI()  = TOTAL COLLISION EMISSIVITIES FOR PREDICTED
C                             SPECTRUM LINE.
C                             UNITS: PH CM-2 SEC-1
C                             DIMENSION: PREDICTED LINE INDEX.
C          (R*8)  AVRGWL()  = AVERAGE AIR WAVELENGTH FOR PREDICTED
C                             SPECTRUM LINE.
C                             UNITS: A
C                             DIMENSION: PREDICTED LINE INDEX.
C          (R*8)  QEFF()    = EFF. RATE COEFFICIENT FOR PREDICTED
C                             SPECTRUM LINE.
C                             UNITS: CM3 SEC-1
C                             DIMENSION: PREDICTED LINE INDEX.
C
C          (R*8)  FTHEXJ(,) = FRACTION OF N-LEVEL MEAN EXCITATION RATE
C                             COEFFICIENTS IN NLJ-LEVEL.
C                             1ST DIMENSION: J SHELL INDEX WHERE:
C                                            1 GIVES J=L+0.5
C                                            2 GIVES J=L-0.5
C                             2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C          (R*8)  FTHCHJ(,) = FRACTION OF N-LEVEL MEAN CHARGE EXCHANGE
C                             COEFFICIENTS IN NLJ-LEVEL.
C                             1ST DIMENSION: J SHELL INDEX WHERE:
C                                            1 GIVES J=L+0.5
C                                            2 GIVES J=L-0.5
C                             2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C          (R*8)  FTHRCJ(,) = FRACTION OF N-LEVEL MEAN RECOMBINATION
C                             RATE COEFFICIENTS IN NLJ-LEVEL.
C                             1ST DIMENSION: J SHELL INDEX WHERE:
C                                            1 GIVES J=L+0.5
C                                            2 GIVES J=L-0.5
C                             2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C          (R*8)  TBQMEP(,) = ELECTRON COLLISIONAL RATE COEFFT. FOR
C                             NLJ->NL+1J'.
C                             1ST DIMENSION: J->J' TRANSITION INDEX.
C                             2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C          (R*8)  TBQMEM(,) = ELECTRON COLLISIONAL RATE COEFFT. FOR
C                             NLJ->NL-1J'.
C                             1ST DIMENSION: J->J' TRANSITION INDEX.
C                             2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C          (R*8)  TBQMIP(,) = POSITIVE ION COLLISIONAL RATE COEFFT. FOR
C                             NLJ->NL+1J'.
C                             1ST DIMENSION: J->J' TRANSITION INDEX.
C                             2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C          (R*8)  TBQMIM(,) = POSITIVE ION COLLISIONAL RATE COEFFT. FOR
C                             NLJ->NL-1J'.
C                             1ST DIMENSION: J->J' TRANSITION INDEX.
C                             2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C          (R*8)  TBFMP(,)  = B-FIELD DEPENDENT MIXING RATE COEFFT. FOR
C                             NLJ->NL+1J'.
C                             1ST DIMENSION: J->J' TRANSITION INDEX.
C                             2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C          (R*8)  TBFM(,)   = B-FIELD DEPENDENT MIXING RATE COEFFT. FOR
C                             NLJ->NLJ'.
C                             1ST DIMENSION: J->J' TRANSITION INDEX.
C                             2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C          (R*8)  TBFMM(,)  = B-FIELD DEPENDENT MIXING RATE COEFFT. FOR
C                             NLJ->NL-1J'.
C                             1ST DIMENSION: J->J' TRANSITION INDEX.
C                             2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C
C          (R*8)  TBLPOP(,,)= TABLE OF COLLISION POP. FOR PREDICTED
C                             SPECTRUM LINE.
C                             UNITS: CM-2
C                             1ST DIMENSION: J->J' TRANSITION INDEX.
C                             2ND DIMENSION: REFERENCED BY I4IDLI().
C                             3RD DIMENSION: PREDICTED LINE INDEX.
C          (R*8)  TBLEMI(,,)= TABLE OF COLLISION EMISSIVITIES FOR
C                             PREDICTED SPECTRUM LINE.
C                             UNITS: PH CM-2 SEC-1
C                             1ST DIMENSION: J->J' TRANSITION INDEX.
C                             2ND DIMENSION: REFERENCED BY I4IDLI().
C                             3RD DIMENSION: PREDICTED LINE INDEX.
C          (R*8)  TBLWLN(,,)= TABLE OF WAVELENGTHS FOR PREDICTED
C                             SPECTRUM LINE.
C                             UNITS: A
C                             1ST DIMENSION: J->J' TRANSITION INDEX.
C                             2ND DIMENSION: REFERENCED BY I4IDLI().
C                             3RD DIMENSION: PREDICTED LINE INDEX.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4UNIT     ADAS      RETURNS UNIT NO. FOR OUTPUT OF MESSAGES.
C          CXTBLF     ADAS      FILLS L-RESOLVED RADIATIVE LIFETIME
C                               TABLE.
C          C6TBIN     ADAS      FILLS N-RESOLVED ELECTRON IMPACT
C                               IONISATION RATE TABLE.
C          C6TBEX     ADAS      FILLS N AND J-RESOLVED ELECTRON IMPACT
C                               EXCITATION RATE TABLES.
C          C6QEIK     ADAS      FILLS N AND J-RESOLVED CHARGE EXCHANGE
C                               RATE TABLES USING EIKONAL APPROXIMATION.
C          C6QXCH     ADAS      FILLS N AND J-RESOLVED CHARGE EXCHANGE
C                               RATE TABLES USING INPUT DATA SET.
C          C6TBRC     ADAS      FILLS N AND J-RESOLVED RADIATIVE
C                               RECOMBINATION RATE TABLES.
C          C6TBQM     ADAS      FILLS N AND J-RESOLVED COLLISIONAL RATE
C                               TABLES.
C          C6TBFM     ADAS      FILLS N AND J-RESOLVED B-FIELD
C                               DEPENDENT MIXING RATE TABLES.
C          C7EMIS     ADAS      PREDICTS THE J-RESOLVED EMISSIVITY FOR
C                               REQUESTED TRANSITIONS.
C
C NOTES:
C       1) THE J->J' TRANSITION INDEX IS AS FOLLOWS:
C              1 : J=L+0.5 -> J'=L'+0.5
C              2 : J=L+0.5 -> J'=L'-0.5
C              3 : J=L-0.5 -> J'=L'+0.5
C              4 : J=L-0.5 -> J'=L'-0.5
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/87
C          JET EXT. 5183
C
C DATE:    26/11/93
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    24TH MAY 1996
C
C VERSION: 1.1                          DATE: 24-05-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST VERSION
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    I4UNIT
C-----------------------------------------------------------------------
      INTEGER    MXN          , MXJSHL       , MXBEAM       ,
     &           MXOBSL       , MXPRSL
      PARAMETER( MXN    = 20  , MXJSHL = 2   , MXBEAM = 1   ,
     &           MXOBSL = 1   , MXPRSL = 1                    )
C-----------------------------------------------------------------------
      REAL*8     EMP
      PARAMETER( EMP = 1836.0D0 )
C-----------------------------------------------------------------------
      INTEGER    MXNENG  , MXNSHL  , NGRND   , NTOT    , NBOT    ,
     &           NTOP    , IRZ0    , IRZ1    , ITHEOR  , IBSTAT  ,
     &           IEMMS   , NTU     , NTL     , NMINF   , NMAXF   ,
     &           NENRGY
      INTEGER    NBEAM   , NOLINE  , NPLINE  , NUMIN   , NUMAX
C-----------------------------------------------------------------------
      REAL*8     RAMSNO  , TEV     , TIEV    , DENS    , DENSZ   ,
     &           ZEFF    , BMAG    , BMENG   , ERATE
      REAL*8     EM
C-----------------------------------------------------------------------
      INTEGER    NL(MXOBSL)      , NU(MXOBSL)      , NPL(MXPRSL)     ,
     &           NPU(MXPRSL)
C-----------------------------------------------------------------------
      REAL*8     ENRGYA(MXNENG)  , ALPHAA(MXNENG)
      REAL*8     BMENA(MXBEAM)   , BMFRA(MXBEAM)   , EMISA(MXOBSL)   ,
     &           QTHIN(MXN)      , QTHEX(MXN)      , QTHCH(MXN)      ,
     &           QTHRC(MXN)      , QEX(MXN)        , TOTPOP(MXPRSL)  ,
     &           TOTEMI(MXPRSL)  , AVRGWL(MXPRSL)  , QEFF(MXPRSL)    ,
     &           TBLF((MXN*(MXN+1))/2)
C-----------------------------------------------------------------------
      REAL*8     XSECNA(MXNENG,MXNSHL)                            ,
     &           FRACLA(MXNENG,(MXNSHL*(MXNSHL+1))/2)
      REAL*8     FTHEXJ(MXJSHL,(MXN*(MXN+1))/2)                   ,
     &           FTHCHJ(MXJSHL,(MXN*(MXN+1))/2)                   ,
     &           FTHRCJ(MXJSHL,(MXN*(MXN+1))/2)                   ,
     &           TBQMEP(2*MXJSHL,(MXN*(MXN+1))/2)                 ,
     &           TBQMEM(2*MXJSHL,(MXN*(MXN+1))/2)                 ,
     &           TBQMIP(2*MXJSHL,(MXN*(MXN+1))/2)                 ,
     &           TBQMIM(2*MXJSHL,(MXN*(MXN+1))/2)                 ,
     &           TBFMP(2*MXJSHL,(MXN*(MXN+1))/2)                  ,
     &           TBFMM(2*MXJSHL,(MXN*(MXN+1))/2)                  ,
     &           TBFM(2*MXJSHL,(MXN*(MXN+1))/2)
C-----------------------------------------------------------------------
      REAL*8     TBLPOP(2*MXJSHL,2*MXN-3,MXPRSL)             ,
     &           TBLEMI(2*MXJSHL,2*MXN-3,MXPRSL)             ,
     &           TBLWLN(2*MXJSHL,2*MXN-3,MXPRSL)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C CHECK INTERNAL PARAMETER 'MXN'.
C-----------------------------------------------------------------------
C
      IF (MXN .LT. MXNSHL) THEN
         WRITE(I4UNIT(-1),1000) MXN, MXNSHL
         STOP
      ENDIF
C
C-----------------------------------------------------------------------
C INTIALIZE VALUES.
C-----------------------------------------------------------------------
C
      NBEAM = 1
      BMFRA(1) = 1.0D0
      BMENA(1) = BMENG
C
      NOLINE = 1
      NL(1) = NTL
      NU(1) = NTU
      EMISA(1) = 1.0D12
C
      NPLINE = 1
      NPL(1) = NTL
      NPU(1) = NTU
C
C-----------------------------------------------------------------------
C  SET UP TABLES OF ALL NECESSARY ATOMIC DATA FOR SUBSEQUENT LOOKUP.
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C FILL RADIATIVE LIFETIME TABLE.
C-----------------------------------------------------------------------
C
      CALL CXTBLF( MXNSHL , IRZ1 , NBOT , NTOP , TBLF )
C
C-----------------------------------------------------------------------
C FILL ELECTRON IMPACT IONISATION RATE TABLE.
C-----------------------------------------------------------------------
C
      CALL C6TBIN( MXNSHL , IRZ1 , NBOT , NTOP , TEV , QTHIN )
C
C-----------------------------------------------------------------------
C FILL EXCITATION RATE TABLES.
C-----------------------------------------------------------------------
C
      CALL C6TBEX( MXNSHL , MXJSHL , IRZ1   , NBOT   , NTOP   ,
     &             NGRND  , TEV    , QTHEX  , FTHEXJ            )
C
C-----------------------------------------------------------------------
C CALCULATE CHARGE EXCHANGE RECOMBINATION RATES.
C-----------------------------------------------------------------------
C
      IF (ITHEOR .EQ. 2) THEN
C
C-----------------------------------------------------------------------
C EIKONAL TREATMENT.
C-----------------------------------------------------------------------
C
         CALL C6QEIK( MXNSHL , MXJSHL , MXBEAM , IRZ1   ,
     &                IBSTAT , NBOT   , NTOP   , NBEAM  ,
     &                BMENA  , BMFRA  , QTHCH  , FTHCHJ   )
C
C-----------------------------------------------------------------------
C
      ELSE
C
C-----------------------------------------------------------------------
C NON-EIKONAL TREATMENT.
C-----------------------------------------------------------------------
C
         CALL C6QXCH ( MXNENG , MXNSHL , MXJSHL , MXBEAM ,
     &                 NBEAM  , BMENA  , BMFRA  , NBOT   ,
     &                 NTOP   , NMINF  , NMAXF  , NENRGY ,
     &                 ENRGYA , ALPHAA , XSECNA , FRACLA ,
     &                 QTHCH  , FTHCHJ
     &               )
C
C-----------------------------------------------------------------------
C
      ENDIF
C
C-----------------------------------------------------------------------
C CALCULATE FREE ELECTRON RECOMBINATION RATES TO L,J LEVELS.
C-----------------------------------------------------------------------
C
      CALL C6TBRC( MXNSHL , MXJSHL , IRZ1   , NBOT   , NTOP   ,
     &             TEV    , QTHRC  , FTHRCJ                     )
C
C-----------------------------------------------------------------------
C FILL L SHELL MIXING TRANSITION RATE TABLES.
C-----------------------------------------------------------------------
C
      CALL C6TBQM( MXNSHL , MXJSHL , IRZ0   , IRZ1   , NBOT   ,
     &             NTOP   , TEV    , DENS   , ZEFF   , TIEV   ,
     &             EMP    , TBLF   , TBQMEP , TBQMEM , TBQMIP ,
     &             TBQMIM                                       )
C
C-----------------------------------------------------------------------
C FILL L SHELL MIXING TRANSITION RATES DUE TO MAGNETIC FIELDS.
C-----------------------------------------------------------------------
C
      CALL C6TBFM( MXNSHL , MXJSHL , IRZ0   , IRZ1   , RAMSNO ,
     &             NBOT   , NTOP   , BMAG   , TIEV   , TBLF   ,
     &             TBFMP  , TBFM   , TBFMM                      )
C
C-----------------------------------------------------------------------
C SELECT CHARGE EXCHANGE, EXCITATION OR RECOMBINATION EMISSION MEASURE
C SOLUTION
C-----------------------------------------------------------------------
C
      IF(IEMMS .EQ. 1) THEN
C
         CALL C7EMIS( MXNSHL , MXJSHL , MXOBSL , MXPRSL , IRZ0   ,
     &                IRZ1   , NGRND  , NTOT   , NBOT   , DENSZ  ,
     &                DENS   , NOLINE , NU     , NL     , EMISA  ,
     &                NPLINE , NPU    , NPL    , QTHCH  , FTHCHJ ,
     &                QTHIN  , TBQMEP , TBQMEM , TBQMIP , TBQMIM ,
     &                TBFMP  , TBFM   , TBFMM  , NUMIN  , NUMAX  ,
     &                EM     , QEX    , TOTPOP , TOTEMI , AVRGWL ,
     &                QEFF   , TBLPOP , TBLEMI , TBLWLN            )
C
      ELSE IF (IEMMS .EQ. 2) THEN
C
         CALL C7EMIS( MXNSHL , MXJSHL , MXOBSL , MXPRSL , IRZ0   ,
     &                IRZ1   , NGRND  , NTOT   , NBOT   , DENSZ  ,
     &                DENS   , NOLINE , NU     , NL     , EMISA  ,
     &                NPLINE , NPU    , NPL    , QTHEX  , FTHEXJ ,
     &                QTHIN  , TBQMEP , TBQMEM , TBQMIP , TBQMIM ,
     &                TBFMP  , TBFM   , TBFMM  , NUMIN  , NUMAX  ,
     &                EM     , QEX    , TOTPOP , TOTEMI , AVRGWL ,
     &                QEFF   , TBLPOP , TBLEMI , TBLWLN            )
C
      ELSE IF (IEMMS .EQ. 3) THEN
C
         CALL C7EMIS( MXNSHL , MXJSHL , MXOBSL , MXPRSL , IRZ0   ,
     &                IRZ1   , NGRND  , NTOT   , NBOT   , DENSZ  ,
     &                DENS   , NOLINE , NU     , NL     , EMISA  ,
     &                NPLINE , NPU    , NPL    , QTHRC  , FTHRCJ ,
     &                QTHIN  , TBQMEP , TBQMEM , TBQMIP , TBQMIM ,
     &                TBFMP  , TBFM   , TBFMM  , NUMIN  , NUMAX  ,
     &                EM     , QEX    , TOTPOP , TOTEMI , AVRGWL ,
     &                QEFF   , TBLPOP , TBLEMI , TBLWLN            )
C
      ENDIF
C
C-----------------------------------------------------------------------
C
      ERATE = QEFF(1)
C
C-----------------------------------------------------------------------
C
 1000 FORMAT( 1X, 32('*'), ' C7CXEE ERROR ', 32('*') //
     &        2X, 'INTERNAL PARAMETER ''MXN'' IS LESS THAN INPUT ',
     &            'ARGUMENT ''MXNSHL''.'/
     &        2X, 'MXN = ', I3 , '   MXNSHL = ', I3/
     &        2X, 'INCREASE PARAMETER ''MXN'' IN SUBROUTINE C7CXEE.' //
     &        1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
