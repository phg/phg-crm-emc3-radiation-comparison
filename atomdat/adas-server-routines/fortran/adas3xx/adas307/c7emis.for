CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas307/c7emis.for,v 1.1 2004/07/06 11:54:12 whitefor Exp $ Date $Date: 2004/07/06 11:54:12 $
CX
      SUBROUTINE C7EMIS( MXNSHL , MXJSHL , MXOBSL , MXPRSL ,
     &                   IZ0    , IZ1    , NGRND  , NTOT   ,
     &                   NBOT   , DENSZ  , DENS   , NOLINE ,
     &                   NU     , NL     , EMISA  , NPLINE ,
     &                   NPU    , NPL    , QTHEOR , FTHEOR ,
     &                   QTHIN  , TBQMEP , TBQMEM , TBQMIP ,
     &                   TBQMIM , TBFMP  , TBFM   , TBFMM  ,
     &                   NUMIN  , NUMAX  , EM     , QEX    ,
     &                   TOTPOP , TOTEMI , AVRGWL , QEFF   ,
     &                   TBLPOP , TBLEMI , TBLWLN
     &                 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C6EMIS *********************
C
C  PURPOSE: PREDICTS THE J-RESOLVED EMMISIVITY FOR REQUESTED
C           TRANSITIONS.
C
C  CALLING PROGRAM: ADAS306
C
C  INPUT : (I*4)  MXNSHL    = MAXIMUM NUMBER OF N SHELLS.
C  INPUT : (I*4)  MXJSHL    = MAXIMUM NUMBER OF J SUB-SHELLS.
C  INPUT : (I*4)  MXOBSL    = MAXIMUM NUMBER OF OBSERVED SPECTRUM
C                             LINES.
C  INPUT : (I*4)  MXPRSL    = MAXIMUM NUMBER OF SPECTRUM LINES TO
C                             PREDICT.
C  INPUT : (I*4)  IZ0       = NUCLEAR CHARGE.
C  INPUT : (I*4)  IZ1       = ION CHARGE.
C  INPUT : (I*4)  NGRND     = PRINCIPAL QUANTUM NUMBER OF GROUND STATE.
C  INPUT : (I*4)  NTOT      = PRINCIPAL QUANTUM NUMBER OF HIGHEST BOUND
C                             STATE.
C  INPUT : (I*4)  NBOT      = MINIMUM PRINCIPAL QUANTUM NUMBER.
C  INPUT : (R*8)  DENSZ     = PLASMA ION DENSITY.
C                             UNITS: CM-3
C  INPUT : (R*8)  DENS      = ELECTRON DENSITY.
C                             UNITS: CM-3
C  INPUT : (I*4)  NOLINE    = NUMBER OF OBSERVED SPECTRUM LINES.
C  INPUT : (I*4)  NU()      = LIST OF UPPER PRINCIPAL QUANTUM NUMBERS
C                             OF OBSERVED SPECTRUM LINES.
C                             DIMENSION: SPECTRUM LINE INDEX.
C  INPUT : (I*4)  NL()      = LIST OF LOWER PRINCIPAL QUANTUM NUMBERS
C                             OF OBSERVED SPECTRUM LINES.
C                             DIMENSION: SPECTRUM LINE INDEX.
C  INPUT : (R*8)  EMISA()   = LIST OF EMISSIVITIES OF OBSERVED SPECTRUM
C                             LINES.
C                             DIMENSION: SPECTRUM LINE INDEX.
C  INPUT : (I*4)  NPLINE    = NUMBER OF SPECTRUM LINES TO PREDICT.
C  INPUT : (I*4)  NPU()     = LIST OF UPPER PRINCIPAL QUANTUM NUMBERS
C                             OF SPECTRUM LINES TO PREDICT.
C                             DIMENSION: PREDICTED LINE INDEX.
C  INPUT : (I*4)  NPL()     = LIST OF LOWER PRINCIPAL QUANTUM NUMBERS
C                             OF SPECTRUM LINES TO PREDICT.
C                             DIMENSION: PREDICTED LINE INDEX.
C  INPUT : (R*8)  QTHEOR()  = MEAN CHARGE EXCHANGE, EXCITATION RATE OR
C                             RECOMBINATION RATE COEFFICIENTS FOR
C                             N-LEVELS AVERAGED OVER BEAM FRACTIONS.
C                             UNITS: CM3 SEC-1
C                             DIMENSION: N SHELL INDEX.
C  INPUT : (R*8)  FTHEOR(,) = FRACTION OF N-LEVEL MEAN CHARGE EXCHANGE,
C                             EXCITATION RATE OR RECOMBINATION RATE
C                             COEFFICIENTS IN NL-LEVEL.
C                             1ST DIMENSION: J SHELL INDEX WHERE:
C                                            1 GIVES J=L+0.5
C                                            2 GIVES J=L-0.5
C                             2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C  INPUT : (R*8)  QTHIN()   = IONISATION RATE COEFFICIENT.
C                             UNITS: CM3 SEC-1
C                             DIMENSION: N SHELL INDEX.
C  INPUT : (R*8)  TBQMEP(,) = ELECTRON COLLISIONAL RATE COEFFT. FOR
C                             NLJ->NL+1J'.
C                             1ST DIMENSION: J->J' TRANSITION INDEX.
C                             2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C  INPUT : (R*8)  TBQMEM(,) = ELECTRON COLLISIONAL RATE COEFFT. FOR
C                             NLJ->NL-1J'.
C                             1ST DIMENSION: J->J' TRANSITION INDEX.
C                             2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C  INPUT : (R*8)  TBQMIP(,) = POSITIVE ION COLLISIONAL RATE COEFFT. FOR
C                             NLJ->NL+1J'.
C                             1ST DIMENSION: J->J' TRANSITION INDEX.
C                            2 ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C  INPUT : (R*8)  TBQMIM(,) = POSITIVE ION COLLISIONAL RATE COEFFT. FOR
C                             NLJ->NL-1J'.
C                             1ST DIMENSION: J->J' TRANSITION INDEX.
C                             2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C  INPUT : (R*8)  TBFMP(,)  = B-FIELD DEPENDENT MIXING RATE COEFFT. FOR
C                             NLJ->NL+1J'.
C                             1ST DIMENSION: J->J' TRANSITION INDEX.
C                             2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C  INPUT : (R*8)  TBFM(,)   = B-FIELD DEPENDENT MIXING RATE COEFFT. FOR
C                             NLJ->NLJ'.
C                             1ST DIMENSION: J->J' TRANSITION INDEX.
C                             2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C  INPUT : (R*8)  TBFMM(,)  = B-FIELD DEPENDENT MIXING RATE COEFFT. FOR
C                             NLJ->NL-1J'.
C                             1ST DIMENSION: J->J' TRANSITION INDEX.
C                             2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C
C  OUTPUT: (I*4)  NUMIN     = MINIMUM UPPER PRINCIPAL QUANTUM NUMBER
C                             FOR OBSERVED SPECTRUM LINES.
C  OUTPUT: (I*4)  NUMAX     = MAXIMUM UPPER PRINCIPAL QUANTUM NUMBER
C                             FOR OBSERVED SPECTRUM LINES.
C  OUTPUT: (R*8)  EM        = EMMISSION MEASURE.
C  OUTPUT: (R*8)  QEX()     =
C                             DIMENSION: MXNSHL.
C  OUTPUT: (R*8)  TOTPOP()  = TOTAL COLLISION POP. FOR PREDICTED
C                             SPECTRUM LINE.
C                             UNITS: CM-2
C                             DIMENSION: PREDICTED LINE INDEX.
C  OUTPUT: (R*8)  TOTEMI()  = TOTAL COLLISION EMISSIVITIES FOR PREDICTED
C                             SPECTRUM LINE.
C                             UNITS: PH CM-2 SEC-1
C                             DIMENSION: PREDICTED LINE INDEX.
C  OUTPUT: (R*8)  AVRGWL()  = AVERAGE AIR WAVELENGTH FOR PREDICTED
C                             SPECTRUM LINE.
C                             UNITS: A
C                             DIMENSION: PREDICTED LINE INDEX.
C  OUTPUT: (R*8)  QEFF()    = EFF. RATE COEFFICIENT FOR PREDICTED
C                             SPECTRUM LINE.
C                             UNITS:
C                             DIMENSION: PREDICTED LINE INDEX.
C  OUTPUT: (R*8)  TBLPOP(,,)= TABLE OF COLLISION POP. FOR PREDICTED
C                             SPECTRUM LINE.
C                             UNITS: CM-2
C                             1ST DIMENSION: J->J' TRANSITION INDEX.
C                             2ND DIMENSION: REFERENCED BY I4IDLI().
C  OUTPUT: (R*8)  TBLEMI(,,)= TABLE OF COLLISION EMISSIVITIES FOR
C                             PREDICTED SPECTRUM LINE.
C                             UNITS: PH CM-2 SEC-1
C                             1ST DIMENSION: J->J' TRANSITION INDEX.
C                             2ND DIMENSION: REFERENCED BY I4IDLI().
C                             3RD DIMENSION: PREDICTED LINE INDEX.
C  OUTPUT: (R*8)  TBLWLN(,,)= TABLE OF WAVELENGTHS FOR PREDICTED
C                             SPECTRUM LINE.
C                             UNITS: A
C                             1ST DIMENSION: J->J' TRANSITION INDEX.
C                             2ND DIMENSION: REFERENCED BY I4IDLI().
C                             3RD DIMENSION: PREDICTED LINE INDEX.
C
C  PARAM : (I*4)  MXN       = MXNSHL.
C  PARAM : (I*4)  MXJ       = MXJSHL.
C  PARAM : (I*4)  MXOB      = MXOBSL.
C
C          (I*4)  NREP      =
C          (I*4)  IC        = LOOP INDEX.
C
C          (I*4)  ICREP()   =
C                             DIMENSION: MXOB.
C
C          (R*8)  WHIGH(,)  =
C                             1ST DIMENSION: J SHELL INDEX.
C                             2ND DIMENSION: REFERENCED BY L+1.
C          (R*8)  WLOW(,,)  =
C                             1ST DIMENSION: J SHELL INDEX.
C                             2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C                             3RD DIMENSION: REFERENCED BY L+1.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4UNIT     ADAS      RETURNS UNIT NO. FOR OUTPUT OF MESSAGES.
C          C6WFIL     ADAS
C          C7EMQX     ADAS
C          C6PRSL     ADAS      PREDICTS REQUESTED SPECTRUM LINES.
C
C NOTES:
C       1) THE J->J' TRANSITION INDEX IS AS FOLLOWS:
C              1 : J=L+0.5 -> J'=L'+0.5
C              2 : J=L+0.5 -> J'=L'-0.5
C              3 : J=L-0.5 -> J'=L'+0.5
C              4 : J=L-0.5 -> J'=L'-0.5
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    10/11/93
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    22ND MAY 1996
C
C VERSION: 1.1                          DATE: 22-05-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST VERSION. COPIED FROM C6EMIS. CALL TO C6EMQX
C		  REPLACED BY CALL TO C7EMQX
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    I4UNIT
C-----------------------------------------------------------------------
      INTEGER    MXN        , MXJ      , MXOB
      PARAMETER( MXN = 20   , MXJ = 2  , MXOB = 10  )
C-----------------------------------------------------------------------
      INTEGER    MXNSHL  , MXJSHL ,
     &           MXOBSL  , MXPRSL  , IZ0     , IZ1     ,
     &           NGRND   , NTOT    , NBOT    , NOLINE  ,
     &           NPLINE  , NUMIN   , NUMAX
      INTEGER    NREP    , IC
C-----------------------------------------------------------------------
      REAL*8     DENSZ   , DENS    , EM
C-----------------------------------------------------------------------
      INTEGER    NU(MXOBSL)   , NL(MXOBSL)   , NPU(MXPRSL)  ,
     &           NPL(MXPRSL)
      INTEGER    ICREP(MXOB)
C-----------------------------------------------------------------------
      REAL*8     EMISA(MXOBSL)   , QTHEOR(MXNSHL)  , QTHIN(MXNSHL)   ,
     &           QEX(MXNSHL)     , TOTPOP(MXPRSL)  , TOTEMI(MXPRSL)  ,
     &           AVRGWL(MXPRSL)  , QEFF(MXPRSL)
C-----------------------------------------------------------------------
      REAL*8     FTHEOR(MXJSHL,(MXNSHL*(MXNSHL+1))/2)    ,
     &           TBQMEP(2*MXJSHL,(MXNSHL*(MXNSHL+1))/2)  ,
     &           TBQMEM(2*MXJSHL,(MXNSHL*(MXNSHL+1))/2)  ,
     &           TBQMIP(2*MXJSHL,(MXNSHL*(MXNSHL+1))/2)  ,
     &           TBQMIM(2*MXJSHL,(MXNSHL*(MXNSHL+1))/2)  ,
     &           TBFMP(2*MXJSHL,(MXNSHL*(MXNSHL+1))/2)   ,
     &           TBFM(2*MXJSHL,(MXNSHL*(MXNSHL+1))/2)    ,
     &           TBFMM(2*MXJSHL,(MXNSHL*(MXNSHL+1))/2)
      REAL*8     WHIGH(MXJ,(MXN*(MXN+1))/2)
C-----------------------------------------------------------------------
      REAL*8     TBLPOP(2*MXJSHL,2*MXNSHL-3,MXPRSL)         ,
     &           TBLEMI(2*MXJSHL,2*MXNSHL-3,MXPRSL)         ,
     &           TBLWLN(2*MXJSHL,2*MXNSHL-3,MXPRSL)
      REAL*8     WLOW(MXJ,(MXN*(MXN+1))/2,MXN)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C CHECK INTERNAL PARAMETERS 'MXN' AND 'MXOB'.
C-----------------------------------------------------------------------
C
      IF (MXN .LT. MXNSHL) THEN
         WRITE(I4UNIT(-1),1000) MXN, MXNSHL
         STOP
      ENDIF
C
      IF (MXJ .LT. MXJSHL) THEN
         WRITE(I4UNIT(-1),1001) MXJ, MXJSHL
         STOP
      ENDIF
C
      IF (MXOB .LT. MXOBSL) THEN
         WRITE(I4UNIT(-1),1002) MXOB, MXOBSL
         STOP
      ENDIF
C
C-----------------------------------------------------------------------
C
      NREP = 1
      ICREP(1) = 1
      NUMIN = NU(1)
      NUMAX = NU(1)
C
      DO 1 IC = 2 , NOLINE
C
         IF (NU(IC) .NE. NU(IC-1)) THEN
            NREP = NREP + 1
            ICREP(NREP) = IC
         ENDIF
C
         NUMIN = MIN0( NUMIN , NU(IC) )
         NUMAX = MAX0( NUMAX , NU(IC) )
C
  1   CONTINUE
C
C-----------------------------------------------------------------------
C
      CALL C6WFIL( MXNSHL , MXJSHL , IZ1    , NGRND  , NTOT   ,
     &             NBOT   , NUMAX  , DENSZ  , DENS   , QTHEOR ,
     &             FTHEOR , QTHIN  , TBQMEP , TBQMEM , TBQMIP ,
     &             TBQMIM , TBFMP  , TBFM   , TBFMM  , WHIGH  ,
     &             WLOW                                         )
C
C-----------------------------------------------------------------------
C
      CALL C7EMQX( MXNSHL , MXJSHL , MXOBSL , IZ1    , NBOT   ,
     &             NUMIN  , NUMAX  , NU     , NL     , EMISA  ,
     &             NREP   , ICREP  , QTHEOR , WLOW   , EM     ,
     &             QEX                                          )
C
C-----------------------------------------------------------------------
C PREDICT REQUESTED SPECTRUM LINES.
C-----------------------------------------------------------------------
C
      CALL C6PRSL( MXNSHL , MXJSHL , MXPRSL , IZ0    , IZ1    ,
     &             NPLINE , NPU    , NPL    , NUMAX  , WHIGH  ,
     &             WLOW   , EM     , QEX    , TOTPOP , TOTEMI ,
     &             AVRGWL , QEFF   , TBLPOP , TBLEMI , TBLWLN   )
C
C-----------------------------------------------------------------------
C
 1000 FORMAT( 1X, 32('*'), ' C6EMIS ERROR ', 32('*') //
     &        2X, 'INTERNAL PARAMETER ''MXN'' IS LESS THAN INPUT ',
     &            'ARGUMENT ''MXNSHL''.'/
     &        2X, 'MXN = ', I3 , '   MXNSHL = ', I3/
     &        2X, 'INCREASE PARAMETER ''MXN'' IN SUBROUTINE C6EMIS.' //
     &        1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )
 1001 FORMAT( 1X, 32('*'), ' C6EMIS ERROR ', 32('*') //
     &        2X, 'INTERNAL PARAMETER ''MXJ'' IS LESS THAN INPUT ',
     &            'ARGUMENT ''MXNSHL''.'/
     &        2X, 'MXJ = ', I3 , '   MXJSHL = ', I3/
     &        2X, 'INCREASE PARAMETER ''MXJ'' IN SUBROUTINE C6EMIS.' //
     &        1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )
 1002 FORMAT( 1X, 32('*'), ' C6EMIS ERROR ', 32('*') //
     &        2X, 'INTERNAL PARAMETER ''MXOB'' IS LESS THAN INPUT ',
     &            'ARGUMENT ''MXOBSL''.'/
     &        2X, 'MXOB = ', I3 , '   MXOBSL = ', I3/
     &        2X, 'INCREASE PARAMETER ''MXOB'' IN SUBROUTINE C6EMIS.'//
     &        1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
