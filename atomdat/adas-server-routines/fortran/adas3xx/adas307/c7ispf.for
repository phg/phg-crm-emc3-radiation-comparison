CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas307/c7ispf.for,v 1.1 2004/07/06 11:54:20 whitefor Exp $ Date $Date: 2004/07/06 11:54:20 $
CX
      SUBROUTINE C7ISPF(LPEND, IBATCH)
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C7ISPF *********************
C
C  PURPOSE: READ FROM IDL ROUTINES c7ispf and c7spf1
C
C  CALLING PROGRAM: ADAS307
C
C  I/O   : (L*4)   LPEND    = FLAGS IF END OF ANALYSIS REQUESTED.
C                             .TRUE.  => END ANALYSIS OF CURRENT DATA
C                                        SETS
C                             .FALSE. => CONTINUE PANALYSIS WITH CURRENT
C                                        DATA SETS
C
C
C          (I*4)   PIPEIN - PARAMETER = UNIT NUMBER FOR INPUT FROM PIPE
C          (I*4)   LOGIC  - USED TO PIPE LOGICAL VALUES
C  I/O   : (I*4)   IBATCH - USED TO FLAG ACTION TO OTHER ROUTINES (
C			    AS LPEND CAN NOW HAVE 3 VALUES WHICH IT IS
C                           NOT POSSIBLE TO TREAT WITH A LOGICAL 
C                           VARIABLE)
C
C ROUTINES: NONE
C
C AUTHOR:  JONATHAN NASH  (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    02/12/93
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    24TH MAY 1996
C
C VERSION: 1.1                          DATE: 24-05-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST VERSION
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    PIPEIN  , LOGIC
      INTEGER    IBATCH
      PARAMETER (PIPEIN=5)
C-----------------------------------------------------------------------
      LOGICAL    LPEND   
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C READ OUPUTS FROM PIPE
C-----------------------------------------------------------------------
C
      READ( PIPEIN, *) LOGIC
      IBATCH = LOGIC
      IF (LOGIC.EQ.1 .OR. LOGIC.EQ.2) THEN
          LPEND = .TRUE.
      ELSE
          LPEND = .FALSE.
      ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN
      END
