C UNIX-IDL PORT - SCCS INFO: MODULE @(#)$Header: /home/adascvs/fortran/adas3xx/adas302/c2setp.for,v 1.1 2004/07/06 11:48:52 whitefor Exp $ DATE $Date: 2004/07/06 11:48:52 $
C
       SUBROUTINE C2SETP( NBSEL )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C2SETP *********************
C
C  PURPOSE:  WRITES THE VALUE OF NBSEL OUT TO IDL
C
C  CALLING PROGRAM: ADAS302
C
C  DATA:
C           DATA IS OBTAINED VIA SUBROUTINE 'C2DATA'
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  NBSEL   = NUMBER OF ION/ATOM X-SECTIONS READ IN.
C                           I.E. NUMBER OF DATA-BLOCKS.
C
C          (I*4)  PIPEOU  = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C
C ROUTINES:
C           ROUTINE     SOURCE      BRIEF DESCRIPTION
C           ------------------------------------------------------------
C
C
C AUTHOR:  H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C          JA8.08
C          TEL. 0141-553-4196
C
C DATE:    12/11/96
C
C UNIX-IDL PORT: H.P.SUMMERS
C
C VERSION: 1.1				DATE: 19-11-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C	    - PUT UNDER S.C.C.S. CONTROL
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER    NBSEL            
      INTEGER    PIPEOU
      PARAMETER ( PIPEOU = 6 )
C-----------------------------------------------------------------------
C
C **********************************************************************
C-----------------------------------------------------------------------
C WRITE NBSEL OUT TO IDL
C-----------------------------------------------------------------------
C
      WRITE( PIPEOU, * ) NBSEL
      CALL XXFLSH( PIPEOU )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
