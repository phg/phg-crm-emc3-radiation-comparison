C UNIX-IDL PORT - SCCS INFO: MODULE @(#)$Header: /home/adascvs/fortran/adas3xx/adas302/c2chkz.for,v 1.1 2004/07/06 11:48:25 whitefor Exp $ DATE $Date: 2004/07/06 11:48:25 $
C
      SUBROUTINE EC2CHKZ( DSNAME , ESYM )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C2CHKZ *********************
C
C  PURPOSE: TO CHECK THAT THE RECEIVER ION ELEMENT SYMBOL READ FROM  THE
C           INPUT DATA SET AGREES WITH THAT STORED IN THE ISPF  FUNCTION
C           POOL. THE ISPF FUNCTION POOL VALUE BEING OBTAINED  FROM  THE
C           INPUT DATA SET MEMBER NAME.
C
C  CALLING PROGRAM: ADAS302
C
C  SUBROUTINE:
C
C  INPUT : (C*(*)) DSNAME  = MVS DATA SET NAME - WHERE INPUT VALUES
C                            ORIGINATE
C  INPUT : (C*2)   ESYM    = INPUT FILE DATA:IONISING ION ELEMENT SYMBOL
C
C          (C*8)   F2      = PARAMETER = 'VCOPY   '
C          (C*8)   F4      = PARAMETER = 'MOVE    '
C
C          (I*4)   I4UNIT  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (I*4)   ILEN    = LENGTH, IN BYTES, OF ISPF DIALOG VARIABLES
C
C          (C*2)   ESYMM   =ISPF MEMBER NAME:EMITTING ION ELEMENT SYMBOL
C          (C*2)   C2      = GENERAL USE 2 BYTE CHARACTER STRING
C          (C*8)   CHIN    = ISPF PANEL VARIABLE NAME: ELEMENT SYMBOL
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXTERM     ADAS      TERMINATES PROGRAM WITH MESSAGE
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          ISPLNK     ISPF      ISPF PANRL ROUTINE
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/37
C          JET EXT. 2520
C
C DATE:    05/06/91
C
C UPDATE:  23/04/93 - PE BRIDEN - ADAS91: ADDED I4UNIT FUNCTION TO WRITE
C                                         STATEMENTS FOR SCREEN MESSAGES
C
C UPDATE:  24/05/93 - PE BRIDEN - ADAS91: CHANGED I4UNIT(0)-> I4UNIT(-1)
C
C UNIX-IDL PORT: H.P.SUMMERS
C
C VERSION: 1.1				DATE: 19-11-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C	    - PUT UNDER S.C.C.S. CONTROL
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      CHARACTER   F2*8              , F4*8
C-----------------------------------------------------------------------
      PARAMETER(  F2 = 'VCOPY   '   , F4 = 'MOVE    '   )
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER     I4UNIT
      INTEGER     ILEN
C-----------------------------------------------------------------------
      CHARACTER   ESYM*2            , DSNAME*(*)
      CHARACTER   ESYMM*2           , C2*2              , CHIN*8
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      DATA        CHIN/ '(SELEM) '  /
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C
      ILEN  = 2
      CALL ISPLNK( F2 , CHIN , ILEN , C2    , F4)
      ESYMM = C2
C
         IF ( ESYM.NE.ESYMM ) THEN
            WRITE(I4UNIT(-1),1000) DSNAME
            WRITE(I4UNIT(-1),1001) ESYM , ESYMM
            CALL XXTERM
         ENDIF
 
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(1X,32('*'),' E2CHKZ ERROR ',32('*')//
     &       1X,'INPUT DATA SET NAME: ',A/)
 1001 FORMAT(1X,'ERROR TYPE: ELEMENT DISCREPANCY.'//
     &       4X,'IONIZING ION ELEMENT SYMBOL: READ FROM DATA SET    = ',
     &   A2/33X,'READ FROM MEMBER NAME = ',A2)
C
C-----------------------------------------------------------------------
C
      RETURN
      END
