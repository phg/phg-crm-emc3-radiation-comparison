C UNIX-IDL PORT - SCCS INFO: MODULE @(#)$Header: /home/adascvs/fortran/adas3xx/adas302/c2data.for,v 1.2 2004/07/06 11:48:30 whitefor Exp $ DATE $Date: 2004/07/06 11:48:30 $
C
       SUBROUTINE C2DATA( IUNIT  , DSNAME ,
     &                    NSTORE , NEDIM  ,
     &                    NBSEL  , ISELA  ,
     &                    CPRIMY , CSECDY , CTYPE,
     &                    AMPA   , AMSA   , ALPHA  , ETHRA  ,
     &                    IEA    ,
     &                    TEEA   , SIA
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C2DATA *********************
C
C  PURPOSE:  TO  FETCH  DATA  FROM  INPUT ION/ATOM CROSS-SECTION
C            FILES OF TYPE ADF02.
C
C  CALLING PROGRAM: ADAS302/SSIA
C
C  DATA:
C
C           UP TO 'NSTORE' SETS (DATA-BLOCKS) OF DATA MAY BE  READ FROM
C           THE FILE - EACH BLOCK FORMING A COMPLETE SET OF  CROSS-
C           SECTION VALUES FOR GIVEN COLLISION ENERGIES.
C           EACH DATA-BLOCK  IS  ANALYSED INDEPENDENTLY OF ANY  OTHER
C           DATA-BLOCK.
C
C           THE UNITS USED IN THE DATA FILE ARE TAKEN AS FOLLOWS:
C
C           COLLISION ENERGIES  : EV/AMU
C           CROSS-SECTION       : CM**2
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNIT    = UNIT TO WHICH INPUT FILE IS ALLOCATED.
C  INPUT : (C*80) DSNAME   = MVS DATA SET NAME OF DATA SET BEING READ
C
C  INPUT : (I*4)  NSTORE   = MAXIMUM NUMBER  OF  INPUT DATA-BLOCKS  THAT
C                            CAN BE STORED.
C  INPUT : (I*4)  NEDIM    = MAX NUMBER OF COLLISION ENERGIES ALLOWED
C
C  OUTPUT: (I*4)  NBSEL    = NUMBER OF DATA-BLOCKS ACCEPTED & READ IN.
C  OUTPUT: (I*4)  ISELA()  = READ - DATA-SET DATA-BLOCK ENTRY INDICES
C                            DIMENSION: DATA-BLOCK INDEX
C
C  OUTPUT: (C*5)  CPRIMY() = READ - PRIMARY SPECIES IDENTIFICATION
C                            DIMENSION: DATA-BLOCK INDEX
C  OUTPUT: (C*5)  CSECDY() = READ - SECONDARY SPECIES IDENTIFICATION
C                            DIMENSION: DATA-BLOCK INDEX
C  OUTPUT: (C*3)  CTYPE()  = READ - CROSS-SECTION TYPE
C                            DIMENSION: DATA-BLOCK INDEX
C
C  OUTPUT: (R*8)  AMPA()   = READ - PRIMARY SPECIES ATOMIC MASS NUMBER
C                            DIMENSION: DATA-BLOCK INDEX
C  OUTPUT: (R*8)  AMSA()   = READ - SECONDARY SPECIES ATOMIC MASS NUMBER
C                            DIMENSION: DATA-BLOCK INDEX
C  OUTPUT: (R*8)  ALPHA()  = READ - HIGH ENERGY EXTRAPOLATION PARM.
C                            DIMENSION: DATA-BLOCK INDEX
C  OUTPUT: (R*8)  ETHRA()  = READ - ENERGY THRESHOLD (EV)
C                            DIMENSION: DATA-BLOCK INDEX
C
C  OUTPUT: (I*4)  IEA()    = READ - NUMBER OF COLLISION ENERGIES
C                            DIMENSION: DATA-BLOCK INDEX
C
C  OUTPUT: (R*8)  TEEA(,)  = READ - COLLISION ENERGIES (UNITS: eV/AMU)
C                            1st DIMENSION: COLLISION ENERGY INDEX
C                            2nd DIMENSION: DATA-BLOCK INDEX
C
C  OUTPUT: (R*8)  SIA(,)   =READ - FULL SET OF COLLISION CROSS-
C                                   SECTION VALUES (cm**2)
C                            1st DIMENSION: COLLISION ENERGY INDEX
C                            2nd DIMENSION: DATA-BLOCK INDEX
C
C          (I*4)  I4EIZ0   = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (I*4)  I4FCTN   = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (I*4)  I4UNIT   = FUNCTION - (SEE ROUTINE SECTION BELOW)
C          (I*4)  IBLK     = ARRAY INDEX: DATA-BLOCK INDEX
C          (I*4)  ITT      = ARRAY INDEX: COLLISION ENERGY INDEX
C          (I*4)  NENUM    = NUMBER OF COLLISION ENERGIES FOR CURRENT
C                            DATA-BLOCK
C          (I*4)  IABT     = RETURN CODE FROM 'I4FCTN'
C          (I*4)  IPOS2    = GENERAL USE STRING INDEX VARIABLE
C
C          (R*8)  R8FCTN   = FUNCTION - (SEE ROUTINES SECTION BELOW)
C
C          (L*4)  LBEND    = IDENTIFIES WHETHER THE LAST OF THE  INPUT
C                            DATA SUB-BLOCKS HAS BEEN LOCATED.
C                            (.TRUE. => END OF SUB-BLOCKS REACHED)
C
C          (C*1)  CSLASH   = '/' - DELIMITER FOR 'XXHKEY'
C          (C*2)  C2       = GENERAL USE TWO BYTE CHARACTER STRING
C          (C*1)  CKEY1    = 'P'      - INPUT BLOCK HEADER KEY
C          (C*1)  CKEY2    = 'S'      - INPUT BLOCK HEADER KEY
C          (C*1)  CKEY3    = 'A'      - INPUT BLOCK HEADER KEY
C          (C*1)  CKEY4    = 'E'      - INPUT BLOCK HEADER KEY
C          (C*4)  CKEY5    = 'T'      - INPUT BLOCK HEADER KEY
C          (C*4)  CKEY6    = 'ISEL'   - INPUT BLOCK HEADER KEY
C          (C*3)  C3       = GENERAL USE THREE BYTE CHARACTER STRING
C          (C*9)  C10      = GENERAL USE NINE BYTE CHARACTER STRING
C          (C*80) C80      = GENERAL USE 80 BYTE  CHARACTER  STRING  FOR
C                            THE INPUT OF DATA-SET RECORDS.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXHKEY     ADAS      OBTAIN KEY/RESPONSE STRINGS FROM TEXT
C          I4FCTN     ADAS      INTEGER*4 FUNCTION    -
C                               CONVERT CHARACTER STRING TO INTEGER
C          I4UNIT     ADAS      INTEGER*4 FUNCTION    -
C                               FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          R8FCTN     ADAS      REAL*8 FUNCTION       -
C                               CONVERT CHARACTER STRING TO REAL*8
C
C AUTHOR:  H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C          JA8.08
C          TEL. 0141-553-4196
C
C DATE:    12/11/96
C
C UNIX-IDL PORT: H.P.SUMMERS
C
C VERSION: 1.1				DATE: 19-11-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C	    - PUT UNDER S.C.C.S. CONTROL
C
C VERSION: 1.2				DATE: 14-02-97
C MODIFIED: RICHARD MARTIN
C		- CHANGED INITIALISATION 'CKEY2 /'S  '/' TO 'CKEY2 /'S'/ '
C
C
C-----------------------------------------------------------------------
      INTEGER    I4FCTN                , I4UNIT
      INTEGER    IUNIT                 , NSTORE              ,
     &           NEDIM                 , NBSEL
      INTEGER    IBLK                  , ITT                 ,
     &           NENUM                 , IABT                ,
     &           IPOS2
C-----------------------------------------------------------------------
      REAL*8     R8FCTN
C-----------------------------------------------------------------------
      LOGICAL    LBEND
C-----------------------------------------------------------------------
      CHARACTER  DSNAME*80     
      CHARACTER  CSLASH*1              , C2*2                ,
     &           CKEY1*1               , CKEY2*1             ,
     &           CKEY3*1               , CKEY4*1             ,
     &           CKEY5*3               , CKEY6*4             , 
     &           C3*3                  , C9*9                , C80*80
C-----------------------------------------------------------------------
      INTEGER    ISELA(NSTORE)         , IEA(NSTORE)
C-----------------------------------------------------------------------
      CHARACTER  CPRIMY(NSTORE)*5      , CSECDY(NSTORE)*5    ,
     &           CTYPE(NSTORE)*3
C-----------------------------------------------------------------------
      REAL*8     AMPA(NSTORE)          , AMSA(NSTORE)
      REAL*8     ALPHA(NSTORE)         , ETHRA(NSTORE)
      REAL*8     TEEA(NEDIM,NSTORE)    , SIA(NEDIM,NSTORE)
C-----------------------------------------------------------------------
      SAVE       CSLASH                ,
     &           CKEY1                 , CKEY2                ,
     &           CKEY3                 , CKEY4                ,
     &           CKEY5
C
C-----------------------------------------------------------------------
C
      DATA       CSLASH / '/' /
      DATA       CKEY1  / 'P'      /   , CKEY2  / 'S'      /  ,
     &           CKEY3  / 'A'      /   , CKEY4  / 'E'      /  ,
     &           CKEY5  / 'T'      /   , CKEY6  / 'ISEL'   /
C
C **********************************************************************
C
      LBEND = .FALSE.
C
C-----------------------------------------------------------------------
C READ IN NUMBER OF DATA-BLOCKS PRESENT IN INPUT FILE.
C-----------------------------------------------------------------------
C
      READ(IUNIT,1000) C80
      READ(C80,* ) NBSEL
C
         IF (NBSEL.GT.NSTORE) THEN
            WRITE(I4UNIT(-1),2000) DSNAME , NSTORE , NBSEL , NSTORE
            NBSEL = NSTORE
         ENDIF
C
C***********************************************************************
C INPUT DATA FOR EACH OF THE DATA-BLOCKS
C***********************************************************************
C
         DO 1 IBLK=1,NBSEL
C
C-----------------------------------------------------------------------
C INPUT DONOR/RECEIVER SPECS. AND OTHER INFORMATION (CHECK BLOCK EXISTS)
C-----------------------------------------------------------------------
C
            IF (.NOT.LBEND) THEN
                READ(IUNIT,1000)   C80
C
                IF ( C80(1:2).NE.'C-') THEN
C
                     IF(INDEX(C80,'/').GT.0) THEN
C
C----------------------------------------------------------------------
C  NEW FORMATTING FOR FIRST LINE OF DATA BLOCK
C----------------------------------------------------------------------
C
                         CPRIMY(IBLK) = C80(1:5)
                         CSECDY(IBLK )= C80( 7:11)
                         IPOS2        = INDEX(C80(12:80),'/') + 11
C
                         READ(C80(12:IPOS2),*) IEA(IBLK)
C
                         CALL XXHKEY( C80 , CKEY1 , CSLASH , C9 )
                         AMPA(IBLK) = R8FCTN(C9,IABT)
                         CALL XXHKEY( C80 , CKEY2 , CSLASH , C9 )
                         AMSA(IBLK) = R8FCTN(C9,IABT)
                         CALL XXHKEY( C80 , CKEY3 , CSLASH , C9 )
                         ALPHA(IBLK) = R8FCTN(C9,IABT)
                         CALL XXHKEY( C80 , CKEY4 , CSLASH , C9 )
                         ETHRA(IBLK) = R8FCTN(C9,IABT)
                         CALL XXHKEY( C80 , CKEY5 , CSLASH , C3 )
                         CTYPE(IBLK) = C3
                         CALL XXHKEY( C80 , CKEY5 , CSLASH , C2 )
                         ISELA(IBLK) = I4FCTN( C2  , IABT )
C
                         NENUM       = IEA(IBLK)
C
C----------------------------------------------------------------------
C  OLD FORMATTING FOR FIRST LINE OF DATA BLOCK
C----------------------------------------------------------------------
C
                     ELSE
                         READ(C80,1003) IEA(IBLK) , AMPA(IBLK)  ,
     &                                  AMSA(IBLK), ALPHA(IBLK) ,
     &                                  ETHRA(IBLK), ISELA(IBLK)
                         CPRIMY(IBLK) = '  *  '
                         CSECDY(IBLK) = '  *  '
C
                     NENUM       = 24
C
                     ENDIF
C
                     IF (NENUM.GT.NEDIM) THEN
                         WRITE(I4UNIT(-1),2001) DSNAME , IBLK  ,
     &                                          NEDIM   , NENUM
                         STOP
                     ENDIF
C
C-----------------------------------------------------------------------
C INPUT COLLISION ENERGIES, CROSS-SECTIONS FOR BLOCK
C-----------------------------------------------------------------------
C
                     READ(IUNIT,1002) ( TEEA(ITT,IBLK) , ITT=1,NENUM )
C
                     READ(IUNIT,1002) ( SIA(ITT,IBLK)  , ITT=1,NENUM )
C
                  ELSE
                     WRITE(I4UNIT(-1),2002) DSNAME  , NBSEL    ,
     &                                     IBLK - 1 , IBLK - 1
                     LBEND = .TRUE.
                     NBSEL = IBLK - 1
                  ENDIF
C
            ENDIF
C
    1    CONTINUE
C
C***********************************************************************
C
 1000 FORMAT(1A80)
 1001 FORMAT(A2)
 1002 FORMAT(6D10.3)
 1003 FORMAT(I5,4D10.3,25X,I5)
C
 2000 FORMAT(/1X,31('*'),' C2DATA MESSAGE ',31('*')/
     &        2X,'INPUT CROSS-SECTION DATA SET NAME: ',A44/
     &        2X,'NUMBER OF DATA-BLOCKS',
     &           ' IN INPUT DATA SET TOO GREAT.'/
     &        2X,'THE MAXIMUM NUMBER ALLOWED     = ',I3/
     &        2X,'THE NUMBER PRESENT IN DATA SET = ',I3/
     &        2X,'THEREFORE ONLY THE FIRST ',I2,' HAVE BEEN ACCEPTED'/
     &        1X,31('*'),' END OF MESSAGE ',31('*'))
 2001 FORMAT(/1X,32('*'),' C2DATA ERROR ',32('*')/
     &        2X,'INPUT CROSS-SECTION DATA SET NAME: ',A44/
     &        2X,'DATA-BLOCK INDEX: ',I3//
     &        2X,'THE NUMBER OF INPUT COLLISION ENERGIES TOO LARGE'/
     &        2X,'THE MAXIMUM NUMBER ALLOWED       = ',I3/
     &        2X,'THE NUMBER PRESENT IN DATA-BLOCK = ',I3/
     &        1X,29('*'),' PROGRAM TERMINATED ',29('*'))
 2002 FORMAT(/1X,31('*'),' C2DATA MESSAGE ',31('*')/
     &        2X,'INPUT CROSS-SECTION DATA SET NAME: ',A44/
     &        2X,'INCONSISTENCY IN THE NUMBER OF DATA-BLOCKS',
     &           ' EXPECTED AND READ.'/
     &        2X,'THE NUMBER EXPECTED = ',I3/
     &        2X,'THE NUMBER READ IN  = ',I3/
     &        2X,'THEREFORE ONLY ',I3,' HAVE BEEN ACCEPTED'/
     &        1X,31('*'),' END OF MESSAGE ',31('*'))
C
C-----------------------------------------------------------------------
C
      RETURN
      END
