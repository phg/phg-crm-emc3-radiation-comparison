C UNIX-IDL PORT - SCCS INFO: MODULE @(#)$Header: /home/adascvs/fortran/adas3xx/adas302/c2ispf.for,v 1.1 2004/07/06 11:48:37 whitefor Exp $ DATE $Date: 2004/07/06 11:48:37 $
C
      SUBROUTINE C2ISPF( IPAN   , LPEND  ,
     &                   NSTORE , NEDIM  , NDEIN  , NDTIN ,
     &                   NBSEL  ,
     &                   CPRIMY , CSECDY , CTYPE ,
     &                   IEA    ,
     &                   EVALS  , TVALS  ,
     &                   TITLE  ,
     &                   IBSEL  ,
     &                   AMD    , AMR    ,
     &                   IETYP  , IEVAL  , EIN    ,
     &                   ITTYP  , ITVAL  , TIN    ,
     &                   LFSEL  , LOSEL  , LTSEL  ,
     &                   TOLVAL  
     &                 )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C2ISPF *********************
C
C  PURPOSE: PIPE COMMUNICATIONS TO AND FROM IDL.
C
C  CALLING PROGRAM: ADAS302
C
C  SUBROUTINE:
C
C  I/O   : (I*4)   IPAN     = ISPF PANEL NUMBER
C  I/O   : (L*4)   LPEND    = .TRUE.  => END ANALYSIS OF CURRENT DATA
C                                        SETS
C                           = .FALSE. => CONTINUE ANALYSIS WITH CURRENT
C                                        DATA SETS
C
C  INPUT : (I*4)   NSTORE   = MAXIMUM NUMBER OF CROSS-SECT. DATA-BLOCKS
C                             WHICH CAN BE READ FROM THE INPUT DATA-SET.
C  INPUT : (I*4)   NEDIM    = MAX. NO. OF COLLISION ENERGIES ALLOWED
C  INPUT : (I*4)   NDEIN    = MAX. NUMBER OF INPUT COLLISION ENERGIES
C  INPUT : (I*4)   NDTIN    = MAX. NUMBER OF INPUT TEMPERATURES
C
C  INPUT : (I*4)   NBSEL    = NUMBER OF DATA-BLOCKS ACCEPTED & READ IN.
C
C  INPUT : (C*5)   CPRIMY() = INPUT DATA FILE: PRIMARY IDENTIFICATION
C                             DIMENSION: DATA-BLOCK INDEX
C  INPUT : (C*5)   CSECDY() = INPUT DATA FILE: SECONDARY IDENTIFICATION
C                             DIMENSION: DATA-BLOCK INDEX
C  INPUT : (C*3)   CTYPE()  = INPUT DATA FILE: CROSS-SECTION TYPE
C                             DIMENSION: DATA-BLOCK INDEX
C
C  INPUT : (I*4)   IEA()    = INPUT FILE - NO. OF COLLISION ENERGIES
C                             DIMENSION: DATA-BLOCK INDEX
C
C  INPUT : (R*8)   EVALS(,,)= INPUT DATA FILE: COLLISION ENERGIES
C                             1ST DIMENSION: ENERGY INDEX
C                             2ND DIMENSION: 1 => VEL. CM S-1(IETYP=1)
C                                            2 => VEL. AT.UN.(IETYP=2)
C                                            3 => ENER EV/AMU(IETYP=3)
C                             3RD DIMENSION: DATA-BLOCK INDEX
C  INPUT : (R*8)   TVALS(,,)= INPUT DATA FILE: COLLISION ENERGIES
C                             1ST DIMENSION: ENERGY INDEX
C                             2ND DIMENSION: 1 => PRIMARY EV   (ITTYP=1)
C                                            2 => SECONDARY EV   (ITTYP=2)
C                                            3 => ENER EV/AMU(ITTYP=3)
C                             3RD DIMENSION: DATA-BLOCK INDEX
C
C  OUTPUT: (C*40)   TITLE   = ISPF ENTERED GENERAL TITLE FOR PROGRAM RUN
C
C  OUTPUT: (I*4)    IBSEL   = SELECTED DATA-BLOCK INDEX FOR ANALYSIS
C
C  OUTPUT: (R*8)    AMD     = PRIMARY SPECIES MASS NUMBER
C  OUTPUT: (R*8)    AMR     = SECONDARY SPECIES MASS NUMBER
C  OUTPUT: (I*4)    IETYP   = 1 => INPUT VELOCITY     CM S-1
C                           = 2 => INPUT VELOCITY     AT.UNITS
C                           = 3 => INPUT ENERGY       EV/AMU
C  OUTPUT: (I*4)    IEVAL   = NUMBER OF INPUT COLLISION ENERGIES (1->24)
C  OUTPUT: (R*8)    EIN()   = USER ENTERED ISPF VALUES -
C                             COLLISION ENERGIES (UNITS: SEE 'IETYP')
C                             DIMENSION: COLLISION ENERGY INDEX
C  OUTPUT: (I*4)    ITTYP   = 1 => UNSPECIFIED
C                           = 2 => UNSPECIFIED
C                           = 3 => UNSPECIFIED
C  OUTPUT: (I*4)    ITVAL   = NUMBER OF INPUT TEMPERATURES (1->12)
C  OUTPUT: (R*8)    TIN()   = USER ENTERED ISPF VALUES -
C                             TEMPERATURES (UNITS: SEE 'IETYP')
C                             DIMENSION: TEMPERATURE INDEX
C
C  OUTPUT: (L*4)    LFSEL   = .TRUE.  => CARRY OUT MINIMAX POLYNOMIAL
C                                        FITTING
C                           = .FALSE. => - DO NOT DO THE ABOVE -
C  OUTPUT: (L*4)    LOSEL   = .TRUE.  => ENERGIES FOR TEXT OUTPUT SET
C                           = .FALSE. => ENERGIES FOR TEXT OUTPUT NOT SET
C  OUTPUT: (L*4)    LTSEL   = .TRUE.  => TEMPS. FOR TCX FILE SET
C                           = .FALSE. => TEMPS. FOR TCX FILE NOT SET
C
C  OUTPUT: (R*8)    TOLVAL  = FRACTIONAL TOLERANCE FOR MINIMAX FIT
C                             (=0 IF MINIMAX FIT NOT SELECTED)
C
C          (I*4)    PIPEIN  = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)    PIPEOU  = PARAMETER = UNIT NUMBER FOR INPUT TO PIPE
C	   (I*4)    I	    = LOOP COUNTER
C          (I*4)    J       = LOOP COUNTER
C          (I*4)    K       = LOOP COUNTER
C 	   (I*4)    ILOGIC  = RETURN VALUE FROM IDL WHICH IS USED TO 
C			      REPRESENT A LOGICAL VARIABLE SINCE IDL DOES
C			      HAVE SUCH DATA TYPES.
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C	   XXFLSH    IDL-ADAS	CALLS XXFLSH COMMAND TO CLEAR PIPE.
C
C
C AUTHOR:  H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C          JA8.08
C          TEL.  0141-553-4196
C
C DATE:    12/11/96
C
C UPDATE:  
C
C VERSION: 1.1				DATE: 19-11-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C	    - PUT UNDER S.C.C.S. CONTROL
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER     L24         , L12
      INTEGER     L3
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      PARAMETER ( L24 = 24      , L12    = 12   , L3     = 3 )
C-----------------------------------------------------------------------
      INTEGER    IPAN          , I4UNIT        ,
     &           NSTORE        , NEDIM         , NDEIN        ,
     &           NDTIN         , ITTYP         , ITVAL        ,
     &           NBSEL         , IBSEL         , IETYP        , IEVAL
      INTEGER    IFORM         , IBLK
C-----------------------------------------------------------------------
      REAL*8     TOLVAL        , AMD           , AMR           
C-----------------------------------------------------------------------
      LOGICAL    LPEND         , LFSEL
      LOGICAL    LOSEL         , LTSEL
C-----------------------------------------------------------------------
      CHARACTER  TITLE*40
C-----------------------------------------------------------------------
      INTEGER    IEA(NSTORE)
C-----------------------------------------------------------------------
      REAL*8     EIN(NDEIN)               , TIN(NDTIN)
      REAL*8     EVALS(NEDIM,3,NSTORE)
      REAL*8     TVALS(NEDIM,3,NSTORE)
C-----------------------------------------------------------------------
      CHARACTER  CTYPE(NSTORE)*3          , CSECDY(NSTORE)*5         ,
     &           CPRIMY(NSTORE)*5
C-----------------------------------------------------------------------
      INTEGER    PIPEIN        , PIPEOU   ,
     &           I             , J        , K                        ,
     &           ILOGIC
      PARAMETER  (PIPEIN = 5, PIPEOU = 6)
C-----------------------------------------------------------------------
C  WRITE OUTPUTS TO PIPE FOR IDL TO READ
C-----------------------------------------------------------------------
C
	WRITE(PIPEOU,*)      NSTORE
	CALL XXFLSH(PIPEOU)
	WRITE(PIPEOU,*)      NEDIM
	CALL XXFLSH(PIPEOU)
	WRITE(PIPEOU,*)      NDEIN
	CALL XXFLSH(PIPEOU)
	WRITE(PIPEOU,*)      NDTIN
	CALL XXFLSH(PIPEOU)
	WRITE(PIPEOU,*)      NBSEL
	CALL XXFLSH(PIPEOU)
        DO 1, I=1, NBSEL
            WRITE(PIPEOU,'(A5)') CPRIMY(I)
	    CALL XXFLSH(PIPEOU)
1       CONTINUE
        DO 2, I=1, NBSEL
	    WRITE(PIPEOU,'(A5)') CSECDY(I)
	    CALL XXFLSH(PIPEOU)
2       CONTINUE
        DO 4, I=1, NBSEL
	    WRITE(PIPEOU,'(A3)') CTYPE(I)
	    CALL XXFLSH(PIPEOU)
4       CONTINUE
        DO 6, I=1, NBSEL
 	    WRITE(PIPEOU,*) IEA(I)
	    CALL XXFLSH(PIPEOU)
6       CONTINUE
        DO 9, K=1, NBSEL
            DO 8, J=1, 3
                DO 7, I=1, IEA(K)
	            WRITE(PIPEOU,'(E12.4)') EVALS(I,J,K)
	            CALL XXFLSH(PIPEOU)
7               CONTINUE
8           CONTINUE
9       CONTINUE
C
C-----------------------------------------------------------------------
C  READ INPUTS FROM PIPE FROM IDL
C-----------------------------------------------------------------------
C
	READ(PIPEIN, *) ILOGIC
	IF (ILOGIC .EQ. 1 ) THEN 
	   LPEND = .TRUE.
	ELSE 
	   LPEND = .FALSE.
	END IF
C
	READ(PIPEIN,'(A40)') TITLE
C
	READ(PIPEIN,*) IBSEL

C --------- CHANGE IDL INDEX TO FORTRAN INDEX BY ADDING 1 ---------

        IBSEL = IBSEL+1
C
	READ(PIPEIN,*) AMD
C
	READ(PIPEIN,*) AMR
C
	READ(PIPEIN,*) IETYP
C
	READ(PIPEIN,*) IEVAL
C
        DO 10, I=1, IEVAL
	    READ(PIPEIN,*) EIN(I)
10      CONTINUE
C
	READ(PIPEIN,*) ITTYP
C
	READ(PIPEIN,*) ITVAL
C
        DO 11, I=1, ITVAL
	    READ(PIPEIN,*) TIN(I)
11      CONTINUE
C
	READ(PIPEIN,*) ILOGIC
	IF (ILOGIC .EQ. 1) THEN
	   LFSEL = .TRUE.
	ELSE
	   LFSEL = .FALSE.
	END IF
C
	READ(PIPEIN,*) TOLVAL
	TOLVAL = TOLVAL / 100.0
C
	READ(PIPEIN, *) ILOGIC
	IF (ILOGIC .EQ. 1 ) THEN 
	   LOSEL = .TRUE.
	ELSE 
	   LOSEL = .FALSE.
	END IF
C
	READ(PIPEIN, *) ILOGIC
	IF (ILOGIC .EQ. 1 ) THEN 
	   LTSEL = .TRUE.
	ELSE 
	   LTSEL = .FALSE.
	END IF
C
C
        DO 21 IBLK=1,NBSEL
          DO 20 IFORM=1,3
            CALL C2ECON( L3          , IFORM                ,
     &                   AMD         , AMR                  ,
     &                   IEA(IBLK)   , EVALS(1,L3, IBLK)    ,
     &                                 TVALS(1,IFORM,IBLK)
     &                 )
20        CONTINUE
21      CONTINUE
C
C-----------------------------------------------------------------------
C
      RETURN
      END
