C UNIX-IDL PORT - SCCS INFO: MODULE @(#)$Header: /home/adascvs/fortran/adas3xx/adas302/c2out0.for,v 1.1 2004/07/06 11:48:43 whitefor Exp $ DATE $Date: 2004/07/06 11:48:43 $
C
       SUBROUTINE C2OUT0( IWRITE   , LFSEL  ,
     &                    TITLE    , TITLX  , TITLM ,
     &                    DATE     , CADAS  ,             
     &                    IBSEL    , IEVAL  ,
     &                    ESYM     , IZ0    , IZ    , IZ1    ,
     &                    CPRIMY   , CSECDY ,
     &                    CTYPE  ,
     &                    LERNG    ,
     &                    VCMS     , VATU   , EEVA  ,
     &                    SIAA     ,
     &                    KPLUS1   , COEF
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C2OUT0 *********************
C
C  PURPOSE:  TO  PRINT DATA CONCERNING THE SELECTED CROSS-SECTION
C            DATA BLOCK UNDER ANALYSIS.
C
C  CALLING PROGRAM: ADAS302
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IWRITE  = OUTPUT UNIT FOR RESULTS
C  INPUT : (L*4)  LFSEL   = .TRUE.  => MINIMAX POLYNOMIAL FITTED.
C                           .FALSE. => MINIMAX POLYNOMIAL NOT FITTED
C
C  INPUT : (C*40) TITLE   = TITLE OF RUN (READ FROM ISPF PANEL)
C  INPUT : (C*120)TITLX   = INFORMATION STRING CONTAINING INPUT DATA-
C                           SET NAME IN BYTES 1->35.
C  INPUT : (C*80) TITLM   = DIAGNOSTIC LINE INFORMATION FROM 'MINIMAX'
C  INPUT : (C*8)  DATE    = CURRENT DATE (AS 'DD/MM/YY')
C  INPUT : (C*80) CADAS   = ADAS HEADER
C
C  INPUT : (I*4)  IBSEL   = DATA-BLOCK INDEX SELECTED FROM INPUT DATASET
C                           FOR ANALYSIS.
C  INPUT : (I*4)  IEVAL   = NUMBER OF USER ENTERED COLLISION ENERGIES
C
C  INPUT : (C*2)  ESYM    = INPUT FILE: SECONDARY - ELEMENT SYMBOL
C  INPUT : (I*4)  IZ0     = INPUT FILE: SECONDARY - NUCLEAR CHARGE
C  INPUT : (I*4)  IZ      = INPUT DATA FILE - SELECTED DATA-BLOCK:
C                           SECONDARY SPECIES CHARGE
C  INPUT : (I*4)  IZ1     = INPUT DATA FILE - SELECTED DATA-BLOCK:
C                           SECONDARY SPECEIS EFFECTIVE CHARGE
C
C  INPUT : (C*9)  CPRIMY  = INPUT DATA FILE - SELECTED DATA-BLOCK:
C                           PRIMARY SPECIES INDENTIFICATION
C  INPUT : (C*9)  CSECDY  = INPUT DATA FILE - SELECTED DATA-BLOCK:
C                           SECONDARY SPECIES IDENTIFICATION
C  INPUT : (C*2)  CTYPE   = INPUT DATA FILE - SELECTED DATA-BLOCK:
C                           CROSS-SECTION TYPE
C
C  INPUT : (L*4)  LERNG()=  .TRUE.  => OUTPUT 'SIAA()' VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      COLLISION ENERGIES 'EEVA()'.
C                           .FALSE. => OUTPUT 'SIAA()' VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      COLLISION ENERGIES 'EEVA()'.
C                           DIMENSION: COLLISION ENERGY INDEX
C
C  INPUT : (R*8)  VCMS()  = USER ENTERED: COLLISION VELOCITY (cm s-1)
C                           DIMENSION: COLLISION ENERGY INDEX
C  INPUT : (R*8)  VATU()  = USER ENTERED: COLLISION VELOCITY (At. Un.)
C                           DIMENSION: COLLISION ENERGY INDEX
C  INPUT : (R*8)  EEVA()  = USER ENTERED: COLLISION ENERGY (EV/AMU)
C                           DIMENSION: COLLSION ENERGY INDEX
C
C  INPUT : (R*8)  SIAA()  = SPLINE INTERPOLATED OR  EXTRAPOLATED CX
C                           COLLSION CROSS-SECTIONS FOR
C                           THE USER ENTERED COLLISION ENERGIES.
C                           DIMENSION: COLLISION ENERGY INDEX
C
C  INPUT : (I*4)  KPLUS1  = NUMBER OF MINIMAX COEFFICIENTS
C  INPUT : (R*8)  COEF()  = COEFFICIENTS OF FITTED MINIMAX POLYNOMIAL
C
C          (I*4)  I       = GENERAL USE - ARRAY ELEMENT INDEX
C
C          (C*1)  C1E     = '*' IF RATE-COEFFICIENT WAS EXTRAPOLATED FOR
C                           THE CURRENT COLLISION ENERGY. (IT EQUALS
C                           ' ' IF IT WAS INTERPOLATED.)
C          (C*12) XFELEM  = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (C*12) CELEM   = RECEIVER ION  ELEMENT NAME
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XFELEM     ADAS      CHARACTER*12 FUNCTION -
C                               RETURNS ELEMENT NAME   FOR GIVEN Z0
C
C AUTHOR  : H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C           JA8.08
C           TEL. 0141-553-4196
C
C DATE:    12/11/96
C
C UNIX-IDL PORT: H.P.SUMMERS
C
C VERSION: 1.1				DATE: 19-11-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C	    - PUT UNDER S.C.C.S. CONTROL
C
C----------------------------------------------------------------------
      INTEGER    IWRITE        , IBSEL         , IEVAL      ,
     &           IZ0           , IZ            , IZ1        , KPLUS1
      INTEGER    I
C----------------------------------------------------------------------
      LOGICAL    LFSEL
C----------------------------------------------------------------------
      CHARACTER  TITLE*(*)     , TITLX*(*)     , TITLM*(*)  , ESYM*2  ,
     &           CPRIMY*5      , CSECDY*5      ,
     &           CTYPE*3       , DATE*8
      CHARACTER  XFELEM*12     ,
     &           C1E*1         ,
     &           CELEM*12      , CADAS*80
C----------------------------------------------------------------------
      REAL*8     VCMS(IEVAL)   , EEVA(IEVAL)   , VATU(IEVAL) ,
     &           SIAA(IEVAL)   ,
     &           COEF(KPLUS1)
C----------------------------------------------------------------------
      LOGICAL    LERNG(IEVAL)
C----------------------------------------------------------------------
C
C**********************************************************************
C
CX      CELEM = XFELEM( IZ0 )
C
C---------------------------------------------------------------------
C OUTPUT HEADINGS AND ION & SELECTED TRANSITION SPECIFICATIONS.
C---------------------------------------------------------------------
C
      WRITE(IWRITE,1000) CADAS(2:80)
      WRITE(IWRITE,1001)
     &     'ION/ATOM CROSS-SECTION INTERROGATION',
     &     'ADAS302' , DATE
      WRITE(IWRITE,1002) TITLE
      WRITE(IWRITE,1003) TITLX(1:120)
      WRITE(IWRITE,1004) CTYPE  ,
     &                   CPRIMY ,
     &                   CSECDY      
C
C---------------------------------------------------------------------
C OUTPUT COLLISION ENERGIES AND CX CROSS-SECTIONS
C---------------------------------------------------------------------
C
      WRITE (IWRITE,1005)
C
         DO 1 I=1,IEVAL
            C1E = '*'
            IF (LERNG(I)) C1E = ' '
            WRITE(IWRITE,1006) VCMS(I) , C1E , VATU(I) , C1E , EEVA(I) ,
     &                         SIAA(I)
    1    CONTINUE
C
      WRITE (IWRITE,1007)
      WRITE (IWRITE,1008)
      WRITE (IWRITE,1007)
C
C---------------------------------------------------------------------
C OUTPUT THE MINIMAX COEFFICIENTS (IF REQUESTED)
C---------------------------------------------------------------------
C
         IF (LFSEL) THEN
C
            WRITE(IWRITE,1009)
C
            WRITE (IWRITE,1007)
C
               DO 2 I=1,KPLUS1,2
                  IF (I.EQ.KPLUS1) THEN
                     WRITE(IWRITE,1010) I , COEF(I)
                  ELSE
                     WRITE(IWRITE,1011) I , COEF(I) , I+1 , COEF(I+1)
                  ENDIF
    2          CONTINUE
C
            WRITE(IWRITE,1012) TITLM
            WRITE(IWRITE,1007)
         ENDIF
C
C---------------------------------------------------------------------
C
 1000 FORMAT(/ / /,A79)
 1001 FORMAT( / ,16('*'),' TABULAR OUTPUT FROM ',A44,' PROGRAM: ',
     & A7,1X,'- DATE: ',A8,1X,16('*')/)
 1002 FORMAT( / ,19('-'),1X,A40,1X,19('-')/ / ,1X,
     & 'ION/ATOM COLLISION CROSS-SECTIONS AS A ',
     & 'FUNCTION OF COLLISION ENERGY    '/
     & 2X,78('-')/
     & 23X,'DATA GENERATED USING PROGRAM: ADAS302'/23X,37('-'))
 1003 FORMAT( / ,A120)
 1004 FORMAT( / ,'REACTION INFORMATION:'/
     &       1H ,25('-')/
     &        / ,'TRANSITION TYPE                =', 2X,A3  /
     &        / ,'PRIMARY:'/
     &       1H ,'   IDENTIFICATION              =', 2X,A5  /
     &        / ,'SECONDARY:'/
     &       1H ,'   IDENTIFICATION              =', 2X,A5  )
 1005 FORMAT(1H / / ,8X,'--- COLLISION ENERGIES/VELOCITIES ----',
     &               9X,'COLLISION CROSS-SECTION'/
     &           1H ,12X,'AT. UNITS.',5X,'CM S-1',5X,'EV/AMU',
     &               20X,'CM**2')
 1006 FORMAT(1H ,1P,10X,D10.3,2(1X,A,D10.3),17X,D10.3)
 1007 FORMAT(1H ,79('-'))
 1008 FORMAT(1H ,'NOTE: * => I/A CROSS-SECTION EXTRAPOLATED ',
     &                     'FOR COLLISION ENERGY VALUE')
C
 1009 FORMAT ( / / / ,'MINIMAX POLYNOMIAL FIT - TAYLOR COEFFICIENTS:'/
     &            1H ,79('-')/
     &            1H ,'- LOG10(I/A CROSS-SECTION <CM**2>) VERSUS ',
     &                'LOG10(COLLISION ENERGY <EV/AMU>) -')
 1010 FORMAT (  12X,'A(',I2,') = ',1P,D17.9 )
 1011 FORMAT (2(12X,'A(',I2,') = ',1P,D17.9))
 1012 FORMAT ( / ,A79)
C
C---------------------------------------------------------------------
C
      RETURN
      END
