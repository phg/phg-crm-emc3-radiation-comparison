C UNIX-IDL PORT - SCCS INFO: MODULE @(#)$Header: /home/adascvs/fortran/adas3xx/adas302/sigia.for,v 1.2 2007/05/17 17:02:20 allan Exp $ DATE $Date: 2007/05/17 17:02:20 $
C
      SUBROUTINE SIGIA ( LSETX    , LPASS    ,    
     &                   ALPH     , ETH      , ILTYP    , IOPT     ,
     &                   NENIN    , ENIN     , SGIN     ,
     &                   LTHETA   , VREL     , XSEC
     &                 )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ********************** FORTRAN77 SUBROUTINE: SIGIA ******************
C
C  VERSION: 1.0 (ADAS91)
C
C  PURPOSE:  INTERPOLATES CROSS-SECTION DATA FROM AN INPUT VECTOR OF
C            VALUES USING CUBIC SPLINES.
C  
C  EXTRAPOLATES FOR RELATIVE SPEEDS OUT OF DATA RANGE ACCORDING TO
C  VARIOUS TYPES (ILTYP).  LOGARITHMIC INTERPOLATION MAY BE USED
C  (LPASS). SPEED ECONOMY IS POSSIBLE FOR REPEATS WITH THE SAME
C  SPLINE KNOTS (LSETX).
C  
C
C  CALLING PROGRAM:  CXTHER
C
C  NOTES:
C        (1) FOR  ILTYP.EQ.0, EXTRAPOLATION IS AS FOLLOWS:
C
C
C  SUBROUTINE:
C
C  INPUT : (L*4)   LSETX    = .TRUE. => SPLINE NOT SET FOR THESE KNOTS
C                             .FLSE. => SPLINE NOT FOR THESE KNOTS
C  INPUT : (L*4)   LPASS    = .TRUE. => DO NOT CONVERT INTO LOG10 FOR
C                                       ENERGIES AND X-SECTS. FOR SPLINE
C                             .FLSE. => CONVERT INTO LOG10 FOR
C                                       ENERGIES AND X-SECTS. FOR SPLINE
C  INPUT : (R*8)   ALPH     = HIGH ENERGY EXTRAPOLATION PARAMETER
C  INPUT : (R*8)   ETH      = THRESHOLD ENERGY (RYD.)
C  INPUT : (I*4)   ILTYP    = TYPE FOR LOW AND HIGH ENERGY CROSS-
C                             -SECTION EXTRAPOLATION.
C  INPUT : (I*4)   IOPT     = SPLINE END POINT CURVATURE/GRADIENT OPTION
C                             1 => DDY1 = 0, DDYN = 0
C                             4 => DY1 = 0 , DDYN = 0
C
C  INPUT : (I*4)   NENIN    = NUMBER OF ENERGIES IN INPUT DATA SET
C  INPUT : (R*8)   ENIN()   = ENERGIES (EV/AMU) IN INPUT DATA SET
C  INPUT : (R*8)   SGIN()   = INPUT X-SECTIONS (CM2) FROM INPUT DATA SET
C                             1ST.DIM: ENERGY INDEX
C  INPUT : (I*4)   LTHETA   = NUMBER OF VALUES IN VREL VECTOR
C  INPUT : (R*8)   VREL()   = RELATIVE SPEEDS FOR OUTPUT (CM S-1)
C
C  OUTPUT: (R*8)   XSEC()   = OUTPUT CROSS-SECTION (CM2)
C
C          (I*4)   MAXENS   = PARAMETER = MAX. LENGTH OF TABULAR XSECT.
C                                         VECTOR
C          (I*4)   LDTHET   = PARAMETER = MAX. LENGTH OF INTERNAL
C                                         VECTORS
C          (R*8)   CMSAMU   = PARAMETER = CONVERSION FACTOR FOR ENERGY
C                                         (AMU) TO VELOCITY (CM S-1)
C
C          (I*4)   I        = GENERAL INDEX
C          (I*4)   N        = GENERAL INDEX
C          (R*8)   ALPH0    = LOW VELOCITY EXTRAPOLATION PARAMETER
C          (R*8)   EXPON    = EXPONENT OF EXPONENTIAL
C          (R*8)   VSLOPE   = HIGH VELOCITY EXTRAPOLATION PARAMETER
C          (R*8)   XIN()    = INTERNAL SPLINE INDEPENDENT VARIABLE
C          (R*8)   YIN()    = INTERNAL SPLINE DEPENDENT VARIABLE
C          (R*8)   VIN()    = INTERNAL VECTOR
C          (R*8)   DY()     = DERIVATIVES AT SPLINE KNOTS
C          (R*8)   XOUT()   = INTERNAL OUTPUT INDEPENDENT VARIABLE
C          (R*8)   YOUT()   = INTERNAL OUTPUT DEPENDENT VARIABLE
C          (L*4)   LINTRP() = .TRUE.  => POINT INTERPOLATED
C                           = .FALSE. => POINT EXTRAPOLATED
C
C
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          XXSPLE     ADAS      INTERPOLATES USING CUBIC SPLINES
C          R8FUN1     ADAS      EXTERNAL FUNCTION FOR XXSPLE
C
C
C  AUTHOR:  H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C           JA8.08
C           TEL. 0141-553-4196
C
C  DATE:    18/11/96
C
C VERSION: 1.1				DATE: 18-11-96
C MODIFIED: HUGH P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C	    - FIRST EDITION
C
C VERSION: 1.1				DATE: 17-05-07
C MODIFIED: Allan Whiteford
C	    - Modified comments as part of subroutine documentation
C             procedure.
C
C
C--------------------------------------------------------------------
      INTEGER  MAXENS   , LDTHET
C--------------------------------------------------------------------
      REAL*8   CMSAMU   , ZERO
C--------------------------------------------------------------------
      PARAMETER ( MAXENS = 50 , LDTHET = 181 )
      PARAMETER ( CMSAMU = 1.389209D+06      , ZERO = 1.0D-30     )
C--------------------------------------------------------------------
      INTEGER  I      , N      , NENIN
      INTEGER  IOPT   , LTHETA , ILTYP  , I4UNIT
C--------------------------------------------------------------------
      REAL*8   ALPH0  , VSLOPE , EXPON
      REAL*8   R8FUN1 , ALPH   , ETH
C--------------------------------------------------------------------
      LOGICAL  LSETX  , LPASS
C--------------------------------------------------------------------
      REAL*8   VREL(LTHETA)    , XSEC(LTHETA)
      REAL*8   XOUT(LDTHET)    , YOUT(LDTHET)
      REAL*8   ENIN(NENIN)     , SGIN(NENIN)     , DY(MAXENS)
      REAL*8   XIN(MAXENS)     , YIN(MAXENS)     , VIN(MAXENS)
C--------------------------------------------------------------------
      LOGICAL  LINTRP(LDTHET)
C--------------------------------------------------------------------
      EXTERNAL R8FUN1
C--------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C  SET PARAMETERS FOR CROSS-SECTION EXTRAPOLATION DEPENDING ON ILTYP
C-----------------------------------------------------------------------
C
      IF(ILTYP.EQ.1) THEN
          ALPH0  = 0.0
          VSLOPE = 7.0
      ELSE
          ALPH0  = 0.0
          VSLOPE = 7.0
      ENDIF
C
C-----------------------------------------------------------------------
C  SET LOGARITHMIC VALUES FOR VARIABLES FOR INTERPOLATION IF REQUIRED
C-----------------------------------------------------------------------
C
      IF(.NOT.LPASS) THEN
          DO 10 N = 1,NENIN
            YIN(N) = DLOG10(SGIN(N)+ZERO)
 10       CONTINUE
          IF(LSETX) THEN
              DO 15 N = 1,NENIN
                VIN(N) = CMSAMU*DSQRT(ENIN(N))
                XIN(N) = DLOG10(VIN(N))
 15           CONTINUE
          ENDIF
C
          DO 20 I = 1,LTHETA
            XOUT(I) = DLOG10(VREL(I))
 20       CONTINUE
      ELSE
          DO 30 N = 1,NENIN
            YIN(N) = SGIN(N)+ZERO
 30       CONTINUE
          IF(LSETX) THEN
              DO 35 N = 1,NENIN
                VIN(N) = CMSAMU*DSQRT(ENIN(N))
                XIN(N) = VIN(N)
 35           CONTINUE
          ENDIF
C
          DO 40 I = 1,LTHETA
            XOUT(I) = VREL(I)
 40       CONTINUE
      ENDIF
C
      CALL XXSPLE ( LSETX   , IOPT   , R8FUN1   ,
     &              NENIN   , XIN    , YIN      ,
     &              LTHETA  , XOUT   , YOUT     ,
     &              DY      , LINTRP
     &             )
C
      DO 50 I = 1,LTHETA
        IF ( LINTRP(I).AND.(.NOT.LPASS) ) THEN
            XSEC(I)    = 10.0**YOUT(I)
        ELSEIF ( LINTRP(I).AND.LPASS )    THEN
            XSEC(I)    = YOUT(I)
        ELSEIF(.NOT.LINTRP(I)) THEN
            IF(XOUT(I).GT.XIN(NENIN)) THEN
                XSEC(I)    = SGIN(NENIN)*(VIN(NENIN)/VREL(I))**VSLOPE
            ELSEIF(XOUT(I).LT.XIN(1)) THEN
                IF(VREL(I).LE.0.0)THEN
                    XSEC(I)=1.0D-74
                ELSE
                    EXPON=ALPH0*(1.0/VIN(1)-1.0/VREL(I))
                    IF(EXPON.LT.-173.0D0)THEN
                        XSEC(I)=1.0D-74
                    ELSE
                        XSEC(I)    = SGIN(1)*DEXP(EXPON)
                    ENDIF
                ENDIF
c*************  vrel(1) can be zero *******************!!!! catch!!!!
            ENDIF
        ENDIF
   50 CONTINUE
C
      RETURN
      END
