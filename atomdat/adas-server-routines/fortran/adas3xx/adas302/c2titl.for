C UNIX-IDL PORT - SCCS INFO: MODULE @(#)$Header: /home/adascvs/fortran/adas3xx/adas302/c2titl.for,v 1.1 2004/07/06 11:49:15 whitefor Exp $ DATE $Date: 2004/07/06 11:49:15 $
C
      SUBROUTINE C2TITL( IBSEL  , DSFULL ,
     &                   CPRIMY , CSECDY ,
     &                   CTYPE ,
     &                   TITLX
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C2TITL *********************
C
C  PURPOSE:  TO CREATE THE DESCRIPTIVE TITLE FOR SELECTED DATA-BLOCK.
C
C  CALLING PROGRAM: ADAS302/SSIA
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IBSEL    = SELECTED DATA-BLOCK: INDEX
C  INPUT : (C*80) DSFULL   = FULL MVS INPUT DATA SET NAME
C
C  INPUT : (C*5)  CPRIMY   = SELECTED DATA-BLOCK: PRIMARY SPECIES
C                                                 IDENTIFICATION.
C  INPUT : (C*5)  CSECDY   = SELECTED DATA-BLOCK: SECONDARY SPECIES
C                                                 IDENTIFICATION.
C
C  INPUT : (C*3)  CTYPE    = SELECTED DATA-BLOCK: CROSS. SECT. TYPE
C
C  OUTPUT: (C*120) TITLX    = SELECTED DATA-BLOCK: DESCRIPTIVE TITLE
C
C          (C*2)  C2       = GENERAL USE 2 BYTE  CHARACTER  STRING
C	   (I*4)  POS_NOW  = CURRENT POSITION IN TITLE STRING
C	   (I*4)  LEN_NAME = LENGTH OF FILENAME
C          (I*4)  IFIRST   = POSITION OF FIRST CHARACTER IN FILENAME
C          (I*4)  ILAST    = POSITION OF LAST CHARACTER IN FILENAME
C
C ROUTINES: 
C          XXSLEN          = UTILITY ROUTINE WHICH FINDS FIRST AND LAST
C                            NON-BLANK CHARACTERS IN A STRING.
C
C AUTHOR:  H. P. SUMMERS
C          JA8.08
C          TEL. 0141-553-4196
C
C DATE:    12/11/96
C
C UNIX-IDL PORT: H.P.SUMMERS
C
C VERSION: 1.1				DATE: 19-11-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C	    - PUT UNDER S.C.C.S. CONTROL
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER    IBSEL
      INTEGER    POS_NOW         , LEN_NAME
      INTEGER    IFIRST          , ILAST
C-----------------------------------------------------------------------
      CHARACTER  DSFULL*80       ,
     &           CPRIMY*5        , CSECDY*5       ,
     &           CTYPE*3        , TITLX*120
      CHARACTER  C2*2
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
         CALL XXSLEN(DSFULL,IFIRST,ILAST)
	 LEN_NAME = ILAST-IFIRST+1
         IF (LEN_NAME.GT.80) IFIRST = ILAST - 80
         LEN_NAME = ILAST-IFIRST+1
         TITLX(1:LEN_NAME+1) = DSFULL(IFIRST:ILAST)
         POS_NOW = LEN_NAME+1         
         WRITE(C2,1000) IBSEL
         TITLX(POS_NOW:POS_NOW+8) =  ' BLK=' // C2 // ';'
         POS_NOW = POS_NOW+9
         TITLX(POS_NOW:120) =  ' <' // CPRIMY //'>,<'// CSECDY //
     &                  '>,<' // CTYPE //  '>' 
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(I2)
C
C-----------------------------------------------------------------------
      RETURN
      END
