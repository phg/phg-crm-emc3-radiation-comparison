CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas303/c3file.for,v 1.2 2004/07/06 11:49:45 whitefor Exp $ Date $Date: 2004/07/06 11:49:45 $
CX
      SUBROUTINE C3FILE(IUNIT, ATNAME, IRCODE, DSNAME)
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C3FILE *********************
C
C  PURPOSE: TO OPEN AN 'IONATOM' DATA SET FOR ATOM 'ATNAME'
C           CURRENTLY AVAILABLE ATOMS ARE : H, HE, LI
C
C
C
C  DATA SET OPENED: 'JETUID.<GROUP>.<TYPE>
C                           (<EXTENSION>#<ATOM SYMBOL>)'
C
C  CALLING PROGRAM: SQEF
C
C  SUBROUTINE:
C
C  INPUT : (I*4)   IUNIT   = UNIT TO WHICH DATA SET WILL BE CONNECTED
C          (C*2)   ATNAME  = NAME OF ATOM
C
C  OUTPUT: (I*4)   IRCODE  = RETURN CODE FROM SUBROUTINE:
C                            0 => DATA SET SUCCESSFULLY CONNECTED
C                            1 => REQUESTED DATA  SET  MEMBER  DOES  NOT
C                                 EXISTS - DATA SET NOT CONNECTED.
C                            9 => REQUESTED DATA  SET  EXISTS BUT CANNOT
C                                 BE OPENED.
C  OUTPUT: (C*44)  DSNAME  = FULL MVS NAME OF OPENED DATA SET
C
C          (I*4)   LENF1   = FIRST NON-BLANK CHR OF 'DSNAME' GROUP PART
C          (I*4)   LENF2   = LAST  NON-BLANK CHR OF 'DSNAME' GROUP PART
C          (I*4)   LENF3   = FIRST NON-BLANK CHR OF 'DSNAME' TYPE PART
C          (I*4)   LENF4   = LAST  NON-BLANK CHR OF 'DSNAME' TYPE PART
C          (I*4)   LENF5   = FIRST NON-BLANK CHR OF 'DSNAME' EXT PART
C          (I*4)   LENF6   = LAST  NON-BLANK CHR OF 'DSNAME' EXT PART
C          (I*4)   LENF7   = FIRST NON-BLANK CHR OF 'ATNAME'
C          (I*4)   LENF8   = LAST  NON-BLANK CHR OF 'ATNAME'
C
C          (C*6)   USERID  = ADAS SOURCE DATA USER ID
C          (C*8)   USRGRP  = ADAS SOURCE DATA GROUPNAME
C          (C*5)   USRTYP  = ADAS SOURCE DATA TYPENAME
C          (C*4)   USREXT  = ADAS SOURCE DATA EXTENSION
C
C          (L*4)   LEXIST  = .TRUE.  => REQUESTED  DATA  SET  EXISTS.
C                            .FALSE. => REQUESTED  DATA  SET  DOES  NOT
C                                       EXIST.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXUID      ADAS      FETCHES/SETS ADAS SOURCE DATA USER ID
C          XXSQEF     ADAS      FETCHES/SETS ADAS SOURCE DATA FILENAME
C          XXSLEN     ADAS      OBTAINS FIRST AND LAST NON-BLANK
C                               CHARACTERS IN A STRING
C
C AUTHOR:  C.J. WHITEHEAD, STRATHCLYDE
C          EXT 4205
C          BASED ON CODE IN F1FILE.F
C
C
C DATE:    24/11/94
C
C UPDATE:  15/05/95 - Tim Hammond - UNIX PORT
C                                   Added SCCS Header
C
C VERSION: 1.2						DATE: 08-11-99
C MODIFIED: RICHARD MARTIN
C		REMOVED ACTION KEYWORD FROM OPEN STATEMENT.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER     IUNIT         , IRCODE         ,
     &            LENF1         , LENF2          , LENF3      ,
     &            LENF4         , LENF5          , LENF6      ,
     &            LENF7         , LENF8
C-----------------------------------------------------------------------
      CHARACTER   USERID*6      , DSNAME*44      , ATNAME*2   ,
     &            USRGRP*8      , USRTYP*5       , USREXT*4
C-----------------------------------------------------------------------
      LOGICAL     LEXIST
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C     IDENTIFY ADAS SOURCE DATA USER ID, GROUPNAME , TYPENAME
C-----------------------------------------------------------------------
C
      USERID = '?'
      CALL XXUID(USERID)
C
      USRGRP = '?'
      USRTYP = '?'
      USREXT = '?'
      CALL XXSQEF(USRGRP , USRTYP, USREXT)
C
C-----------------------------------------------------------------------
C
      CALL XXSLEN( USRGRP , LENF1 , LENF2 )
      CALL XXSLEN( USRTYP , LENF3 , LENF4 )
      CALL XXSLEN( USREXT , LENF5 , LENF6 )
      CALL XXSLEN( ATNAME , LENF7 , LENF8 )
C
      DSNAME = '/'//USERID//'.'//USRGRP(LENF1:LENF2)//'.'
     &         //USRTYP(LENF3:LENF4)//'('//USREXT(LENF5:LENF6)
     &         //'#'//ATNAME(LENF7:LENF8)//')'
C
      INQUIRE( FILE=DSNAME , EXIST=LEXIST )
C
      IF (.NOT.LEXIST) THEN
         IRCODE = 1
      ELSE
         OPEN( UNIT=IUNIT,FILE=DSNAME,ERR=9999 )
         IRCODE = 0
      ENDIF
C
      RETURN
C
C-----------------------------------------------------------------------
C OPEN STATEMENT ERROR HANDLING
C-----------------------------------------------------------------------
C
 9999 IRCODE = 9
      RETURN
C
C-----------------------------------------------------------------------
C
      END
