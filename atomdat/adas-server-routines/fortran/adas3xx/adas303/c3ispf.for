CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas303/c3ispf.for,v 1.3 2010/10/12 15:22:58 mog Exp $ Date $Date: 2010/10/12 15:22:58 $
CX
      SUBROUTINE C3ISPF( IPAN   , LPEND  ,
     &                   NSTORE , NEDIM  ,
     &                   NDTEM  , NDDEN  , NDEIN  , NDZEF   , NDMAG  ,
     &                   NBSEL  ,
     &                   ctrans , CDONOR , CRECVR ,
     &                   CFILE  , CPCODE , CINDM  ,
     &                   ENREF  , TEREF  , DEREF  , ZEREF   , BMREF  ,
     &                   NENERA , NTEMPA , NDENSA , NZEFFA  , NBMAGA ,
     &                   EVALS  , TEMPA  , DENSA  , ZEFFA   , BMAGA  ,
     &                   TITLE  ,
     &                   IBSEL  ,
     &                   DIVAL  , TIVAL  , ZFVAL  , BMVAL   ,
     &                   IETYP  , IEVAL  , EIN    ,
     &                   LOSEL  , LFSEL  , LGRD1  , LDEF1   ,
     &                   TOLVAL ,
     &                   XL1    , XU1    , YL1    , YU1
     &                 )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C3ISPF *********************
C
C  PURPOSE:  PIPE COMMUNICATIONS WITH IDL AND TO RETURN USER SELECTED
C            OPTIONS AND VALUES
C
C  CALLING PROGRAM: ADAS303
C
C  SUBROUTINE:
C
C
C  I/O   : (L*4)   LPEND    = .TRUE.  => END ANALYSIS OF CURRENT DATA
C                                        SETS
C                           = .FALSE. => CONTINUE PANALYSIS WITH CURRENT
C                                        DATA SETS
C
C  INPUT : (I*4)   NSTORE   = MAXIMUM NUMBER OF WAVELENGTH DATA-BLOCKS
C                             WHICH CAN BE READ FROM THE INPUT DATA-SET.
C  INPUT : (I*4)   NEDIM    = MAX. NUMBER OF INPUT TEMPERATURE/DENSITY
C                             PAIRS ALLOWED.
C
C  INPUT : (I*4)   NBSEL    = NUMBER OF DATA-BLOCKS ACCEPTED & READ IN.
C
C  INPUT : (C*5)  ctrans()= INPUT DATA FILE: TRANSITION
C                           DIMENSION: DATA-BLOCK INDEX
C  INPUT : (C*8)  CDONOR()= INPUT DATA FILE: DONOR NEUTRAL ATOM
C                           DIMENSION: DATA-BLOCK INDEX
C  INPUT : (C*5)  CRECVR()= INPUT DATA FILE: RECEIVER NUCLEUS
C                           DIMENSION: DATA-BLOCK INDEX
C  INPUT : (C*8)  CFILE() = INPUT DATA FILE: SPECIFIC ION FILE SOURCE
C                           DIMENSION: DATA-BLOCK INDEX
C  INPUT : (C*8)  CPCODE()= INPUT DATA FILE: PROCESSING CODE
C                           DIMENSION: DATA-BLOCK INDEX
C  INPUT : (C*6)  CINDM() = FILE DATA FILE: EMISSION TYPE
C                           DIMENSION: DATA-BLOCK INDEX
C
C  INPUT : (R*8)   ENREF()  = REFERENCE VALUE OF ENERGY
C  INPUT : (R*8)   TEREF()  = REFERENCE VALUE OF TEMPERATURE
C  INPUT : (R*8)   DEREF()  = REFERENCE VALUE OF DENSITY
C  INPUT : (R*8)   ZEREF()  = REFERECNE VALUE OF EFFECTIVE Z
C  INPUT : (R*8)   BMREF()  = REFERECNE VALUE OF MAGNETIC FIELD
C                             1ST. DIM: NSTORE
C                             (FOR ABOVE ARRAYS)
C
C  INPUT : (I*4)   NENERA() = INPUT FILE - NO. OF BEAM ENERGIES
C  INPUT : (I*4)   NTEMPA() = NUMBER OF TEMPERATURES
C  INPUT : (I*4)   NDENSA() = NUMBER OF DENSITIES
C  INPUT : (I*4)   NZEFFA() = NUMBER OF EFFECTIVE Z'S
C  INPUT : (I*4)   NBMAGA() = NUMBER OF MAGNETIC FIELD VALUES
C                             1ST. DIM: NSTORE
C                             (FOR ABOVE ARRAYS)
C
C  INPUT : (R*8)   EVALS(,,)= INPUT DATA FILE: BEAM ENERGIES
C                             1ST DIMENSION: BEAM ENERGY INDEX
C                             2ND DIMENSION: 1 => EV/AMU   (IETYP=1)
C                                            2 => AT UNITS (IETYP=2)
C                                            3 => CM S-1   (IETYP=3)
C                             3RD DIMENSION: DATA-BLOCK INDEX
C  INPUT : (R*8)   TEMPA(,) = TEMPERATURES
C  INPUT : (R*8)   DENSA(,) = DENSITIES
C  INPUT : (R*8)   ZEFFA(,) = EFFECTIVE Z
C  INPUT : (R*8)   BMAGA(,) = MAGNETIC FIELD
C                             1ST DIM: 12 OR 24  DEPENDING ON PARAMETER
C                             2ND DIM: NSTORE
C
C  OUTPUT: (C*40)   TITLE   = ISPF ENTERED GENERAL TITLE FOR PROGRAM RUN
C
C  OUTPUT: (I*4)    IBSEL   = SELECTED DATA-BLOCK INDEX FOR ANALYSIS
C
C  OUTPUT: (R*8)    DIVAL   = SELECTED OUTPUT ION DENSITY (CM-3)
C  OUTPUT: (R*8)    TIVAL   = SELECTED OUTPUT ION TEMPERATURE (EV)
C  OUTPUT: (R*8)    ZFVAL   = SELECTED OUTPUT Z-EFFECTIVE
C  OUTPUT: (R*8)    BMVAL   = SELECTED OUTPUT MAGNETIC FIELD (T)
C
C  OUTPUT: (I*4)    IETYP   = 1 => INPUT BEAM ENERGIES IN EV/AMU
C                           = 2 => INPUT BEAM ENERGIES IN AT. UNITS
C                           = 3 => INPUT BEAM ENERGIES IN CM S-1
C  OUTPUT: (I*4)    IEVAL   = NUMBER OF INPUT TEMP/DENSITY PAIRS (1->20)
C  OUTPUT: (R*8)    EIN()   = USER ENTERED ISPF VALUES -
C                             BEAM ENERGY VALUES (UNITS: SEE 'IETYP')
C                             DIMENSION: BEAM ENERGY INDEX
C
C  OUTPUT: (L*4)    LOSEL   = .TRUE.  => CALCULATE INTERPOLATED VALUES
C                                        FOR OUTPUT. (ALWAYS IS .TRUE.)
C  OUTPUT: (L*4)    LFSEL   = .TRUE.  => CARRY OUT MINIMAX POLYNOMIAL
C                                        FITTING
C                           = .FALSE. => - DO NOT DO THE ABOVE -
C  OUTPUT: (L*4)    LGRD1   = .TRUE.  => PUT GRAPH IN GRID FILE
C                           = .FALSE. => DO NOT PUT GRAPH IN GRID FILE
CX UNIX PORT - LGRD1 ONLY USED TO KEEP ARGUMENT LIST THE SAME
C  OUTPUT: (L*4)    LDEF1   = .TRUE.  => USE GRAPH DEFAULT SCALING
C                           = .FALSE. => DO NOT USE DEFAULT SCALING
CX UNIX PORT - AXES SCALING CHOSEN LATER UNDER IDL-ADAS
C
C  OUTPUT: (R*8)    TOLVAL  = FRACTIONAL TOLERANCE FOR MINIMAX FIT
C                             (=0 IF MINIMAX FIT NOT SELECTED)
C
C  OUTPUT: (R*8)    XL1     = LOWER LIMIT FOR X-AXIS OF GRAPH
C  OUTPUT: (R*8)    XU1     = UPPER LIMIT FOR X-AXIS OF GRAPH
C  OUTPUT: (R*8)    YL1     = LOWER LIMIT FOR Y-AXIS OF GRAPH
C  OUTPUT: (R*8)    YU1     = UPPER LIMIT FOR Y-AXIS OF GRAPH
C
C          (I*4)   PIPEIN - PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)   PIPEOU - PARAMETER = UNIT NUMBER FOR INPUT FROM PIPE
C          (I*4)   LOGIC  - USED TO PIPE LOGICAL VALUES
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXFLSH     IDL_ADAS  calls 'flush' to clear the pipe.
C
C
C AUTHOR:  H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C          JA8.08
C          TEL. 0141-553-4196
C
C DATE:    20/04/95
C
C UPDATE:  15/05/95 - Tim Hammond - UNIX port
C                     Routine totally reworked to communicate via
C                     IDL rather than ISPF.
C
C UPDATE:  07/03/08 - Allan Whiteford
C                     Changed variable which descibes maximum size
C                     of panel input energies rom NDEIN to NEDIM.
C
C                     Dimensions for read datafile now received from
C                     calling program (ADAS303) but note that some of
C                     the dimensions are assumed in the IDL/Fortran
C                     communication.
C
C VERSION : 1.4                               
C DATE    : 12-12-2010
C MODIFIED: Martin O'Mullane
C           - xxdata_12 holds the transition information in ctrans 
C             rather than cwavel.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    NDTEM        , NDDEN        , NDEIN    ,
     &           NDZEF        , NDMAG
C-----------------------------------------------------------------------
      INTEGER    IPAN          ,
     &           NSTORE        , NEDIM         ,
     &           NBSEL         , IBSEL         , IETYP        , IEVAL
      INTEGER    NEMAX         , IBLAST        , ITLAST       ,
     &           IPANRC        , ILEN          , IABT
C-----------------------------------------------------------------------
      REAL*8     TOLVAL        ,
     &           XL1           , XU1           , YL1          , YU1
      REAL*8     DIVAL         , TIVAL         , ZFVAL        , BMVAL
C-----------------------------------------------------------------------
      LOGICAL    LPEND         ,
     &           LOSEL         , LFSEL         , LGRD1        , LDEF1
C-----------------------------------------------------------------------
      CHARACTER  TITLE*40
      CHARACTER  P*2           , PLAST*2       , KEY*4        ,
     &           CURPOS*8      , MSGTXT*8      , PNAME*8
C-----------------------------------------------------------------------
      INTEGER    NENERA(NSTORE)              , NTEMPA(NSTORE)          ,
     &           NDENSA(NSTORE)              , NZEFFA(NSTORE)          ,
     &           NBMAGA(NSTORE)
C-----------------------------------------------------------------------
      REAL*8     EIN(NEDIM)
      REAL*8     EVALS(NDEIN,3,NSTORE)
      REAL*8     ENREF(NSTORE) , DEREF(NSTORE) , TEREF(NSTORE),
     &           ZEREF(NSTORE) , BMREF(NSTORE) 
      REAL*8     TEMPA(NDTEM,NSTORE)         ,
     &           DENSA(NDDEN,NSTORE)         , ZEFFA(NDZEF,NSTORE)     ,
     &           BMAGA(NDMAG,NSTORE)
C-----------------------------------------------------------------------
      CHARACTER  CINDM(NSTORE)*6             , CFILE(NSTORE)*8         ,
     &           CPCODE(NSTORE)*8            , ctrans(NSTORE)*7        ,
     &           CDONOR(NSTORE)*8            , CRECVR(NSTORE)*5
C-----------------------------------------------------------------------
      INTEGER    I              , J             , LOGIC
      INTEGER    K
      INTEGER    PIPEOU         , PIPEIN
      PARAMETER (PIPEOU=6       , PIPEIN=5)
C-----------------------------------------------------------------------
C WRITE VARIABLES OUT TO IDL VIA PIPE
C-----------------------------------------------------------------------
C
      DO J = 1, NBSEL
          WRITE(PIPEOU, *) TEREF(J)
      ENDDO
      CALL XXFLSH(PIPEOU)
      DO J = 1, NBSEL
          WRITE(PIPEOU, *) DEREF(J)
      ENDDO
      CALL XXFLSH(PIPEOU)
      DO J = 1, NBSEL
          WRITE(PIPEOU, *) ZEREF(J)
      ENDDO
      CALL XXFLSH(PIPEOU)
      DO J = 1, NBSEL
          WRITE(PIPEOU, *) BMREF(J)
      ENDDO
      CALL XXFLSH(PIPEOU)

      DO J = 1, NBSEL
          WRITE(PIPEOU, *) NENERA(J)
          WRITE(PIPEOU, *) NTEMPA(J)
          WRITE(PIPEOU, *) NDENSA(J)
          WRITE(PIPEOU, *) NZEFFA(J)
          WRITE(PIPEOU, *) NBMAGA(J)
      ENDDO
      CALL XXFLSH(PIPEOU)

      DO J = 1, 24
          DO I = 1, NBSEL
              WRITE(PIPEOU, *) DENSA(J,I)
          ENDDO
      ENDDO
      CALL XXFLSH(PIPEOU)
      DO J = 1, 24
          DO K = 1, 3
              DO I = 1, NBSEL
                  WRITE(PIPEOU, *) EVALS(J,K,I)
              ENDDO
          ENDDO
      ENDDO
      CALL XXFLSH(PIPEOU)
      DO J = 1, 12
          DO I = 1, NBSEL
              WRITE(PIPEOU, *) TEMPA(J,I)
              WRITE(PIPEOU, *) ZEFFA(J,I)
              WRITE(PIPEOU, *) BMAGA(J,I)
          ENDDO
      ENDDO
      CALL XXFLSH(PIPEOU)

      DO J = 1, NBSEL
          WRITE(PIPEOU, '(A8)') CPCODE(J)
      ENDDO
      CALL XXFLSH(PIPEOU)
      DO J = 1, NBSEL
          WRITE(PIPEOU, '(A8)') CDONOR(J)
      ENDDO
      CALL XXFLSH(PIPEOU)
      DO J = 1, NBSEL
          WRITE(PIPEOU, '(A5)') CRECVR(J)
      ENDDO
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU, '(A5)') (ctrans(J) , J =1, NBSEL)
      CALL XXFLSH(PIPEOU)
      DO J = 1, NBSEL
          WRITE(PIPEOU, '(A8)') CFILE(J)
      ENDDO
      CALL XXFLSH(PIPEOU)
      DO J = 1, NBSEL
          WRITE(PIPEOU, '(A6)') CINDM(J)
      ENDDO
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU, *) NEDIM
      CALL XXFLSH(PIPEOU)
C-----------------------------------------------------------------------
C READ OUTPUTS FROM PIPE FROM IDL
C-----------------------------------------------------------------------
      READ(PIPEIN, *) LOGIC
      IF (LOGIC .EQ. 1 ) THEN
         LPEND = .TRUE.
      ELSE
         LPEND = .FALSE.
      END IF
C
      READ(PIPEIN,'(A)') TITLE
C
      READ(PIPEIN,*) IBSEL
C CHANGE IDL INDEX TO FORTRAN INDEX BY ADDING 1
      IBSEL = IBSEL+1

      READ(PIPEIN,*) IETYP
      READ(PIPEIN,*) IEVAL
      READ(PIPEIN,*) (EIN(I), I=1,NEDIM)
      READ(PIPEIN,*) LOGIC
      IF (LOGIC .EQ. 1) THEN
         LFSEL = .TRUE.
      ELSE
         LFSEL = .FALSE.
      END IF
C
      READ(PIPEIN,*) TOLVAL
      TOLVAL = TOLVAL / 100.0

      READ(PIPEIN,*) DIVAL
      READ(PIPEIN,*) TIVAL
      READ(PIPEIN,*) ZFVAL
      READ(PIPEIN,*) BMVAL

      RETURN
      END
