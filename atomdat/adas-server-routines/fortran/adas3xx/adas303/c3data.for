CX UNIX PORT - SCCS Info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas303/c3data.for,v 1.3 2004/12/02 14:54:07 mog Exp $ Date $Date: 2004/12/02 14:54:07 $
CX
      SUBROUTINE C3DATA( IUNIT  , DSNAME ,
     &                   NSTORE ,
     &                   NBSEL  , ISELA  ,
     &                   CWAVEL , CDONOR , CRECVR ,
     &                   CFILE  , CPCODE , CINDM  ,
     &                   QEFREF ,
     &                   ENREF  , TEREF  , DEREF  , ZEREF   , BMREF  ,
     &                   NENERA , NTEMPA , NDENSA , NZEFFA  , NBMAGA ,
     &                   ENERA  , TEMPA  , DENSA  , ZEFFA   , BMAGA  ,
     &                   QENERA , QTEMPA , QDENSA , QZEFFA  , QBMAGA
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C3DATA *********************
C
C  PURPOSE : TO FETCH DATA FROM INPUT QEF DATA SET.
C
C  CALLING PROGRAM: ADAS303, SQEF
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNIT     = UNIT NUMBER TO READ FROM
C  INPUT : (C*80) DSNAME    = MVS DATA SET NAME OF DATA SET BEING READ
C  INPUT : (I*4)  NSTORE    = MAXIMUM NUMBER OF DATA BLOCKS ALLOWED
C
C  OUTPUT: (I*4)  NBSEL     = NUMBER OF BLOCKS PRESENT
C  OUTPUT: (I*4)  ISELA()   = INDEX NUMBER OF DATA BLOCK
C
C  OUTPUT: (C*5)  CWAVEL()= INPUT DATA FILE: TRANSITION
C                           DIMENSION: DATA-BLOCK INDEX
C  OUTPUT: (C*8)  CDONOR()= INPUT DATA FILE: DONOR NEUTRAL ATOM
C                           DIMENSION: DATA-BLOCK INDEX
C  OUTPUT: (C*5)  CRECVR()= INPUT DATA FILE: RECEIVER NUCLEUS
C                           DIMENSION: DATA-BLOCK INDEX
C  OUTPUT: (C*8)  CFILE() = INPUT DATA FILE: SPECIFIC ION FILE SOURCE
C                           DIMENSION: DATA-BLOCK INDEX
C  OUTPUT: (C*8)  CPCODE()= INPUT DATA FILE: PROCESSING CODE
C                           DIMENSION: DATA-BLOCK INDEX
C  OUTPUT: (C*6)  CINDM() = FILE DATA FILE: EMISSION TYPE
C                           DIMENSION: DATA-BLOCK INDEX
C
C  OUTPUT: (R*8)  QEFREF()  = REFERENCE VALUE OF RATE COEFFICIENT
C  OUTPUT: (R*8)  ENREF()   =     "       "   "  ENERGY
C  OUTPUT: (R*8)  TEREF()   =     "       "   "  TEMPERATURE
C  OUTPUT: (R*8)  DEREF()   =     "       "   "  DENSITY
C  OUTPUT: (R*8)  ZEREF()   =     "       "   "  EFFECTIVE Z
C  OUTPUT: (R*8)  BMREF()   =     "       "   "  MAGNETIC FIELD
C  OUTPUT: (I*4)  NENERA()  = NUMBER OF ENERGIES
C  OUTPUT: (I*4)  NTEMPA()  = NUMBER OF TEMPERATURES
C  OUTPUT: (I*4)  NDENSA()  = NUMBER OF DENSITIES
C  OUTPUT: (I*4)  NZEFFA()  = NUMBER OF EFFECTIVE Z'S
C  OUTPUT: (I*4)  NBMAGA()  = NUMBER OF MAGNETIC FIELD VALUES
C                             1ST. DIM: NSTORE
C                             (FOR ABOVE ARRAYS)
C
C  OUTPUT: (R*8)  ENERA(,)  = ENERGIES
C  OUTPUT: (R*8)  QENERA(,) = RATE COEFFICIENTS FOR ENERGY VALUE
C  OUTPUT: (R*8)  TEMPA(,)  = TEMPERATURES
C  OUTPUT: (R*8)  QTEMPA(,) = RATE COEFFICIENTS FOR TEMPERATURES
C  OUTPUT: (R*8)  DENSA(,)  = DENSITIES
C  OUTPUT: (R*8)  QDENSA(,) = RATE COEFFICIENTS FOR DESNITIES
C  OUTPUT: (R*8)  ZEFFA(,)  = EFFECTIVE Z
C  OUTPUT: (R*8)  QZEFFA(,) = RATE COEFFICIENTS FOR EFFECTIVE Z
C  OUTPUT: (R*8)  BMAGA(,)  = MAGNETIC FIELD
C  OUTPUT: (R*8)  QBMAGA(,) = RATE COEFFICIENTS FOR MAGNETIC FIELDS
C                             1ST DIM: 12 OR 24  DEPENDING ON PARAMETER
C                             2ND DIM: NSTORE
C
C  ROUTINES:
C           ROUTINE    SOURCE    BRIEF DESCRIPTION
C           ------------------------------------------------------------
C           I4UNIT     ADAS      FETCHES FILE HANDLE FOR ERROR MESSAGE
C
C  AUTHOR:  H P SUMMERS, UNIVERSITY OF STRATHCLDYE
C           JA 8.08
C           TEL.  0141-553-4196
C
C  DATE:    19/04/95
C
C  UPDATE:  04/05/95 TIM HAMMOND - UNIX PORT
C                                  Increased DSNAME from 44 -> 80
C
C  UPDATE:  15/05/95 TIM HAMMOND - UNIX PORT
C                                  Changed delimiter character from
C				   '\' to '!' as otherwise will not
C				   compile.
C
C
C VERSION : 1.2
C DATE    : 02-12-2004
C MODIFIED: Martin O'Mullane
C	       - Warn user that the routine is now deprecated
C                and that xxdata_12 should be used instead.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER IUNIT  , ISTORE , NSTORE  , NBSEL  , J
      INTEGER I4UNIT
C-----------------------------------------------------------------------
      CHARACTER DSNAME*80                   , STRING*80
      CHARACTER CINDM(NSTORE)*6             , CFILE(NSTORE)*8         ,
     &          CPCODE(NSTORE)*8            , CWAVEL(NSTORE)*5        ,
     &          CDONOR(NSTORE)*8            , CRECVR(NSTORE)*5
C-----------------------------------------------------------------------
      INTEGER ISELA(NSTORE)
      INTEGER NENERA(NSTORE), NTEMPA(NSTORE), NDENSA(NSTORE)
      INTEGER NZEFFA(NSTORE), NBMAGA(NSTORE)
C-----------------------------------------------------------------------
      REAL*8 QEFREF(NSTORE)
      REAL*8 ENREF(NSTORE) , TEREF(NSTORE)
      REAL*8 DEREF(NSTORE) , ZEREF(NSTORE)
      REAL*8 BMREF(NSTORE)
      REAL*8 ENERA(24,NSTORE), QENERA(24,NSTORE)
      REAL*8 TEMPA(12,NSTORE), QTEMPA(12,NSTORE)
      REAL*8 DENSA(24,NSTORE), QDENSA(24,NSTORE)
      REAL*8 ZEFFA(12,NSTORE), QZEFFA(12,NSTORE)
      REAL*8 BMAGA(12,NSTORE), QBMAGA(12,NSTORE)
C-----------------------------------------------------------------------
      EXTERNAL I4UNIT
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
C Warn user that this routine is deprecated but continue.
C-----------------------------------------------------------------------

      WRITE(I4UNIT(-1),3000)

C-----------------------------------------------------------------------
      READ(IUNIT,*)NBSEL
C
      IF (NBSEL.GT.NSTORE) THEN
         WRITE(I4UNIT(-1),2000) DSNAME , NSTORE , NBSEL , NSTORE
         NBSEL = NSTORE
      ENDIF
C
      DO 10 ISTORE=1,NBSEL
         ISELA(ISTORE) = ISTORE
C
         READ(IUNIT,1000) STRING
CX  Character changed to an exclamation mark in the following
CX  ---------------------------------------------------------
         IF( INDEX(STRING,'!').LE.0) THEN
             READ(STRING,1003) CPCODE(ISTORE) , CDONOR(ISTORE) ,
     &                         CRECVR(ISTORE) , CWAVEL(ISTORE) ,
     &                         CFILE(ISTORE)  , CINDM(ISTORE)
C
C---------------------------------------------------------------------
C   **** THIS IS TO BE EXTENDED TO A '/' AND KEYWORD ANALYSIS   ****
C   **** USING ONLY A SIMPLE FORMATTED READ IF NO '/' DELIMITER ****
C---------------------------------------------------------------------
C
         ENDIF
C
         READ(IUNIT,1001) QEFREF(ISTORE)
         READ(IUNIT,1001) ENREF(ISTORE), TEREF(ISTORE), DEREF(ISTORE),
     &        ZEREF(ISTORE), BMREF(ISTORE)
         READ(IUNIT,1002) NENERA(ISTORE),NTEMPA(ISTORE),NDENSA(ISTORE),
     &        NZEFFA(ISTORE),NBMAGA(ISTORE)
         READ(IUNIT,1001) (ENERA(J,ISTORE),J=1,24)
         READ(IUNIT,1001) (QENERA(J,ISTORE),J=1,24)
         READ(IUNIT,1001) (TEMPA(J,ISTORE),J=1,12)
         READ(IUNIT,1001) (QTEMPA(J,ISTORE),J=1,12)
         READ(IUNIT,1001) (DENSA(J,ISTORE),J=1,24)
         READ(IUNIT,1001) (QDENSA(J,ISTORE),J=1,24)
         READ(IUNIT,1001) (ZEFFA(J,ISTORE),J=1,12)
         READ(IUNIT,1001) (QZEFFA(J,ISTORE),J=1,12)
         READ(IUNIT,1001) (BMAGA(J,ISTORE),J=1,12)
         READ(IUNIT,1001) (QBMAGA(J,ISTORE),J=1,12)
   10 CONTINUE
C
      RETURN
C-----------------------------------------------------------------------
 1000 FORMAT(1A80)
 1001 FORMAT(6D10.2)
 1002 FORMAT(5I10)
 1003 FORMAT(1A8,11X,1A8,3X,1A5,3X,1A5,3X,1A8,2X,1A6)
 2000 FORMAT(/1X,31('*'),' C3DATA MESSAGE ',31('*')/
     &        2X,'INPUT EFF. COEFFT. DATA SET NAME: ',A80/
     &        2X,'NUMBER OF COEFFICIENT DATA-BLOCKS',
     &           ' IN INPUT DATA SET TOO GREAT.'/
     &        2X,'THE MAXIMUM NUMBER ALLOWED     = ',I3/
     &        2X,'THE NUMBER PRESENT IN DATA SET = ',I3/
     &        2X,'THEREFORE ONLY THE FIRST ',I2,' HAVE BEEN ACCEPTED'/
     &        1X,31('*'),' END OF MESSAGE ',31('*'))

 3000  format(/1X,30('*'),' C3DATA WARNING',30('*'),/,1x,
     &        '  Use of this subroutine is deprecated,',/,1x,
     &        '  Suggest using xxdata_12.for instead.',/, 1x, 75('*'))


C-----------------------------------------------------------------------
      END
