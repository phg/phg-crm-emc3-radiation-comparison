CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas303/c3titl.for,v 1.1 2004/07/06 11:50:50 whitefor Exp $ Date $Date: 2004/07/06 11:50:50 $
CX
      SUBROUTINE C3TITL( IBSEL  , DSFULL ,
     &                   CWAVEL , CDONOR ,
     &                   CRECVR , CFILE  ,
     &                   CPCODE , CINDM  ,
     &                   TITLX
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C3TITL *********************
C
C  PURPOSE:  TO CREATE THE DESCRIPTIVE TITLE FOR SELECTED DATA-BLOCK.
C
C  CALLING PROGRAM: ADAS303/SQEF
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IBSEL    = SELECTED DATA-BLOCK: INDEX
CX INPUT : (C*(*)) DSFULL   = FULL MVS INPUT DATA SET NAME
C
C  INPUT : (C*5)  CWAVEL   = INPUT DATA FILE: TRANSITION
C  INPUT : (C*8)  CDONOR   = INPUT DATA FILE: DONOR NEUTRAL ATOM
C  INPUT : (C*5)  CRECVR   = INPUT DATA FILE: RECEIVER NUCLEUS
C  INPUT : (C*8)  CFILE    = INPUT DATA FILE: SPECIFIC ION FILE SOURCE
C  INPUT : (C*8)  CPCODE   = INPUT DATA FILE: PROCESSING CODE
C  INPUT : (C*6)  CINDM    = FILE DATA FILE: EMISSION TYPE
C
CX OUTPUT: (C*120)TITLX    = SELECTED DATA-BLOCK: DESCRIPTIVE TITLE
C
C          (C*2)  C2       = GENERAL USE 2 BYTE  CHARACTER  STRING
CX         (I*4)  LEN_NAME = LENGTH OF FILENAME
CX         (I*4)  POS_NOW  = CURRENT POSITION IN TITLE STRING
C
C ROUTINES: 
C 	XXSLEN			USED TO FIND LENGTH OF FILE NAME
C
C AUTHOR:  H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C          JA8.08
C          TEL. 0141-553-4196
C
C DATE:    23/04/95
C
C UPDATE:  TIM HAMMOND 15/05/95		UNIX PORT UPDATES    
C					Included call to xxslen to find
C					length of filename and edit if
C					required.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER    IBSEL         , LEN_NAME
      INTEGER    IFIRST        , ILAST
      INTEGER    POS_NOW  
C-----------------------------------------------------------------------
      CHARACTER  DSFULL*(*)    , TITLX*120
      CHARACTER  C2*2
      CHARACTER  CINDM*6       , CFILE*8       ,
     &           CPCODE*8      , CWAVEL*5      ,
     &           CDONOR*8      , CRECVR*5
C-----------------------------------------------------------------------
C
C **********************************************************************
C
      CALL XXSLEN(DSFULL,IFIRST,ILAST)
      LEN_NAME = ILAST-IFIRST+1
      IF (LEN_NAME.GT.80) IFIRST = ILAST - 80
      TITLX(1:LEN_NAME+1) = DSFULL(IFIRST:ILAST)
      POS_NOW = LEN_NAME+1
      WRITE(C2,1000) IBSEL
      TITLX(POS_NOW:POS_NOW+8) =  ' BLK=' // C2 // ';'
      POS_NOW = POS_NOW+9
      TITLX(POS_NOW:120)=' R= ' // CRECVR // ' D= ' // CDONOR //
     &               ' N= ' // CWAVEL // ' M= ' // CINDM
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(I2)
C
C-----------------------------------------------------------------------
C
      RETURN
      END
