CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas303/c3corr.for,v 1.1 2004/07/06 11:49:26 whitefor Exp $ Date $Date: 2004/07/06 11:49:26 $
CX
      SUBROUTINE C3CORR( NVALS   , IBSEL  ,
     &                   QATOM   , ATOM   ,
     &                   QEFREF  , ATMREF ,
     &                   NSTORE  , NA     ,
     &                   RION    , SCALED
     &                 )
      IMPLICIT NONE
C----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C3CORR ********************
C
C  PURPOSE: APPLY A CORRECTION TO REFERENCE RATE COEFFICIENT TO ALLOW
C           FOR VARIATION OF PLASMA PARAMETERS ALONG ONE-DIMENSIONAL
C           SCANS
C
C  CALLING PROGRAM: SQEF
C
C  SUBROUTINE:
C
C  INPUT : (I*4)   NVALS()    = NUMBER OF VALUES IN SELECTED DATA BLOCK
C                             1ST. DIM: NSTORE
C  INPUT : (I*4)   IBSEL    = SELECTED BLOCK INDEX
C  INPUT : (R*8)   QATOM(,) = EFFECT. RATE COEFFT. TO SCALE WITH PARAM.
C                             1ST. DIM: NA
C                             2ND. DIM: NSTORE
C  INPUT : (R*8)   ATOM(,)  = PLASMA PARAM. VALUES TO BASE SCALING ON
C                             1ST. DIM: NA
C                             2ND. DIM: NSTORE
C  INPUT : (R*8)   QEFREF() = RATE COEFFICIENT REFERENCE VALUES
C                             1ST. DIM: NSTORE
C  INPUT : (R*8)   ATMREF() = REFERENCE VALUES FOR PLASMA PARAM.
C                             1ST. DIM: NSTORE
C  INPUT : (R*8)   RION     = SPECIFIED PARM. VALUE FOR COEFFT. OUTPUT
C  INPUT : (I*4)   NSTORE   = NUMBER OF DATA BLOCKS IN DATASET.
C  INPUT : (I*4)   NA       = MAX. NO. OF PARAM. VALUES IN BLOCK
C
C  OUTPUT: (R*8)   SCALED   = SCALED PLASMA PARAMETER
C
C  PARAM : (I*4)   MXIN     = MAX. NO. OF INPUT DATA SET VALUES
C
C          (I*4)   MAXVAL   = NUMBER OF DATA VALUES IN SELECTED SET
C          (I*4)   IOPT     = DEFINES THE BOUNDARY DERIVATIVES FOR THE
C                             SPLINE ROUTINE 'XXSPLE'. SEE 'XXSPLE'.
C                             ( VALID VALUES = <0, 0, 1, 2, 3, 4)
C          (I*4)   J        = LOOP INDEX.
C
C          (L*4)   LSETX    = FLAGS TO SPLINE ROUTINE 'XXSPLN' IF
C                             SPLINE PARAMETERS SHOULD BE SET UP.
C                             .TRUE.  => SET UP SPLINE PARAMS.
C                             .FALSE. => DO NOT SET UP SPLINE PARAMS.
C
C          (L*4)   LINTRP   = FLAGS TO IDENTIFY IF SCALED VALUE INTER-
C                             POLATED. (OUTPUT FROM XXSPLE).
C                             .TRUE.  => SCALED VALUE INTERPOLATED
C                             .FALSE. => SCALED VALUE EXTRAPOLATED.
C
C          (R*8)   VL()     = LN(INPUT VALUE/REF. VALUE) FOR PARAM.
C                             DIMENSION: 1
C          (R*8)   QVL()    = LN(INPUT TABLE RATE COEF.) FOR PARAM.
C                             DIMENSION: 1
C          (R*8)   VECL()   = LN(TABLE VALUE/REF. VALUE) FOR PARAM.
C                             DIMENSION: MXIN
C          (R*8)   QVECL()  = LN(TABLE RATE COEF./REF. RATE COEF.)
C                             DIMENSION: MXIN
C          (R*8)   DY()     = DERIVATIVES AT INPUT KNOTS TO XXSPLN.
C                             DIMENSION: MXIN
C
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          I4UNIT     ADAS      RETURNS UNIT NO. FOR OUTPUT OF MESSAGES.
C          XXSPLE     ADAS      SPLINE SUBROUTINE (EXTENDED DIAGNOSTICS)
C          R8FUN1     ADAS      PERFORMS TRANSFORMATION ( X -> X )
C
C  NOTES:  THE QUANTITIES WHICH ARE SCALED BY THIS ROUTINE ARE :  ION
C          DENSITY, ION TEMPERATURE, EFFECTIVE Z AND MAGNETIC
C          FIELD THIS CODE IS TAKEN FROM THE OLDER ADAS CODE
C          'QEFFH.FOR' BY H.P.  SUMMERS
C
C  AUTHOR:  C.J. WHITEHEAD, PAP, UNIVERSITY OF STRATHCLYDE
C           EXT 4205
C
C  DATE:    24/11/94
C
C  UPDATE:  19/12/94 - HP SUMMERS: ADJUST FORMATTING
C
C  UPDATE:  21/04/95 - HP SUMMERS: REORDER TWO-DIMENSIONAL ARRAY INDICES
C
C  UPDATE:  03/05/95 - PE BRIDEN : 1) REPLACED CALLS TO SPLINE NAG
C                                     ROUTINES E01BAF/E02BBF WITH A
C                                     CALL TO XXSPLE. (REQUIRED SOME
C                                     RECODING). + CHECK FOR ATTEMPTED
C                                     EXTRAPOLATION.
C                                  2) MADE CHANGES TO CODE TO MAKE IT
C                                     ANSI STANDARD FORTRAN 77.
C                                  3) TIDIED UP CODE + ADDED CHECK TO
C                                     MAKE SURE INTERNAL ARRAYS ARE
C                                     LARGE ENOUGH.
C                                  4) GENERAL CHANGES TO FORMAT ETC.
C
C  UPDATE:  15/05/95 - Tim Hammond: UNIX PORT
C                      Put under SCCS control
C
C----------------------------------------------------------------------
C----------------------------------------------------------------------
      INTEGER    MXIN
      REAL*8     R8FUN1
C
      PARAMETER( MXIN = 24 )
      EXTERNAL   R8FUN1
C----------------------------------------------------------------------
C
C----------------------------------------------------------------------
      INTEGER    I4UNIT
      INTEGER    IBSEL            , NSTORE          , NA
C----------------------------------------------------------------------
      REAL*8     RION             , SCALED
C----------------------------------------------------------------------
      INTEGER    NVALS(NSTORE)
C----------------------------------------------------------------------
      REAL*8     QEFREF(NSTORE)   , ATMREF(NSTORE)  ,
     &           QATOM(NA,NSTORE) , ATOM(NA,NSTORE)
C----------------------------------------------------------------------
C
C----------------------------------------------------------------------
      INTEGER    MAXVAL           , IOPT            , J
C----------------------------------------------------------------------
      LOGICAL    LSETX            , LINTRP(1)
C----------------------------------------------------------------------
      REAL*8     VL(1)            , QVL(1)          ,
     &           VECL(MXIN)       , QVECL(MXIN)     ,
     &           DY(MXIN)
C----------------------------------------------------------------------
C----------------------------------------------------------------------
C
      MAXVAL = NVALS(IBSEL)
C
C----------------------------------------------------------------------
C     ESTABLISH SCALING FACTOR
C----------------------------------------------------------------------
C
      IF     (MAXVAL.GT.MXIN)             THEN
         STOP ' C3CORR ERROR: MXIN < MAXVAL - INCREASE MXIN'
      ELSEIF (RION.LE.ATOM(1     ,IBSEL)) THEN
         SCALED = QATOM(1     ,IBSEL) / QEFREF(IBSEL)
      ELSEIF (RION.GE.ATOM(MAXVAL,IBSEL)) THEN
         SCALED = QATOM(MAXVAL,IBSEL) / QEFREF(IBSEL)
      ELSEIF (MAXVAL.LE.1 )               THEN
         SCALED = 1.0
      ELSE
C
         VL(1) = DLOG(RION/ATMREF(IBSEL))
C
            DO 10 J=1,MAXVAL
               VECL(J)  = DLOG(ATOM(J,IBSEL) /ATMREF(IBSEL))
               QVECL(J) = DLOG(QATOM(J,IBSEL)/QEFREF(IBSEL))
   10       CONTINUE
C
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C     IF THERE ARE ONLY 2 VALUES, USE LINEAR INTERPOLATION
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C
            IF (MAXVAL.EQ.2) THEN
C
               QVL(1) = ( (VL(1)-VECL(2))*QVECL(1)-
     &                    (VL(1)-VECL(1))*QVECL(2)  ) /
     &                       (VECL(1)-VECL(2))
               SCALED = DEXP(QVL(1))
C
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C     USE CUBIC SPLINE INTERPOLATION FOR > 2 POINTS  (NO EXTRAPOLATION)
C- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
C
            ELSE
C
               LSETX  = .TRUE.
               IOPT   = -1
C
               CALL XXSPLE( LSETX  , IOPT   , R8FUN1 ,
     &                      MAXVAL , VECL   , QVECL  ,
     &                      1      , VL     , QVL    ,
     &                      DY     , LINTRP
     &                    )
C
                  IF (LINTRP(1)) THEN
                     SCALED = DEXP(QVL(1))
                  ELSE
                     SCALED = 1.0
                     WRITE(I4UNIT(-1),1000)
                  ENDIF
C
            ENDIF
C
      ENDIF
C
C----------------------------------------------------------------------
C
 1000 FORMAT(1X,31('*'),' C3CORR WARNING ',31('*')//
     & 1X,'OUTPUT CORRECTION FACTOR SET TO 1.0 AS REQUESTED VALUE IS',
     & ' OUT OF RANGE.')
C
C----------------------------------------------------------------------
C
      RETURN
      END
