CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas303/adas303.for,v 1.9 2010/10/12 15:22:58 mog Exp $ Date $Date: 2010/10/12 15:22:58 $
CX
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 PROGRAM: ADAS303 **********************
C
C  ORIGINAL NAME: SPRTGRF3
C
C  VERSION:  1.0 (ADAS91)
C
C  PURPOSE:  TO GRAPH AND INTERPOLATE SELECTED  DATA  FROM  COMPILATIONS
C            OF EFFECTIVE EMISSION COEFFICIENTS FOR CXS LINES.
C
C            THE SELECTED INPUT DATA SET IS FOR A SPECIFIC EMITTING  ION
C            I.E. ELEMENT AND CHARGE STATE. THIS FILE CONTAINS FOR  THIS
C            ION A NUMBER OF BLOCKS OF DATA REPRESENTING DIFFERENT N-N'
C            TRANSITIONS.  FOR EACH TRANSITION A TABLE OF EFFECTIVE
C            COEFFICIENTS BASED ON ONE DIMENSIONAL SCANS FROM A
C            REFERENCE SET OF CONDITIONS ARE GIVEN.  THE SCANS ARE IN
C            BEAM ENERGY, ION TEMPERATURE, ION DENSITY, Z EFFECTIVE  AND
C            B-FIELD.  THIS  PROGRAM  ALLOWS  FOR THE EXAMINATION  AND
C            DISPLAY OF A GIVEN TRANSITION AS FOLLOWS:
C
C            TABULATED EFF. COEFFICIENTS FOR A SELECTED TRANSITION ARE
C            INTERPOLATED TO A CHOSEN ION TEMPERATURE, ION DENSITY,
C            Z EFFECTIVE AND B-FIELD AT THE SET OF BEAM ENERGIES.  THESE
C            COEFFICIENTS ARE THEN FITTED  IN BEAM ENERGY (EV/AMU)
C            USING A CUBIC SPLINE ROUTINE,  FROM  WHICH  INTER-
C            POLATIONS ARE MADE IF REQUIRED.   IF  REQUESTED  A  MINIMAX
C            POLYNOMIAL  IS FITTED  THROUGH  THE  DATA  AND  GRAPHICALLY
C            DISPLAYED.  THE MINIMAX COEFFICIENTS AND  THE  ACCURACY  OF
C            THE FIT ARE PRINTED OUT ARE PRINTED OUT WITH THE GRAPH.
C
C            IF REQUESTED THE GRAPH AND A SUMMARY TEXT FILE  ARE  OUTPUT
C            TO HARDCOPY.
C
C
CX DATA:     THE SOURCE DATA IS CONTAINED AS MEMBERS  IN  A  PARTITIONED
CX           DATA SET AS FOLLOWS:-
CX
CX                   'JETSHP.IONATOM.DATA(QEFF#<DONOR>)'
CX
CX           WHERE <DONOR> REPRESENTS THE DONOR (NEUTRAL) ELEMENT
CX                      E.G. BE, C
CX
CX           OR ALTERNATIVELY AS:-
CX                   '<UID>.QEF<YR>#<DONOR>.DATA(<CODE>#<ION>)
CX
CX           WHERE <UID>   IS THE USER IDENTIFIER
CX                 <YR>    IS A TWO DIGIT YEAR NUMBER
CX                 <DONOR> IS THE DONOR (NEUTRAL) ELEMENT
CX                 <CODE>  IS AN ARBITRARY DISTINQUISHING THREE LETTER
CX                         CODE
CX                 <ION>   IS THE BARE NUCLEUS RECOMBINING ION (EG C6)
CX
CX
C            THE UNITS USED IN THE DATA FILE ARE TAKEN AS FOLLOWS:
C
C            COLL. ENERGIES    : EV/AMU
C            ION TEMPERATURES : EV
C            ION DENSITIES    : CM-3
C            Z-EFFECTIVE      : (DIMENSIONLESS)
C            B-FIELD          : TESLA
C            EFFECTIVE COEFFTS.: CM3 S-1
C
C  PROGRAM:
C
C          (I*4)  NSTORE  = PARAMETER = MAXIMUM NUMBER  OF  DATA-BLOCKS
C                                       WHICH CAN BE READ FROM THE INPUT
C                                       DATA-SET.
C          (I*4)  NDTEM   = PARAMETER = MAX. NO. OF INPUT FILE PLASMA
C                                       TEMPERATURE (FIXED AT 12)
C          (I*4)  NDDEN   = PARAMETER = MAX. NO. OF INPUT FILE PLASMA
C                                       DENSITIES (FIXED AT 24)
C          (I*4)  NDEIN   = PARAMETER = MAX. NO. OF INPUT FILE BEAM
C                                       ENERGIES (FIXED AT 24)
C          (I*4)  NDZEF   = PARAMETER = MAX. NO. OF INPUT FILE BEAM
C                                       ENERGIES (FIXED AT 12)
C          (I*4)  NDMAG   = PARAMETER = MAX. NO. OF INPUT FILE MAG.
C                                       FIELDS (FIXED AT 12)
C
C          (I*4)  NEDIM   = PARAMETER = MAX. NO. OF ISPF ENTERED BEAM
C                                       ENERGIES
C          (I*4)  IUNT07  = PARAMETER = OUTPUT UNIT FOR RESULTS
C          (I*4)  IUNT10  = PARAMETER = INPUT UNIT FOR DATA
C
C          (I*4)  NMX     = PARAMETER =
C                           NUMBER OF MINIMAX  GENERATED IONS PER PHOTON
C                           /TEMPERATUREOR DENSITY PAIRS  FOR GRAPHICAL
C                           OUTPUT.
C          (I*4)  MAXDEG  = PARAMETER = MAX. NO. OF DEGREES FOR MINIMAX
C          (I*4)  L1      = PARAMETER = 1
C          (I*4)  L2      = PARAMETER = 2
C          (I*4)  L3      = PARAMETER = 3
C
C          (L*4)  LMLOG   = PARAMETER = .TRUE. => FIT MINIMAX POLYNOMIAL
C                           TO LOG10 TRANSFORMATION OF INPUT DATA.
C
C          (I*4)  I4UNIT  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (I*4)  NBSEL   = NUMBER OF DATA-BLOCKS ACCEPTED & READ IN.
C          (I*4)  IBLK    = DATA-BLOCK WAVELENGTH INDEX
C          (I*4)  IFORM   = TEMPERATURE FORM INDEX
C          (I*4)  IBSEL   = SELECTED INPUT DATA-BLOCK FOR ANALYSIS
C          (I*4)  KPLUS1  = NUMBER OF MINIMAX COEFFICIENTS
C          (I*4)  IEVAL   = NUMBER OF USER ENTERED TEMPERATURE/DENSITY
C                           PAIRS.
C          (I*4)  IETYP   = 1 => 'TIN(array)' UNITS: KELVIN
C                         = 2 => 'TIN(array)' UNITS: EV
C                         = 3 => 'TIN(array)' UNITS: REDUCED
C          (I*4)  IPAN    = ISPF PANEL NUMBER
C
C
C          (R*8)  R8BCON  = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (R*8)  XMIN    = GRAPH: LOWER LIMIT FOR beam energy
C                                  UNITS: SEE 'IETYP'
C          (R*8)  XMAX    = GRAPH: UPPER LIMIT FOR beam energy
C                                  UNITS: SEE 'IETYP'
C          (R*8)  XMINPU  = GRAPH: LOWER LIMIT FOR beam energy (at un.)
C          (R*8)  XMAXPU  = GRAPH: UPPER LIMIT FOR beam energy (at. un.)
C          (R*8)  YMIN    = GRAPH: LOWER LIMIT FOR eff. emiss. coefft.
C          (R*8)  YMAX    = GRAPH: UPPER LIMIT FOR eff. emiss. coefft.
C          (R*8)  TOLVAL  = FRACTIONAL  TOLERANCE  FOR  ACCEPTANCE  OF
C                           MINIMAX POLYNOMIAL FIT TO DATA (IF SELECTED)
C
C          (L*4)  OPEN10  = .TRUE.  => FILE ALLOCATED TO UNIT 10.
C                           .FALSE. => NO FILE ALLOCATED TO UNIT 10.
C          (L*4)  LGHOST  = .TRUE.  => GHOST80 OUTPUT WRITTEN
C                           .FALSE. => NO GHOST80 OUTPUT WRITTEN
C          (L*4)  LPEND   = .TRUE.  => END ANALYSIS OF CURRENT DATA SET
C                           .FALSE. => CONTINUE ANALYSIS WITH CURRENT
C                                      DATA SET.
C          (L*4)  LOSEL   = .TRUE.  => CALCULATE INTERPOLATED VALUES FOR
C                                      OUTPUT (ALWAYS IS .TRUE.)
C          (L*4)  LFSEL   = .TRUE.  => CARRY OUT MINIMAX POLYNOMIAL
C                                      FITTING
C                           .FALSE. => - DO NOT DO THE ABOVE -
C          (L*4)  LDSEL   = .TRUE.  => DISPLAY INPUT FILE DESCRIPTIVE
C                                      TEXT
C                           .FALSE. => - DO NOT DO THE ABOVE -
C          (L*4)  LGRD1   = .TRUE.  => GRAPH OUTPUT TO BE STORED IN
C                                      GHOST80 GRID FILE.
C                           .FALSE. => DO NOT STORE GRAPH IN GRID FILE.
C          (L*4)  LDEF1   = .TRUE.  => USE GRAPH DEFAULT SCALING
C                           .FALSE. => DO NOT USE DEFAULT SCALING
C
C          (C*3)  REP     = 'YES' => TERMINATE PROGRAM EXECUTION
C                           'NO'  => CONTINUE PROGRAM EXECUTION
C          (C*8)  DATE    = CURRENT DATE (AS 'DD/MM/YY')
C          (C*80) DSFULL  = FULL INPUT DATA SET NAME (READ FROM ISPF)
C                           (INCORPORATED INTO 'TITLX')
C          (C*40) TITLE   = TITLE OF RUN (READ FROM ISPF PANEL)
C          (C*80) TITLM   = DIAGNOSTIC LINE INFORMATION FROM 'MINIMAX'
C          (C*120) TITLX   = INFORMATION STRING CONTAINING: INPUT DATA
C                           FILE NAME, SELECTED DATA-BLOCK INDEX  AND
C                           FURTHER INFORMATION READ FROM DATA-SET.
C
C
C          (I*4)  NENERA()= NUMBER OF ENERGIES
C          (I*4)  NTEMPA()= NUMBER OF TEMPERATURES
C          (I*4)  NDENSA()= NUMBER OF DENSITIES
C          (I*4)  NZEFFA()= NUMBER OF EFFECTIVE Z'S
C          (I*4)  NBMAGA()= NUMBER OF MAGNETIC FIELD VALUES
C          (R*8)  QEFREF()= REFERENCE VALUE OF RATE COEFFICIENT
C          (R*8)  ENREF() =     "       "   "  ENERGY
C          (R*8)  TEREF() =     "       "   "  TEMPERATURE
C          (R*8)  DEREF() =     "       "   "  DENSITY
C          (R*8)  ZEREF() =     "       "   "  EFFECTIVE Z
C          (R*8)  BMREF() =     "       "   "  MAGNETIC FIELD
C                           1ST. DIM: NSTORE
C                           (FOR ABOVE ARRAYS)
C
C          (R*8)  ENERA(,)  = ENERGIES
C          (R*8)  QENERA(,) = RATE COEFFICIENTS FOR ENERGY VALUE
C          (R*8)  TEMPA(,)  = TEMPERATURES
C          (R*8)  QTEMPA(,) = RATE COEFFICIENTS FOR TEMPERATURES
C          (R*8)  DENSA(,)  = DENSITIES
C          (R*8)  QDENSA(,) = RATE COEFFICIENTS FOR DESNITIES
C          (R*8)  ZEFFA(,)  = EFFECTIVE Z
C          (R*8)  QZEFFA(,) = RATE COEFFICIENTS FOR EFFECTIVE Z
C          (R*8)  BMAGA(,)  = MAGNETIC FIELD
C          (R*8)  QBMAGA(,) = RATE COEFFICIENTS FOR MAGNETIC FIELDS
C                             1ST DIM: 12 OR 24  DEPENDING ON PARAMETER
C                             2ND DIM: NSTORE
C          (R*8)  DIVAL     = SELECTED OUTPUT ION DENSITY (CM-3)
C          (R*8)  TIVAL     = SELECTED OUTPUT ION TEMPERATURE (EV)
C          (R*8)  ZFVAL     = SELECTED OUTPUT Z-EFFECTIVE
C          (R*8)  BMVAL     = SELECTED OUTPUT MAGNETIC FIELD (T)
C          (R*8)  SCDENS    = DENSITY SCAN CORRECTION FACTOR
C          (R*8)  SCTEMP    = TEMPERATURE SCAN CORRECTION FACTOR
C          (R*8)  SCZEFF    = Z-EFFECTIVE SCAN CORRECTION FACTOR
C          (R*8)  SCBMAG    = B-FIELD SCAN CORRECTION FACTOR
C          (R*8)  SC        = RESULTANT CORRECTION FACTOR
C
C          (R*8)  EIN()   = USER ENTERED COLLISION ENERGIES
C                           (NOTE: UNITS ARE GIVEN BY 'IETYP')
C                           DIMENSION: ENERGY INDEX
C          (R*8)  EATM()  = USER ENTERED BEAM ENERGIES
C                           (UNITS: ATOMIC UNITS)
C                           DIMENSION: BEAM ENERGY INDEX
C          (R*8)  EAMU()  = USER ENTERED BEAM ENERGIES
C                           (UNITS: EV/AMU)
C                           DIMENSION: BEAM ENERGY INDEX
C          (R*8)  ECMS()  = USER ENTERED BEAM ENERGIES
C                           (UNITS: CM/SEC)
C                           DIMENSION: BEAM ENERGY INDEX
C          (R*8)  EFITM() = MINIMAX: SELECTED BEAM ENERGIES(EV/AMU)
C          (R*8)  QEFA()  = SPLINE INTERPOLATED OR EXTRAPOLATED EFF.
C                           EMIS. COEFF. FOR THE USER ENTERED BEAM
C                           ENERGY.
C                           DIMENSION: BEAM ENERGY INDEX.
C          (R*8)  QEFM()  = EFF. EMISS. COEFFT. AT 'EFITM()'.
C          (R*8)  COEF()  = COEFFICIENTS OF FITTED MINIMAX POLYNOMIAL
C
C
C          (R*8)  EVALS(,,)= INPUT DATA SET - BEAM ENERGIES
C                            1ST DIMENSION: BEAM ENERGY INDEX
C                                           ( SEE 'ENERA(,)' )
C                            2ND DIMENSION: 1 => EV/AMU  (IETYP=1)
C                                           2 => AT. UNT.(IETYP=2)
C                                           3 => UNUSED  (IETYP=3)
C                            3RD DIMENSION: DATA-BLOCK INDEX
C
C          (C*5)  CWAVEL()= INPUT DATA FILE: TRANSITION
C                           DIMENSION: DATA-BLOCK INDEX
C          (C*8)  CDONOR()= INPUT DATA FILE: DONOR NEUTRAL ATOM
C                           DIMENSION: DATA-BLOCK INDEX
C          (C*5)  CRECVR()= INPUT DATA FILE: RECEIVER NUCLEUS
C                           DIMENSION: DATA-BLOCK INDEX
C          (C*8)  CFILE() = INPUT DATA FILE: SPECIFIC ION FILE SOURCE
C                           DIMENSION: DATA-BLOCK INDEX
C          (C*8)  CPCODE()= INPUT DATA FILE: PROCESSING CODE
C                           DIMENSION: DATA-BLOCK INDEX
C          (C*6)  CINDM() = FILE DATA FILE: EMISSION TYPE
C                           DIMENSION: DATA-BLOCK INDEX
C
C          (L*4)  LERNG()=  .TRUE.  => OUTPUT 'QEFA()'  VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      BEAM ENERGIES 'EATM()'.
C                           .FALSE. => OUTPUT 'QEFA()'  VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      BEAM ENERGIES 'EATM()'.
C                           DIMENSION: BEAM ENERGY INDEX
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          C3SPF0     ADAS      GATHERS INPUT FILE NAMES VIA ISPF PANELS
C          C3CHKZ     ADAS      VERIFY EMITTER READ FROM MEMBER NAME OK
C          C3SETP     ADAS      SETS UP ISPF NON-READABLE VARIABLES
C          C3ISPF     ADAS      GATHERS USERS VALUES VIA ISPF PANELS
C          C3SPLN     ADAS      SPLINE CURVE FITTING/INTERPOLATION.
C          C3TITL     ADAS      CREATE DESCRIPTIVE TITLE FOR OUTPUT
C          C3OUTG     ADAS      GRAPHICAL OUTPUT ROUTINE USING GHOST80
C          C3OUT0     ADAS      RESULTS OUTPUT ROUTINE
C          XX0000     ADAS      SET MACHINE DEPENDANT ADAS CONFIGURATION
C          XXDATE     ADAS      PROVIDES CURRENT DATE
C          XXENDG     ADAS      CLOSE GHOST80 GRIDFILE & OUTPUT CHANNEL
C          XXTEXT     ADAS      DISPLAYS DESCRIPTIVE TEXT FROM FILE
C          XXBCON     ADAS      CONVERTS ISPF ENTERED BEAM ENERGY FORM
C          XXMNMX     ADAS      MINIMAX POLYNOMIAL FIT ROUTINE
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          R8BCON     ADAS      REAL*8 FUNCTION:CONVERT BEAM ENERGY FORM
C          XXDATA_12  ADAS      GATHERS RELEVANT DATA FROM INPUT FILE
C
C AUTHOR:  H P SUMMERS, UNIVERSITY OF STRATHCLYDE
C          JA8.08
C          TEL. 0141-553-4196
C
C DATE:    20/04/95
C
C UPDATE:  03/05/95 - PE BRIDEN - AMMENDED C3OUT0 and C3OUTG ARGUMENT
C                                 LISTS (DUE TO MODS TO THESE ROUTINES)
C                               - ADDED CREATION OF ECMS ARRAY.
C UNIX-IDL PORT:
C
C AUTHOR:  TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    04/05/95
C
C VERSION: 1.1				DATE: 04/05/95
C MODIFIED: TIM HAMMOND
C	    - FIRST CONVERTED
C           - INCREASED LENGTH OF TITLX TO 120
C           _ INCREASED LENGTH OF DSFULL TO 80
C           - REMOVED LDSEL FROM CALL TO C3SPF0
C
C VERSION: 1.2: 			DATE: 15/05/95
C MODIFIED: TIM HAMMOND
C	  - UPDATED COMMENTS
C	  - CHANGED LABELS 200 and 300
C
C VERSION: 1.3: 			DATE: 22/05/95
C MODIFIED: TIM HAMMOND
C	  - CHANGED XMINPU, XMAXPU TO
C	  - XMIN, XMAX IN CALL TO C3OUTG
C
C VERSION: 1.4                               DATE: 05-06-96
C MODIFIED: WILLIAM OSBORN
C           - ADDED MENU BUTTON CODE
C
C VERSION: 1.5                               DATE: 24-07-96
C MODIFIED: WILLIAM OSBORN
C           - INCREASED NDEIN TO 24
C
C VERSION: 1.6                               DATE: 14-10-96
C MODIFIED: TIM HAMMOND 
C           - CHANGED OUTPUT UNIT IUNT07 FROM 7 TO 17 TO AVOID
C             CLASHING WITH STANDARD ERROR STREAM 7 ON HP 
C             WORKSTATIONS.
C
C VERSION: 1.7                               DATE: 02-12-2004
C MODIFIED: Martin O'Mullane
C           - Replace c3data with xxdata_12.
C           - Remove isela index array.
C
C VERSION: 1.7                               DATE: 07-03-2008
C MODIFIED: Allan Whiteford
C           - Changed meaning of NEDIM dimension so that it
C             is the maximum input dimensions from the panel.
C           - Changed meaning of NDEIN dimension so that it
C             is the maximum energy dimensions from the file.
C           - Updated for new version of xxdata_12:
C             * Added dimension variables of:
C                NDTEM , NDDEN , NDZEF , NDMAG.
C             * Added variables:
C                CSYMB(NSTORE)*2 , CZION(NSTORE)*2 , CTRANS(NSTORE)*7
C             * Changed call to xxdata_12
C             * Sent dimensions into c3ispf but note warning that
C               fortran/IDL communication assumes dimensions
C
C VERSION : 1.8                               
C DATE    : 12-12-2010
C MODIFIED: Martin O'Mullane
C           - xxdata_12 holds the transition information in ctrans 
C             rather than cwavel.
C
C-----------------------------------------------------------------------
      INTEGER    NSTORE       , NEDIM       ,
     &           MAXDEG       , ISTOP       , PIPEIN
      INTEGER    IUNT07       , IUNT10
      INTEGER    NMX
      INTEGER    L1           , L2           , L3
      INTEGER    NDTEM        , NDDEN        , NDEIN    ,
     &           NDZEF        , NDMAG
C-----------------------------------------------------------------------
      LOGICAL    LMLOG
C-----------------------------------------------------------------------
      PARAMETER( NSTORE = 150 , NEDIM = 24  , MAXDEG = 19 )
      PARAMETER( IUNT07 =  17 , IUNT10 = 10 )
      PARAMETER( NMX    = 100 , PIPEIN = 5)
      PARAMETER( L1     =   1 , L2     =  2  , L3     =  3 )
C
C     NOTE: IDL display part of program requires that these
C           values are not changed. See c3ispf.{for,pro}
C
      PARAMETER( NDTEM  =  12 , NDDEN  = 24  , NDEIN = 24 )
      PARAMETER( NDZEF  =  12 , NDMAG  = 12  )
C-----------------------------------------------------------------------
      PARAMETER( LMLOG  = .TRUE. )
C-----------------------------------------------------------------------
      INTEGER    I4UNIT
      INTEGER    NBSEL        , IBLK         , IFORM      ,
     &           IBSEL        , KPLUS1       ,
     &           IEVAL        , IETYP        , IPAN
      INTEGER    J
C-----------------------------------------------------------------------
      REAL*8     R8BCON
      REAL*8     DIVAL        , TIVAL        , ZFVAL      , BMVAL
      REAL*8     XMIN         , XMAX         , XMINPU     , XMAXPU     ,
     &           YMIN         , YMAX         ,
     &           TOLVAL
      REAL*8     SCDENS       , SCTEMP       , SCZEFF     , SCBMAG     ,
     &           SC
C-----------------------------------------------------------------------
      LOGICAL    OPEN10       , LGHOST       , LPEND      ,
     &           LOSEL        , LFSEL        ,
     &           LDSEL        , LGRD1        , LDEF1
C-----------------------------------------------------------------------
CX    UNIX PORT - Following variables added for output selection options
      LOGICAL    LGRAPH       , L2FILE
C-----------------------------------------------------------------------
      CHARACTER  REP*3        ,
     &           DATE*8       , DSFULL*80    ,
     &           TITLE*40     , TITLM*80     , TITLX*120
CX    UNIX PORT - Following is name of output text file
      CHARACTER  SAVFIL*80
C-----------------------------------------------------------------------
      INTEGER    NENERA(NSTORE)              , NTEMPA(NSTORE)          ,
     &           NDENSA(NSTORE)              , NZEFFA(NSTORE)          ,
     &           NBMAGA(NSTORE)
C-----------------------------------------------------------------------
      REAL*8     QEF(NDEIN)
      REAL*8     EIN(NEDIM)                  , ECMS(NEDIM)             ,
     &           EATM(NEDIM)                 , EAMU(NEDIM)             ,
     &           QEFA(NEDIM)                 , QEFM(NMX)               ,
     &           EFITM(NMX)                  , COEF(MAXDEG+1)
C-----------------------------------------------------------------------
      CHARACTER  CINDM(NSTORE)*6             , CFILE(NSTORE)*8         ,
     &           CPCODE(NSTORE)*8            , CWAVEL(NSTORE)*5        ,
     &           CDONOR(NSTORE)*8            , CRECVR(NSTORE)*5        ,
     &           CSYMB(NSTORE)*2             , CZION(NSTORE)*2         ,
     &           CTRANS(NSTORE)*7
C-----------------------------------------------------------------------
      LOGICAL    LERNG(NEDIM)
C-----------------------------------------------------------------------
      REAL*8     EVALS(NDEIN,3,NSTORE)
      REAL*8     QEFREF(NSTORE)
      REAL*8     ENREF(NSTORE)               , TEREF(NSTORE)           ,
     &           DEREF(NSTORE)               , ZEREF(NSTORE)           ,
     &           BMREF(NSTORE)
      REAL*8     ENERA(NDEIN,NSTORE)         , TEMPA(NDTEM,NSTORE)     ,
     &           DENSA(NDDEN,NSTORE)         , ZEFFA(NDZEF,NSTORE)     ,
     &           BMAGA(NDMAG,NSTORE)
      REAL*8     QENERA(NDEIN,NSTORE)        , QTEMPA(NDTEM,NSTORE)    ,
     &           QDENSA(NDDEN,NSTORE)        , QZEFFA(NDZEF,NSTORE)    ,
     &           QBMAGA(NDMAG,NSTORE)
C-----------------------------------------------------------------------
      DATA IPAN   /0/       , IEVAL  /0/
      DATA OPEN10 /.FALSE./ , LGHOST /.FALSE./
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C *************************** MAIN PROGRAM ****************************
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C SET MACHINE DEPENDANT ADAS CONFIGURATION VALUES
C-----------------------------------------------------------------------
C
      CALL XX0000
C
C-----------------------------------------------------------------------
C GATHER CURRENT DATE
C-----------------------------------------------------------------------
C
      CALL XXDATE( DATE )
C-----------------------------------------------------------------------
  100 IPAN  = 0
C
C-----------------------------------------------------------------------
C IF FILE IS ACTIVE ON UNIT 10 - CLOSE THE UNIT
C-----------------------------------------------------------------------
C
         IF (OPEN10) THEN
            CLOSE(10)
            OPEN10 = .FALSE.
         ENDIF
C
C-----------------------------------------------------------------------
C GET INPUT DATA SET NAME (FROM ISPF PANELS)
C-----------------------------------------------------------------------
C
      CALL C3SPF0( REP , DSFULL )
C
C-----------------------------------------------------------------------
C IF PROGRAM RUN IS COMPLETED:  END RUN
C-----------------------------------------------------------------------
C
         IF (REP.EQ.'YES') THEN
CX          IF (LGHOST) CALL XXENDG
            GOTO 9999
         ENDIF
C
C-----------------------------------------------------------------------
C OPEN INPUT DATA FILE - DSFULL
C-----------------------------------------------------------------------
C
CX    OPEN( UNIT=IUNT10 , FILE=DSFULL , ACTION='READ' )
      OPEN( UNIT=IUNT10 , FILE=DSFULL , STATUS='OLD'  )
      OPEN10=.TRUE.
CX
CX----------------------------------------------------------------------
CX IF DESCRIPTIVE TEXT REQUESTED - SCAN MEMBER AND OUTPUT TEXT
CX-----------------------------------------------------------------------
CX THIS PART IS NOW HANDLED BY THE IDL SUBROUTINE XXTEXT.PRO
CX----------------------------------------------------------------------
CX       IF (LDSEL) THEN
CX          CALL XXTEXT(IUNT10)
CX          GOTO 100
CX       ENDIF
C
C-----------------------------------------------------------------------
C FETCH DATA FROM SELECTED DATASET
C-----------------------------------------------------------------------
C
      CALL xxdata_12( IUNT10 , DSFULL ,
     &                NDTEM  , NDDEN  , NDEIN  , NDZEF   , NDMAG  ,
     &                NSTORE ,
     &                NBSEL  ,
     &                csymb  , czion  , CWAVEL , CDONOR ,  CRECVR ,
     &                CTRANS , CFILE  , CPCODE , CINDM  ,
     &                QEFREF ,
     &                ENREF  , TEREF  , DEREF  , ZEREF   , BMREF  ,
     &                NENERA , NTEMPA , NDENSA , NZEFFA  , NBMAGA ,
     &                ENERA  , TEMPA  , DENSA  , ZEFFA   , BMAGA  ,
     &                QENERA , QTEMPA , QDENSA , QZEFFA  , QBMAGA
     &              )
C
C-----------------------------------------------------------------------
C SET UP ARRAY 'EVALS(,)' - CONTAINS INPUT BEAM ENERGIES IN 3 FORMS.
C    NOTE: INPUT DATA SET BEAM ENERGGY 'ENERA()' IS IN EV/AMU
C-----------------------------------------------------------------------
C

         DO 1 IBLK=1,NBSEL
            DO 2 IFORM=1,3
               CALL XXBCON( L1          , IFORM                ,
     &                      NENERA(IBLK), ENERA(1,IBLK)       ,
     &                                    EVALS(1,IFORM,IBLK)
     &                    )
    2       CONTINUE
    1    CONTINUE
C
C-----------------------------------------------------------------------
C SET UP ISPF FUNCTION POOL PARAMETERS (NON-READABLE VARIABLES)
C-----------------------------------------------------------------------
C
      CALL C3SETP( NBSEL  )
C
C-----------------------------------------------------------------------
C GET USER-DEFINED VALUES (FROM ISPF PANELS)
C-----------------------------------------------------------------------
C
200   CONTINUE
      CALL C3ISPF( IPAN   , LPEND  ,
     &             NSTORE , NEDIM  ,
     &             NDTEM  , NDDEN  , NDEIN  , NDZEF   , NDMAG  ,
     &             NBSEL  ,
     &             ctrans , CDONOR , CRECVR , CFILE   , CPCODE , CINDM ,
     &             ENREF  , TEREF  , DEREF  , ZEREF   , BMREF  ,
     &             NENERA , NTEMPA , NDENSA , NZEFFA  , NBMAGA ,
     &             EVALS  , TEMPA  , DENSA  , ZEFFA   , BMAGA  ,
     &             TITLE  ,
     &             IBSEL  ,
     &             DIVAL  , TIVAL  , ZFVAL  , BMVAL   ,
     &             IETYP  , IEVAL  , EIN    ,
     &             LOSEL  , LFSEL  , LGRD1  , LDEF1   ,
     &             TOLVAL ,
     &             XMIN   , XMAX   , YMIN   , YMAX
     &           )
      IPAN=-1
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) ISTOP
      IF(ISTOP.EQ.1) GOTO 9999
C
C-----------------------------------------------------------------------
C IF END OF CURRENT FILE REQUESTED GO BACK TO INPUT FILE PANEL.
C-----------------------------------------------------------------------
 
      IF (LPEND) GOTO 100
C
C-----------------------------------------------------------------------
C CONVERT INPUT BEAM ENERGIES INTO EV/AMU (EIN -> EAMU)
C-----------------------------------------------------------------------
C
      CALL XXBCON( IETYP , L1 , IEVAL , EIN , EAMU )
C
C-----------------------------------------------------------------------
C CONVERT INPUT BEAM ENERGIES INTO AT UNT.(EIN -> EATM)
C-----------------------------------------------------------------------
C
      CALL XXBCON( IETYP , L2 , IEVAL , EIN , EATM )
C
C-----------------------------------------------------------------------
C CONVERT INPUT BEAM ENERGIES INTO AT UNT.(EIN -> EATM)
C-----------------------------------------------------------------------
C
      CALL XXBCON( IETYP , L3 , IEVAL , EIN , ECMS )
C
C-----------------------------------------------------------------------
C CONVERT XMIN & XMAX PLOTTING UNITS INTO EV/AMU
C-----------------------------------------------------------------------
C
      XMINPU = R8BCON( IETYP , L1 , XMIN )
      XMAXPU = R8BCON( IETYP , L1 , XMAX )
C
C-----------------------------------------------------------------------
C  DEVELOP CORRECTION FACTORS FROM ONE-DIMENSIONAL SCANS
C-----------------------------------------------------------------------
C
C -------- CORRECTION : PLASMA ION DENSITY
C
      CALL C3CORR(NDENSA  , IBSEL , QDENSA  , DENSA ,
     &            QEFREF  , DEREF , NSTORE  , NDDEN ,
     &            DIVAL   , SCDENS )
C
C -------- CORRECTION : PLASMA ION TEMPERATURE
C
      CALL C3CORR(NTEMPA  , IBSEL , QTEMPA  , TEMPA ,
     &            QEFREF  , TEREF , NSTORE  , NDTEM ,
     &            TIVAL   , SCTEMP )
C
C -------- CORRECTION : PLASMA Z EFFECTIVE
C
      CALL C3CORR(NZEFFA  , IBSEL , QZEFFA  , ZEFFA ,
     &            QEFREF  , ZEREF , NSTORE  , NDZEF ,
     &            ZFVAL   , SCZEFF )
C
C -------- CORRECTION : PLASMA MAGNETIC FIELD
C
      CALL C3CORR(NBMAGA  , IBSEL , QBMAGA  , BMAGA ,
     &            QEFREF  , BMREF , NSTORE  , NDMAG ,
     &            BMVAL   , SCBMAG )
C
C-----------------------------------------------------------------------
C  ASSEMBLE COMPLETE CORRECTION.  CONVERT COEFFICIENT/ENERGY SCAN VALUES
C-----------------------------------------------------------------------
C
      SC=SCDENS*SCTEMP*SCZEFF*SCBMAG
C
      DO 150 J=1,NENERA(IBSEL)
         QEF(J)=SC*QENERA(J,IBSEL)
  150 CONTINUE
C
C
C-----------------------------------------------------------------------
C GENERATE COEFTS FOR GRAPHICAL & TABULAR OUTPUT USING ONE-WAY SPLINE
C-----------------------------------------------------------------------
C
      CALL C3SPLN( NENERA(IBSEL)     , IEVAL             ,
     &             ENERA(1,IBSEL)    , EAMU              ,
     &             QEF               , QEFA              ,
     &             LERNG
     &           )
C
C-----------------------------------------------------------------------
C  FIT MINIMAX POLYNOMIAL IF REQUIRED - ONLY IF (IEVAL > 2).
C-----------------------------------------------------------------------
C
      IF (IEVAL.LE.2) LFSEL = .FALSE.
 
         IF (LFSEL) THEN
              CALL XXMNMX( LMLOG   , MAXDEG   , TOLVAL  ,
     &                     IEVAL   , EAMU     , QEFA    ,
     &                     NMX     , EFITM    , QEFM    ,
     &                     KPLUS1  , COEF     ,
     &                     TITLM
     &                   )
            WRITE(I4UNIT(-1),1000) TITLM(1:79)
         ENDIF
C
C-----------------------------------------------------------------------
C CONSTRUCT DESCRIPTIVE TITLE FOR CURRENT DATA-BLOCK UNDER ANALYSIS.
C-----------------------------------------------------------------------
C
      CALL C3TITL( IBSEL         , DSFULL        ,
     &             CWAVEL(IBSEL) , CDONOR(IBSEL),
     &             CRECVR(IBSEL) , CFILE(IBSEL) ,
     &             CPCODE(IBSEL) , CINDM(IBSEL) ,
     &             TITLX
     &           )
C
C-----------------------------------------------------------------------
C  GENERATE GRAPH
C-----------------------------------------------------------------------
CX ADDED THE NEW CALL TO C3SPF1 ROUTINE WHICH COMMUNICATES WITH IDL VIA
CX A PIPE TO GET USER SELECTED OUTPUT OPTIONS
C-----------------------------------------------------------------------
300   CONTINUE
      CALL C3SPF1( DSFULL	 , 
     &		   IBSEL         , LFSEL         , LDEF1    ,
     &             LGRAPH        , L2FILE        , SAVFIL   ,
     &             XMIN          , XMAX          , YMIN     , YMAX   ,
     &             LPEND
     &           )
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) ISTOP
      IF(ISTOP.EQ.1) GOTO 9999
C
C-----------------------------------------------------------------------
CX IF USER HAS SELECTED 'CANCEL' RETURN TO PROCESSING OPTIONS WINDOW.
C-----------------------------------------------------------------------
      IF (LPEND) GOTO 200
C
C-----------------------------------------------------------------------
C  IF SELECTED - GENERATE GRAPH
CX UNIX PORT - C3OUTG COMMUNICATES WITH IDL VIA PIPE.
C-----------------------------------------------------------------------
C
      IF (LGRAPH) THEN
          CALL C3OUTG( LGHOST        ,
     &             TITLE         , TITLX        , TITLM      , DATE   ,
     &             DIVAL         , DEREF(IBSEL) ,
     &             TIVAL         , TEREF(IBSEL) ,
     &             ZFVAL         , ZEREF(IBSEL) ,
     &             BMVAL         , BMREF(IBSEL) ,
     &             CWAVEL(IBSEL) , CDONOR(IBSEL),
     &             CRECVR(IBSEL) , CFILE(IBSEL) ,
     &             CPCODE(IBSEL) , CINDM(IBSEL) ,
     &             EAMU          , QEFA         , IEVAL      ,
     &             EFITM                        , QEFM       , NMX    ,
     &             LGRD1         , LDEF1        , LFSEL      ,
     &             XMIN          , XMAX         , YMIN       , YMAX
     &           )
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
          READ(PIPEIN,*) ISTOP
          IF(ISTOP.EQ.1) GOTO 9999
C
       ENDIF
C
C----------------------------------------------------------------------
C OUTPUT RESULTS
C----------------------------------------------------------------------
C
      IF (L2FILE) THEN
         OPEN(UNIT=IUNT07, FILE = SAVFIL, STATUS='UNKNOWN')
          CALL C3OUT0( IUNT07        , LFSEL        ,
     &                 TITLE         , TITLX        , TITLM  , DATE  ,
     &                 IBSEL         , IEVAL        ,
     &                 DIVAL         , DEREF(IBSEL) ,
     &                 TIVAL         , TEREF(IBSEL) ,
     &                 ZFVAL         , ZEREF(IBSEL) ,
     &                 BMVAL         , BMREF(IBSEL) ,
     &                 CWAVEL(IBSEL) , CDONOR(IBSEL),
     &                 CRECVR(IBSEL) , CFILE(IBSEL) ,
     &                 CPCODE(IBSEL) , CINDM(IBSEL) ,
     &                 LERNG         ,
     &                 EAMU          , EATM         , ECMS   ,
     &                 QEFA          ,
     &                 KPLUS1        , COEF
     &               )
         CLOSE(IUNT07)
      ENDIF
C
C-----------------------------------------------------------------------
C  RETURN TO ISPF PANELS. (ON RETURN UNIT 10 IS CLOSED)
C-----------------------------------------------------------------------
C
      GOTO 300
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(/1X,'MINIMAX INFORMATION:'/1X,20('-')/1X,1A79/)
C
C-----------------------------------------------------------------------
C
 9999 END
