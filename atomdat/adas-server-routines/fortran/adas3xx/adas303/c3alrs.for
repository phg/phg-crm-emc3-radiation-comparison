CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas303/c3alrs.for,v 1.1 2004/07/06 11:49:24 whitefor Exp $ Date $Date: 2004/07/06 11:49:24 $
CX
      SUBROUTINE C3ALRS( IORD , EM1  , EM2  ,
     &                   EPRO , TTAR , ETHR ,
     &                   YA   , N    , NS   ,
     &                   OA   , SQEF
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ********************** FORTRAN77 SUBROUTINE : C3ALRS ****************
C
C  PURPOSE:  COMPUTES ALPHAS AND REDUCED SPEEDS.  RETURNS AN EFFECTIVE
C            CHARGE-EXCHANGE RATE COEFFICIENT
C
C  CALLING PROGRAM: SQEF
C
C  SUBROUTINE:
C
C  INPUT : (I*4)   IORD  = 1 FOR 1ST PARTICLE INCIDENT AND MONOENERGETIC
C                        = 2 FOR 2ND PARTICLE INCIDENT AND MONOENERGETIC
C  INPUT : (I*4)   ISEL  = SELECTOR FOR PARTICULAR RATE COEFFT.
C                          CHOSEN FROM TABLE
C  INPUT : (R*8)   EM1   = ATOMIC MASS NUMBER FOR 1ST PARTICLE
C  INPUT : (R*8)   EM2   = ATOMIC MASS NUMBER FOR 2ND PARTICLE
C  INPUT : (R*8)   EPRO  = INCIDENT PARTICLE ENERGY (EV/AMU)
C  INPUT : (R*8)   TTAR  = MAXWELL TEMPERATURE OF TARGET PARTICLES (EV)
C  INPUT : (R*8)   ETHR  = THRESHOLD ENERGY
C  INPUT : (R*8)   ZSEL  = NUCLEAR CHARGE (REQUIRED ONLY
C                          FOR PARTICULAR ISEL)
C  INPUT : (I*4)   NSEL  = PRINC. QUANTUM NO. (REQUIRED ONLY
C                          FOR PARTICULAR ISEL
C                          NB. NSEL SHOULD BE ZERO ON ENTRY OTHERWISE)
C  INPUT : (I*4)   N     = NUMBER OF SOURCE DATA VALUES
C
C  OUTPUT: (R*8)   SQEF  = RATE COEFFICIENT (CM3 SEC-1)
C  OUTPUT: (R*8)   OA()  = RATE COEFFTS.(CM**3 SEC-1) FOR SELECTED
C                          SOURCE DATA
C  OUTPUT: (R*8)   EA()  = SET OF ENERGIES (EV/AMU) FOR
C                          SELECTED SOURCE DATA
C  I/O   : (R*8)   YA()  = ENERGIES ON INPUT, SPEEDS ON OUTPUT
C
C          (I*4)   ISWIT = ENERGY RANGE SWITCHING INDEX
C          (I*4)   I     = GENERAL INDEX
C          (I*4)   K     = GENERAL INDEX
C
C          (R*8)   ABI   = FUNCTION - SEE BELOW
C          (R*8)   EMT   = SELECTED MASS
C          (R*8)   F     = GENERAL VARIABLE
C          (R*8)   SUM   = GENERAL VARIABLE
C          (R*8)   SXI   = GENERAL VARIABLE
C          (R*8)   SXXI  = GENERAL VARIABLE
C          (R*8)   U     = GENERAL VARIABLE
C          (R*8)   V     = GENERAL VARIABLE
C          (R*8)   VTHR  = THRESHOLD SPEED
C          (R*8)   X     = GENERAL VARIABLE
C          (R*8)   XI    = GENERAL VARIABLE
C          (R*8)   XRMIN = GENERAL VARIABLE
C          (R*8)   XXI   = GENERAL VARIABLE
C          (R*8)   XA()  = GAUSS-LAGUERRE NODES   (9-POINT)
C          (R*8)   WXA() = GAUSS-LAGUERRE WEIGHTS (9-POINT)
C
C
C  ROUTINES:
C           ROUTINE    SOURCE    BRIEF DESCRIPTION
C           ------------------------------------------------------------
C           ABI        ADAS      COMPUTES INTEGRAL FOR RATE COEFFICIENT
C
C  AUTHOR:  C J. WHITEHEAD, PAP, UNIVERSITY OF STRATHCLYDE
C           EXT 4205
C
C  DATE:    14/11/94
C
C  UPDATE:  09/12/94 - HP SUMMERS: ADJUST FORMATTING
C
C  UPDATE:  03/05/95 - PE BRIDEN : ADD DATA DECLARATION FOR F AND EMT.
C                                  (STOPS COMPILATION WARNING OF
C                                   UNINITIALISED VARIABLES.)
C
C  UPDATE:  15/05/95 - TIM HAMMOND: UNIX PORT - PUT INTO SCCS
C
C----------------------------------------------------------------------
C----------------------------------------------------------------------
      INTEGER IORD   , N     , ISWIT , K     , NS    , I
C----------------------------------------------------------------------
      REAL*8  EM1    , EM2   , EPRO  , TTAR  , ETHR  ,
     &        EMT    , U     , V     , VTHR  , ABI
      REAL*8  XI     , X     , XRMIN , SUM   , SXI   , F     , XXI   ,
     &        SQEF   , SXXI
C----------------------------------------------------------------------
      REAL*8  XA(9)     , WXA(9)
      REAL*8  YA(24)    , FA(24)   , OA(24)
C----------------------------------------------------------------------
C     GAUSS-LAGUERRE DATA (9PT)
C----------------------------------------------------------------------
      DATA F  / 0.0 / ,
     &     EMT/ 0.0 /
      DATA XA / 0.1523222277D0   , 0.8072200227D0   , 2.0051351556D0   ,
     &          3.7834739733D0   , 6.2049567778D0   , 9.3729852516D0   ,
     &          13.4662369110D0  , 18.8335977889D0  , 26.3740718909D0  /
      DATA WXA/ 3.36126421798D-1 , 4.11213980424D-1 , 1.99287525371D-1 ,
     &          4.74605627657D-2 , 5.59962661079D-3 , 3.05249767093D-4 ,
     &          6.59212302608D-6 , 4.11076933035D-8 , 3.29087403035D-11/
C----------------------------------------------------------------------
C----------------------------------------------------------------------
      IF (IORD.EQ.1) EMT=EM2
      IF (IORD.EQ.2) EMT=EM1
      V = 1.38377D6*DSQRT(EPRO)
      U = 1.38377D6*DSQRT(TTAR/EMT)
      VTHR=1.38377D6*DSQRT(ETHR*(EM1+EM2))/(EM1*EM2)
      X=V/U
      XRMIN=VTHR/U
C
      DO I=1,N-1
         FA(I)=-DLOG(OA(I)/OA(I+1))/DLOG(YA(I)/YA(I+1))
         YA(I)=1.38377D6*DSQRT(YA(I))/U
      END DO
C
      YA(N)=1.38377D6*DSQRT(YA(N))/U
      FA(N)=FA(N-1)
      SUM=0.0D0
C
      DO 100 I=1,9
         XI=XA(I)
         SXI=DSQRT(XI)
         ISWIT=1
         IF(X.LT.XRMIN) ISWIT=2
         IF(X.EQ.0.0D0) ISWIT=3
C
         GO TO (30,50,70),ISWIT
C
 30      IF(SXI.GE.0.0D0.AND.SXI.LE.X-XRMIN) THEN
            F=ABI(OA,YA,FA,X-SXI,X+SXI,N)
         ENDIF
         IF(SXI.GT.X-XRMIN.AND.SXI.LE.X+XRMIN)THEN
            F=ABI(OA,YA,FA,XRMIN,X+SXI,N)
         ENDIF
         IF(SXI.GT.X+XRMIN) THEN
            F=ABI(OA,YA,FA,SXI-X,X+SXI,N)
         ENDIF
         GO TO 100
C
 50      XXI=XI+XRMIN**2
         SXXI=DSQRT(XXI)
         IF(SXXI.GE.XRMIN-X.AND.SXXI.LE.X+XRMIN) THEN
            F=ABI(OA,YA,FA,XRMIN,SXXI+X,N)
         ENDIF
         IF(SXXI.GT.X+XRMIN) THEN
            F=ABI(OA,YA,FA,SXXI-X,X+SXXI,N)
         ENDIF
         GO TO 100
C
 70      XXI=XI+XRMIN**2
         SXXI=DSQRT(XXI)
         K=0
 75      K=K+1
         IF(K.EQ.N+1) GO TO 80
         IF(SXXI.GT.YA(K)) GO TO 75
 80      K=K-1
         IF(K.EQ.0) K=1
         F=XXI*OA(K)*(YA(K)/SXXI)**(2.0D0*FA(K))
         GO TO 100
  100 SUM=SUM+F*WXA(I)
C
      GO TO (110,120,130),ISWIT
C
  110 SQEF=5.64190D-1*U*SUM/X
      RETURN
  120 SQEF=5.64190D-1*U*DEXP(-XRMIN*XRMIN)*SUM/X
      RETURN
  130 SQEF=1.12838D0*U*DEXP(-XRMIN*XRMIN)*SUM
      RETURN
      END
