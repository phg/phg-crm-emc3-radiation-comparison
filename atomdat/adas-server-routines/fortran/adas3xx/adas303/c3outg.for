CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas303/c3outg.for,v 1.1 2004/07/06 11:50:09 whitefor Exp $ Date $Date: 2004/07/06 11:50:09 $
CX
      SUBROUTINE C3OUTG( LGHOST ,
     &                   TITLE  , TITLX  , TITLM , DATE  ,
     &                   DIVAL  , DEREF  ,
     &                   TIVAL  , TEREF  ,
     &                   ZFVAL  , ZEREF  ,
     &                   BMVAL  , BMREF  ,
     &                   CWAVEL , CDONOR ,
     &                   CRECVR , CFILE  ,
     &                   CPCODE , CINDM  ,
     &                   EAMU   , QEFA   , IEVAL ,
     &                   EFITM           , QEFM  , NMX   ,
     &                   LGRD1  , LDEF1  , LFSEL ,
     &                   XMIN   , XMAX   , YMIN  , YMAX
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C3OUTG *********************
C
C  PURPOSE:  PIPE COMMUNICATION WITH IDL TO PLOT GRAPH             
C
C            PROVIDES COMPARATIVE GRAPH OF:
C                                SPLINE INTERPOLATED POINTS: CROSSES
C                                CURVE THROUGH SPLINE POINTS: FULL CURVE
C                                MINIMAX FIT TO SPLINE DATA : DASH CURVE
C
C            PLOT IS LOG10(EFF. EMIS. COEF.) V LOG10(BM. ENRGY(EV/AMU)
C
C  CALLING PROGRAM: ADAS303
C
C  SUBROUTINE:
C
CX UNIX PORT - LGHOST RETAINED ONLY TO KEEP ARGUMENT LIST THE SAME.
C  INPUT : (L*4)  LGHOST  = .TRUE.  => GHOST80 INITIALISED
C                           .FALSE. => GHOST80 NOT INITIALISED
C
C  INPUT : (C*40) TITLE   = TITLE OF RUN (READ FROM ISPF PANEL)
CX INPUT : (C*120)TITLX   = INFORMATION STRING CONTAINING: INPUT DATA
C                           FILE-NAME, SELECTED BLOCK,TYPE AND TRANSIT.
C  INPUT : (C*80) TITLM   = DIAGNOSTIC LINE INFORMATION FROM 'MINIMAX'
C  INPUT : (C*8)  DATE    = CURRENT DATE (AS 'DD/MM/YY')
C
C  INPUT : (R*8)  DIVAL     = SELECTED OUTPUT VALUE: ION DENSITY (CM-3)
C  INPUT : (R*8)  DEREF     = REFERENCE VALUE      : ION DENSITY (CM-3)
C
C  INPUT : (R*8)  TIVAL     = SELECTED OUTPUT VALUE: ION TEMPERATURE(EV)
C  INPUT : (R*8)  TEREF     = REFERENCE VALUE      : ION TEMPERATURE(EV)
C
C  INPUT : (R*8)  ZFVAL     = SELECTED OUTPUT VALUE: Z-EFFECTIVE
C  INPUT : (R*8)  ZEREF     = REFERENCE VALUE      : Z-EFFECTIVE
C
C  INPUT : (R*8)  BMVAL     = SELECTED OUTPUT VALUE: MAGNETIC FIELD (T)
C  INPUT : (R*8)  BMREF     = REFERENCE VALUE      : MAGNETIC FIELD (T)
C
C  INPUT : (C*5)  CWAVEL  = INPUT DATA FILE: TRANSITION
C  INPUT : (C*8)  CDONOR  = INPUT DATA FILE: DONOR NEUTRAL ATOM
C  INPUT : (C*5)  CRECVR  = INPUT DATA FILE: RECEIVER NUCLEUS
C  INPUT : (C*8)  CFILE   = INPUT DATA FILE: SPECIFIC ION FILE SOURCE
C  INPUT : (C*8)  CPCODE  = INPUT DATA FILE: PROCESSING CODE
C  INPUT : (C*6)  CINDM   = FILE DATA FILE: EMISSION TYPE
C
C  INPUT : (R*8)  EAMU()  = USER ENTERED: BEAM ENERGIES (EV/AMU)
C                           DIMENSION: BEAM ENERGY INDEX
C  INPUT : (R*8)  QEFA()  = SPLINE INTERPOLATED OR  EXTRAPOLATED  EFF.
C                           EMISS. COEFFTS. FOR THE USER ENTERED BEAM
C                           ENERGIES.
C                           DIMENSION: BEAM ENERGY INDEX
C  INPUT : (I*4)  IEVAL   = NUMBER OF USER ENTERED BEAM ENERGIES.
C
C  INPUT : (R*8)  EFITM() = MINIMAX: SELECTED BEAM ENERGIES(EV/AMU)
C  INPUT : (R*8)  QEFM()  = EFF. EMISSION COEFFTS. AT 'EFITM()'
C  INPUT : (I*4)  NMX     = NUMBER OF MINIMAX GENERATED EFF. EMIS. COEF.
C
C  INPUT : (L*4)  LGRD1   = .TRUE.  => PUT GRAPH IN GRID FILE
C                           .FALSE. => DO NOT PUT GRAPH IN GRID FILE
C  INPUT : (L*4)  LDEF1   = .TRUE.  => USE DEFAULT GRAPH SCALING
C                           .FALSE. => DO NOT USE DEFAULT GRAPH SCALING
C  INPUT : (L*4)  LFSEL   = .TRUE.  => CARRY OUT MINIMAX POLYNOMIAL
C                                      FITTING
C                           .FALSE. => - DO NOT DO THE ABOVE -
C
C  INPUT : (R*8)  XMIN    = GRAPH: LOWER LIMIT FOR BEAM ENERGY
C  INPUT : (R*8)  XMAX    = GRAPH: UPPER LIMIT FOR BEAM ENERGY
C  INPUT : (R*8)  YMIN    = GRAPH: LOWER LIMIT FOR EFF. EMIS. COEFFT.
C  INPUT : (R*8)  YMAX    = GRAPH: UPPER LIMIT FOR EFF. EMIS. COEFFT.
C
C
C          (R*4)  GHZERO  = PARAMETER = VALUE BELOW WHICH GHOST80
C                           TAKES NUMBERS AS BEING ZERO = 1.0E-36
C          (R*4)  YDMIN   = PARAMETER = MINIMUM ALLOWED Y-VALUE FOR
C                           PLOTTING. (USED FOR DEFAULT GRAPH SCALING)
C                           (SET TO 'GHZERO'/ZERO TO REMOVE)
C
C          (I*4)  I4UNIT  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (I*4)  I       = GENERAL USE - ARRAY INDEX
C          (I*4)  IKEY    = NUMBER OF 'KEY()' VALUES TO BE OUTPUT
C          (I*4)  ICOUNT  = NUMBER OF POINTS PLOTTED FOR GRAPH CURVE
C
C          (R*4)  XHIGH   = UPPER X-AXIS LIMIT FOR USE WITH GHOST80
C          (R*4)  XLOW    = LOWER X-AXIS LIMIT FOR USE WITH GHOST80
C          (R*4)  YHIGH   = UPPER Y-AXIS LIMIT FOR USE WITH GHOST80
C          (R*4)  YLOW    = LOWER Y-AXIS LIMIT FOR USE WITH GHOST80
C          (R*4)  XA4()   = X-AXIS CO-ORDINATES FOR USE WITH GHOST80
C          (R*4)  YA4()   = Y-AXIS CO-ORDINATES FOR USE WITH GHOST80
C
C          (R*8)  R8VAL1()= 1-D SCAN POSITIONS - OUTPUT DATA VALUES
C                           DIMENSION: 4
C          (R*8)  R8VAL2()= 1-D SCAN POSITIONS - OUTPUT REFERENCE VALUES
C                           DIMENSION: 4
C
C
C          (C*88) ISPEC   = GRAPH TITLE (INCORPORATES 'TITLE')
C          (C*80) CADAS   = ADAS HEADER: INCLUDES RELEASE, PROGRAM, TIME
C          (C*12) DNAME   = '      DATE: '
C          (C*25) XTITE   = X-AXIS UNITS/TITLE: NEUTRAL BEAM ENERGY
C          (C*22) YTIT    = Y-AXIS UNITS/TITLE
C          (C*9)  KEY0    = '    KEY: '
C          (C*9)  MNMX0   = 'MINIMAX: '
C          (C*8)  ADAS0   = 'ADAS   :'
C          (C*28) KEY()   = DESCRIPTIVE KEY FOR GRAPH (2 TYPES)
C
C          (C*1)  GRID    = DUMMY NAME VARIABLE FOR USE WITH GHOST80
C          (C*1)  PIC     = DUMMY NAME VARIABLE FOR USE WITH GHOST80
C          (C*3)  C3BLNK  = BLANK 3 BYTE STRING
C          (C*7)  C7      =  7 BYTE STRING = 'TITLX(1:4)'//'C3BLNK'
C
C          (C*28) STRG1   =  DESCRIPTIVE STRING FOR ELEMENT SYMBOL
C          (C*28) STRG2   =  DESCRIPTIVE STRING FOR NUCLEAR CHARGE
C          (C*28) STRG3   =  DESCRIPTIVE STRING FOR CHARGE-STATE
C          (C*28) STRG4   =  DESCRIPTIVE STRING TRANSITION
C          (C*28) STRG5   =  DESCRIPTIVE STRING NEUTRAL DONOTOR ATOM
C          (C*28) STRG6   =  DESCRIPTIVE STRING BARE NUCLEUS RECEIVER
C          (C*28) STRG7   =  DESCRIPTIVE STRING QCX SOURCE FILE MEMBER
C          (C*28) STRG8   =  DESCRIPTIVE STRING PROCESSING CODE
C          (C*28) STRG9   =  DESCRIPTIVE STRING EMISSION TYPE
C          (C*13) STRG10()=  DESCRIPTIVE STRINGS: 1-D SCAN POSITIONS
C                            DIMENSION: 4
C          (C*32) HEAD1   =  HEADING FOR RECEIVER INFORMATION
C          (C*45) HEAD2A  =  FIRST  HEADER FOR RECEIVER/DONOR ENER. INFO
C          (C*45) HEAD2B  =  SECOND HEADER FOR RECEIVER/DONOR ENER. INFO
C
C ROUTINES:
C          ROUTINE     SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          XXADAS      ADAS      GATHERS ADAS HEADER INFORMATION
C          XXLIM8      ADAS      SETTING UP OF DEFAULT GRAPH AXES
C          XXGSEL      ADAS      SELECTS POINTS WHICH WILL FIT ON GRAPH
C          I4UNIT      ADAS      GET UNIT NUMBER FOR OUTPUT OF MESSAGES
C
C AUTHOR:  H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C          JA 8.08
C          TEL. 0141-553-4196
C
C DATE:    23/04/95
C
C UPDATE:  03/05/95 PE BRIDEN - ADDED/MODIFIED 1-D SCAN POSITIONS TABLE.
C                               (REQD. UPDATE OF ARGUMENT LIST)
C                             + OTHER MINOR MODS.
C
C UPDATE:  15/05/95 TIM HAMMOND - UNIX PORT
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      REAL*4     GHZERO           , YDMIN
C-----------------------------------------------------------------------
      PARAMETER( GHZERO = 1.0E-36 , YDMIN = 1.0E-20 )
C-----------------------------------------------------------------------
      INTEGER    I4UNIT
      INTEGER    IEVAL           , NMX
      INTEGER    I               , IKEY             , ICOUNT
C-----------------------------------------------------------------------
      REAL*4     XHIGH           , XLOW             ,
     &           YHIGH           , YLOW
C-----------------------------------------------------------------------
      REAL*8     XMIN            , XMAX             ,
     &           YMIN            , YMAX
C-----------------------------------------------------------------------
      REAL*8     DIVAL           , DEREF            ,
     &           TIVAL           , TEREF            ,
     &           ZFVAL           , ZEREF            ,
     &           BMVAL           , BMREF
C-----------------------------------------------------------------------
      LOGICAL    LGHOST          ,
     &           LGRD1           , LDEF1            , LFSEL
C-----------------------------------------------------------------------
      CHARACTER  TITLE*40       , TITLX*120         , TITLM*80  ,
     &           DATE*8
      CHARACTER  CINDM*6        , CFILE*8           ,
     &           CPCODE*8       , CWAVEL*5          ,
     &           CDONOR*8       , CRECVR*5
      CHARACTER  GRID*1         , PIC*1             , C3BLNK*3  , C7*7 ,
     &           MNMX0*9        , KEY0*9            , ADAS0*8   ,
     &           DNAME*12       ,
     &           XTITE*25       , YTIT*32   ,
     &           CADAS*80       , ISPEC*88
      CHARACTER  STRG1*28       , STRG2*28          ,
     &           STRG3*28       , STRG4*28          ,
     &           STRG5*28       , STRG6*28          , STRG7*28         ,
     &           STRG8*28       , STRG9*28          ,
     &           HEAD1*32
      CHARACTER  STRG10(4)*13   ,
     &           HEAD2A*45      , HEAD2B*45
C-----------------------------------------------------------------------
      REAL*4     XA4(100)       , YA4(100)
C-----------------------------------------------------------------------
      REAL*8     R8VAL1(4)      , R8VAL2(4)
C-----------------------------------------------------------------------
      REAL*8     EAMU(IEVAL)    , QEFA(IEVAL)      ,
     &           EFITM(NMX)     , QEFM(NMX)
C-----------------------------------------------------------------------
      CHARACTER  KEY(2)*28
C-----------------------------------------------------------------------
      SAVE       ISPEC          , CADAS
C-----------------------------------------------------------------------
      DATA ISPEC(1:24)
     &           /'EFF. EMISSION COEFFTS.  '/
      DATA DNAME /'      DATE: '/
      DATA XTITE /'BEAM ENERGY (EV/AMU)     '/
      DATA YTIT  /'EFF. EMISSION COEFFT. (cm3/sec)'/
      DATA ADAS0 /'ADAS   :'/
     &     MNMX0 /'MINIMAX: '/               ,
     &     KEY0  /'KEY    : '/               ,
     &     KEY(1)/'(CROSSES/FULL LINE - SPLINE)'/  ,
     &     KEY(2)/'  (DASH LINE - MINIMAX)     '/
      DATA GRID  /' '/   ,
     &     PIC   /' '/   ,
     &     C3BLNK/'   '/ ,
     &     CADAS /' '/
      DATA HEAD1 /'--- EMITTING ION INFORMATION ---'/              ,
     &     HEAD2A/'----- ONE DIMENSIONAL SCAN POSITIONS -----' / ,
     &     HEAD2B/'  PARAMETER         VALUE        REFERENCE   ' /
      DATA STRG1 /' ELEMENT SYMBOL           = '/ ,
     &     STRG2 /' NUCLEAR CHARGE           = '/ ,
     &     STRG3 /' CHARGE STATE             = '/ ,
     &     STRG4 /' TRANSITION (N-N'')        = '/ ,
     &     STRG5 /' NEUTRAL ATOM DONOR       = '/ ,
     &     STRG6 /' BARE NUCLEUS RECEIVER    = '/
     &     STRG7 /' QCX SOURCE FILE MEMBER   = '/ ,
     &     STRG8 /' PROCESSING CODE          = '/ ,
     &     STRG9 /' EMISSION TYPE            = '/
      DATA STRG10/'DENSI (cm-3) ',
     &            'TI    (eV)   ',
     &            'ZEFF         ',
     &            'BMAG  (T)    '/
C-----------------------------------------------------------------------
      INTEGER    PIPEIN    , PIPEOU , ONE      , ZERO
      PARAMETER( PIPEIN=5  , PIPEOU=6 , ONE=1    , ZERO=0 )
      INTEGER    J        , K        , L
C
C-----------------------------------------------------------------------
C WRITE OUT TO PIPE
C-----------------------------------------------------------------------
      WRITE(PIPEOU,'(A40)') TITLE
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A120)') TITLX
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A80)') TITLM
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A8)') DATE
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A5)') CWAVEL
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A8)') CFILE
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A8)') CPCODE
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A8)') CDONOR
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A5)') CRECVR
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A6)') CINDM
      CALL XXFLSH(PIPEOU)

      WRITE(PIPEOU,*) IEVAL
      CALL XXFLSH(PIPEOU)
      DO 1 I = 1,IEVAL
         WRITE(PIPEOU,*) EAMU(I)
         CALL XXFLSH(PIPEOU)
 1    CONTINUE
      DO 2 I = 1,IEVAL
         WRITE(PIPEOU,*) QEFA(I)
         CALL XXFLSH(PIPEOU)
 2    CONTINUE

      IF (.NOT. LDEF1 ) THEN
         WRITE(PIPEOU,*) ZERO
         CALL XXFLSH(PIPEOU)
      ELSE
         WRITE(PIPEOU,*) ONE
         CALL XXFLSH(PIPEOU)
         WRITE(PIPEOU,*) XMIN
         CALL XXFLSH(PIPEOU)
         WRITE(PIPEOU,*) XMAX
         CALL XXFLSH(PIPEOU)
         WRITE(PIPEOU,*) YMIN
         CALL XXFLSH(PIPEOU)
         WRITE(PIPEOU,*) YMAX
         CALL XXFLSH(PIPEOU)
      ENDIF
      IF (LFSEL) THEN
         WRITE(PIPEOU,*) ONE
         CALL XXFLSH(PIPEOU)
         WRITE(PIPEOU,*) NMX
         CALL XXFLSH(PIPEOU)
         DO 4 I = 1, NMX
            WRITE(PIPEOU,*) QEFM(I)
            CALL XXFLSH(PIPEOU)
 4       CONTINUE
         DO  5 I = 1, NMX
            WRITE(PIPEOU,*) EFITM(I)
            CALL XXFLSH(PIPEOU)
 5       CONTINUE
      ELSE
         WRITE(PIPEOU,*) ZERO
         CALL XXFLSH(PIPEOU)
      ENDIF

      WRITE(PIPEOU,'(A32)') HEAD1
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A45)') HEAD2A
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,'(A45)') HEAD2B
      CALL XXFLSH(PIPEOU)

      WRITE(PIPEOU,*) BMVAL
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) ZFVAL
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) TIVAL
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) DIVAL
      CALL XXFLSH(PIPEOU)

      WRITE(PIPEOU,*) BMREF
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) ZEREF
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) TEREF
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) DEREF
      CALL XXFLSH(PIPEOU)
C
C-----------------------------------------------------------------------
C
      RETURN
C-----------------------------------------------------------------------
      END
