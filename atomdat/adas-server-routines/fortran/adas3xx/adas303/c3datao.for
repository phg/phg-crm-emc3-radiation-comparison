CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas303/c3datao.for,v 1.2 2004/07/06 11:49:39 whitefor Exp $ Date $Date: 2004/07/06 11:49:39 $
CX
      SUBROUTINE C3DATAO( IUNIT  , IBSEL  ,
     &                    NBSEL  , NSTORE ,
     &                    IPASS  ,
     &                    IONNAM , QEFREF ,
     &                    TEREF  , DEREF  ,
     &                    ZEREF  , ENREF  ,
     &                    BMREF  , NENERA ,
     &                    NDENSA , NZEFFA ,
     &                    NBMAGA , NTEMPA ,
     &                    ENERA  , QENERA ,
     &                    TEMPA  , QTEMPA ,
     &                    DENSA  , QDENSA ,
     &                    ZEFFA  , QZEFFA ,
     &                    BMAGA  , QBMAGA )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C3DATAO ********************
C
C **********************************************************************
C * WARNING - WARNING - WARNING - WARNING - WARNING - WARNING - WARNING*
C *--------------------------------------------------------------------*
C * OLD VERSION OF C3DATA - ONLY USED BY SQEF (UNTIL SQEF UPDATED)     *
C **********************************************************************
C
C  PURPOSE : READ IN VALUES FROM AN 'IONATOM' DATA SET OPENED BY
C            C3FILE
C
C  CALLING PROGRAM: SQEF
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNIT     = UNIT NUMBER TO READ FROM
C                             (OPENED BY C3FILE)
C  INPUT : (I*4)  NSTORE    = ARRAY DIMENSION
C  INPUT : (I*4)  ISEL      = INDEX NUMBER OF SELECTED BLOCK
C                             FROM IONATOM FILE
C  INPUT : (I*4)  IPASS     = 0 IF DATA FILE TO BE READ IN AFRESH
C                           = 1 IF DATA FILE IS NOT TO BE READ IN AGAIN
C                             (IPASS IS SET TO 0 WHEN
C                              ISEL IS NOT A VALID INDEX)
C  OUTPUT: (I*4)  NBSEL     = NUMBER OF BLOCKS PRESENT
C  OUTPUT: (C*80) IONNAM    = NAME OF ION
C
C  OUTPUT: (R*8)  QEFREF()  = REFERENCE VALUE OF RATE COEFFICIENT
C  OUTPUT: (R*8)  ENREF()   =     "       "   "  ENERGY
C  OUTPUT: (R*8)  TEREF()   =     "       "   "  TEMPERATURE
C  OUTPUT: (R*8)  DEREF()   =     "       "   "  DENSITY
C  OUTPUT: (R*8)  ZEREF()   =     "       "   "  EFFECTIVE Z
C  OUTPUT: (R*8)  BMREF()   =     "       "   "  MAGNETIC FIELD
C  OUTPUT: (I*4)  NENERA()  = NUMBER OF ENERGIES
C  OUTPUT: (I*4)  NTEMPA()  = NUMBER OF TEMPERATURES
C  OUTPUT: (I*4)  NDENSA()  = NUMBER OF DENSITIES
C  OUTPUT: (I*4)  NZEFFA()  = NUMBER OF EFFECTIVE Z'S
C  OUTPUT: (I*4)  NBMAGA()  = NUMBER OF MAGNETIC FIELD VALUES
C                             1ST. DIM: NSTORE
C                             (FOR ABOVE ARRAYS)
C  OUTPUT: (R*8)  ENERA(,)  = ENERGIES
C  OUTPUT: (R*8)  QENERA(,) = RATE COEFFICIENTS FOR ENERGY VALUE
C  OUTPUT: (R*8)  TEMPA(,)  = TEMPERATURES
C  OUTPUT: (R*8)  QTEMPA(,) = RATE COEFFICIENTS FOR TEMPERATURES
C  OUTPUT: (R*8)  DENSA(,)  = DENSITIES
C  OUTPUT: (R*8)  QDENSA(,) = RATE COEFFICIENTS FOR DESNITIES
C  OUTPUT: (R*8)  ZEFFA(,)  = EFFECTIVE Z
C  OUTPUT: (R*8)  QZEFFA(,) = RATE COEFFICIENTS FOR EFFECTIVE Z
C  OUTPUT: (R*8)  BMAGA(,)  = MAGNETIC FIELD
C  OUTPUT: (R*8)  QBMAGA(,) = RATE COEFFICIENTS FOR MAGNETIC FIELDS
C                             1ST DIM: NSTORE
C                             2ND DIM: 12 OR 24  DEPENDING ON PARAMETER
C
C  ROUTINES:
C           ROUTINE    SOURCE    BRIEF DESCRIPTION
C           ------------------------------------------------------------
C           I4UNIT     ADAS      FETCHES FILE HANDLE FOR ERROR MESSAGE
C
C UPDATE: 15/05/95	-	Tim Hammond	UNIX PORT
C				Put under SCCS control
C
C VERSION:	1.2					DATE: 08-11-99
C MODIFIED: RICHARD MARTIN
C		CHANGED IONNAM*80(80) TO IONNAM(80)*80
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER IUNIT  , ISTORE , NBSEL  , J     ,
     &        IBSEL  , NSTORE , IPASS
      INTEGER I4UNIT
C-----------------------------------------------------------------------
      CHARACTER STRING*80
      CHARACTER IONNAM(80)*80
C-----------------------------------------------------------------------
      INTEGER NENERA(NSTORE), NTEMPA(NSTORE), NDENSA(NSTORE)
      INTEGER NZEFFA(NSTORE), NBMAGA(NSTORE)
C-----------------------------------------------------------------------
      REAL*8 QEFREF(NSTORE)
      REAL*8 ENREF(NSTORE) , TEREF(NSTORE)
      REAL*8 DEREF(NSTORE) , ZEREF(NSTORE)
      REAL*8 BMREF(NSTORE)
      REAL*8 ENERA(NSTORE,24), QENERA(NSTORE,24)
      REAL*8 TEMPA(NSTORE,12), QTEMPA(NSTORE,12)
      REAL*8 DENSA(NSTORE,24), QDENSA(NSTORE,24)
      REAL*8 ZEFFA(NSTORE,12), QZEFFA(NSTORE,12)
      REAL*8 BMAGA(NSTORE,12), QBMAGA(NSTORE,12)
C-----------------------------------------------------------------------
      EXTERNAL I4UNIT
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      READ(IUNIT,*)NBSEL
      DO ISTORE=1,NBSEL
         READ(IUNIT,1000) IONNAM(ISTORE)
         READ(IUNIT,1001) QEFREF(ISTORE)
         READ(IUNIT,1001) ENREF(ISTORE), TEREF(ISTORE), DEREF(ISTORE),
     &        ZEREF(ISTORE), BMREF(ISTORE)
         READ(IUNIT,1002) NENERA(ISTORE),NTEMPA(ISTORE),NDENSA(ISTORE),
     &        NZEFFA(ISTORE),NBMAGA(ISTORE)
         READ(IUNIT,1001) (ENERA(ISTORE,J),J=1,24)
         READ(IUNIT,1001) (QENERA(ISTORE,J),J=1,24)
         READ(IUNIT,1001) (TEMPA(ISTORE,J),J=1,12)
         READ(IUNIT,1001) (QTEMPA(ISTORE,J),J=1,12)
         READ(IUNIT,1001) (DENSA(ISTORE,J),J=1,24)
         READ(IUNIT,1001) (QDENSA(ISTORE,J),J=1,24)
         READ(IUNIT,1001) (ZEFFA(ISTORE,J),J=1,12)
         READ(IUNIT,1001) (QZEFFA(ISTORE,J),J=1,12)
         READ(IUNIT,1001) (BMAGA(ISTORE,J),J=1,12)
         READ(IUNIT,1001) (QBMAGA(ISTORE,J),J=1,12)
      END DO
      IF(IBSEL.LE.0.OR.IBSEL.GT.NBSEL)THEN
         IPASS=0
         RETURN
      ELSE
         IPASS=1
      ENDIF
C
      RETURN
C-----------------------------------------------------------------------
 1000 FORMAT(1A80)
 1001 FORMAT(6D10.2)
 1002 FORMAT(5I10)
C-----------------------------------------------------------------------
      END
