CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas303/c3spln.for,v 1.1 2004/07/06 11:50:46 whitefor Exp $ Date $Date: 2004/07/06 11:50:46 $
CX
      SUBROUTINE C3SPLN( NEIN   , NEOUT   ,
     &                   EIN    , EOUT    ,
     &                   QEF    , QEFA    ,
     &                   LERNG
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C3SPLN *********************
C
C  PURPOSE:
C          PERFORMS CUBIC SPLINE ON LOG(BEAM ENERGY <EV/AMU> ) VERSUS
C          LOG(EFF. EMISSION COEFFICIENTS).
C          INPUT DATA FOR A GIVEN EMITTING ION EMISS. COEFF. DATA BLOCK
C
C          USING  ONE-WAY SPLINES  IT  CALCULATES  THE  EFF. EMISSION
C          COEFFICIENT  FOR  'NEOUT'  BEAM ENERGY  VALUES  FROM
C          THE LIST OF BEAM ENERGIES READ IN FROM THE INPUT FILE
C
C          IF A  VALUE  CANNOT  BE  INTERPOLATED  USING  SPLINES  IT  IS
C          EXTRAPOLATED VIA 'XXSPLE'. (SEE NOTES BELOW).
C
C  CALLING PROGRAM: ADAS303/SQEF
C
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  NEIN    = INPUT DATA FILE: NUMBER OF BEAM ENERGIES
C                           READ FOR THE DATA-BLOCK BEING ASSESSED
C  INPUT : (I*4)  NEOUT   = NUMBER OF ISPF ENTERED BEAM ENERGY
C                           VALUES FOR  WHICH  EFF. EMISS. COEFFTS.
C                           ARE REQUIRED FOR TABULAR/GRAPHICAL OUTPUT.
C
C  INPUT : (R*8)  EIN()   = INPUT DATA FILE: BEAM ENERGIES (EV/AMU)
C                           FOR THE DATA-BLOCK BEING ASSESSED.
C                           DIMENSION: ELECTRON TEMPERATURE INDEX
C  INPUT : (R*8)  EOUT()  = USER ENTERED: BEAM ENERGIES (EV/AMU)
C                           DIMENSION: BEAM ENERGY INDEX
C
C  INPUT : (R*8)  QEF()    =INPUT DATA FILE: FULL SET OF EFF. EMISS.
C                           COEFFTS FOR THE DATA-BLOCK
C                           BEING ANALYSED.
C                           1ST DIMENSION: BEAM ENERGY INDEX
C  OUTPUT: (R*8)  QEFA()  = SPLINE INTERPOLATED OR  EXTRAPOLATED  EFF.
C                           EMISS. COEFFTS FOR
C                           THE USER ENTERED BEAM ENERGIES.
C                           DIMENSION: BEAM ENERGY INDEX
C
C  OUTPUT: (L*4)  LERNG()=  .TRUE.  => OUTPUT 'QEFA()' VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      BEAM ENERGY 'EOUT()'.
C                           .FALSE. => OUTPUT 'QEFA()' VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      BEAM ENERGY 'EOUT()'.
C                           DIMENSION: ELECTRON TEMPERATURE INDEX
C
C          (I*4)  NIN     = PARAMETER = MAX. NO. OF INPUT  BEAM ENERGIES
C                                       VALUES. MUST BE >= 'NEIN'
C          (I*4)  NOUT    = PARAMETER = MAX. NO. OF OUTPUT BEAM ENERGY
C                                       VALUES  MUST BE >= 'NEOUT'
C          (I*4)  L1      = PARAMETER = 1
C
C          (I*4)  IET     = ARRAY SUBSCRIPT USED INPUT  FILE  BEAM
C                           ENERGIES.
C          (I*4)  IT      = ARRAY  SUBSCRIPT  USED  FOR  USER  ENTERED
C                           BEAM ENERGIES.
C          (I*4)  IOPT    = DEFINES THE BOUNDARY DERIVATIVES FOR THE
C                           SPLINE ROUTINE 'XXSPLE', SEE 'XXSPLE'.
C                           (VALID VALUES = <0, 0, 1, 2, 3, 4)
C
C          (L*4)  LSETX   = .TRUE.  => SET UP SPLINE PARAMETERS RELATING
C                                      TO 'XIN' AXIS.
C                           .FALSE. => DO NOT SET UP SPLINE PARAMETERS
C                                      RELATING TO 'XIN' AXIS.
C                                      (I.E. THEY WERE SET IN A PREVIOUS
C                                            CALL )
C                           (VALUE SET TO .FALSE. BY 'XXSPLE')
C
C          (R*8)  R8FUN1  = FUNCTION - (SEE ROUTINES SECTION BELOW)
C
C          (R*8)  XIN()   = LOG( DATA FILE ELECTRON TEMPERATURES )
C          (R*8)  YIN()   = LOG( DATA FILE SCALED ION. RATE COEFFTS.)
C          (R*8)  XOUT()  = LOG( USER ENTERED ELECTRON TEMPS.)
C          (R*8)  YOUT()  = LOG( OUTPUT GENERATED SCALED ION. RATE COEF)
C          (R*8)  DF()    = SPLINE INTERPOLATED DERIVATIVES
C
C
C NOTE:
C
C          ONE-DIMENSIONAL SPLINE CARRIED OUT BY THIS SUBROUTINE:
C
C           LOG( QEF )  VS. LOG( Te )
C
C           TE      = ELECTRON TEMPERATURE (UNITS: EV)
C           QEF     = ZERO DENSITY RADIATED POWER COEFFICIENT
C                                          (UNITS: W CM**3)
C
C           EXTRAPOLATION CRITERIA:
C
C           LOW  TE: ZERO GRADIENT  EXTRAPOLATION (I.E. DY(1)  = 0.0)
C           HIGH TE: ZERO CURVATURE EXTRAPOLATION (I.E. DDY(N) = 0.0)
C
C           (THESE CRITERIA ARE MET BY CALLING XXSPLE WITH IOPT=4)
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXSPLE     ADAS      SPLINE SUBROUTINE (EXTENDED DIAGNOSTICS)
C          R8FUN1     ADAS      REAL*8 FUNCTION: ( X -> X )
C
C AUTHOR:  H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C          JA8.08
C          TEL. 0141-553-4196
C
C DATE:    21/04/95
C
C UPDATE:  15/05/95	Tim Hammond	UNIX PORT
C			Put under SCCS control
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    NIN                   , NOUT            , L1
C-----------------------------------------------------------------------
      PARAMETER( NIN    = 24           , NOUT = 24       , L1 = 1     )
C-----------------------------------------------------------------------
      INTEGER    NEIN                  , NEOUT
      INTEGER    IET                   , IT              , IOPT
C-----------------------------------------------------------------------
      REAL*8     R8FUN1
C-----------------------------------------------------------------------
      LOGICAL    LSETX
C-----------------------------------------------------------------------
      REAL*8     EIN(NEIN)             , EOUT(NEOUT)     ,
     &           QEF(NEIN)             , QEFA(NEOUT)
      REAL*8     DF(NIN)               ,
     &           XIN(NIN)              , YIN(NIN)        ,
     &           XOUT(NOUT)            , YOUT(NOUT)
C-----------------------------------------------------------------------
      LOGICAL    LERNG(NEOUT)
C-----------------------------------------------------------------------
      EXTERNAL   R8FUN1
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      IF (NIN.LT.NEIN)
     &             STOP ' C3SPLN ERROR: NIN < NEIN - INCREASE NIN'
      IF (NOUT.LT.NEOUT)
     &             STOP ' C3SPLN ERROR: NOUT < NEOUT - INCREASE NTOUT'
C
C-----------------------------------------------------------------------
C SET UP SPLINE BOUNDARY CONDITIONS - SWITCH ON EXTRAPOLATION.
C-----------------------------------------------------------------------
C
      LSETX  = .TRUE.
      IOPT   = 4
C
C-----------------------------------------------------------------------
C SET UP ARRAYS CONTAINING LOG VALUES OF INPUT/OUTPUT BEAM ENERGIES.
C-----------------------------------------------------------------------
C
         DO 1 IET=1,NEIN
            XIN(IET) = DLOG( EIN(IET) )
    1    CONTINUE
C
         DO 2 IT=1,NEOUT
            XOUT(IT) = DLOG( EOUT(IT)  )
    2    CONTINUE
C
C-----------------------------------------------------------------------
C SPLINE OVER ALL USER BEAM ENERGIES
C-----------------------------------------------------------------------
C
         DO 3 IET=1,NEIN
            YIN(IET) = DLOG( QEF(IET) )
    3    CONTINUE
C
      CALL XXSPLE( LSETX , IOPT    , R8FUN1 ,
     &             NEIN  , XIN     , YIN    ,
     &             NEOUT , XOUT    , YOUT   ,
     &             DF    , LERNG
     &           )
C
C-----------------------------------------------------------------------
C SET UP OUTPUT EFF. EMISS. COEFFT. ARRAY.
C-----------------------------------------------------------------------
C
         DO 4 IT=1,NEOUT
            QEFA(IT) = DEXP( YOUT(IT) )
    4    CONTINUE
C
C-----------------------------------------------------------------------
C
      RETURN
      END
