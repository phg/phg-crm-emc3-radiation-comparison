CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas303/c3out0.for,v 1.2 2004/07/06 11:50:06 whitefor Exp $ Date $Date: 2004/07/06 11:50:06 $
CX
       SUBROUTINE C3OUT0( IWRITE   , LFSEL  ,
     &                    TITLE    , TITLX  , TITLM , DATE   ,
     &                    IBSEL    , IEVAL  ,
     &                    DIVAL    , DEREF  ,
     &                    TIVAL    , TEREF  ,
     &                    ZFVAL    , ZEREF  ,
     &                    BMVAL    , BMREF  ,
     &                    CWAVEL   , CDONOR ,
     &                    CRECVR   , CFILE  ,
     &                    CPCODE   , CINDM  ,
     &                    LERNG    ,
     &                    EAMU     , EATM   , ECMS  ,
     &                    QEFA     ,
     &                    KPLUS1   , COEF
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C3OUT0 *********************
C
C  PURPOSE:  TO  PRINT DATA CONCERNING THE SELECTED TRANSITION DATA-
C            BLOCK UNDER ANALYSIS.
C
C  CALLING PROGRAM: ADAS303
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IWRITE  = OUTPUT UNIT FOR RESULTS
C  INPUT : (L*4)  LFSEL   = .TRUE.  => MINIMAX POLYNOMIAL FITTED.
C                           .FALSE. => MINIMAX POLYNOMIAL NOT FITTED
C
C  INPUT : (C*40) TITLE   = TITLE OF RUN (READ FROM ISPF PANEL)
CX INPUT : (C*120)TITLX   = INFORMATION STRING CONTAINING INPUT DATA-
C                           SET NAME IN BYTES 1->35.
C  INPUT : (C*80) TITLM   = DIAGNOSTIC LINE INFORMATION FROM 'MINIMAX'
C  INPUT : (C*8)  DATE    = CURRENT DATE (AS 'DD/MM/YY')
C
C  INPUT : (R*8)  DIVAL     = SELECTED OUTPUT VALUE: ION DENSITY (CM-3)
C  INPUT : (R*8)  DEREF     = REFERENCE VALUE      : ION DENSITY (CM-3)
C
C  INPUT : (R*8)  TIVAL     = SELECTED OUTPUT VALUE: ION TEMPERATURE(EV)
C  INPUT : (R*8)  TEREF     = REFERENCE VALUE      : ION TEMPERATURE(EV)
C
C  INPUT : (R*8)  ZFVAL     = SELECTED OUTPUT VALUE: Z-EFFECTIVE
C  INPUT : (R*8)  ZEREF     = REFERENCE VALUE      : Z-EFFECTIVE
C
C  INPUT : (R*8)  BMVAL     = SELECTED OUTPUT VALUE: MAGNETIC FIELD (T)
C  INPUT : (R*8)  BMREF     = REFERENCE VALUE      : MAGNETIC FIELD (T)
C
C  INPUT : (I*4)  IBSEL   = DATA-BLOCK INDEX SELECTED FROM INPUT DATASET
C                           FOR ANALYSIS.
C  INPUT : (I*4)  IEVAL   = NUMBER OF USER ENTERED BEAM ENERGY VALUES
C
C  INPUT : (C*5)  CWAVEL  = INPUT DATA FILE: TRANSITION
C  INPUT : (C*8)  CDONOR  = INPUT DATA FILE: DONOR NEUTRAL ATOM
C  INPUT : (C*5)  CRECVR  = INPUT DATA FILE: RECEIVER NUCLEUS
C  INPUT : (C*8)  CFILE   = INPUT DATA FILE: SPECIFIC ION FILE SOURCE
C  INPUT : (C*8)  CPCODE  = INPUT DATA FILE: PROCESSING CODE
C  INPUT : (C*6)  CINDM   = FILE DATA FILE: EMISSION TYPE
C
C  INPUT : (L*4)  LERNG()=  .TRUE.  => OUTPUT 'QEFA()' VALUE WAS INTER-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      BEAM ENERGY 'EAMU()'.
C                           .FALSE. => OUTPUT 'QEFA()' VALUE WAS EXTRA-
C                                      POLATED  FOR  THE  USER  ENTERED
C                                      BEAM ENERGY 'EAMU()'.
C                           DIMENSION: BEAM ENERGY INDEX
C
C  INPUT : (R*8)  EAMU()  = USER ENTERED: BEAM ENERGY (EV/AMU)
C                           DIMENSION: BEAM ENERGY INDEX
C  INPUT : (R*8)  EATM()  = USER ENTERED: BEAM ENERGY (AT. UNITS)
C                           DIMENSION: BEAM ENERGY INDEX
C  INPUT : (R*8)  ECMS()  = USER ENTERED: BEAM ENERGY (CM/SEC)
C                           DIMENSION: BEAM ENERGY INDEX
C
C  INPUT : (R*8)  QEFA()  = SPLINE INTERPOLATED OR  EXTRAPOLATED  EFF.
C                           EMIS. COEFFTS. FOR THE USER ENTERED BEAM
C                           ENERGIES.
C                           DIMENSION: BEAM ENERGY INDEX
C
C  INPUT : (I*4)  KPLUS1  = NUMBER OF MINIMAX COEFFICIENTS
C  INPUT : (R*8)  COEF()  = COEFFICIENTS OF FITTED MINIMAX POLYNOMIAL
C
C          (I*4)  I       = GENERAL USE - ARRAY ELEMENT INDEX
C
C          (C*1)  C1E     = '*' IF RATE-COEFFICIENT WAS EXTRAPOLATED FOR
C                           THE CURRENT BEAM ENERGY. (IT EQUALS
C                           ' ' IF IT WAS INTERPOLATED.)
C          (C*80) CADAS   = ADAS HEADER: INCLUDES RELEASE, PROGRAM, TIME
C          (C*80) CSTRNG  = MARKER TO USED TO FIND TITLE IN TITLX
C          (I*4)  IFIRST  = FIRST POSITION OF MARKER CSTRNG IN TITLX
C          (I*4)  ILAST   = LAST POSITION OF MARKER CSTRNG IN TITLX
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXADAS     ADAS      GATHERS ADAS HEADER INFORMATION
C
C AUTHOR  : H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C           JA8.08
C           TEL. 0141-553-4196
C
C DATE:    23/04/95
C
C UPDATE:  03/05/95 PE BRIDEN - ADDED 1-D SCAN POSITIONS TABLE.
C                             - ADDED BEAM ENERGY COLUMN FOR CM/SEC.
C                               (REQD. UPDATE OF ARGUMENT LIST)
C                             + OTHER MINOR MODS.
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 15-05-95
C MODIFIED: TIM HAMMOND(TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C VERSION: 1.2                          DATE: 13-05-96
C MODIFIED: TIM HAMMOND
C               - REMOVED HOLLERITH CONSTANTS FROM OUTPUT AND TIDIED UP
C                 HEADER
C----------------------------------------------------------------------
      INTEGER    IWRITE        , IBSEL         , IEVAL     ,
     &           IZ0           , IZ            , KPLUS1
      INTEGER    I             , IFIRST        , ILAST
C----------------------------------------------------------------------
      LOGICAL    LFSEL
C----------------------------------------------------------------------
      CHARACTER  TITLE*(*)     , TITLX*(*)     , TITLM*(*) , DATE*8
      CHARACTER  CINDM*6       , CFILE*8       ,
     &           CPCODE*8      , CWAVEL*5      ,
     &           CDONOR*8      , CRECVR*5
      CHARACTER  C1E*1         ,
     &           CADAS*80
      CHARACTER  CSTRNG*80
C----------------------------------------------------------------------
      REAL*8     DIVAL         , DEREF         ,
     &           TIVAL         , TEREF         ,
     &           ZFVAL         , ZEREF         ,
     &           BMVAL         , BMREF
C----------------------------------------------------------------------
      REAL*8     EAMU(IEVAL)   ,
     &           EATM(IEVAL)   ,
     &           ECMS(IEVAL)   ,
     &           QEFA(IEVAL)   ,
     &           COEF(KPLUS1)
C----------------------------------------------------------------------
      LOGICAL    LERNG(IEVAL)
C----------------------------------------------------------------------
      SAVE       CADAS
C----------------------------------------------------------------------
      DATA       CADAS /' '/ CSTRNG/'BLK'/
C----------------------------------------------------------------------
C
C**********************************************************************
C
C---------------------------------------------------------------------
C GATHER ADAS HEADER
C---------------------------------------------------------------------
C
      CALL XXADAS( CADAS )
C
C---------------------------------------------------------------------
C OUTPUT HEADINGS AND ION & SELECTED TRANSITION SPECIFICATIONS.
C---------------------------------------------------------------------
C
      WRITE(IWRITE,1000) CADAS(2:80)
      WRITE(IWRITE,1001)
     &                 'EFF. EMISSION COEFFTS. INTERROGATION',
     &                 'ADAS303' , DATE
      WRITE(IWRITE,1002) TITLE
CX    WRITE(IWRITE,1003) TITLX(1:35) , IBSEL
CX  UNIX PORT - FIND LENGTH OF FILENAME IN TITLX
      CALL XXFCHR(TITLX(1:80), CSTRNG, IFIRST, ILAST)
      WRITE(IWRITE,1003) TITLX(1:IFIRST-1), IBSEL
      WRITE(IWRITE,1004) CWAVEL , CDONOR ,
     &                   CRECVR , CFILE  ,
     &                   CPCODE , CINDM
      WRITE(IWRITE,1005) DIVAL  , DEREF  ,
     &                   TIVAL  , TEREF  ,
     &                   ZFVAL  , ZEREF  ,
     &                   BMVAL  , BMREF
C
C---------------------------------------------------------------------
C OUTPUT BEAM ENERGIES AND EFFECTIVE EMISSION COEFFICIENTS
C---------------------------------------------------------------------
C
      WRITE(IWRITE,1006)
C
         DO 1 I=1,IEVAL
            C1E = '*'
            IF (LERNG(I)) C1E = ' '
            WRITE(IWRITE,1007) C1E     , EAMU(I) , EATM(I) , ECMS(I) ,
     &                         QEFA(I)
    1    CONTINUE
C
      WRITE(IWRITE,1008)
      WRITE(IWRITE,1009)
      WRITE(IWRITE,1008)
C
C---------------------------------------------------------------------
C OUTPUT THE MINIMAX COEFFICIENTS (IF REQUESTED) (ON A NEW PAGE)
C---------------------------------------------------------------------
C
      WRITE(IWRITE,1000) CADAS(2:80)
C
         IF (LFSEL) THEN
C
               WRITE(IWRITE,1010)
C
               WRITE (IWRITE,1008)
C
               DO 2 I=1,KPLUS1,2
                  IF (I.EQ.KPLUS1) THEN
                     WRITE(IWRITE,1011) I , COEF(I)
                  ELSE
                     WRITE(IWRITE,1012) I , COEF(I) , I+1 , COEF(I+1)
                  ENDIF
    2          CONTINUE
C
            WRITE(IWRITE,1013) TITLM
            WRITE(IWRITE,1008)
         ENDIF
C
C---------------------------------------------------------------------
C
 1000 FORMAT(1H ,A79)
 1001 FORMAT(1H ,20('*'),' TABULAR OUTPUT FROM ',A36,' PROGRAM: ',
     & A7,1X,'- DATE: ',A8,1X,20('*')/)
 1002 FORMAT(1H ,19('-'),1X,A40,1X,19('-')/1H ,1X,
     & 'EFFECTIVE EMISSION COEFFICIENTS AS A FUNCTION OF ',
     & 'NEUTRAL BEAM ENERGY '/
     & 2X,77('-')/
     & 21X,'DATA GENERATED USING PROGRAM: ADAS303'/21X,37('-'))
 1003 FORMAT(1H ,A35,' - DATA-BLOCK: ',I2)
 1004 FORMAT(1H ,'EMITTING ION INFORMATION:'/
     &       1H ,25('-')/
     &       1H ,'TRANSITION (N-N'')           =', 2X,A5  /
     &       1H ,'NEUTRAL ATOM DONOR          =', 2X,A8  /
     &       1H ,'BARE NUCLEUS RECEIVER       =', 2X,A5  /
     &       1H ,'QCX SOURCE FILE MEMBER      =', 2X,A8  /
     &       1H ,'PROCESSING CODE             =', 2X,A8  /
     &       1H ,'EMISSION TYPE               =', 2X,A6  /)
 1005 FORMAT(1H ,'ONE DIMENSIONAL SCAN POSITIONS:'/
     &       1H ,31('-')//
     &       1H ,23X,'INTERPOLATED',4X,'REFERENCE'/
     &       1H ,'PARAMETER',17X,'VALUES',9X,'VALUES'//
     &       1H ,'ION DENSITY (cm-3)   =',2X,1P,D10.3,4X,D10.3 /
     &       1H ,'ION TEMPERATURE (eV) =',2X,1P,D10.3,4X,D10.3 /
     &       1H ,'Z-EFFECTIVE          =',2X,1P,D10.3,4X,D10.3 /
     &       1H ,'MAGNETIC FIELD (T)   =',2X,1P,D10.3,4X,D10.3 /)
 1006 FORMAT(1H /1H ,10X,'----------- BEAM ENERGY -----------',
     &               16X,' EFFECTIVE '/
     &           1H ,13X,'EV/AMU',6X,'AT. V0',6X,'CM/SEC',
     &               13X,'EMIS. COEF. (CM3/SEC)')
 1007 FORMAT(1H ,1P,8X,A,1X,D10.3,2X,D10.3,2X,D10.3,17X,D10.3)
 1008 FORMAT(1H ,79('-'))
 1009 FORMAT(1H ,'NOTE: * => EFF. EMISSION COEFFTS. EXTRAPOLATED FOR ',
     &                     'BEAM ENERGY VALUE')
C
 1010 FORMAT (1H /1H ,'MINIMAX POLYNOMIAL FIT TO BEAM ENERGIES',
     &                ' - TAYLOR COEFFICIENTS:'/
     &            1H ,79('-')/
     &            1H ,'- LOG10(EFF. EMISSION COEFFT.) versus ',
     &                'LOG10(BEAM ENERGY<EV/AMU>) -')
 1011 FORMAT (  12X,'A(',I2,') = ',1P,D17.9 )
 1012 FORMAT (2(12X,'A(',I2,') = ',1P,D17.9))
 1013 FORMAT (1H ,A79)
C
C---------------------------------------------------------------------
C
      RETURN
      END
