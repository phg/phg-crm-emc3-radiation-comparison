CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas303/c3setp.for,v 1.1 2004/07/06 11:50:19 whitefor Exp $ Date $Date: 2004/07/06 11:50:19 $
CX
       SUBROUTINE C3SETP( NBSEL )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C3SETP *********************
C
C  PURPOSE:  Writes the value of nbsel out to IDL.                        
C            *** IDENTICAL TO: E1SETP (EXCEPT 'SNCOMB' -> 'SLINES')
C
C  CALLING PROGRAM: ADAS303
C
C  DATA:
C           DATA IS OBTAINED VIA SUBROUTINE 'C3DATA'
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  NBSEL   = NUMBER OF TRANSITIONS (LINES) READ IN.
C                           I.E. NUMBER OF DATA-BLOCKS.
C
C          (I*4)  PIPEOU  = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C
C ROUTINES:
C           ROUTINE     SOURCE      BRIEF DESCRIPTION
C           ------------------------------------------------------------
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/37
C          JET EXT. 2520
C
C DATE:    30/04/91
C
C UPDATE:  04/05/95 Tim Hammond - Unix port
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER    NBSEL     
      INTEGER    PIPEOU    
      PARAMETER( PIPEOU=6 )
C-----------------------------------------------------------------------
C
C **********************************************************************
C
C-----------------------------------------------------------------------
C WRITE NBSEL OUT TO IDL
C-----------------------------------------------------------------------
C

      WRITE( PIPEOU, * ) NBSEL
      CALL XXFLSH( PIPEOU )


      RETURN
      END
