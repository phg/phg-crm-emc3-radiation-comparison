       SUBROUTINE RPHOTO(ATE,EN,RREC)
       IMPLICIT REAL*8(A-H,O-Z)
C-----------------------------------------------------------------------
C  PURPOSE: Calculates <gff>. See Burgess and Summers, MNRAS,
C           vol 226, p257-272 (1987).
C
C-----------------------------------------------------------------------
C
C VERSION  : 1.1
C DATE     : 18-03-1999
C MODIFIED : unknown
C
C VERSION  : 1.2
C DATE     : 04-07-2007
C MODIFIED : Martin O'Mullane
C             - Comments added.
C
C-----------------------------------------------------------------------
       DIMENSION XA(6),WA(6)
       DATA NX/6/
       DATA XA/0.2228466042D0,1.1889321017D0,2.9927363261D0,
     &         5.7751435691D0,9.8374674184D0,15.9828739806D0/
       DATA WA/4.58964673950D-1,4.17000830772D-1,1.13373382074D-1,
     &         1.03991974531D-2,2.61017202815D-4,8.98547906430D-7/
       T1=ATE/(EN*EN)
       SUM=0.0D0
       DO 20 I=1,NX
       X=XA(I)
       U=X/T1
   20  SUM=SUM+GBF(EN,U)*WA(I)
       RREC=SUM
       RETURN
       END
