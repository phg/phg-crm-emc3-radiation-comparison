       subroutine cgdnck( ndimp  ,
     &                    denion , zeff   ,
     &                    nimp   , zimpa  , frimpa ,
     &                    dens   , densp  , denimp
     &                  )
       implicit none
c-----------------------------------------------------------------------
c
c  ********************* fortran 77 subroutine: cgdnmk *****************
c
c  purpose: check consistency of plasma ion and impurity concentration
c           with mean impurity density and zeff and adjust if necessary.
c
c
c  subroutine:
c
c   input : (i*4)  ndimp    = maximum number of plasma impurities
c
c   input : (i*4)  denion   = plasma mean ion density (cm-3)
c   input : (i*4)  zeff     = plasma zeff
c   input : (i*4)  nimp     = number of plasma impurities
c                             (excl. plasma H+ species)
c   input : (r*8)  zimpa()  = impurity species charge
c                             1st dim: index of impurity
c   input : (r*8)  frimpa() = fraction of impurity (normalised to 1)
c                             1st dim: index of impurity
c
c   output: (r*8)  dens     = plasma electron density (cm-3)
c   output: (r*8)  densp    = plasma H+ density (cm-3)
c   output: (r*8)  denimp   = plasma mean impurity ion density (cm-3)
c                             (excl. plasma H+ species)
c
c  routines:
c
c     routine    source    brief description
c     -------------------------------------------------------------
c     i4unit     adas      set output stream for messages
c     xxelem     adas      obtain element name from nuclear charge
c     xxslen     adas      locate first and last char. of string
c
c
c  author   : Hugh Summers
c  date     : 23-05-2007
c
c
c  Version : 1.1
c  Date    : 23-05-2007
c  Modified: Hugh Summers
c            - First version.
c
c-----------------------------------------------------------------------
       real*8    zero
c-----------------------------------------------------------------------
       parameter ( zero = 1.0d-15 )
c-----------------------------------------------------------------------
       integer   i4unit
       integer   ndimp
       integer   nimp    , imp
c-----------------------------------------------------------------------
       real*8    dens    , densp  , denimp , denion ,
     &           zeff    , zsum1  , zsum2
c-----------------------------------------------------------------------
       real*8    zimpa(ndimp)     , frimpa(ndimp)
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c  initialise
c-----------------------------------------------------------------------
c
       if(nimp.eq.0)then
           if(zeff.eq.1.0d0) then
               denimp=0.0d0
               densp = denion
               dens  = densp
           else
               write(i4unit(-1),1001)'zeff',zeff
               write(i4unit(-1),1002)
               stop
           endif
       else
           zsum1=0.0d0
           zsum2=0.0d0
           do imp=1,nimp
             zsum1=zsum1+zimpa(imp)*frimpa(imp)
             zsum2=zsum2+zimpa(imp)**2*frimpa(imp)
           enddo
           if((zeff-(zsum2/zsum1).gt.zero)) then
               write(i4unit(-1),1001)'zeff',zeff
               write(i4unit(-1),1002)
               stop
           elseif(dabs(zeff-zsum2/zsum1).le.zero) then
                 denimp=denion
                 densp=0.0d0
                 dens=zsum1*denion
           else
                 dens=((1.0d0-zsum1)*zsum2+zsum1*(zsum2-1.0d0))*
     &                 denion/(zeff*(1.0d0-zsum1)+(zsum2-1.0d0))
                 densp=(zsum2-zeff*zsum1)*
     &                 denion/(zeff*(1.0d0-zsum1)+(zsum2-1.0d0))
                 denimp=denion-densp
           endif
       endif


       return
c
c----------------------------------------------------------------------
c
 1001 format(1x,30('*'),'  cgdnck error  ',30('*')//
     &       1x,'inconsistent parameter: ',a,' = ',f5.2)
 1002 format(/1x,29('*'),' program terminated ',29('*'))
c
c-------------------------------------------------------------------------
c

      end
