       subroutine cgwr26 ( iunit  , dsname , user  , date   ,
     &                     ndrep  , ndimp  ,
     &                     iz0    , iz1    ,
     &                     dens   , densp  , denimp , denion,
     &                     te     , tp     , timp   , tion  ,
     &                     zeff   ,
     &                     bmener , densh  ,
     &                     nimp   , zimpa  , frimpa,
     &                     ts     , w      , w1     ,
     &                     cion   , cpy    , nip    , intd  , iprs   ,
     &                     ilow   , ionip  , nionip , ilprs , ivdisp ,
     &                     imax   , nrep   ,
     &                     f1     , f2     , f3     ,
     &                     bncalc , bnact  , expsh  ,
     &                     alpha  , hiss   , xpop   ,
     &                     plt    , prb    , prc
     &                   )
       implicit none
c-----------------------------------------------------------------------
c
c  ********************* fortran 77 subroutine: cgwr26 *****************
c
c  purpose: Write output from adas316 for a selected case as an adf26
c           table.
c
c
c  subroutine:
c
c   input : (i*4)  iunit    = unit number for input adf01 file.
c   input : (c*80) dsname   = file name of adf25 format to be read.
c   input : (c*30) user     = name of producer.
c   input : (c*8)  date     = date of run.
c
c   input : (i*4)  ndrep    = maximum number of representative n-shells
c   input : (i*4)  ndimp    = maximum number of plasma impurities
c
c   input : (i*4)  iz0      = nuclear charge of bundle-n ion
c   input : (i*4)  iz1      = recombining ion charge of bundle-n ion
c   input : (i*4)  dens     = plasma electron density (cm-3)
c   input : (i*4)  densp    = plasma H+ density (cm-3)
c   input : (i*4)  denimp   = plasma mean impurity ion density (cm-3)
c   input : (i*4)  denion   = plasma mean ion density (cm-3)
c   input : (i*4)  te       = plasma electron temp. (K)
c   input : (i*4)  tp       = plasma H+ temp. (K)
c   input : (i*4)  timp     = plasma mean impurity ion temp (K)
c   input : (i*4)  zeff     = plasma zeff
c   input : (i*4)  bmener   = beam energy (ev/amu)
c   input : (i*4)  densh    = beam H+ density (cm-3)
c
c   input : (i*4)  nimp     = number of plasma impurities (excl.h+)
c   input : (r*8)  zimpa()  = impurity species charge
c                             1st dim: index of impurity
c   input : (r*8)  frimpa() = fraction of impurity (normalised to 1)
c                             1st dim: index of impurity
c
c   input : (r*8)  ts       = external radiation field temperature (K)
c   input : (r*8)  w        = general radiation dilution factor
c   input : (i*4)  w1       = external radiation field dilution factor
c                             for photo-ionisation form the ground level.
c
c   input : (r*8)  cion     = adjustment multiplier for ground ionis.
c   input : (r*8)  cpy      = adjustment multiplier for VR xsects.
c   input : (i*4)  nip      = range of delta n for IP xsects. (le.4)
c   input : (i*4)  intd     = order of Maxw. quad. for IP xsects.(le.3)
c   input : (i*4)  iprs     = 0  => default to VR xsects. beyond nip range
c                             1  => use PR xsects. beyond nip range
c   input : (i*4)  ilow     = 0  => no special low level data accessed
c                             1  => special low level data accessed
c   input : (i*4)  ionip    = 0 =>   no ion impact collisions included
c                             1 =>ion impact excit. and ionis. included
c   input : (i*4)  nionip   = range of delta n for ion impact
c                             excitation xsects.
c   input : (i*4)  ilprs    = 0 => default to vainshtein xsects.
c                             1 => use lodge-percival-richards xsects.
c   input : (i*4)  ivdisp   = 0 => ion impact at thermal Maxw. energies
c                             1 => ion impact at displaced thermal
c                                  energies according to the neutral
c                                  beam energy parameter
c                               * if(ivdisp=0 then special low level
c                                 data for ion impact is not substituted -
c                                 only vainshtein and lodge et al.
c                                 options are open.  Electron impact
c                                 data substitution does occur.
c   input : (i*4)  imax     = number of representative n-shells
c   input : (i*4)  nrep()   = representative n-shells
c                             1st dim: index of representative n-shell
c
c   input : (r*8)  f1()     = excitation part of b-factor
c                             1st dim: index of representative n-shell
c   input : (r*8)  f2()     = free electron recom. part of b-factor
c                             1st dim: index of representative n-shell
c   input : (r*8)  f3()     = cx recom. part of b-factor
c                             1st dim: index of representative n-shell
c   input : (r*8)  bncalc() = recomputed b-factor from f's
c                             1st dim: index of representative n-shell
c   input : (r*8)  bnact()  = actual b-factor from loop 4
c                             1st dim: index of representative n-shell
c   input : (r*8)  expsh()  = Saha exponential factor = Nn/(NeN+bn)
c                             1st dim: index of representative n-shell
c   input : (r*8)  alpha()  = coll.- rad. recom. coefft.
c                             (1 = cx. off; 2 = cx. on)
c   input : (r*8)  hiss()   = coll.- rad. ionis. coefft.
c                             (1 = cx. off; 2 = cx. on)
c   input : (r*8)  xpop()   = ground population N1/(NeN+)
c                             (1 = cx. off; 2 = cx. on)
c   input : (r*8)  plt      = low level line power coeff. (W cm3)
c   input : (r*8)  prb      = recom./brems. power coeff.(W cm3)
c   input : (r*8)  prc      = cx. recom. power coeff. (W cm3)
c
c  routines:
c
c     routine    source    brief description
c     -------------------------------------------------------------
c     xxelem     adas      obtain element name from nuclear charge
c
c
c  author   : Hugh Summers
c  date     : 15-06-2007
c
c  Version : 1.1
c  Date    : 15-06-2007
c  Modified: Hugh Summers
c            - First version.
c
c  Version : 1.2
c  Date    : 15-07-2009
c  Modified: Martin O'Mullane
c            - Increase dsname to 120 characters.
c
c-----------------------------------------------------------------------
       integer   iunit
       integer   ndrep   , ndimp
       integer   iz0     , iz1    ,
     &           nimp    , imp    ,
     &           nip     , intd   , iprs   , ilow   , ionip  , ilprs  ,
     &           ivdisp  , imax   , nionip
       integer   i       , l1     , l2
c-----------------------------------------------------------------------
       real*8    ts      , w      , w1     , cion   , cpy
       real*8    dens    , densp  , denimp , denion ,
     &           te      , tp     , timp   , tion   ,
     &           bmener  , densh  ,
     &           zeff    , xdens
       real*8    flux
       real*8    plt     , prb    , prc
c-----------------------------------------------------------------------
       character dsname*120 , elname*12
       character date*8     , user*30
c-----------------------------------------------------------------------
       integer   nrep(ndrep+1)
c-----------------------------------------------------------------------
       real*8    zimpa(ndimp)     , frimpa(ndimp)
       real*8    f1(ndrep)        , f2(ndrep)       , f3(ndrep)       ,
     &           bncalc(ndrep)    , bnact(ndrep)    , expsh(ndrep)
       real*8    xpop(2)
       real*8    alpha(2)         , hiss(2)
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c  initialise
c-----------------------------------------------------------------------

       call xxelem(iz0,elname)

       write(iunit,2045)
       write(iunit,2045)
       write(iunit,2045)
       write(iunit,2050)
       write(iunit,2045)
       write(iunit,2045)
       write(iunit,3002)elname,iz0,iz1
       write(iunit,2045)
       write(iunit,2061)ts,te,tp,timp,tion
       write(iunit,2062)w,dens,densp,denimp,denion
       write(iunit,2045)
       xdens=densh/dens
       flux=1.38317498d6*dsqrt(bmener)*densh
       write(iunit,2063)bmener,densh,xdens,flux
       write(iunit,2045)
       write(iunit,2045)
       write(iunit,2071)xpop(2),alpha(2),hiss(2)
       write(iunit,2045)
       write(iunit,2072)xpop(1),alpha(1),hiss(1)
       write(iunit,2045)
       write(iunit,2076)plt/(1.0d7*xpop(2)),prb/1.0d7,prc/1.0d7
       write(iunit,2045)
       write(iunit,2066)

       do i=1,imax
         write(iunit,2068)i,nrep(i),f1(i),f2(i),f3(i),
     &                    bncalc(i),bnact(i),expsh(i)
       enddo

       write(iunit,2045)
       write(iunit,2045)
       write(iunit,2045)
       write(iunit,2075)
       write(iunit,2045)
       write(iunit,2045)
       write(iunit,3003)nip,intd,iprs,ilow,ionip,nionip,ilprs,ivdisp,
     &                  zeff,ts,w,cion,cpy,w1,zimpa(1),frimpa(1)

       if(nimp.gt.1)then
           do imp=2,nimp
             write(iunit,3004)zimpa(imp),frimpa(imp)
           enddo
       endif

C Attribution at end

       call xxslen(dsname, l1, l2)
       write(iunit,2045)
       write(iunit,4000)
       write(iunit,4005)dsname(l1:l2)
       write(iunit,4010)'adas316',user,date
       write(iunit,4000)

       return
c
c-----------------------------------------------------------------------
c
 2045  format('    ')
 2050  format(' effective contribution table for ion principal quantum s
     &hell populations in thermal plasma')
 2061  format('    trad = ',1pe8.2,' k        te = ',1pe8.2,' k        t
     &p = ',1pe8.2,' k        timp = ',1pe8.2,' k        tion = ',
     &1pe8.2,' k')
 2062  format('       w = ',1pe8.2,'          ne = ',1pe8.2,' cm-3     n
     &p = ',1pe8.2,' cm-3   denimp = ',1pe8.2,' cm-3   denion = ',
     &1pe8.2,' cm-3')
 2063  format('      eh = ',1pe8.2,' ev/amu   nh = ',1pe8.2,' cm-3
     &  nh/ne = ',1pe8.2,'   flux = ',1pe8.2,' cm-2 sec-1')
 2066  format(4x,'i',4x,'n',15x,'f1',13x,'f2',13x,'f3',10x
     &,'b(check)',7x,'b(actual)',6x,'nn/(bn*n+)')
 2068  format(2i5,7x,1p,7e15.5)
 2071  format(' charge exchange off : n1/n+ = ',1pe11.5,'  recomb coeff
     &= ',1pe11.5,' cm+3 sec-1   ioniz coeff = ',1pe11.5,' cm+3 sec-1')
 2072  format(' charge exchange on  : n1/n+ = ',1pe11.5,'  recomb coeff
     &= ',1pe11.5,' cm+3 sec-1   ioniz coeff = ',1pe11.5,' cm+3 sec-1')
 2075  format(47x,'bn = f1*(n1/n+) + f2 + f3*(nh/ne)'/
     &47x,'n1 = population of ground state of ion'/
     &47x,'n+ = population of ground state of next ionisation stage'/
     &47x,'nn = population of principal quantum shell n of ion'/
     &47x,'bn = saha-boltzmann factor for principal quantum shell n '/
     &47x,'eh = neutral hydrogen beam energy'/
     &47x,' w = radiation dilution factor'/
     &47x,'z0 = nuclear charge'/
     &47x,'z1 = ion charge+1')
 2076  format('                         plt = ',1pe11.5,' w cm+3
     & prb = ',1pe11.5,' w cm+3          prc = ',1pe11.5,' w cm+3    ')
 3002  format(3x,a12,12x,1x,' z0 = ',i2,21x,' z1 = ',i2)
 3003  format(1x, 'nip   =',i3,5x,'intd  =',i3,5x,'iprs  =',i3,5x,
     &            'ilow  =',i3,5x,
     &            'ionip =',i3,5x,'nionip=',i3,5x,'ilprs =',i3,5x,
     &            'ivdisp=',i3/
     &        1x, 'zeff  =',0p,f4.1,4x,'ts  =',1pd10.2,5x,
     &            'w   =',1pd10.2,5x,
     &            'cion  =',0p,f3.1,5x,'cpy =',0p,f3.1,5x,
     &            'w1  =',1pd10.2,5x,'zimp  =',0p,f4.1,1x,'(',1pd10.2,
     &            ')')
 3004  format(1x, 110x,f4.1,1x,'(',1pd10.2,')')

 4000  FORMAT('C', 80('-'))
 4005  FORMAT('C '/,
     &        'C  adf25 driver : ',A)
 4010  FORMAT('C',/,
     &        'C  Code     : ',1A7,/,
     &        'C  Producer : ',A30,/,
     &        'C  Date     : ',1A8,/,
     &        'C')
c
c-------------------------------------------------------------------------
c

      end
