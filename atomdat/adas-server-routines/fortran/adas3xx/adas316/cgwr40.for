       subroutine cgwr40( iunit  , dsname  ,user   , date  ,
     &                    ndtem  , ndden  , ndrep  ,
     &                    ndimp  , ndlev  , ndein , ndzef  ,
     &                    ndwvl  , ndpix  ,
     &                    iz0    , iz1    ,
     &                    id_ref , densa  ,
     &                    deniona,
     &                    it_ref , tiona  ,
     &                    ib_ref , bmena  ,
     &                    nimp   , zimpa  , amimpa, frimpa ,
     &                    ts     , w      , w1     ,
     &                    cion   , cpy    , nip    , intd  , iprs   ,
     &                    ilow   , ionip  , nionip , ilprs , ivdisp ,
     &                    nmin   , nmax   , imax   , nrep  ,
     &                    p3a_ib ,
     &                    nwvl   , wvmin  , wvmax  , npix  , emis_thres
     &                  )
       implicit none
c-----------------------------------------------------------------------
c
c  ***************** fortran77 subroutine: cgwr40 **********************
c
c  purpose:  To write an adf40 dataset from the reference conditions
c            of the bundle-n model.
c
c  Notes:
c
c  Subroutine:
c
c   input : (i*4)  iunit    = unit number for output adf40 file.
c   input : (c*80) dsname   = file name of adf25 format read.
c   input : (c*30) user     = name of producer.
c   input : (c*8)  date     = date of run.
c
c   input : (i*4)  ndtem    = maximum number of electron temperatures
c   input : (i*4)  ndden    = maximum number of electron densities
c   input : (i*4)  ndrep    = maximum number of representative n-shells
c   input : (i*4)  ndimp    = maximum number of plasma impurities
c   input : (i*4)  ndlev    = maximum number of n-shells
c   input : (i*4)  ndein    = maximum number of beam energies
c   input : (i*4)  ndzef    = maximum number of z effectives
c   input : (i*4)  ndwvl   = maximum number of wavelength intervals
c   input : (i*4)  ndpix   = maximum no. of pixels in a wvl. interval
c
c   input : (i*4)  iz0      = nuclear charge of bundle-n ion
c   input : (i*4)  iz1      = recombining ion charge of bundle-n ion
c   input : (i*4)  id_ref   = reference electron density pointer in vectors
c   input : (i*4)  densa()  = plasma electron density vector (cm-3)
c                             1st dim: index of electron density
c   input : (i*4)  deniona()= total ion density (plasma+impurity) (cm-3)
c                             1st dim: index of electron density
c   input : (i*4)  it_ref   = reference electron temp. pointer in vectors
c   input : (i*4)  tiona()  = mean ion temp (plasma+impurity) (K)
c                             1st dim: index of electron temperature
c   input : (i*4)  ib_ref   = reference beam energy pointer in vectors
c   input : (i*4)  bmena()  = beam energy vector (ev/amu)
c                             1st dim: index of beam energies
c
c   input : (i*4)  nimp     = number of plasma impurities (excl.h+)
c   input : (r*8)  zimpa()  = impurity species charge
c                             1st dim: index of impurity
c   input : (r*8)  amimpa() = atomic mass number of impurity species
c                             1st dim: index of impurity
c   input : (r*8)  frimpa() = fraction of impurity (normalised to 1)
c                             1st dim: index of impurity
c
c   input : (r*8)  ts       = external radiation field temperature (K)
c   input : (r*8)  w        = general radiation dilution factor
c   input : (i*4)  w1       = external radiation field dilution factor
c                             for photo-ionisation form the ground level.
c
c   input : (r*8)  cion     = adjustment multiplier for ground ionis.
c   input : (r*8)  cpy      = adjustment multiplier for VR xsects.
c   input : (i*4)  nip      = range of delta n for IP xsects. (le.4)
c   input : (i*4)  intd     = order of Maxw. quad. for IP xsects.(le.3)
c   input : (i*4)  iprs     = 0  => default to VR xsects. beyond nip range
c                             1  => use PR xsects. beyond nip range
c   input : (i*4)  ilow     = 0  => no special low level data accessed
c                             1  => special low level data accessed
c   input : (i*4)  ionip    = 0 =>   no ion impact collisions included
c                             1 =>ion impact excit. and ionis. included
c   input : (i*4)  nionip   = range of delta n for ion impact
c                             excitation xsects.
c   input : (i*4)  ilprs    = 0 => default to vainshtein xsects.
c                             1 => use lodge-percival-richards xsects.
c   input : (i*4)  ivdisp   = 0 => ion impact at thermal Maxw. energies
c                             1 => ion impact at displaced thermal
c                                  energies according to the neutral
c                                  beam energy parameter
c                               * if(ivdisp=0 then special low level
c                                 data for ion impact is not substituted -
c                                 only vainshtein and lodge et al.
c                                 options are open.  Electron impact
c                                 data substitution does occur.
c   input : (i*4)  nmin     = lowest n-shell for population structure
c   input : (i*4)  nmax     = highest n-shell for population structure
c   input : (i*4)  imax     = number of representative n-shells
c   input : (i*4)  nrep()   = representative n-shells
c                             1st dim: index of representative n-shell
c
c   input : (r*8)  p3a_ib(,)= cx part of population
c                             1st dim: index of representative level
c                             2nd dim: index of beam energy scan
c   input : (i*4)  nwvl     = number of wavln. ranges for line inclusion
c   input : (r*8)  wvmin()  = lower bound of wavelength range (A)
c                              1st dim: index of wavelength range
c   input : (r*8)  wvmax()  = upper bound of wavelength range (A)
c                              1st dim: index of wavelength range
c   input : (i*4)  npix()   = number of pixels assigned to range
c                              1st dim: index of wavelength range
c   input : (r*8)  emis_thres=threshold emissivity for line inclusion
c
c  Routines:
c
c     routine    source    brief description
c     -------------------------------------------------------------
c     xxcase     adas      convert string to upper or lower case
c     xxslen     adas      locate first and last char. of string
c     xxordr     adas      rank vector with forward/reverse indices
c     xxeiam     adas      obtain elemnt atomic mass number
c     xfesym     adas      obtain element symbol
c     xxrams     adas      set rydberg constant in atomic constants
c     r8const    adas      obtain atomic constants
c     xxrmve     adas      remove selected characters from a string
c     xxmkrp     adas      remove selected characters from a string
c     cxvcwr     adas      format vector output lines with title
c     xxpixv     adas      doppler broaden line over pixel range
c     r8erfc     adas      returns erfc(x) function value
c
c
c
c  author   : Hugh Summers
c  date     : 15-06-2007
c
c  Version : 1.1
c  Date    : 15-06-2007
c  Modified: Hugh Summers
c            - First version.
c
c  Version : 1.2
c  Date    : 04-10-2007
c  Modified: Martin O'Mullane
c            - Output temperatures in eV to adf40 dataset.
c
c  Version : 1.3
c  Date    : 17-11-2007
c  Modified: Martin O'Mullane
c            - Correct calculation of transition probability.
c
c  Version : 1.4
c  Date    : 15-07-2009
c  Modified: Martin O'Mullane
c            - Increase dsname to 120 characters.
c
c  Version : 1.5
c  Date    : 14-05-2013
c  Modified: Martin O'Mullane
c            - Write reference beam energy to comments.
c            - Additional argument required - bmena.
c            - Tidy comments arrangement.
c
c-----------------------------------------------------------------------
       integer   idlev
       integer   idtem  , idden    , idein  , idzef    , idwvl   , idpix
       integer   ndcnct , ndstack
c-----------------------------------------------------------------------
       real*8    c1                , c2                ,
     &           c3                , c4                ,
     &           c5
       real*8    ev
c-----------------------------------------------------------------------
       parameter (idlev = 550 )
       parameter (idtem  = 35 , idden  = 35 , idein   = 30 , idzef = 10)
       parameter (idwvl = 5   , idpix  = 1024)
       parameter (ndcnct = 100, ndstack = 40  )
       parameter (c1 = 6432.8d0     , c2 = 2949810.0d0  ,
     &            c3 = 25540.0d0    , c4 = 146.0d0      ,
     &            c5 = 41.0d0       )
       parameter (ev = 11605.4D0    )
c-----------------------------------------------------------------------
       integer    i4unit  , iunit
       integer    ndtem   , ndden  , ndrep  ,
     &            ndimp   , ndlev  , ndein  , ndzef  ,
     &            ndwvl   , ndpix
       integer    iz0     , iz1    , id_ref , it_ref ,
     &            ib_ref  , nimp   ,
     &            nip     , intd   , iprs   , ilow   , ionip  , ilprs  ,
     &            ivdisp  , nmin   , nmax   , imax   , nionip
       integer    imp
       integer    i       , i1     , i2     , i3     , icount ,
     &            n       , n1     , nmax1  , iw     , ipix   ,
     &            ind1    , ind2   , iwvrg  , ii
       integer    nwvl    , isel_f
       integer    ncnct   , iptnl  , ncptn_stack
       integer    l1      , l2
c-----------------------------------------------------------------------
       real*8     r8const , finter
       real*8     ts      , w      , w1     , cion   , cpy
       real*8     z1
       real*8     rz      , en     , en1    , delta  , sig2   , rf     ,
     &            wavair  , d      , g      , gbb    , x      , x1     ,
     &            x2      , x3     , a1     , p3_ref , emis
       real*8     xsym
       real*8     emis_thres       ,tion    , cpixmx
c-----------------------------------------------------------------------
       character  xfesym*2
       character  dsname*120
       character  amsr*7     , esym*2
       character  c80*80
       character  cdash*80   , cblanks*1
       character  crcvr*5    , chr5*5
       character  film40_exc*29  , film40_rec*29 , film40_cxs*29
       character  user*30    , date*8
c-----------------------------------------------------------------------
       logical    lwv_incl
c-----------------------------------------------------------------------
       integer    nrep(ndrep+1)
       integer    inter(idlev)
       integer    npix(ndwvl)
       integer    icnctv(ndcnct)
c-----------------------------------------------------------------------
       real*8     densa(ndden)     ,
     &            deniona(ndden)   ,
     &            tiona(ndtem)     , bmena(ndein)    ,
     &            zimpa(ndimp)     , amimpa(ndimp)   ,
     &            frimpa(ndimp)
       real*8     p3a_ib(ndrep,ndein)
       real*8     flag(idlev)      , agrl(idlev,3)
       real*8     wvmin(ndwvl)     , wvmax(ndwvl)
       real*8     cpixa(idpix)     , cpixmxa(idwvl)
       real*8     f_cpixa(idpix,idwvl)
c-----------------------------------------------------------------------
       character  type(3)*5
       character  cptn_stack(ndstack)*80
c-----------------------------------------------------------------------
       data       type/'excit','recom','chexc'/
       data       film40_exc/'pl=0*:ss=**:pb=**:lz=**:hz=**'/
       data       film40_rec/'pl=0*:ss=**:pp=**:lz=**:hz=**'/
       data       film40_cxs/'pl=01:ss=**:pp=**:lz=**:hz=**'/
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c  initialise
c-----------------------------------------------------------------------
         if(idtem.ne.ndtem) then
            write(i4unit(-1),1003) 'idtem',idtem,'ndtem',ndtem
            write(i4unit(-1),1002)
            stop
        endif
         if(idden.ne.ndden) then
            write(i4unit(-1),1003) 'idden',idden,'ndden',ndden
            write(i4unit(-1),1002)
            stop
        endif
         if(idein.ne.ndein) then
            write(i4unit(-1),1003) 'idein',idein,'ndein',ndein
            write(i4unit(-1),1002)
            stop
        endif
         if(idzef.ne.ndzef) then
            write(i4unit(-1),1003) 'idzef',idzef,'ndzef',ndzef
            write(i4unit(-1),1002)
            stop
        endif
        if(idlev.ne.ndlev) then
            write(i4unit(-1),1003) 'idlev',idlev,'ndlev',ndlev
            write(i4unit(-1),1002)
            stop
        endif
        if(idpix.ne.ndpix) then
            write(i4unit(-1),1003) 'idpix',idpix,'ndpix',ndpix
            write(i4unit(-1),1002)
            stop
        endif
        if(idwvl.ne.ndwvl) then
            write(i4unit(-1),1003) 'idwvl',idwvl,'ndwvl',ndwvl
            write(i4unit(-1),1002)
            stop
        endif

       z1 = iz1

       iptnl = 1

       tion = tiona(it_ref)/ev

       esym = xfesym(iz0)
       call xxeiam(esym,xsym)
       write(amsr,'(f7.1)')xsym
       write(chr5,'(1a2,1a1,i2)')esym,'+',iz1
       call xxrmve(chr5,crcvr,' ')

c-----------------------------------------------------------------------
c  get correct rydberg constant for the cx receiver
c-----------------------------------------------------------------------
       call xxrams(amsr)
       rz = r8const('RYR')

c-----------------------------------------------------------------------
c  set up interpolation between representative n-shells
c-----------------------------------------------------------------------
       nmax1=nmax+nip

       do n=nmin,nmax1
         en=n
         flag(n)=dlog(finter(en))
       enddo

       nrep(imax+1)=nmax+nmax
       i=3
       n1=nmin+2

       do n=n1,nmax
         if((n+n-nrep(i)-nrep(i+1)).gt.0) then
             i=i+1
         endif
         inter(n)=i
       enddo

       inter(nmin)=2
       inter(nmin+1)=2

       do n=nmin,nmax
         x=flag(n)
         i=inter(n)
         i1=nrep(i-1)
         i2=nrep(i)
         x1=flag(i1)
         x2=flag(i2)

         if(imax.gt.i)then
             i3=nrep(i+1)
             x3=flag(i3)
             agrl(n,1)=(x-x2)*(x-x3)/((x1-x2)*(x1-x3))
             agrl(n,2)=(x-x1)*(x-x3)/((x2-x1)*(x2-x3))
             agrl(n,3)=(x-x1)*(x-x2)/((x3-x1)*(x3-x2))
         else
             agrl(n,1)=(x-x2)/(x1-x2)
             agrl(n,2)=(x-x1)/(x2-x1)
             agrl(n,3)=0.0d0
         endif


       enddo
c-----------------------------------------------------------------------
c zero the feature pixel count accumulation vectors
c-----------------------------------------------------------------------

        do ipix = 1,idpix
          cpixa(ipix) = 0.0D0
        enddo

        do iw = 1,nwvl
             cpixmxa(iw) = 0.0D0
          do ipix=1,idpix
             f_cpixa(ipix,iw) = 0.0D0
          enddo
        enddo


c-----------------------------------------------------------------------
c  evaluate transition wavelengths and check for inclusion in count
c-----------------------------------------------------------------------
       icount = 0

       do n=nmin+1,nmax
c       do n=nmin+1,nmin+10
         i=inter(n)

         if(i.eq.2) then
             p3_ref = dexp(agrl(n,2)*dlog(p3a_ib(i,ib_ref)) +
     &                agrl(n,3)*dlog(p3a_ib(i+1,ib_ref)))
         elseif(imax.gt.i) then
             p3_ref = dexp(agrl(n,1)*dlog(p3a_ib(i-1,ib_ref)) +
     &                agrl(n,2)*dlog(p3a_ib(i,ib_ref))   +
     &                agrl(n,3)*dlog(p3a_ib(i+1,ib_ref)))
         else
             p3_ref = dexp(agrl(n,1)*dlog(p3a_ib(i-1,ib_ref)) +
     &                agrl(n,2)*dlog(p3a_ib(i,ib_ref)))
         endif

         en = n
         do n1=nmin,n-1
           en1   = n1
           d     = (1.0d0/(en1*en1)-1.0d0/(en*en))
           delta = rz*z1*z1*d
           sig2  = delta**2 * 1.0d-8
           rf    = 1.0d0 + 1.0d-8 * ( c1 + ( c2 / ( c4 - sig2 ) )
     &             + ( c3 / ( c5 - sig2 ) ) )
           wavair= 1.0d8 / ( delta * rf )

           lwv_incl = .false.
           do iw = 1, nwvl
             if((wavair.ge.wvmin(iw)).and.(wavair.le.wvmax(iw)))then
                 lwv_incl = .true.
             endif
           enddo

           if(lwv_incl) then
               d=1.0D0/d
               x2=1.0d0/(en*en)
               x=en1**(-0.6666667d0)*(en-en1)**(-0.6666667d0)/
     &           (en-1.0d0)**(-0.6666667d0)
               g = gbb(en,en1,x2,x)
               a1=1.5746d10*z1**4*d*g/(en**5*en1**3)
               emis = a1*p3_ref/densa(id_ref)

               if(emis.ge.emis_thres) then
                   icount=icount+1

                   do iw=1,nwvl
                     iwvrg = iw
                     if((wavair.ge.wvmin(iw)).and.
     &                  (wavair.le.wvmax(iw)))then
                        cpixmx = cpixmxa(iw)
                        call xxpixv( idwvl  , idpix  , emis_thres  ,
     &                               nwvl   , npix   , wvmin  , wvmax  ,
     &                               iwvrg  , cpixmx ,
     &                               wavair , tion   , xsym   , emis   ,
     &                               cpixa  , ind1   , ind2
     &                             )

                        do ipix=ind1,ind2
                          f_cpixa(ipix,iw) = f_cpixa(ipix,iw)+
     &                                          cpixa(ipix)
                          cpixmxa(iw) = dmax1(cpixmxa(iw),
     &                                            f_cpixa(ipix,iw))
                        enddo
                        do ipix = ind1,ind2
                          cpixa(ipix)=0.0D0
                        enddo
                     endif
                   enddo
                endif
           endif
         enddo
       enddo

c-----------------------------------------------------------------------
c  write adf40 dataset
c-----------------------------------------------------------------------
       write(iunit,2002) nwvl, esym , iz1-1

c-----output the root connection vector text lines

           call  xxmkrc( ndcnct   ,
     &                   iz0      , iptnl    ,
     &                   ncnct    , icnctv
     &                 )
      cdash='C---------------------------------------'//
     &      '----------------------------------------'

           write(iunit,'(1a80)')'-'//cdash(2:80)
           write(iunit,'(14i5)')(icnctv(ii),ii=1,ncnct)


c-----output the root partition level text lines

           call xxmkrp( ndstack       ,
     &                  iz0           , iptnl       ,
     &                  ncptn_stack   , cptn_stack
     &                )

           write(iunit,'(1a80)')'-'//cdash(2:80)
           do ii=1,ncptn_stack
             write(iunit,'(1a80)') cptn_stack(ii)
           enddo
           write(iunit,'(1a80)')'-'//cdash(2:80)
c----------------------------------------------
c  write each block header and reference values
c----------------------------------------------
       isel_f = 0
       do iw=1,nwvl
         isel_f = isel_f+1
         write(film40_cxs(16:17),'(i2)')1
         write(film40_cxs(16:17),'(i2)')1
         write(film40_cxs(10:11),'(i2)')iz1-1
         write(film40_cxs(22:23),'(i2)')iz1-1
         write(film40_cxs(28:29),'(i2)')iz1-1

         write(iunit,2000) npix(iw) , 1 , 1 ,
     &          film40_cxs , type(3) , 'ispb',1 , isel_f
         write(iunit,'(2f12.5)') wvmin(iw), wvmax(iw)
         write(iunit,'(1p,8e9.2)') deniona(id_ref)
         write(iunit,'(1p,8e9.2)') tiona(it_ref)/ev
         write(iunit,'(1p,8e9.2)')(f_cpixa(ipix,iw),ipix=1,npix(iw))
       enddo

c-----------------------------------------------------------------------
c  write comments to adf12 dataset
c-----------------------------------------------------------------------

      cblanks='C'
      c80 = ' '
      c80(1:21)='C  Wavelength ranges:'
      write(iunit,'(1a80)')cdash
      write(iunit,'(1a80)')c80
      write(iunit,'(a)')cblanks
      c80 = ' '
      c80(1:27)=
     &        'C     wvmin(A)    wvmax(A)'
      write(iunit,'(1a80)')c80
      c80 = ' '
      c80(1:27)=
     &        'C     --------    --------'
      write(iunit,'(1a80)')c80
      do iw=1,nwvl
        write(iunit,'(a,f8.2,4x,f8.2)')'C    ',wvmin(iw),wvmax(iw)
      enddo
      write(iunit,'(a)')cblanks
      write(iunit,'(a,1pd10.2)')'C     emis_thres  = ', emis_thres
      write(iunit,'(a,1pd10.2, 7H eV/amu)')
     &            'C     beam energy = ', bmena(ib_ref)
      write(iunit,'(a)')cblanks

      write(iunit,'(a)')cblanks

      c80 = ' '
      c80(1:39)='C  Bundle-n population code parameters:'
      write(iunit,'(1a80)')c80
      write(iunit,'(a)')cblanks
      write(iunit,1004)nip   , intd   , iprs  , ilow
      write(iunit,1005)ionip , nionip , ilprs , ivdisp
      write(iunit,1006)cion,cpy
      write(iunit,'(a)')cblanks
      write(iunit,1007)ts,w,w1
      write(iunit,1008)zimpa(1),amimpa(1),frimpa(1)

      if(nimp.gt.1)then
          do imp=2,nimp
            write(iunit,1009)imp,zimpa(imp),imp,amimpa(imp),
     &                       imp,frimpa(imp)
          enddo
      endif

      write(iunit,'(a)')cblanks
      write(iunit,'(a)')cblanks

      call xxslen(dsname, l1, l2)
      write(iunit,'(1a16,a)')'C  adf25 file : ',dsname(l1:l2)
      write(iunit,'(1a16,a)')'C  Code       : ','adas316'
      write(iunit,'(1a16,a)')'C  Producer   : ',user
      write(iunit,'(1a16,a)')'C  Date       : ',date
      write(iunit,'(a)')cblanks
      write(iunit,'(1a80)')cdash

       return
c-----------------------------------------------------------------------
 1001 format(1x,30('*'),'  cgwr40 error  ',30('*')//
     &       1x,'for loop operation parameter: ',a,' must be non-zero')
 1002 format(/1x,29('*'),' program terminated ',29('*'))
 1003 format(1x,30('*'),'  cgwr40 error  ',30('*')//
     &       1x,'internal param. ',1a5,' = ',i4,' .ne. ',1a5,' = ',i4)
 1004  format('C    ', 'nip   =',i3,5x,'intd  =',i3,5x,'iprs  =',i3,5x,
     &                 'ilow  =',i3)
 1005  format('C    ', 'ionip =',i3,5x,'nionip=',i3,5x,'ilprs =',i3,5x,
     &                 'ivdisp=',i3)

 1006  format('C    ', 'cion  =',0p,f4.1,4x,'cpy   =',0p,f4.1)
 1007  format('C    ', 'ts(K)   =',1pd10.2,5x,'w       =',1pd10.2,5x,
     &                 'w1      =',1pd10.2)
 1008  format('C    ', 'zimp(1) =',1pd10.2,5x,'amimp(1)=',1pd10.2,5x,
     &                 'frimp(1)=',1pd10.2)
 1009  format('C    ', 'zimp(',i1,') =',1pd10.2,5x,
     &                 'amimp(',i1,')=',1pd10.2,5x,
     &                 'frimp(',i1,')=',1pd10.2)
 2000  format(2x,i4,'  ',2i4,' /',1a29,'/type=',1a5,
     &       '/',1a4,' = ',i1,'/isel = ',i4)
 2002  format(i5,'    /',1a2,':',i2,' feature photon emissivity coeffi',
     &       'cients/')
c-----------------------------------------------------------------------

      end
