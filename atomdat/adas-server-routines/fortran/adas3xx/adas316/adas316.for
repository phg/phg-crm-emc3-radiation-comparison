       program adas316

       implicit none

C-----------------------------------------------------------------------
C
C  ********************* fortran 77 program: adas316   *****************
C
C  Purpose: Prepare effective emission coefficient data in adf12 format
C           in the bundle-n approximation.
C
C  Notes:   Revised v2b1mod for heavy species cx. Encompasses the old
C           looped running of v2bnmod (replaced by cgbnhs) and the
C           primary output reprocessing to adf26 tables. Designed for
C           use of universal adf49 sourced CX data and application to
C           partially ionised heavy specie receivers.
C
C  Program:
C
C
C  Routines:
C
C     routine    source    brief description
C     -------------------------------------------------------------
C     xxopen     adas      check existence and open a file
C     cgdnck     adas      check consistency of ion density & zeff
C     cgbntb     adas      execute four f-resolving bundle-n calcs
C     cgwr12     adas      write adf12 file from the bundle-n model
C     cgwr40     adas      write adf40 file from the bundle-n model
C     cgbnhs     adas      execute a single bundle-n calculation.
C     cxqntb     adas      return n-shell-selective cx coeffts.
C                          from an adf01 file
C     cxvcwr     adas      write vector with first line title field
C     xxpixv     adas      pixellate doppler broadened line
C     xxdata_25  adas      read a complete adf25 driver dataset
C
C  Note: Based on offline version from Hugh Summers, 15-06-2007.
C
C
C  Author   : Martin O'Mullane
C  Date     : 25-06-2007
C
C
C  Version : 1.1
C  Date    : 25-06-2007
C  Modified: Martin O'Mullane
C            - First version.
C
C  Version : 1.4
C  Date    : 15-07-2009
C  Modified: Martin O'Mullane
C            - Increase filename lengths to 120 characters.
C
C  Version : 1.5
C  Date    : 14-05-2013
C  Modified: Martin O'Mullane
C            - Write the reference beam energy value to the comments 
C              of the adf40 output file.
C            - An additional argument to cgwr40 is required (bmena).
C
C-----------------------------------------------------------------------
       integer    iunt12  , iunt26 , iunt25 , iunt40
       integer    ndtem   , ndden  , ndrep  , ndcor  , nddiel ,
     &            nddef   , ndimp  , ndlev  , ndein  , ndzef
       integer    ndwvl   , ndpix
       integer    pipein  , pipeou , one
C-----------------------------------------------------------------------
       parameter (iunt12 = 12 , iunt26 = 26 , iunt25 = 25 , iunt40 = 40)
       parameter (ndtem  = 35 , ndden  = 35 , ndrep  = 30 , ndcor = 20 ,
     &            nddiel = 10 , nddef  = 10 , ndimp  = 10 , ndlev = 550,
     &            ndein  = 30 , ndzef  = 10)
       parameter (ndwvl  = 5  , ndpix  = 1024)
       parameter (pipein = 5  , pipeou = 6  , one = 1)
C-----------------------------------------------------------------------
       integer    iz0     , iz1
       integer    ndens   , ntemp
       integer    nmin    , nmax   , imax
       integer    nip     , intd   , iprs   , ilow    , ionip , nionip ,
     &            ilprs   , ivdisp , nimp   , nbeam
       integer    jcor    , jmax   , jdef
       integer    jbm
       integer    i       , jdens  , jte    ,
     &            i4unit  , jzef
       integer    nzef
       integer    id_ref  , it_ref , iz_ref , ib_ref  , im_ref
       integer    nwvl    , nloops
C-----------------------------------------------------------------------
       real*8     ts      , w      , cion   , cpy    , w1
       real*8     zeff    , bmener , densh  , bmfr
       real*8     dens    , densp  , denimp , denion
       real*8     te      , tp     , timp   , tion
       real*8     prb     , plt    , prc
       real*8     emis_thres
C-----------------------------------------------------------------------
       logical    lexist
       logical    open25  , open26 , open12 , open40
C-----------------------------------------------------------------------
       character  a25fmt*8    , outfmt*5
       character  dsn25*120   , dsn26*120   , cxfile*120  , exfile*120
       character  dsn12*120   , dsn40*120
       character  date*8      , user*30     , rep*8
C-----------------------------------------------------------------------
       integer    nrep(ndrep+1)
       integer    npix(ndwvl)
C-----------------------------------------------------------------------
       real*8     densa(ndden)  , tea(ndtem)  ,
     &            denpa(ndden)  , tpa(ndtem)  ,
     &            denimpa(ndden), timpa(ndtem),
     &            deniona(ndden), tiona(ndtem)
       real*8     wbrep(ndrep)
       real*8     cor(ndcor)
       real*8     epsil(nddiel) , fij(nddiel) , wij(nddiel)
       real*8     def(nddef)
       real*8     f1(ndrep)     , f2(ndrep)     , f3(ndrep)     ,
     &            bncalc(ndrep) , bnact(ndrep)  , expsh(ndrep)
       real*8     zimpa(ndimp)  , amimpa(ndimp) , frimpa(ndimp)
       real*8     zefa(ndzef)
       real*8     bmena(ndein)  , denha(ndein)   , bmfra(ndein)
       real*8     xpop(2)
       real*8     alpha(2),hiss(2)
       real*8     p3a_ib(ndrep,ndein)  , p3a_it(ndrep,ndtem)  ,
     &            p3a_id(ndrep,ndden)  , p3a_iz(ndrep,ndzef)
       real*8     wvmin(ndwvl)         , wvmax(ndwvl)
C-----------------------------------------------------------------------
C *************************** MAIN PROGRAM ****************************
C-----------------------------------------------------------------------
C Set machine dependant ADAS configuration values and get date from IDL.
C-----------------------------------------------------------------------

       call xx0000
       call xxname(user)
       call xxdate(date)

C-----------------------------------------------------------------------
C       initialise
C-----------------------------------------------------------------------

       nwvl       = 1
       wvmin(1)   = 3000
       wvmax(1)   = 7000
       npix(1)    = 1024
       emis_thres = 1.0d-12

       open25 = .FALSE.
       open26 = .FALSE.
       open12 = .FALSE.
       open40 = .FALSE.

C-----------------------------------------------------------------------
C Screen 1 of ADAS316 - input adf25 dataset.
C-----------------------------------------------------------------------

 100   continue

       read(pipein, '(A)')rep
       if (rep(1:6).EQ.'CANCEL') goto 999

       if (open25) close(iunt25)

C  Read adf316 driver dataset of format adf25/a25_p316 - assume a25fmt

       read(pipein, '(A)')dsn25

       call xxopen( iunt25 , dsn25 , lexist )

       call xxdata_25( iunt25 , a25fmt , dsn25  ,
     &                 ndtem  , ndden  , ndrep  , ndcor , nddiel ,
     &                 nddef  , ndimp  , ndein  , ndzef  ,
     &                 iz0    , iz1    , outfmt ,
     &                 exfile , cxfile ,
     &                 ndens  , id_ref , densa  , denpa , denimpa,
     &                 deniona,
     &                 ntemp  , it_ref , tea    , tpa   , timpa  ,
     &                 tiona  ,
     &                 nzef   , iz_ref , zefa   ,
     &                 nbeam  , ib_ref , bmena  , denha , bmfra  ,
     &                 nimp   , im_ref , zimpa  , amimpa, frimpa ,
     &                 ts     , w      , w1     ,
     &                 cion   , cpy    , nip    , intd  , iprs   ,
     &                 ilow   , ionip  , nionip , ilprs , ivdisp ,
     &                 nmin   , nmax   , imax   , nrep  , wbrep  ,
     &                 jdef   , def    ,
     &                 jcor   , cor    , jmax   , epsil , fij    ,
     &                 wij
     &               )
       close(iunt25)
       open25 = .FALSE.



C-----------------------------------------------------------------------
C Screen 2 of ADAS316 - view graphs of reference case.
C-----------------------------------------------------------------------

 200   continue

       zeff   = zefa(iz_ref)
       tion   = tiona(it_ref)
       te     = tion
       tp     = tion
       timp   = tion
       denion = deniona(id_ref)

       call cgdnck( ndimp ,
     &              denion, zeff   ,
     &              nimp  , zimpa  , frimpa ,
     &              dens  , densp  , denimp
     &            )

       bmener = bmena(ib_ref)
       densh  = denha(ib_ref)
       bmfr   = bmfra(ib_ref)

       call cgbntb( ndtem  , ndden  , ndrep  , ndcor , nddiel ,
     &              nddef  , ndimp  , ndlev  , ndein , ndzef  ,
     &              iz0    , iz1    ,
     &              exfile , cxfile ,
     &              dens   , densp  , denimp ,
     &              te     , tp     , timp   ,
     &              zeff   ,
     &              bmener , densh  , bmfr   ,
     &              nimp   , zimpa  , amimpa , frimpa,
     &              ts     , w      , w1     ,
     &              cion   , cpy    , nip    , intd  , iprs   ,
     &              ilow   , ionip  , nionip , ilprs , ivdisp ,
     &              nmin   , nmax   , imax   , nrep  , wbrep  ,
     &              jdef   , def    ,
     &              jcor   , cor    , jmax   , epsil , fij    ,
     &              wij    ,
     &              f1     , f2     , f3     ,
     &              bncalc , bnact  , expsh  ,
     &              alpha  , hiss   , xpop   ,
     &              plt    , prb    , prc
     &            )


C Data to IDL for graphing

       write(pipeou, *)xpop(1)
       call xxflsh(pipeou)

       write(pipeou, *)dens
       call xxflsh(pipeou)

       write(pipeou, *)densh
       call xxflsh(pipeou)

       write(pipeou, *)imax
       call xxflsh(pipeou)

       do i = 1, imax
          write(pipeou, *)nrep(i)
          call xxflsh(pipeou)
       enddo
       do i = 1, imax
          write(pipeou, *)bnact(i)
          call xxflsh(pipeou)
       enddo
       do i = 1, imax
          write(pipeou, *)f1(i)
          call xxflsh(pipeou)
       enddo
       do i = 1, imax
          write(pipeou, *)f2(i)
          call xxflsh(pipeou)
       enddo
       do i = 1, imax
          write(pipeou, *)f3(i)
          call xxflsh(pipeou)
       enddo

       write(pipeou, *)iz0
       call xxflsh(pipeou)

       write(pipeou, *)iz1
       call xxflsh(pipeou)

       write(pipeou, *)tion
       call xxflsh(pipeou)

       write(pipeou, *)densp
       call xxflsh(pipeou)

       write(pipeou, *)denimp
       call xxflsh(pipeou)

       write(pipeou, *)denion
       call xxflsh(pipeou)

       write(pipeou, *)bmener
       call xxflsh(pipeou)

       write(pipeou, *)nimp
       call xxflsh(pipeou)

       do i = 1, nimp
         write(pipeou, *)zimpa(i)
         call xxflsh(pipeou)
       end do
       do i = 1, nimp
         write(pipeou, *)frimpa(i)
         call xxflsh(pipeou)
       end do


       read(pipein, '(A)')rep
       if (rep(1:4).EQ.'MENU') goto 999
       if (rep(1:6).EQ.'CANCEL') goto 100

C-----------------------------------------------------------------------
C Screen 3 of ADAS316 - select output files and calculate values.
C-----------------------------------------------------------------------

 300   continue

       nloops = nbeam + ntemp + ndens + nzef

       write(pipeou, *)nloops
       call xxflsh(pipeou)
       write(pipeou, *)ndwvl
       call xxflsh(pipeou)

C Get status from IDL

       read(pipein, '(A)')rep
       if (rep(1:4).EQ.'MENU') goto 999
       if (rep(1:6).EQ.'CANCEL') goto 200

C Get data to proceed to calculations

       read(pipein, *)nwvl
       do i = 1, nwvl
          read(pipein, *)npix(i)
          read(pipein, *)wvmin(i)
          read(pipein, *)wvmax(i)
       end do
       read(pipein, *)emis_thres

       read(pipein, '(A)')dsn26
       if (dsn26(1:4).EQ.'NULL') then
          open26 = .FALSE.
       else
          open26 = .TRUE.
       endif

       read(pipein, '(A)')dsn12
       if (dsn12(1:4).EQ.'NULL') then
          open12 = .FALSE.
       else
          open12 = .TRUE.
       endif

       read(pipein, '(A)')dsn40
       if (dsn40(1:4).EQ.'NULL') then
          open40 = .FALSE.
       else
          open40 = .TRUE.
       endif



C Now the calculations - send tick to IDL for updating

C  execute beam energy loop

       zeff = zefa(iz_ref)

       tion = tiona(it_ref)
       te   = tion
       tp   = tion
       timp = tion

       denion = deniona(id_ref)

       call cgdnck( ndimp ,
     &              denion, zeff   ,
     &              nimp  , zimpa  , frimpa ,
     &              dens  , densp  , denimp
     &            )

       do jbm = 1, nbeam

         write(pipeou, *)one
         call xxflsh(pipeou)

         bmener = bmena(jbm)
         densh  = denha(jbm)
         bmfr   = bmfra(jbm)

         call cgbntb ( ndtem  , ndden  , ndrep  , ndcor , nddiel ,
     &                 nddef  , ndimp  , ndlev  , ndein , ndzef  ,
     &                 iz0    , iz1    ,
     &                 exfile , cxfile ,
     &                 dens   , densp  , denimp ,
     &                 te     , tp     , timp   ,
     &                 zeff   ,
     &                 bmener , densh  , bmfr   ,
     &                 nimp   , zimpa  , amimpa , frimpa,
     &                 ts     , w      , w1     ,
     &                 cion   , cpy    , nip    , intd  , iprs   ,
     &                 ilow   , ionip  , nionip , ilprs , ivdisp ,
     &                 nmin   , nmax   , imax   , nrep  , wbrep  ,
     &                 jdef   , def    ,
     &                 jcor   , cor    , jmax   , epsil , fij    ,
     &                 wij    ,
     &                 f1     , f2     , f3     ,
     &                 bncalc , bnact  , expsh  ,
     &                 alpha  , hiss   , xpop   ,
     &                 plt    , prb    , prc
     &              )

C  write summary table for reference conditions

         if (ib_ref.eq.jbm.and.open26)then

            open( unit = iunt26 , file=dsn26 , status = 'unknown' )

            call cgwr26( iunt26 , dsn25  , user  , date   ,
     &                   ndrep  , ndimp  ,
     &                   iz0    , iz1    ,
     &                   dens   , densp  , denimp , denion,
     &                   te     , tp     , timp   , tion  ,
     &                   zeff   ,
     &                   bmener , densh  ,
     &                   nimp   , zimpa  , frimpa,
     &                   ts     , w      , w1     ,
     &                   cion   , cpy    , nip    , intd  , iprs   ,
     &                   ilow   , ionip  , nionip , ilprs , ivdisp ,
     &                   imax   , nrep   ,
     &                   f1     , f2     , f3     ,
     &                   bncalc , bnact  , expsh  ,
     &                   alpha  , hiss   , xpop   ,
     &                   plt    , prb    , prc
     &                 )

             close(iunt26)
             open26 = .FALSE.

         endif

         do i=1,imax
           p3a_ib(i,jbm)=f3(i)*expsh(i)
         enddo

       enddo


C  execute ion temperature loop


       zeff = zefa(iz_ref)

       bmener = bmena(ib_ref)
       densh  = denha(ib_ref)
       bmfr   = bmfra(ib_ref)

       denion = deniona(id_ref)

       call cgdnck( ndimp ,
     &              denion, zeff   ,
     &              nimp  , zimpa  , frimpa ,
     &              dens  , densp  , denimp
     &            )

       do jte=1,ntemp

         write(pipeou, *)one
         call xxflsh(pipeou)

         tion = tiona(jte)
         te   = tion
         tp   = tion
         timp = tion

         tea(jte)   = tion
         tpa(jte)   = tion
         timpa(jte) = tion

         call cgbntb( ndtem  , ndden  , ndrep  , ndcor , nddiel ,
     &                nddef  , ndimp  , ndlev  , ndein , ndzef  ,
     &                iz0    , iz1    ,
     &                exfile , cxfile ,
     &                dens   , densp  , denimp ,
     &                te     , tp     , timp   ,
     &                zeff   ,
     &                bmener , densh  , bmfr   ,
     &                nimp   , zimpa  , amimpa , frimpa,
     &                ts     , w      , w1     ,
     &                cion   , cpy    , nip    , intd  , iprs   ,
     &                ilow   , ionip  , nionip , ilprs , ivdisp ,
     &                nmin   , nmax   , imax   , nrep  , wbrep  ,
     &                jdef   , def    ,
     &                jcor   , cor    , jmax   , epsil , fij    ,
     &                wij    ,
     &                f1     , f2     , f3     ,
     &                bncalc , bnact  , expsh  ,
     &                alpha  , hiss   , xpop   ,
     &                plt    , prb    , prc
     &              )

         do i=1,imax
           p3a_it(i,jte)=f3(i)*expsh(i)
         enddo

       enddo


C  execute ion density loop


       zeff = zefa(iz_ref)

       bmener = bmena(ib_ref)
       densh  = denha(ib_ref)
       bmfr   = bmfra(ib_ref)

       tion   = tiona(it_ref)
       te     = tion
       tp     = tion
       timp   = tion

       do jdens=1,ndens

         write(pipeou, *)one
         call xxflsh(pipeou)

         denion = deniona(jdens)

         call cgdnck( ndimp ,
     &                denion, zeff   ,
     &                nimp  , zimpa  , frimpa ,
     &                dens  , densp  , denimp
     &              )
         densa(jdens)   = dens
         denpa(jdens)   = densp
         denimpa(jdens) = denimp


         call cgbntb( ndtem  , ndden  , ndrep  , ndcor , nddiel ,
     &                nddef  , ndimp  , ndlev  , ndein , ndzef  ,
     &                iz0    , iz1    ,
     &                exfile , cxfile ,
     &                dens   , densp  , denimp ,
     &                te     , tp     , timp   ,
     &                zeff   ,
     &                bmener , densh  , bmfr   ,
     &                nimp   , zimpa  , amimpa , frimpa,
     &                ts     , w      , w1     ,
     &                cion   , cpy    , nip    , intd  , iprs   ,
     &                ilow   , ionip  , nionip , ilprs , ivdisp ,
     &                nmin   , nmax   , imax   , nrep  , wbrep  ,
     &                jdef   , def    ,
     &                jcor   , cor    , jmax   , epsil , fij    ,
     &                wij    ,
     &                f1     , f2     , f3     ,
     &                bncalc , bnact  , expsh  ,
     &                alpha  , hiss   , xpop   ,
     &                plt    , prb    , prc
     &              )

         do i=1,imax
           p3a_id(i,jdens)=f3(i)*expsh(i)
         enddo

       enddo


C  execute zeff loop

       denion = deniona(id_ref)

       bmener = bmena(ib_ref)
       densh  = denha(ib_ref)
       bmfr   = bmfra(ib_ref)

       tion   = tiona(it_ref)
       te     = tion
       tp     = tion
       timp   = tion

       do jzef=1,nzef

         write(pipeou, *)one
         call xxflsh(pipeou)

         zeff = zefa(jzef)

         call cgdnck( ndimp ,
     &                denion, zeff   ,
     &                nimp  , zimpa  , frimpa ,
     &                dens  , densp  , denimp
     &              )


         call cgbntb( ndtem  , ndden  , ndrep  , ndcor , nddiel ,
     &                nddef  , ndimp  , ndlev  , ndein , ndzef  ,
     &                iz0    , iz1    ,
     &                exfile , cxfile ,
     &                dens   , densp  , denimp ,
     &                te     , tp     , timp   ,
     &                zeff   ,
     &                bmener , densh  , bmfr   ,
     &                nimp   , zimpa  , amimpa , frimpa,
     &                ts     , w      , w1     ,
     &                cion   , cpy    , nip    , intd  , iprs   ,
     &                ilow   , ionip  , nionip , ilprs , ivdisp ,
     &                nmin   , nmax   , imax   , nrep  , wbrep  ,
     &                jdef   , def    ,
     &                jcor   , cor    , jmax   , epsil , fij    ,
     &                wij    ,
     &                f1     , f2     , f3     ,
     &                bncalc , bnact  , expsh  ,
     &                alpha  , hiss   , xpop   ,
     &                plt    , prb    , prc
     &              )

         do i=1,imax
           p3a_iz(i,jzef)=f3(i)*expsh(i)
         enddo

       enddo



C  write adf40 file if requested

       if (open40) then

          if (nwvl.GT.0) then

             open( unit = iunt40 , file=dsn40 , status = 'unknown' )

             call cgwr40( iunt40 , dsn25  ,user   , date  ,
     &                    ndtem  , ndden  , ndrep  ,
     &                    ndimp  , ndlev  , ndein , ndzef  ,
     &                    ndwvl  , ndpix  ,
     &                    iz0    , iz1    ,
     &                    id_ref , densa  ,
     &                    deniona,
     &                    it_ref ,
     &                    tiona  ,
     &                    ib_ref , bmena  ,
     &                    nimp   , zimpa  , amimpa, frimpa ,
     &                    ts     , w      , w1     ,
     &                    cion   , cpy    , nip    , intd  , iprs   ,
     &                    ilow   , ionip  , nionip , ilprs , ivdisp ,
     &                    nmin   , nmax   , imax   , nrep  ,
     &                    p3a_ib ,
     &                    nwvl   , wvmin  , wvmax  , npix  , emis_thres
     &                    )

             close(iunt40)

          else

             write(i4unit(-1),*)'ADAS316: No wavelength intervals'

          endif

          open40 = .FALSE.

       endif


C  write adf12 file if requested

       if (open12) then

          if (nwvl.GT.0) then

            open( unit = iunt12 , file=dsn12 , status = 'unknown' )

             call cgwr12( iunt12 , dsn25  , user   , date  ,
     &                    ndtem  , ndden  , ndrep  ,
     &                    ndimp  , ndlev  , ndein , ndzef  , ndwvl  ,
     &                    iz0    , iz1    ,
     &                    ndens  , id_ref , densa  ,
     &                    deniona,
     &                    ntemp  , it_ref ,
     &                    tiona  ,
     &                    nzef   , iz_ref , zefa   ,
     &                    nbeam  , ib_ref , bmena  ,
     &                    nimp   , zimpa  , amimpa, frimpa ,
     &                    ts     , w      , w1     ,
     &                    cion   , cpy    , nip    , intd  , iprs   ,
     &                    ilow   , ionip  , nionip , ilprs , ivdisp ,
     &                    nmin   , nmax   , imax   , nrep  ,
     &                    p3a_ib , p3a_it , p3a_id , p3a_iz,
     &                    nwvl   , wvmin  , wvmax  , emis_thres
     &                  )

             close(iunt12)

          else

             write(i4unit(-1),*)'ADAS316: No wavelength intervals'

          endif

          open12 = .FALSE.

       endif


C-----------------------------------------------------------------------
C Return to the start.
C-----------------------------------------------------------------------

       goto 100

C----------------------------------------------------------------------
 999  stop
C-----------------------------------------------------------------------

       end
