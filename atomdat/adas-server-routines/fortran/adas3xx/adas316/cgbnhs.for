       subroutine cgbnhs ( ndrep  , ndcor  , nddiel , nddef , ndimp   ,
     &                     ndlev  , ndein  ,
     &                     z0     , cxfile , exfile ,
     &                     lproj  ,
     &                     dens   , densp  , te    , tp     ,
     &                     ts     , w      , z     , cion   , cpy     ,
     &                     w1     , nip    , intd  , iprs   , ilow    ,
     &                     ionip  , nionip , ilprs , ivdisp , zeff    ,
     &                     nimp   ,
     &                     zimpa  , amimpa , frimpa , denimp ,
     &                     bmener , densh  ,
     &                     nmin   , nmax   , imax  ,
     &                     nrep   , wbrep  ,
     &                     jcor   , cor    ,
     &                     jmax   , epsil  , fij   , wij    ,
     &                     jdef   , defect ,
     &                     brep   , dexpbn , pop   , dexpte ,
     &                     alfa   , s      ,
     &                     rl     , rr     , rd    , rb
     &                   )
       implicit none
c-----------------------------------------------------------------------
c
c  ********************* fortran 77 subroutine: cgbnhs *****************
c
c  purpose: executes a bundle-n population calaculation.
c
c  notes:  1. major revision of v2bnmod for heavy species
c
c          2. excit. xsect. options:
c
c                     (a) van regemorter    - electrons
c                     (b) impact parameter  - electrons
c                                             protons
c                     (c) percival-richards - electrons
c                                           - protons & zimp ions
c                     (d) vainshtein        - protons & zimp ions
c                     (e) special low level - electrons
c                                           - protons & zimp ions
c          3. ionis. xsect. options:
c
c                     (a) ecip              - electrons
c                     (b) percival-richards - protons & zimp ions
c                     (c) special low level - electrons
c                                           - protons & zimp ions
c          4. cx recom. xsect. options:
c
c                     (a) special           - h(1s) donor
c
c          5. special low level data accessed by specific ion routine:
c
c                    ion   routine    accessed file
c                    ---   -------    -------------
c                     h0    nsuph1    ionatom.data(h)
c                                     ionelec.data(szd#h)
c                                     hlike.data(bn#l96h)
c                                     hlike.data(bn#h96h)
c                     he1   nsuphe2   ionatom.data(h) ! not meaningful
c                                     ionelec.data(szd#he)
c                                     hlike.data(bn#l96he)
c                                     hlike.data(bn#h96he)
c                     c5    nsupc6    ionatom.data(c) ! not meaningful
c                                     ionelec.data(szd#c)
c                                     hlike.data(bn#l96c)
c                                     hlike.data(bn#h96c)
c
c
c  subroutine:
c
c   input : (i*4)  ndrep    = maximum number of representative n-shells
c   input : (i*4)  ndcor    = maximum number of DR bethe corrections
c   input : (i*4)  nddiel   = maximum number of DR core transitions
c   input : (i*4)  nddef    = maximum number of quantum defects
c   input : (i*4)  ndimp    = maximum number of plasma impurities
c   input : (i*4)  ndlev    = maximum number of n-shells
c   input : (i*4)  ndein    = maximum number of beam energies
c   input : (r*8)  z0       = nuclear charge of bundle-n ion
c   input : (c*80) cxfile   = file name for charge exchange data input
c   input : (c*80) exfile   = file name for map of proj. matrix output
c   input : (l*4)  lproj    = .true.  => write projection data to file
c                             .false. => do not write projection data
c   input : (r*8)  dens     = electron density (cm-3)
c   input : (r*8)  densp    = proton density (cm-3)
c   input : (r*8)  te       = electron temperature (K)
c   input : (r*8)  tp       = proton temperature (K)
c   input : (r*8)  ts       = external radiation field temperature (K)
c   input : (r*8)  w        = general radiation dilution factor
c   input : (r*8)  z        = recombining bundle-n ion charge
c   input : (r*8)  cion     = adjustment multiplier for ground ionis.
c   input : (r*8)  cpy      = adjustment multiplier for VR xsects.
c   input : (i*4)  w1       = external radiation field dilution factor
c                             for photo-ionisation form the ground level.
c   input : (i*4)  nip      = range of delta n for IP xsects. (le.4)
c   input : (i*4)  intd     = order of Maxw. quad. for IP xsects.(le.3)
c   input : (i*4)  iprs     = 0  => default to VR xsects. beyond nip range
c                             1  => use PR xsects. beyond nip range
c   input : (i*4)  ilow     = 0  => no special low level data accessed
c                             1  => special low level data accessed
c   input : (i*4)  ionip    = 0 =>   no ion impact collisions included
c                             1 =>ion impact excit. and ionis. included
c   input : (i*4)  nionip   = range of delta n for ion impact
c                             excitation xsects.
c   input : (i*4)  ilprs    = 0 => default to vainshtein xsects.
c                             1 => use lodge-percival-richards xsects.
c   input : (i*4)  ivdisp   = 0 => ion impact at thermal Maxw. energies
c                             1 => ion impact at displaced thermal
c                                  energies according to the neutral
c                                  beam energy parameter
c                               * if(ivdisp=0 then special low level
c                                 data for ion impact is not substituted -
c                                 only vainshtein and lodge et al.
c                                 options are open.  Electron impact
c                                 data substitution does occur.
c   input : (r*8)  zeff     = plasma z effective
c   input : (i*4)  nimp     = number of impurities (excl.h+)
c
c   input : (r*8)  zimpa()  = charge of impurity species
c                             1st dim: impurity index
c   input : (r*8)  amimpa() = atomic mass number of impurity species
c                             1st dim: impurity index
c   input : (r*8)  frimpa() = fraction of impurity (normalised to 1)
c                             1st dim: impurity index
c
c   input : (r*8)  denimp   = net impurity number density (cm-3)
c                               * adjusted to be consistent with zeff
c   input : (r*8)  bmener   = neutral beam energy (ev/amu)
c   input : (r*8)  densh    = neutral beam number density (cm-3)
c   input : (i*4)  nmin     = lowest n-shell for population structure
c   input : (i*4)  nmax     = highest n-shell fro population structure
c   input : (i*4)  imax     = number of representative n-shells
c   input : (i*4)  nrep()   = representative n-shells
c                             1st dim: index of representative n-shell
c   input : (r*8)  wbrep()  = dilution factors for nmin->nrep() trans.
c                             1st dim: index of representative n-shell
c   input : (i*4)  jcor     = number of DR Bethe correction factors
c   input : (r*8)  cor()    = DR Bethe correction factors
c                             1st dim: index of correction factor
c   input : (i*4)  jmax     = number of DR core transitions
c   input : (r*8)  epsil()  = reduced energy of core transition
c                             [delta Eij/I_H=(z+1)^2*epsil()]
c                             1st dim: index of DR core transition
c   input : (r*8)  fij()    = absorption oscillator strength for
c                             DR core transition
c                             1st dim: index of DR core transition
c   input : (r*8)  wij()    = dilution factor for DR core transition
c                             1st dim: index of DR core transition
c   input : (i*4)  jdef     = number of n-shell quantum defects
c   input : (r*8)  defect() = quanum defects for n-shells
c                             1st dim: index of n-shell quantum defects
c                                      upwards from nmin
c
c   output: (r*8)  brep()   = b-factors for representative n-shells
c                             1st dim: index of representative n-shell
c   output: (r*8)  dexpbn() = exp(I_n/kT_e)b_n for represent. n-shell
c                             1st dim: index of representative n-shell
c   output: (r*8)  pop()    = population of representative n-shell
c                              [ =  N_n/N_eN+ (cm-3) ]
c                             1st dim: index of representative n-shell
c   output: (r*8)  dexpte() = exp(I_n/kT_e) for represent. n-shell
c                             1st dim: index of representative n-shell
c   output: (r*8)  alfa     = coll. rad. recom. coefft. (cm3 s-1)
c   output: (r*8)  s        = coll. rad. ionis. coefft. (cm3 s-1)
c   output: (r*8)  rl       = line radiated power
c   output: (r*8)  rr       = radiative recom. radiated power
c   output: (r*8)  rd       = dielectronic recom. radiated power
c   output: (r*8)  rb       = bremsstrahlung radiated power
c
c  routines:
c
c     routine    source    brief description
c     -------------------------------------------------------------
c     cxqntb     adas      return n-shell-selective cx coeffts.
c                          from an adf01 file
c     xxopen     adas
c     i4unit     adas
c     finter     adas
c     pyip       adas
c     gbb        adas
c     pyvr       adas
c     pypr       adas
c     rqvnew     adas
c     rqlnew     adas
c     colint     adas
c     rqinew     adas
c     photo      adas
c     diel       adas
c     rphoto     adas
c     cldlbn2    adas
c     matinv     adas
c     ngffmh     adas
c
c
c  author   : Hugh Summers
c  date     : 15-06-2007
c
c  Version : 1.1
c  Date    : 15-06-2007
c  Modified: Hugh Summers
c            - First version.
c
c  Version : 1.2
c  Date    : 04-12-2008
c  Modified: Martin O'Mullane
c            - Remove redundant format statements. These produced
c              fatal errors with ifort compiler.
c
c  Version : 1.3
c  Date    : 14-07-2009
c  Modified: Martin O'Mullane
c            - Use a variable, embeam, for mass of beam particle.
c            - Increase dimension of bmena and bmfra from 6 to 30 to
c              be consistent with other code.
c            - Remove diagnostic print blocks.
c            - cxfile and exfile set to 120 characters in length.
c
c  Version : 1.4
c  Date    : 20-03-2019
c  Modified: Martin O'Mullane
c            - Save the values in bmener_sav and qthrep between calls.
c            - Dimension qthrep (and qcxn) with local parameters and
c              not the pass-in dimension of ndrep and ndein.
c
c-----------------------------------------------------------------------
       integer   iunt11
       integer   idrep  , idcor  , iddiel , iddef  , idlev, idein
       integer   ndlow
c-----------------------------------------------------------------------
       parameter  (iunt11 = 11 )
       parameter  (idrep = 30 , idcor = 20 , iddiel= 10 , iddef = 10 ,
     &             idlev = 550, idein = 30)
       parameter ( ndlow = 10 )
c-----------------------------------------------------------------------
       integer   i4unit
       integer   ndrep  , ndcor  , nddiel , nddef   , ndimp  , ndlev  ,
     &           ndein
       integer   nip    , intd   , iprs   , ilow    , ionip   , nionip ,
     &           ilprs  , ivdisp , nimp
       integer   nmin   , nmax   , imax
       integer   jcor   , jmax   , jdef
       integer   nbeam  , i      , j      , ityp1   , ityp2   , ityp3  ,
     &           ityp4  , ityp5  , ityp6  , lpstbe  , lpstbp  , lpstbz ,
     &           idl    , idl1   , iz     , nmax1   , n       , n1     ,
     &           n11    , n2     , n3     , ntest   , i1      , i2     ,
     &           i3     , imp    , iecion , iedrec  , ierrec  , iexrec ,
     &           idcn   , iz0
c-----------------------------------------------------------------------
       real*8    z0
       real*8    ts     , w      , z      , cion    , cpy    , w1     ,
     &           zeff   , denimp
       real*8    bmener , densh  , embeam
       real*8    flux   , alfa   , s      ,
     &           rl     , rr     , rd     , rb
       real*8    bmener_sav
       real*8    finter , gbb    , rqvnew , rqlnew, rqinew   , ngffmh
       real*8    rho    , rhop   , rhoimp , py0   , enmax2   , eye     ,
     &           en     , def    , a1     , en1   , x1       , x2      ,
     &           x3     , zcol   , en11   , d     , d1       , phi     ,
     &           wi     , wj     , r      , em    , w2       , cpl     ,
     &           y      , ys     , g      , ei    , ej       ,
     &           f      , rdexc  , pyd    , pyu   , pypd     , pypu    ,
     &           pyimpd , pyimpu , pydval , pyuval, a2       , a3d     ,
     &           a3u    , ye     , enc    , ai    , secip    , sion    ,
     &           sionp  , sionz  ,
     &           sionval, pion   , prec     , pstim ,
     &           ads    , eij    , weijj  , ad    , expon    , rad     ,
     &           rrec   , rx3    , x4     , ra    , z1       , acnst   ,
     &           a1cnst , determ , c      , cn1   , cn2      , cn3     ,
     &           vdisp  , ate    , ats
       real*8    cut    , alpha  , x      , py    , pyp      , pstbe   ,
     &           pstbp  , pstbz  , tev    , te    , tpev     , tp      ,
     &           dens   , densp  , atp
c-----------------------------------------------------------------------
       character cxfile*120, exfile*120
c-----------------------------------------------------------------------
       logical   lexist    , lproj        , lpass
c-----------------------------------------------------------------------
       integer   nrep(ndrep+1)
       integer   inter(idlev)
c-----------------------------------------------------------------------
       real*8    wbrep(ndrep)
       real*8    cor(ndcor)
       real*8    epsil(nddiel)   , fij(nddiel)   , wij(nddiel)     ,
     &           eeij(iddiel)    , weij(iddiel)
       real*8    defect(nddef)
       real*8    dexpte(ndlev)
       real*8    en2(idlev)      , en3(idlev)    , en23(idlev)     ,
     &           dexpts(idlev)   , wb(idlev)     ,
     &           flag(idlev)     , a(idlev)      ,  ogarl(2*idlev) ,
     &           agrl(idlev,3)   , pymat(idlev,4,2)
       real*8    rhs(idrep)      , brep(ndrep)   , dexpbn(ndrep)
       real*8    pop(ndrep), delcn(idrep),wblog(idrep)
       real*8    dvec(idrep),cionpt(idrep),drecpt(idrep),
     &           rrecpt(idrep),xrecpt(idrep)
       real*8    ared(idrep,idrep)
       real*8    dg(idrep+1),dgs1(idrep+1),dgs2(idrep+1),vec(idrep+1)
       real*8    prep(idrep+1),rlrep(idrep+1),rdrep(idrep+1),
     &           rrrep(idrep+1)
       real*8    qthrep(idrep+1) , qcxn(idein,idrep+1)
       real*8    zimpa(ndimp)    ,amimpa(ndimp), frimpa(ndimp)
       real*8    xtbe(ndlow,ndlow), xtbp(ndlow,ndlow), xtbz(ndlow,ndlow)
       real*8    pxtbe(ndlow)     , pxtbp(ndlow)     , pxtbz(ndlow)
       real*8    stbe(ndlow)      , stbp(ndlow)      , stbz(ndlow)
       real*8    bmena(30)        , bmfra(30)
c-----------------------------------------------------------------------
       integer   lxtbe(ndlow,ndlow) , lxtbp(ndlow,ndlow) ,
     &           lxtbz(ndlow,ndlow)
       integer   lpxtbe(ndlow)      , lpxtbp(ndlow)      , lpxtbz(ndlow)
       integer   lstbe(ndlow)       , lstbp(ndlow)       , lstbz(ndlow)
c-----------------------------------------------------------------------
       save      bmener_sav         , qthrep
c-----------------------------------------------------------------------
       data      bmener_sav/0.0d0/
c-----------------------------------------------------------------------
       

c-----------------------------------------------------------------------
c  initial internal dimension check
c-----------------------------------------------------------------------

       if (idrep.ne.ndrep) then
          write(i4unit(-1),1001) 'idrep=',idrep,' .ne. ndrep=',ndrep
          write(i4unit(-1),1002)
          stop
       endif

       if (idlev.ne.ndlev) then
          write(i4unit(-1),1001) 'idlev=',idlev,' .ne. ndlev=',ndlev
          write(i4unit(-1),1002)
          stop
       endif

       if (idcor.ne.ndcor) then
          write(i4unit(-1),1001) 'idcor=',idcor,' .ne. ndcor=',ndcor
          write(i4unit(-1),1002)
          stop
       endif

       if (iddiel.ne.nddiel) then
          write(i4unit(-1),1001) 'iddiel=',iddiel,' .ne. nddiel=',nddiel
          write(i4unit(-1),1002)
          stop
       endif

       if (iddef.ne.nddef) then
          write(i4unit(-1),1001) 'iddef=',iddef,' .ne. nddef=',nddef
          write(i4unit(-1),1002)
          stop
       endif

c-----------------------------------------------------------------------
c  initialise
c-----------------------------------------------------------------------
       embeam = 2.0D0

       em  = 918.0d0
       cut = 170.d00

       flux=1.38317498d6*dsqrt(bmener)*densh
       nbeam=1
       bmena(1)=bmener
       bmfra(1)=1.0d0
       
       lpass = .false.
       
c-----------------------------------------------------------------------
c  fetch charge exchange data from archived data set if necessary
c-----------------------------------------------------------------------
       if (bmener.ne.bmener_sav) then
            
            call xxopen(iunt11 , cxfile , lexist)

            if(.not.lexist) then
                write(i4unit(-1),*)'cxfile does not exist: ',cxfile
                stop
            endif

            call cxqntb( iunt11 ,
     &                   ndein  , ndrep  , ndlev  ,
     &                   imax   , nrep   ,
     &                   nbeam  , bmena  , bmfra  ,
     &                   qcxn   , qthrep , alpha
     &                 )

             close(iunt11)

             bmener_sav=bmener

       endif

       do 76 j=1,jmax
       if(wij(j)*fij(j)-1.0d-50)71,72,72
   71  weij(j)=0.0d0
       go to 76
   72  x=z+1.0d0
       x=x*x*epsil(j)*157890.0d0/ts
       if(x-cut)73,71,71
   73  if(x-0.001d0)74,75,75
   74  weij(j)=wij(j)/(x*(1.0d0+x*(0.5d0+0.16666667d0*x)))
       go to 76
   75  weij(j)=wij(j)/(dexp(x)-1.0d0)
   76  continue

c-----------------------------------------------------------------------
c  compute displaced speed for ion impact maxwellian averaging
c-----------------------------------------------------------------------
       if(ivdisp.eq.1)then
           vdisp=1.38317498d6*dsqrt(bmener)
       else
           vdisp=0.0d0
       endif
c
       py=0.0d0
       pyp=0.0d0

c-----------------------------------------------------------------------
c  zero lookup arrays and pointer arrays associated with low level data
c-----------------------------------------------------------------------
       ityp1=1
       ityp2=1
       ityp3=1
       ityp4=1
       ityp5=1
       ityp6=1
       pstbe=1.0d0
       pstbp=1.0d0
       pstbz=1.0d0
       lpstbe=0
       lpstbp=0
       lpstbz=0
       do 162 idl=1,ndlow
        stbe(idl)=0.0d0
        stbp(idl)=0.0d0
        stbz(idl)=0.0d0
        lstbe(idl)=0
        lstbp(idl)=0
        lstbz(idl)=0
        pxtbe(idl)=1.0d0
        pxtbp(idl)=1.0d0
        pxtbz(idl)=1.0d0
        lpxtbe(idl)=0
        lpxtbp(idl)=0
        lpxtbz(idl)=0
        do 160 idl1=1,ndlow
         xtbe(idl,idl1)=0.0d0
         xtbp(idl,idl1)=0.0d0
         xtbz(idl,idl1)=0.0d0
         lxtbe(idl,idl1)=0
         lxtbp(idl,idl1)=0
         lxtbz(idl,idl1)=0
  160   continue
  162  continue
c-----------------------------------------------------------------------
c  acquire special low level data for the particular ion
c
c      *  ion collisions at vdisp corresponding to the beam speed is
c         forced independent of the ivdisp parameter
c-----------------------------------------------------------------------
       iz=int(z)
       iz0=int(z0)
       tev=te/11605.4d0
       tpev=tp/11605.4d0

c        if(ilow.gt.0)then
c            if(iz0.eq.1.and.iz.eq.1)then
c                call nsuph1(tev,bmener,tpev,nimp ,zimpa ,frimpa,amimpa,
c      &                      ityp1 ,ityp2 ,ityp3 ,ityp4 ,ityp5 ,ityp6 ,
c      &                      xtbe  , xtbp , xtbz , stbe , stbp , stbz ,
c      &                      lxtbe ,lxtbp ,lxtbz ,lstbe ,lstbp ,lstbz ,
c      &                      pxtbe ,pxtbp ,pxtbz ,pstbe ,pstbp ,pstbz ,
c      &                      lpxtbe,lpxtbp,lpxtbz,lpstbe,lpstbp,lpstbz,
c      &                      dlspath)
c      
c           elseif(iz0.eq.2.and.iz.eq.2)then
c               call nsuphe2(tev,bmener,tpev,nimp ,zimpa ,frimpa,amimpa,
c     &                      ityp1 ,ityp2 ,ityp3 ,ityp4 ,ityp5 ,ityp6 ,
c     &                      xtbe  , xtbp , xtbz , stbe , stbp , stbz ,
c     &                      lxtbe ,lxtbp ,lxtbz ,lstbe ,lstbp ,lstbz ,
c     &                      pxtbe ,pxtbp ,pxtbz ,pstbe ,pstbp ,pstbz ,
c     &                      lpxtbe,lpxtbp,lpxtbz,lpstbe,lpstbp,lpstbz)
c           elseif(iz0.eq.6.and.iz.eq.6)then
c               call nsupc6 (tev,bmener,tpev,nimp ,zimpa ,frimpa,amimpa,
c     &                      ityp1 ,ityp2 ,ityp3 ,ityp4 ,ityp5 ,ityp6 ,
c     &                      xtbe  , xtbp , xtbz , stbe , stbp , stbz ,
c     &                      lxtbe ,lxtbp ,lxtbz ,lstbe ,lstbp ,lstbz ,
c     &                      pxtbe ,pxtbp ,pxtbz ,pstbe ,pstbp ,pstbz ,
c     &                      lpxtbe,lpxtbp,lpxtbz,lpstbe,lpstbp,lpstbz)
c           else
c               continue
c           endif
c       endif
c
c       write(6,131)
c       write(6,132)dens,te
c       write(6,143)
c       write(6,132)densp,tp

       x=157890.0d0*z*z
       ate=x/te
       atp=x/tp
       ats=x/ts
       rho=1.27787d-17/z**7
       rhop=rho*densp*dsqrt(x/tp)
       rhoimp=rho*denimp*dsqrt(x/tp)
       rho=rho*dens*dsqrt(ate)
       py0=-dlog(ate)-0.41d0
       do 79 j=1,jmax
       if(fij(j)-0.000001d0)77,77,78
   77  eeij(j)=0.0d0
       go to 79
   78  x=z+1.0d0
       x=x*x*epsil(j)*157890.0d0/te
       eeij(j)=dexp(-x)
   79  continue
       x=nmax

       enmax2=1.0d0/((x+0.5d0)*(x+0.5d0))
c      enmax2=0.00d+00
       eye=dexp(ate*enmax2)

       nmax1=nmax+nip
       do 64 n=1,nmin
       en=n
       en23(n)=en**(-0.6666667d0)
   64  continue
       do 202 n=nmin,nmax1
       en=n
       def=0.0d0
       j=n+1-nmin
       if(j-jdef)191,191,192
  191  def=defect(j)
  192  a1=1.0d0/(en-def)
       en2(n)=a1*a1
       en3(n)=en2(n)*a1
       en23(n)=en**(-0.6666667d0)
       flag(n)=finter(en)
       dexpte(n)=dexp(ate*en2(n))
       x=ats*en2(n)
       if(x-cut)201,200,200
  200  dexpts(n)=0.0d0
       go to 202
  201  dexpts(n)=dexp(x)
  202  continue
       do 1 n=1,nmax
       en=n
       ogarl(n)=dlog(en)
       n1=nmax+n
       en1=n1
    1  ogarl(n1)=dlog(en1)
       nrep(imax+1)=nmax+nmax
       i=3
       n1=nmin+2
       do 4 n=n1,nmax
       if(n+n-nrep(i)-nrep(i+1))3,3,2
    2  i=i+1
    3  inter(n)=i
    4  continue
       inter(nmin)=2
       inter(nmin+1)=2
       do 80 i=1,imax+1
       vec(i)=0.0d0
       dg(i)=0.0d0
       dgs1(i)=0.0d0
       dgs2(i)=0.0d0
       prep(i)=0.0d0
       rlrep(i)=0.0d0
       rdrep(i)=0.0d0
   80  rrrep(i)=0.0d0
       do 8 n=nmin,nmax
       x=flag(n)
       i=inter(n)
       i1=nrep(i-1)
       i2=nrep(i)
       x1=flag(i1)
       x2=flag(i2)
       if(imax-i)6,6,5
    5  i3=nrep(i+1)
       x3=flag(i3)
       go to 7
    6  x3=0.0d0
    7  agrl(n,1)=(x-x2)*(x-x3)/((x1-x2)*(x1-x3))
       agrl(n,2)=(x-x1)*(x-x3)/((x2-x1)*(x2-x3))
       agrl(n,3)=(x-x1)*(x-x2)/((x3-x1)*(x3-x2))
       dg(i-1)=dg(i-1)+agrl(n,1)**2
       dg(i)=dg(i)+agrl(n,2)**2
       dg(i+1)=dg(i+1)+agrl(n,3)**2
       dgs1(i-1)=dgs1(i-1)+agrl(n,1)*agrl(n,2)
       dgs1(i)=dgs1(i)+agrl(n,2)*agrl(n,3)
       dgs2(i-1)=dgs2(i-1)+agrl(n,1)*agrl(n,3)
       vec(i-1)=vec(i-1)+agrl(n,1)
       vec(i)=vec(i)+agrl(n,2)
       vec(i+1)=vec(i+1)+agrl(n,3)
    8  continue
c-----------------------------------------------------------------------
c  evaluate and store impact parameter cross-sections for electrons and
c  protons
c-----------------------------------------------------------------------
       zcol=z-1.0d0
       if(nip.gt.0)then
           do 500 n=nmin,nmax
            en=n
            do 501 i=1,nip
             n11=n+i
             en11=n11
             d=en2(n)-en2(n11)
             d1=1.0d0/(d*d)
             phi=1.56d0*en3(n)*en2(n)*en3(n11)*d1*d1/(z*z)
             wi=2.0d0*en*en
             wj=2.0d0*en11*en11
             r=0.25d0*(5.0d0*en*en+1.0d0)/z
             if(dens.gt.0.0d0)then
                 call pyip(d,1.0d0,zcol,phi,cpy,wi,wj,r,te,intd,py)
             else
                 py=0.0d0
             endif
             pymat(n,i,1)=py
             if(densp.gt.0.0d0)then
                 call pyip(d,em,zcol,phi,cpy,wi,wj,r,tp,intd,pyp)
             else
                 pyp=0.0d0
             endif
             pymat(n,i,2)=pyp
  501       continue
  500      continue
       endif
c
       if(wbrep(1))60,60,62
   60  do 61 n=nmin,nmax
       wb(n)=w
   61  continue
       go to 63
   62  do 49 i=1,imax
       wblog(i)=dlog(wbrep(i))
   49  continue
       n1=nrep(imax-1)-1
       do 50 n=nmin,n1
       i=inter(n)
       w2=wblog(i-1)*agrl(n,1)+wblog(i)*agrl(n,2)+wblog(i+1)
     1*agrl(n,3)
       wb(n)=dexp(w2)
   50  continue
       w2=wbrep(imax)
       n1=n1+1
       do 48 n=n1,nmax
       wb(n)=w2
   48  continue
   63  continue
       rl=0.0d0
       rd=0.0d0
       rr=0.0d0
       cpl=2.26530d-24*dens*(157890.0d0/te)**1.5d0*z**6
       do 30 i=1,imax
       rhs(i)=0.0d0
       n=nrep(i)
       a(n)=0.0d0
       en=n
       dvec(i)=en*en*dexpte(n)
       n1=n+1
       do 16 n11=n1,nmax
       w2=w
       if(n.le.nmin)then
           w2=wb(n11)
       endif
       d=en2(n)-en2(n11)
       y=ate*d
       ys=ats*d
       d=1.0d0/d
       en11=n11
       x2=en2(n11)
       n2=n11-n
       x=en23(n)*en23(n2)/en23(n11-1)
       g=gbb(en11,en,x2,x)
       a1=en3(n)*en3(n11)*d*g
c-----------------------------------------------------------------------
c  impact excitation data
c-----------------------------------------------------------------------
       ei=en2(n)
       wi=2.0d0*en*en
       ej=en2(n11)
       wj=2.0d0*en11*en11
       f=1.96028d0*g*d*d*d*en3(n)*en2(n)*en3(n11)
       phi=f*d/(z*z)
c-----------------------------------------------------------------------
c      basic electron data  - van regemorter or percival-richards
c-----------------------------------------------------------------------
       if(iprs.le.0)then
           call pyvr(y,z,py)
       else
           call pypr(ei,ej,n,n11,1.0d0,z,phi,wi,wj,te,intd,py,rdexc)
       endif
c-----------------------------------------------------------------------
c                         substitute impact parameter if requested
c-----------------------------------------------------------------------
       ntest=n11-n
       if(ntest.le.nip)then
           py=pymat(n,ntest,1)
       endif
       pyd=py
       pyu=py
c-----------------------------------------------------------------------
c                         substitute special low level data if available
c                         nb. de-excitation and excitation rate coeffts.
c                             supplied separately
c-----------------------------------------------------------------------
       if(ilow.gt.0.and.n.le.ndlow.and.n11.le.ndlow)then
           if(lxtbe(n,n11).eq.1)then
               pyd=xtbe(n11,n)/
     &             (3.15d-7*dsqrt(1.57890d5/te)*phi*
     &             (en/en11)**2)
               pyu=xtbe(n,n11)/
     &            (3.15d-7*dsqrt(1.57890d5/te)*phi*
     &            dexpte(n11)/dexpte(n))
            endif
       endif
c-----------------------------------------------------------------------
c      basic ion  data      - vainshtein or lodge-percival-richards
c                             nb. excitation and deexcitation coeffts.
c                                 must be supplied separately
c-----------------------------------------------------------------------
       pypd=0.0d0
       pypu=0.0d0
       pyimpd=0.0d0
       pyimpu=0.0d0
       if(ionip.eq.1)then
           if(ilprs.le.0)then
               if(densp.gt.0.0d0.and.ntest.le.nionip)then
                   pypd=rqvnew(z,n11,n,phi,1.0d0,embeam,tp,vdisp)/
     &                  (3.15d-7*dsqrt(1.57890d5/tp)*phi*
     &                  (en/en11)**2)
                   pypu=rqvnew(z,n,n11,phi,1.0d0,embeam,tp,vdisp)/
     &                  (3.15d-7*dsqrt(1.57890d5/tp)*phi*
     &                  dexpte(n11)/dexpte(n))
               else
                   pypd=0.0d0
                   pypu=0.0d0
               endif
               pyimpd=0.0d0
               pyimpu=0.0d0
               if(denimp.gt.0.0d0.and.ntest.le.nionip)then
                   do 41 imp=1,nimp
                   if(frimpa(imp).eq.0.0d0)go to 41
                   pyimpd=pyimpd+frimpa(imp)*rqvnew(z,n11,n,phi,
     &                    zimpa(imp),amimpa(imp),tp,vdisp)/
     &                    (3.15d-7*dsqrt(1.57890d5/tp)*phi*
     &                    (en/en11)**2)
                   pyimpu=pyimpu+frimpa(imp)*rqvnew(z,n,n11,phi,
     &                    zimpa(imp),amimpa(imp),tp,vdisp)/
     &                     (3.15d-7*dsqrt(1.57890d5/tp)*phi*
     &                     dexpte(n11)/dexpte(n))
   41              continue
               else
                   pyimpd=0.0d0
                   pyimpu=0.0d0
               endif
           else
               if(densp.gt.0.0d0.and.ntest.le.nionip)then
                   pypd=rqlnew(z,n11,n,phi,1.0d0,embeam,tp,vdisp)
                   pypd=pypd/
     &                  (3.15d-7*dsqrt(1.57890d5/tp)*phi*
     &                  (en/en11)**2)
                   pypu=rqlnew(z,n,n11,phi,1.0d0,embeam,tp,vdisp)
                   pypu=pypu/
     &                  (3.15d-7*dsqrt(1.57890d5/tp)*phi*
     &                  dexpte(n11)/dexpte(n))
               else
                   pypd=0.0d0
                   pypu=0.0d0
               endif
               if(denimp.gt.0.0d0.and.ntest.le.nionip)then
                   pyimpd=0.0d0
                   pyimpu=0.0d0
                   do 42 imp=1,nimp
                    if(frimpa(imp).eq.0.0d0)go to 42
                    pydval=frimpa(imp)*rqlnew(z,n11,n,phi,
     &                     zimpa(imp),amimpa(imp),tp,vdisp)
                    pyimpd=pyimpd+pydval/
     &                     (3.15d-7*dsqrt(1.57890d5/tp)*phi*
     &                     (en/en11)**2)
                    pyuval=frimpa(imp)*rqlnew(z,n,n11,phi,
     &                     zimpa(imp),amimpa(imp),tp,vdisp)
                    pyimpu=pyimpu+pyuval/
     &                     (3.15d-7*dsqrt(1.57890d5/tp)*phi*
     &                     dexpte(n11)/dexpte(n))
   42               continue
               else
                   pyimpd=0.0d0
                   pyimpu=0.0d0
               endif
           endif
c-----------------------------------------------------------------------
c                         substitute special low level data if available
c                         nb. excitation and deexcitation coeffts. are
c                             separately supplied
c-----------------------------------------------------------------------
           if(ilow.gt.0.and.n.le.ndlow.and.n11.le.ndlow.and.
     &                                     ivdisp.eq.1)then
               if(densp.gt.0.0d0)then
                   if(lxtbp(n,n11).eq.1)then
                       pypd=xtbp(n11,n)/
     &                      (3.15d-7*dsqrt(1.57890d5/tp)*phi*
     &                      (en/en11)**2)
                       pypu=xtbp(n,n11)/
     &                      (3.15d-7*dsqrt(1.57890d5/tp)*phi*
     &                      dexpte(n11)/dexpte(n))
                   endif
               endif
               if(denimp.gt.0.0d0)then
                   if(lxtbz(n,n11).eq.1)then
                       pyimpd=xtbz(n11,n)/
     &                        (3.15d-7*dsqrt(1.57890d5/tp)*phi*
     &                        (en/en11)**2)
                       pyimpu=xtbz(n,n11)/
     &                        (3.15d-7*dsqrt(1.57890d5/tp)*phi*
     &                        dexpte(n11)/dexpte(n))
                   endif
                endif
           endif
       endif
       pyd=pyd*rho+pypd*rhop+pyimpd*rhoimp
       pyu=pyu*rho+pypu*rhop+pyimpu*rhoimp

       if(ys-0.001d0)203,203,204
  203  a2=w2/(ys+0.5d0*ys*ys)
       go to 209
  204  if(dexpts(n))205,205,208
  205  if(ys-cut)207,206,206
  206  a2=0.0d0
       go to 209
  207  a2=w2/(dexp(ys)-1.0d0)
       go to 209
  208  a2=w2/(dexpts(n)/dexpts(n11)-1.0d0)
  209  a3d=3.06998d0*d*d*d*pyd
       a3u=3.06998d0*d*d*d*pyu
       a(n11)=-a1*(1.0d0+a2+a3d)*dexpte(n11)
       a(n)=a(n)+a1*(a2*dexpte(n)+a3u*dexpte(n11))
       rhs(i)=rhs(i)+a1*(dexpte(n11)  +a2*(dexpte(n11)-dexpte(n)))
   16  continue
       ye=ate*(en2(n)-enmax2)
       ys=ats*(en2(n)-enmax2)
c-----------------------------------------------------------------------
c  impact direct and charge exchange ionisation data
c-----------------------------------------------------------------------
c
c-----------------------------------------------------------------------
c      basic electron data  - ecip
c-----------------------------------------------------------------------
       enc=1.0d0/dsqrt(en2(n)-enmax2)
       call colint(ye,z,enc,ai)
       ai=ai/cion
       w2=w
       if(n.le.nmin)then
           ai=cion*ai
           w2=w1
       endif
       secip=pstbe*0.43171d0*rho*ai*enc*enc*1.57456d10*z**4/
     &       (dens*dexpte(n))
c-----------------------------------------------------------------------
c                        substitute special electron data
c-----------------------------------------------------------------------
       if(ilow.gt.0.and.n.le.ndlow)then
           sion=secip
           if(lstbe(n).eq.1)then
               sion=stbe(n)
               if(lpstbe.eq.n)pstbe=sion/secip
           endif
c-----------------------------------------------------------------------
c      basic ion  data      - percival-richards direct only
c-----------------------------------------------------------------------
           if(ionip.gt.0)then
               if(densp.gt.0.0d0)then
                   sionp=pstbp*rqinew(z,n,1.0d0,1.0d0,tp,vdisp)
               else
                   sionp=0.0d0
               endif
               if(denimp.gt.0.0d0)then
                   sionval=0.0d0
                   do 43 imp=1,nimp
                     if(frimpa(imp).eq.0.0d0)go to 43
                     sionval=sionval +frimpa(imp)*rqinew(z,n,
     &                       zimpa(imp),amimpa(imp),tp,vdisp)
   43              continue
                   sionz=pstbz*sionval
               else
                   sionz=0.0d0
               endif
           endif
c-----------------------------------------------------------------------
c                        substitute special ion data
c-----------------------------------------------------------------------
           if(ionip.gt.0.and.ivdisp.gt.0)then
               if(densp.gt.0.0d0)then
                   if(lstbp(n).eq.1)then
                       if(lpstbp.eq.n)pstbp=stbp(n)/sionp
                       sionp=stbp(n)
                   endif
               endif
               if(denimp.gt.0.0d0)then
                   if(lstbz(n).eq.1)then
                       if(lpstbz.eq.n)pstbz=stbz(n)/sionz
                       sionz=stbz(n)
                   endif
               endif
           endif
           ai=ai*(sion+(densp*sionp+denimp*sionz)/dens)/secip
       endif

       if(ys-cut)223,223,220
  220  x=ys-ate*en2(n)
       if(x-cut)222,222,221
  221  x1=0.0d0
       x2=0.0d0
       go to 224
  222  x1=dexp(-x)/(ys+1.0d0)
       x2=x1*dexp(-ye)
       go to 224
  223  call photo(pion,prec,pstim,z,te,ts,enc,1,0,1)
       x1=dexpte(n)*pion
       x2=dexpte(n)*pstim
  224  a(n)=a(n)+0.43171d0*rho*ai*en*en*enc*enc+0.5d0*w2*en3(n)*x1
       cionpt(i)=0.43171d0*rho*ai*en*en*enc*enc
       ads=0.0d0
       if(n-nmin)303,303,302
  302  do 301 j=1,jmax
       f=fij(j)
       eij=epsil(j)
       weijj=weij(j)
       if(f-0.000001d0)301,301,300
  300  call diel(z,eij,f,weijj,cor,jcor,n,ad)
       expon=(z+1.0d0)*(z+1.0d0)*eij*157890.0d0/te-ate*en2(n)
       ads=ads+ad*(eeij(j)-weijj*(1.0d0-eeij(j)))*dexpte(n)
       a(n)=a(n)+ad*weijj*dexpte(n)
c  evaluate dielectronic recombination power coefficient
       if(expon.le.cut)then
           rad=(((z+1.0d0)/z)**2*eij-en2(n))*ad*dexp(-expon)
       else
           rad=0.0d0
       endif
       rdrep(i)=rdrep(i)+rad
  301  continue
       rdrep(i)=cpl*rdrep(i)
  303  continue
       call photo(pion,prec,pstim,z,te,ts,enc,0,1,0)
       x3=dexpte(n)*prec
       call rphoto(ate,en,rrec)
       rx3=0.5d0*en3(n)*rrec/ate
       rrrep(i)=cpl*rx3
       x4=qthrep(i)
       x4=9.62134d12*densh*x4/(dens*z*ate**1.5d0)
       rhs(i)=rhs(i)+0.5d0*en3(n)*(w*x2-w2*x1 +x3)+ads+x4
       drecpt(i)=ads
       rrecpt(i)=0.5d0*en3(n)*x3
       xrecpt(i)=x4
       prep(i)=0.0d0
       if(n-nmin)26,26,17
   17  n2=n-1
       do 25 n1=nmin,n2
       w2=w
       if(n1-nmin)53,53,54
   53  w2=wb(n)
   54  continue
       d=en2(n1)-en2(n)
       y=ate*d
       ys=ats*d
       d=1.0d0/d
       en1=n1
       x2=en2(n)
       n3=n-n1
       x=en23(n1)*en23(n3)/en23(n-1)
       g=gbb(en,en1,x2,x)
       a1=en3(n)*en3(n1)*d*g
c-----------------------------------------------------------------------
c  impact excitation data
c-----------------------------------------------------------------------
       ei=en2(n1)
       wi=2.0d0*en1*en1
       ej=en2(n)
       wj=2.0d0*en*en
       f=1.96028d0*g*d*d*d*en3(n1)*en2(n1)*en3(n)
       phi=f*d/(z*z)
c-----------------------------------------------------------------------
c      basic electron data  - van regemorter or percival-richards
c-----------------------------------------------------------------------
       if(iprs.le.0)then
           call pyvr(y,z,py)
       else
           call pypr(ei,ej,n1,n,1.0d0,z,phi,wi,wj,te,intd,py,rdexc)
       endif
c-----------------------------------------------------------------------
c                         substitute impact parameter if requested
c-----------------------------------------------------------------------
       ntest=n-n1
       if(ntest.le.nip)then
           py=pymat(n1,ntest,1)
       endif
       pyd=py
       pyu=py
c-----------------------------------------------------------------------
c                         substitute special low level data if available
c                         nb. excitation and deexcitation coeffts.
c                             separately supplied
c-----------------------------------------------------------------------
       if(ilow.gt.0.and.n1.le.ndlow.and.n.le.ndlow)then
           if(lxtbe(n1,n).eq.1)then
               pyu=xtbe(n1,n)/
     &             (3.15d-7*dsqrt(1.57890d5/te)*phi*
     &             dexpte(n)/dexpte(n1))
               pyd=xtbe(n,n1)/
     &             (3.15d-7*dsqrt(1.57890d5/te)*phi*
     &             (en1/en)**2)
            endif
       endif
c-----------------------------------------------------------------------
c      basic ion  data      - vainshtein or lodge-percival-richards
c                             nb. excitation and deexcitation coeffts.
c                                 must be supplied separately
c-----------------------------------------------------------------------
       pypu=0.0d0
       pypd=0.0d0
       pyimpu=0.0d0
       pyimpd=0.0d0
       if(ionip.eq.1)then
           if(ilprs.le.0)then
               if(densp.gt.0.0d0.and.ntest.le.nionip)then
                   pypu=rqvnew(z,n1,n,phi,1.0d0,embeam,tp,vdisp)/
     &                  (3.15d-7*dsqrt(1.57890d5/tp)*phi*
     &                  dexpte(n)/dexpte(n1))
                   pypd=rqvnew(z,n,n1,phi,1.0d0,embeam,tp,vdisp)/
     &                  (3.15d-7*dsqrt(1.57890d5/tp)*phi*
     &                  (en1/en)**2)
               else
                   pypu=0.0d0
                   pypd=0.0d0
               endif
               if(denimp.gt.0.0d0.and.ntest.le.nionip)then
                   pyimpu=0.0d0
                   pyimpd=0.0d0
                   do 44 imp=1,nimp
                    if(frimpa(imp).eq.0.0d0)go to 44
                    pyimpu=pyimpu+frimpa(imp)*rqvnew(z,n1,n,phi,
     &                     zimpa(imp),amimpa(imp),tp,vdisp)/
     &                     (3.15d-7*dsqrt(1.57890d5/tp)*phi*
     &                     dexpte(n)/dexpte(n1))
                    pyimpd=pyimpd+frimpa(imp)*rqvnew(z,n,n1,phi,
     &                     zimpa(imp),amimpa(imp),tp,vdisp)/
     &                     (3.15d-7*dsqrt(1.57890d5/tp)*phi*
     &                     (en1/en)**2)
   44              continue
               else
                   pyimpu=0.0d0
                   pyimpd=0.0d0
               endif
           else
               if(densp.gt.0.0d0.and.ntest.le.nionip)then
                   pypu=rqlnew(z,n1,n,phi,1.0d0,embeam,tp,vdisp)/
     &                  (3.15d-7*dsqrt(1.57890d5/tp)*phi*
     &                  dexpte(n)/dexpte(n1))
                   pypd=rqlnew(z,n,n1,phi,1.0d0,embeam,tp,vdisp)/
     &                  (3.15d-7*dsqrt(1.57890d5/tp)*phi*
     &                  (en1/en)**2)
               else
                   pypu=0.0d0
                   pypd=0.0d0
               endif
               if(denimp.gt.0.0d0.and.ntest.le.nionip)then
                   pyimpu=0.0d0
                   pyimpd=0.0d0
                   do 45 imp=1,nimp
                    if(frimpa(imp).eq.0.0d0)go to 45
                    pyimpu=pyimpu+frimpa(imp)*rqlnew(z,n1,n,phi,
     &                     zimpa(imp),amimpa(imp),tp,vdisp)/
     &                     (3.15d-7*dsqrt(1.57890d5/tp)*phi*
     &                     dexpte(n)/dexpte(n1))
                    pyimpd=pyimpd+frimpa(imp)*rqlnew(z,n,n1,phi,
     &                     zimpa(imp),amimpa(imp),tp,vdisp)/
     &                     (3.15d-7*dsqrt(1.57890d5/tp)*phi*
     &                     (en1/en)**2)
   45              continue
               else
                   pyimpu=0.0d0
                   pyimpd=0.0d0
               endif
           endif
c-----------------------------------------------------------------------
c                         substitute special low level data if available
c                         nb. excitation and deexcitation coeffts.
c                             supplied separately
c-----------------------------------------------------------------------
           if(ilow.gt.0.and.n1.le.ndlow.and.n.le.ndlow.and.
     &                             ivdisp.eq.1)then
               if(densp.gt.0.0d0)then
                   if(lxtbp(n1,n).eq.1)then
                       pypu=xtbp(n1,n)/
     &                      (3.15d-7*dsqrt(1.57890d5/tp)*phi*
     &                      dexpte(n)/dexpte(n1))
                       pypd=xtbp(n,n1)/
     &                      (3.15d-7*dsqrt(1.57890d5/tp)*phi*
     &                      (en1/en)**2)
                   endif
               endif
               if(denimp.gt.0.0d0)then
                   if(lxtbz(n1,n).eq.1)then
                       pyimpu=xtbz(n1,n)/
     &                        (3.15d-7*dsqrt(1.57890d5/tp)*phi*
     &                        dexpte(n)/dexpte(n1))
                       pyimpd=xtbz(n,n1)/
     &                        (3.15d-7*dsqrt(1.57890d5/tp)*phi*
     &                        (en1/en)**2)
                   endif
               endif
           endif
       endif
       pyu=pyu*rho+pypu*rhop+pyimpu*rhoimp
       pyd=pyd*rho+pypd*rhop+pyimpd*rhoimp
       if(ys-0.001d0)225,225,226
  225  a2=w2/(ys+0.5d0*ys*ys)
       go to 231
  226  if(dexpts(n1))227,227,230
  227  if(ys-cut)229,228,228
  228  a2=0.0d0
       go to 231
  229  a2=w2/(dexp(ys)-1.0d0)
       go to 231
  230  a2=w2/(dexpts(n1)/dexpts(n)-1.0d0)
  231  a3u=3.06998d0*d*d*d*pyu
       a3d=3.06998d0*d*d*d*pyd
       a(n1)=-a1*(a2*dexpte(n1)+a3u*dexpte(n))
       a(n)=a(n)+a1*(1.0d0+a2+a3d)*dexpte(n)
       rhs(i)=rhs(i)-a1*(dexpte(n)  +a2*(dexpte(n)-dexpte(n1)))
       ra=en3(n)*en3(n1)*g
       prep(i)=prep(i)+ra
   25  continue
       prep(i)=cpl*dexpte(n)*prep(i)
   26  do 27 j=1,imax
   27  ared(i,j)=0.0d0
       do 29 n=nmin,nmax
       j=inter(n)
       ared(i,j-1)=ared(i,j-1)+agrl(n,1)*a(n)
       ared(i,j)=ared(i,j)+agrl(n,2)*a(n)
       if(j-imax)28,29,29
   28  ared(i,j+1)=ared(i,j+1)+agrl(n,3)*a(n)
   29  continue
       if(i.gt.2)rlrep(i-2)=rlrep(i-2)+prep(i)*dgs2(i-2)
       if(i.gt.1)rlrep(i-1)=rlrep(i-1)+prep(i)*dgs1(i-1)
       rlrep(i)=rlrep(i)+prep(i)*dg(i)
       if(i.lt.imax-1)rlrep(i+2)=rlrep(i+2)+prep(i)*dgs2(i)
       if(i.lt.imax)rlrep(i+1)=rlrep(i+1)+prep(i)*dgs1(i)
       rl=rl+prep(i)*vec(i)
       rd=rd+rdrep(i)*vec(i)
       rr=rr+rrrep(i)*vec(i)
   30  continue
c-----------------------------------------------------------------------
c  initiate call to cldlbn2 to obtain projection onto low levels
c
c        bypass call if lproj =.false.
c-----------------------------------------------------------------------
       z1=iz
       iecion=0
       iedrec=0
       ierrec=0
       iexrec=0
       if(lproj)then
           z1=z
           acnst=1.03928d-13*z*ate*dsqrt(ate)
           a1cnst=6.60074d-24*(157890.0d0/te)**1.5d0
           iecion=0
           iedrec=0
           ierrec=0
           iexrec=0
           if(exfile.ne.' ')then
               call cldlbn2(exfile,z0,z1,zeff,dens,te,densp,tp,bmener,
     &         densh,w1,nmin,nmax,nrep,imax,ared,rhs,
     &         cionpt,drecpt,rrecpt,xrecpt,iecion,iedrec,ierrec,iexrec,
     &         dvec,acnst,a1cnst, lpass)
           endif
       endif
c-----------------------------------------------------------------------
c  resume principal calculation
c-----------------------------------------------------------------------
       call matinv(ared,imax,rhs,2,determ)
       c=6.60074d-24*dens*(157890.0d0/te)**1.5d0
       do 31 i=1,imax
       rl=rl+rlrep(i)*rhs(i)
       n=nrep(i)
       if(i-1)601,601,600
  600  idcn=inter(n-1)
       cn1=rhs(idcn-1)
       cn2=rhs(idcn)
       if(imax-idcn)603,603,602
  602  cn3=rhs(idcn+1)
       go to 604
  603  cn3=0.0d0
  604  continue
       if(i-idcn)605,606,605
  605  delcn(i)=-cn1*agrl(n-1,1)-cn2*agrl(n-1,2)-cn3*
     &          (agrl(n-1,3)-1.0d0)
       go to 601
  606  delcn(i)=(cn2-cn1)*agrl(n-1,1)+(cn2-cn3)*agrl(n-1,3)
  601  continue
       delcn(1)=0.0d0
       brep(i)=1.0d0+rhs(i)
       dexpbn(i)=dexpte(n)*brep(i)
       en=n
       pop(i)=c*en*en*dexpbn(i)
   31  continue
       rb=cpl*0.25d0*ngffmh(ate)/(ate*ate)
       c=1.03928d-13*z*ate*dsqrt(ate)
       alfa=c*brep(1)/ared(1,1)
       s=alfa/pop(1)

       return
c
c----------------------------------------------------------------------
c
 1001 format(1x,31('*'),' cgbnhs error ',30('*')//
     &       1x,'Incompatible internal parameters: ',a,i2,a,i2)
 1002 format(/1x,29('*'),' Program terminated ',29('*'))
c
c-----------------------------------------------------------------------
c

      end
