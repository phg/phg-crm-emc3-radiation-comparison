       subroutine cgbntb ( ndtem  , ndden  , ndrep  , ndcor , nddiel ,
     &                     nddef  , ndimp  , ndlev  , ndein , ndzef  ,
     &                     iz0    , iz1    ,
     &                     exfile , cxfile ,
     &                     dens   , densp  , denimp ,
     &                     te     , tp     , timp   ,
     &                     zeff   ,
     &                     bmener , densh  , bmfr   ,
     &                     nimp   , zimpa  , amimpa , frimpa,
     &                     ts     , w      , w1     ,
     &                     cion   , cpy    , nip    , intd  , iprs   ,
     &                     ilow   , ionip  , nionip , ilprs , ivdisp ,
     &                     nmin   , nmax   , imax   , nrep  , wbrep  ,
     &                     jdef   , def    ,
     &                     jcor   , cor    , jmax   , epsil , fij    ,
     &                     wij    ,
     &                     f1     , f2     , f3     ,
     &                     bncalc , bnact  , expsh  ,
     &                     alpha  , hiss   , xpop   ,
     &                     plt    , prb    , prc
     &                   )
       implicit none
c-----------------------------------------------------------------------
c
c  ********************* fortran 77 subroutine: cgbntb *****************
c
c  purpose: executes a set of four connected bundle-n population
c           calculation and produce separated population parts due to
c           excitation, free electron recombination and charge
c           recombination from a neutral beam
c
c
c  subroutine:
c
c   input : (i*4)  ndtem    = maximum number of electron temperatures
c   input : (i*4)  ndden    = maximum number of electron densities
c   input : (i*4)  ndrep    = maximum number of representative n-shells
c   input : (i*4)  ndcor    = maximum number of DR bethe corrections
c   input : (i*4)  nddiel   = maximum number of DR core transitions
c   input : (i*4)  nddef    = maximum number of quantum defects
c   input : (i*4)  ndimp    = maximum number of plasma impurities
c   input : (i*4)  ndlev    = maximum number of n-shells
c   input : (i*4)  ndein    = maximum number of beam energies
c   input : (i*4)  ndzef    = maximum number of z effectives
c
c   input : (i*4)  iz0      = nuclear charge of bundle-n ion
c   input : (i*4)  iz1      = recombining ion charge of bundle-n ion
c   input : (c*80) cxfile   = file name for charge exchange data input
c   input : (c*80) exfile   = file name for map of proj. matrix output
c
c   input : (i*4)  dens     = plasma electron density (cm-3)
c   input : (i*4)  densp    = plasma H+ density (cm-3)
c   input : (i*4)  denimp   = plasma mean impurity ion density (cm-3)
c   input : (i*4)  te       = plasma electron temp. (K)
c   input : (i*4)  tp       = plasma H+ temp. (K)
c   input : (i*4)  timp     = plasma mean impurity ion temp (K)
c   input : (i*4)  zeff     = plasma zeff
c   input : (i*4)  bmener   = beam energy (ev/amu)
c   input : (i*4)  densh    = beam H+ density (cm-3)
c   input : (i*4)  bmfr     = fractions of beam at given energy
c
c   input : (i*4)  nimp     = number of plasma impurities (excl.h+)
c   input : (r*8)  zimpa()  = impurity species charge
c                             1st dim: index of impurity
c   input : (r*8)  amimpa() = atomic mass number of impurity species
c                             1st dim: index of impurity
c   input : (r*8)  frimpa() = fraction of impurity (normalised to 1)
c                             1st dim: index of impurity
c
c   input : (r*8)  ts       = external radiation field temperature (K)
c   input : (r*8)  w        = general radiation dilution factor
c   input : (i*4)  w1       = external radiation field dilution factor
c                             for photo-ionisation form the ground level.
c
c   input : (r*8)  cion     = adjustment multiplier for ground ionis.
c   input : (r*8)  cpy      = adjustment multiplier for VR xsects.
c   input : (i*4)  nip      = range of delta n for IP xsects. (le.4)
c   input : (i*4)  intd     = order of Maxw. quad. for IP xsects.(le.3)
c   input : (i*4)  iprs     = 0  => default to VR xsects. beyond nip range
c                             1  => use PR xsects. beyond nip range
c   input : (i*4)  ilow     = 0  => no special low level data accessed
c                             1  => special low level data accessed
c   input : (i*4)  ionip    = 0 =>   no ion impact collisions included
c                             1 =>ion impact excit. and ionis. included
c   input : (i*4)  nionip   = range of delta n for ion impact
c                             excitation xsects.
c   input : (i*4)  ilprs    = 0 => default to vainshtein xsects.
c                             1 => use lodge-percival-richards xsects.
c   input : (i*4)  ivdisp   = 0 => ion impact at thermal Maxw. energies
c                             1 => ion impact at displaced thermal
c                                  energies according to the neutral
c                                  beam energy parameter
c                               * if(ivdisp=0 then special low level
c                                 data for ion impact is not substituted -
c                                 only vainshtein and lodge et al.
c                                 options are open.  Electron impact
c                                 data substitution does occur.
c   input : (i*4)  nmin     = lowest n-shell for population structure
c   input : (i*4)  nmax     = highest n-shell for population structure
c   input : (i*4)  imax     = number of representative n-shells
c   input : (i*4)  nrep()   = representative n-shells
c                             1st dim: index of representative n-shell
c   input : (r*8)  wbrep()  = dilution factors for nmin->nrep() trans.
c                             1st dim: index of representative n-shell
c   input : (i*4)  jdef     = number of n-shell quantum defects
c   input : (r*8)  def()    = quantum defects for n-shells
c                             1st dim: index of n-shell quantum defects
c                                      upwards from nmin
c   input : (i*4)  jcor     = number of DR Bethe correction factors
c   input : (r*8)  cor()    = DR Bethe correction factors
c                             1st dim: index of correction factor
c   input : (i*4)  jmax     = number of DR core transitions
c   input : (r*8)  epsil()  = reduced energy of core transition
c                             [delta Eij/I_H=(z+1)^2*epsil()]
c                             1st dim: index of DR core transition
c   input : (r*8)  fij()    = absorption oscillator strength for
c                             DR core transition
c                             1st dim: index of DR core transition
c   input : (r*8)  wij()    = dilution factor for DR core transition
c                             1st dim: index of DR core transition
c
c   output: (r*8)  f1()     = excitation part of b-factor
c                             1st dim: index of representative n-shell
c   output: (r*8)  f2()     = free electron recom. part of b-factor
c                             1st dim: index of representative n-shell
c   output: (r*8)  f3()     = cx recom. part of b-factor
c                             1st dim: index of representative n-shell
c   output: (r*8)  bncalc() = recomputed b-factor from f's
c                             1st dim: index of representative n-shell
c   output: (r*8)  bnact()  = actual b-factor from loop 4
c                             1st dim: index of representative n-shell
c   output: (r*8)  expsh()  = Saha exponential factor = Nn/(NeN+bn)
c                             1st dim: index of representative n-shell
c   output: (r*8)  alpha()  = coll.- rad. recom. coefft.
c                             (1 = cx. off; 2 = cx. on)
c   output: (r*8)  hiss()   = coll.- rad. ionis. coefft.
c                             (1 = cx. off; 2 = cx. on)
c   output: (r*8)  xpop()   = ground population N1/(NeN+)
c                             (1 = cx. off; 2 = cx. on)
c   output: (r*8)  plt      = low level line power coeff. (W cm3)
c   output: (r*8)  prb      = recom./brems. power coeff.(W cm3)
c   output: (r*8)  prc      = cx. recom. power coeff. (W cm3)
c
c  routines:
c
c     routine    source    brief description
c     -------------------------------------------------------------
c     cgbnhs     adas      executes a bundle-n population calc.
c
c
c  author   : Hugh Summers
c  date     : 10-05-2007
c
c
c  Version : 1.1
c  Date    : 10-05-2007
c  Modified: Hugh Summers
c            - First version.
c
c  Version : 1.4
c  Date    : 15-07-2009
c  Modified: Martin O'Mullane
c            - Increase cxfile and exfile to 120 characters.
c
c  Version : 1.5
c  Date    : 20-03-2019
c  Modified: Martin O'Mullane
c            - Save densh_sav and w1_sav between calls.
c
c-----------------------------------------------------------------------
       integer   idrep   , idlev
c-----------------------------------------------------------------------
       parameter (idrep = 30 , idlev = 550)
c-----------------------------------------------------------------------
       integer   i4unit
       integer   ndtem   , ndden  , ndrep  , ndcor  , nddiel ,
     &           nddef   , ndimp  , ndlev  , ndein  , ndzef
       integer   iz0     , iz1    ,
     &           nimp    ,
     &           nip     , intd   , iprs   , ilow   , ionip  , ilprs  ,
     &           ivdisp  , nmin   , nmax   , imax   , jdef   , jcor   ,
     &           jmax    , nionip
       integer   iposnt  , i      , n
c-----------------------------------------------------------------------
       real*8    ts      , w      , w1     , cion   , cpy
       real*8    w1_sav  , densh_sav
       real*8    z0      ,  z1    ,
     &           dens    , densp  , denimp ,
     &           te      , tp     , timp   ,
     &           bmener  , densh  , bmfr   ,
     &           zeff
       real*8    alfa    , s      ,
     &           rl      , rr     , rd     , rb     , rlrb    , rllt   ,
     &           rlrc
       real*8    plt     , prb    , prc
       real*8    en
c-----------------------------------------------------------------------
       character exfile*120       , cxfile*120
c-----------------------------------------------------------------------
       logical   lproj
c-----------------------------------------------------------------------
       integer   nrep(ndrep+1)
c-----------------------------------------------------------------------
       real*8    zimpa(ndimp)     , amimpa(ndimp)   , frimpa(ndimp)    ,
     &           wbrep(ndrep)     , def(nddef)      ,
     &           cor(ndcor)       , epsil(nddiel)   , fij(nddiel)      ,
     &           wij(nddiel)
       real*8    dexpte(ndlev)
       real*8    f1(ndrep)        , f2(ndrep)       , f3(ndrep)       ,
     &           bncalc(ndrep)    , bnact(ndrep)    , expsh(ndrep)
       real*8    xpop(2)
       real*8    alpha(2),hiss(2)
       real*8    brep(idrep)      , dexpbn(idrep)   , pop(idrep)
c-----------------------------------------------------------------------
       save      densh_sav        , w1_sav
c-----------------------------------------------------------------------
c  initialise
c-----------------------------------------------------------------------

        if(idrep.ne.ndrep) then
            write(i4unit(-1),1003) 'idrep',idrep,'ndrep',ndrep
            write(i4unit(-1),1002)
            stop
        endif
        if(idlev.ne.ndlev) then
            write(i4unit(-1),1003) 'idlev',idlev,'ndlev',ndlev
            write(i4unit(-1),1002)
            stop
        endif

         z0        = iz0
         z1        = iz1
         w1_sav    = w1
         densh_sav = densh

         do i=1,3
            f1(i)=0.00d+00
            f2(i)=0.00d+00
            f3(i)=0.00d+00
            bncalc(i)=0.00d+00
            bnact(i)=0.00d+00
          enddo
          xpop(1)=0.00d+00
          xpop(2)=0.00d+00


       do 100 iposnt=1,4
c--------------------------------------------------
c   activate control parameters according to iposnt
c--------------------------------------------------

         if(iposnt.eq.2.or.iposnt.eq.4)then
            lproj = .true.
         else
            lproj = .false.
         endif

         if(w1_sav.eq.0.0d0) then
           write(i4unit(-1),1001)'w1'
           write(i4unit(-1),1002)
           stop
         endif

         if((iposnt.eq.1).or.(iposnt.eq.3)) then
            w1=w1_sav
         else
            w1=0.0d0
         endif

         if (densh_sav.eq.0.0d0) then
           write(i4unit(-1),1001)'densh'
           write(i4unit(-1),1002)
           stop
         endif

         if((iposnt.eq.3).or.(iposnt.eq.4)) then
            densh=densh_sav
         else
            densh=0.0d0
         endif

         call cgbnhs( ndrep  , ndcor  , nddiel , nddef , ndimp   ,
     &                ndlev  , ndein  ,
     &                z0     , cxfile , exfile ,
     &                lproj  ,
     &                dens   , densp  , te    , tp     ,
     &                ts     , w      , z1    , cion   , cpy     ,
     &                w1     , nip    , intd  , iprs   , ilow    ,
     &                ionip  , nionip , ilprs , ivdisp , zeff    ,
     &                nimp   ,
     &                zimpa  , amimpa , frimpa, denimp ,
     &                bmener , densh  ,
     &                nmin   , nmax   , imax  ,
     &                nrep   , wbrep  ,
     &                jcor   , cor    ,
     &                jmax   , epsil  , fij   , wij    ,
     &                jdef   , def    ,
     &                brep   , dexpbn , pop   , dexpte ,
     &                alfa   , s      ,
     &                rl     , rr     , rd    , rb
     &               )
       do i=1,imax
         if(iposnt.eq.1)f2(i)=brep(i)
         if(iposnt.eq.2)f1(i)=(brep(i)-f2(i))/pop(1)
         if(iposnt.eq.3)f3(i)=(brep(i)-f2(i))/(densh/dens)
         if(iposnt.eq.4)then
             bncalc(i)=f1(i)*pop(1)+f2(i)+f3(i)*(densh/dens)
             bnact(i)=brep(i)
         endif
       enddo

       if(iposnt.eq.1) then
           rlrb=rl
           prb=(rl+rr+rd+rb)/dens
       endif
       if(iposnt.eq.2) then
           xpop(2)=pop(1)
           rllt=rl-rlrb
           plt = rllt/dens
           alpha(2)=alfa
           hiss(2)=s
       endif
       if(iposnt.eq.3) then
           rlrc=rl-rlrb
           prc = rlrc/densh
       endif
       if(iposnt.eq.4)then
           xpop(1)=pop(1)
           alpha(1)=alfa
           hiss(1)=s
       endif
  100  continue

       w1    = w1_sav
       densh = densh_sav

       do i=1,imax
         n=nrep(i)
         en=n
         expsh(i)=dexpte(n)*en*en*dens*4.14131d-16/(te**1.5d0)
       enddo

       return
c
c----------------------------------------------------------------------
c
 1001 format(1x,30('*'),'  cgbntb error  ',30('*')//
     &       1x,'for loop operation parameter: ',a,' must be non-zero')
 1002 format(/1x,29('*'),' program terminated ',29('*'))
 1003 format(1x,30('*'),'  cgbntb error  ',30('*')//
     &       1x,'internal param. ',1a5,' = ',i4,' .ne. ',1a5,' = ',i4)
c
c-------------------------------------------------------------------------
c

      end
