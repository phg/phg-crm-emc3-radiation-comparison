       subroutine cgwr12( iunit  , dsname , user   , date  ,
     &                    ndtem  , ndden  , ndrep  , 	   
     &                    ndimp  , ndlev  , ndein , ndzef  , ndwvl  ,	  
     &                    iz0	 , iz1    , 		     	
     &                    ndens  , id_ref , densa  ,
     &                    deniona,				     	
     &                    ntemp  , it_ref ,	
     &                    tiona  ,				     	
     &                    nzef   , iz_ref , zefa   ,		     	
     &                    nbeam  , ib_ref , bmena  , 	
     &                    nimp   , zimpa  , amimpa, frimpa ,	
     &                    ts	 , w	  , w1     ,		     	
     &                    cion   , cpy    , nip    , intd  , iprs   ,	
     &                    ilow   , ionip  , nionip , ilprs , ivdisp ,	
     &                    nmin   , nmax   , imax   , nrep  ,	
     &                    p3a_ib , p3a_it , p3a_id , p3a_iz,          	
     &                    nwvl   , wvmin  , wvmax  , emis_thres	             	
     &                  )					     	
       implicit none
c-----------------------------------------------------------------------
c
c  ***************** fortran77 subroutine: cgwr12 **********************
c
c  purpose:  To write an adf12 dataset from the bundle-n model.
c
c  Notes:     
c
c  Subroutine:
c
c   input : (i*4)  iunit    = unit number for output adf12 file.
c   input : (c*80) dsname   = file name of adf25 format read.
c   input : (c*30) user     = name of producer.
c   input : (c*8)  date     = date of run.
c
c   input : (i*4)  ndtem    = maximum number of electron temperatures
c   input : (i*4)  ndden    = maximum number of electron densities
c   input : (i*4)  ndrep    = maximum number of representative n-shells
c   input : (i*4)  ndimp    = maximum number of plasma impurities
c   input : (i*4)  ndlev    = maximum number of n-shells
c   input : (i*4)  ndein    = maximum number of beam energies
c   input : (i*4)  ndzef    = maximum number of z effectives
c   input : (i*4)  ndwvl    = maximum number of wavelength intervals
c
c   input : (i*4)  iz0      = nuclear charge of bundle-n ion
c   input : (i*4)  iz1      = recombining ion charge of bundle-n ion
c
c   input : (i*4)  ndens    = number of electron densities
c   input : (i*4)  id_ref   = reference electron density pointer in vectors
c   input : (i*4)  densa()  = plasma electron density vector (cm-3)
c                             1st dim: index of electron density
c   input : (i*4)  denpa()  = plasma H+ density vector (cm-3)
c                             1st dim: index of electron density
c   input : (i*4)  deniona()= total ion density (plasma+impurity) (cm-3)
c                             1st dim: index of electron density
c
c   input : (i*4)  ntemp    = number of electron temperatures
c   input : (i*4)  it_ref   = reference electron temp. pointer in vectors
c   input : (i*4)  tiona()  = mean ion temp (plasma+impurity) (K)
c                             1st dim: index of electron temperature
c
c   input : (i*4)  nzef     = number of plasma zeff
c   input : (i*4)  iz_ref   = reference zeff pointer in vector
c   input : (i*4)  zefa()   = plasma zeff vector 
c                             1st dim: index of zeff
c
c   input : (i*4)  nbeam    = number of beam energies
c   input : (i*4)  ib_ref   = reference beam energy pointer in vectors
c   input : (i*4)  bmena()  = beam energy vector (ev/amu)
c                             1st dim: index of beam energies
c
c   input : (i*4)  nimp     = number of plasma impurities (excl.h+)         
c   input : (r*8)  zimpa()  = impurity species charge
c                             1st dim: index of impurity
c   input : (r*8)  amimpa() = atomic mass number of impurity species
c                             1st dim: index of impurity
c   input : (r*8)  frimpa() = fraction of impurity (normalised to 1)
c                             1st dim: index of impurity
c
c   input : (r*8)  ts       = external radiation field temperature (K)
c   input : (r*8)  w        = general radiation dilution factor
c   input : (i*4)  w1       = external radiation field dilution factor 
c                             for photo-ionisation form the ground level.
c
c   input : (r*8)  cion     = adjustment multiplier for ground ionis.
c   input : (r*8)  cpy      = adjustment multiplier for VR xsects.
c   input : (i*4)  nip      = range of delta n for IP xsects. (le.4)
c   input : (i*4)  intd     = order of Maxw. quad. for IP xsects.(le.3)
c   input : (i*4)  iprs     = 0  => default to VR xsects. beyond nip range
c                             1  => use PR xsects. beyond nip range
c   input : (i*4)  ilow     = 0  => no special low level data accessed
c                             1  => special low level data accessed
c   input : (i*4)  ionip    = 0 =>   no ion impact collisions included
c                             1 =>ion impact excit. and ionis. included
c   input : (i*4)  nionip   = range of delta n for ion impact 
c                             excitation xsects.
c   input : (i*4)  ilprs    = 0 => default to vainshtein xsects.
c                             1 => use lodge-percival-richards xsects.
c   input : (i*4)  ivdisp   = 0 => ion impact at thermal Maxw. energies
c                             1 => ion impact at displaced thermal 
c                                  energies according to the neutral 
c                                  beam energy parameter
c                               * if(ivdisp=0 then special low level 
c                                 data for ion impact is not substituted -
c                                 only vainshtein and lodge et al. 
c                                 options are open.  Electron impact
c                                 data substitution does occur.
c   input : (i*4)  nmin     = lowest n-shell for population structure
c   input : (i*4)  nmax     = highest n-shell for population structure
c   input : (i*4)  imax     = number of representative n-shells
c   input : (i*4)  nrep()   = representative n-shells
c                             1st dim: index of representative n-shell
c
c   input : (r*8)  p3a_ib(,)= cx part of population
c                             1st dim: index of representative level
c                             2nd dim: index of beam energy scan
c   input : (r*8)  p3a_it(,)= cx part of population
c                             1st dim: index of representative level
c                             2nd dim: index of ion temperature scan
c   input : (r*8)  p3a_id(,)= cx part of population
c                             1st dim: index of representative level
c                             2nd dim: index of ion density scan
c   input : (r*8)  p3a_iz(,)= cx part of population  
c                             1st dim: index of representative level
c                             2nd dim: index of zeff scan
c   input : (i*4)  nwvl     = number of wavln. ranges for line inclusion
c   input : (r*8)  wvmin()  = lower bound of wavelength range (A)
c                              1st dim: index of wavelength range
c   input : (r*8)  wvmax()  = upper bound of wavelength range (A)
c                              1st dim: index of wavelength range
c   input : (r*8)  emis_thres=threshold emissivity for line inclusion	             	
c
c  Routines:
c
c     routine    source    brief description
c     -------------------------------------------------------------
c     xxcase	 adas	   convert string to upper or lower case
c     xxslen     adas      locate first and last char. of string
c     xxordr     adas      rank vector with forward/reverse indices  
c     xxeiam     adas      obtain elemnt atomic mass number  
c     xfesym     adas      obtain element symbol  
c     xxrams     adas      set rydberg constant in atomic constants  
c     r8const    adas      obtain atomic constants  
c     xxrmve     adas      remove selected characters from a string  
c     cxvcwr     adas      format vector output lines with title  
c
c
c  author   : Hugh Summers
c  date     : 15-06-2007
c
c  Version : 1.1
c  Date    : 15-06-2007
c  Modified: Hugh Summers
c            - First version.
c
c  Version : 1.2
c  Date    : 17-11-2007
c  Modified: Martin O'Mullane
c            - Correct calculation of transition probability.
c
c  Version : 1.4
c  Date    : 15-07-2009
c  Modified: Martin O'Mullane
c            - Increase dsname to 120 characters.
c
c-----------------------------------------------------------------------
       integer   idlev  , idtran   , idvec
       integer   idtem  , idden    , idein  , idzef
       integer   nbmag
c-----------------------------------------------------------------------
       real*8    c1     , c2       ,  c3    , c4       ,  c5
       real*8    ev     , bmag                
c-----------------------------------------------------------------------
       parameter (idlev = 550 , idtran = 5000 , idvec = 60)
       parameter (idtem  = 35 , idden  = 35 , idein   = 30 , idzef = 10)
       parameter (nbmag = 1 )
       parameter (c1 = 6432.8D0     , c2 = 2949810.0D0  ,
     &            c3 = 25540.0D0    , c4 = 146.0D0      ,
     &            c5 = 41.0D0       )
       parameter (ev = 11605.4D0    , bmag = 3.0D0)
c-----------------------------------------------------------------------
       integer    i4unit  , iunit
       integer    ndtem   , ndden  , ndrep  , 
     &            ndimp   , ndlev  , ndein  , ndzef  , ndwvl
       integer    iz0     , iz1    , ndens  , id_ref , ntemp  , it_ref ,
     &            nzef    , iz_ref , nbeam  , ib_ref , nimp   ,
     &            nip     , intd   , iprs   , ilow   , ionip  , ilprs  ,
     &            ivdisp  , nmin   , nmax   , imax   , nionip 
       integer    ifirst  , ilast
       integer    ib      , it     , id     , iz     , imp
       integer    i       , i1     , i2     , i3     , icount ,  
     &            n       , n1     , nmax1  , ic     , iw 
       integer    nwvl    , idtran_new    
       integer    l1      , l2
c-----------------------------------------------------------------------
       real*8     r8const , finter  
       real*8     ts      , w      , w1     , cion   , cpy    , z1
       real*8     rz      , en     , en1    , delta  , sig2   , rf     ,
     &            wavair  , d      , g      , gbb    , x      , x1     ,
     &            x2      , x3     , a1     , p3_ref , emis   , tiev
       real*8     xsym             
       real*8     emis_thres    , aval
c-----------------------------------------------------------------------
       character  xfesym*2
       character  dsname*120 , c10*10
       character  amsr*7     , esym*2
       character  c80*80     , cline*80
       character  cdash*80   , cblanks*80   , filnam*10
       character  crcvr*5    , chr5*5       , ctran*7 , c7*7
       character  user*30    , date*8
c-----------------------------------------------------------------------
       logical    lup        , lwv_incl
c-----------------------------------------------------------------------
       integer    nrep(ndrep+1)
       integer    inter(idlev)
       integer    na(idtran) , n1a(idtran)
       integer    ia(idtran) , iinva(idtran)
c-----------------------------------------------------------------------
       real*8     densa(ndden)     , 
     &            deniona(ndden)   ,
     &            tiona(ndtem)     ,
     &            zefa(ndzef)      , bmena(ndein)    ,
     &            zimpa(ndimp)     , amimpa(ndimp)   ,
     &            frimpa(ndimp)
       real*8     tieva(idtem)    
       real*8     p3a_ib(ndrep,ndein)  , p3a_it(ndrep,ndtem)  ,
     &            p3a_id(ndrep,ndden)  , p3a_iz(ndrep,ndzef)  
       real*8     flag(idlev)      , agrl(idlev,3)
       real*8     wvla(idtran)     , aa(idtran)
       real*8     vec(idvec)             , emis_ref(idtran)
       real*8     emis_ib(idtran,ndein)  , emis_it(idtran,ndtem)  ,
     &            emis_id(idtran,ndden)  , emis_iz(idtran,ndzef)
       real*8     xa(idtran)  
       real*8     wvmin(ndwvl)           , wvmax(ndwvl)
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c  initialise
c-----------------------------------------------------------------------
         if(idtem.ne.ndtem) then
	    write(i4unit(-1),1003) 'idtem',idtem,'ndtem',ndtem
	    write(i4unit(-1),1002)
	    stop
	endif
         if(idden.ne.ndden) then
	    write(i4unit(-1),1003) 'idden',idden,'ndden',ndden
	    write(i4unit(-1),1002)
	    stop
	endif
         if(idein.ne.ndein) then
	    write(i4unit(-1),1003) 'idein',idein,'ndein',ndein
	    write(i4unit(-1),1002)
	    stop
	endif
         if(idzef.ne.ndzef) then
	    write(i4unit(-1),1003) 'idzef',idzef,'ndzef',ndzef
	    write(i4unit(-1),1002)
	    stop
	endif
       if(idlev.ne.ndlev) then
	    write(i4unit(-1),1003) 'idlev',idlev,'ndlev',ndlev
	    write(i4unit(-1),1002)
	    stop
	endif
	
	idtran_new = idtran
	       
       z1 = iz1
       
       esym = xfesym(iz0)
       call xxeiam(esym,xsym)
       write(amsr,'(f7.1)')xsym
       write(chr5,'(1a2,1a1,i2)')esym,'+',iz1
       call xxrmve(chr5,crcvr,' ')
	  
c-----------------------------------------------------------------------
c  get correct rydberg constant for the cx receiver         
c-----------------------------------------------------------------------
       call xxrams(amsr)
       rz = r8const('RYR')
       
c-----------------------------------------------------------------------
c  set up interpolation between representative n-shells         
c-----------------------------------------------------------------------
       nmax1=nmax+nip
       
       do n=nmin,nmax1
         en=n
	 flag(n)=dlog(finter(en))
       enddo
       
       nrep(imax+1)=nmax+nmax	 
       i=3
       n1=nmin+2
       
       do n=n1,nmax
         if((n+n-nrep(i)-nrep(i+1)).gt.0) then
             i=i+1
         endif
         inter(n)=i
       enddo
       
       inter(nmin)=2
       inter(nmin+1)=2
       
       do n=nmin,nmax
         x=flag(n)
         i=inter(n)
         i1=nrep(i-1)
         i2=nrep(i)
         x1=flag(i1)
         x2=flag(i2)
	 
         if(imax.gt.i)then
             i3=nrep(i+1)
             x3=flag(i3)
             agrl(n,1)=(x-x2)*(x-x3)/((x1-x2)*(x1-x3))
             agrl(n,2)=(x-x1)*(x-x3)/((x2-x1)*(x2-x3))
             agrl(n,3)=(x-x1)*(x-x2)/((x3-x1)*(x3-x2))
	 else
             agrl(n,1)=(x-x2)/(x1-x2)
             agrl(n,2)=(x-x1)/(x2-x1)
             agrl(n,3)=0.0d0
         endif
	 
	 
       enddo
       
c-----------------------------------------------------------------------
c  evaluate transition wavelengths and check for inclusion in count          
c-----------------------------------------------------------------------
       icount = 0
       
       do n=nmin+1,nmax
         i=inter(n)
	 
         if(i.eq.2) then
	     p3_ref = dexp(agrl(n,2)*dlog(p3a_ib(i,ib_ref)) +
     &                agrl(n,3)*dlog(p3a_ib(i+1,ib_ref)))    
	 elseif(imax.gt.i) then
	     p3_ref = dexp(agrl(n,1)*dlog(p3a_ib(i-1,ib_ref)) +
     &                agrl(n,2)*dlog(p3a_ib(i,ib_ref))   +    
     &                agrl(n,3)*dlog(p3a_ib(i+1,ib_ref)))
         else
	     p3_ref = dexp(agrl(n,1)*dlog(p3a_ib(i-1,ib_ref)) +
     &                agrl(n,2)*dlog(p3a_ib(i,ib_ref)))    
	 endif

         en = n
         do n1=nmin,n-1
	   en1   = n1
	   d     = (1.0d0/(en1*en1)-1.0d0/(en*en))
	   delta = rz*z1*z1*d
           sig2  = delta**2 * 1.0d-8			             
           rf    = 1.0d0 + 1.0d-8 * ( c1 + ( c2 / ( c4 - sig2 ) )       
     &     	   + ( c3 / ( c5 - sig2 ) ) )		             
           wavair= 1.0d8 / ( delta * rf )
	   lwv_incl = .false.
	   do iw = 1, nwvl
	     if((wavair.ge.wvmin(iw)).and.(wavair.le.wvmax(iw)))then
	         lwv_incl = .true.
	     endif
	   enddo
	     	  
	   if(lwv_incl) then
               d=1.0D0/d
               x2=1.0d0/(en*en)
               x=en1**(-0.6666667d0)*(en-en1)**(-0.6666667d0)/
     &           (en-1.0d0)**(-0.6666667d0)
               g = gbb(en,en1,x2,x)
               a1=1.5746d10*z1**4*d*g/(en**5*en1**3)
	       emis = a1*p3_ref/densa(id_ref)
	       
	       if(emis.ge.emis_thres) then
	           if(icount.ge.idtran) then
		       idtran_new=idtran_new+1
		       go to 50
		   endif
	           icount=icount+1
		   na(icount)=n
		   n1a(icount)=n1
		   wvla(icount)=wavair
		   aa(icount)=a1
		   emis_ref(icount)=emis
		   
c----------------------------------------
c stack emissivities for beam energy scan          
c----------------------------------------
		   do ib=1,nbeam
	 	     if(imax.gt.i) then
	 	     	 emis_ib(icount,ib) = 
     &                        dexp(agrl(n,1)*dlog(p3a_ib(i-1,ib)) +
     &   	     	      agrl(n,2)*dlog(p3a_ib(i,ib))   +    
     &   	     	      agrl(n,3)*dlog(p3a_ib(i+1,ib)))
         	     else
	 	     	 emis_ib(icount,ib) = 
     &                        dexp(agrl(n,1)*dlog(p3a_ib(i-1,ib)) +
     &   	     	      agrl(n,2)*dlog(p3a_ib(i,ib)))    
	 	     endif
	             emis_ib(icount,ib) = a1*emis_ib(icount,ib)/
     &                                    densa(id_ref)
                   enddo
c--------------------------------------------
c stack emissivities for ion temperature scan          
c--------------------------------------------
		   do it=1,ntemp
	 	     if(imax.gt.i) then
	 	     	 emis_it(icount,it) = 
     &                        dexp(agrl(n,1)*dlog(p3a_it(i-1,it)) +
     &   	     	      agrl(n,2)*dlog(p3a_it(i,it))   +    
     &   	     	      agrl(n,3)*dlog(p3a_it(i+1,it)))
         	     else
	 	     	 emis_it(icount,it) = 
     &                        dexp(agrl(n,1)*dlog(p3a_it(i-1,it)) +
     &   	     	      agrl(n,2)*dlog(p3a_it(i,it)))    
	 	     endif
	             emis_it(icount,it) = a1*emis_it(icount,it)/
     &                                    densa(id_ref)
                   enddo
c--------------------------------------------
c stack emissivities for ion density scan          
c--------------------------------------------
		   do id=1,ndens
	 	     if(imax.gt.i) then
	 	     	 emis_id(icount,id) = 
     &                        dexp(agrl(n,1)*dlog(p3a_id(i-1,id)) +
     &   	     	      agrl(n,2)*dlog(p3a_id(i,id))   +    
     &   	     	      agrl(n,3)*dlog(p3a_id(i+1,id)))
         	     else
	 	     	 emis_id(icount,id) = 
     &                        dexp(agrl(n,1)*dlog(p3a_id(i-1,id)) +
     &   	     	      agrl(n,2)*dlog(p3a_id(i,id)))    
	 	     endif
	             emis_id(icount,id) = a1*emis_id(icount,id)/
     &                                    densa(id)
                   enddo
c----------------------------------------
c stack emissivities for plasma zeff scan          
c----------------------------------------
		   do iz=1,nzef
	 	     if(imax.gt.i) then
	 	     	 emis_iz(icount,iz) = 
     &                        dexp(agrl(n,1)*dlog(p3a_iz(i-1,iz)) +
     &   	     	      agrl(n,2)*dlog(p3a_iz(i,iz))   +    
     &   	     	      agrl(n,3)*dlog(p3a_iz(i+1,iz)))
         	     else
	 	     	 emis_iz(icount,iz) = 
     &                        dexp(agrl(n,1)*dlog(p3a_iz(i-1,iz)) +
     &   	     	      agrl(n,2)*dlog(p3a_iz(i,iz)))    
	 	     endif
	             emis_iz(icount,iz) = a1*emis_iz(icount,iz)/
     &                                    densa(id_ref)
                   enddo
		     
	       endif   
	       
	   endif
	   
   50      continue	   
  	 enddo
       enddo
       
       if(idtran_new.gt.idtran) then        	  
          write(i4unit(-1),1010)idtran_new  	  
          write(i4unit(-1),1011)        	  
       endif			        	  
       
c-----------------------------------------------------------------------
c  write adf12 dataset         
c-----------------------------------------------------------------------
       c80=' '
       c80(1:16) ='E=    W=        '
       c80(17:63)=' D=h(1s) R='//crcvr//
     &            ' N=        F=           M=cx   '
       c80(64:69)='isel= '
       
       c10=' '
       filnam=' '
       ilast=index(dsname,'.dat')
       if (ilast.gt.0) then
          c10=dsname(max(ilast-10,1):ilast-1)
  	  ifirst=index(c10,'/')
	  filnam(1:10-ifirst)=c10(ifirst+1:10)
       endif
       
       tiev=tiona(it_ref)/ev
       do it=1,ntemp
         tieva(it)=tiona(it)/ev
       enddo	    

       write(iunit,'(i5)')icount
c----------------------------------------------
c  write each block header and reference values         
c----------------------------------------------
       do ic=1,icount
         c80(3:4)=esym
         write(c80(9:16),'(f8.2)')wvla(ic)
         write(c7,'(i3,a1,i3)')na(ic),'-',n1a(ic)
	 call xxrmve(c7,ctran,' ')
	 c80(36:42)=ctran
	 write(c80(46:55),'(1a10)')filnam
	 write(c80(69:72),'(i4)')ic
         write(iunit,'(1a80)')c80
       
         cline=' '
         write(cline,'(1pd10.2,53x,1a6,11x)')
     &               emis_ref(ic),'qefref'
         write(iunit,'(1a80)')cline
	 cline=' '
         write(cline,'(1p5d10.2,13x,1a7,10x)')
     &         bmena(ib_ref),tiev,deniona(id_ref),
     &         zefa(iz_ref),bmag,'parmref'
         write(iunit,'(1a80)')cline
	 cline=' '
         write(cline,'(5i10,13x,1a7,10x)')
     &         nbeam,ntemp,ndens,nzef,nbmag,'nparmsc'
         write(iunit,'(1a80)')cline
	 
c-----------------------------------------------
c  write set of parameter and emissivity vectors         
c-----------------------------------------------
         call cxvcwr( iunit   , ndein  ,  
     &                nbeam   , bmena  , 'ener'           
     &              )
         do ib=1,nbeam
	   vec(ib)=emis_ib(ic,ib)
	 enddo
         call cxvcwr( iunit   , idvec  ,  
     &                nbeam   , vec    , 'qener'           
     &                   )

         call cxvcwr( iunit   , ndtem  ,  
     &                ntemp   , tieva  , 'tiev'           
     &              )
         do it=1,ntemp
	   vec(it)=emis_it(ic,it)
	 enddo
         call cxvcwr( iunit   , idvec  ,  
     &                ntemp   , vec    , 'qtiev'           
     &                   )

         call cxvcwr( iunit   , ndden  ,  
     &                ndens   , deniona, 'densi'           
     &              )
         do id=1,ndens
	   vec(id)=emis_id(ic,id)
	 enddo
         call cxvcwr( iunit   , idvec  ,  
     &                ndens   , vec    , 'qdensi'           
     &                   )

         call cxvcwr( iunit   , ndzef  ,  
     &                nzef    , zefa   , 'zeff'           
     &              )
         do iz=1,nzef
	   vec(iz)=emis_iz(ic,iz)
	 enddo
         call cxvcwr( iunit   , idvec  ,  
     &                nzef    , vec    , 'qzeff'           
     &                   )

c-----------------------------------------------
c fix up for magnetic field dependency         
c-----------------------------------------------
         vec(1) = bmag
         call cxvcwr( iunit   , idvec  ,  
     &                nbmag   , vec    , 'bmag'           
     &              )
         vec(1)=emis_ref(ic)
         call cxvcwr( iunit   , idvec  ,  
     &                nbmag   , vec    , 'qbmag'           
     &                   )
 	 
	 
       enddo
       	 
c-----------------------------------------------
c set up reference emissivity ranking         
c-----------------------------------------------
      do ic=1,icount
        xa(ic)=emis_ref(ic)
      enddo
      
      lup = .false. 
      call xxordr(icount , lup  , xa  , ia  , iinva )
      	    
c-----------------------------------------------------------------------
c  write comments to adf12 dataset         
c-----------------------------------------------------------------------

      cdash='C---------------------------------------'//
     &      '----------------------------------------'

      cblanks=' '
      cblanks(1:1)='C'
      c80 = ' '
      c80(1:21)='C  Wavelength ranges:'
      write(iunit,'(1a80)')cdash
      write(iunit,'(1a80)')c80
      write(iunit,'(1a80)')cblanks
      c80 = ' '
      c80(1:27)=
     &        'C     wvmin(A)    wvmax(A)'   
      write(iunit,'(1a80)')c80
      c80 = ' '
      c80(1:27)=
     &        'C     --------    --------'
      write(iunit,'(1a80)')c80
      do iw=1,nwvl
        write(iunit,'(a,f8.2,4x,f8.2)')'C    ',wvmin(iw),wvmax(iw)
      enddo
      write(iunit,'(1a80)')cblanks
      write(iunit,'(a,1pd10.2)')'C     emis_thres = ',emis_thres
      write(iunit,'(1a80)')cblanks
      	
      cblanks=' '
      cblanks(1:1)='C'
      c80 = ' '
      c80(1:30)='C  Effective coefficient list:'
      write(iunit,'(1a80)')cblanks
      write(iunit,'(1a80)')c80
      write(iunit,'(1a80)')cblanks
      c80 = ' '
      c80(1:56)=
     &        'C     isel    type      ion       transition   wavln.(A)'
      c80(57:69)=
     &        '   emis. rank'
      write(iunit,'(1a80)')c80
      c80 = ' '
      c80(1:56)=
     &        'C     ----    ----      ---       ----------   ---------'
      c80(57:69)=
     &        '   ----------'
      write(iunit,'(1a80)')c80
      do ic=1,icount
        write(iunit,'(1a4,i5,1a14,1a3,i2,1a9,i3,1a1,i3,f11.2,i10)')
     &       'C   ',ic,'.   cx.emis.  ',esym//'+',iz1-1,'      n =',
     &        na(ic),'-',n1a(ic),wvla(ic),iinva(ic)
      enddo      
      write(iunit,'(1a80)')cblanks
      write(iunit,'(1a80)')cblanks
      
      c80 = ' '
      c80(1:39)='C  Bundle-n population code parameters:'
      write(iunit,'(1a80)')c80
      write(iunit,'(1a80)')cblanks
      write(iunit,1004)nip   , intd   , iprs  , ilow
      write(iunit,1005)ionip , nionip , ilprs , ivdisp
      write(iunit,1006)cion,cpy
      write(iunit,'(1a80)')cblanks
      write(iunit,1007)ts,w,w1
      write(iunit,'(1a80)')cblanks
      write(iunit,1008)zimpa(1),amimpa(1),frimpa(1)

      if(nimp.gt.1)then
          do imp=2,nimp
            write(iunit,1009)imp,zimpa(imp),imp,amimpa(imp),
     &                       imp,frimpa(imp)
	  enddo
      endif
      
      write(iunit,'(1a80)')cblanks
      write(iunit,'(1a80)')cblanks
      
      call xxslen(dsname, l1, l2)
      write(iunit,'(1a16,a)')'C  adf25 file : ',dsname(l1:l2)
      write(iunit,'(1a16,a)')'C  Code       : ','adas316'
      write(iunit,'(1a16,a)')'C  Producer   : ',user
      write(iunit,'(1a16,a)')'C  Date       : ',date
      write(iunit,'(1a80)')cblanks
      write(iunit,'(1a80)')cdash
	
       return
c-----------------------------------------------------------------------
 1001 format(1x,30('*'),'  cgwr12 error  ',30('*')//
     &       1x,'for loop operation parameter: ',a,' must be non-zero')
 1002 format(/1x,29('*'),' program terminated ',29('*'))
 1003 format(1x,30('*'),'  cgwr12 error  ',30('*')//
     &       1x,'internal param. ',1a5,' = ',i4,' .ne. ',1a5,' = ',i4)
 1004  format('C    ', 'nip   =',i3,5x,'intd  =',i3,5x,'iprs  =',i3,5x,
     &                 'ilow  =',i3)
 1005  format('C    ', 'ionip =',i3,5x,'nionip=',i3,5x,'ilprs =',i3,5x,
     &                 'ivdisp=',i3)
     
 1006  format('C    ', 'cion  =',0p,f4.1,4x,'cpy   =',0p,f4.1)
 1007  format('C    ', 'ts(K)   =',1pd10.2,5x,'w       =',1pd10.2,5x,
     &                 'w1      =',1pd10.2)
 1008  format('C    ', 'zimp(1) =',1pd10.2,5x,'amimp(1)=',1pd10.2,5x,
     &                 'frimp(1)=',1pd10.2)
 1009  format('C    ', 'zimp(',i1,') =',1pd10.2,5x,
     &                 'amimp(',i1,')=',1pd10.2,5x,
     &                 'frimp(',i1,')=',1pd10.2)
 1010 format(1x,30('*'),'  cgwr12 error  ',30('*')//
     &       1x,'transitions exceeded: increase idtran to ',i5)
 1011 format(/1x,29('*'),' program continues  ',29('*'))
c-----------------------------------------------------------------------
	
      end       
	    	
	    
	           
                
   
