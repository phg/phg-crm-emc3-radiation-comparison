CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas306/c6outg.for,v 1.3 2004/07/06 11:52:56 whitefor Exp $ Date $Date: 2004/07/06 11:52:56 $
CX
      SUBROUTINE C6OUTG( MXNSHL , MXJSHL , MXBEAM , MXOBSL ,
     &                   MXPRSL , MXGRF  , NGFPLN , NGRF   ,
     &                   IDGRF  , LPLT1  , LGRD1  , LDEF1  ,
     &                   DATE   , TITLE  , DSFULL , SYMBD  ,
     &                   IDZ0   , SYMBR  , IRZ0   , IRZ1   ,
     &                   IRZ2   , RAMSNO , TEV    , TIEV   ,
     &                   DENS   , DENSZ  , ZEFF   , BMAG   ,
     &                   NBEAM  , BMFRA  , BMENA  , NOLINE ,
     &                   NU     , NL     , EMISA  , ITHEOR ,
     &                   IBSTAT , IEMMS  , NPLINE , NPU    ,
     &                   NPL    , XLG    , XUG    , YLG    ,
     &                   YUG    , QEFF   , TBLEMI , TBLWLN ,
     &                   LGHOST
     &                 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C6OUTG *********************
C
C  PURPOSE: PLOTS EMISSIVITY GRAPHS FOR ADAS306.
C
C  CALLING PROGRAM: ADAS306
C
C  INPUT : (I*4)  MXNSHL    = MAXIMUM NUMBER OF N SHELLS.
C  INPUT : (I*4)  MXJSHL    = MAXIMUM NUMBER OF J SUB-SHELLS.
C  INPUT : (I*4)  MXBEAM    = MAXIMUM NUMBER OF BEAM COMPONENTS.
C  INPUT : (I*4)  MXOBSL    = MAXIMUM NUMBER OF OBSERVED SPECTRUM
C                             LINES.
C  INPUT : (I*4)  MXPRSL    = MAXIMUM NUMBER OF SPECTRUM LINES TO
C                             PREDICT.
C  INPUT : (I*4)  MXGRF     = MAXIMUM NUMBER OF GRAPHS FOR OUTPUT.
C  INPUT : (I*4)  NGFPLN    = NUMBER OF GRAPHS PER PREDICTED SPECTRUM
C                             LINE.
C  INPUT : (I*4)  NGRF      = NUMBER OF GRAPHS FOR OUTPUT.
C  INPUT : (I*4)  IDGRF()   = LIST OF INDEXES OF GRAPHS FOR OUTPUT.
C                             DIMENSION: MXGRF
C  INPUT : (L*4)  LPLT1     = FLAGS WHETHER TO PLOT GRAPHS ON SCREEN.
C                             .TRUE.  => PLOT GRAPHS ON SCREEN.
C                             .FALSE. => DO NOT PLOT GRAPHS ON SCREEN.
C  INPUT : (L*4)  LGRD1     = FLAGS WHETHER TO PUT GRAPHS IN GRID FILE.
C                             .TRUE.  => PUT GRAPHS IN GRID FILE
C                             .FALSE. => DO NOT PUT GRAPHS IN GRID FILE
C  INPUT : (L*4)  LDEF1     = FLAGS DEFAULT GRAPH SCALING
C                             .TRUE.  => USE DEFAULT GRAPH SCALING.
C                             .FALSE. => DO NOT USE DEFAULT SCALING.
C  INPUT : (C*8)  DATE      = DATE.
C  INPUT : (C*40) TITLE     = ISPF ENTERED GENERAL TITLE FOR RUN.
C  INPUT : (C*80) DSFULL    = FILE NAME OF INPUT DATA SET.
C  INPUT : (C*2)  SYMBD     = DONOR ELEMENT SYMBOL.
C  INPUT : (I*4)  IDZ0      = DONOR NUCLEAR CHARGE.
C  INPUT : (C*2)  SYMBR     = RECEIVER ELEMENT SYMBOL.
C  INPUT : (I*4)  IRZ0      = RECEIVER NUCLEAR CHARGE.
C  INPUT : (I*4)  IRZ1      = RECEIVER ION INITIAL CHARGE.
C  INPUT : (I*4)  IRZ2      = RECEIVER ION FINAL CHARGE.
C  INPUT : (R*8)  RAMSNO    = RECEIVER ATOMIC MASS.
C  INPUT : (R*8)  TEV       = ELECTRON TEMPERATURE.
C                             UNITS: EV
C  INPUT : (R*8)  TIEV      = ION TEMPERATURE.
C                             UNITS: EV
C  INPUT : (R*8)  DENS      = ELECTRON DENSITY.
C                             UNITS: CM-3
C  INPUT : (R*8)  DENSZ     = PLASMA ION DENSITY.
C                             UNITS: CM-3
C  INPUT : (R*8)  ZEFF      = EFFECTIVE ION CHARGE.
C  INPUT : (R*8)  BMAG      = PLASMA MAGNETIC INDUCTION.
C                             UNITS: TESLA
C  INPUT : (I*4)  NBEAM     = NUMBER OF BEAM ENERGIES.
C  INPUT : (R*8)  BMFRA()   = BEAM COMPONENT FRACTIONS.
C                             DIMENSION: COMPONENT INDEX.
C  INPUT : (R*8)  BMENA()   = BEAM ENERGY COMPONENTS.
C                             UNITS: EV/AMU
C                             DIMENSION: COMPONENT INDEX.
C  INPUT : (I*4)  NOLINE    = NUMBER OF OBSERVED SPECTRUM LINES.
C  INPUT : (I*4)  NU()      = LIST OF UPPER PRINCIPAL QUANTUM NUMBERS
C                             OF OBSERVED SPECTRUM LINES.
C                             DIMENSION: SPECTRUM LINE INDEX.
C  INPUT : (I*4)  NL()      = LIST OF LOWER PRINCIPAL QUANTUM NUMBERS
C                             OF OBSERVED SPECTRUM LINES.
C                             DIMENSION: SPECTRUM LINE INDEX.
C  INPUT : (R*8)  EMISA()   = LIST OF EMISSIVITIES OF OBSERVED SPECTRUM
C                             LINES.
C                             DIMENSION: SPECTRUM LINE INDEX.
C  INPUT : (I*4)  ITHEOR    = CHARGE EXCHANGE MODEL OPTION.
C                             1 => USE INPUT DATA SET.
C                             2 => USE EIKONAL MODEL.
C  INPUT : (I*4)  IBSTAT    = DONOR STATE FOR EIKONAL MODEL.
C                             1 => H(1S)
C                             2 => H(2S)
C                             3 => H(2P)
C                             4 => HE(1S2)
C                             5 => HE(1S2S)
C  INPUT : (I*4)  IEMMS     = EMISSION MEASURE MODEL OPTION.
C                             1 => CHARGE EXCHANGE.
C                             2 => ELECTRON IMPACT EXCITATION.
C                             3 => RADIATIVE RECOMBINATION.
C  INPUT : (I*4)  NPLINE    = NUMBER OF SPECTRUM LINES TO PREDICT.
C  INPUT : (I*4)  NPU()     = LIST OF UPPER PRINCIPAL QUANTUM NUMBERS
C                             OF SPECTRUM LINES TO PREDICT.
C                             DIMENSION: PREDICTED LINE INDEX.
C  INPUT : (I*4)  NPL()     = LIST OF LOWER PRINCIPAL QUANTUM NUMBERS
C                             OF SPECTRUM LINES TO PREDICT.
C                             DIMENSION: PREDICTED LINE INDEX.
C  INPUT : (R*8)  XLG()     = LOWER LIMIT FOR X-AXIS OF GRAPHS.
C                             DIMENSION: NGFPLN
C  INPUT : (R*8)  XUG()     = UPPER LIMIT FOR X-AXIS OF GRAPHS.
C                             DIMENSION: NGFPLN
C  INPUT : (R*8)  YLG()     = LOWER LIMIT FOR Y-AXIS OF GRAPHS.
C                             DIMENSION: NGFPLN
C  INPUT : (R*8)  YUG()     = UPPER LIMIT FOR Y-AXIS OF GRAPHS.
C                             DIMENSION: NGFPLN
C  INPUT : (R*8)  QEFF()    = EFF. RATE COEFFICIENT FOR PREDICTED
C                             SPECTRUM LINE.
C                             UNITS:
C                             DIMENSION: PREDICTED LINE INDEX.
C  INPUT : (R*8)  TBLEMI(,,)= TABLE OF COLLISION EMISSIVITIES FOR
C                             PREDICTED SPECTRUM LINE.
C                             UNITS: PH CM-2 SEC-1
C                             1ST DIMENSION: J->J' TRANSITION INDEX.
C                             2ND DIMENSION: REFERENCED BY I4IDLI().
C                             3RD DIMENSION: PREDICTED LINE INDEX.
C  INPUT : (R*8)  TBLWLN(,,)= TABLE OF WAVELENGTHS FOR PREDICTED
C                             SPECTRUM LINE.
C                             UNITS: A
C                             1ST DIMENSION: J->J' TRANSITION INDEX.
C                             2ND DIMENSION: REFERENCED BY I4IDLI().
C                             3RD DIMENSION: PREDICTED LINE INDEX.
C
C  I/O   : (L*4)  LGHOST    = INITIALISATION FLAG FOR GHOST80.
C                             .TRUE.  => GHOST80 INITIALISED.
C                             .FALSE. => GHOST80 NOT INITIALISED.
C
C  PARAM : (I*4)  NDIV      = NUMBER OF DIVISIONS ON X AXIS.
C  PARAM : (I*4)  MXA       = MAXIMUM NUMBER OF DATA POINTS FOR GRAPH A.
C  PARAM : (I*4)  MXB       = MAXIMUM NUMBER OF DATA POINTS FOR GRAPH B.
C
C  PARAM : (R*8)  ANGDIV    = NO. OF ANGSTROMS PER DIVISION.
C
C          (I*4)  N         = LOOP INDEX FOR PRINCIPAL QUANTUM NUMBER.
C          (I*4)  L         = LOOP INDEX FOR ORBITAL QUANTUM NUMBER.
C          (I*4)  J         = LOOP INDEX FOR J QUANTUM NUMBER INDEX.
C          (I*4)  JMX       = UPPER LIMIT ON 'J' LOOP.
C          (I*4)  N1        = LOOP INDEX FOR PRINCIPAL QUANTUM NUMBER.
C          (I*4)  L1        = LOOP INDEX FOR ORBITAL QUANTUM NUMBER.
C          (I*4)  J1        = LOOP INDEX FOR J QUANTUM NUMBER INDEX.
C          (I*4)  J1MX      = UPPER LIMIT ON 'J' LOOP.
CX          (I*4)  IDL       = L-RESOLVED TABLE INDEX.
C          (I*4)  NPTS      = NO. OF EMISSIVITY AND WAVELENGTH VALUES.
C          (I*4)  ID        = SPECTRUM LINE TABLE INDEX.
C          (I*4)  I         = LOOP INDEX.
C          (I*4)  K         = ARRAY INDEX.
C
C          (R*8)  CWLN      = CENTRAL WAVELENGTH ON GRAPH.
C                             UNITS: A
C          (R*8)  ERATE     = EFFECTIVE RATE COEFFICIENT.
C                             UNITS:
C
C          (C*1)  GRID      = DUMMY NAME VARIABLE FOR USE WITH GHOST80.
C          (C*1)  PIC       = DUMMY NAME VARIABLE FOR USE WITH GHOST80.
C
C          (R*8)  CEMIS()   = COLUMN EMISSIVITIES.
C                             UNITS: PHOT CM-2 SEC-1
C                             DIMENSION: MXA
C          (R*8)  WAVLN()   = WAVELENGTHS.
C                             UNITS: A
C                             DIMENSION: MXA
C          (R*8)  XA()      = X DATA POINTS FOR GRAPH A.
C                             DIMENSION: MXA
C          (R*8)  YA()      = Y DATA POINTS FOR GRAPH A.
C                             DIMENSION: MXA
C          (R*8)  XB()      = X DATA POINTS FOR GRAPH B.
C                             DIMENSION: MXB
C          (R*8)  YB()      = Y DATA POINTS FOR GRAPH B.
C                             DIMENSION: MXB
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4UNIT     ADAS      RETURNS UNIT NO. FOR OUTPUT OF MESSAGES.
CX          I4IDFL     ADAS      RETURNS UNIQUE INDEX GIVEN QUANTUM
CX                               NUMBERS N AND L.
C          I4IDLI     ADAS      RETURNS INDEX FOR PREDICTED SPECTRUM
C                               LINE TABLES.
C          CXGFIL     ADAS      FILLS ADAS306 / 308 GRAPH ARRAYS.
C          CXGEMI     ADAS      PLOTS ADAS306 / 308 EMISSIVITY GRAPHS.
C                     GHOST80   VARIOUS SUBROUTINES.
C
C NOTES:
C       1) THE J->J' TRANSITION INDEX IS AS FOLLOWS:
C              1 : J=L+0.5 -> J'=L'+0.5
C              2 : J=L+0.5 -> J'=L'-0.5
C              3 : J=L-0.5 -> J'=L'+0.5
C              4 : J=L-0.5 -> J'=L'-0.5
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/87
C          JET EXT. 5183
C
C DATE:    24/11/93
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    22ND MAY 1996
C
C VERSION: 1.1                          DATE: 22-05-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST VERSION. IBM VERSION NOT CHANGED
C
C VERSION: 1.2                          DATE: 29-05-96
C MODIFIED: WILLIAM OSBORN
C               - REMOVED UNUSED VARIABLES
C
C VERSION: 1.3                          DATE: 29-05-96
C MODIFIED: WILLIAM OSBORN
C               - S.C.C.S. MISTAKE
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    I4UNIT  , I4IDLI
C-----------------------------------------------------------------------
      INTEGER    MXN           , MXJ          , NDIV
      PARAMETER( MXN = 20      , MXJ = 2      , NDIV = 600 )
      INTEGER    MXA                        , MXB
      PARAMETER( MXA = (2*MXJ-1)*(2*MXN-3)  , MXB = NDIV )
C-----------------------------------------------------------------------
      REAL*8     ANGDIV
      PARAMETER( ANGDIV = 0.096D0 )
C-----------------------------------------------------------------------
      INTEGER    MXNSHL  , MXJSHL  , MXBEAM  , MXOBSL  , MXPRSL  ,
     &           MXGRF   , NGFPLN  , NGRF    , IDZ0    , IRZ0    ,
     &           IRZ1    , IRZ2    , NBEAM   , NOLINE  , ITHEOR  ,
     &           IBSTAT  , IEMMS   , NPLINE
      INTEGER    N       , L       , J       , JMX     , N1      ,
     &           L1      , J1      , J1MX    , NPTS    ,
     &           ID      , I       , K
C-----------------------------------------------------------------------
      REAL*8     RAMSNO  , TEV     , TIEV    , DENS    , DENSZ   ,
     &           ZEFF    , BMAG
      REAL*8     CWLN    , ERATE
C-----------------------------------------------------------------------
      LOGICAL    LPLT1   , LGRD1   , LDEF1   , LGHOST
C-----------------------------------------------------------------------
      CHARACTER  DATE*8     , TITLE*40   , DSFULL*80  , SYMBD*2    ,
     &           SYMBR*2
      CHARACTER  GRID*1     , PIC*1
C-----------------------------------------------------------------------
      INTEGER    IDGRF(MXGRF)    , NU(MXOBSL)      , NL(MXOBSL)    ,
     &           NPU(MXPRSL)     , NPL(MXPRSL)
C-----------------------------------------------------------------------
      REAL*8     BMFRA(MXBEAM)   , BMENA(MXBEAM)   , EMISA(MXOBSL) ,
     &           XLG(NGFPLN)     , XUG(NGFPLN)     , YLG(NGFPLN)   ,
     &           YUG(NGFPLN)     , QEFF(MXPRSL)
      REAL*8     CEMIS(MXA)      , WAVLN(MXA)      , XA(MXA)       ,
     &           YA(MXA)         , XB(MXB)         , YB(MXB)
C-----------------------------------------------------------------------
      REAL*8     TBLEMI(2*MXJSHL,2*MXNSHL-3,MXPRSL)      ,
     &           TBLWLN(2*MXJSHL,2*MXNSHL-3,MXPRSL)
C-----------------------------------------------------------------------
      INTEGER     PIPEIN        , PIPEOU
      PARAMETER(  PIPEIN=5      , PIPEOU=6)
C-----------------------------------------------------------------------
      DATA       GRID / ' ' / ,
     &           PIC  / ' ' /
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C CHECK INTERNAL PARAMETERS 'MXN' AND 'MXJ'.
C-----------------------------------------------------------------------
C
      IF (MXN .LT. MXNSHL) THEN
         WRITE(I4UNIT(-1),1000) MXN, MXNSHL
         STOP
      ENDIF
C
      IF (MXJ .LT. MXJSHL) THEN
         WRITE(I4UNIT(-1),1001) MXJ, MXJSHL
         STOP
      ENDIF
C
C-----------------------------------------------------------------------
C WRITE OUT TO PIPE
C-----------------------------------------------------------------------
C
      WRITE(PIPEOU,*)NGRF
      CALL XXFLSH(PIPEOU)

      IF(NGRF.EQ.0) RETURN

      WRITE(PIPEOU,*)MXA
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*)MXB
      CALL XXFLSH(PIPEOU)

CXC
CXC-----------------------------------------------------------------------
CXC CHECK OUTPUT STREAM FOR GRAPH.
CXC-----------------------------------------------------------------------
CXC
CX      IF ( ( .NOT. (LPLT1 .OR. LGRD1) ) .OR. ( NGRF .EQ. 0 ) ) RETURN

C-----------------------------------------------------------------------
C LOOP THROUGH GRAPHS.
C-----------------------------------------------------------------------
C
      DO 1 I = 1 , NGRF
C
         K = IDGRF(I)
         N  = NPU(K)
         N1 = NPL(K)
         ERATE = QEFF(K)
         NPTS = 0
C
C-----------------------------------------------------------------------
C COPY DATA IN REQUIRED ORDER TO LOCAL ARRAYS.
C-----------------------------------------------------------------------
C
         DO 2 L = 0 , N1
            IF (L .EQ. 0) THEN
               JMX = 1
            ELSE
               JMX = 2
            ENDIF
            DO 3 J = 1 , JMX
               DO 4 L1 = L+1 , L-1 , -2
                  IF ((L1 .GE. 0) .AND. (L1 .LT. N1)) THEN
                     IF (L1 .EQ. 0) THEN
                        J1MX = 1
                     ELSE
                        J1MX = 2
                     ENDIF
                     DO 5 J1 = 1 , J1MX
                        IF (IABS( (L-L1)-(J-J1) ) .LE. 1) THEN
                           NPTS = NPTS + 1
                           ID = I4IDLI( N1 , L , L1 )
                           CEMIS(NPTS) = TBLEMI(2*(J-1)+J1,ID,K)
                           WAVLN(NPTS) = TBLWLN(2*(J-1)+J1,ID,K)
                        ENDIF
    5                CONTINUE
                  ENDIF
    4          CONTINUE
    3       CONTINUE
    2    CONTINUE
C
C-----------------------------------------------------------------------
C FILL GRAPH ARRAYS.
C-----------------------------------------------------------------------
C
         CALL CXGFIL( MXA    , MXB    , IRZ0   , RAMSNO , N      ,
     &                N1     , TIEV   , NDIV   , ANGDIV , NPTS   ,
     &                CEMIS  , WAVLN  , CWLN   , XA     , YA     ,
     &                XB     , YB                                  )

C
C-----------------------------------------------------------------------
C WRITE OUT TO PIPE
C-----------------------------------------------------------------------
C
         WRITE(PIPEOU,*)NPTS
         CALL XXFLSH(PIPEOU)
         WRITE( PIPEOU, *) ERATE
         CALL XXFLSH( PIPEOU )
         DO 10, L=1, MXA 
            WRITE( PIPEOU, *) XA(L)
            CALL XXFLSH( PIPEOU )
            WRITE( PIPEOU, *) YA(L)
            CALL XXFLSH( PIPEOU )
 10      CONTINUE
         DO 20, L=1, MXB
            WRITE( PIPEOU, *) XB(L)
            CALL XXFLSH( PIPEOU )
            WRITE( PIPEOU, *) YB(L)
            CALL XXFLSH( PIPEOU )
 20      CONTINUE

 1    CONTINUE
C     
C-----------------------------------------------------------------------
C
 1000 FORMAT( 1X, 32('*'), ' C6OUTG ERROR ', 32('*') //
     &        2X, 'INTERNAL PARAMETER ''MXN'' IS LESS THAN INPUT ',
     &            'ARGUMENT ''MXNSHL''.'/
     &        2X, 'MXN = ', I3 , '   MXNSHL = ', I3/
     &        2X, 'INCREASE PARAMETER ''MXN'' IN SUBROUTINE C6OUTG.' //
     &        1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )
 1001 FORMAT( 1X, 32('*'), ' C6OUTG ERROR ', 32('*') //
     &        2X, 'INTERNAL PARAMETER ''MXJ'' IS LESS THAN INPUT ',
     &            'ARGUMENT ''MXJSHL''.'/
     &        2X, 'MXJ = ', I3 , '   MXJSHL = ', I3/
     &        2X, 'INCREASE PARAMETER ''MXJ'' IN SUBROUTINE C6OUTG.' //
     &        1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
