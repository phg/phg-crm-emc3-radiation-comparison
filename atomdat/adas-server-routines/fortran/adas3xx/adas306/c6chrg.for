CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas306/c6chrg.for,v 1.1 2004/07/06 11:52:20 whitefor Exp $ Date $Date: 2004/07/06 11:52:20 $
CX
      SUBROUTINE C6CHRG( SYMBD , IZD   , SYMBR , IZR   , IDZ0  ,
     &                   IRZ0  , IRZ1  , IRZ2  , NGRND , NBOT
     &                 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C6CHRG *********************
C
C  PURPOSE:  SETS UP NUCLEAR CHARGE OF DONOR AND NULEAR, INITIAL AND
C            FINAL CHARGES OF RECEIVER. CHECKS VALIDITY OF RECEIVER
C            CHARGES. ALSO SETS GROUND STATE N LEVEL AND LOWEST N LEVEL
C            FOR TABULAR OUTPUTS.
C
C  CALLING PROGRAM: ADAS306
C
C  INPUT : (C*2)  SYMBD   = DONOR ELEMENT SYMBOL.
C  INPUT : (I*4)  IZD     = DONOR ION CHARGE.
C  INPUT : (C*2)  SYMBR   = RECEIVER ELEMENT SYMBOL.
C  INPUT : (I*4)  IZR     = RECEIVER ION CHARGE.
C
C  OUTPUT: (I*4)  IDZ0    = DONOR NUCLEAR CHARGE.
C  OUTPUT: (I*4)  IRZ0    = RECEIVER NUCLEAR CHARGE.
C  OUTPUT: (I*4)  IRZ1    = RECEIVER ION INITIAL CHARGE.
C  OUTPUT: (I*4)  IRZ2    = RECEIVER ION FINAL CHARGE.
C  OUTPUT: (I*4)  NGRND   = PRINCIPAL QUANTUM NUMBER OF GROUND STATE.
C  OUTPUT: (I*4)  NBOT    = MINIMUM PRINCIPAL QUANTUM NUMBER.
C
C ROUTINES:
C           ROUTINE     SOURCE      BRIEF DESCRIPTION
C           ------------------------------------------------------------
C           I4UNIT      ADAS        RETURNS UNIT NO. FOR OUTPUT MESSAGES
C           CXCHRG      ADAS        RETURNS DONOR NUCLEAR CHARGE AND
C                                   RECEIVER NULEAR, INITIAL AND FINAL
C                                   CHARGES.
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    11/11/93
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    22ND MAY 1996
C
C VERSION: 1.1                          DATE: 22-05-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST VERSION. NO CHANGES FROM IBM VERSION.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    I4UNIT
C-----------------------------------------------------------------------
      INTEGER    IZR     , IZD     , IDZ0    , IRZ0    , IRZ1    ,
     &           IRZ2    , NGRND   , NBOT
C-----------------------------------------------------------------------
      CHARACTER  SYMBD*2  , SYMBR*2
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C SET UP CHARGES.
C-----------------------------------------------------------------------
C
      CALL CXCHRG( SYMBD , IZD   , SYMBR , IZR   , IDZ0  ,
     &             IRZ0  , IRZ1  , IRZ2                    )
C
C-----------------------------------------------------------------------
C CHECK CHARGES.
C-----------------------------------------------------------------------
C
      IF ((IRZ1 .NE. IRZ0) .AND. ((IRZ1 + 2) .NE. IRZ0) .AND.
     &    ((IRZ1 + 10) .NE. IRZ0)                             ) THEN
         WRITE(I4UNIT(-1),1000) IRZ0 , IRZ1
         STOP
      ENDIF
C
C-----------------------------------------------------------------------
C SET UP LOWER PRINCIPAL QUANTUM NUMBERS.
C-----------------------------------------------------------------------
C
      IF (IRZ1 .EQ. IRZ0 ) THEN
         NGRND = 1
         NBOT = 2
      ELSE IF (IRZ1 .EQ. (IRZ0 - 2) ) THEN
         NGRND = 2
         NBOT = 3
      ELSE IF (IRZ1 .EQ. (IRZ0 - 10) ) THEN
         NGRND = 3
         NBOT = 4
      ENDIF
C
C-----------------------------------------------------------------------
C
 1000 FORMAT( 1X, 32('*'), ' C6CHRG ERROR ', 32('*') //
     &        2X, 'DIFFERENCE BETWEEN NUCLEAR AND ION CHARGE GIVES ',
     &            'UNKNOWN ION TYPE.'/
     &        2X, 'NUCLEAR CHARGE Z0 = ', I3 ,
     &            '   ION CHARGE Z1 = ', I3/
     &        2X, 'DIFFERENCE MUST BE 0 (H-LIKE), 2 (LI-LIKE), ',
     &            'OR 10 (NA-LIKE).' //
     &        1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
