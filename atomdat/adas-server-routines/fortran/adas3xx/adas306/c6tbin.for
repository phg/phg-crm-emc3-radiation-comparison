CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas306/c6tbin.for,v 1.1 2004/07/06 11:53:49 whitefor Exp $ Date $Date: 2004/07/06 11:53:49 $
CX
      SUBROUTINE C6TBIN( MXNSHL , IZ1 , NBOT , NTOP , TEV , QTHIN )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C6TBIN *********************
C
C  PURPOSE:  SETS UP AN ELECTRON IMPACT IONISATION RATE COEFFICIENT
C            TABLE FOR N-LEVELS BASED ON THE ECIP APPROXIMATION. ENERGY
C            LEVELS ARE ASSUMED HYDROGENIC IN THE EFFECTIVE ION CHARGE.
C
C  CALLING PROGRAM: ADAS306
C
C  INPUT : (I*4)  MXNSHL   = MAXIMUM VALUE OF PRINCIPAL QUANTUM NUMBER.
C  INPUT : (I*4)  IZ1      = ION CHARGE.
C  INPUT : (I*4)  NBOT     = MINIMUM PRINCIPAL QUANTUM NUMBER.
C  INPUT : (I*4)  NTOP     = MAXIMUM PRINCIPAL QUANTUM NUMBER.
C  INPUT : (R*8)  TEV      = ELECTRON TEMPERATURE.
C                            UNITS: EV
C
C  OUTPUT: (R*8)  QTHIN()  = IONISATION RATE COEFFICIENT.
C                            UNITS: CM3 SEC-1
C                            DIMENSION: N SHELL INDEX.
C
C  PARAM : (R*8)  P1       =
C
C          (I*4)  N        = LOOP INDEX.
C          (I*4)  IZC      = IZ1-1.
C
C          (R*8)  ZETA    = EFFECTIVE NUMBER OF EQUIVALENT ELECTRONS FOR
C                           SHELL.
C          (R*8)  TE      = ELECTRON TEMPERATURE.
C                           UNITS: K
C          (R*8)  XI       =
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          R8ECIP     ADAS
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    03/11/93
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    22ND MAY 1996
C
C VERSION: 1.1                          DATE: 22-05-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST VERSION. IBM VERSION NOT CHANGED
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      REAL*8     R8ECIP
C-----------------------------------------------------------------------
      REAL*8     P1
      PARAMETER( P1 = 1.16054D4 )
C-----------------------------------------------------------------------
      INTEGER    MXNSHL  , IZ1     , NBOT    , NTOP
      INTEGER    N       , IZC
C-----------------------------------------------------------------------
      REAL*8     TEV
      REAL*8     ZETA    , TE      , XI
C-----------------------------------------------------------------------
      REAL*8     QTHIN(MXNSHL)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C CLEAR OUTPUT TABLE.
C-----------------------------------------------------------------------
C
      DO 1 N = 1 , MXNSHL
         QTHIN(N) = 0.0D0
    1 CONTINUE
C
C-----------------------------------------------------------------------
C INITIALIZE VALUES.
C-----------------------------------------------------------------------
C
      IZC  = IZ1 - 1
      ZETA = 1.0D0
      TE   = P1 * TEV
C
C-----------------------------------------------------------------------
C FILL OUTPUT TABLE.
C-----------------------------------------------------------------------
C
      DO 2 N = NBOT , NTOP
         XI = ( DFLOAT( IZ1 ) / DFLOAT( N ) )**2
         QTHIN(N) = R8ECIP( IZC , XI , ZETA , TE )
    2 CONTINUE
C
C-----------------------------------------------------------------------
C
      RETURN
      END
