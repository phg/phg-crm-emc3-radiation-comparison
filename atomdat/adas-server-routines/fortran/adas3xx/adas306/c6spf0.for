CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas306/c6spf0.for,v 1.2 2004/07/06 11:53:23 whitefor Exp $ Date $Date: 2004/07/06 11:53:23 $
CX
      SUBROUTINE C6SPF0( REP    , DSFULL)
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C6SPF0 *********************
C
C  PURPOSE: TO DISPLAY AND FETCH VALUES FROM IDL
C           (INPUT DATA SET SPECIFICATIONS).
C
C  CALLING PROGRAM: ADAS306
C
C  SUBROUTINE:
C
C  OUTPUT: (C*3)   REP     = 'YES' => TERMINATE PROGRAM EXECUTION.
C                          = 'NO ' => CONTINUE PROGRAM EXECUTION.
C
C  OUTPUT: (C*80)  DSFULL  = INPUT DATA SET NAME (FULL MVS DSN)
C                            (IN FORM SUITABLE FOR DYNAMIC ALLOCATION)
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    05/03/91
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    22ND MAY 1996
C
C VERSION: 1.1                          DATE: 22-05-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST VERSION
C
C VERSION: 1.2                          DATE: 29-05-96
C MODIFIED: WILLIAM OSBORN
C               - REMOVED UNUSED VARIABLES
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      CHARACTER   REP*3         , DSFULL*80
C-----------------------------------------------------------------------
      INTEGER     PIPEIN        , PIPEOU
      PARAMETER(  PIPEIN=5      , PIPEOU=6)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C  READ OUTPUTS FROM PIPE
C-----------------------------------------------------------------------
C
      READ(PIPEIN,'(A)') REP
      READ(PIPEIN,'(A)') DSFULL
C
C-----------------------------------------------------------------------
C

      RETURN
      END
