CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas306/c6ajtb.for,v 1.1 2004/07/06 11:52:17 whitefor Exp $ Date $Date: 2004/07/06 11:52:17 $
CX
      SUBROUTINE C6AJTB( MXJSHL , IZ1 , NU , LU , NL , LL , AA )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C6AJTB *********************
C
C  PURPOSE:  CALCULATES HYDRONIC LJ RESOLVED A-VALUES.
C
C            THE SUBROUTINE CHECKS TO SEE IF A-VALUE IS POSSIBLE AND
C            DIPOLE ALLOWED AND RETURNS ZEROES IF NOT.
C
C  CALLING PROGRAM: ADAS306
C
C  INPUT : (I*4)  MXJSHL   = MAXIMUM NUMBER OF J SUB-SHELLS.
C  INPUT : (I*4)  IZ1      = ION CHARGE.
C  INPUT:  (I*4)  NU       = UPPER PRINCIPAL QUANTUM NUMBER.
C  INPUT:  (I*4)  LU       = ORBITAL QUANTUM NUMBER FOR NU.
C  INPUT:  (I*4)  NL       = LOWER PRINCIPAL QUANTUM NUMBER.
C  INPUT:  (I*4)  LL       = ORBITAL QUANTUM NUMBER FOR NL.
C
C  OUTPUT: (R*8)  AA()     = LJ RESOLVED A-VALUE.
C                            DIMENSION: TRANSITION INDEX WHERE:
C                                       1 GIVES LU+0.5 --> LL+0.5
C                                       2 GIVES LU+0.5 --> LL-0.5
C                                       3 GIVES LU-0.5 --> LL+0.5
C                                       4 GIVES LU-0.5 --> LL-0.5
C
C          (I*4)  I        = LOOP INDEX.
C
C          (R*8)  A        = L RESOLVED A VALUE.
C          (R*8)  XLU      = REAL VALUE = LU.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          R8ATAB     ADAS      RETURNS L RESOLVED HYDRONIC A-VALUE.
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    08/11/93
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    22ND MAY 1996
C
C VERSION: 1.1                          DATE: 22-05-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST VERSION. IBM VERSION NOT CHANGED
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      REAL*8    R8ATAB
C-----------------------------------------------------------------------
      INTEGER   MXJSHL  , IZ1     , NU      , LU      , NL      ,
     &          LL
      INTEGER   I
C-----------------------------------------------------------------------
      REAL*8    A       , XL
C-----------------------------------------------------------------------
      REAL*8    AA(2*MXJSHL)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C CLEAR OUTPUT ARRAY.
C-----------------------------------------------------------------------
C
      DO 1 I = 1 , 2*MXJSHL
         AA(I) = 0.0D0
    1 CONTINUE
C
C-----------------------------------------------------------------------
C GET L-RESOLVED A VALUES.
C-----------------------------------------------------------------------
C
      A = R8ATAB( IZ1 , NU , LU , NL , LL )
C
C-----------------------------------------------------------------------
C FILL OUTPUT ARRAY.
C-----------------------------------------------------------------------
C
      IF (A .GT. 0.0D0) THEN
C
         XL = DFLOAT( LU )
C
         IF (LL .EQ. (LU + 1)) THEN
C
            AA(2) = A / ( ( XL + 1.0D0 ) * ( 2.0D0 * XL + 3.0D0 ) )
            AA(1) = ( XL + 2.0D0 ) * ( 2.0D0 * XL + 1.0D0 ) * AA(2)
            IF (LU .NE. 0) THEN
               AA(4) = A
            ENDIF
C
         ELSE
C
            AA(1) = A
            AA(3) = A / ( XL * ( 2.0D0 * XL - 1.0D0 ) )
            IF (LL .NE. 0) THEN
               AA(4) = ( XL - 1.0D0 ) * ( 2.0D0 * XL + 1.0D0 ) * AA(3)
            ENDIF
C
         ENDIF
C
      ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN
      END
