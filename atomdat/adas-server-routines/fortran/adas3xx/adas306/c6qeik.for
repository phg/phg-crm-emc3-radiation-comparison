CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas306/c6qeik.for,v 1.2 2007/05/17 17:01:32 allan Exp $ Date $Date: 2007/05/17 17:01:32 $
CX
      SUBROUTINE C6QEIK( MXNSHL , MXJSHL , MXBEAM , IZ1    ,
     &                   IDONOR , NBOT   , NTOP   , NBEAM  ,
     &                   BMENA  , BMFRA  , QTHCH  , FTHCHJ
     &                 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C6QEIK *********************
C
C  PURPOSE:  USES THE EIKONIAL APPROXIMATION TO CALCULATE THE
C            THEORETICAL CHARGE EXCHANGE RATE COEFFICIENTS TO N SHELLS
C            AND THE NLJ FRACTIONS FROM NEUTRAL HYDROGEN OR HELIUM IN
C            GROUND OR EXCITED STATE TO A BARE NUCLEUS TARGET.
C
C  CALLING PROGRAM: ADAS306
C
C  INPUT : (I*4)  MXNSHL    = MAXIMUM NO. OF N SHELLS.
C  INPUT : (I*4)  MXJSHL    = MAXIMUM NO. OF J SUB-SHELLS.
C  INPUT : (I*4)  MXBEAM    = MAXIMUM NO. OF BEAM ENERGIES.
C  INPUT : (R*8)  IZ1       = CHARGE OF TARGET ION.
C  INPUT : (I*4)  IDONOR    = DONOR STATE FOR EIKONAL MODEL.
C                             1 = H(1S)    DONOR
C                             2 = H(2S)    DONOR
C                             3 = H(2P)    DONOR
C                             4 = H(1S2)   DONOR
C                             5 = HE(1S2S) DONOR
C  INPUT : (I*4)  NBOT      = MINIMUM PRINCIPAL QUANTUM NUMBER.
C  INPUT : (I*4)  NTOP      = MAXIMUM PRINCIPAL QUANTUM NUMBER.
C  INPUT : (I*4)  NBEAM     = NUMBER OF BEAM ENERGIES.
C  INPUT : (R*8)  BMENA()   = BEAM ENERGY COMPONENTS.
C                             UNITS: EV/AMU
C                             DIMENSION: COMPONENT INDEX.
C  INPUT : (R*8)  BMFRA()   = BEAM COMPONENT FRACTIONS.
C                             DIMENSION: COMPONENT INDEX.
C
C  OUTPUT: (R*8)  QTHCH()   = MEAN RATE COEFFICIENTS FOR N-LEVELS
C                             AVERAGED OVER BEAM FRACTIONS.
C                             UNITS: CM3 SEC-1
C                             DIMENSION: REFERENCED BY N QUANTUM NUMBER.
C  OUTPUT: (R*8)  FTHCHJ(,) = MEAN RATE COEFFICIENTS FOR NLJ-LEVELS
C                             AVERAGED OVER BEAM FRACTIONS. EXPRESSED AS
C                             FRACTIONS OF CORRESPONDING N-LEVELS.
C                             1ST DIMENSION: J SUB-SHELL
C                                            1 => J=L+0.5
C                                            2 => J=L-0.5
C                             2ND DIMENSION: REFERENCED BY I4IDFL(N,L)
C
C  PARAM : (I*4)  MXN       = 'MXNSHL'.
C
C          (I*4)  N         = N-SHELL INDEX.
C          (I*4)  L         = L-SHELL INDEX.
C          (I*4)  J         = J-SHELL INDEX.
C          (I*4)  IDL       = L-RESOLVED INDEX.
C
C          (R*8)  XL        = REAL VALUE = L.
C          (R*8)  WL        =
C
C          (R*8)  FTHCH()   = MEAN RATE COEFFICIENTS FOR NL-LEVELS
C                             AVERAGED OVER BEAM FRACTIONS. EXPRESSED AS
C                             FRACTIONS OF CORRESPONDING N-LEVELS.
C                             DIMENSION: REFERENCED BY I4IDFL(N,L)
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4UNIT     ADAS      RETURNS UNIT NO. FOR OUTPUT OF MESSAGES.
C          I4IDFL     ADAS      RETURNS UNIQUE INDEX GIVEN QUANTUM
C                               NUMBERS N AND L.
C          CXQEIK     ADAS      CALCULATES N-LEVEL AND NL-LEVEL CHARGE
C                               EXCHANGE RATE COEFFICIENTS USING EIKONAL
C                               APPROX. NL RATES ARE GIVEN AS FRACTION
C                               OF CORRESPONDING N RATE.
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    22/10/93
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    22ND MAY 1996
C
C VERSION: 1.1                          DATE: 22-05-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST VERSION. IBM VERSION NOT CHANGED
C
C VERSION: 1.2                          DATE: 17-05-07
C MODIFIED: Allan Whiteford
C               - Fixed typo in comments
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    I4UNIT  , I4IDFL
C-----------------------------------------------------------------------
      INTEGER    MXN
      PARAMETER( MXN = 20 )
C-----------------------------------------------------------------------
      INTEGER    MXNSHL  , MXJSHL  , MXBEAM  , IZ1     , IDONOR  ,
     &           NBOT    , NTOP    , NBEAM
      INTEGER    N       , L       , J       , IDL
C-----------------------------------------------------------------------
      REAL*8     XL      , WL
C-----------------------------------------------------------------------
      REAL*8     BMENA(MXBEAM)                 ,
     &           BMFRA(MXBEAM)                 ,
     &           QTHCH(MXNSHL)
      REAL*8     FTHCH((MXN*(MXN+1))/2)
      REAL*8     FTHCHJ(MXJSHL,(MXNSHL*(MXNSHL+1))/2)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C CHECK INTERNAL PARAMETER 'MXN'.
C-----------------------------------------------------------------------
C
      IF (MXN .LT. MXNSHL) THEN
         WRITE(I4UNIT(-1),1000) MXN, MXNSHL
         STOP
      ENDIF
C
C-----------------------------------------------------------------------
C CLEAR 'QTHCH' AND 'FTHCHJ' ARRAYS.
C-----------------------------------------------------------------------
C
      DO 1 N = 1 , MXNSHL
         QTHCH(N) = 0.00D+00
    1 CONTINUE
C
      DO 2 IDL = 1 , I4IDFL( MXNSHL , MXNSHL-1 )
         DO 3 J = 1 , MXJSHL
             FTHCHJ(J,IDL)=0.0D0
    3    CONTINUE
    2 CONTINUE
C
C-----------------------------------------------------------------------
C CALACULATE N AND NL CHARGE EXCHANGE RATES.
C-----------------------------------------------------------------------
C
      CALL CXQEIK( MXNSHL , MXBEAM , IZ1    , IDONOR , NBOT   ,
     &             NTOP   , NBEAM  , BMENA  , BMFRA  , QTHCH  ,
     &             FTHCH                                        )
C
C-----------------------------------------------------------------------
C SPLIT NL-LEVEL RATES INTO NLJ-RATES.
C-----------------------------------------------------------------------
C
      DO 4 N = 1 , MXNSHL
         DO 5 L = 0 , N-1
            XL = DFLOAT( L )
            WL = 2.0D0 * XL + 1.0D0
            IDL = I4IDFL( N , L )
            FTHCHJ(1,IDL) = ( XL + 1.0D0 ) * FTHCH(IDL) / WL
            FTHCHJ(2,IDL) = XL * FTHCH(IDL) / WL
    5    CONTINUE
    4 CONTINUE
C
C-----------------------------------------------------------------------
C
 1000 FORMAT( 1X, 32('*'), ' C6QEIK ERROR ', 32('*') //
     &        2X, 'INTERNAL PARAMETER ''MXN'' IS LESS THAN INPUT ',
     &            'ARGUMENT ''MXNSHL''.'/
     &        2X, 'MXN = ', I3 , '   MXNSHL = ', I3/
     &        2X, 'INCREASE PARAMETER ''MXN'' IN SUBROUTINE C6QEIK.' //
     &        1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
