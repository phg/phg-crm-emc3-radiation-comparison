CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas306/c6prsl.for,v 1.2 2004/07/06 11:53:12 whitefor Exp $ Date $Date: 2004/07/06 11:53:12 $
CX
      SUBROUTINE C6PRSL( MXNSHL , MXJSHL , MXPRSL , IZ0    ,
     &                   IZ1    , NPLINE , NPU    , NPL    ,
     &                   NUMAX  , WHIGH  , WLOW   , EM     ,
     &                   QEX    , TOTPOP , TOTEMI , AVRGWL ,
     &                   QEFF   , TBLPOP , TBLEMI , TBLWLN
     &                 )

      IMPLICIT NONE

C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C6PRSL *********************
C
C  PURPOSE:  FILLS TABLES FOR REQUESTED PREDICTIONS OF SPECTRUM LINES.
C
C  CALLING PROGRAM: C6EMIS
C
C  INPUT : (I*4)  MXNSHL    = MAXIMUM NUMBER OF N SHELLS.
C  INPUT : (I*4)  MXJSHL    = MAXIMUM NUMBER OF J SUB-SHELLS.
C  INPUT : (I*4)  MXPRSL    = MAXIMUM NUMBER OF SPECTRUM LINES TO
C                             PREDICT.
C  INPUT : (R*8)  IZ0       = NUCLEAR CHARGE.
C  INPUT : (R*8)  IZ1       = ION CHARGE.
C  INPUT : (I*4)  NPLINE    = NUMBER OF SPECTRUM LINES TO PREDICT.
C  INPUT : (I*4)  NPU()     = LIST OF UPPER PRINCIPAL QUANTUM NUMBERS
C                             OF SPECTRUM LINES TO PREDICT.
C                             DIMENSION: PREDICTED LINE INDEX.
C  INPUT : (I*4)  NPL()     = LIST OF LOWER PRINCIPAL QUANTUM NUMBERS
C                             OF SPECTRUM LINES TO PREDICT.
C                             DIMENSION: PREDICTED LINE INDEX.
C  INPUT : (I*4)  NUMAX     = MAXIMUM UPPER PRINCIPAL QUANTUM NUMBER
C                             FOR OBSERVED SPECTRUM LINES.
C  INPUT : (R*8)  WHIGH(,)  =
C                             1ST DIMENSION: J SHELL INDEX.
C                             2ND DIMENSION: REFERENCED BY L+1.
C  INPUT : (R*8)  WLOW(,,)  =
C                             1ST DIMENSION: J SHELL INDEX.
C                             2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C                             3RD DIMENSION: REFERENCED BY L+1.
C  INPUT : (R*8)  EM        = EMMISION MEASURE.
C  INPUT : (R*8)  QEX()     =
C                             DIMENSION: MXNSHL.
C
C  OUTPUT: (R*8)  TOTPOP()  = TOTAL COLLISION POP. FOR PREDICTED
C                             SPECTRUM LINE.
C                             UNITS: CM-2
C                             DIMENSION: PREDICTED LINE INDEX.
C  OUTPUT: (R*8)  TOTEMI()  = TOTAL COLLISION EMISSIVITIES FOR
C                             PREDICTED
C                             SPECTRUM LINE.
C                             UNITS: PH CM-2 SEC-1
C                             DIMENSION: PREDICTED LINE INDEX.
C  OUTPUT: (R*8)  AVRGWL()  = AVERAGE AIR WAVELENGTH FOR PREDICTED
C                             SPECTRUM LINE.
C                             UNITS: A
C                             DIMENSION: PREDICTED LINE INDEX.
C  OUTPUT: (R*8)  QEFF()    = EFF. RATE COEFFICIENT FOR PREDICTED
C                             SPECTRUM LINE.
C                             UNITS:
C                             DIMENSION: PREDICTED LINE INDEX.
C  OUTPUT: (R*8)  TBLPOP(,,)= TABLE OF COLLISION POP. FOR PREDICTED
C                             SPECTRUM LINE.
C                             UNITS: CM-2
C                             1ST DIMENSION: J->J' TRANSITION INDEX.
C                             2ND DIMENSION: REFERENCED BY I4IDLI().
C  OUTPUT: (R*8)  TBLEMI(,,)= TABLE OF COLLISION EMISSIVITIES FOR
C                             PREDICTED SPECTRUM LINE.
C                             UNITS: PH CM-2 SEC-1
C                             1ST DIMENSION: J->J' TRANSITION INDEX.
C                             2ND DIMENSION: REFERENCED BY I4IDLI().
C                             3RD DIMENSION: PREDICTED LINE INDEX.
C  OUTPUT: (R*8)  TBLWLN(,,)= TABLE OF WAVELENGTHS FOR PREDICTED
C                             SPECTRUM LINE.
C                             UNITS: A
C                             1ST DIMENSION: J->J' TRANSITION INDEX.
C                             2ND DIMENSION: REFERENCED BY I4IDLI().
C                             3RD DIMENSION: PREDICTED LINE INDEX.
C
C  PARAM : (I*4)  C1        = PRECISION AIR WAVELENGTH PARAM.
C  PARAM : (I*4)  C2        = PRECISION AIR WAVELENGTH PARAM.
C  PARAM : (I*4)  C3        = PRECISION AIR WAVELENGTH PARAM.
C  PARAM : (I*4)  C4        = PRECISION AIR WAVELENGTH PARAM.
C  PARAM : (I*4)  C5        = PRECISION AIR WAVELENGTH PARAM.
C  PARAM : (I*4)  RZ        = PRECISION AIR WAVELENGTH PARAM.
C
C          (I*4)  IN        = LOOP INDEX FOR SPECTRUM LINES.
C          (I*4)  N         = PRINCIPAL QUANTUM NUMBER OF INITIAL
C                             STATE.
C          (I*4)  L         = LOOP INDEX FOR ORBITAL QUANTUM NUMBER OF
C          (I*4)  J         = LOOP INDEX FOR J QUANTUM NUMBER OF
C                             INITIAL STATE.
C          (I*4)  N1        = PRINCIPAL QUANTUM NUMBER OF FINAL STATE.
C          (I*4)  L1        = LOOP INDEX FOR ORBITAL QUANTUM NUMBER OF
C                             FINAL STATE.
C          (I*4)  J1        = LOOP INDEX FOR J QUANTUM NUMBER OF
C                             FINAL STATE.
C          (I*4)  NP        = LOOP INDEX FOR PRINCIPAL QUANTUM NUMBER.
C          (I*4)  IDL       = TABLE INDEX.
C          (I*4)  ID        = TABLE INDEX.
C
C          (R*8)  Z1        = REAL VALUE = IZ1.
C          (R*8)  ZEFF1     = EFFECTIVE ION CHARGE.
C          (R*8)  ZEFF2     = EFFECTIVE ION CHARGE.
C          (R*8)  ZEFF3     = EFFECTIVE ION CHARGE.
C          (R*8)  SUM1      = SUM OF COL. POP. FOR PREDICTED LINE.
C                             UNITS: CM-2
C          (R*8)  SUM2      = SUM OF COL. EMIS. FOR PREDICTED LINE.
C                             UNITS: PH CM-2 SEC-1
C          (R*8)  SUM3      = SUM OF WAVELENGTHS FOR PREDICTED LINE.
C                             UNITS: A
C          (R*8)  EU0       = BINDING ENERGY
C                             UNITS: RYD
C          (R*8)  T1        = COL. POP. FOR PREDICTED SPECTRUM LINE.
C                             UNITS: CM-2
C          (R*8)  T2        = COL. EMIS. FOR PREDICTED SPECTRUM LINE.
C                             UNITS: PH CM-2 SEC-1
C          (R*8)  DELTA     =
C          (R*8)  SIG2      =
C          (R*8)  RF        =
C          (R*8)  WAVAIR    = WAVELENGTH FOR PREDICTED SPECTRUM LINE.
C                             UNITS: A
C
C          (R*8)  EL()      = BINDING ENERGY.
C                             UNITS: RYD
C                             DIMENSION: J SHELL INDEX WHERE:
C                                        1 => L+0.5
C                                        2 => L-0.5
C          (R*8)  EUJ()     = INITIAL STATE J RESOLVED ENERGY.
C                             DIMENSION: J SHELL INDEX WHERE:
C                                        1 => L+0.5
C                                        2 => L-0.5
C          (R*8)  AA()      = LJ RESOLVED A-VALUE.
C                             DIMENSION: TRANSITION INDEX WHERE:
C                                        1 => L+0.5 --> L'+0.5
C                                        2 => L+0.5 --> L'-0.5
C                                        3 => L-0.5 --> L'+0.5
C                                        4 => L-0.5 --> L'-0.5
C
C          (R*8)  ELJ(,)    = FINAL STATE J RESOLVED ENERGY.
C                             1ST DIMENSION: L SHELL INDEX WHERE:
C                                            1 => L'=L+1
C                                            2 => L'=L-1
C                             2ND DIMENSION: J SHELL INDEX WHERE:
C                                            1 => J'=L'+0.5
C                                            2 => J'=L'-0.5
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4UNIT     ADAS      RETURNS UNIT NO. FOR OUTPUT OF MESSAGES.
C          I4IDFL     ADAS      RETURNS UNIQUE INDEX GIVEN QUANTUM
C                               NUMBERS N AND L.
C          I4IDLI     ADAS      RETURNS INDEX FOR PREDICTED SPECTRUM
C                               LINE TABLES.
C          R8ZETA     ADAS
C          C6AJTB     ADAS      RETURNS LJ RESOLVED A-VALUES.
C
C NOTES:
C       1) THE J->J' TRANSITION INDEX IS AS FOLLOWS:
C              1 : J=L+0.5 -> J'=L'+0.5
C              2 : J=L+0.5 -> J'=L'-0.5
C              3 : J=L-0.5 -> J'=L'+0.5
C              4 : J=L-0.5 -> J'=L'-0.5
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    09/11/93
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    22ND MAY 1996
C
C VERSION: 1.1                          DATE: 22-05-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST VERSION. IBM VERSION NOT CHANGED
C
C
C VERSION: 1.2                          DATE: 08-04-99
C MODIFIED: Martin O'Mullane
C           - Corrected the vacuum to air conversion. Parameter C4 of the
C             expression for the refractive index of air should be 146.0 
C             and not 176.0. (see Astrophysical Quantities section 54)
C           - Introduced R8CONST to return fundamental atomic constants.
C             The mass dependent rydberg constant is returned if set
C             by XXRAMS in the main program. As a consequence RZ has
C             been removed from the parameter statement.
C
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    I4UNIT  , I4IDFL  , I4IDLI
      REAL*8     R8ZETA
C-----------------------------------------------------------------------
      INTEGER    MXJ
      PARAMETER( MXJ = 2 )
C-----------------------------------------------------------------------
      REAL*8     C1                , C2                ,
     &           C3                , C4                ,
     &           C5                
      PARAMETER( C1 = 6432.8D0     , C2 = 2949810.0D0  ,
     &           C3 = 25540.0D0    , C4 = 146.0D0      ,
     &           C5 = 41.0D0       )
C-----------------------------------------------------------------------
      INTEGER    MXNSHL  , MXJSHL  , MXPRSL  , IZ0     , IZ1     ,
     &           NPLINE  , NUMAX
      INTEGER    IN      , N       , L       , J       , N1      ,
     &           L1      , J1      , NP      , IDL     , ID
C-----------------------------------------------------------------------
      REAL*8     EM      , RZ      , R8CONST
      REAL*8     Z1      , ZEFF1   , ZEFF2   , ZEFF3   , SUM1    ,
     &           SUM2    , SUM3    , EU0     , T1      , T2      ,
     &           DELTA   , SIG2    , RF      , WAVAIR
C-----------------------------------------------------------------------
      INTEGER    NPU(MXPRSL)  , NPL(MXPRSL)
C-----------------------------------------------------------------------
      REAL*8     QEX(MXNSHL)     , TOTPOP(MXPRSL)  , TOTEMI(MXPRSL)  ,
     &           AVRGWL(MXPRSL)  , QEFF(MXPRSL)
      REAL*8     EL(MXJ)         , EUJ(MXJ)        , AA(2*MXJ)
C-----------------------------------------------------------------------
      REAL*8     WHIGH(MXJSHL,(MXNSHL*(MXNSHL+1))/2)
      REAL*8     ELJ(MXJ,MXJ)
C-----------------------------------------------------------------------
      REAL*8     WLOW(MXJSHL,(MXNSHL*(MXNSHL+1))/2,MXNSHL) ,
     &           TBLPOP(2*MXJSHL,2*MXNSHL-3,MXPRSL)        ,
     &           TBLEMI(2*MXJSHL,2*MXNSHL-3,MXPRSL)        ,
     &           TBLWLN(2*MXJSHL,2*MXNSHL-3,MXPRSL)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C CHECK PARAMETER 'MXJ'.
C-----------------------------------------------------------------------

      IF (MXJ .LT. MXJSHL) THEN
         WRITE(I4UNIT(-1),1000) MXJ, MXJSHL
         STOP
      ENDIF

C-----------------------------------------------------------------------

      Z1 = DFLOAT( IZ1 )

C Get the mass corrected Rydberg constant

      RZ = R8CONST('RYR')

C-----------------------------------------------------------------------

      DO 1 IN = 1 , NPLINE

         N  = NPU(IN)
         N1 = NPL(IN)

         SUM1 = 0.0D0
         SUM2 = 0.0D0
         SUM3 = 0.0D0

         DO 2 L = 0 , N-1

C-----------------------------------------------------------------------
C PRECISION ENERGY LEVEL CALCULATION.
C-----------------------------------------------------------------------

            IF (IZ1 .EQ. IZ0) THEN

               ZEFF1 = Z1
               ZEFF2 = Z1
               ZEFF3 = Z1
 
               CALL CXHYDE( IZ0 , Z1 , N , L , EU0 )
 
               L1 = L + 1
               IF (L1 .LT. N1) THEN
                  CALL CXHYDE( IZ0 , Z1 , N1 , L1 , EL(1) )
               ENDIF
 
               L1 = L - 1
               IF ((L1 .GE. 0) .AND. (L1 .LT. N1)) THEN
                  CALL CXHYDE( IZ0 , Z1 , N1 , L1 , EL(2) )
               ENDIF
 
            ELSE IF (IZ1 .EQ. (IZ0 - 2)) THEN
 
               ZEFF1 = Z1
               ZEFF2 = Z1
               ZEFF3 = Z1
 
               CALL CXLTHE( IZ0 , Z1 , N , L , EU0 )
 
               L1 = L + 1
               IF (L1 .LT. N1) THEN
                  CALL CXLTHE( IZ0 , Z1 , N1 , L1 , EL(1) )
               ENDIF
 
               L1 = L - 1
               IF ((L1 .GE. 0) .AND. (L1 .LT. N1)) THEN
                  CALL CXLTHE( IZ0 , Z1 , N1 , L1 , EL(2) )
               ENDIF
 
            ELSE IF (IZ1 .EQ. (IZ0 - 10)) THEN
 
               CALL CXSODE( IZ0 , N , L , ZEFF1 , EU0 )
 
               L1 = L + 1
               IF (L1 .LT. N1) THEN
                  CALL CXSODE( IZ0 , N1 , L1 , ZEFF2 , EL(1) )
               ENDIF
 
               L1 = L - 1
               IF ((L1 .GE. 0) .AND. (L1 .LT. N1)) THEN
                  CALL CXSODE( IZ0 , N1 , L1 , ZEFF3 , EL(2) )
               ENDIF
 
            ELSE
 
               WRITE(I4UNIT(-1),1001) IZ0 , IZ1
               STOP
 
            ENDIF
 
            IF (L .GT. 0) THEN
               EUJ(1) = DABS( -EU0 + 0.5D0 * DFLOAT( L ) *
     &                        R8ZETA( ZEFF1 , N , L ) )
               EUJ(2) = DABS( -EU0 - 0.5D0 * DFLOAT( L + 1 ) *
     &                        R8ZETA( ZEFF1 , N , L ) )
            ELSE
               EUJ(1) = EU0
               EUJ(2) = 0.0D0
            ENDIF
 
            L1 = L + 1
            IF (L1 .LT. N1) THEN
               ELJ(1,1) = DABS( -EL(1) + 0.5D0 * DFLOAT( L1 ) *
     &                          R8ZETA( ZEFF2 , N1, L1 ) )
               ELJ(1,2) = DABS( -EL(1) - 0.5D0 * DFLOAT( L1 + 1 ) *
     &                          R8ZETA( ZEFF2 , N1 , L1 ) )
            ELSE
               ELJ(1,1) = 0.0D0
               ELJ(1,2) = 0.0D0
            ENDIF

            L1 = L - 1
            IF ((L1 .GT. 0) .AND. (L1 .LT. N1)) THEN
               ELJ(2,1) = DABS( -EL(2) + 0.5D0 * DFLOAT( L1 ) *
     &                          R8ZETA( ZEFF3 , N1 , L1 ) )
               ELJ(2,2) = DABS( -EL(2) - 0.5D0 * DFLOAT( L1 + 1 ) *
     &                          R8ZETA( ZEFF3 , N1 , L1 ) )
            ELSE
               IF (L1 .EQ. 0) THEN
                  ELJ(2,1) = EL(2)
               ELSE
                  ELJ(2,1) = 0.0D0
               ENDIF
               ELJ(2,2) = 0.0D0
            ENDIF

C-----------------------------------------------------------------------

            IDL = I4IDFL( N , L )
            DO 3 J = 1 , 2

               IF (N .GT. NUMAX) THEN

                  T1 = WHIGH(J,IDL) * EM * QEX(NUMAX)
 
               ELSE
 
                  T1 = 0.0D0
                  DO 4 NP = N , NUMAX
                     T1 = T1 + WLOW(J,IDL,NP) * EM * QEX(NP)
    4             CONTINUE
 
               ENDIF
 
               SUM1 = SUM1 + T1
 
               DO 5 L1 = L+1 , L-1 , -2
 
                  CALL C6AJTB( MXJ , IZ1 , N , L , N1 , L1 , AA )
 
                  DO 6 J1 = 1 , 2
 
                     T2 = AA(2*(J-1)+J1) * T1
 
                     IF ((T2 .GT. 0.0D0) .AND.
     &                   (EUJ(J) .NE. 0) .AND.
     &                   (ELJ((3+L-L1)/2,J1) .NE. 0)) THEN
 
                        DELTA = DABS( RZ * ( EUJ(J) -
     &                                       ELJ((3+L-L1)/2,J1) ) )
                        SIG2 = DELTA**2 * 1.0D-8
                        RF = 1.0D0 + 1.0D-8 * ( C1 +
     &                       ( C2 / ( C4 - SIG2 ) ) +
     &                       ( C3 / ( C5 - SIG2 ) ) )
                        WAVAIR = 1.0D8 / ( DELTA * RF )
 
                        ID = I4IDLI( N1 , L , L1 )
                        TBLPOP(2*(J-1)+J1,ID,IN) = T1
                        TBLEMI(2*(J-1)+J1,ID,IN) = T2
                        TBLWLN(2*(J-1)+J1,ID,IN) = WAVAIR
 
                        SUM2 = SUM2 + T2
                        SUM3 = SUM3 + T2 * WAVAIR
 
                     ENDIF
 
    6             CONTINUE
    5          CONTINUE
    3       CONTINUE
    2    CONTINUE
 
C-----------------------------------------------------------------------

         TOTPOP(IN) = SUM1
         TOTEMI(IN) = SUM2
         AVRGWL(IN) = SUM3 / SUM2
         QEFF(IN)   = SUM2 / EM

C-----------------------------------------------------------------------

    1 CONTINUE

C-----------------------------------------------------------------------

 1000 FORMAT( 1X, 32('*'), ' C6PRSL ERROR ', 32('*') //
     &        2X, 'INTERNAL PARAMETER ''MXJ'' IS LESS THAN INPUT ',
     &            'ARGUMENT ''MXJSHL''.'/
     &        2X, 'MXJ = ', I3 , '   MXJSHL = ', I3/
     &        2X, 'INCREASE PARAMETER ''MXJ'' IN SUBROUTINE C6PRSL.' //
     &        1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )
 1001 FORMAT( 1X, 32('*'), ' C6PRSL ERROR ', 32('*') //
     &        2X, 'DIFFERENCE BETWEEN NUCLEAR AND ION CHARGE GIVES ',
     &            'UNKNOWN ION TYPE.'/
     &        2X, 'NUCLEAR CHARGE Z0 = ', I3 ,
     &            '   ION CHARGE Z1 = ', I3/
     &        2X, 'DIFFERENCE MUST BE 0 (H-LIKE), 2 (LI-LIKE), ',
     &            'OR 10 (NA-LIKE).' //
     &        1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )

C-----------------------------------------------------------------------

      RETURN
      END
