CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas306/c6ispf.for,v 1.2 2004/07/06 11:52:39 whitefor Exp $ Date $Date: 2004/07/06 11:52:39 $
CX
      SUBROUTINE C6ISPF( MXBEAM , MXOBSL , MXPRSL , MXTAB  ,
     &                   MXGRF  , NRTABS , NGFPLN , NGRND  ,
     &                   NTOT   , IPAN   , LPEND  , TITLE  ,
     &                   RAMSNO , TIEV   , TEV    , DENSZ  ,
     &                   DENS   , ZEFF   , BMAG   , NBEAM  ,
     &                   BMFRA  , BMENA  , ITHEOR , IBSTAT ,
     &                   IEMMS  , NOLINE , NU     , NL     ,
     &                   EMISA  , NPLINE , NPU    , NPL    ,
     &                   NTAB   , NGRF   , IDTAB  , IDGRF  ,
     &                   LRTTB  , LRTABS , LPLT1  , LGRD1  ,
     &                   LDEF1  , XLG    , XUG    , YLG    ,
     &                   YUG
     &                 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C6ISPF *********************
C
C  PURPOSE: PIPE COMMS WITH IDL INPUT SUBROUTINE FOR 'ADAS306'.
C
C  CALLING PROGRAM: ADAS306
C
C  INPUT : (I*4)   MXBEAM   = MAXIMUM NUMBER OF BEAM COMPONENTS.
C  INPUT : (I*4)   MXOBSL   = MAXIMUM NUMBER OF OBSERVED SPECTRUM LINES.
C  INPUT : (I*4)   MXPRSL   = MAXIMUM NUMBER OF SPECTRUM LINES TO
C                             PREDICT.
C  INPUT : (I*4)   MXTAB    = MAXIMUM NUMBER OF TABLES FOR OUTPUT.
C  INPUT : (I*4)   MXGRF    = MAXIMUM NUMBER OF GRAPHS FOR OUTPUT.
C  INPUT : (I*4)   NRTABS   = NUMBER OF INDIVIDUAL RATE TABLES.
C  INPUT : (I*4)   NGFPLN   = NUMBER OF GRAPHS PER PREDICTED SPECTRUM
C                             LINE.
C  INPUT : (I*4)   NGRND    = MINIMUM ALLOWED VALUE OF N QUANTUM NUMBER.
C  INPUT : (I*4)   NTOT     = MAXIMUM ALLOWED VALUE OF N QUANTUM NUMBER.
C
C  I/O   : (I*4)   IPAN     = ISPF PANEL NUMBER
C  I/O   : (L*4)   LPEND    = FLAGS IF END OF ANALYSIS REQUESTED.
C                             .TRUE.  => END ANALYSIS OF CURRENT DATA
C                                        SETS
C                             .FALSE. => CONTINUE PANALYSIS WITH CURRENT
C                                        DATA SETS
C
C  OUTPUT: (C*40)  TITLE    = IDL ENTERED GENERAL TITLE FOR PROGRAM RUN
C  OUTPUT: (R*8)   RAMSNO   = RECEIVER ATOMIC MASS.
C  OUTPUT: (R*8)   TIEV     = ION TEMPERATURE.
C                             UNITS: EV
C  OUTPUT: (R*8)   TEV      = ELECTRON TEMPERATURE.
C                             UNITS: EV
C  OUTPUT: (R*8)   DENSZ    = PLASMA ION DENSITY.
C                             UNITS: CM-3
C  OUTPUT: (R*8)   DENS     = ELECTRON DENSITY.
C                             UNITS: CM-3
C  OUTPUT: (R*8)   ZEFF     = EFFECTIVE ION CHARGE.
C  OUTPUT: (R*8)   BMAG     = PLASMA MAGNETIC INDUCTION.
C                             UNITS: TESLA
C  OUTPUT: (I*4)   NBEAM    = NUMBER OF BEAM ENERGIES.
C  OUTPUT: (R*8)   BMFRA()  = BEAM COMPONENT FRACTIONS.
C                             DIMENSION: COMPONENT INDEX.
C  OUTPUT: (R*8)   BMENA()  = BEAM ENERGY COMPONENTS.
C                             UNITS: EV/AMU
C                             DIMENSION: COMPONENT INDEX.
C  OUTPUT: (I*4)   ITHEOR   = CHARGE EXCHANGE MODEL OPTION.
C                             1 => USE INPUT DATA SET.
C                             2 => USE EIKONAL MODEL.
C  OUTPUT: (I*4)   IBSTAT   = DONOR STATE FOR EIKONAL MODEL.
C                             1 => H(1S)
C                             2 => H(2S)
C                             3 => H(2P)
C                             4 => HE(1S2)
C                             5 => HE(1S2S)
C  OUTPUT: (I*4)   IEMMS    = EMISSION MEASURE MODEL OPTION.
C                             1 => CHARGE EXCHANGE.
C                             2 => ELECTRON IMPACT EXCITATION.
C                             3 => RADIATIVE RECOMBINATION.
C  OUTPUT: (I*4)   NOLINE   = NUMBER OF OBSERVED SPECTRUM LINES.
C  OUTPUT: (I*4)   NU()     = LIST OF UPPER PRINCIPAL QUANTUM NUMBERS
C                             OF OBSERVED SPECTRUM LINES.
C                             DIMENSION: SPECTRUM LINE INDEX.
C  OUTPUT: (I*4)   NL()     = LIST OF LOWER PRINCIPAL QUANTUM NUMBERS
C                             OF OBSERVED SPECTRUM LINES.
C                             DIMENSION: SPECTRUM LINE INDEX.
C  OUTPUT: (R*8)   EMISA()  = LIST OF EMISSIVITIES OF OBSERVED SPECTRUM
C                             LINES.
C                             DIMENSION: SPECTRUM LINE INDEX.
C  OUTPUT: (I*4)   NPLINE   = NUMBER OF SPECTRUM LINES TO PREDICT.
C  OUTPUT: (I*4)   NPU()    = LIST OF UPPER PRINCIPAL QUANTUM NUMBERS
C                             OF SPECTRUM LINES TO PREDICT.
C                             DIMENSION: SPECTRUM LINE INDEX.
C  OUTPUT: (I*4)   NPL()    = LIST OF LOWER PRINCIPAL QUANTUM NUMBERS
C                             OF SPECTRUM LINES TO PREDICT.
C                             DIMENSION: SPECTRUM LINE INDEX.
C  OUTPUT: (I*4)   NTAB     = NUMBER OF TABLES FOR OUTPUT.
C  OUTPUT: (I*4)   NGRF     = NUMBER OF GRAPHS FOR OUTPUT.
C  OUTPUT: (I*4)   IDTAB()  = LIST OF INDEXES OF TABLES FOR OUTPUT.
C                             DIMENSION: MXTAB
C  OUTPUT: (I*4)   IDGRF()  = LIST OF INDEXES OF GRAPHS FOR OUTPUT.
C                             DIMENSION: MXGRF
C  OUTPUT: (L*4)   LRTTB    = FLAG FOR RATE TABLE PRINTING.
C                             .TRUE.  = PRINT RATE TABLES.
C                             .FALSE. = DO NOT PRINT RATE TABLES.
C  OUTPUT: (L*4)   LRTABS() = FLAGS FOR INDIVIDUAL TABLES.
C                             INDEX: 1 = DIRECT CAPTURE TABLE.
C                                    2 = FIELD DYNAMIC TABLE.
C                                    3 = FIELD STATIC TABLE.
C                                    4 = ION IMPACT TABLE.
C                                    5 = ELECTRON IMPACT TABLE.
C  OUTPUT: (L*4)   LPLT1    = FLAGS WHETHER TO PLOT GRAPHS ON SCREEN.
C                             .TRUE.  => PLOT GRAPHS ON SCREEN.
C                             .FALSE. => DO NOT PLOT GRAPHS ON SCREEN.
C  OUTPUT: (L*4)   LGRD1    = FLAGS WHETHER TO PUT GRAPHS IN GRID FILE.
C                             .TRUE.  => PUT GRAPHS IN GRID FILE
C                             .FALSE. => DO NOT PUT GRAPHS IN GRID FILE
C  OUTPUT: (L*4)   LDEF1    = FLAGS DEFAULT GRAPH SCALING
C                             .TRUE.  => USE DEFAULT GRAPH SCALING.
C                             .FALSE. => DO NOT USE DEFAULT SCALING.
C  OUTPUT: (R*8)   XLG()    = LOWER LIMIT FOR X-AXIS OF GRAPHS.
C                             DIMENSION: NGFPLN
C  OUTPUT: (R*8)   XUG()    = UPPER LIMIT FOR X-AXIS OF GRAPHS.
C                             DIMENSION: NGFPLN
C  OUTPUT: (R*8)   YLG()    = LOWER LIMIT FOR Y-AXIS OF GRAPHS.
C                             DIMENSION: NGFPLN
C  OUTPUT: (R*8)   YUG()    = UPPER LIMIT FOR Y-AXIS OF GRAPHS.
C                             DIMENSION: NGFPLN
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C AUTHOR:  JONATHAN NASH  (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    18/11/93
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    22ND MAY 1996
C
C VERSION: 1.1                          DATE: 22-05-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST VERSION
C
C VERSION: 1.2                          DATE: 29-05-96
C MODIFIED: WILLIAM OSBORN
C               - REMOVED UNUSED VARIABLES
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    MXBEAM  , MXOBSL  , MXPRSL  , MXTAB   , MXGRF   ,
     &           NGRND   , NTOT    , IPAN    , NBEAM   , ITHEOR  ,
     &           IBSTAT  , IEMMS   , NOLINE  , NPLINE  , NTAB    ,
     &           NGRF    , NRTABS  , NGFPLN  , I       , LOG
C-----------------------------------------------------------------------
      REAL*8     RAMSNO  , TIEV    , TEV     , DENSZ   , DENS    ,
     &           ZEFF    , BMAG
C-----------------------------------------------------------------------
      LOGICAL    LPEND   , LRTTB   , LPLT1   , LGRD1   , LDEF1
C-----------------------------------------------------------------------
      CHARACTER  TITLE*40
C-----------------------------------------------------------------------
      INTEGER    NU(MXOBSL)     , NL(MXOBSL)     , NPU(MXPRSL)    ,
     &           NPL(MXPRSL)    , IDTAB(MXTAB)   , IDGRF(MXGRF)
C-----------------------------------------------------------------------
      REAL*8     BMFRA(MXBEAM)  , BMENA(MXBEAM)  , EMISA(MXOBSL)  ,
     &           XLG(NGFPLN)    , XUG(NGFPLN)    , YLG(NGFPLN)    ,
     &           YUG(NGFPLN)
C-----------------------------------------------------------------------
      LOGICAL    LRTABS(NRTABS)
C-----------------------------------------------------------------------
      INTEGER     PIPEIN        , PIPEOU
      PARAMETER(  PIPEIN=5      , PIPEOU=6)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------

CC-----------------------------------------------------------------------
CC WRITE INPUT DATA TO IDL VIA PIPE - NOT NEEDED NOW
CC-----------------------------------------------------------------------
C      WRITE(PIPEOU,*)MXBEAM
C      CALL XXFLSH(PIPEOU)
C      WRITE(PIPEOU,*)MXOBSL
C      CALL XXFLSH(PIPEOU)
C      WRITE(PIPEOU,*)MXPRSL
C      CALL XXFLSH(PIPEOU)
C      WRITE(PIPEOU,*)MXTAB
C      CALL XXFLSH(PIPEOU)
C      WRITE(PIPEOU,*)MXGRF
C      CALL XXFLSH(PIPEOU)
C      WRITE(PIPEOU,*)NRTABS
C      CALL XXFLSH(PIPEOU)
C      WRITE(PIPEOU,*)NGFPLN
C      CALL XXFLSH(PIPEOU)
C      WRITE(PIPEOU,*)NGRND
C      CALL XXFLSH(PIPEOU)
C      WRITE(PIPEOU,*)NTOT
C      CALL XXFLSH(PIPEOU)
C
C-----------------------------------------------------------------------
C READ OUTPUT DATA FROM IDL VIA PIPE
C-----------------------------------------------------------------------
      READ(PIPEIN,*)LOG
      IF(LOG.EQ.1)THEN
         LPEND=.TRUE.
      ELSE
         LPEND=.FALSE.
      ENDIF
      READ(PIPEIN,'(A)')TITLE
      READ(PIPEIN,*)RAMSNO
      READ(PIPEIN,*)TIEV
      READ(PIPEIN,*)TEV
      READ(PIPEIN,*)DENSZ
      READ(PIPEIN,*)DENS
      READ(PIPEIN,*)ZEFF
      READ(PIPEIN,*)BMAG
      READ(PIPEIN,*)ITHEOR
      READ(PIPEIN,*)IBSTAT
      READ(PIPEIN,*)IEMMS
      READ(PIPEIN,*)NBEAM
      DO 1 I=1,NBEAM
         READ(PIPEIN,*)BMFRA(I)
         READ(PIPEIN,*)BMENA(I)
 1    CONTINUE
      READ(PIPEIN,*)NOLINE
      DO 3 I=1,NOLINE
         READ(PIPEIN,*)NU(I)
         READ(PIPEIN,*)NL(I)
         READ(PIPEIN,*)EMISA(I)
 3    CONTINUE
      READ(PIPEIN,*)NPLINE
      DO 6 I=1,NPLINE
         READ(PIPEIN,*)NPU(I)
         READ(PIPEIN,*)NPL(I)
 6    CONTINUE
      READ(PIPEIN,*)LOG
      IF(LOG.EQ.1)THEN
         LRTTB = .TRUE.
      ELSE
         LRTTB = .FALSE.
      ENDIF
      READ(PIPEIN,*)NGRF
      IF (NGRF .GT. 0) THEN
         DO 9 I=1,NGRF
            READ(PIPEIN,*)IDGRF(I)
 9       CONTINUE
      ENDIF
      READ(PIPEIN,*)NTAB
      IF (NTAB .GT. 0) THEN
         DO 8 I=1,NTAB
            READ(PIPEIN,*)IDTAB(I)
 8       CONTINUE
      ENDIF
      DO 10 I=1,NRTABS
         READ(PIPEIN,*)LOG
         IF(LOG.EQ.1)THEN
            LRTABS(I) = .TRUE.
         ELSE
            LRTABS(I) = .FALSE.
         ENDIF
 10   CONTINUE

C
C-----------------------------------------------------------------------
C
      RETURN
      END
