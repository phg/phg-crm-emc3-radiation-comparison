CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas306/c6wfil.for,v 1.1 2004/07/06 11:54:02 whitefor Exp $ Date $Date: 2004/07/06 11:54:02 $
CX
      SUBROUTINE C6WFIL( MXNSHL , MXJSHL , IZ1    , NGRND  ,
     &                   NTOT   , NBOT   , NUMAX  , DENSZ  ,
     &                   DENS   , QTHEOR , FTHEOR , QTHIN  ,
     &                   TBQMEP , TBQMEM , TBQMIP , TBQMIM ,
     &                   TBFMP  , TBFM   , TBFMM  , WHIGH  ,
     &                   WLOW
     &                 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C6WFIL *********************
C
C  PURPOSE:
C
C  CALLING PROGRAM: C6EMIS
C
C  INPUT : (I*4)  MXNSHL   = MAXIMUM VALUE OF PRINCIPAL QUANTUM NUMBER.
C  INPUT : (I*4)  MXJSHL   = MAXIMUM NUMBER OF J SUB-SHELLS.
C  INPUT : (I*4)  IZ1      = ION CHARGE.
C  INPUT : (I*4)  NGRND    = PRINCIPAL QUANTUM NUMBER OF GROUND STATE.
C  INPUT : (I*4)  NTOT     = PRINCIPAL QUANTUM NUMBER OF HIGHEST BOUND
C                            STATE.
C  INPUT : (I*4)  NBOT     = MINIMUM PRINCIPAL QUANTUM NUMBER.
C  INPUT : (I*4)  NUMAX    = MAXIMUM UPPER PRINCIPAL QUANTUM NUMBER FOR
C                            OBSERVED SPECTRUM LINES.
C  INPUT : (R*8)  DENSZ    = PLASMA ION DENSITY.
C                            UNITS: CM-3
C  INPUT : (R*8)  DENS     = ELECTRON DENSITY.
C                            UNITS: CM-3
C  INPUT : (R*8)  QTHEOR() = MEAN CHARGE EXCHANGE, EXCITATION RATE OR
C                            RECOMBINATION RATE COEFFICIENTS FOR
C                            N-LEVELS AVERAGED OVER BEAM FRACTIONS.
C                            UNITS: CM3 SEC-1
C                            DIMENSION: REFERENCED BY N QUANTUM NUMBER.
C  INPUT : (R*8)  FTHEOR(,)= FRACTION OF N-LEVEL MEAN CHARGE EXCHANGE,
C                            EXCITATION RATE OR RECOMBINATION RATE
C                            COEFFICIENTS IN NL-LEVEL.
C                            1ST DIMENSION: J SHELL INDEX WHERE:
C                                           1 GIVES J=L+0.5
C                                           2 GIVES J=L-0.5
C                            2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C  INPUT : (R*8)  QTHIN()  = IONISATION RATE COEFFICIENT.
C                            UNITS: CM3 SEC-1
C                            DIMENSION: N SHELL INDEX.
C  INPUT : (R*8)  TBQMEP(,)= ELECTRON COLLISIONAL RATE COEFFT. FOR
C                            NLJ->NL+1J'.
C                            1ST DIMENSION: J->J' TRANSITION INDEX.
C                            2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C  INPUT : (R*8)  TBQMEM(,)= ELECTRON COLLISIONAL RATE COEFFT. FOR
C                            NLJ->NL-1J'.
C                            1ST DIMENSION: J->J' TRANSITION INDEX.
C                            2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C  INPUT : (R*8)  TBQMIP(,)= POSITIVE ION COLLISIONAL RATE COEFFT. FOR
C                            NLJ->NL+1J'.
C                            1ST DIMENSION: J->J' TRANSITION INDEX.
C                            2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C  INPUT : (R*8)  TBQMIM(,)= POSITIVE ION COLLISIONAL RATE COEFFT. FOR
C                            NLJ->NL-1J'.
C                            1ST DIMENSION: J->J' TRANSITION INDEX.
C                            2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C  INPUT : (R*8)  TBFMP(,) = B-FIELD DEPENDENT MIXING RATE COEFFT. FOR
C                            NLJ->NL+1J'.
C                            1ST DIMENSION: J->J' TRANSITION INDEX.
C                            2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C  INPUT : (R*8)  TBFM(,)  = B-FIELD DEPENDENT MIXING RATE COEFFT. FOR
C                            NLJ->NLJ'.
C                            1ST DIMENSION: J->J' TRANSITION INDEX.
C                            2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C  INPUT : (R*8)  TBFMM(,) = B-FIELD DEPENDENT MIXING RATE COEFFT. FOR
C                            NLJ->NL-1J'.
C                            1ST DIMENSION: J->J' TRANSITION INDEX.
C                            2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C
C  OUTPUT: (R*8)  WHIGH(,) =
C                            1ST DIMENSION: J SHELL INDEX.
C                            2ND DIMENSION: REFERENCED BY L+1.
C  OUTPUT: (R*8)  WLOW(,,) =
C                            1ST DIMENSION: J SHELL INDEX.
C                            2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C                            3RD DIMENSION: REFERENCED BY L+1.
C
C  PARAM : (I*4)  MXN      = MXNSHL.
C  PARAM : (I*4)  MXJ      = MXJSHL.
C
C          (I*4)  N       = LOOP INDEX FOR PRINCIPAL QUANTUM NUMBER.
C          (I*4)  L       = LOOP INDEX FOR ORBITAL QUANTUM NUMBER.
C          (I*4)  J       = LOOP INDEX FOR J QUANTUM NUMBER.
C          (I*4)  N1      = LOOP INDEX FOR PRINCIPAL QUANTUM NUMBER.
C          (I*4)  L1      = LOOP INDEX FOR ORBITAL QUANTUM NUMBER.
C          (I*4)  N2      = LOOP INDEX FOR PRINCIPAL QUANTUM NUMBER.
C          (I*4)  L2      = LOOP INDEX FOR ORBITAL QUANTUM NUMBER.
C          (I*4)  LP      = ARRAY INDEX = L+1.
C          (I*4)  IDL     = ARRAY INDEX.
C
C          (R*8)  AA()    = LJ RESOLVED A-VALUE.
C                           DIMENSION: TRANSITION INDEX WHERE:
C                                      1 GIVES LU+0.5 --> LL+0.5
C                                      2 GIVES LU+0.5 --> LL-0.5
C                                      3 GIVES LU-0.5 --> LL+0.5
C                                      4 GIVES LU-0.5 --> LL-0.5
C
C          (R*8)  RHS()   = RIGHT HAND SIDE OF N LEVEL EQUATION.
C                           1ST DIMENSION: MXJ.
C                           2ND DIMENSION: MXN.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4UNIT     ADAS      RETURNS UNIT NO. FOR OUTPUT OF MESSAGES.
C          I4IDFL     ADAS      RETURNS UNIQUE INDEX GIVEN QUANTUM
C                               NUMBERS N AND L.
C          C6WSOL     ADAS
C
C NOTES:
C       1) THE J->J' TRANSITION INDEX IS AS FOLLOWS:
C              1 : J=L+0.5 -> J'=L'+0.5
C              2 : J=L+0.5 -> J'=L'-0.5
C              3 : J=L-0.5 -> J'=L'+0.5
C              4 : J=L-0.5 -> J'=L'-0.5
C
C       2) FOR MATRIX HANDLING PURPOSES, L=0 STATES ARE TREATED AS TWO
C          STATES OF J=0.5. RATES INTO EACH OF THESE ARE SET EQUAL TO
C          HALF THE TOTAL INTO A TRUE L=0,J=0.5 STATE. RATES OUT OF
C          EACH STATE ARE SET EQUAL TO THE TOTAL OUT OF A TRUE L=0,J=0.5
C          STATE. THUS EACH ARTIFICIAL STATE HAS THE SAME POPULATION
C          WHICH IS HALF THE TOTAL OF THE TRUE L=0,J=0.5 STATE. THE
C          POPULATIONS ARE RECOMBINED AT THE END OF THE MATRIX
C          MANIPULATIONS.
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    08/10/93
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    22ND MAY 1996
C
C VERSION: 1.1                          DATE: 22-05-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST VERSION. IBM VERSION NOT CHANGED
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    I4UNIT  , I4IDFL
C-----------------------------------------------------------------------
      INTEGER    MXN       , MXJ
      PARAMETER( MXN = 20  , MXJ = 2 )
C-----------------------------------------------------------------------
      INTEGER    MXNSHL  , MXJSHL  , IZ1     , NGRND   , NTOT   ,
     &           NBOT    , NUMAX
      INTEGER    N       , L       , J       , N1      , L1     ,
     &           N2      , L2      , LP      , IDL
C-----------------------------------------------------------------------
      REAL*8     DENSZ   , DENS
C-----------------------------------------------------------------------
      REAL*8     QTHEOR(MXNSHL)  , QTHIN(MXNSHL)
      REAL*8     AA(2*MXJ)
C-----------------------------------------------------------------------
      REAL*8     FTHEOR(MXJSHL,(MXNSHL*(MXNSHL+1))/2)    ,
     &           TBQMEP(2*MXJSHL,(MXNSHL*(MXNSHL+1))/2)  ,
     &           TBQMEM(2*MXJSHL,(MXNSHL*(MXNSHL+1))/2)  ,
     &           TBQMIP(2*MXJSHL,(MXNSHL*(MXNSHL+1))/2)  ,
     &           TBQMIM(2*MXJSHL,(MXNSHL*(MXNSHL+1))/2)  ,
     &           TBFMM(2*MXJSHL,(MXNSHL*(MXNSHL+1))/2)   ,
     &           TBFM(2*MXJSHL,(MXNSHL*(MXNSHL+1))/2)    ,
     &           TBFMP(2*MXJSHL,(MXNSHL*(MXNSHL+1))/2)   ,
     &           WHIGH(MXJSHL,(MXNSHL*(MXNSHL+1))/2)
      REAL*8     RHS(MXJ,MXN)
C-----------------------------------------------------------------------
      REAL*8     WLOW(MXJSHL,(MXNSHL*(MXNSHL+1))/2,MXNSHL)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C CHECK PARAMETERS 'MXN' AND 'MXJ'.
C-----------------------------------------------------------------------
C
      IF (MXN .LT. MXNSHL) THEN
         WRITE(I4UNIT(-1),1000) MXN, MXNSHL
         STOP
      ENDIF
C
      IF (MXJ .LT. MXJSHL) THEN
         WRITE(I4UNIT(-1),1001) MXJ, MXJSHL
         STOP
      ENDIF
C
C-----------------------------------------------------------------------
C INITIALISE VECTOR WHIGH.
C-----------------------------------------------------------------------
C
      DO 1 N = NUMAX+1 , NTOT
C
         DO 2 L = 0 , N-1
            IDL = I4IDFL( N , L )
            DO 3 J = 1 , 2
               WHIGH(J,IDL) = QTHEOR(N) * FTHEOR(J,IDL) / QTHEOR(NUMAX)
    3       CONTINUE
    2    CONTINUE
C
         IDL = I4IDFL( N , 0 )
         WHIGH(1,IDL) = 0.5D0 * WHIGH(1,IDL)
         WHIGH(2,IDL) = WHIGH(1,IDL)
C
    1 CONTINUE
C
C-----------------------------------------------------------------------
C INITIALISE WLOW DIAGONAL VECTORS AND ZERO OTHER COLUMNS.
C-----------------------------------------------------------------------
C
      DO 4 N = 1 , NUMAX
         DO 5 L = 0 , N-1
C
            IDL = I4IDFL( N , L )
            DO 6 N1 = N+1 , NUMAX
               DO 7 J = 1 , 2
                  WLOW(J,IDL,N1) = 0.0D0
    7          CONTINUE
    6       CONTINUE
C
            DO 8 J = 1 , 2
               WLOW(J,IDL,N) = FTHEOR(J,IDL)
    8       CONTINUE
C
    5    CONTINUE
C
         IDL = I4IDFL( N , 0 )
         WLOW(1,IDL,N) = 0.5D0 * WLOW(1,IDL,N)
         WLOW(2,IDL,N) = WLOW(1,IDL,N)
C
    4 CONTINUE
C
C-----------------------------------------------------------------------
C MAIN DOWNWARD RECURSION FROM MXNSHL TO NUMAX+1.
C-----------------------------------------------------------------------
C
      DO 9 N = MXNSHL , NUMAX+1 , -1
C
C-----------------------------------------------------------------------
C COPY WHIGH COMPONENTS INTO RHS AND ADD CASCADE CONTRIBUTIONS FROM
C LEVELS GREATER THAN N.
C-----------------------------------------------------------------------
C
         DO 10 L = 0 , N-1
C
            LP = L + 1
            DO 11 J = 1 , 2
               RHS(J,LP) = WHIGH(J,I4IDFL( N , L ))
   11       CONTINUE
C
            DO 12 N1 = N+1 , MXNSHL
C
               L1 = L + 1
               CALL C6AJTB( MXJ , IZ1 , N1 , L1 , N , L , AA )
               IF (L .EQ. 0) THEN
                  AA(1) = 0.5D0 * AA(1)
                  AA(2) = AA(1)
                  AA(3) = 0.5D0 * AA(3)
                  AA(4) = AA(3)
               ENDIF
C
               DO 13 J = 1 , 2
                  RHS(J,LP) = RHS(J,LP) +
     &                        AA(J) * WHIGH(1,I4IDFL( N1, L1 )) +
     &                        AA(J+2) * WHIGH(2,I4IDFL( N1, L1 ))
   13          CONTINUE
C
               L1 = L - 1
               CALL C6AJTB( MXJ , IZ1 , N1 , L1 , N , L , AA )
               IF (L1 .EQ. 0) THEN
                  AA(3) = AA(1)
                  AA(4) = AA(2)
               ENDIF
C
               DO 14 J = 1 , 2
                  RHS(J,LP) = RHS(J,LP) +
     &                        AA(J) * WHIGH(1,I4IDFL( N1, L1 )) +
     &                        AA(J+2) * WHIGH(2,I4IDFL( N1, L1 ))
   14          CONTINUE
C
   12       CONTINUE
   10    CONTINUE
C
C-----------------------------------------------------------------------
C SET UP AND SOLVE MATRIX M FOR LEVEL N.
C-----------------------------------------------------------------------
C
         CALL C6WSOL( MXNSHL , MXJSHL , IZ1    , NGRND  ,
     &                N      , DENSZ  , DENS   , QTHIN  ,
     &                TBQMEP , TBQMEM , TBQMIP , TBQMIM ,
     &                TBFMM  , TBFM   , TBFMP  , RHS      )
C
C-----------------------------------------------------------------------
C COPY RESULTS INTO WHIGH
C-----------------------------------------------------------------------
C
         DO 15 L = 0 , N-1
            DO 16 J = 1 , 2
               WHIGH(J,I4IDFL( N , L )) = RHS(J,L+1)
   16       CONTINUE
   15    CONTINUE
C
C-----------------------------------------------------------------------
C
    9 CONTINUE
C
C-----------------------------------------------------------------------
C  ADD ALL CASCADE CONTRIBUTIONS FROM LEVELS ABOVE NUMAX INTO LEVELS
C  NGRND TO NUMAX INTO LAST COLUMN ON WLOW ARRAY. THESE ALL DEPEND ON
C  Q (CH. EX.) AT NUMAX.
C-----------------------------------------------------------------------
C
      DO 17 N = NGRND , NUMAX
         DO 18 L = 0 , N-1
C
            IDL = I4IDFL( N , L )
C
            DO 19 N1 = NUMAX+1 , MXNSHL
C
               L1 = L + 1
               CALL C6AJTB( MXJ , IZ1 , N1 , L1 , N , L , AA )
               IF (L .EQ. 0) THEN
                  AA(1) = 0.5D0 * AA(1)
                  AA(2) = AA(1)
                  AA(3) = 0.5D0 * AA(3)
                  AA(4) = AA(3)
               ENDIF
C
               DO 20 J = 1 , 2
                  WLOW(J,IDL,NUMAX) = WLOW(J,IDL,NUMAX) +
     &                             AA(J) * WHIGH(1,I4IDFL( N1 , L1 )) +
     &                             AA(J+2) * WHIGH(2,I4IDFL( N1 , L1 ))
   20          CONTINUE
C
               L1 = L - 1
               CALL C6AJTB( MXJ , IZ1 , N1 , L1 , N , L , AA )
               IF (L1 .EQ. 0) THEN
                  AA(3) = AA(1)
                  AA(4) = AA(2)
               ENDIF
C
               DO 21 J = 1 , 2
                  WLOW(J,IDL,NUMAX) = WLOW(J,IDL,NUMAX) +
     &                             AA(J) * WHIGH(1,I4IDFL( N1 , L1 )) +
     &                             AA(J+2) * WHIGH(2,I4IDFL( N1 , L1 ))
   21          CONTINUE
C
   19       CONTINUE
   18    CONTINUE
   17 CONTINUE
C
C-----------------------------------------------------------------------
C START MAIN REDUCTION LOOP DOWNWARD FROM NUMAX TO NBOT.
C-----------------------------------------------------------------------
C
      DO 22 N = NUMAX , NBOT , -1
         DO 23 N1 = N , NUMAX
C
C-----------------------------------------------------------------------
C COPY WLOW COMPONENTS INTO RHS.
C-----------------------------------------------------------------------
C
            DO 24 L = 0 , N-1
               DO 25 J = 1 , 2
                  RHS(J,L+1) = WLOW(J,I4IDFL( N , L ),N1)
   25          CONTINUE
   24       CONTINUE
C
C-----------------------------------------------------------------------
C SET UP AND SOLVE MATRIX M FOR LEVEL N.
C-----------------------------------------------------------------------
C
            CALL C6WSOL( MXNSHL , MXJSHL , IZ1    , NGRND  ,
     &                   N      , DENSZ  , DENS   , QTHIN  ,
     &                   TBQMEP , TBQMEM , TBQMIP , TBQMIM ,
     &                   TBFMM  , TBFM   , TBFMP  , RHS      )
C
C-----------------------------------------------------------------------
C COPY RESULT TO WLOW.
C-----------------------------------------------------------------------
C
         DO 26 L = 0 , N-1
            DO 27 J = 1 , 2
               WLOW(J,I4IDFL( N , L ),N1) = RHS(J,L+1)
   27       CONTINUE
   26    CONTINUE
C
C-----------------------------------------------------------------------
C  ADD IN CASCADE CONTRIBUTIONS TO LOWER LEVELS IN APPROPRIATE COLUMNS
C  OF WLOW.
C-----------------------------------------------------------------------
C
         DO  28 N2 = NGRND , N-1
            DO 29 L2 = 0 , N2-1
C
               IDL = I4IDFL( N2 , L2 )
C
               L1 = L2 + 1
               CALL C6AJTB( MXJ , IZ1 , N , L1 , N2 , L2 , AA )
               IF (L2 .EQ. 0) THEN
                  AA(1) = 0.5D0 * AA(1)
                  AA(2) = AA(1)
                  AA(3) = 0.5D0 * AA(3)
                  AA(4) = AA(3)
               ENDIF
C
               DO 30 J = 1 , 2
                  WLOW(J,IDL,N1) = WLOW(J,IDL,N1) +
     &                           AA(J) * WLOW(1,I4IDFL( N , L1 ),N1) +
     &                           AA(J+2) * WLOW(2,I4IDFL( N , L1 ),N1)
   30          CONTINUE
C
               L1 = L2 - 1
               CALL C6AJTB( MXJ , IZ1 , N , L1 , N2 , L2 , AA )
               IF (L1 .EQ. 0) THEN
                  AA(3) = AA(1)
                  AA(4) = AA(2)
               ENDIF
C
               DO 31 J = 1 , 2
                  WLOW(J,IDL,N1) = WLOW(J,IDL,N1) +
     &                           AA(J) * WLOW(1,I4IDFL( N , L1 ),N1) +
     &                           AA(J+2) * WLOW(2,I4IDFL( N , L1 ),N1)
   31          CONTINUE
C
   29       CONTINUE
   28    CONTINUE
C
C-----------------------------------------------------------------------
C
   23  CONTINUE
   22  CONTINUE
C
C-----------------------------------------------------------------------
C CORRECT TO NORMAL L=0,J=0.5 POPULATIONS.
C-----------------------------------------------------------------------
C
      DO 32 N = NUMAX+1 , NTOT
         IDL = I4IDFL( N , 0 )
         WHIGH(1,IDL) = 2.0D0 * WHIGH(1,IDL)
         WHIGH(2,IDL) = 0.0D0
   32 CONTINUE
C
      DO 33 N = NBOT , NUMAX
         IDL = I4IDFL( N , 0 )
         DO 34 N1 = NBOT , NUMAX
            WLOW(1,IDL,N1) = 2.0D0 * WLOW(1,IDL,N1)
            WLOW(2,IDL,N1) = 0.0D0
   34 CONTINUE
   33 CONTINUE
C
C-----------------------------------------------------------------------
C
 1000 FORMAT( 1X, 32('*'), ' C6WFIL ERROR ', 32('*') //
     &        2X, 'INTERNAL PARAMETER ''MXN'' IS LESS THAN INPUT ',
     &            'ARGUMENT ''MXNSHL''.'/
     &        2X, 'MXN = ', I3 , '   MXNSHL = ', I3/
     &        2X, 'INCREASE PARAMETER ''MXN'' IN SUBROUTINE C6WFIL.' //
     &        1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )
 1001 FORMAT( 1X, 32('*'), ' C6WFIL ERROR ', 32('*') //
     &        2X, 'INTERNAL PARAMETER ''MXJ'' IS LESS THAN INPUT ',
     &            'ARGUMENT ''MXJSHL''.'/
     &        2X, 'MXJ = ', I3 , '   MXJSHL = ', I3/
     &        2X, 'INCREASE PARAMETER ''MXJ'' IN SUBROUTINE C6WFIL.' //
     &        1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
