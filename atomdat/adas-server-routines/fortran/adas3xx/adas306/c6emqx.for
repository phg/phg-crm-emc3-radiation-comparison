CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas306/c6emqx.for,v 1.2 2004/07/06 11:52:30 whitefor Exp $ Date $Date: 2004/07/06 11:52:30 $
CX
      SUBROUTINE C6EMQX( MXNSHL , MXJSHL , MXOBSL , IZ1    ,
     &                   NBOT   , NUMIN  , NUMAX  , NU     ,
     &                   NL     , EMISA  , NREP   , ICREP  ,
     &                   QTHEOR , WLOW   , EM     , QEX
     &                 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C6EMQX *********************
C
C  PURPOSE:
C
C  CALLING PROGRAM: C6EMIS
C
C  INPUT : (I*4)  MXNSHL   = MAXIMUM NUMBER OF N SHELLS.
C  INPUT : (I*4)  MXJSHL   = MAXIMUM NUMBER OF J SUB-SHELLS.
C  INPUT : (I*4)  MXOBSL   = MAXIMUM NUMBER OF OBSERVED SPECTRUM LINES.
C  INPUT : (I*4)  IZ1      = ION CHARGE.
C  INPUT : (I*4)  NBOT     = MINIMUM VALUE OF PRINCIPAL QUANTUM NUMBER.
C  INPUT : (I*4)  NUMIN    = MINIMUM UPPER PRINCIPAL QUANTUM NUMBER FOR
C                            OBSERVED SPECTRUM LINES.
C  INPUT : (I*4)  NUMAX    = MAXIMUM UPPER PRINCIPAL QUANTUM NUMBER FOR
C                            OBSERVED SPECTRUM LINES.
C  INPUT : (I*4)  NU()     = LIST OF UPPER PRINCIPAL QUANTUM NUMBERS OF
C                            OBSERVED SPECTRUM LINES.
C                            DIMENSION: SPECTRUM LINE INDEX.
C  INPUT : (I*4)  NL()     = LIST OF LOWER PRINCIPAL QUANTUM NUMBERS OF
C                            OBSERVED SPECTRUM LINES.
C                            DIMENSION: SPECTRUM LINE INDEX.
C  INPUT : (R*8)  EMISA()  = LIST OF EMISSIVITIES OF OBSERVED SPECTRUM
C                            LINES.
C                            DIMENSION: SPECTRUM LINE INDEX.
C  INPUT : (I*4)  NREP     =
C  INPUT : (I*4)  ICREP()  =
C                            DIMENSION: MXOBSL.
C  INPUT : (R*8)  QTHEOR() = MEAN CHARGE EXCHANGE, EXCITATION RATE OR
C                            RECOMBINATION RATE COEFFICIENTS FOR
C                            N-LEVELS AVERAGED OVER BEAM FRACTIONS.
C                            UNITS: CM3 SEC-1
C                            DIMENSION: N SHELL INDEX.
C  INPUT : (R*8)  WLOW(,,) =
C                            1ST DIMENSION: J SHELL INDEX.
C                            2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C                            3RD DIMENSION: REFERENCED BY L+1.
C
C  OUTPUT: (R*8)  EM       = EMMISSION MEASURE.
C  OUTPUT: (R*8)  QEX()    =
C                            DIMENSION: MXNSHL.
C
C  PARAM : (I*4)  MXN      = MXNSHL.
C  PARAM : (I*4)  MXJ      = MXJSHL.
C  PARAM : (I*4)  MXOB     = MXOBSL.
C
C          (I*4)  N        = LOOP INDEX FOR PRINCIPAL QUANTUM NUMBER.
C          (I*4)  L        = LOOP INDEX FOR ORBITAL QUANTUM NUMBER.
C          (I*4)  N1       = LOOP INDEX FOR PRINCIPAL QUANTUM NUMBER.
C          (I*4)  L1       = LOOP INDEX FOR ORBITAL QUANTUM NUMBER.
C          (I*4)  IDL      = TABLE INDEX.
C          (I*4)  IC       = ARRAY INDEX.
C          (I*4)  IC1      = ARRAY INDEX.
C          (I*4)  IC2      = ARRAY INDEX.
C          (I*4)  IFAIL    = RETURN FLAG FROM NAG ROUTINE.
C          (I*4)  M        = LOOP INDEX.
C          (I*4)  I        = LOOP INDEX.
C          (I*4)  J        = ARRAY INDEX.
C
C          (R*8)  XM       = REAL VALUE = M.
C          (R*8)  X1       =
C          (R*8)  X2       =
C          (R*8)  T1       =
C          (R*8)  T2       =
C          (R*8)  SUM      =
C          (R*8)  SUMEX    =
C          (R*8)  SUMTH    =
C
C          (I*4)  JLIST()  =
C                            DIMENSION: MXN.
C
C          (R*8)  AA()     = LJ RESOLVED A-VALUE.
C                            DIMENSION: TRANSITION INDEX WHERE:
C                                       1 GIVES LU+0.5 --> LL+0.5
C                                       2 GIVES LU+0.5 --> LL-0.5
C                                       3 GIVES LU-0.5 --> LL+0.5
C                                       4 GIVES LU-0.5 --> LL-0.5
C
C          (R*8)  REMISA() =
C                            DIMENSION: MXOB.
C          (R*8)  REMQ()   =
C                            DIMENSION: MXOB.
C          (R*8)  WKSPCE() = WORKSPACE FOR NAG ROUTINE.
C                            DIMENSION: MXOB.
C          (R*8)  EMQ()    =
C                            DIMENSION: MXNSHL.
C
C          (R*8)  CNDSA(,) = CONDENSED MATRIX.
C                            1ST DIMENSION: MXN.
C                            2ND DIMENSION: 2.
C          (R*8)  ARED(,)  = LINEAR CONDENSATION TRANSFORMATION ARRAY.
C                            1ST DIMENSION: MXN.
C                            2ND DIMENSION: MXN.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4UNIT     ADAS      RETURNS UNIT NO. FOR OUTPUT OF MESSAGES.
C          I4IDFL     ADAS      RETURNS UNIQUE INDEX GIVEN QUANTUM
C                               NUMBERS N AND L.
C          C6AJTB     ADAS      CALCULATES LJ RESOLVED A-VALUES.
CX          F04ARF     NAG       FINDS SOLUTION TO A SET OF REAL LINEAR
CX                               EQUATIONS.
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    09/11/93
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    22ND MAY 1996
C
C VERSION: 1.1                          DATE: 22-05-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST VERSION. IBM VERSION NOT CHANGED
C
C VERSION: 1.2                          DATE: 29-05-96
C MODIFIED: WILLIAM OSBORN
C               - REMOVED UNUSED VARIABLES
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    I4UNIT  , I4IDFL
C-----------------------------------------------------------------------
      INTEGER    MXN       , MXJ      , MXOB
      PARAMETER( MXN = 20  , MXJ = 2  , MXOB = 10 )
C-----------------------------------------------------------------------
      INTEGER    MXNSHL  , MXJSHL  , MXOBSL  , IZ1     , NBOT    ,
     &           NUMIN   , NUMAX   , NREP
      INTEGER    N       , L       , N1      , L1      , IDL     ,
     &           IC      , IC1     , IC2     , M       ,
     &           I       , J
C-----------------------------------------------------------------------
      REAL*8     EM
      REAL*8     XM      , X1      , X2      , T1      , T2      ,
     &           SUM     , SUMEX   , SUMTH
C-----------------------------------------------------------------------
      INTEGER    ICREP(MXOBSL) , NU(MXOBSL)   , NL(MXOBSL)
      INTEGER    JLIST(MXN)
C-----------------------------------------------------------------------
      REAL*8     EMISA(MXOBSL)   , QTHEOR(MXNSHL)  , QEX(MXNSHL)
      REAL*8     AA(2*MXJ)       , REMISA(MXOB)    , REMQ(MXOB)      ,
     &           EMQ(MXN)
      REAL*8     CNDSA(MXN,2)                       ,
     &           ARED(MXOB,MXOB)
      REAL*8     WLOW(MXJSHL,(MXNSHL*(MXNSHL+1))/2,MXNSHL)
C-----------------------------------------------------------------------
      INTEGER     PIPEIN        , PIPEOU
      PARAMETER(  PIPEIN=5      , PIPEOU=6)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C CHECK PARAMETERS 'MXN', 'MXJ' AND 'MXOB'.
C-----------------------------------------------------------------------
C
      IF (MXN .LT. MXNSHL) THEN
         WRITE(I4UNIT(-1),1000) MXN, MXNSHL
         STOP
      ENDIF
C
      IF (MXJ .LT. MXJSHL) THEN
         WRITE(I4UNIT(-1),1001) MXJ, MXJSHL
         STOP
      ENDIF
C
      IF (MXOB .LT. MXOBSL) THEN
         WRITE(I4UNIT(-1),1002) MXOB, MXOBSL
         STOP
      ENDIF
C
C-----------------------------------------------------------------------
C
      IF (NUMIN .EQ. NUMAX) THEN
C
         JLIST(NUMIN) = 1
         CNDSA(NUMIN,1) = 1.0D0
         CNDSA(NUMIN,2) = 0.0D0
C
      ELSE
C
         DO 1 M = NUMIN , NUMAX
C
            J = 1
    2       CONTINUE
            IF ((M .GE. NU(ICREP(J))) .AND. (J .EQ. NREP)) THEN
               J = J + 1
               GOTO 2
            ENDIF
            JLIST(M) = J - 1
C
            XM = DFLOAT( M )
            IC1 = ICREP(J-1)
            IC2 = ICREP(J)
            X1 = DFLOAT( NU(IC1) )
            X2 = DFLOAT( NU(IC2) )
C
            IF ((QTHEOR(M) .EQ. 0.0D0)       .OR.
     &          (QTHEOR(NU(IC1)) .EQ. 0.0D0) .OR.
     &          (QTHEOR(NU(IC2)) .EQ. 0.0D0)      ) THEN
               T1 = ( XM - X2 ) / ( X1 - X2 )
               T2 = ( XM - X1 ) / ( X2 - X1 )
            ELSE
               T1 = ( XM - X2 ) * QTHEOR(M)
     &              / ( ( X1 - X2 ) * QTHEOR(NU(IC1)) )
               T2 = ( XM - X1 ) * QTHEOR(M)
     &              / ( ( X2 - X1 ) * QTHEOR(NU(IC2)) )
            ENDIF
C
            CNDSA(M,1) = T1
            CNDSA(M,2) = T2
C
    1    CONTINUE
C
      ENDIF
C
C-----------------------------------------------------------------------
C
      DO 3 I = 1 , NREP
C
         IC = ICREP(I)
         N  = NU(IC)
         N1 = NL(IC)
C
         DO 4 J = 1 , NREP
            ARED(I,J) = 0.0D0
    4    CONTINUE
C
         DO 5 M = N , NUMAX
C
            SUM = 0.0D0
            DO 6 L = 0 , N-1
               IDL = I4IDFL( N , L )
               DO 7 L1 = L-1 , L+1 , 2
                  CALL C6AJTB( MXJ , IZ1 , N , L , N1 , L1 , AA )
                  SUM = SUM + ( AA(1) + AA(2) ) * WLOW(1,IDL,M) +
     &                  ( AA(3) + AA(4) ) * WLOW(2,IDL,M)
    7          CONTINUE
    6       CONTINUE
C
            J = JLIST(M)
            ARED(I,J) = ARED(I,J) + SUM * CNDSA(M,1)
            IF (NUMIN .NE. NUMAX) THEN
               ARED(I,J+1) = ARED(I,J+1) + SUM * CNDSA(M,2)
            ENDIF
C
    5    CONTINUE
C
         REMISA(I) = EMISA(IC)
C
    3 CONTINUE
C
C-----------------------------------------------------------------------
C SOLVE FOR EMISSION MEASURE * CHARGE EXCHANGE PRODUCTS REMQ FOR
C REPRESENTATIVE LEVELS.
C-----------------------------------------------------------------------
C
CX    IFAIL = 0
CX    CALL F04ARF( ARED , MXOB , REMISA , NREP , REMQ , WKSPCE , IFAIL )
C----------------------------------------------------------------------
C    THE ABOVE IS A CALL TO THE NAG MATHEMATICAL LIBRARY. THIS HAS
C    NOW BEEN REPLACED BY THE APPROPRIATE IDL ROUTINE. THE RELEVANT
C    VARIABLES ARE THEREFORE FED TO THE IDL ROUTINE c6emqx.pro AND
C    THE RESULT RETURNED INTO THE VARIABLE REMQ 
C----------------------------------------------------------------------
C    WRITE OUT DATA TO PIPE
C----------------------------------------------------------------------

      WRITE (PIPEOU, *) NREP
      CALL XXFLSH (PIPEOU)
      DO 100,I=1,NREP
         DO 200,J=1,NREP
            WRITE( PIPEOU, *) ARED(I,J)
            CALL XXFLSH (PIPEOU)
 200     CONTINUE
         WRITE( PIPEOU, *) REMISA(I)
         CALL XXFLSH (PIPEOU)
 100  CONTINUE
      
C----------------------------------------------------------------------
C     READ RESULT FROM THE PIPE
C----------------------------------------------------------------------
      
      DO 300,I=1,NREP
         READ(PIPEIN, *) REMQ(I)
 300  CONTINUE

C
C-----------------------------------------------------------------------
C EXTEND TO FULL SET OF LEVELS BETWEEN NUMIN AND NUMAX AND COMPARE SUM
C WITH THEORETICAL SUM.
C-----------------------------------------------------------------------
C
      SUMEX = 0.0D0
      SUMTH = 0.0D0
      DO 8 M = NUMIN , NUMAX
         J = JLIST(M)
         EMQ(M) = CNDSA(M,1) * REMQ(J)
         IF (NUMIN .NE. NUMAX) THEN
            EMQ(M) = EMQ(M) + CNDSA(M,2) * REMQ(J+1)
         ENDIF
         SUMEX = SUMEX + EMQ(M)
         SUMTH = SUMTH + QTHEOR(M)
    8 CONTINUE
C
C-----------------------------------------------------------------------
C CALCULATE THE EMISSION MEASURE.
C-----------------------------------------------------------------------
C
      EM = SUMEX / SUMTH
C
C-----------------------------------------------------------------------
C ISOLATE THE EXPERIMENTAL MEAN CHARGE EXCHANGE RATE COEFFTS.
C-----------------------------------------------------------------------
C
      DO 9 N = NUMIN , NUMAX
         QEX(N) = EMQ(N) / EM
    9 CONTINUE
C
C-----------------------------------------------------------------------
C EXTEND EXPERIMENTAL MEAN CHARGE EXCHANGE RATE COEFFICIENTS TO         N
C N < NUMIN USING THE THEORETICAL RATIO.
C-----------------------------------------------------------------------
C
      DO 10 N = NBOT , NUMIN
         IF (QTHEOR(NUMIN) .GT. 0.0D0) THEN
            QEX(N) = QTHEOR(N) * QEX(NUMIN) / QTHEOR(NUMIN)
         ELSE
            QEX(N) = 0.0D0
         ENDIF
   10 CONTINUE
C
C-----------------------------------------------------------------------
C
 1000 FORMAT( 1X, 32('*'), ' C6EMQX ERROR ', 32('*') //
     &        2X, 'INTERNAL PARAMETER ''MXN'' IS LESS THAN INPUT ',
     &            'ARGUMENT ''MXJSHL''.'/
     &        2X, 'MXN = ', I3 , '   MXNSHL = ', I3/
     &        2X, 'INCREASE PARAMETER ''MXN'' IN SUBROUTINE C6EMQX.' //
     &        1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )
 1001 FORMAT( 1X, 32('*'), ' C6EMQX ERROR ', 32('*') //
     &        2X, 'INTERNAL PARAMETER ''MXJ'' IS LESS THAN INPUT ',
     &            'ARGUMENT ''MXOBSL''.'/
     &        2X, 'MXJ = ', I3 , '   MXJSHL = ', I3/
     &        2X, 'INCREASE PARAMETER ''MXJ'' IN SUBROUTINE C6EMQX.'//
     &        1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )
 1002 FORMAT( 1X, 32('*'), ' C6EMQX ERROR ', 32('*') //
     &        2X, 'INTERNAL PARAMETER ''MXOB'' IS LESS THAN INPUT ',
     &            'ARGUMENT ''MXOBSL''.'/
     &        2X, 'MXOB = ', I3 , '   MXOBSL = ', I3/
     &        2X, 'INCREASE PARAMETER ''MXOB'' IN SUBROUTINE C6EMQX.'//
     &        1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
