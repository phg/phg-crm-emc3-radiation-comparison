CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas306/c6tbrc.for,v 1.2 2004/07/06 11:53:59 whitefor Exp $ Date $Date: 2004/07/06 11:53:59 $
CX
      SUBROUTINE C6TBRC( MXNSHL , MXJSHL , IZ1    , NBOT   ,
     &                   NTOP   , TEV    , QTHRC  , FTHRCJ
     &                 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C6TBRC *********************
C
C  PURPOSE:  SETS UP A TABLE OF RADIATIVE RECOMBINATION RATE
C            COEFFICIENTS FOR A BARE NUCLEUS, HELIUM-LIKE OR NEON-LIKE
C            ION TO EXCITED NLJ LEVELS.
C
C  CALLING PROGRAM: ADAS306
C
C  INPUT : (I*4)  MXNSHL   = MAXIMUM VALUE OF PRINCIPAL QUANTUM NUMBER.
C  INPUT : (I*4)  MXJSHL   = MAXIMUM NUMBER OF J SUB-SHELLS.
C  INPUT : (I*4)  IZ1      = ION CHARGE.
C  INPUT : (I*4)  NBOT     = MINIMUM PRINCIPAL QUANTUM NUMBER.
C  INPUT : (I*4)  NTOP     = MAXIMUM PRINCIPAL QUANTUM NUMBER.
C  INPUT : (R*8)  TEV      = ELECTRON TEMPERATURE.
C                            UNITS: EV
C
C  OUTPUT: (R*8)  QTHRC()  = RECOMBINATION RATE COEFFICIENT TO LEVEL N.
C                            UNITS: CM3 SEC-1
C                            DIMENSION: N-SHELL
C  OUTPUT: (R*8)  FTHRCJ(,)= FRACTION OF RECOMBINATION RATE OF LEVEL N
C                            TO STATE NLJ.
C                            1ST DIMENSION: J-SHELL INDEX WHERE
C                                           1 GIVES J=L+0.5
C                                           2 GIVES J=L-0.5
C                            2ND DIMENSION: REFERENCED BY I4IDFL().
C
C  PARAM : (R*8)  P1       = BOLTZMANN CONSTANT.
C                            UNITS: EV K-1
C  PARAM : (R*8)  P2       =
C
C          (I*4)  N        = PRINCIPAL QUANTUM NUMBER OF BOUND ELECTRON.
C          (I*4)  L        = ORBITAL QUANTUM NUMBER OF BOUND ELECTRON.
C          (I*4)  L1       = ORBITAL QUANTUM NUMBER OF FREE ELECTRON.
C          (I*4)  LP       = TOTAL ORBITAL ANGULAR MOMENTUM QUANTUM
C                           NUMBER OF PARENT STATE.
C          (I*4)  ISP      = 2*SP+1 WHERE SP IS TOTAL SPIN OF PAREN     T
C                            STATE.
C          (I*4)  LT       = TOTAL ORBITAL ANGULAR MOMENTUM QUANTUM
C                            NUMBER OF BOUND SYSTEM.
C          (I*4)  LT1      = TOTAL ORBITAL ANGULAR MOMENTUM QUANTUM
C                            NUMBER OF FREE SYSTEM.
C          (I*4)  IS       = 2*S+1 WHERE S IS TOTAL SPIN OF SYSTEM.
C          (I*4)  IRES     = LEVEL OF RESOLUTION.
C                          = 1 :
C                          = 2 : ABOVE LT1 SUM.
C                          = 3 : ABOVE LT SUM.
C                          = 4 : ABOVE S SUM.
C                          = 5 : UNRESOLVED GBF.
C          (I*4)  I        = LOOP INDEX.
C          (I*4)  J        = LOOP INDEX.
C          (I*4)  IDL      = TABLE INDEX.
C
C          (R*8)  Z1       = REAL VALUE = IZ1.
C          (R*8)  TE       = ELECTRON TEMPERATURE.
C                            UNITS: K
C          (R*8)  V        = EFFECTIVE PRINCIPAL QUANTUM NUMBER OF BOUND
C                            ELECTRON.
C          (R*8)  FACT     =
C          (R*8)  SUM      =
C          (R*8)  XL       = REAL VALUE = L.
C          (R*8)  WL       =
C          (R*8)  T        =
C          (R*8)  PREC1    = RADIATIVE RECOMBINATION INTEGRAL.
C          (R*8)  PION1    = PHOTOIONISATION INTEGRAL.
C          (R*8)  PSTIM1   = STIMULATED RECOMBINATION INTEGRAL.
C          (R*8)  PREC2    = RADIATIVE RECOMBINATION INTEGRAL.
C          (R*8)  PION2    = PHOTOIONISATION INTEGRAL.
C          (R*8)  PSTIM2   = STIMULATED RECOMBINATION INTEGRAL.
C
C  PARAM : (R*8)  P1       = BOLTZMANN CONSTANT.
C                            UNITS: EV K-1
C  PARAM : (R*8)  P2       =
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4IDFL     ADAS      RETURNS UNIQUE INDEX GIVEN QUANTUM
C                               NUMBERS N AND L.
C          CXPHOT     ADAS      CALCULATES PHOTO INTEGRALS USING GIIH
C                               BOUND-FREE GAUNT-FACTORS.
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    05/11/93
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    22ND MAY 1996
C
C VERSION: 1.1                          DATE: 22-05-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST VERSION. IBM VERSION NOT CHANGED
C
C VERSION: 1.2                          DATE: 29-05-96
C MODIFIED: WILLIAM OSBORN
C               - REMOVED UNUSED VARIABLES
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    I4IDFL
C-----------------------------------------------------------------------
      REAL*8     P1               , P2
      PARAMETER( P1 = 8.61663D-5  , P2 = 1.5789D5 )
C-----------------------------------------------------------------------
      INTEGER    MXNSHL  , MXJSHL  , IZ1     , NBOT    , NTOP
      INTEGER    N       , L       , L1      , LP      , ISP     ,
     &           LT      , LT1     , IS      , IRES    , I       ,
     &           J       , IDL
C-----------------------------------------------------------------------
      REAL*8     TEV
      REAL*8     Z1      , TE      , V       , FACT    ,
     &           SUM     , XL      , WL      , T       , PREC1   ,
     &           PION1   , PSTIM1  , PREC2   , PION2   , PSTIM2
C-----------------------------------------------------------------------
      REAL*8     QTHRC(MXNSHL)
C-----------------------------------------------------------------------
      REAL*8     FTHRCJ(MXJSHL,(MXNSHL*(MXNSHL+1))/2)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C CLEAR OUTPUT TABLES.
C-----------------------------------------------------------------------
C
      DO 1 N = 1 , MXNSHL
         QTHRC(N) = 0.0D0
    1 CONTINUE
C
      DO 2 I = 1 , I4IDFL( MXNSHL , MXNSHL-1 )
         DO 3 J = 1 , MXJSHL
            FTHRCJ(J,I) = 0.0D0
    3    CONTINUE
    2 CONTINUE
C
C-----------------------------------------------------------------------
C INITIALIZE VALUES.
C-----------------------------------------------------------------------
C
      Z1 = DFLOAT( IZ1 )
      TE = TEV / P1
C
      LP  = 0
      ISP = 1
      IS  = 2
      IRES = 4
C
C-----------------------------------------------------------------------
C FILL OUTPUT TABLES BETWEEN 'NTOP' AND 'NBOT'.
C-----------------------------------------------------------------------
C
      DO 4 N = NBOT , NTOP
C
         V = DFLOAT( N )
         FACT = 5.19678D-14 * Z1 *
     &          ( P2 * Z1**2 / ( V**2 * TE ) )**1.5D0
         SUM = 0.0D0
C
         DO 5 L = 0 , N-1
C
            LT = L
            XL = DFLOAT( L )
            WL = 2.0D0 * XL + 1.0D0
            PREC1 = 0.0D0
            PREC2 = 0.0D0
C
            L1 = L + 1
            LT1 = L + 1
            CALL CXPHOT( IZ1    , TE     , TE     , V      ,
     &                   N      , L      , L1     , LP     ,
     &                   ISP    , LT     , LT1    , IS     ,
     &                   IRES   , PREC1  , PION1  , PSTIM1   )
C
            IF (L .GT. 0) THEN
               L1 = L - 1
               LT1 = L - 1
               CALL CXPHOT( IZ1    , TE     , TE     , V      ,
     &                      N      , L      , L1     , LP     ,
     &                      ISP    , LT     , LT1    , IS     ,
     &                      IRES   , PREC2  , PION2  , PSTIM2   )
            ENDIF
C
            T = FACT * ( PREC1 + PREC2 )
            SUM = SUM + T
C
            IDL = I4IDFL( N , L )
            FTHRCJ(1,IDL) = ( XL + 1.0D0 ) * T / WL
            IF (L .GT. 0) THEN
               FTHRCJ(2,IDL) = XL * T / WL
            ELSE
               FTHRCJ(2,IDL) = 0.0D0
            ENDIF
C
    5    CONTINUE
C
         QTHRC(N) = SUM
C
         DO 6 IDL = I4IDFL( N , 0 ) , I4IDFL( N , N-1 )
            DO 7 J = 1 , 2
               FTHRCJ(J,IDL) = FTHRCJ(J,IDL) / SUM
    7       CONTINUE
    6    CONTINUE
C
    4 CONTINUE
C
C-----------------------------------------------------------------------
C
      RETURN
      END
