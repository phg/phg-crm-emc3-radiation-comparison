CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas306/c6tbqm.for,v 1.2 2007/05/17 17:02:30 allan Exp $ Date $Date: 2007/05/17 17:02:30 $
CX
      SUBROUTINE C6TBQM( MXNSHL , MXJSHL , IZ0    , IZ1    ,
     &                   NBOT   , NTOP   , TEV    , DENS   ,
     &                   ZP     , TPV    , EMP    , TBLF   ,
     &                   TBQMEP , TBQMEM , TBQMIP , TBQMIM
     &                 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C6TBQM *********************
C
C  PURPOSE:  SETS UP TABLES OF ELECTRON AND POSITIVE ION COLLISIONAL
C            RATE COEFFICIENTS BETWEEN NEARLY DEGENERATE LEVELS FOR
C            H-, LI-, AND NA-LIKE IONS.
C
C            THE RATES FOR THE SEPARATE TRANSITIONS NLJ->NL+1J' AND
C            NLJ->NL-1J' ARE OBTAINED.
C
C  CALLING PROGRAM: ADAS306
C
C  INPUT : (I*4)  MXNSHL   = MAXIMUM VALUE OF PRINCIPAL QUANTUM NUMBER.
C  INPUT : (I*4)  MXJSHL   = MAXIMUM NUMBER OF J SUB-SHELLS.
C  INPUT : (I*4)  IZ0      = NUCLEAR CHARGE OF TARGET ION.
C  INPUT : (I*4)  IZ1      = ION CHARGE.
C  INPUT : (I*4)  NBOT     = MINIMUM PRINCIPAL QUANTUM NUMBER.
C  INPUT : (I*4)  NTOP     = MAXIMUM PRINCIPAL QUANTUM NUMBER.
C  INPUT : (R*8)  TEV      = ELECTRON TEMPERATURE.
C                            UNITS: EV
C  INPUT : (R*8)  DENS     = ELECTRON DENSITY.
C                            UNITS: CM-3
C  INPUT : (R*8)  ZP       = CHARGE OF COLLIDING POSITIVE ION.
C  INPUT : (R*8)  TPV      = TEMPERATURE (COLLIDING POSITIVE ION
C                            DISTRIBUTION).
C                            UNITS: EV
C  INPUT : (R*8)  EMP      = REDUCED MASS FOR COLLIDING POSITIVE ION.
C                            UNITS: ELECTRON MASSES
C  INPUT : (R*8)  ZP       = CHARGE OF COLLIDING POSITIVE ION.
C  INPUT : (R*8)  TPV      = POSITIVE ION TEMPERATURE.
C                            UNITS: EV
C  INPUT : (R*8)  EMP      = REDUCED MASS FOR COLLIDING POSITIVE ION.
C                            UNITS: ELECTRON MASSES
C  INPUT : (R*8)  TBLF()   = TABLE OF RADIATIVE LIFETIMES.
C                            UNITS: SECS
C                            DIMENSION: REFERENCED BY FUNC I4IDFL(N,L).
C
C  OUTPUT: (R*8)  TBQMEP(,)= ELECTRON RATE COEFFT. FOR NLJ->NL+1J'.
C                            1ST DIMENSION: J->J' TRANSITION INDEX.
C                            2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C  OUTPUT: (R*8)  TBQMEM(,)= ELECTRON RATE COEFFT. FOR NLJ->NL-1J'.
C                            1ST DIMENSION: J->J' TRANSITION INDEX.
C                            2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C  OUTPUT: (R*8)  TBQMIP(,)= POSITIVE ION RATE COEFFT. FOR NLJ->NL+1J'.
C                            1ST DIMENSION: J->J' TRANSITION INDEX.
C                            2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C  OUTPUT: (R*8)  TBQMIM(,)= POSITIVE ION RATE COEFFT. FOR NLJ->NL-1J'.
C                            1ST DIMENSION: J->J' TRANSITION INDEX.
C                            2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C
C  PARAM : (I*4)  MXJ     = 'MXJSHL'.
C
C          (I*4)  NI      = VALENCE ELECTRON PRINCIPAL QUANTUM NUMBER
C                           IN STATE I.
C          (I*4)  NJ      = VALENCE ELECTRON PRINCIPAL QUANTUM NUMBER
C                           IN STATE J.
C          (I*4)  LI      = VALENCE ELECTRON ORBITAL QUANTUM NUMBER IN
C                           STATE I.
C          (I*4)  LJ      = VALENCE ELECTRON ORBITAL QUANTUM NUMBER IN
C                           STATE J.
C          (I*4)  IDLI    = TABLE INDEX.
C          (I*4)  IDLJ    = TABLE INDEX.
C          (I*4)  I       = LOOP INDEX.
C          (I*4)  J       = LOOP INDEX.
C
C          (R*8)  GAE     = GAMA RATE PARAMETER FOR ELECTRON COLLISIONS.
C          (R*8)  GAP     = GAMA RATE PARAMETER FOR POSITIVE ION
C                           COLLISIONS.
C
C          (R*8)  QEP()   = ELECTRON RATE COEFFT. FOR NLJ->NL+1J'
C                           DIMENSION: J->J' TRANSITION INDEX.
C          (R*8)  QEM()   = ELECTRON RATE COEFFT. FOR NLJ->NL-1J'
C                           DIMENSION: J->J' TRANSITION INDEX.
C          (R*8)  QIP()   = POSITIVE ION RATE COEFFT. FOR NLJ->NL+1J'
C                           DIMENSION: J->J' TRANSITION INDEX.
C          (R*8)  QIM()   = POSITIVE ION RATE COEFFT. FOR NLJ->NL-1J'
C                           DIMENSION: J->J' TRANSITION INDEX.
C
C NOTES:
C       1) THE J->J' TRANSITION INDEX IS AS FOLLOWS:
C              1 : J=L+0.5 -> J'=L'+0.5
C              2 : J=L+0.5 -> J'=L'-0.5
C              3 : J=L-0.5 -> J'=L'+0.5
C              4 : J=L-0.5 -> J'=L'-0.5
C
C       2) BEFORE CALLING C6TBQM THE LIFETIME TABLE MUST BE FILLED
C          WITH A CALL TO C6TBLF.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4UNIT     ADAS      RETURNS UNIT NO. FOR OUTPUT OF MESSAGES.
C          I4IDFL     ADAS      RETURNS UNIQUE INDEX GIVEN QUANTUM
C                               NUMBERS N AND L.
C          CXCRDG     ADAS      CALCULATES COLLISIONAL RATE COEFFICIENTS
C                               BETWEEN NEARLY DEGENERATE LEVELS OF
C                               H-, LI- OR NA-LIKE IONS.
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    02/11/93
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    22ND MAY 1996
C
C VERSION: 1.1                          DATE: 22-05-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST VERSION. IBM VERSION NOT CHANGED
C
C VERSION: 1.2                          DATE: 17-05-07
C MODIFIED: Allan Whiteford
C               - Updated comments as part of subroutine documentation
C                 procedure.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    I4UNIT  , I4IDFL
C-----------------------------------------------------------------------
      INTEGER    MXJ
      PARAMETER( MXJ = 2 )
C-----------------------------------------------------------------------
      INTEGER    MXNSHL  , MXJSHL  , IZ0     , IZ1     , NTOP    ,
     &           NBOT
      INTEGER    NI      , NJ      , LI      , LJ      , IDLI    ,
     &           IDLJ    , I       , J
C-----------------------------------------------------------------------
      REAL*8     TEV     , DENS    , ZP      , TPV     , EMP
      REAL*8     GAE     , GAP
C-----------------------------------------------------------------------
      REAL*8     TBLF((MXNSHL*(MXNSHL+1))/2)             ,
     &           TBQMEP(2*MXJSHL,(MXNSHL*(MXNSHL+1))/2)  ,
     &           TBQMEM(2*MXJSHL,(MXNSHL*(MXNSHL+1))/2)  ,
     &           TBQMIP(2*MXJSHL,(MXNSHL*(MXNSHL+1))/2)  ,
     &           TBQMIM(2*MXJSHL,(MXNSHL*(MXNSHL+1))/2)
      REAL*8     QEP(2*MXJ)  , QEM(2*MXJ)  , QIP(2*MXJ)  ,
     &           QIM(2*MXJ)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C CHECK INTERNAL PARAMETER 'MXJ'.
C-----------------------------------------------------------------------
C
      IF (MXJ .LT. MXJSHL) THEN
         WRITE(I4UNIT(-1),1000) MXJ, MXJSHL
         STOP
      ENDIF
C
C-----------------------------------------------------------------------
C CLEAR OUTPUT TABLES.
C-----------------------------------------------------------------------
C
      DO 1 I = 1 , I4IDFL( MXNSHL , MXNSHL-1 )
         DO 2 J = 1 , 2 * MXJSHL
            TBQMEP(J,I) = 0.0D0
            TBQMEM(J,I) = 0.0D0
            TBQMIP(J,I) = 0.0D0
            TBQMIM(J,I) = 0.0D0
   2     CONTINUE
   1  CONTINUE
C
C-----------------------------------------------------------------------
C FILL OUTPUT TABLES BETWEEN 'NBOT' AND 'NTOP'.
C-----------------------------------------------------------------------
C
      DO 3 NI = NBOT , NTOP
C
         NJ = NI
C
         DO 4 LI = 0 , NI-2
C
            LJ = LI + 1
C
            CALL CXCRDG( MXNSHL , MXJ    , IZ0    , IZ1    , NI     ,
     &                   LI     , NJ     , LJ     , TEV    , DENS   ,
     &                   ZP     , TPV    , EMP    , TBLF   , GAE    ,
     &                   GAP    , QEP    , QEM    , QIP    , QIM      )
C
            IDLI = I4IDFL( NI , LI )
            IDLJ = I4IDFL( NJ , LJ )
            DO 5 J = 1 , 2 * MXJSHL
               TBQMEP(J,IDLI) = QEP(J)
               TBQMEM(J,IDLJ) = QEM(J)
               TBQMIP(J,IDLI) = QIP(J)
               TBQMIM(J,IDLJ) = QIM(J)
    5       CONTINUE
C
    4    CONTINUE
    3 CONTINUE
C
C-----------------------------------------------------------------------
C
 1000 FORMAT( 1X, 32('*'), ' C6TBQM ERROR ', 32('*') //
     &        2X, 'INTERNAL PARAMETER ''MXJ'' IS LESS THAN INPUT ',
     &            'ARGUMENT ''MXJSHL''.'/
     &        2X, 'MXJ = ', I3 , '   MXJSHL = ', I3/
     &        2X, 'INCREASE PARAMETER ''MXJ'' IN SUBROUTINE C6TBQM.' //
     &        1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
