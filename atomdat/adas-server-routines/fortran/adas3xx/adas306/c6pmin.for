CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas306/c6pmin.for,v 1.3 2004/07/06 11:53:05 whitefor Exp $ Date $Date: 2004/07/06 11:53:05 $
CX
      SUBROUTINE C6PMIN( MXNSHL , MXJSHL , N , VD , VDS , VDI , RHS )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C6PMIN *********************
C
C  PURPOSE:  CALCULATES THE SOLUTION OF A TRIDIAGONAL PARTITIONED MATRIX
C            ORGANISED SET OF SIMULTANEOUS EQUATIONS.
C
C            THE PARTITIONS ARE 2X2 IN THE PRESENT IMPLEMENTATION. A
C            VARIANT OF OF THE DOUBLE PASS ALGORITHM (IN A PARTITIONED
C            MATRIX SENSE) IS USED WITH RECURRENCE IN TWO DIRECTIONS TO
C            THE CENTRE AND BACK. THIS IS ANALOGOUS TO THE NAG ROUTINE
C            F04EAF FOR AN ORDINARY TRIDAGONAL MATRIX THE INDEXING OF
C            THE DIAG, SUPRADIAG AND INFRADIAG ELEMENTS FOLLOWS THAT OF
C            THE NAG ROUTINE F04EAF.
C
C  CALLING PROGRAM: C6WSOL
C
C  INPUT : (I*4)  MXNSHL   = MAXIMUM NUMBER OF N SHELLS.
C  INPUT : (I*4)  MXJSHL   = MAXIMUM NUMBER OF J SUB-SHELLS.
C  INPUT : (I*4)  N        = NUMBER OF PARTIONS ALONG THE DIAGONAL.
C  INPUT : (R*8)  VD(,)    = DIAGONAL PARTITION.
C                            1ST DIMENSION: 2 * MXJSHL
C                            2ND DIMENSION: N SHELL INDEX.
C  INPUT : (R*8)  VDS(,)   = SUPRADIAGONAL PARTITION.
C                            1ST DIMENSION: 2 * MXJSHL
C                            2ND DIMENSION: N SHELL INDEX.
C  INPUT : (R*8)  VDI(,)   = INFRADIAGONAL PARTITION.
C                            1ST DIMENSION: 2 * MXJSHL
C                            2ND DIMENSION: N SHELL INDEX.
C
C  I/O   : (R*8)  RHS(,)   = INPUT: RIGHT HAND SIDE OF VECTOR PARTITION.
C                            OUTPUT: SOLUTION OF VECTOR PARTITION.
C                            1ST DIMENSION: 2 * MXJSHL
C                            2ND DIMENSION: N SHELL INDEX.
C
C
C  PARAM : (I*4)  MXJ      = MXJSHL.
C
C          (I*4)  I        = LOOP INDEX.
C          (I*4)  K        =
C
C          (R*8)  UNIT()   =
C                            DIMENSION: 4
C          (R*8)  W1()     =
C                            DIMENSION: 4
C          (R*8)  W2()     =
C                            DIMENSION: 4
CX          (R*8)  W3()     =
CX                            DIMENSION: 4
C          (R*8)  VW1()    =
C                            DIMENSION: 2
C          (R*8)  VW2()    =
C                            DIMENSION: 2
C          (R*8)  TEMP()   = TEMPORARY STORE.
C                            DIMENSION: 4
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4UNIT     ADAS      RETURNS UNIT NO. FOR OUTPUT OF MESSAGES.
C
C NOTES:
C        1) THE 2X2 PARTITIONS ARE STORED AS LINEAR VECTORS BY COLUMN
C           IN THE 1ST DIMENSIONS OF VD(,), VDS(,), VDI(,).
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    11/11/93
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    22ND MAY 1996
C
C VERSION: 1.1                          DATE: 22-05-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST VERSION. IBM VERSION NOT CHANGED
C
C VERSION: 1.2                          DATE: 29-05-96
C MODIFIED: WILLIAM OSBORN
C               - REMOVED UNUSED VARIABLES
C
C VERSION: 1.3                          DATE: 29-05-96
C MODIFIED: WILLIAM OSBORN
C               - S.C.C.S. MISTAKE
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    I4UNIT
C-----------------------------------------------------------------------
      INTEGER    MXJ
      PARAMETER( MXJ = 2 )
C-----------------------------------------------------------------------
      INTEGER    MXNSHL  , MXJSHL  , N
      INTEGER    I       , K
C-----------------------------------------------------------------------
      REAL*8     VD(2*MXJSHL,MXNSHL)   , VDS(2*MXJSHL,MXNSHL)  ,
     &           VDI(2*MXJSHL,MXNSHL)  , RHS(MXJSHL,MXNSHL)
      REAL*8     UNIT(2*MXJ)  , W1(2*MXJ)    , W2(2*MXJ)    ,
     &           VW1(MXJ)     , VW2(MXJ)     ,
     &           TEMP(2*MXJ)
C-----------------------------------------------------------------------
      DATA   UNIT / 1.0D0 , 0.0D0 , 0.0D0 , 1.0D0 /
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C CHECK PARAMETER 'MXJ'.
C-----------------------------------------------------------------------
C
      IF (MXJ .LT. MXJSHL) THEN
         WRITE(I4UNIT(-1),1000) MXJ, MXJSHL
         STOP
      ENDIF
C
C-----------------------------------------------------------------------
C
      IF (N .LE. 1) THEN
C
         CALL XXMINO( MXJSHL , VD(1,1) , W1 )
         CALL XXMMUL( MXJSHL , MXJSHL , 1 , W1 , RHS(1,1) , TEMP )
         CALL XXMCPY( MXJSHL , 1 , TEMP , RHS(1,1) )
C
C-----------------------------------------------------------------------
C
      ELSE
C
         K = ( N + 1 ) / 2
C
         CALL XXMINO( MXJSHL , VD(1,1) , W1 )
         CALL XXMMUL( MXJSHL , MXJSHL , MXJSHL , W1 , VDS(1,2) , TEMP ) ) )
         CALL XXMCPY( MXJSHL , MXJSHL , TEMP , VDS(1,2) )
         CALL XXMMUL( MXJSHL , MXJSHL , 1 , W1 , RHS(1,1) , TEMP )
         CALL XXMCPY( MXJSHL , 1 , TEMP , RHS(1,1) )
C
         DO 1 I = 2 , K
            CALL XXMMUL( MXJSHL , MXJSHL , MXJSHL , VDI(1,I) ,
     &                   VDS(1,I) , W1 )
            CALL XXMADD( MXJSHL , MXJSHL , 1.0D0 , VD(1,I) , -1.0D0 ,
     &                   W1 , VD(1,I) )
            CALL XXMINO( MXJSHL , VD(1,I) , W1 )
            CALL XXMMUL( MXJSHL , MXJSHL , MXJSHL , W1 , VDS(1,I+1) ,
     &                   TEMP )
            CALL XXMCPY( MXJSHL , MXJSHL , TEMP , VDS(1,I+1) )
            CALL XXMMUL( MXJSHL , MXJSHL , MXJSHL , VDI(1,I) ,
     &                   RHS(1,I-1) , VW1 )
            CALL XXMADD( MXJSHL , 1 , 1.0D0 , RHS(1,I) , -1.0D0 , VW1 ,
     &                   VW2 )
            CALL XXMMUL( MXJSHL , MXJSHL , 1 , W1 , VW2 , RHS(1,I) )
    1    CONTINUE
C
         CALL XXMINO( MXJSHL , VD(1,N) , W1 )
         CALL XXMMUL( MXJSHL , MXJSHL , MXJSHL , W1 , VDI(1,N) , TEMP )
         CALL XXMCPY( MXJSHL , MXJSHL , TEMP , VDI(1,N) )
         CALL XXMMUL( MXJSHL , MXJSHL , 1 , W1 , RHS(1,N) , TEMP )
         CALL XXMCPY( MXJSHL , 1 , TEMP , RHS(1,N) )
C
         DO 2 I = N-1 , K+1 , -1
            CALL XXMMUL( MXJSHL , MXJSHL , MXJSHL , VDS(1,I+1) ,
     &                   VDI(1,I +1) , W1 )
            CALL XXMADD( MXJSHL , MXJSHL , 1.0D0 , VD(1,I) , -1.0D0 ,    ,
     &                   W1 , VD(1,I) )
            CALL XXMINO( MXJSHL , VD(1,I) , W1 )
            CALL XXMMUL( MXJSHL , MXJSHL , MXJSHL , W1 , VDI(1,I) ,
     &                   TEMP )
            CALL XXMCPY( MXJSHL , MXJSHL , TEMP , VDI(1,I) )
            CALL XXMMUL( MXJSHL , MXJSHL , 1 , VDS(1,I+1) ,
     &                   RHS(1,I+1) , VW1 )
            CALL XXMADD( MXJSHL , 1 , 1.0D0 , RHS(1,I) , -1.0D0 , VW1 ,
     &                   VW2 )
            CALL XXMMUL( MXJSHL , MXJSHL , 1 , W1 , VW2 , RHS(1,I) )
    2    CONTINUE
C
         CALL XXMMUL( MXJSHL , MXJSHL , MXJSHL , VDI(1,K+1) ,
     &                VDS(1,K+1) , W1 )
         CALL XXMADD( MXJSHL , MXJSHL , 1.0D0 , UNIT , -1.0D0 , W1 ,
     &                W2 )
         CALL XXMINO( MXJSHL , W2 , W1 )
         CALL XXMMUL( MXJSHL , MXJSHL , 1 , VDI(1,K+1) , RHS(1,K) ,
     &                VW1 )
         CALL XXMADD( MXJSHL , 1 , 1.0D0 , RHS(1,K+1) , -1.0D0 , VW1 ,
     &                VW2 )
         CALL XXMMUL( MXJSHL , MXJSHL , 1 , W1 , VW2 , RHS(1,K+1) )
C
         DO 3 I = K , 1 , -1
            CALL XXMMUL( MXJSHL , MXJSHL , 1 , VDS(1,I+1) ,
     &                   RHS(1,I+1) , VW1 )
            CALL XXMADD( MXJSHL , 1 , 1.0D0 , RHS(1,I) , -1.0D0 , VW1 ,
     &                   RHS(1,I) )
    3    CONTINUE
C
         DO 4 I = K+2 , N
            CALL XXMMUL( MXJSHL , MXJSHL , 1 , VDI(1,I) , RHS(1,I-1) ,
     &                   VW1 )
            CALL XXMADD( MXJSHL , 1 , 1.0D0 , RHS(1,I) , -1.0D0 , VW1 ,
     &                   RHS(1,I) )
    4    CONTINUE
C
      ENDIF
C
C-----------------------------------------------------------------------
C
 1000 FORMAT( 1X, 32('*'), ' C6PMIN ERROR ', 32('*') //
     &        2X, 'INTERNAL PARAMETER ''MXJ'' IS LESS THAN INPUT ',
     &            'ARGUMENT ''MXJSHL''.'/
     &        2X, 'MXJ = ', I3 , '   MXJSHL = ', I3/
     &        2X, 'INCREASE PARAMETER ''MXJ'' IN SUBROUTINE C6PMIN.' //
     &        1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
