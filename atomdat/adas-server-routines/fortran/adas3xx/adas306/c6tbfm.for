CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas306/c6tbfm.for,v 1.2 2007/05/17 17:02:30 allan Exp $ Date $Date: 2007/05/17 17:02:30 $
CX
      SUBROUTINE C6TBFM( MXNSHL , MXJSHL , IZ0    , IZ1    ,
     &                   AMSSNO , NBOT   , NTOP   , BMAG   ,
     &                   TIEV   , TBLF   , TBFMP  , TBFM   ,
     &                   TBFMM
     &                 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C6TBFM *********************
C
C  PURPOSE:  FILLS TABLES OF  MAGNETIC FIELD DEPENDENT MIXING RATE
C            COEFFICIENTS BETWEEN NEARLY DEGENERATE LEVELS FOR
C            HYDROGEN-LIKE, LITHIUM-LIKE AND SODIUM-LIKE IONS.
C
C            RATES ARE CALCULATED FOR THE SEPARATE NLJ->NL+1J',
C            NLJ->NLJ' AND NLJ->NL-1J' TRANSITIONS.
C
C  CALLING PROGRAM: ADAS306
C
C  INPUT : (I*4)  MXNSHL  = MAXIMUM NUMBER OF N SHELLS.
C  INPUT : (I*4)  MXJSHL  = MAXIMUM NUMBER OF J SUB-SHELLS.
C  INPUT : (I*4)  IZ0     = TARGET NUCLEAR CHARGE.
C  INPUT : (I*4)  IZ1     = ION CHARGE.
C  INPUT : (R*8)  AMSSNO  = ATOMIC MASS NO.
C  INPUT : (I*4)  NBOT    = MINIMUM PRINCIPAL QUANTUM NUMBER.
C  INPUT : (I*4)  NTOP    = MAXIMUM PRINCIPAL QUANTUM NUMBER.
C  INPUT : (R*8)  BMAG    = MAGNETIC INDUCTION.
C                           UNITS: TESLA
C  INPUT : (R*8)  TIEV    = TEMPERATURE (ION DISTRIBUTION).
C                           UNITS: EV
C  INPUT : (R*8)  TBLF()  = TABLE OF RADIATIVE LIFETIMES.
C                           UNITS: SECS
C                           DIMENSION: REFERENCED BY FUNC I4IDFL(N,L).
C
C  OUTPUT: (R*8)  TBFMP(,)= RATE COEFFT. FOR NLJ->NL+1J'.
C                           1ST DIMENSION: J->J' TRANSITION INDEX.
C                           2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C  OUTPUT: (R*8)  TBFM(,) = RATE COEFFT. FOR NLJ->NL+1J'.
C                           1ST DIMENSION: J->J' TRANSITION INDEX.
C                           2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C  OUTPUT: (R*8)  TBFMM(,)= RATE COEFFT. FOR NLJ->NLJ' FOR STATE I.
C                           1ST DIMENSION: J->J' TRANSITION INDEX.
C                           2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C
C  PARAM : (I*4)  MXJ     = 'MXJSHL'.
C
C          (I*4)  NI      = VALENCE ELECTRON PRINCIPAL QUANTUM NUMBER
C                           IN STATE I.
C          (I*4)  NJ      = VALENCE ELECTRON PRINCIPAL QUANTUM NUMBER
C                           IN STATE J.
C          (I*4)  LI      = VALENCE ELECTRON ORBITAL QUANTUM NUMBER IN
C                           STATE I.
C          (I*4)  LJ      = VALENCE ELECTRON ORBITAL QUANTUM NUMBER IN
C                           STATE J.
C          (I*4)  IDLI    = TABLE INDEX.
C          (I*4)  IDLJ    = TABLE INDEX.
C          (I*4)  I       = LOOP INDEX.
C          (I*4)  J       = LOOP INDEX.
C
C          (R*8)  FMP()   = RATE COEFFT. FOR NLJ->NL+1J'.
C                           DIMENSION: J->J' TRANSITION INDEX.
C          (R*8)  FMM()   = RATE COEFFT. FOR NLJ->NL+1J'.
C                           DIMENSION: J->J' TRANSITION INDEX.
C          (R*8)  FMI()   = RATE COEFFT. FOR NLJ->NLJ' FOR STATE I.
C                           DIMENSION: J->J' TRANSITION INDEX.
C          (R*8)  FMJ()   = RATE COEFFT. FOR NLJ->NLJ' FOR STATE J.
C                           DIMENSION: J->J' TRANSITION INDEX.
C
C NOTES:
C       1) THE J->J' TRANSITION INDEX IS AS FOLLOWS:
C              1 : J=L+0.5 -> J'=L'+0.5
C              2 : J=L+0.5 -> J'=L'-0.5
C              3 : J=L-0.5 -> J'=L'+0.5
C              4 : J=L-0.5 -> J'=L'-0.5
C
C       2) BEFORE CALLING C6TBQM THE LIFETIME TABLE MUST BE FILLED
C          WITH A CALL TO C6TBLF.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4UNIT     ADAS      RETURNS UNIT NO. FOR OUTPUT OF MESSAGES.
C          I4IDFL     ADAS      RETURNS UNIQUE INDEX GIVEN QUANTUM
C                               NUMBERS N AND L.
C          CXMRDG     ADAS      CALCULATES MIXING RATE COEFFICIENTS
C                               BETWEEN NEARLY DEGENERATE LEVELS OF
C                               H-, LI- OR NA-LIKE IONS.
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    04/11/93
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    22ND MAY 1996
C
C VERSION: 1.1                          DATE: 22-05-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST VERSION. IBM VERSION NOT CHANGED
C
C VERSION: 1.2                          DATE: 17-05-07
C MODIFIED: Allan Whiteford
C               - Updated comments as part of subroutine documentation
C                 procedure.
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    I4UNIT  , I4IDFL
C-----------------------------------------------------------------------
      INTEGER    MXJ
      PARAMETER( MXJ = 2 )
C-----------------------------------------------------------------------
      INTEGER    MXNSHL  , MXJSHL  , IZ0     , IZ1     , NTOP    ,
     &           NBOT
      INTEGER    NI      , NJ      , LI      , LJ      , IDLI    ,
     &           IDLJ    , I       , J
C-----------------------------------------------------------------------
      REAL*8     AMSSNO  , BMAG    , TIEV
C-----------------------------------------------------------------------
      REAL*8     TBLF((MXNSHL*(MXNSHL+1))/2)            ,
     &           TBFMP(2*MXJSHL,(MXNSHL*(MXNSHL+1))/2)  ,
     &           TBFM(2*MXJSHL,(MXNSHL*(MXNSHL+1))/2)   ,
     &           TBFMM(2*MXJSHL,(MXNSHL*(MXNSHL+1))/2)
      REAL*8     FMP(2*MXJ)  , FMM(2*MXJ)  , FMI(2*MXJ)  ,
     &           FMJ(2*MXJ)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C CHECK INTERNAL PARAMETER 'MXJ'.
C-----------------------------------------------------------------------
C
      IF (MXJ .LT. MXJSHL) THEN
         WRITE(I4UNIT(-1),1000) MXJ, MXJSHL
         STOP
      ENDIF
C
C-----------------------------------------------------------------------
C CLEAR OUTPUT TABLES.
C-----------------------------------------------------------------------
C
      DO 1 I = 1 , I4IDFL( MXNSHL , MXNSHL-1 )
         DO 2 J = 1 , 2 * MXJSHL
            TBFMP(J,I) = 0.0D0
            TBFM(J,I)  = 0.0D0
            TBFMM(J,I) = 0.0D0
   2     CONTINUE
   1  CONTINUE
C
C-----------------------------------------------------------------------
C FILL OUTPUT TABLES BETWEEN 'NBOT' AND 'NTOP'.
C-----------------------------------------------------------------------
C
      DO 3 NI = NBOT , NTOP
C
         NJ = NI
C
         DO 4 LI = 0 , NI-2
C
            LJ = LI + 1
C
            CALL CXMRDG ( MXNSHL , MXJ    , IZ0    , IZ1    ,
     &                    AMSSNO , NI     , LI     , NJ     ,
     &                    LJ     , BMAG   , TIEV   , TBLF   ,
     &                    FMP    , FMM    , FMI    , FMJ      )
C
            IDLI = I4IDFL( NI , LI )
            IDLJ = I4IDFL( NJ , LJ )
            DO 5 J = 1 , 2 * MXJSHL
               TBFMP(J,IDLI) = FMP(J)
               TBFMM(J,IDLJ) = FMM(J)
               TBFM(J,IDLI)  = FMI(J)
    5       CONTINUE
C
    4    CONTINUE
C
         IDLJ = I4IDFL( NJ , NJ-1 )
         DO 6 J = 1 , 2 * MXJSHL
            TBFM(J,IDLJ) = FMJ(J)
    6    CONTINUE
C
    3 CONTINUE
C
C-----------------------------------------------------------------------
C
 1000 FORMAT( 1X, 32('*'), ' C6TBFM ERROR ', 32('*') //
     &        2X, 'INTERNAL PARAMETER ''MXJ'' IS LESS THAN INPUT ',
     &            'ARGUMENT ''MXJSHL''.'/
     &        2X, 'MXJ = ', I3 , '   MXJSHL = ', I3/
     &        2X, 'INCREASE PARAMETER ''MXJ'' IN SUBROUTINE C6TBFM.' //
     &        1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
