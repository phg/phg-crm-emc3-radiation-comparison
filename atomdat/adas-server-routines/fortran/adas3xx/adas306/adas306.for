CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas306/adas306.for,v 1.5 2007/05/24 15:27:23 mog Exp $ Date $Date: 2007/05/24 15:27:23 $
CX
      PROGRAM ADAS306
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 PROGRAM: ADAS306 **********************
C
C  ORIGINAL NAME: SPLMSJ
C
C  VERSION:  1.0 (ADAS91)
C
C  PURPOSE:  CODE FOR INTERPRETATION OF J-RESOLVED COLUMN INTEGRATED
C            SPECTRUM LINE EMISSIVITIES IN TERMS OF MEAN RATE
C            COEFFICIENTS AND EMISSION MEASURE.  APPLICABLE TO
C            IMPURITIES IN PLASMA TRAVERSED BY NEUTRAL BEAMS OF H OR HE.
C
C            THE RECOMBINED TARGET ION MAY BE H, LI OR NA-LIKE.
C
C            THE MODEL INCLUDES CAPTURE, N-N' LEVEL CASCADE, AND MIXING
C            AMONG L,J LEVELS OF SAME N BY COLLISIONS OR MAGNETIC
C            FIELDS.
C
C            ELECTRON IMPACT IONISATION IS INCLUDED TO GIVE COLLISION
C            LIMIT EFFECT.
C
C            EIKONAL APPROXIMATION IS USED FOR CAPTURE FROM EXCITED H OR
C            HE STATES.
C
C            OUTPUT FROM THE PROGRAM IS GRAPHS AND TABLES OF J-RESOLVED
C            EMISSIVITY FOR THE REQUESTED TRANSITIONS. J-RESOLVED
C            DIRECT CAPTURE, FIELD DYNAMIC, FIELD STATIC, ION IMPACT,
C            AND ELCTRON IMPACT TABLES ARE OPTIONALLY OUTPUT.  IF
C            REQUESTED THE GRAPHS AND THE TEXT FILE ARE OUTPUT TO
C            HARDCOPY.
C
C  DATA:     THE SOURCE DATA IS CONTAINED AS MEMBERS IN A PARTITIONED
C            DATA SET AS FOLLOWS:-
C
C                    'JETSHP.QCX#<DONOR>.DATA(<MEMBER>)'
C
C            WHERE
C              <DONOR>    = <DONOR ELEMENT SYMBOL><DONOR CHARGE STATE>
C              <MEMBER>   = <ID>#<RECEIVER>
C
C              <ID>       = 3 CHARACTER PREFIX IDENTIFYING THE SOURCE
C                           OF THE DATA.
C              <RECEIVER> = <RECVR ELEMENT SYMBOL><RECVR CHARGE STATE>
C
C
C              E.G. 'JETSHP.QCX#H0.DATA(OLD#C6)'
C
C           THE UNITS USED IN THE DATA FILE ARE TAKEN AS FOLLOWS:
C
C           ENERGIES            : KEV / AMU
C           CROSS SECTIONS      : CM**2
C
C  PARAM : (I*4)  IUNT7     = OUTPUT UNIT FOR RESULTS.
C  PARAM : (I*4)  IUNT10    = INPUT UNIT FOR DATA.
C  PARAM : (I*4)  MXNENG    = MAXIMUM NO. OF ENERGIES IN DATA SET.
C  PARAM : (I*4)  MXNSHL    = MAXIMUM NUMBER OF N SHELLS.
C  PARAM : (I*4)  MXJSHL    = MAXIMUM NUMBER OF J SUB-SHELLS.
C  PARAM : (I*4)  MXBEAM    = MAXIMUM NUMBER OF BEAM COMPONENTS.
C  PARAM : (I*4)  MXOBSL    = MAXIMUM NUMBER OF OBSERVED SPECTRUM
C                             LINES.
C  PARAM : (I*4)  MXPRSL    = MAXIMUM NUMBER OF SPECTRUM LINES TO
C                             PREDICT.
C  PARAM : (I*4)  MXTAB     = MAXIMUM NUMBER OF TABLES FOR OUTPUT.
C  PARAM : (I*4)  MXGRF     = MAXIMUM NUMBER OF GRAPHS FOR OUTPUT.
C  PARAM : (I*4)  NRTABS    = NUMBER OF INDIVIDUAL RATE TABLES.
C  PARAM : (I*4)  NGFPLN    = NUMBER OF GRAPHS PER PREDICTED SPECTRUM
C                             LINE.
C
C  PARAM : (R*8)  EMP       = REDUCED MASS FOR POSITIVE ION.
C                             UNITS: ELECTRON MASSES
C
C          (I*4)  NGRND     = PRINCIPAL QUANTUM NUMBER OF GROUND STATE.
C          (I*4)  NTOT      = PRINCIPAL QUANTUM NUMBER OF HIGHEST BOUND
C                             STATE.
C          (I*4)  NBOT      = MINIMUM PRINCIPAL QUANTUM NUMBER FOR
C                             RATE TABLES.
C          (I*4)  NTOP      = MAXIMUM PRINCIPAL QUANTUM NUMBER FOR
C                             RATE TABLES.
C          (I*4)  IZR       = ION CHARGE OF RECEIVER.
C          (I*4)  IZD       = ION CHARGE OF DONOR.
C          (I*4)  INDD      = DONOR STATE INDEX.
C          (I*4)  NENRGY    = NUMBER OF ENERGIES READ FROM DATA SET.
C          (I*4)  NMINF     = LOWEST N-SHELL FOR WHICH DATA READ.
C          (I*4)  NMAXF     = HIGHEST N-SHELL FOR WHICH DATA READ.
C          (I*4)  IDZ0      = DONOR NUCLEAR CHARGE.
C          (I*4)  IRZ0      = RECEIVER NUCLEAR CHARGE.
C          (I*4)  IRZ1      = RECEIVER ION INITIAL CHARGE.
C          (I*4)  IRZ2      = RECEIVER ION FINAL CHARGE.
C          (I*4)  IPAN      = ISPF PANEL NUMBER.
C          (I*4)  NBEAM     = NUMBER OF BEAM ENERGIES.
C          (I*4)  ITHEOR    = CHARGE EXCHANGE MODEL OPTION.
C                             1 => USE INPUT DATA SET.
C                             2 => USE EIKONAL MODEL.
C          (I*4)  IBSTAT    = DONOR STATE FOR EIKONAL MODEL.
C                             1 => H(1S)
C                             2 => H(2S)
C                             3 => H(2P)
C                             4 => HE(1S2)
C                             5 => HE(1S2S)
C          (I*4)  IEMMS     = EMISSION MEASURE MODEL OPTION.
C                             1 => CHARGE EXCHANGE.
C                             2 => ELECTRON IMPACT EXCITATION.
C                             3 => RADIATIVE RECOMBINATION.
C          (I*4)  NOLINE    = NUMBER OF OBSERVED SPECTRUM LINES.
C          (I*4)  NPLINE    = NUMBER OF SPECTRUM LINES TO PREDICT.
C          (I*4)  NTAB      = NUMBER OF EMISSIVITY TABLES FOR OUTPUT.
C          (I*4)  NGRF      = NUMBER OF EMISSIVITY GRAPHS FOR OUTPUT.
C          (I*4)  NUMIN     = MINIMUM UPPER PRINCIPAL QUANTUM NUMBER
C                             FOR OBSERVED SPECTRUM LINES.
C          (I*4)  NUMAX     = MAXIMUM UPPER PRINCIPAL QUANTUM NUMBER
C                             FOR OBSERVED SPECTRUM LINES.
C
C          (R*8)  RAMSNO    = RECEIVER ATOMIC MASS.
C          (R*8)  TEV       = ELECTRON TEMPERATURE.
C                             UNITS: EV
C          (R*8)  TIEV      = ION TEMPERATURE.
C                             UNITS: EV
C          (R*8)  DENS      = ELECTRON DENSITY.
C                             UNITS: CM-3
C          (R*8)  DENSZ     = PLASMA ION DENSITY.
C                             UNITS: CM-3
C          (R*8)  ZEFF      = EFFECTIVE ION CHARGE.
C          (R*8)  BMAG      = PLASMA MAGNETIC INDUCTION.
C                             UNITS: TESLA
C          (R*8)  EM        = EMMISSION MEASURE.
C                             UNITS: CM-5
C
C          (L*4)  OPEN10    = FLAGS IF INPUT DATA SET OPEN.
C                             .TRUE.  => INPUT DATA SET OPEN.
C                             .FALSE. => INPUT DATA SET CLOSED.
C          (L*4)  LGHOST    = INITIALISATION FLAG FOR GHOST80.
C                             .TRUE.  => GHOST80 INITIALISED.
C                             .FALSE. => GHOST80 NOT INITIALISED.
C          (L*4)  LPARMS    = FLAGS IF L-SPLITTING PARAMETERS PRESENT.
C                             .TRUE.  => L-SPLITTING PARAMETERS PRESENT.
C                             .FALSE  => L-SPLITTING PARAMETERS ABSENT.
C          (L*4)  LSETL     = FLAGS IF L-RESOLVED DATA PRESENT.
C                             .TRUE.  => L-RESOLVED DATA PRESENT.
C                             .FALSE  => L-RESOLVED DATA ABSENT.
C          (L*4)  LSETM     = FLAGS IF M-RESOLVED DATA PRESENT.
C                             .TRUE.  => M-RESOLVED DATA PRESENT.
C                             .FALSE  => M-RESOLVED DATA ABSENT.
C          (L*4)  LPEND     = FLAGS IF END OF ANALYSIS REQUESTED.
C                             .TRUE.  => END ANALYSIS OF CURRENT DATA
C                                        SETS
C                             .FALSE. => CONTINUE PANALYSIS WITH CURRENT
C                                        DATA SETS
C          (L*4)  LRTTB     = FLAG FOR RATE TABLE PRINTING.
C                             .TRUE.  = PRINT RATE TABLES.
C                             .FALSE. = DO NOT PRINT RATE TABLES.
C          (L*4)  LPLT1     = FLAGS WHETHER TO PLOT GRAPHS ON SCREEN.
C                             .TRUE.  => PLOT GRAPHS ON SCREEN.
C                             .FALSE. => DO NOT PLOT GRAPHS ON SCREEN.
C          (L*4)  LGRD1     = FLAGS WHETHER TO PUT GRAPHS IN GRID FILE.
C                             .TRUE.  => PUT GRAPHS IN GRID FILE
C                             .FALSE. => DO NOT PUT GRAPHS IN GRID FILE
C          (L*4)  LDEF1     = FLAGS DEFAULT GRAPH SCALING
C                             .TRUE.  => USE DEFAULT GRAPH SCALING.
C                             .FALSE. => DO NOT USE DEFAULT SCALING.
C
C          (C*8)  DATE      = DATE.
C          (C*3)  REP       = REQUEST FOR PROGRAM TERMINATION.
C                             'YES' => TERMINATE PROGRAM EXECUTION.
C                             'NO'  => CONTINUE PROGRAM EXECUTION.
C          (C*80) DSFULL    = FILE NAME OF INPUT DATA SET.
C          (C*80) TITLEF    = NOT SET - TITLE FROM INPUT DATA SET.
C          (C*2)  SYMBR     = RECEIVER ELEMENT SYMBOL.
C          (C*2)  SYMBD     = DONOR ELEMENT SYMBOL.
C          (C*40) TITLE     = ISPF ENTERED GENERAL TITLE FOR RUN.
C
C          (I*4)  LFORMA()  = PARAMETERS FOR CALCULATING L-RES X-SEC
C                             READ FROM INPUT DATA SET.
C                             DIMENSION: ENERGY INDEX
C          (I*4)  NL()      = LIST OF LOWER PRINCIPAL QUANTUM NUMBERS
C                             OF OBSERVED SPECTRUM LINES.
C                             DIMENSION: SPECTRUM LINE INDEX.
C          (I*4)  NU()      = LIST OF UPPER PRINCIPAL QUANTUM NUMBERS
C                             OF OBSERVED SPECTRUM LINES.
C                             DIMENSION: SPECTRUM LINE INDEX.
C          (I*4)  NPL()     = LIST OF LOWER PRINCIPAL QUANTUM NUMBERS
C                             OF SPECTRUM LINES TO PREDICT.
C                             DIMENSION: SPECTRUM LINE INDEX.
C          (I*4)  NPU()     = LIST OF UPPER PRINCIPAL QUANTUM NUMBERS
C                             OF SPECTRUM LINES TO PREDICT.
C                             DIMENSION: SPECTRUM LINE INDEX.
C          (I*4)  IDTAB()   = LIST OF INDEXES OF EMISSIVITY TABLES FOR
C                             OUTPUT.
C                             DIMENSION: MXTAB
C          (I*4)  IDGRF()   = LIST OF INDEXES OF EMISSIVITY GRAPHS FOR
C                             OUTPUT.
C                             DIMENSION: MXGRF
C
C          (R*8)  ENRGYA()  = COLLISION ENERGIES READ FROM INPUT DATA
C                             SET.
C                             UNITS: EV/AMU
C                             DIMENSION: ENERGY INDEX
C          (R*8)  ALPHAA()  = EXTRAPOLATION PARAMETER ALPHA READ FROM
C                             INPUT DATA SET.
C                             DIMENSION: ENERGY INDEX
C          (R*8)  XLCUTA()  = PARAMETERS FOR CALCULATING L-RES X-SEC
C                             READ FROM INPUT DATA SET.
C                             DIMENSION: ENERGY INDEX
C          (R*8)  PL2A()    = PARAMETERS FOR CALCULATING L-RES X-SEC
C                             READ FROM INPUT DATA SET.
C                             DIMENSION: ENERGY INDEX
C          (R*8)  PL3A()    = PARAMETERS FOR CALCULATING L-RES X-SEC
C                             READ FROM INPUT DATA SET.
C                             DIMENSION: ENERGY INDEX
C          (R*8)  XSECTA()  = TOTAL CHARGE EXCHANGE CROSS-SECTIONS READ
C                             FROM INPUT DATA SET.
C                             UNITS: CM2
C                             DIMENSION: ENERGY INDEX
C          (R*8)  BMFRA()   = BEAM COMPONENT FRACTIONS.
C                             DIMENSION: COMPONENT INDEX.
C          (R*8)  BMENA()   = BEAM ENERGY COMPONENTS.
C                             UNITS: EV/AMU
C          (R*8)  EMISA()   = LIST OF EMISSIVITIES OF OBSERVED SPECTRUM
C                             LINES.
C                             UNITS: PH CM-2 SEC-1
C                             DIMENSION: SPECTRUM LINE INDEX.
C          (R*8)  XLG()     = LOWER LIMIT FOR X-AXIS OF GRAPHS.
C                             DIMENSION: NGFPLN
C          (R*8)  XUG()     = UPPER LIMIT FOR X-AXIS OF GRAPHS.
C                             DIMENSION: NGFPLN
C          (R*8)  YLG()     = LOWER LIMIT FOR Y-AXIS OF GRAPHS.
C                             DIMENSION: NGFPLN
C          (R*8)  YUG()     = UPPER LIMIT FOR Y-AXIS OF GRAPHS.
C                             DIMENSION: NGFPLN
C          (R*8)  TBLF()    = TABLE OF RADIATIVE LIFETIMES.
C                             UNITS: SECS
C                             DIMENSION: REFERENCED BY I4IDFL(N,L).
C          (R*8)  QTHIN()   = IONISATION RATE COEFFICIENT.
C                             UNITS: CM3 SEC-1
C                             DIMENSION: N SHELL INDEX.
C          (R*8)  QTHEX()   = MEAN EXCITATION RATE COEFFICIENTS FOR
C                             N-LEVELS AVERAGED OVER BEAM FRACTIONS.
C                             UNITS: CM3 SEC-1
C                             DIMENSION: N SHELL INDEX.
C          (R*8)  QTHCH()   = MEAN CHARGE EXCHANGE COEFFICIENTS FOR
C                             N-LEVELS AVERAGED OVER BEAM FRACTIONS.
C                             UNITS: CM3 SEC-1
C                             DIMENSION: N SHELL INDEX.
C          (R*8)  QTHRC()   = MEAN RECOMBINATION RATE COEFFICIENTS FOR
C                             N-LEVELS AVERAGED OVER BEAM FRACTIONS.
C                             UNITS: CM3 SEC-1
C                             DIMENSION: N SHELL INDEX.
C          (R*8)  QEX()     =
C                             DIMENSION: N SHELL INDEX.
C          (R*8)  TOTPOP()  = TOTAL COLLISION POP. FOR PREDICTED
C                             SPECTRUM LINE.
C                             UNITS: CM-2
C                             DIMENSION: PREDICTED LINE INDEX.
C          (R*8)  TOTEMI()  = TOTAL COLLISION EMISSIVITIES FOR PREDICTED
C                             SPECTRUM LINE.
C                             UNITS: PH CM-2 SEC-1
C                             DIMENSION: PREDICTED LINE INDEX.
C          (R*8)  AVRGWL()  = AVERAGE AIR WAVELENGTH FOR PREDICTED
C                             SPECTRUM LINE.
C                             UNITS: A
C                             DIMENSION: PREDICTED LINE INDEX.
C          (R*8)  QEFF()    = EFF. RATE COEFFICIENT FOR PREDICTED
C                             SPECTRUM LINE.
C                             UNITS: CM3 SEC-1
C                             DIMENSION: PREDICTED LINE INDEX.
C
C          (L*4)  LRTABS()  = FLAGS FOR INDIVIDUAL TABLES.
C                             INDEX: 1 = DIRECT CAPTURE TABLE.
C                                    2 = FIELD DYNAMIC TABLE.
C                                    3 = FIELD STATIC TABLE.
C                                    4 = ION IMPACT TABLE.
C                                    5 = ELECTRON IMPACT TABLE.
C
C          (R*8)  XSECNA(,) = N-RESOLVED CHARGE EXCHANGE CROSS-SECTIONS
C                             READ FROM INPUT DATA SET.
C                             UNITS: CM2
C                             1ST DIMENSION: ENERGY INDEX
C                             2ND DIMENSION: N-SHELL
C          (R*8)  FRACLA(,) = L-RESOLVED CHARGE EXCHANGE CROSS-SECTIONS.
C                             AFTER CXDATA: ABSOLUTE VALUES (CM2).
C                             AFTER CXFRAC: FRACTION OF N-RESOLVED
C                                            DATA.
C                             1ST DIMENSION: ENERGY INDEX
C                             2ND DIMENSION: INDEXED BY I4IDFL(N,L)
C          (R*8)  FRACMA(,) = M-RESOLVED CHARGE EXCHANGE CROSS-SECTIONS.
C                             AFTER CXDATA: ABSOLUTE VALUES (CM2).
C                             AFTER CXFRAC: FRACTION OF L-RESOLVED
C                                            DATA.
C                             1ST DIMENSION: ENERGY INDEX
C                             2ND DIMENSION: INDEXED BY I4IDFM(N,L,M)
C                                            WITH M >= 0 ONLY
C          (R*8)  FTHEXJ(,) = FRACTION OF N-LEVEL MEAN EXCITATION RATE
C                             COEFFICIENTS IN NLJ-LEVEL.
C                             1ST DIMENSION: J SHELL INDEX WHERE:
C                                            1 GIVES J=L+0.5
C                                            2 GIVES J=L-0.5
C                             2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C          (R*8)  FTHCHJ(,) = FRACTION OF N-LEVEL MEAN CHARGE EXCHANGE
C                             COEFFICIENTS IN NLJ-LEVEL.
C                             1ST DIMENSION: J SHELL INDEX WHERE:
C                                            1 GIVES J=L+0.5
C                                            2 GIVES J=L-0.5
C                             2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C          (R*8)  FTHRCJ(,) = FRACTION OF N-LEVEL MEAN RECOMBINATION
C                             RATE COEFFICIENTS IN NLJ-LEVEL.
C                             1ST DIMENSION: J SHELL INDEX WHERE:
C                                            1 GIVES J=L+0.5
C                                            2 GIVES J=L-0.5
C                             2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C          (R*8)  TBQMEP(,) = ELECTRON COLLISIONAL RATE COEFFT. FOR
C                             NLJ->NL+1J'.
C                             1ST DIMENSION: J->J' TRANSITION INDEX.
C                             2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C          (R*8)  TBQMEM(,) = ELECTRON COLLISIONAL RATE COEFFT. FOR
C                             NLJ->NL-1J'.
C                             1ST DIMENSION: J->J' TRANSITION INDEX.
C                             2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C          (R*8)  TBQMIP(,) = POSITIVE ION COLLISIONAL RATE COEFFT. FOR
C                             NLJ->NL+1J'.
C                             1ST DIMENSION: J->J' TRANSITION INDEX.
C                             2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C          (R*8)  TBQMIM(,) = POSITIVE ION COLLISIONAL RATE COEFFT. FOR
C                             NLJ->NL-1J'.
C                             1ST DIMENSION: J->J' TRANSITION INDEX.
C                             2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C          (R*8)  TBFMP(,)  = B-FIELD DEPENDENT MIXING RATE COEFFT. FOR
C                             NLJ->NL+1J'.
C                             1ST DIMENSION: J->J' TRANSITION INDEX.
C                             2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C          (R*8)  TBFM(,)   = B-FIELD DEPENDENT MIXING RATE COEFFT. FOR
C                             NLJ->NLJ'.
C                             1ST DIMENSION: J->J' TRANSITION INDEX.
C                             2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C          (R*8)  TBFMM(,)  = B-FIELD DEPENDENT MIXING RATE COEFFT. FOR
C                             NLJ->NL-1J'.
C                             1ST DIMENSION: J->J' TRANSITION INDEX.
C                             2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C
C          (R*8)  TBLPOP(,,)= TABLE OF COLLISION POP. FOR PREDICTED
C                             SPECTRUM LINE.
C                             UNITS: CM-2
C                             1ST DIMENSION: J->J' TRANSITION INDEX.
C                             2ND DIMENSION: REFERENCED BY I4IDLI().
C                             3RD DIMENSION: PREDICTED LINE INDEX.
C          (R*8)  TBLEMI(,,)= TABLE OF COLLISION EMISSIVITIES FOR
C                             PREDICTED SPECTRUM LINE.
C                             UNITS: PH CM-2 SEC-1
C                             1ST DIMENSION: J->J' TRANSITION INDEX.
C                             2ND DIMENSION: REFERENCED BY I4IDLI().
C                             3RD DIMENSION: PREDICTED LINE INDEX.
C          (R*8)  TBLWLN(,,)= TABLE OF WAVELENGTHS FOR PREDICTED
C                             SPECTRUM LINE.
C                             UNITS: A
C                             1ST DIMENSION: J->J' TRANSITION INDEX.
C                             2ND DIMENSION: REFERENCED BY I4IDLI().
C                             3RD DIMENSION: PREDICTED LINE INDEX.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          C6SPF0     ADAS      USES IDL FOR DATA SET INPUT.
C          C6CHRG     ADAS      SETS UP CHARGES AND EXTREME N LEVELS FOR
C                               DONOR AND RECEIVER.
C          C6ISPF     ADAS      HANDELS ISPF PANELS FOR USER INPUT.
C          C6TBIN     ADAS      FILLS N-RESOLVED ELECTRON IMPACT
C                               IONISATION RATE TABLE.
C          C6TBEX     ADAS      FILLS N AND J-RESOLVED ELECTRON IMPACT
C                               EXCITATION RATE TABLES.
C          C6QEIK     ADAS      FILLS N AND J-RESOLVED CHARGE EXCHANGE
C                               RATE TABLES USING EIKONAL APPROXIMATION.
C          C6QXCH     ADAS      FILLS N AND J-RESOLVED CHARGE EXCHANGE
C                               RATE TABLES USING INPUT DATA SET.
C          C6TBRC     ADAS      FILLS N AND J-RESOLVED RADIATIVE
C                               RECOMBINATION RATE TABLES.
C          C6TBQM     ADAS      FILLS N AND J-RESOLVED COLLISIONAL RATE
C                               TABLES.
C          C6TBFM     ADAS      FILLS N AND J-RESOLVED B-FIELD
C                               DEPENDENT MIXING RATE TABLES.
C          C6EMIS     ADAS      PREDICTS THE J-RESOLVED EMISSIVITY FOR
C                               REQUESTED TRANSITIONS.
C          C6OUTG     ADAS      CONTROLS GRAPHICAL OUTPUT FOR ADAS306.
C          C6OUT0     ADAS      CONTROLS TEXT OUTPUT FOR ADAS306.
C          CXDATA     ADAS      READS INPUT DATA SET IN ADF01 FORMAT.
C          CXFRAC     ADAS      CONVERTS L AND M RESOLVED CROSS-SECTIONS
C                               FROM ABSOLUTE VALUES TO FRACTIONS.
C          CXEXTR     ADAS      EXTRAPOLATES N AND L RESOLVED CROSS-
C                               SECTIONS FROM INPUT DATA SET.
C          CXSETP     ADAS      SETS UP PARAMETERS IN THE SHARED POOL
C                               FOR ISPF PANEL DISPLAY.
C          CXTBLF     ADAS      FILLS L-RESOLVED RADIATIVE LIFETIME
C                               TABLE.
C          XX0000     ADAS      SETS MACHINE DEPENDENT ADAS
C                               CONFIGURATION.
C          XXDATE     ADAS      RETURNS CURRENT DATE.
C          XXENDG     ADAS      CLOSES GHOST80 GRIDFILE AND OUTPUT
C                               CHANNEL.
C          XXRAMS     ADAS      SETS/RETRIEVES RECEIVER ATOMIC WEIGHT.
C
C NOTES:
C       1) THE J->J' TRANSITION INDEX IS AS FOLLOWS:
C              1 : J=L+0.5 -> J'=L'+0.5
C              2 : J=L+0.5 -> J'=L'-0.5
C              3 : J=L-0.5 -> J'=L'+0.5
C              4 : J=L-0.5 -> J'=L'-0.5
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/87
C          JET EXT. 5183
C
C DATE:    25/11/93
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    22ND MAY 1996
C
C VERSION: 1.1                          DATE: 22-05-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST VERSION
C
C VERSION: 1.2                          DATE: 29-05-96
C MODIFIED: WILLIAM OSBORN
C               - REMOVED UNUSED VARIABLES
C
C VERSION:  1.3                          DATE: 08/04/99
C MODIFIED: Martin O'Mullane
C            - Added XXRAMS to set the weight of the receiver for
C              subsequent use in C6PRSL.
C
C VERSION:  1.4                          DATE: 15-11-2000
C MODIFIED: Richard Martin
C		Increased MXNENG to 40 .
C
C VERSION  : 1.5
C DATE     : 22-05-2007
C MODIFIED : Martin O'Mullane
C              - Remove unused m-subshell data possibility.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    IUNT7        , IUNT10       , MXNENG       ,
     &           MXNSHL       , MXJSHL       , MXBEAM       ,
     &           MXOBSL       , MXPRSL       , MXTAB        ,
     &           MXGRF        , NRTABS       , NGFPLN
      PARAMETER( IUNT10 = 11  , IUNT7  = 7   , MXNENG = 40  ,
     &           MXNSHL = 20  , MXJSHL = 2   , MXBEAM = 6   ,
     &           MXOBSL = 10  , MXPRSL = 20  , MXTAB  = 5   ,
     &           MXGRF  = 2   , NRTABS = 5   , NGFPLN = 2     )
C-----------------------------------------------------------------------
C IF YOU CHANGE NRTABS, YOU MUST CHANGE IDL ROUTINES C6ISPF.PRO AND
C CW_ADAS306_PROC.PRO
C-----------------------------------------------------------------------
      REAL*8     EMP
      PARAMETER( EMP = 1836.0D0 )
C-----------------------------------------------------------------------
      INTEGER    NGRND   , NTOT    , NBOT    , NTOP    , IZR     ,
     &           IZD     , INDD    , NENRGY  , NMINF   , NMAXF   ,
     &           IDZ0    , IRZ0    , IRZ1    , IRZ2    , IPAN    ,
     &           NBEAM   , ITHEOR  , IBSTAT  , IEMMS   , NOLINE  ,
     &           NPLINE  , NTAB    , NGRF    , NUMIN   , NUMAX
C-----------------------------------------------------------------------
      REAL*8     RAMSNO  , TEV     , TIEV    , DENS    , DENSZ   ,
     &           ZEFF    , BMAG    , EM
C-----------------------------------------------------------------------
      LOGICAL    OPEN10  , LGHOST  , LPARMS  , LSETL   ,
     &           LSETM   , LPEND   , LRTTB   , LPLT1   , LGRD1   ,
     &           LDEF1
CX   UNIX PORT - Following variables added for output selection options
      LOGICAL    LGRAPH  , L2FILE
C-----------------------------------------------------------------------
      CHARACTER  DATE*8     , REP*3      , DSFULL*80  , TITLEF*80  ,
     &           SYMBR*2    , SYMBD*2    , TITLE*40
CX   UNIX PORT - Following is name of output text file
      CHARACTER  SAVFIL*80
      CHARACTER  RMASS*7  
C-----------------------------------------------------------------------
      INTEGER    LFORMA(MXNENG)  , NL(MXOBSL)      , NU(MXOBSL)      ,
     &           NPL(MXPRSL)     , NPU(MXPRSL)     , IDTAB(MXTAB)    ,
     &           IDGRF(MXGRF)
C-----------------------------------------------------------------------
      REAL*8     ENRGYA(MXNENG)  , ALPHAA(MXNENG)  , XLCUTA(MXNENG)  ,
     &           PL2A(MXNENG)    , PL3A(MXNENG)    , XSECTA(MXNENG)  ,
     &           BMENA(MXBEAM)   , BMFRA(MXBEAM)   , EMISA(MXOBSL)   ,
     &           XLG(NGFPLN)     , XUG(NGFPLN)     , YLG(NGFPLN)     ,
     &           YUG(NGFPLN)     , QTHIN(MXNSHL)   , QTHEX(MXNSHL)   ,
     &           QTHCH(MXNSHL)   , QTHRC(MXNSHL)   , QEX(MXNSHL)     ,
     &           TOTPOP(MXPRSL)  , TOTEMI(MXPRSL)  , AVRGWL(MXPRSL)  ,
     &           QEFF(MXPRSL)    ,
     &           TBLF((MXNSHL*(MXNSHL+1))/2)
C-----------------------------------------------------------------------
      LOGICAL    LRTABS(NRTABS)
C-----------------------------------------------------------------------
      REAL*8     XSECNA(MXNENG,MXNSHL)                            ,
     &           FRACLA(MXNENG,(MXNSHL*(MXNSHL+1))/2)             ,
     &           FTHEXJ(MXJSHL,(MXNSHL*(MXNSHL+1))/2)             ,
     &           FTHCHJ(MXJSHL,(MXNSHL*(MXNSHL+1))/2)             ,
     &           FTHRCJ(MXJSHL,(MXNSHL*(MXNSHL+1))/2)             ,
     &           TBQMEP(2*MXJSHL,(MXNSHL*(MXNSHL+1))/2)           ,
     &           TBQMEM(2*MXJSHL,(MXNSHL*(MXNSHL+1))/2)           ,
     &           TBQMIP(2*MXJSHL,(MXNSHL*(MXNSHL+1))/2)           ,
     &           TBQMIM(2*MXJSHL,(MXNSHL*(MXNSHL+1))/2)           ,
     &           TBFMP(2*MXJSHL,(MXNSHL*(MXNSHL+1))/2)            ,
     &           TBFMM(2*MXJSHL,(MXNSHL*(MXNSHL+1))/2)            ,
     &           TBFM(2*MXJSHL,(MXNSHL*(MXNSHL+1))/2)
C-----------------------------------------------------------------------
      REAL*8     TBLPOP(2*MXJSHL,2*MXNSHL-3,MXPRSL)          ,
     &           TBLEMI(2*MXJSHL,2*MXNSHL-3,MXPRSL)          ,
     &           TBLWLN(2*MXJSHL,2*MXNSHL-3,MXPRSL)
C-----------------------------------------------------------------------
      INTEGER    PIPEIN,     PIPEOU,     ISTOP
      PARAMETER (PIPEIN = 5, PIPEOU = 6)
C-----------------------------------------------------------------------
C *************************** MAIN PROGRAM ****************************
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C SET MACHINE DEPENDANT ADAS CONFIGURATION VALUES.
C-----------------------------------------------------------------------
C
      CALL XX0000
C
C-----------------------------------------------------------------------
C GET CURRENT DATE.
C-----------------------------------------------------------------------
C
      CALL XXDATE( DATE )
C
C-----------------------------------------------------------------------
C INITIALIZE VALUES.
C-----------------------------------------------------------------------
C
      OPEN10 = .FALSE.
      LGHOST = .FALSE.
      NTOT = MXNSHL
      NTOP = MXNSHL
      NGRND = 1
      NBOT = 2
C
C-----------------------------------------------------------------------
C START OF CONTROL LOOP.
C-----------------------------------------------------------------------
C
    1 CONTINUE
C
C-----------------------------------------------------------------------
C IF FILE IS ACTIVE ON UNIT 10 THEN CLOSE THE UNIT.
C-----------------------------------------------------------------------
C
      IF (OPEN10) THEN
         CLOSE( IUNT10 )
         OPEN10 = .FALSE.
      ENDIF
C
C-----------------------------------------------------------------------
C GET INPUT DATA SET NAME (FROM IDL).
C-----------------------------------------------------------------------
C
      CALL C6SPF0( REP , DSFULL)
C
C-----------------------------------------------------------------------
C IF PROGRAM RUN IS COMPLETED END RUN.
C-----------------------------------------------------------------------
C
      IF (REP .EQ. 'YES') THEN
C MAYBE NEED TO CLOSE OUTPUT CHANNEL??
         GOTO 9999
      ENDIF
C
C-----------------------------------------------------------------------
C OPEN INPUT DATA FILE - DSFULL
C-----------------------------------------------------------------------
C
      OPEN( UNIT=IUNT10 , FILE=DSFULL , STATUS='OLD' )
      OPEN10 = .TRUE.
C
C-----------------------------------------------------------------------
C FETCH DATA FROM SELECTED DATASET
C-----------------------------------------------------------------------
C
      call xxdata_01( iunt10 , mxneng , mxnshl ,
     &                symbr  , symbd  , izr    , izd    ,
     &                indd   , nenrgy , nminf  , nmaxf  ,
     &                lparms , lsetl  , enrgya ,
     &                alphaa , lforma , xlcuta , pl2a   ,
     &                pl3a   , xsecta , xsecna , fracla   
     &              )
C
C-----------------------------------------------------------------------
C SET UP TARGET CHARGES AND LOWER PRINCIPAL QUANTUM NUMBERS.
C-----------------------------------------------------------------------
C
      CALL C6CHRG( SYMBD  , IZD    , SYMBR  , IZR    , IDZ0   ,
     &             IRZ0   , IRZ1   , IRZ2   , NGRND  , NBOT     )
C
C-----------------------------------------------------------------------
C CONVERT L AND M RESOLVED CROSS-SECTIONS TO FRACTIONS.
C-----------------------------------------------------------------------
C
      CALL CXFRAC( MXNENG , MXNSHL , NENRGY , NMINF  , NMAXF  ,
     &             LSETL  , XSECNA , FRACLA 
     &           )
C
C-----------------------------------------------------------------------
C EXTRAPOLATE N AND L RESOLVED CROSS-SECTIONS BEYOND NMINF AND NMAXF.
C-----------------------------------------------------------------------
C
      CALL CXEXTR( MXNENG , MXNSHL , NMINF  , NMAXF  , NENRGY ,
     &             LPARMS , ALPHAA , LFORMA , XLCUTA , PL2A   ,
     &             PL3A   , XSECNA , FRACLA                     )
C
C-----------------------------------------------------------------------
C SET UP ISPF FUNCTION POOL PARAMETERS (NON-READABLE VARIABLES).
C-----------------------------------------------------------------------
C
      CALL CXSETP( MXTAB  , MXGRF  , SYMBD  , IDZ0   , SYMBR  ,
     &             IRZ0   , IRZ1   , IRZ2   , NGRND  , NTOT     )
C
C-----------------------------------------------------------------------
C GET USER-DEFINED VALUES (FROM ISPF PANELS)
C-----------------------------------------------------------------------
C
    2 CONTINUE
C
      IPAN = 0
      LPEND = .FALSE.
C
      CALL C6ISPF( MXBEAM , MXOBSL , MXPRSL , MXTAB  , MXGRF  ,
     &             NRTABS , NGFPLN , NGRND  , NTOT   , IPAN   ,
     &             LPEND  , TITLE  , RAMSNO , TIEV   , TEV    ,
     &             DENSZ  , DENS   , ZEFF   , BMAG   , NBEAM  ,
     &             BMFRA  , BMENA  , ITHEOR , IBSTAT , IEMMS  ,
     &             NOLINE , NU     , NL     , EMISA  , NPLINE ,
     &             NPU    , NPL    , NTAB   , NGRF   , IDTAB  ,
     &             IDGRF  , LRTTB  , LRTABS , LPLT1  , LGRD1  ,
     &             LDEF1  , XLG    , XUG    , YLG    , YUG      )

C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C 
      READ(PIPEIN,*) ISTOP
      IF(ISTOP.EQ.1) GOTO 9999
C
C-----------------------------------------------------------------------
C IF END OF CURRENT FILE REQUESTED GO BACK TO INPUT FILE PANEL.
C-----------------------------------------------------------------------
C
      IF (LPEND) GOTO 1

C-----------------------------------------------------------------------
C SET THE RECEIVER ATOMIC MASS.
C-----------------------------------------------------------------------
      
      WRITE(RMASS,'(F7.3)')RAMSNO
      CALL XXRAMS(RMASS)
C
C-----------------------------------------------------------------------
C  SET UP TABLES OF ALL NECESSARY ATOMIC DATA FOR SUBSEQUENT LOOKUP.
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C FILL RADIATIVE LIFETIME TABLE.
C-----------------------------------------------------------------------
C
      CALL CXTBLF( MXNSHL , IRZ1 , NBOT , NTOP , TBLF )
C
C-----------------------------------------------------------------------
C FILL ELECTRON IMPACT IONISATION RATE TABLE.
C-----------------------------------------------------------------------
C
      CALL C6TBIN( MXNSHL , IRZ1 , NBOT , NTOP , TEV , QTHIN )
C
C-----------------------------------------------------------------------
C FILL EXCITATION RATE TABLES.
C-----------------------------------------------------------------------
C
      CALL C6TBEX( MXNSHL , MXJSHL , IRZ1   , NBOT   , NTOP   ,
     &             NGRND  , TEV    , QTHEX  , FTHEXJ            )
C
C-----------------------------------------------------------------------
C CALCULATE CHARGE EXCHANGE RECOMBINATION RATES.
C-----------------------------------------------------------------------
C
      IF (ITHEOR .EQ. 2) THEN
C
C-----------------------------------------------------------------------
C EIKONAL TREATMENT.
C-----------------------------------------------------------------------
C
         CALL C6QEIK( MXNSHL , MXJSHL , MXBEAM , IRZ1   ,
     &                IBSTAT , NBOT   , NTOP   , NBEAM  ,
     &                BMENA  , BMFRA  , QTHCH  , FTHCHJ   )
C
C-----------------------------------------------------------------------
C
      ELSE
C
C-----------------------------------------------------------------------
C NON-EIKONAL TREATMENT.
C-----------------------------------------------------------------------
C
         CALL C6QXCH ( MXNENG , MXNSHL , MXJSHL , MXBEAM ,
     &                 NBEAM  , BMENA  , BMFRA  , NBOT   ,
     &                 NTOP   , NMINF  , NMAXF  , NENRGY ,
     &                 ENRGYA , ALPHAA , XSECNA , FRACLA ,
     &                 QTHCH  , FTHCHJ
     &               )
C
C-----------------------------------------------------------------------
C
      ENDIF
C
C-----------------------------------------------------------------------
C CALCULATE FREE ELECTRON RECOMBINATION RATES TO L,J LEVELS.
C-----------------------------------------------------------------------
C
      CALL C6TBRC( MXNSHL , MXJSHL , IRZ1   , NBOT   , NTOP   ,
     &             TEV    , QTHRC  , FTHRCJ                     )
C
C-----------------------------------------------------------------------
C FILL L SHELL MIXING TRANSITION RATE TABLES.
C-----------------------------------------------------------------------
C
      CALL C6TBQM( MXNSHL , MXJSHL , IRZ0   , IRZ1   , NBOT   ,
     &             NTOP   , TEV    , DENS   , ZEFF   , TIEV   ,
     &             EMP    , TBLF   , TBQMEP , TBQMEM , TBQMIP ,
     &             TBQMIM                                       )
C
C-----------------------------------------------------------------------
C FILL L SHELL MIXING TRANSITION RATES DUE TO MAGNETIC FIELDS.
C-----------------------------------------------------------------------
C
      CALL C6TBFM( MXNSHL , MXJSHL , IRZ0   , IRZ1   , RAMSNO ,
     &             NBOT   , NTOP   , BMAG   , TIEV   , TBLF   ,
     &             TBFMP  , TBFM   , TBFMM                      )
C
C-----------------------------------------------------------------------
C SELECT CHARGE EXCHANGE, EXCITATION OR RECOMBINATION EMISSION MEASURE
C SOLUTION
C-----------------------------------------------------------------------
C
      IF(IEMMS .EQ. 1) THEN
C
         CALL C6EMIS( MXNSHL , MXJSHL , MXOBSL , MXPRSL , IRZ0   ,
     &                IRZ1   , NGRND  , NTOT   , NBOT   , DENSZ  ,
     &                DENS   , NOLINE , NU     , NL     , EMISA  ,
     &                NPLINE , NPU    , NPL    , QTHCH  , FTHCHJ ,
     &                QTHIN  , TBQMEP , TBQMEM , TBQMIP , TBQMIM ,
     &                TBFMP  , TBFM   , TBFMM  , NUMIN  , NUMAX  ,
     &                EM     , QEX    , TOTPOP , TOTEMI , AVRGWL ,
     &                QEFF   , TBLPOP , TBLEMI , TBLWLN            )
C
      ELSE IF (IEMMS .EQ. 2) THEN
C
         CALL C6EMIS( MXNSHL , MXJSHL , MXOBSL , MXPRSL , IRZ0   ,
     &                IRZ1   , NGRND  , NTOT   , NBOT   , DENSZ  ,
     &                DENS   , NOLINE , NU     , NL     , EMISA  ,
     &                NPLINE , NPU    , NPL    , QTHEX  , FTHEXJ ,
     &                QTHIN  , TBQMEP , TBQMEM , TBQMIP , TBQMIM ,
     &                TBFMP  , TBFM   , TBFMM  , NUMIN  , NUMAX  ,
     &                EM     , QEX    , TOTPOP , TOTEMI , AVRGWL ,
     &                QEFF   , TBLPOP , TBLEMI , TBLWLN            )
C
      ELSE IF (IEMMS .EQ. 3) THEN
C
         CALL C6EMIS( MXNSHL , MXJSHL , MXOBSL , MXPRSL , IRZ0   ,
     &                IRZ1   , NGRND  , NTOT   , NBOT   , DENSZ  ,
     &                DENS   , NOLINE , NU     , NL     , EMISA  ,
     &                NPLINE , NPU    , NPL    , QTHRC  , FTHRCJ ,
     &                QTHIN  , TBQMEP , TBQMEM , TBQMIP , TBQMIM ,
     &                TBFMP  , TBFM   , TBFMM  , NUMIN  , NUMAX  ,
     &                EM     , QEX    , TOTPOP , TOTEMI , AVRGWL ,
     &                QEFF   , TBLPOP , TBLEMI , TBLWLN            )
C
      ENDIF
C
C-----------------------------------------------------------------------
CX ADDED THE NEW CALL TO C8SPF1 ROUTINE WHICH COMMUNICATES WITH IDL VIA
CX A PIPE TO GET USER SELECTED OUTPUT OPTIONS
C-----------------------------------------------------------------------
3     CONTINUE

      CALL C6SPF1( DSFULL        ,
     &             LGRAPH        , L2FILE        , SAVFIL   ,
     &             LPEND
     &           )
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C 
      READ(PIPEIN,*) ISTOP
      IF(ISTOP.EQ.1) GOTO 9999

CX IF USER HAS SELECTED 'CANCEL' RETURN TO PROCESSING OPTIONS WINDOW.
      IF (LPEND) GOTO 2
C
C  IF SELECTED - GENERATE GRAPH
CX UNIX PORT - C6OUTG COMMUNICATES WITH IDL VIA PIPE.
C-----------------------------------------------------------------------
C
      IF (LGRAPH) THEN
C
C-----------------------------------------------------------------------
C OUTPUT GRAPHS.
C-----------------------------------------------------------------------
C
         CALL C6OUTG( MXNSHL , MXJSHL , MXBEAM , MXOBSL , MXPRSL ,
     &        MXGRF  , NGFPLN , NGRF   , IDGRF  , LPLT1  ,
     &        LGRD1  , LDEF1  , DATE   , TITLE  , DSFULL ,
     &        SYMBD  , IDZ0   , SYMBR  , IRZ0   , IRZ1   ,
     &        IRZ2   , RAMSNO , TEV    , TIEV   , DENS   ,
     &        DENSZ  , ZEFF   , BMAG   , NBEAM  , BMFRA  ,
     &        BMENA  , NOLINE , NU     , NL     , EMISA  ,
     &        ITHEOR , IBSTAT , IEMMS  , NPLINE , NPU    ,
     &        NPL    , XLG    , XUG    , YLG    , YUG    ,
     &        QEFF   , TBLEMI , TBLWLN , LGHOST            )

         READ(PIPEIN,*)ISTOP
         IF(ISTOP.EQ.1) GOTO 9999

      ENDIF
C
C-----------------------------------------------------------------------
C OUTPUT TEXT RESULTS.
C-----------------------------------------------------------------------
C
      IF (L2FILE) THEN
         OPEN(UNIT=IUNT7, FILE = SAVFIL, STATUS='UNKNOWN')
         CALL C6OUT0( IUNT7  , MXNSHL , MXJSHL , MXBEAM , MXOBSL ,
     &        MXPRSL , MXTAB  , NRTABS , NUMIN  , NUMAX  ,
     &        NBOT   , NTOP   , NTAB   , IDTAB  , LRTTB  ,
     &        LRTABS , DATE   , TITLE  , DSFULL , SYMBD  ,
     &        IDZ0   , SYMBR  , IRZ0   , IRZ1   , IRZ2   ,
     &        RAMSNO , TEV    , TIEV   , DENS   , DENSZ  ,
     &        ZEFF   , BMAG   , NBEAM  , BMFRA  , BMENA  ,
     &        NOLINE , NU     , NL     , EMISA  , ITHEOR ,
     &        IBSTAT , IEMMS  , EM     , QEX    , TBLF   ,
     &        QTHEX  , FTHEXJ , QTHCH  , FTHCHJ , QTHRC  ,
     &        FTHRCJ , QTHIN  , TBQMEP , TBQMEM , TBQMIP ,
     &        TBQMIM , TBFMP  , TBFM   , TBFMM  , NPLINE ,
     &        NPL    , NPU    , TOTPOP , TOTEMI , AVRGWL ,
     &        QEFF   , TBLPOP , TBLEMI , TBLWLN            )
         CLOSE(IUNT7)
      ENDIF
C
C-----------------------------------------------------------------------
C
      GOTO 3
C
C-----------------------------------------------------------------------
C
 9999 CONTINUE
      STOP
C
C-----------------------------------------------------------------------
C
      END
