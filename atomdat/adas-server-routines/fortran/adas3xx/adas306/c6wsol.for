CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas306/c6wsol.for,v 1.1 2004/07/06 11:54:05 whitefor Exp $ Date $Date: 2004/07/06 11:54:05 $
CX
      SUBROUTINE C6WSOL( MXNSHL , MXJSHL , IZ1    , NGRND  ,
     &                   N      , DENSZ  , DENS   , QTHIN  ,
     &                   TBQMEP , TBQMEM , TBQMIP , TBQMIM ,
     &                   TBFMM  , TBFM   , TBFMP  , RHS
     &                 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C6WSOL *********************
C
C  PURPOSE:
C
C  CALLING PROGRAM: C6WFIL
C
C  INPUT : (I*4)  MXNSHL   = MAXIMUM NUMBER OF N SHELLS.
C  INPUT : (I*4)  MXJSHL   = MAXIMUM NUMBER OF J SUB-SHELLS.
C  INPUT : (I*4)  IZ1      = ION CHARGE.
C  INPUT : (I*4)  NGRND    = PRINCIPAL QUANTUM NUMBER OF GROUND STATE.
C  INPUT : (I*4)  N        = PRINCIPAL QUANTUM NUMBER.
C  INPUT : (R*8)  DENSZ    = PLASMA ION DENSITY.
C                            UNITS: CM-3
C  INPUT : (R*8)  DENS     = ELECTRON DENSITY.
C                            UNITS: CM-3
C  INPUT : (R*8)  QTHIN()  = IONISATION RATE COEFFICIENT.
C                            UNITS: CM3 SEC-1
C                            DIMENSION: N SHELL INDEX.
C  INPUT : (R*8)  TBQMEP(,)= ELECTRON RATE COEFFT. FOR NLJ->NL+1J'.
C                            1ST DIMENSION: J->J' TRANSITION INDEX.
C                            2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C  INPUT : (R*8)  TBQMEM(,)= ELECTRON RATE COEFFT. FOR NLJ->NL-1J'.
C                            1ST DIMENSION: J->J' TRANSITION INDEX.
C                            2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C  INPUT : (R*8)  TBQMIP(,)= POSITIVE ION RATE COEFFT. FOR NLJ->NL+1J'.
C                            1ST DIMENSION: J->J' TRANSITION INDEX.
C                            2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C  INPUT : (R*8)  TBQMIM(,)= POSITIVE ION RATE COEFFT. FOR NLJ->NL-1J'.
C                            1ST DIMENSION: J->J' TRANSITION INDEX.
C                            2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C  INPUT : (R*8)  TBFMP(,) = RATE COEFFT. FOR NLJ->NL+1J'.
C                            1ST DIMENSION: J->J' TRANSITION INDEX.
C                            2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C  INPUT : (R*8)  TBFM(,)  = RATE COEFFT. FOR NLJ->NL+1J'.
C                            1ST DIMENSION: J->J' TRANSITION INDEX.
C                            2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C  INPUT : (R*8)  TBFMM(,) = RATE COEFFT. FOR NLJ->NLJ' FOR STATE I.
C                            1ST DIMENSION: J->J' TRANSITION INDEX.
C                            2ND DIMENSION: REFERENCED BY I4IDFL(N,L).
C
C  I/O   : (R*8)  RHS()    =
C                            DIMENSION: REFERENCED BY L+1.
C
C  PARAM : (I*4)  MXN      = MXNSHL.
C  PARAM : (I*4)  MXJ      = MXJSHL.
C
C          (I*4)  L       = ORBITAL QUANTUM NUMBER.
C          (I*4)  L1      = ORBITAL QUANTUM NUMBER.
C          (I*4)  J       = J->J' TRANSITION INDEX.
C          (I*4)  IDL     = TABLE INDEX.
C          (I*4)  N1      = PRINCIPAL QUANTUM NUMBER.
C          (I*4)  LP      = ARRAY INDEX = L+1.
C
C          (R*8)  AA()    = LJ RESOLVED A-VALUE.
C                           DIMENSION: TRANSITION INDEX WHERE:
C                                      1 GIVES LU+0.5 --> LL+0.5
C                                      2 GIVES LU+0.5 --> LL-0.5
C                                      3 GIVES LU-0.5 --> LL+0.5
C                                      4 GIVES LU-0.5 --> LL-0.5
C
C          (R*8)  VDS(,)  =
C          (R*8)  VDI(,)  =
C          (R*8)  VD(,)   =
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4UNIT     ADAS      RETURNS UNIT NO. FOR OUTPUT OF MESSAGES.
C          I4IDFL     ADAS      RETURNS UNIQUE INDEX GIVEN QUANTUM
C                               NUMBERS N AND L.
C
C NOTES:
C       1) THE J->J' TRANSITION INDEX IS AS FOLLOWS:
C              1 : J=L+0.5 -> J'=L'+0.5
C              2 : J=L+0.5 -> J'=L'-0.5
C              3 : J=L-0.5 -> J'=L'+0.5
C              4 : J=L-0.5 -> J'=L'-0.5
C
C       2) FOR MATRIX HANDLING PURPOSES, L=0 STATES ARE TREATED AS TWO
C          STATES OF J=0.5. RATES INTO EACH OF THESE ARE SET EQUAL TO
C          HALF THE TOTAL INTO A TRUE L=0,J=0.5 STATE. RATES OUT OF
C          EACH STATE ARE SET EQUAL TO THE TOTAL OUT OF A TRUE L=0,J=0.5
C          STATE. THUS EACH ARTIFICIAL STATE HAS THE SAME POPULATION
C          WHICH IS HALF THE TOTAL OF THE TRUE L=0,J=0.5 STATE.
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    08/11/93
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    22ND MAY 1996
C
C VERSION: 1.1                          DATE: 22-05-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST VERSION. IBM VERSION NOT CHANGED
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    I4UNIT  , I4IDFL
C-----------------------------------------------------------------------
      INTEGER    MXN        , MXJ
      PARAMETER( MXN = 20   , MXJ = 2 )
C-----------------------------------------------------------------------
      INTEGER    MXNSHL  , MXJSHL  , NGRND   , N       , IZ1
      INTEGER    L       , L1      , J       , IDL     , N1      ,
     &           LP
C-----------------------------------------------------------------------
      REAL*8     DENSZ   , DENS
C-----------------------------------------------------------------------
      REAL*8     QTHIN(MXNSHL)
      REAL*8     AA(2*MXJ)
C-----------------------------------------------------------------------
      REAL*8     TBQMEP(2*MXJSHL,(MXNSHL*(MXNSHL+1))/2)  ,
     &           TBQMEM(2*MXJSHL,(MXNSHL*(MXNSHL+1))/2)  ,
     &           TBQMIP(2*MXJSHL,(MXNSHL*(MXNSHL+1))/2)  ,
     &           TBQMIM(2*MXJSHL,(MXNSHL*(MXNSHL+1))/2)  ,
     &           TBFMM(2*MXJSHL,(MXNSHL*(MXNSHL+1))/2)   ,
     &           TBFM(2*MXJSHL,(MXNSHL*(MXNSHL+1))/2)    ,
     &           TBFMP(2*MXJSHL,(MXNSHL*(MXNSHL+1))/2)   ,
     &           RHS(MXJSHL,MXNSHL)
      REAL*8     VDS(2*MXJ,MXN)  , VDI(2*MXJ,MXN)  , VD(2*MXJ,MXN)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C CHECK PARAMETERS 'MXN' AND 'MXJ'.
C-----------------------------------------------------------------------
C
      IF (MXN .LT. MXNSHL) THEN
         WRITE(I4UNIT(-1),1000) MXN, MXNSHL
         STOP
      ENDIF
C
      IF (MXJ .LT. MXJSHL) THEN
         WRITE(I4UNIT(-1),1001) MXJ, MXJSHL
         STOP
      ENDIF
C
C-----------------------------------------------------------------------
C ESTABLISH DIAGONAL AND OFF-DIAGONAL VECTORS OF MATRIX M FOR LEVEL N.
C-----------------------------------------------------------------------
C
      DO 1 J = 1 , 2 * MXJSHL
         VDS(J,1) = 0.0D0
         VDI(J,1) = 0.0D0
    1 CONTINUE
C
      DO 2 L = 1 , N-1
         DO 3 J = 1 , 4
            LP = L + 1
            IDL = I4IDFL( N , L )
            VDS(J,LP) = -DENSZ * TBQMIM(J,IDL) - DENS * TBQMEM(J,IDL) -
     &                  TBFMM(J,IDL)
            IDL = I4IDFL( N , L-1 )
            VDI(J,LP) = -DENSZ * TBQMIP(J,IDL) - DENS * TBQMEP(J,IDL) -
     &                  TBFMP(J,IDL)
    3    CONTINUE
    2 CONTINUE
C
C-----------------------------------------------------------------------
C
      VDS(1,2) = 0.5D0 * VDS(1,2)
      VDS(2,2) = VDS(1,2)
      VDS(3,2) = 0.5D0 * VDS(3,2)
      VDS(4,2) = VDS(3,2)
      VDI(3,2) = VDI(1,2)
      VDI(4,2) = VDI(2,2)
C
C-----------------------------------------------------------------------
C
      DO 4 L = 0 , N-1
C
         LP = L + 1
         IDL = I4IDFL( N , L )
C
         DO 5 J = 1 , 4
            VD(J,LP) = -TBFM(J,IDL)
    5    CONTINUE
C
         IF (LP .LT. N) THEN
            VD(1,LP) = -VD(2,LP) - VDI(1,LP+1) - VDI(2,LP+1) -
     &                 VDS(1,LP) - VDS(2,LP)
            VD(4,LP) = -VD(3,LP) - VDI(3,LP+1) - VDI(4,LP+1) -
     &                 VDS(3,LP) - VDS(4,LP)
         ELSE
            VD(1,LP) = -VD(2,LP) - VDS(1,LP) - VDS(2,LP)
            VD(4,LP) = -VD(3,LP) - VDS(3,LP) - VDS(4,LP)
         ENDIF
C
         DO 6 N1 = NGRND , N-1
C
            L1 = L + 1
            CALL C6AJTB( MXJ , IZ1 , N , L , N1 , L1 , AA )
            IF (L .EQ. 0) THEN
               AA(3) = AA(1)
               AA(4) = AA(2)
            ENDIF
            VD(1,LP) = VD(1,LP) + AA(1) + AA(2)
            VD(4,LP) = VD(4,LP) + AA(3) + AA(4)
C
            L1 = L - 1
            CALL C6AJTB( MXJ , IZ1 , N , L , N1 , L1 , AA )
            VD(1,LP) = VD(1,LP) + AA(1) + AA(2)
            VD(4,LP) = VD(4,LP) + AA(3) + AA(4)
C
    6    CONTINUE
C
         VD(1,LP) = VD(1,LP) + DENS * QTHIN(N)
         VD(4,LP) = VD(4,LP) + DENS * QTHIN(N)
C
    4 CONTINUE
C
C-----------------------------------------------------------------------
C SOLVE N LEVEL EQUATIONS USING TRI-DIGONAL PARTITION ROUTINE.
C-----------------------------------------------------------------------
C
      CALL C6PMIN( MXNSHL , MXJSHL , N , VD , VDS , VDI , RHS )
C
C-----------------------------------------------------------------------
C
 1000 FORMAT( 1X, 32('*'), ' C6WSOL ERROR ', 32('*') //
     &        2X, 'INTERNAL PARAMETER ''MXN'' IS LESS THAN INPUT ',
     &            'ARGUMENT ''MXNSHL''.'/
     &        2X, 'MXN = ', I3 , '   MXNSHL = ', I3/
     &        2X, 'INCREASE PARAMETER ''MXN'' IN SUBROUTINE C6WSOL.' //
     &        1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )
 1001 FORMAT( 1X, 32('*'), ' C6WSOL ERROR ', 32('*') //
     &        2X, 'INTERNAL PARAMETER ''MXJ'' IS LESS THAN INPUT ',
     &            'ARGUMENT ''MXJSHL''.'/
     &        2X, 'MXJ = ', I3 , '   MXJSHL = ', I3/
     &        2X, 'INCREASE PARAMETER ''MXJ'' IN SUBROUTINE C6WSOL.' //
     &        1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
