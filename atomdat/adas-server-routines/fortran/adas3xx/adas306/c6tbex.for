CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas306/c6tbex.for,v 1.1 2004/07/06 11:53:42 whitefor Exp $ Date $Date: 2004/07/06 11:53:42 $
CX
      SUBROUTINE C6TBEX( MXNSHL , MXJSHL , IZ1    , NBOT   ,
     &                   NTOP   , NGRND  , TEV    , QTHEX  ,
     &                   FTHEXJ
     &                 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C6TBEX *********************
C
C  PURPOSE:  SETS UP A TABLE OF ELECTRON IMPACT EXCITATION RATE
C            COEFFICIENTS FOR A HYDROGENIC ION FROM THE GROUND STATE
C            TO EXCITED NL LEVELS.
C
C  CALLING PROGRAM: ADAS306.
C
C  INPUT : (I*4)  MXNSHL    = MAXIMUM NO. OF N SHELLS.
C  INPUT : (I*4)  MXJSHL    = MAXIMUM NO. J SUB-SHELLS.
C  INPUT : (I*4)  IZ1       = ION CHARGE.
C  INPUT : (I*4)  NBOT      = MINIMUM PRINCIPAL QUANTUM NUMBER.
C  INPUT : (I*4)  NTOP      = MAXIMUM PRINCIPAL QUANTUM NUMBER.
C  INPUT : (I*4)  NGRND     =
C  INPUT : (R*8)  TEV       = ELECTRON TEMPERATURE.
C                             UNITS: EV
C
C  OUTPUT: (R*8)  QTHEX()   = TABLE OF N-LEVEL EXCITATION RATE
C                             COEFFICIENTS.
C                             UNITS:
C                             DIMENSION: N-SHELL
C  OUTPUT: (R*8)  FTHEXJ(,) = TABLE OF NLJ-LEVEL EXCITATION RATE
C                             COEFFICIENTS EXPRESSED AS FRACTION OF
C                             CORRESPONDING N-LEVEL RATE.
C                             1ST DIMENSION: J SUB-SHELL
C                                            1 => J=L+0.5
C                                            2 => J=L-0.5
C                             2ND DIMENSION: REFERENCED BY I4IDFL(N,L)
C
C  PARAM : (I*4)  MXN       = 'MXNSHL'.
C
C          (I*4)  N         = N-SHELL INDEX.
C          (I*4)  L         = L-SHELL INDEX.
C          (I*4)  J         = J-SHELL INDEX.
C          (I*4)  IDL       = L-RESOLVED INDEX.
C
C          (R*8)  XL        = REAL VALUE = L.
C          (R*8)  WL        =
C
C          (R*8)  TBQEX()   = TABLE OF NL-LEVEL EXCITATION RATE
C                             COEFFICIENTS.
C                             UNITS:
C                             DIMENSION: REFERENCED BY I4IDFL(N,L).
C          (R*8)  FTHEX()   = TABLE OF NL-LEVEL EXCITATION RATE
C                             COEFFICIENTS EXPRESSED AS FRACTION OF
C                             CORRESPONDING N-LEVEL RATE.
C                             DIMENSION: REFERENCED BY I4IDFL(N,L).
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4UNIT     ADAS      RETURNS UNIT NO. FOR OUTPUT OF MESSAGES.
C          I4IDFL     ADAS      RETURNS UNIQUE INDEX GIVEN QUANTUM
C                               NUMBERS N AND L.
C          CXTBEX     ADAS      CALCULATES N-LEVEL AND NL-LEVEL
C                               EXCITATION RATE COEFFICIENTS. NL RATES
C                               ARE GIVEN AS FRACTION OF CORRESPONDING
C                               N RATE.
C
C AUTHOR:   JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C           K1/0/81
C           JET EXT. 5183
C
C DATE:     22/10/93
C
C UNIX-IDL PORT:
C
C AUTHOR:  WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    22ND MAY 1996
C
C VERSION: 1.1                          DATE: 22-05-96
C MODIFIED: WILLIAM OSBORN
C               - FIRST VERSION. IBM VERSION NOT CHANGED
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    I4UNIT  , I4IDFL
C-----------------------------------------------------------------------
      INTEGER    MXN
      PARAMETER( MXN = 20 )
C-----------------------------------------------------------------------
      INTEGER    MXNSHL  , MXJSHL  , IZ1     , NBOT    , NTOP   ,
     &           NGRND
      INTEGER    N       , L       , J       , IDL
C-----------------------------------------------------------------------
      REAL*8     TEV
      REAL*8     XL      , WL
C-----------------------------------------------------------------------
      REAL*8     QTHEX(MXNSHL)
      REAL*8     TBQEX((MXN*(MXN+1))/2)  ,
     &           FTHEX((MXN*(MXN+1))/2)
      REAL*8     FTHEXJ(MXJSHL,(MXNSHL*(MXNSHL+1))/2)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C CHECK INTERNAL PARAMETER 'MXN'.
C-----------------------------------------------------------------------
C
      IF (MXN .LT. MXNSHL) THEN
         WRITE(I4UNIT(-1),1000) MXN, MXNSHL
         STOP
      ENDIF
C
C-----------------------------------------------------------------------
C ZERO EXCITATION RATE TABLES.
C-----------------------------------------------------------------------
C
      DO 1 N = 1 , MXNSHL
         QTHEX(N) = 0.0D0
    1 CONTINUE
C
      DO 2 IDL = 1 , I4IDFL( MXNSHL , MXNSHL-1 )
         DO 3 J = 1 , MXJSHL
            FTHEXJ(J,IDL) = 0.0D0
    3    CONTINUE
    2 CONTINUE
C
C-----------------------------------------------------------------------
C FILL N-LEVEL AND NL-LEVEL TABLES.
C-----------------------------------------------------------------------
C
      CALL CXTBEX( MXNSHL , IZ1    , NBOT   , NTOP   , NGRND  ,
     &             TEV    , TBQEX  , QTHEX  , FTHEX             )
C
C-----------------------------------------------------------------------
C SPLIT NL-LEVEL RATES INTO NLJ-RATES.
C-----------------------------------------------------------------------
C
      DO 4 N = 1 , MXNSHL
         DO 5 L = 0 , N-1
            XL = DFLOAT( L )
            WL = 2.0D0 * XL + 1.0D0
            IDL = I4IDFL( N , L )
            FTHEXJ(1,IDL) = ( XL + 1.0D0 ) * FTHEX(IDL) / WL
            FTHEXJ(2,IDL) = XL * FTHEX(IDL) / WL
    5    CONTINUE
    4 CONTINUE
C
C-----------------------------------------------------------------------
C
 1000 FORMAT( 1X, 32('*'), ' C6TBEX ERROR ', 32('*') //
     &        2X, 'INTERNAL PARAMETER ''MXN'' IS LESS THAN INPUT ',
     &            'ARGUMENT ''MXNSHL''.'/
     &        2X, 'MXN = ', I3 , '   MXNSHL = ', I3/
     &        2X, 'INCREASE PARAMETER ''MXN'' IN SUBROUTINE C6TBEX.' //
     &        1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
