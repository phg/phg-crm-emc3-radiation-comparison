CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas314/cether.for,v 1.3 2007/05/24 15:27:35 mog Exp $ Date $Date: 2007/05/24 15:27:35 $
CX
       SUBROUTINE CETHER ( NDENR  , NDSHL  ,
     &                     AMDON  , AMREC  , CATYP  , DREN   , 
     &                     ILTYP  , IEXTYP ,
     &                     LSETL  , NMIN   , NMAX   ,
     &                     NENIN  , ENIN   , NENOUT , ENOUT  ,
     &                     SGTIN  , SGNIN  , SGLIN  ,
     &                     RCTOUT , RCNOUT , RCLOUT
     &                   )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ********************** FORTRAN77 SUBROUTINE: CETHER *****************
C
C  VERSION: 1.0  
C
C  PURPOSE:  OBTAINS RATE COEFFICIENTS FOR DONOR/RECEIVER CHARGE
C            EXCHANGE COLLISIONS FOR CASES OF
C            MONOENERGETIC DONOR/THERMAL RECEIVER, THERMAL
C            DONOR/MONOENERGETIC RECEIVER, THERMAL DONOR/THERMAL
C            RECEIVER (SAME TEMPERATURE) FROM CROSS-SECTION TABULATIONS
C
C  CALLING PROGRAM:  ADAS314
C
C  SUBROUTINE:
C  INPUT :  (I*4)  NDENR   = MAX. NUMBER OF ENERGIES
C                            ALLOWED IN CROSS-SECTION FILE
C                            OR TEMPERATURES IN THERMAL
C                            AVERAGED RATE COEFFT. OUTPUT FILE.
C  INPUT :  (I*4)  NDSHL   = PARAMETER = MAX. NUMBER OF N-SHELLS
C                                        ALLOWED IN CROSS-SECTION FILE
C  INPUT :  (R*8)  AMDON    = DONOR MASS NUMBER
C  INPUT :  (R*8)  AMREC    = RECEIVER MASS NUMBER
C  INPUT :  (C2)   CATYP    = 'TT' THERMAL/THERMAL (EQUAL TEMP. CASE)
C                             'TR' THERMAL RECEIVER, MONOENERGETIC DONOR
C                             'TD' THERMAL DONOR, MONOENERGETIC RECEIVER
C  INPUT : (R*8)   DREN     = DONOR ENERGY    ( 'TR' CASE )
C                             RECEIVER ENERGY ( 'TD' CASE )
C  INPUT : (I*4)   ILTYP    = TYPE FOR LOW ENERGY CROSS-SECTION EXTRAPOL
C                             REDUNDANT - SUPERCEEDED BY IEXTYP
C          (I*4)   IEXTYP   = 1 => SET LOWER ENERGIES TO FIRST POINT IN DATA 
C                           = 2 => SET LOWER ENERGIES TO 0.0
C  INPUT : (L*4)   LSETL    = .TRUE.  => L-RESOLVED DATA READ
C                             .FALSE. => NO L-RESOLVED DATA READ
C  INPUT : (L*4)   LSETM    = .TRUE.  => M-RESOLVED DATA READ
C                             .FALSE. => NO M-RESOLVED DATA READ
C  INPUT : (I*4)   NMIN     = LOWEST N-SHELL IN DATA SET
C  INPUT : (I*4)   NMAX     = HIGHEST N-SHELL IN DATA SET
C  INPUT : (I*4)   NENIN    = NUMBER OF ENERGIES IN INPUT DATA SET
C  INPUT : (R*8)   ENIN()   = ENERGIES (EV/AMU) IN INPUT DATA SET
C  INPUT : (I*4)   NENOUT   = NUMBER OF ENERGIES FOR OUTPUT DATA SET
C  INPUT : (R*8)   ENOUT()  = TEMPERATURES (EV/AMU) FOR OUTPUT DATA SET
C  INPUT : (R*8)   SGTIN()  = TOTAL X-SECTIONS (CM2) IN INPUT DATA SET
C                             1ST.DIM: ENERGY INDEX
C  INPUT : (R*8)   SGNIN(,) = N-SHELL X-SECTIONS (CM2) IN INPUT DATA SET
C                             1ST.DIM: ENERGY INDEX
C                             2ND.DIM: PRINCIPAL QUANTUM NUMBER
C  INPUT : (R*8)   SGLIN(,) = L-SHELL X-SECTIONS (CM2) IN INPUT DATA SET
C                             1ST.DIM: ENERGY INDEX
C                             2ND.DIM: NL REFERENCE INDEX
C  INPUT : (R*8)   SGMIN(,) = M-SHELL X-SECTIONS (CM2) IN INPUT DATA SET
C                             1ST.DIM: ENERGY INDEX
C                             2ND.DIM: NLM REFERENCE INDEX
C  OUTPUT: (R*8)   RCTOUT() = TOTAL RATE COEFFT. (CM3 S-1) FOR OUTPUT
C                             1ST.DIM: TEMPERATURE INDEX
C  OUTPUT: (R*8)   RCNOUT(,)= N-SHELL RATE COEFFT. (CM3 S-1) FOR OUTPUT
C                             1ST.DIM: TEMPERATURE INDEX
C                             2ND.DIM: PRINCIPAL QUANTUM NUMBER
C  OUTPUT: (R*8)   RCLOUT(,)= L-SHELL RATE COEFFT. (CM3 S-1) FOR OUTPUT
C                             1ST.DIM: TEMPERATURE INDEX
C                             2ND.DIM: NL REFERENCE INDEX
C  OUTPUT: (R*8)   RCMOUT(,)= M-SHELL RATE COEFFT. (CM3 S-1) FOR OUTPUT
C                             1ST.DIM: TEMPERATURE INDEX
C                             2ND.DIM: NLM REFERENCE INDEX
C                  LSETX    = .TRUE. => SPLINE PRESET FOR THESE KNOTS
C                             .FLSE. => SPLINE NOT SET FOR THESE KNOTS
C                  LPASS    = .TRUE. => DO NOT CONVERT INTO LOG10 FOR
C                                       ENERGIES AND X-SECTS. FOR SPLINE
C                             .FLSE. => CONVERT INTO LOG10 FOR
C                                       ENERGIES AND X-SECTS. FOR SPLINE
C          (I*4)   PIPEOU   = STANDARD OUTPUT
C
C
C  ROUTINES:
C
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          CDEVTH     ADAS      EVALUATES THERMAL AVERAGE RATE COEFFTS.
C          I4FCTN     ADAS      RETURNS CHARACTER STRING AS AN INTEGER.
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          I4IDFL     ADAS      RETURNS UNIQUE INDEX FROM QUANTUM
C                               NUMBERS N AND L.
C          I4IDFM     ADAS      RETURNS UNIQUE INDEX FROM QUANTUM
C                               NUMBERS N, L AND M.
C
C
C  AUTHOR:  H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C           JA8.08
C           TEL. 0141-553-4196
C  DATE:    10/10/95
C  UPDATE:  27/08/97  HP SUMMERS - CHANGED NAME FROM CCTHER TO CDTHER
C
C VERSION:	1.1					DATE: 01-12-98
C MODIFIED: RICHARD MARTIN
C		- PUT UNDER SCCS CONTROL
C
C VERSION:      1.2                                     DATE: 24-03-99
C MODIFIED: MARTIN O'MULLANE
C               - SECOND VERSION
C
C VERSION  : 1.3
C DATE     : 22-05-2007
C MODIFIED : Martin O'Mullane
C              - Remove unused m-subshell data possibility.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER     PIPEOU        , ONE
C-----------------------------------------------------------------------
      PARAMETER ( PIPEOU = 6    , ONE = 1)
C-----------------------------------------------------------------------
      INTEGER     NDENR         , NDSHL         ,
     &            ILTYP         , IEXTYP        , NMIN        , NMAX
      INTEGER     NENIN         , NENOUT
      INTEGER     I4FCTN        , I4UNIT        , I4IDFL      ,
     &            I4IDFM
      INTEGER     N             , L             , M           , IREF
C-----------------------------------------------------------------------
      REAL*8      AMDON         , AMREC         , DREN
C-----------------------------------------------------------------------
      REAL*8      ENIN(NDENR)   , SGTIN(NDENR)  ,
     &            ENOUT(NDENR)  , RCTOUT(NDENR)
      REAL*8      SGNIN(NDENR,NDSHL)            ,
     &            SGLIN(NDENR,(NDSHL*(NDSHL+1))/2)
      REAL*8      RCNOUT(NDENR,NDSHL)           ,
     &            RCLOUT(NDENR,(NDSHL*(NDSHL+1))/2)
C-----------------------------------------------------------------------
      CHARACTER   CATYP*2
C-----------------------------------------------------------------------
      LOGICAL     LSETL       , LSETM        , LSETX       , LPASS
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C  SET CONDITIONS FOR SPLINE OPERATION IN SUBROUTINE CDEVTH INITIALLY
C-----------------------------------------------------------------------
        LSETX = .TRUE.
        LPASS = .FALSE.
C-----------------------------------------------------------------------
C  COMPUTE FOR TOTAL, N-SHELL, NL-SHELL AND NLM-SHELL COEFFTS.IN TURN
C-----------------------------------------------------------------------
        CALL CEEVTH  ( NDENR  ,
     &                 LSETX  , LPASS  ,
     &                 AMDON  , AMREC  , CATYP  , DREN   , 
     &                 ILTYP  , IEXTYP ,
     &                 NENIN  , ENIN   , NENOUT , ENOUT  ,
     &                 SGTIN  , RCTOUT
     &               )
     
        WRITE(PIPEOU,*)ONE
        CALL XXFLSH(PIPEOU)
        
        DO 90 N = NMIN,NMAX
        
          CALL CEEVTH  ( NDENR  ,
     &                   LSETX  , LPASS  ,
     &                   AMDON  , AMREC  , CATYP  , DREN   , 
     &                   ILTYP  , IEXTYP ,
     &                   NENIN  , ENIN   , NENOUT , ENOUT  ,
     &                   SGNIN(1,N)      ,
     &                   RCNOUT(1,N)
     &                 )
     
          WRITE(PIPEOU,*)ONE
          CALL XXFLSH(PIPEOU)
          IF(LSETL) THEN
              DO 80 L = 0,N-1
                IREF = I4IDFL(N,L)
                CALL CEEVTH  ( NDENR  ,
     &                         LSETX  , LPASS  ,
     &                         AMDON  , AMREC  , CATYP  , DREN   ,
     &                         ILTYP  , IEXTYP ,
     &                         NENIN  , ENIN   , NENOUT , ENOUT  ,
     &                         SGLIN(1,IREF)   ,
     &                         RCLOUT(1,IREF)
     &                       )
                WRITE(PIPEOU,*)ONE
                CALL XXFLSH(PIPEOU)
   80        CONTINUE
          ENDIF
   90   CONTINUE
C-----------------------------------------------------------------------
       RETURN
       END
