CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas314/adas314.for,v 1.3 2007/05/24 15:27:33 mog Exp $ Date $Date: 2007/05/24 15:27:33 $
CX
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 PROGRAM: ADAS314 **********************
C
C  VERSION:  1.0  
C
C  PURPOSE:  TO CONVERT RESOLVED CHARGE EXCHANGE CROSS-SECTION FILES
C            OF TYPE ADF01 TO FILES OF THE SAME TYPE BUT WITH MODIFIED
C            CROSS-SECTIONS OF THERMAL/THERMAL, THERMAL/BEAM OR
C            BEAM/THERMAL NATURE. THE PROGRAM CAN ALSO REGRID NORMAL
C            ADF01 FILES ONTO A NEW ENERGY SET.
C            THE OUTPUT DATA SET IS STRUCTURED AS ADF01.  THE MEANING
C            OF THE CROSS-SECTIONS MUST BE UNDERSTOOD FOR APPLICATION.
C            THEY ARE REQUIRED TO GIVE CORRECT RATE COEFFICIENTS WHEN
C            USED WITH ADAS SERIES PROGRAMS.  LET  EAMU BE THE ENTRY
C            IN THE 'KEV/AMU' ROW. AND 'SIGMA' BE THE VALUE IN A CROSS-
C            SECTION ROW, THEN ???*DSQRT(EAMU)*SIGMA GIVES THE RATE
C            COEFFICIENT IN CM3 S-1.
C
C  DATA:     THE SOURCE DATA IS adf01 DATASETS
C            THE UNITS USED IN THE DATA FILE ARE TAKEN AS FOLLOWS:
C               ENERGIES            : KEV / AMU
C               CROSS SECTIONS      : CM**2
C
C  PROGRAM:
C          (I*4)  NENRGY  = PARAMETER = NO. OF TABULATED INPUT ENERGIES.
C          (I*4)  NSHELL  = PARAMETER = NUMBER  OF  TABULATED  PRINCIPAL
C                                       QUANTUM N-SHELLS.
C          (I*4)  NDEIN   = PARAMETER = MAX. NO. OF USER ENTRED ENERGIES
C          (I*4)  IUNT07  = PARAMETER = OUTPUT UNIT FOR RESULTS
C          (I*4)  IUNT10  = PARAMETER = INPUT UNIT FOR DATA
C          (I*4)  L3      = PARAMETER = 3
C          (I*4)  I4UNIT  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (I*4)  IFORM   = ENERGY/VELOCITY FORM INDEX
C          (I*4)  IEVAL   = NO. OF USER ENTERED ENERGY/VELOCITY VALUES
C          (I*4)  IETYP   = 1 => INPUT DONOR TEMPERATURE IN EV
C                         = 2 => INPUT RECVR TEMPERATURE IN EV
C                         = 3 => INPUT ENERGIES   IN EV/AMU
C          (I*4)  IEXTYP  = 1 => SET LOWER ENERGIES TO FIRST POINT IN DATA 
C                         = 2 => SET LOWER ENERGIES TO 0.0
C          (I*4)  ILTYP   = TYPE FOR LOW ENERGY CROSS-SECTION EXTRAPOL
C                           REDUNDANT - SUPERCEEDED BY IEXTYP
C          (I*4)  IE      = ENERGY INDEX
C          (R*8)  R8ECON  = FUNCTION (SEE ROUTINE SECTION BELOW)
C          (L*4)  OPEN10  = .TRUE.  => FILE ALLOCATED TO UNIT 10.
C                         = .FALSE. => NO FILE ALLOCATED TO UNIT 10.
C          (L*4)  OPEN11  = .TRUE.  => FILE ALLOCATED TO UNIT 11.
C                         = .FALSE. => NO FILE ALLOCATED TO UNIT 11.
C          (L*4)  OPEN12  = .TRUE.  => FILE ALLOCATED TO UNIT 12.
C                         = .FALSE. => NO FILE ALLOCATED TO UNIT 12.
C          (L*4)  LQCX    = .TRUE.  => OUTPUT DATA TO QCX PASSING FILE.
C                           .FALSE. => NO OUTPUT OF CURRENT DATA TO
C                                      QCX PASSING FILE.
C          (L*4)  LSCX    = .TRUE.  => OUTPUT DATA TO SCX PASSING FILE.
C                           .FALSE. => NO OUTPUT OF CURRENT DATA TO
C                                      SCX PASSING FILE.
C          (L*4)  LPASS   = .TRUE.  => OUTPUT DATA TO SCX PASSING FILE.
C                           .FALSE. => NO OUTPUT OF CURRENT DATA TO
C                                      SCX PASSING FILE.
C          (L*4)  LPEND   = .TRUE.  => END ANALYSIS OF CURRENT DATA SET
C                         = .FALSE. => CONTINUE ANALYSIS WITH CURRENT
C                                      DATA SET.
C          (C*3)  REP     = 'YES' => TERMINATE PROGRAM EXECUTION
C                           'NO'  => CONTINUE PROGRAM EXECUTION
C          (C*8)  DATE    = CURRENT DATE (AS 'DD/MM/YY')
C          (C*44) DSFULL  = FULL INPUT DATA SET NAME (READ FROM IDL)
C          (C*40) TITLE   = TITLE OF RUN (READ FROM ISPF PANEL)
C          (I*4)  NENER   = NUMBER OF ENERGIES IN 'ENERA()' 
C          (I*4)  IZR     = ION CHARGE OF RECEIVER.
C          (I*4)  IZD     = ION CHARGE OF DONOR.
C          (I*4)  INDD    = DONOR STATE INDEX.
C          (I*4)  NDATA() = INPUT DATA FILE: VALID RANGE FOR N QUANTUM
C                                            NO. IN PARTIAL CROSS-SEC.
C                           1st DIMENSION: 1 => LOWER INDEX BOUND
C                                          2 => UPPER INDEX BOUND
C          (I*4)  IEDATA()= VALID RANGE FOR ENERGY INDEX
C                           1st DIMENSION: 1 => LOWER INDEX BOUND
C                                          2 => UPPER INDEX BOUND
C          (I*4)  LFORMA()=  PARAMETERS FOR CALCULATING L-RES X-SEC.
C                            1ST DIMENSION: ENERGY INDEX
C          (R*8)  SIGA()  = VALID CROSS SECTIONS FROM INPUT FILE FOR
C                           PRINCIPAL QUANTUM NUMBER 'INSEL' AND DATA
C                           SUB-BLOCK 'IBSEL'. (UNITS: CM**2)
C                           DIMENSION: VALID ENERGY INDEX.
C          (R*8)  EIN()   = USER ENTERED ELECTRON VELOCITY/ENERGIES
C                           (NOTE: UNITS ARE GIVEN BY 'IETYP')
C          (R*8)  EOA()   = USER ENTERED ELECTRON ENERGIES
C                           (UNITS: EV/AMU)
C          (R*8)  ENERA() = INPUT DATA FILE: TABULATED ENERGIES (EV)
C                           DIMENSION: ENERGY INDEX
C          (R*8)  ALPHAA()= INPUT DATA FILE: EXTRAPOLATION PARAMETERS
C                           DIMENSION: ENERGY INDEX
C          (R*8)  XLCUTA()= PARAMETERS FOR CALCULATING L-RES X-SEC.
C                           DIMENSION: ENERGY INDEX
C          (R*8)  PL2A()  = PARAMETERS FOR CALCULATING L-RES X-SEC.
C                           DIMENSION: ENERGY INDEX
C          (R*8)  PL3A()  = PARAMETERS FOR CALCULATING L-RES X-SEC.
C                           DIMENSION: ENERGY INDEX
C          (R*8)  XTOT()  = INPUT DATA FILE: TOTAL CROSS-SECTIONS(CM**2)
C                           DIMENSION: ENERGY INDEX
C          (R*8)  XSIGN(,)=INPUT DATA FILE: N-RESOLVED CROSS-SECTIONS.
C                           1st DIMENSION: ENERGY INDEX
C                           2nd DIMENSION: QUANTUM N-SHELL INDEX
C          (R*8)  XSIGL(,)=INPUT DATA FILE: L-RESOLVED CROSS-SECTIONS.
C                           1st DIMENSION: ENERGY INDEX
C                           2ND DIMENSION: INDEXED BY FUNCTION I4IDFL.
C          (R*8)  XSIGM(,)=INPUT DATA FILE: M-RESOLVED CROSS-SECTIONS.
C                           1st DIMENSION: ENERGY INDEX
C                           2ND DIMENSION: INDEXED BY FUNCTION I4IDFM.
C          (R*8)  SGTOUT() = TOTAL X-SECTS. (CM2) IN OUTPUT DATA SET
C                            1ST.DIM: ENERGY INDEX
C          (R*8)  SGNOUT(,)= N-SHELL X-SECTS. (CM2) IN OUTPUT DATA SET
C                            1ST.DIM: ENERGY INDEX
C                             2ND.DIM: PRINCIPAL QUANTUM NUMBER
C          (R*8)  SGLOUT(,)= L-SHELL X-SECTS. (CM2) IN OUTPUT DATA SET
C                            1ST.DIM: ENERGY INDEX
C                            2ND.DIM: NL REFERENCE INDEX
C          (R*8)  SGMOUT(,)= M-SHELL X-SECTS. (CM2) IN OUTPUT DATA SET
C                            1ST.DIM: ENERGY INDEX
C                            2ND.DIM: NLM REFERENCE INDEX
C          (L*4)  LPARMS()= FLAGS IF L-SPLITTING PARAMETERS PRESENT.
C                           .TRUE.  => L-SPLITTING PARAMETERS PRESENT.
C                           .FALSE  => L-SPLITTING PARAMETERS ABSENT.
C          (L*4)  LSETL() = .TRUE.  => L-RESOLVED DATA READ
C                           .FALSE. => NO L-RESOLVED DATA READ
C          (L*4)  LSETM() = .TRUE.  => M-RESOLVED DATA READ
C                           .FALSE. => NO M-RESOLVED DATA READ
C          (C*80) DSNPAP  = OUTPUT TEXT FILE
C          (C*80) DSNQCX  = OUTPUT PASSING FILE NAME - TYPE ADF01 
C          (C*80) DSNSCX  = OUTPUT PASSING FILE NAME - TYPE ADF24
C          (C*80) DSN80   = 80 BYTE CHARACTER STRING  FOR  TEMPORARY
C                           STORAGE OF DATA SET NAMES FOR DYNAMIC ALLOC.
C          (C*80) TITLF() = INPUT DATA FILE: SUB-BLOCK HEADINGS
C                           DIMENSION: DATA SUB-BLOCK INDEX.
C          (C*2)  SYMBR() = INPUT DATA FILE: RECEIVER ION ELEMENT SYMBOL
C                           DIMENSION: DATA SUB-BLOCK INDEX.
C          (C*2)  SYMBD() = INPUT DATA FILE: DONOR ION ELEMENT SYMBOL
C                           DIMENSION: DATA SUB-BLOCK INDEX.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          CESPF0     ADAS	GATHERS INPUT FILE NAMES VIA IDL
C          CEISPF     ADAS	GATHERS USERS VALUES VIA IDL 
C          CEFILL     ADAS	FILL BLANK EXTRAPOLATION REGIONS OF DATA
C          CETHER     ADAS	PERFORM DOUBLE MAXWELL AVERAGES
C          CEEVTH     ADAS	CALCULATES RATE COEFFICIENTS
C          CEECON     ADAS	CONVERT ENERGIES TO VARIOUS UNITS
C          CEWR11     ADAS	WRITE ADF01 (QCX) DATA TO STREAM 11
C          CEWR12     ADAS	WRITE ADF24 (SCX) DATA TO STREAM 12
C          CEPARM     ADAS	CONVERT ALPHAS ETC TO OUTPUT ENERGIES
C          CXDATA     ADAS      READS INPUT DATA SET IN ADF01 FORMAT.
C          XX0000     ADAS      SET MACHINE DEPENDANT ADAS CONFIGURATION
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C
C
C AUTHOR:  H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C          JA8.08
C          TEL. 0141-553-4196
C DATE:    18/09/95
C
C MODIFIED: Martin O'Mullane
C DATE:    9-07-98
C VERSION: 1.0 - ported to IDL 
C
C VERSION:	1.1					DATE: 01-12-98
C MODIFIED: RICHARD MARTIN
C		- PUT UNDER SCCS CONTROL
C
C VERSION:	1.2					DATE: 17-02-99
C MODIFIED: Martin O'Mullane
C               - increased max number of energies to 40 (from 30)
C               - introduced new extrapolation at low energies methods
C
C VERSION  : 1.3
C DATE     : 22-05-2007
C MODIFIED : Martin O'Mullane
C              - Remove unused m-subshell data possibility.
C
C-----------------------------------------------------------------------
      INTEGER    NENRGY       , NSHELL       , NDEIN
      INTEGER    IUNT17       , IUNT10       , IUNT11     , IUNT12  ,
     &           PIPEIN       , PIPEOU       , ONE       
      INTEGER    NPSPL
      INTEGER    L1           , L2           , L3
C-----------------------------------------------------------------------
      PARAMETER( NENRGY =  40 , NSHELL = 20 , NDEIN  = 40)
      PARAMETER( IUNT17 =  17 , IUNT10 = 10 , IUNT11 = 11 , IUNT12 = 12)
      PARAMETER( PIPEIN = 5   , PIPEOU = 6  , ONE    = 1  )
      PARAMETER( NPSPL  = 100 )
      PARAMETER( L1     = 1   , L2     = 2  , L3     =  3 )
C-----------------------------------------------------------------------
      INTEGER    I4UNIT       , ISTOP
      INTEGER    ILTYP        , IEVAL        , IETYP     , IEXTYP       
      INTEGER    NENER        , IZR          ,
     &           IZD          , INDD
      INTEGER    NUM          , N            , L          , M
C-----------------------------------------------------------------------
      REAL*8     AMDON        , AMREC       , DREN
C-----------------------------------------------------------------------
      LOGICAL    OPEN10       , OPEN11      , OPEN12     , OPEN17 
      LOGICAL    LPAPER       , LQCX        , LSCX
      LOGICAL    LPEND
      LOGICAL    LPARMS       , LSETL       , LSETM
C-----------------------------------------------------------------------
      CHARACTER  DSNIN*80       
      CHARACTER  DSNPAP*80    , DSNQCX*80   , DSNSCX*80 , DSN80*80
      CHARACTER  DATE*8       , REP*3       , CADAS*120
      CHARACTER  CATYP*2      , TITLE*40
      CHARACTER  TITLF*80     , SYMBR*2     , SYMBD*2
C-----------------------------------------------------------------------
      INTEGER    NDATA(2)     , LFORMA(NENRGY)
      INTEGER    LFMOUT(NENRGY)
C-----------------------------------------------------------------------
      REAL*8     EIN(NDEIN)            , EOA(NDEIN)                   
      REAL*8     XTOT(NENRGY)          , ALPHAA(NENRGY)         ,
     &           XLCUTA(NENRGY)        , PL2A(NENRGY)           ,
     &           PL3A(NENRGY)          , ENERA(NENRGY)
      REAL*8     ALFOUT(NENRGY)        , XLCOUT(NENRGY)         ,
     &           PL2OUT(NENRGY)        , PL3OUT(NENRGY)
      REAL*8     XSIGN(NENRGY,NSHELL)                           ,
     &           XSIGL(NENRGY,(NSHELL*(NSHELL+1))/2)
      REAL*8     SGTOUT(NENRGY)
      REAL*8     SGNOUT(NENRGY,NSHELL)                          ,
     &           SGLOUT(NENRGY,(NSHELL*(NSHELL+1))/2)
C-----------------------------------------------------------------------
      DATA IEVAL  /0/
      DATA OPEN10 /.FALSE./ , OPEN11/.FALSE./ , OPEN12/.FALSE./ ,
     &     OPEN17 /.FALSE./
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
C *************************** MAIN PROGRAM ****************************
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
C SET MACHINE DEPENDANT ADAS CONFIGURATION VALUES AND GET DATE
C-----------------------------------------------------------------------
      CALL XX0000
      CALL XXDATE( DATE )

  100 CONTINUE

C-----------------------------------------------------------------------
C IF FILE IS ACTIVE ON UNIT 10 - CLOSE THE UNIT
C-----------------------------------------------------------------------
      IF (OPEN10) THEN
         CLOSE(10)
         OPEN10=.FALSE.
      ENDIF
      
C-----------------------------------------------------------------------
C GET INPUT DATA SET NAME (FROM ISPF PANELS)
C-----------------------------------------------------------------------
      CALL CESPF0( REP , DSNIN )
      
C-----------------------------------------------------------------------
C IF PROGRAM RUN IS COMPLETED THEN OFF TO THE END
C-----------------------------------------------------------------------
      IF (REP.EQ.'YES') THEN
         GOTO 9999
      ENDIF
         
C-----------------------------------------------------------------------
C OPEN INPUT DATA FILE - DSFULL
C-----------------------------------------------------------------------
      OPEN( UNIT=IUNT10 , FILE=DSNIN , STATUS='OLD' )
      OPEN10=.TRUE.
      
C-----------------------------------------------------------------------
C FETCH DATA FROM SELECTED DATASET
C-----------------------------------------------------------------------
     
      call xxdata_01( iunt10     , nenrgy     , nshell     ,
     &                symbr      , symbd      , izr        , izd      ,
     &                indd       , nener      , ndata(1)   , ndata(2) ,
     &                lparms     , lsetl      , enera      ,
     &                alphaa     , lforma     , xlcuta     , pl2a     ,
     &                pl3a       , xtot       , xsign      , xsigl
     &              )

C-----------------------------------------------------------------------
C GET USER-DEFINED VALUES FROM IDL
C-----------------------------------------------------------------------
  200  CONTINUE
  
       CALL CEISPF( LPEND  ,
     &              NENER  , NDEIN  , ENERA  , 
     &              TITLE  ,
     &              AMDON  , AMREC  ,
     &              CATYP  , DREN   ,      
     &              IEXTYP , IETYP  , IEVAL  , EIN )
     
     
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
      READ(PIPEIN,*) ISTOP
      IF (ISTOP.EQ.1) GOTO 9999
      
C-----------------------------------------------------------------------
C IF END OF CURRENT FILE REQUESTED GO BACK TO INPUT FILE PANEL.
C-----------------------------------------------------------------------
      IF (LPEND) GOTO 100
     
C-----------------------------------------------------------------------
C ANALYSE ENTERED DATA  
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
C REPLACE ZEROES WITH EXTRAPOLATED VALUES AT HIGH N IF NECESSARY
C-----------------------------------------------------------------------
      CALL CEFILL( NENRGY  , NSHELL  ,
     &             SYMBR   , SYMBD   , IZR      , IZD      ,
     &             INDD    , NENER   , NDATA(1) , NDATA(2) ,
     &             LPARMS  , LSETL   , ENERA    ,
     &             ALPHAA  , LFORMA  , XLCUTA   , PL2A     ,
     &             PL3A    ,
     &             XTOT    , XSIGN   ,
     &             XSIGL
     &           )
     
C-----------------------------------------------------------------------
C CONVERT INPUT ENERGIES/VELOCITIES INTO EV/AMU (EIN -> EOA)
C-----------------------------------------------------------------------
      
      IF(CATYP.EQ.'TT') then
          CALL CEECON( IETYP , L1 , IEVAL , EIN , AMDON , AMREC , EOA )
      ELSEIF(CATYP.EQ.'TD')THEN
          CALL CEECON( IETYP , L1 , IEVAL , EIN , AMDON , AMREC , EOA )
      ELSEIF(CATYP.EQ.'TR')THEN
          CALL CEECON( IETYP , L2 , IEVAL , EIN , AMDON , AMREC , EOA )
      ELSEIF(CATYP.EQ.'ME')THEN
          CALL CEECON( IETYP , L3 , IEVAL , EIN , AMDON , AMREC , EOA )
      ENDIF
C-----------------------------------------------------------------------
C  GET OUTPUT FILES FROM IDL
C-----------------------------------------------------------------------
  300   CONTINUE     
        CALL CESPF1( LQCX   , LSCX   , LPAPER ,
     &               DSNQCX , DSNSCX , DSNPAP , 
     &               LPEND  )
       
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
      READ(PIPEIN,*) ISTOP
      IF (ISTOP.EQ.1) GOTO 9999
      
C-----------------------------------------------------------------------
C IF USER HAS SELECTED 'CANCEL' RETURN TO PROCESSING OPTIONS WINDOW.
C-----------------------------------------------------------------------
      IF (LPEND) GOTO 200
      
C-----------------------------------------------------------------------
C TELL THE IDL HOW MUCH COMPUTING THERE IS TO DO
C-----------------------------------------------------------------------

      NUM = 0
      DO N = NDATA(1), NDATA(2)
        IF (LSETL) THEN
           DO L = 0,N-1
              NUM = NUM + 1
           END DO
         ENDIF
         NUM = NUM + 1
      END DO  
      NUM = NUM + 1
       
      WRITE(PIPEOU,*)NUM
      CALL XXFLSH(PIPEOU)
      
C-----------------------------------------------------------------------
C OPEN OUTPUT FILES PAPER.TXT AND PASSING FILES - IF REQUESTED
C-----------------------------------------------------------------------

      IF ((.NOT.OPEN17) .AND. LPAPER) THEN
         DSN80=' '
         DSN80=DSNPAP
         OPEN(UNIT=IUNT17,FILE=DSN80, STATUS='UNKNOWN')
         OPEN17=.TRUE.
      ENDIF
      IF ((.NOT.OPEN11) .AND. LQCX) THEN
         DSN80=' '
         DSN80=DSNQCX
         OPEN(UNIT=IUNT11,FILE=DSN80, STATUS='UNKNOWN')
         OPEN11=.TRUE.
      ENDIF
      IF ((.NOT.OPEN12) .AND. LSCX) THEN
         DSN80=' '
         DSN80=DSNSCX
         OPEN(UNIT=IUNT12,FILE=DSN80, STATUS='UNKNOWN')
         OPEN12=.TRUE.
      ENDIF
      
C-----------------------------------------------------------------------
C Write something to paper.txt
C-----------------------------------------------------------------------
      IF (OPEN17) THEN
      
C         WRITE(IUNT17,2000) CADAS(2:80)
         WRITE(IUNT17,2001) 'ADF01 REINTERPRETATION','ADAS314',DATE
         WRITE(IUNT17,2002) TITLE
         WRITE(IUNT17,2003) DSNQCX,DSNSCX
         
      ENDIF
      
C-----------------------------------------------------------------------
C  ALL THE WORK - CALCULATE REQUIRED MAXWELL AVERAGES
C-----------------------------------------------------------------------
      CALL CETHER ( NENRGY   , NSHELL   ,
     &              AMDON    , AMREC    , CATYP    , DREN     ,
     &              ILTYP    , IEXTYP   ,
     &              LSETL    , NDATA(1) , NDATA(2) ,
     &              NENER    , ENERA    , IEVAL    , EOA      ,
     &              XTOT     , XSIGN    , XSIGL    ,
     &              SGTOUT   , SGNOUT   , SGLOUT
     &            )
     
C-----------------------------------------------------------------------
C  CONVERT ALPHAS ETC TO OUTPUT ENERGIES
C-----------------------------------------------------------------------
       CALL CEPARM ( NENRGY  ,
     &               LPARMS  ,
     &               NENER   , ENERA  , IEVAL  , EOA    ,
     &               ALPHAA  , XLCUTA , PL2A   , PL3A   ,
     &               LFORMA  ,
     &               ALFOUT  , XLCOUT , PL2OUT , PL3OUT ,
     &               LFMOUT
     &             )
     
C-----------------------------------------------------------------------
C OUTPUT DATA TO PASSING QCX FILE 
C-----------------------------------------------------------------------
      IF (LQCX) THEN
          CALL CEWR11( DSNIN     , DATE       ,
     &                 IUNT11    , NENRGY     , NSHELL   ,
     &                 CATYP     , AMDON      , AMREC    , DREN     ,
     &                 SYMBR     , SYMBD      , IZR      , IZD      ,
     &                 INDD      , IEVAL      , NDATA(1) , NDATA(2) ,
     &                 LPARMS    , LSETL      , EOA      ,
     &                 ALFOUT    , LFMOUT     , XLCOUT   , PL2OUT   ,
     &                 PL3OUT    ,
     &                 SGTOUT    , SGNOUT     , SGLOUT    
     &                )
      ENDIF
          
C-----------------------------------------------------------------------
C OUTPUT DATA TO PASSING SCX FILE 
C-----------------------------------------------------------------------
      IF (LSCX) THEN
      
         CALL CEWR12( IUNT12     , NENRGY     , NSHELL   , ILTYP    ,
     &                DSNIN      , DATE     ,
     &                CATYP      ,
     &                SYMBR      , SYMBD      , IZR      , IZD      ,
     &                INDD       , IEVAL      , NDATA(1) , NDATA(2) ,
     &                LPARMS     , LSETL      , LSETM    , EOA      ,
     &                ALFOUT     , LFMOUT     , XLCOUT   , PL2OUT   ,
     &                PL3OUT     ,
     &                SGTOUT     , SGNOUT     , SGLOUT     
     &               )
     
       ENDIF
       
C-----------------------------------------------------------------------
C TELL IDL THAT WE HAVE FINISHED
C-----------------------------------------------------------------------
       WRITE(PIPEOU,*)ONE
       CALL XXFLSH(PIPEOU)
       
C-----------------------------------------------------------------------
C Close FILES -- do not allow appending
C-----------------------------------------------------------------------
      IF (OPEN17) THEN
         OPEN17 = .FALSE.
         CLOSE(IUNT17)
      ENDIF
      IF (OPEN11) THEN
         OPEN11 = .FALSE.
         CLOSE(IUNT11)
      ENDIF
      IF (OPEN12) THEN
         OPEN12 = .FALSE.
         CLOSE(IUNT12)
      ENDIF
      
C-----------------------------------------------------------------------
C  RETURN TO OUTPUT PANELS. (ON RETURN UNITS ARE CLOSED)
C-----------------------------------------------------------------------
      GOTO 300
      
C-----------------------------------------------------------------------
 9999 CONTINUE
 
      CLOSE( IUNT10 )
      CLOSE( IUNT17 )
      CLOSE( IUNT11 )
      CLOSE( IUNT12 )
      
C-----------------------------------------------------------------------
 2000 FORMAT(A79)
 2001 FORMAT(1x,24('*'),' TABULAR OUTPUT FROM ',A28,' PROGRAM: ',
     & A7,1X,'- DATE: ',A8,1X,24('*')/)
 2002 FORMAT(/ ,19('-'),1X,A40,1X,19('-')//)
 2003 FORMAT(1x,'QCX file    : ',A44/
     &       1X,'SCX file    : ',A44/)
C-----------------------------------------------------------------------
      END
