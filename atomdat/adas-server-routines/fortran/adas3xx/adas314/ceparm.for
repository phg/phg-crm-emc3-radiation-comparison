CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas314/ceparm.for,v 1.2 2007/05/17 17:08:17 allan Exp $ Date $Date: 2007/05/17 17:08:17 $
CX
       SUBROUTINE CEPARM ( NDENR  ,
     &                     LPARMS ,
     &                     NENIN  , ENIN   , NENOUT , ENOUT  ,
     &                     ALFIN  , XLCIN  , PL2IN  , PL3IN  , LFMIN  ,
     &                     ALFOUT , XLCOUT , PL2OUT , PL3OUT , LFMOUT
     &                   )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C  ********************** FORTRAN77 SUBROUTINE: CEPARM *****************
C  VERSION: 1.0  
C
C  PURPOSE:  CONVERTS ALPHA, PL2, PL3 AND LFORM CHARGE EXCHANGE
C            PARAMETER VALUES AT INPUT ENERGIES TO VALUES AT OUTPUT
C            ENERGIES
C
C  CALLING PROGRAM:  ADAS314
C  SUBROUTINE:
C  INPUT :  (I*4)  NDENR   = MAX. NUMBER OF ENERGIES
C                            ALLOWED IN CROSS-SECTION FILE
C                            OR TEMPERATURES IN THERMAL
C                            AVERAGED RATE COEFFT. OUTPUT FILE.
C  INPUT : (I*4)   LPARMS   = .TRUE.  => INPUT DATA HAS L-FIT PARAMETERS
C                             .FALSE. => INPUT DATA HAS L-FIT PARAMETERS
C  INPUT : (I*4)   NENIN    = NUMBER OF ENERGIES IN INPUT DATA SET
C  INPUT : (R*8)   ENIN()   = ENERGIES (EV/AMU) IN INPUT DATA SET
C  INPUT : (I*4)   NENOUT   = NUMBER OF ENERGIES FOR OUTPUT DATA SET
C  INPUT : (R*8)   ENOUT()  = TEMPERATURES (EV/AMU) FOR OUTPUT DATA SET
C  INPUT : (R*8)   ALFIN()  = ALPHA PARAMETER IN INPUT DATA SET
C                             1ST.DIM: ENERGY INDEX
C  INPUT : (R*8)   XLCIN()  = NON-INTEGER L-CUT-OFF PARAMETER
C                             1ST.DIM: ENERGY INDEX
C  INPUT : (R*8)   PL2IN(,) = P2 L-FIT PARAMETER IN INPUT DATA SET
C                             1ST.DIM: ENERGY INDEX
C  INPUT : (R*8)   PL3IN(,) = P3 L-FIT PARAMETER IN INPUT DATA SET
C                             1ST.DIM: ENERGY INDEX
C  INPUT : (I*4)   LFMIN(,) = L-FIT FORM TYPE INDEX IN INPUT DATA SET
C                             1ST.DIM: ENERGY INDEX
C  OUTPUT: (R*8)   ALFOUT() = ALPHA PARAMETER IN INPUT DATA SET
C                             1ST.DIM: ENERGY INDEX
C  OUTPUT: (R*8)   XLCOUT() = NON-INTEGER L-CUT-OFF PARAMETER
C                             1ST.DIM: ENERGY INDEX
C  OUTPUT: (R*8)   PL2OUT() = P2 L-FIT PARAMETER IN INPUT DATA SET
C                             1ST.DIM: ENERGY INDEX
C  OUTPUT: (R*8)   PL3OUT() = P3 L-FIT PARAMETER IN INPUT DATA SET
C                             1ST.DIM: ENERGY INDEX
C  OUTPUT: (I*4)   LFMOUT() = L-FIT FORM TYPE INDEX IN INPUT DATA SET
C                             1ST.DIM: ENERGY INDEX
C          (L*4)   LSETX    = .TRUE. => SPLINE PRESET FOR THESE KNOTS
C                             .FLSE. => SPLINE NOT SET FOR THESE KNOTS
C          (L*4)   LPASS    = .TRUE. => DO NOT CONVERT INTO LOG10 FOR
C                                       ENERGIES AND X-SECTS. FOR SPLINE
C                             .FLSE. => CONVERT INTO LOG10 FOR
C                                       ENERGIES AND X-SECTS. FOR SPLINE
C          (I*4)   IOPT     = SPLINE END POINT CURVATURE/GRADIENT OPTION
C                             1 => DDY1 = 0, DDYN = 0
C                             4 => DY1 = 0 , DDYN = 0
C          (R*8)   CMSAMU   = PARAMETER = CONVERSION FACTOR FOR ENERGY
C                                         (AMU) TO VELOCITY (CM S-1)
C          (I*4)   I        = GENERAL INDEX
C          (I*4)   IT       = GENERAL INDEX
C          (R*8)   XIN()    = INTERNAL SPLINE INDEPENDENT VARIABLE
C          (R*8)   YIN()    = INTERNAL SPLINE DEPENDENT VARIABLE
C          (R*8)   VIN()    = INTERNAL VECTOR
C          (R*8)   DY()     = DERIVATIVES AT SPLINE KNOTS
C          (R*8)   XOUT()   = INTERNAL OUTPUT INDEPENDENT VARIABLE
C          (R*8)   YOUT()   = INTERNAL OUTPUT DEPENDENT VARIABLE
C          (L*4)   LINTRP() = .TRUE.  => POINT INTERPOLATED
C                           = .FALSE. => POINT EXTRAPOLATED
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          XXSPLE     ADAS      INTERPOLATES USING CUBIC SPLINES
C          R8FUN1      ADAS      EXTERNAL FUNCTION FOR XXSPLE
C  AUTHOR:  H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C           JA8.08
C           TEL. 0141-553-4196
C  DATE:    13/11/95
C  UPDATE:  27/08/97  HP SUMMERS - CHANGED NAME FROM CCPARM TO CDPARM
C
C VERSION:	1.1					DATE: 01-12-98
C MODIFIED: RICHARD MARTIN
C		- PUT UNDER SCCS CONTROL
C
C VERSION:      1.2                                     DATE: 17-05-07
C MODIFIED: Allan Whiteford
C               - Updated comments as part of subroutine documentation
C                 procedure.
C
C-----------------------------------------------------------------------
C--------------------------------------------------------------------
      INTEGER  MAXENS
C--------------------------------------------------------------------
      REAL*8   CMSAMU   , ZERO
C--------------------------------------------------------------------
      PARAMETER ( MAXENS = 50 )
      PARAMETER ( CMSAMU = 1.389209D+06      , ZERO = 1.0D-30     )
C--------------------------------------------------------------------
      INTEGER     NDENR
      INTEGER     NENIN         , NENOUT
      INTEGER     IT            , I            , IOPT
C-----------------------------------------------------------------------
      REAL*8      R8FUN1
C-----------------------------------------------------------------------
      LOGICAL     LPARMS        , LSETX         , LPASS
C-----------------------------------------------------------------------
      INTEGER     LFMIN(NDENR)
      INTEGER     LFMOUT(NDENR)
C-----------------------------------------------------------------------
      REAL*8      ENIN(NDENR)   , XLCIN(NDENR)
      REAL*8      ENOUT(NDENR)  , XLCOUT(NDENR)
      REAL*8      ALFIN(NDENR)  , PL2IN(NDENR)  , PL3IN(NDENR)
      REAL*8      ALFOUT(NDENR) , PL2OUT(NDENR) , PL3OUT(NDENR)
      REAL*8      XOUT(MAXENS)  , YOUT(MAXENS)
      REAL*8      DY(MAXENS)
      REAL*8      XIN(MAXENS)   , YIN(MAXENS)
C--------------------------------------------------------------------
      LOGICAL     LINTRP(MAXENS)
C--------------------------------------------------------------------
      EXTERNAL    R8FUN1
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C  SET CONDITIONS FOR SPLINE OPERATION INITIALLY
C-----------------------------------------------------------------------
        LSETX = .TRUE.
        LPASS = .FALSE.
        IOPT  = 4
C-----------------------------------------------------------------------
C  INTERPOLATE ALPHA PARAMETER  ( LOG10 IN X,  LOG10 IN Y)
C-----------------------------------------------------------------------
      IF(.NOT.LPASS) THEN
          DO 10 I = 1, NENIN
            YIN(I) = DLOG10(ALFIN(I)+ZERO)
 10       CONTINUE
          IF(LSETX) THEN
              DO 15 I = 1,NENIN
                XIN(I) = DLOG10(CMSAMU*DSQRT(ENIN(I)))
 15           CONTINUE
          ENDIF
          DO 20 I = 1,NENOUT
            XOUT(I) = DLOG10(CMSAMU*DSQRT(ENOUT(I)))
 20       CONTINUE
      ELSE
          DO 30 I = 1,NENIN
            YIN(I) = ALFIN(I)+ZERO
 30       CONTINUE
          IF(LSETX) THEN
              DO 35 I = 1,NENIN
                XIN(I) = CMSAMU*DSQRT(ENIN(I))
 35           CONTINUE
          ENDIF
          DO 40 I = 1,NENOUT
            XOUT(I) = CMSAMU*DSQRT(ENOUT(I))
 40       CONTINUE
      ENDIF
      CALL XXSPLE ( LSETX   , IOPT   , R8FUN1   ,
     &              NENIN   , XIN    , YIN      ,
     &              NENOUT  , XOUT   , YOUT     ,
     &              DY      , LINTRP
     &             )
      DO 50 I = 1,NENOUT
        IF ( LINTRP(I).AND.(.NOT.LPASS) ) THEN
            ALFOUT(I)  = 10.0**YOUT(I)
        ELSEIF ( LINTRP(I).AND.LPASS )    THEN
            ALFOUT(I)  = YOUT(I)
        ELSEIF(.NOT.LINTRP(I)) THEN
            IF(XOUT(I).GT.XIN(NENIN)) THEN
                ALFOUT(I)  = ALFIN(NENIN)
            ELSEIF(XOUT(I).LT.XIN(1)) THEN
                ALFOUT(I)  = ALFIN(1)
            ENDIF
        ENDIF
   50 CONTINUE
      IF ( LPARMS ) THEN
C-----------------------------------------------------------------------
C  INTERPOLATE PL2 PARAMETER  ( LOG10 IN X,  LINEAR IN Y)
C-----------------------------------------------------------------------
          IF(.NOT.LPASS) THEN
              DO 60  I = 1,NENIN
                YIN(I) = PL2IN(I)+ZERO
 60           CONTINUE
              IF(LSETX) THEN
                  DO 65 I = 1,NENIN
                    XIN(I) = DLOG10(CMSAMU*DSQRT(ENIN(I)))
 65               CONTINUE
              ENDIF
              DO 70 I = 1,NENOUT
                XOUT(I) = DLOG10(CMSAMU*DSQRT(ENOUT(I)))
 70           CONTINUE
          ELSE
              DO 80 I = 1,NENIN
                YIN(I) = PL2IN(I)+ZERO
 80           CONTINUE
              IF(LSETX) THEN
                  DO 85 I = 1,NENIN
                    XIN(I) = CMSAMU*DSQRT(ENIN(I))
 85               CONTINUE
              ENDIF
              DO 90 I = 1,NENOUT
                XOUT(I) = CMSAMU*DSQRT(ENOUT(I))
 90           CONTINUE
          ENDIF
          CALL XXSPLE ( LSETX   , IOPT   , R8FUN1   ,
     &                  NENIN   , XIN    , YIN      ,
     &                  NENOUT  , XOUT   , YOUT     ,
     &                  DY      , LINTRP
     &                 )
          DO 100 I = 1,NENOUT
            IF ( LINTRP(I).AND.(.NOT.LPASS) ) THEN
                PL2OUT(I)  = YOUT(I)
            ELSEIF ( LINTRP(I).AND.LPASS )    THEN
                PL2OUT(I)  = YOUT(I)
            ELSEIF(.NOT.LINTRP(I)) THEN
                IF(XOUT(I).GT.XIN(NENIN)) THEN
                    PL2OUT(I)  = PL2IN(NENIN)
                ELSEIF(XOUT(I).LT.XIN(1)) THEN
                    PL2OUT(I)  = PL2IN(1)
                ENDIF
            ENDIF
  100     CONTINUE
C-----------------------------------------------------------------------
C  INTERPOLATE PL3 PARAMETER  ( LOG10 IN X,  LINEAR IN Y)
C-----------------------------------------------------------------------
          IF(.NOT.LPASS) THEN
              DO 110 I = 1, NENIN
                YIN(I) = PL3IN(I)+ZERO
  110         CONTINUE
              IF(LSETX) THEN
                  DO 115 I = 1,NENIN
                    XIN(I) = DLOG10(CMSAMU*DSQRT(ENIN(I)))
  115             CONTINUE
              ENDIF
              DO 120 I = 1,NENOUT
                XOUT(I) = DLOG10(CMSAMU*DSQRT(ENOUT(I)))
  120         CONTINUE
          ELSE
              DO 130 I = 1,NENIN
                YIN(I) = PL3IN(I)+ZERO
  130         CONTINUE
              IF(LSETX) THEN
                  DO 135 I = 1,NENIN
                    XIN(I) = CMSAMU*DSQRT(ENIN(I))
  135             CONTINUE
              ENDIF
              DO 140 I = 1,NENOUT
                XOUT(I) = CMSAMU*DSQRT(ENOUT(I))
  140         CONTINUE
          ENDIF
          CALL XXSPLE ( LSETX   , IOPT   , R8FUN1   ,
     &                  NENIN   , XIN    , YIN      ,
     &                  NENOUT  , XOUT   , YOUT     ,
     &                  DY      , LINTRP
     &                 )
          DO 150 I = 1,NENOUT
            IF ( LINTRP(I).AND.(.NOT.LPASS) ) THEN
                PL3OUT(I)  = YOUT(I)
            ELSEIF ( LINTRP(I).AND.LPASS )    THEN
                PL3OUT(I)  = YOUT(I)
            ELSEIF(.NOT.LINTRP(I)) THEN
                IF(XOUT(I).GT.XIN(NENIN)) THEN
                    PL3OUT(I)  = PL3IN(NENIN)
                ELSEIF(XOUT(I).LT.XIN(1)) THEN
                    PL3OUT(I)  = PL3IN(1)
                ENDIF
            ENDIF
  150     CONTINUE
C-----------------------------------------------------------------------
C  INTERPOLATE XLCUT PARAMETER  ( LOG10 IN X,  LOG10 IN Y)
C-----------------------------------------------------------------------
          IF(.NOT.LPASS) THEN
              DO 160 I = 1, NENIN
                YIN(I) = DLOG10(XLCIN(I)+ZERO)
  160         CONTINUE
              IF(LSETX) THEN
                  DO 165 I = 1,NENIN
                    XIN(I) = DLOG10(CMSAMU*DSQRT(ENIN(I)))
  165             CONTINUE
              ENDIF
              DO 170 I = 1,NENOUT
                XOUT(I) = DLOG10(CMSAMU*DSQRT(ENOUT(I)))
  170         CONTINUE
          ELSE
              DO 180 I = 1,NENIN
                YIN(I) = XLCIN(I)+ZERO
  180         CONTINUE
              IF(LSETX) THEN
                  DO 185 I = 1,NENIN
                    XIN(I) = CMSAMU*DSQRT(ENIN(I))
  185             CONTINUE
              ENDIF
              DO 190 I = 1,NENOUT
                XOUT(I) = CMSAMU*DSQRT(ENOUT(I))
  190         CONTINUE
          ENDIF
          CALL XXSPLE ( LSETX   , IOPT   , R8FUN1   ,
     &                  NENIN   , XIN    , YIN      ,
     &                  NENOUT  , XOUT   , YOUT     ,
     &                  DY      , LINTRP
     &                 )
          DO 200 I = 1,NENOUT
            IF ( LINTRP(I).AND.(.NOT.LPASS) ) THEN
                XLCOUT(I)  = 10.0**YOUT(I)
            ELSEIF ( LINTRP(I).AND.LPASS )    THEN
                XLCOUT(I)  = YOUT(I)
            ELSEIF(.NOT.LINTRP(I)) THEN
                IF(XOUT(I).GT.XIN(NENIN)) THEN
                    XLCOUT(I)  = XLCIN(NENIN)
                ELSEIF(XOUT(I).LT.XIN(1)) THEN
                    XLCOUT(I)  = XLCIN(1)
                ENDIF
            ENDIF
  200     CONTINUE
C-----------------------------------------------------------------------
C   SPECIFY THE L-FIT FORM
C-----------------------------------------------------------------------
          DO 250 I = 1 , NENOUT
            IF(ENOUT(I).LE.ENIN(1))THEN
                LFMOUT(I) = LFMIN(1)
            ELSEIF(ENOUT(I).GE.ENIN(NENIN))THEN
                LFMOUT(I) = LFMIN(NENIN)
            ELSE
                DO 240 IT = 2,NENIN
                    IF(ENOUT(I).LT.ENIN(IT))THEN
                        LFMOUT(I) = LFMIN(IT-1)
                        GO TO 250
                    ENDIF
  240           CONTINUE
            ENDIF
  250     CONTINUE
      ENDIF
C-----------------------------------------------------------------------
       RETURN
       END
