CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas314/ceecon.for,v 1.2 2007/05/17 17:08:17 allan Exp $ Date $Date: 2007/05/17 17:08:17 $
CX
      SUBROUTINE CEECON( INTYP , OUTTYP, IEVAL, EIN,
     &                   AMDON , AMREC , EOUT
     &                 )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C  ****************** FORTRAN77 SUBROUTINE: CEECON *********************
C
C  PURPOSE: TO CONVERT AN ARRAY OF ENERGIES INTO A  SPECIFIED FORM.
C
C  CALLING PROGRAM: ADAS314
C  SUBROUTINE:
C  INPUT :    (I*4)  INTYP   = 1 => 'EIN(array)' UNITS: eV (Donor temp.)
C                            = 2 => 'EIN(array)' UNITS: eV (Recvr temp.)
C                            = 3 => 'EIN(array)' UNITS: eV/AMU  (ENERGY)
C  INPUT :    (I*4)  OUTTYP  = 1 => 'EOUT(array)' UNITS: eV (Donor temp)
C                            = 2 => 'EOUT(array)' UNITS: eV (Recvr temp)
C                            = 3 => 'EOUT(array)' UNITS: EV/AMU (ENERGY)
C  INPUT :    (I*4)  IEVAL   = NO. OF ENERGIES IN EIN(array)
C  INPUT :    (R*8)  EIN()   = INPUT  ENERGIES (STATED UNITS)
C  INPUT :    (R*8)  AMDON   = DONOR MASS NUMBER
C  INPUT :    (R*8)  AMREC   = RECEIVER MASS NUMBER
C  OUTPUT:    (R*8)  EOUT()  = OUTPUT ENERGIES (STATED UNITS)
C             (I*4)  I       = GENERAL USE
C             (R*8)  ECONV() = ENERGY CONVERSION PARAMETERS
C ROUTINES:  NONE
C NOTE:
C            ENERGY CONVERSION PARAMETERS:
C            INTYP = 1 ; ECONV(1) =>  ENERGY  : EV       -> OUTPUT FORM
C            INTYP = 2 ; ECONV(2) =>  ENERGY  : EV       -> OUTPUT FORM
C            INTYP = 3 ; ECONV(3) =>  ENERGY  : EV/AMU   -> OUTPUT FORM
C AUTHOR:  H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C          JA8.08
C          TEL. 0141-553-4196
C DATE:    19/09/95
C UPDATE:  27/08/97  HP SUMMERS - CHANGED NAME FROM CCECON TO CDECON
C MODIFIED: Martin O'Mullane
C DATE:    9-07-98
C VERSION: 1.0 - ported to IDL
C
C VERSION:	1.1					DATE: 01-12-98
C MODIFIED: RICHARD MARTIN
C		- PUT UNDER SCCS CONTROL
C
C VERSION:      1.2                                     DATE: 17-05-07
C MODIFIED: Allan Whiteford
C               - Updated comments as part of subroutine documentation
C                 procedure.
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      REAL*8     AMDON                  , AMREC
C-----------------------------------------------------------------------
      INTEGER    INTYP                  , OUTTYP                  ,
     &           IEVAL
      INTEGER    I
C-----------------------------------------------------------------------
      REAL*8     EIN(IEVAL)             , EOUT(IEVAL)
      REAL*8     ECONV(3)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C  OUTPUT ENERGY UNITS: EV   DONOR TEMPERATURE
C-----------------------------------------------------------------------
         IF (OUTTYP.EQ.1) THEN
            ECONV(2) = AMDON/AMREC
            ECONV(3) = AMDON
C-----------------------------------------------------------------------
C  OUTPUT ENERGY UNITS: EV   RECEIVER TEMPERATURE
C-----------------------------------------------------------------------
         ELSEIF (OUTTYP.EQ.2) THEN
            ECONV(1) = AMREC/AMDON
            ECONV(3) = AMREC
C-----------------------------------------------------------------------
C  OUTPUT ENERGY UNITS: EV/AMU
C-----------------------------------------------------------------------
         ELSEIF (OUTTYP.EQ.3) THEN
            ECONV(1) = 1.0/AMDON
            ECONV(2) = 1.0/AMREC
         ELSE
            STOP
     &      ' CEECON ERROR: INVALID OUTPUT ENERGY TYPE SPECIFIER'
         ENDIF
         
C-----------------------------------------------------------------------
         IF (INTYP.EQ.OUTTYP) THEN
            DO 1 I=1,IEVAL
               EOUT(I) = EIN(I)
    1       CONTINUE
         ELSE
            DO 4 I=1,IEVAL
               EOUT(I) = ECONV(INTYP) * EIN(I)
    4       CONTINUE
         ENDIF
C-----------------------------------------------------------------------
      RETURN
      END
