CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas314/cespf1.for,v 1.1 2004/07/06 12:04:00 whitefor Exp $ Date $Date: 2004/07/06 12:04:00 $
CX
      SUBROUTINE CESPF1( LQCX   , LSCX   , LPAPER ,
     &                   DSNQCX , DSNSCX , DSNPAP , 
     &                   LPEND  )
     
      IMPLICIT NONE
C-----------------------------------------------------------------------
C  ****************** FORTRAN77 SUBROUTINE: CESPF1 *********************
C  PURPOSE: TO RETRIEVE OUTPUT DATA SET SPECIFICATIONS FROM IDL.
C  CALLING PROGRAM: ADAS314
C  SUBROUTINE:
C  OUTPUT: (L*4)   LPAPER   = .TRUE.  => OUTPUT TEXT TO 'PAPER.TXT'
C                                        FILE.
C                             .FALSE. => NO TEXT OUTPUT
C  OUTPUT: (L*4)   LQCX     = .TRUE.  => OUTPUT DATA TO ADF01 PASSING
C                                        FILE.
C                             .FALSE. => NO OUTPUT OF CURRENT DATA TO
C                                        ADF01 PASSING FILE.
C  OUTPUT: (L*4)   LSCX     = .TRUE.  => OUTPUT DATA TO ADF24 PASSING
C                                        FILE.
C                             .FALSE. => NO OUTPUT OF CURRENT DATA TO
C                                        ADF14 PASSING FILE.
C  OUTPUT: (C*80)  DSNQCX   = OUTPUT PASSING ADF01 NAME
C  OUTPUT: (C*80)  DSNSCX   = OUTPUT PASSING ADF14 NAME
C  OUTPUT: (C*80)  DSNPAP   = OUTPUT TEXT FILE NAME
C  OUTPUT: (L*4)   LPEND    = .TRUE.  => USER SELECTED 'CANCEL'
C                             .FALSE. => USER DID NOT
C          (I*4)   PIPEIN   = STANDARD INPUT
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C AUTHOR:  Martin O'Mullane
C DATE:    09/07/98
C VERSION: 1.1                          DATE: 09-07-98
C MODIFIED: Martin O'Mullane
C           FIRST VERSION
C
C VERSION:	1.1					DATE: 01-12-98
C MODIFIED: RICHARD MARTIN
C		- PUT UNDER SCCS CONTROL
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER      PIPEIN, ILOGIC, ONE
      PARAMETER   (PIPEIN = 5 , ONE = 1 )
C-----------------------------------------------------------------------
      CHARACTER    DSNQCX*80 , DSNSCX*80 , DSNPAP*80 
C-----------------------------------------------------------------------
      LOGICAL      LPAPER    , LQCX      , LSCX       , LPEND      
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      READ(PIPEIN,*) ILOGIC
      IF (ILOGIC .EQ. ONE) THEN 
         LPEND = .TRUE.
      ELSE
         LPEND = .FALSE.
      ENDIF
C-----------------------------------------------------------------------
C  UNLESS USER ACTIONED CANCEL READ MORE INPUT
C-----------------------------------------------------------------------
      IF(.NOT.LPEND)THEN
         READ(PIPEIN,'(A)')DSNQCX
         READ(PIPEIN,'(A)')DSNSCX
         READ(PIPEIN,'(A)')DSNPAP
         READ(PIPEIN,*)ILOGIC
         IF(ILOGIC.EQ.0)THEN
            LQCX=.FALSE.
         ELSE
            LQCX=.TRUE.
         ENDIF
         READ(PIPEIN,*)ILOGIC
         IF(ILOGIC.EQ.0)THEN
            LSCX=.FALSE.
         ELSE
            LSCX=.TRUE.
         ENDIF
         READ(PIPEIN,*)ILOGIC
         IF(ILOGIC.EQ.0)THEN
            LPAPER=.FALSE.
         ELSE
            LPAPER=.TRUE.
         ENDIF
      ELSE
         LPEND = .TRUE.
      ENDIF
      RETURN
      END
