CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas314/cesgcx.for,v 1.4 2015/12/21 14:24:22 mog Exp $ Date $Date: 2015/12/21 14:24:22 $
CX
      SUBROUTINE CESGCX ( LSETX    , LPASS    , ILTYP    , IOPT     ,
     &                    NENIN    , ENIN     , SGIN     ,
     &                    LTHETA   , VREL     , XSEC
     &                  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ********************** FORTRAN77 SUBROUTINE: CESGCX ******************
C
C  VERSION: 1.0 (ADAS91)
C
C  PURPOSE:  INTERPOLATES CROSS-SECTION DATA FROM AN INPUT VECTOR OF
C            VALUES USING CUBIC SPLINES.
C
C            EXTRAPOLATES FOR RELATIVE SPEEDS OUT OF DATA RANGE
C            ACCORDING TO VARIOUS TYPES (ILTYP).  LOGARITHMIC
C            INTERPOLATION MAY BE USED (LPASS). SPEED ECONOMY IS
C            POSSIBLE FOR REPEATS WITH THE SAME SPLINE KNOTS (LSETX).
C
C  CALLING PROGRAM:  CEEVTH
C
C  NOTES:
C        (1) FOR  ILTYP.EQ.0, EXTRAPOLATION IS AS FOLLOWS:
C              XSEC = SIG0*DEXP(-ALPH0/VREL) FOR VREL<VREL(MIN)
C              XSEC = SIG1*VREL**(-7.0)  FOR VREL> VREL(MAX),
C              WHERE VREL(MIN), VREL(MAX) ARE THE FIRST AND LAST FROM
C              INPUT VALUES IN DATA TABLES IN ADF24.
C            FOR  ILTYP.NE.0, EXTRAPOLATION IS AS AS ABOVE AT THIS
C              TIME.
C
C        (2) Based on sigcx.for but is a special version for ADAS314.
C
C  SUBROUTINE:
C
C  INPUT : (L*4)   LSETX    = .TRUE. => SPLINE NOT SET FOR THESE KNOTS
C                             .FLSE. => SPLINE NOT FOR THESE KNOTS
C  INPUT : (L*4)   LPASS    = .TRUE. => DO NOT CONVERT INTO LOG10 FOR
C                                       ENERGIES AND X-SECTS. FOR SPLINE
C                             .FLSE. => CONVERT INTO LOG10 FOR
C                                       ENERGIES AND X-SECTS. FOR SPLINE
C  INPUT : (I*4)   ILTYP    = TYPE FOR LOW AND HIGH ENERGY CROSS-
C                             -SECTION EXTRAPOLATION.
C  INPUT : (I*4)   IOPT     = SPLINE END POINT CURVATURE/GRADIENT OPTION
C                             1 => DDY1 = 0, DDYN = 0
C                             4 => DY1 = 0 , DDYN = 0
C
C  INPUT : (I*4)   NENIN    = NUMBER OF ENERGIES IN INPUT DATA SET
C  INPUT : (R*8)   ENIN()   = ENERGIES (EV/AMU) IN INPUT DATA SET
C  INPUT : (R*8)   SGIN()   = INPUT X-SECTIONS (CM2) FROM INPUT DATA SET
C                             1ST.DIM: ENERGY INDEX
C  INPUT : (I*4)   LTHETA   = NUMBER OF VALUES IN VREL VECTOR
C  INPUT : (R*8)   VREL()   = RELATIVE SPEEDS FOR OUTPUT (CM S-1)
C
C  OUTPUT: (R*8)   XSEC()   = OUTPUT CROSS-SECTION (CM2)
C
C          (I*4)   MAXENS   = PARAMETER = MAX. LENGTH OF TABULAR XSECT.
C                                         VECTOR
C          (I*4)   LDTHET   = PARAMETER = MAX. LENGTH OF INTERNAL
C                                         VECTORS
C          (R*8)   CMSAMU   = PARAMETER = CONVERSION FACTOR FOR ENERGY
C                                         (AMU) TO VELOCITY (CM S-1)
C
C          (I*4)   I        = GENERAL INDEX
C          (I*4)   N        = GENERAL INDEX
C          (R*8)   ALPH0    = LOW VELOCITY EXTRAPOLATION PARAMETER
C          (R*8)   EXPON    = EXPONENT OF EXPONENTIAL
C          (R*8)   VSLOPE   = HIGH VELOCITY EXTRAPOLATION PARAMETER
C          (R*8)   XIN()    = INTERNAL SPLINE INDEPENDENT VARIABLE
C          (R*8)   YIN()    = INTERNAL SPLINE DEPENDENT VARIABLE
C          (R*8)   VIN()    = INTERNAL VECTOR
C          (R*8)   DY()     = DERIVATIVES AT SPLINE KNOTS
C          (R*8)   XOUT()   = INTERNAL OUTPUT INDEPENDENT VARIABLE
C          (R*8)   YOUT()   = INTERNAL OUTPUT DEPENDENT VARIABLE
C          (L*4)   LINTRP() = .TRUE.  => POINT INTERPOLATED
C                           = .FALSE. => POINT EXTRAPOLATED
C
C
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          XXSPLE     ADAS      INTERPOLATES USING CUBIC SPLINES
C          R8FUN1     ADAS      EXTERNAL FUNCTION FOR XXSPLE
C
C
C  AUTHOR:  H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C           JA8.08
C           TEL. 0141-553-4196
C
C  DATE:    03/11/95
C
C  UPDATE:  11/04/96  HP SUMMERS - TRAPPED CASE OF ZERO RELATIVE SPEED
C
C UNIX-IDL PORT: H.P.SUMMERS
C
C VERSION: 1.1				DATE: 18-02-99
C MODIFIED: Martin O'Mullane
C           - Based on SIGCX.FOR
C
C VERSION: 1.2				DATE: 17-05-07
C MODIFIED: Allan Whiteford
C               - Updated comments as part of subroutine documentation
C                 procedure.
C
C VERSION : 1.3				
C DATE    : 18-12-2013
C MODIFIED: Martin O'Mullane
C           - Save value of xin between calls.
C
C--------------------------------------------------------------------
      INTEGER  MAXENS   , LDTHET
C--------------------------------------------------------------------
      REAL*8   CMSAMU   , ZERO
C--------------------------------------------------------------------
      PARAMETER ( MAXENS = 50 , LDTHET = 181 )
      PARAMETER ( CMSAMU = 1.389209D+06      , ZERO = 1.0D-60     )
C--------------------------------------------------------------------
      INTEGER  I      , N      , NENIN
      INTEGER  IOPT   , LTHETA , ILTYP  , I4UNIT
C--------------------------------------------------------------------
      REAL*8   ALPH0  , VSLOPE , EXPON
      REAL*8   R8FUN1
C--------------------------------------------------------------------
      LOGICAL  LSETX  , LPASS
C--------------------------------------------------------------------
      REAL*8   VREL(LTHETA)    , XSEC(LTHETA)
      REAL*8   XOUT(LDTHET)    , YOUT(LDTHET)
      REAL*8   ENIN(NENIN)     , SGIN(NENIN)     , DY(MAXENS)
      REAL*8   XIN(MAXENS)     , YIN(MAXENS)     , VIN(MAXENS)
C--------------------------------------------------------------------
      LOGICAL  LINTRP(LDTHET)
C--------------------------------------------------------------------
      EXTERNAL R8FUN1
C--------------------------------------------------------------------
      SAVE     XIN
C--------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C  SET PARAMETERS FOR CROSS-SECTION EXTRAPOLATION DEPENDING ON ILTYP
C-----------------------------------------------------------------------
C
      IF(ILTYP.EQ.1) THEN
          ALPH0  = 0.0
          VSLOPE = 7.0
      ELSE
          ALPH0  = 0.0
          VSLOPE = 7.0
      ENDIF
C
C-----------------------------------------------------------------------
C  SET LOGARITHMIC VALUES FOR VARIABLES FOR INTERPOLATION IF REQUIRED
C-----------------------------------------------------------------------
C
      IF(.NOT.LPASS) THEN
          DO 10 N = 1,NENIN
            YIN(N) = DLOG10(SGIN(N)+ZERO)
 10       CONTINUE
          IF(LSETX) THEN
              DO 15 N = 1,NENIN
                VIN(N) = CMSAMU*DSQRT(ENIN(N))
                XIN(N) = DLOG10(VIN(N))
 15           CONTINUE
          ENDIF
C
          DO 20 I = 1,LTHETA
            XOUT(I) = DLOG10(VREL(I))
 20       CONTINUE
      ELSE
          DO 30 N = 1,NENIN
            YIN(N) = SGIN(N)+ZERO
 30       CONTINUE
          IF(LSETX) THEN
              DO 35 N = 1,NENIN
                VIN(N) = CMSAMU*DSQRT(ENIN(N))
                XIN(N) = VIN(N)
 35           CONTINUE
          ENDIF

          DO 40 I = 1,LTHETA
            XOUT(I) = VREL(I)
 40       CONTINUE
      ENDIF

      CALL XXSPLE ( LSETX   , IOPT   , R8FUN1   ,
     &              NENIN   , XIN    , YIN      ,
     &              LTHETA  , XOUT   , YOUT     ,
     &              DY      , LINTRP
     &             )

      DO I=1,LTHETA
        IF (LPASS) THEN
           XSEC(I) = YOUT(I)
        ELSE
           XSEC(I) = 10.0**YOUT(I)
        ENDIF
        IF (XSEC(I).LT.ZERO) XSEC(I) = ZERO
      END DO


C FOR IOPT=0 SET VALUES BELOW FIRST ENERGY IN INPUT TO ZERO

      IF (IOPT.EQ.0) THEN
      
         DO I=1,LTHETA
           IF (.NOT.LINTRP(I).AND.XOUT(I).LT.XIN(1)) XSEC(I) = ZERO
         END DO
         
      ENDIF

      RETURN
      END
