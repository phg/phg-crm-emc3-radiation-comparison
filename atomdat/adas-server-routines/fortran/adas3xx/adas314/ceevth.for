CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas314/ceevth.for,v 1.3 2007/05/17 17:08:17 allan Exp $ Date $Date: 2007/05/17 17:08:17 $
CX
       SUBROUTINE CEEVTH ( NDENR  ,
     &                     LSETX  , LPASS  ,
     &                     AMDON  , AMREC  , CATYP  , DREN   , 
     &                     ILTYP  , IEXTYP ,  
     &                     NENIN  , ENIN   , NENOUT , ENOUT  ,
     &                     SGIN   , RCOUT
     &                   )
       IMPLICIT NONE
C-----------------------------------------------------------------------
c
C  ********************** FORTRAN77 SUBROUTINE: CEEVTH *****************
C
C  VERSION: 1.0  
C
C  PURPOSE:  OBTAINS RATE COEFFICIENTS FOR DONOR/RECEIVER CHARGE
C            EXCHANGE COLLISIONS FOR CASES OF
C            MONOENERGETIC DONOR/THERMAL RECEIVER, THERMAL
C            DONOR/MONOENERGETIC RECEIVER, THERMAL DONOR/THERMAL
C            RECEIVER (SAME TEMPERATURE) FROM CROSS-SECTION TABULATIONS.
C
C            A MONO-ENERGETIC CASE IS ALLOWED WHICH CONVERTS INPUT
C            CROSS-SECTIONS TABULATED AT A SET OF ENERGIES/AMU TO
C            OUTPUT CROSS-SECTIONS TABULATED A DIFFERENT SET OF
C            ENERGIES/AMU.
C
C  CALLING PROGRAM:  ADAS314
C
C
C  SUBROUTINE:
C
C  INPUT :  (I*4)  NDENR    = MAX. NUMBER OF ENERGIES/TEMPERATURES
C                             IN INPUT/OUTPUT ENERGY/TEMPERATURE
C                             VECTORS
C  INPUT :  (L*4)  LSETX    = .TRUE. => SPLINE PRESET FOR THESE KNOTS
C                             .FLSE. => SPLINE NOT SET FOR THESE KNOTS
C  INPUT :  (L*4)  LPASS    = .TRUE. => DO NOT CONVERT INTO LOG10 FOR
C                                       ENERGIES AND X-SECTS. FOR SPLINE
C                             .FALSE.=> CONVERT INTO LOG10 FOR
C                                       ENERGIES AND X-SECTS. FOR SPLINE
C  INPUT :  (R*8)  AMDON    = DONOR MASS NUMBER
C  INPUT :  (R*8)  AMREC    = RECEIVER MASS NUMBER
C  INPUT :  (C2)   CATYP    = 'TT' THERMAL/THERMAL (EQUAL TEMPERATURES
C                                  FOR DONOR AND RECEIVER ONLY)
C                             'TR' THERMAL RECEIVER, MONOENERGETIC DONOR
C                             'TD' THERMAL DONOR, MONOENERGETIC RECEIVER
C                             'ME' SPECIAL MONOENERGETIC CASE
C  INPUT : (R*8)   DREN     = DONOR ENERGY    ( 'TR' CASE )
C                             RECEIVER ENERGY ( 'TD' CASE )
C  INPUT : (I*4)   ILTYP    = TYPE FOR LOW AND HIGH ENERGY CROSS-
C                             SECTION EXTRAPOLATION
C                             *** a redundant parameter ***
C                             *** superceeded by IEXTYP ***
C          (I*4)   IEXTYP   = 1 => SET LOWER ENERGIES TO FIRST POINT IN DATA 
C                           = 2 => SET LOWER ENERGIES TO 0.0
C  INPUT : (I*4)   NENIN    = NUMBER OF ENERGIES IN INPUT DATA SET
C  INPUT : (R*8)   ENIN()   = ENERGIES (EV/AMU) IN INPUT DATA SET
C  INPUT : (I*4)   NENOUT   = NUMBER OF TEMPERATURES FOR OUTPUT DATA SET
C  INPUT : (R*8)   ENOUT()  = TEMPERATURES (EV) FOR OUTPUT DATA SET FOR
C                             'TT', 'TD', 'TR' CASES.
C                           = ENERGY/AMU FOR OUTPUT DATA SET FOR
C                             'ME' CASE.
C  INPUT : (R*8)   SGIN()   = INPUT X-SECTIONS (CM2) FROM INPUT DATA SET
C                             1ST.DIM: ENERGY INDEX
C  OUTPUT: (R*8)   RCOUT()  = RATE COEFF. (CM3 S-1) IN OUTPUT DATA SET
C                             1ST.DIM: TEMPERATURE INDEX
C          (I*4)   I        = GENERAL INDEX
C          (I*4)   IT       = GENERAL INDEX
C          (I*4)   ITHETA   = GENERAL INDEX
C          (I*4)   IOPT     = SPLINE END POINT CURVATURE/GRADIENT OPTION
C                             1 => DDY1 = 0, DDYN = 0
C                             4 => DY1 = 0 , DDYN = 0
C          (I*4)   IXD      = DONOR GAUSSIAN QUADRATURE INDEX
C          (I*4)   IXR      = RECEIVER GAUSSIAN QUADRATURE INDEX
C          (I*4)   NGS      = GAUSSIAN QUARATURE DIMENSION
C          (I*4)   NTHETA   = NUMBER OF ANGLE VALUES FOR QUADRATURE
C          (I*4)   LTHETA   = NTHETA+1
C          (I*4)   L1       = PARAMETER = 1
C          (R*8)   ETHD     = THERMAL ENERGY OF DONOR       (JOULES)
C          (R*8)   ETHR     = THERMAL ENERGY RECEIVER       (JOULES)
C          (R*8)   HSIMP    = SIMPSON'S RULE STEP INTERVAL
C          (R*8)   THETA    = ANGLE BETWEEN PARTICLE VELOCITIES (RAD)
C          (R*8)   FAC      = GENERAL VARIABLE
C          (R*8)   FLAG     = GENERAL VARIABLE
C          (R*8)   XMDKG    = DONOR MASS    (KG)
C          (R*8)   XMRKG    = RECEIVER MASS (KG)
C          (R*8)   VD       = DONOR SPEED    (M S-1)
C          (R*8)   VR       = RECEIVER SPEED (M S-1)
C          (R*8)   RATE     = EVALUATED RATE COEFFICIENT (CM3 S-1)
C          (R*8)   PART1    = GENERAL VARIABLE
C          (R*8)   PART2    = GENERAL VARIABLE
C          (R*8)   PART3    = GENERAL VARIABLE
C          (R*8)   PART12   = GENERAL VARIABLE
C          (R*8)   PART23   = GENERAL VARIABLE
C          (R*8)   PART123  = GENERAL VARIABLE
C          (R*8)   VREL1    = GENERAL RELATIVE SPEED VARIABLE
C          (R*8)   XSEC1    = GENERAL CROSS-SECTION VARIABLE
C          (R*8)   VAL      = GENERAL VARIABLE
C          (R*8)   XGS()    = GAUSSIAN QUADRATURE NODES
C          (R*8)   WGS()    = GAUSSIAN QUADRATURE WEIGHTS
C          (R*8)   VREL()   = RELATIVE SPEED OF PARTICLES FOR DIFFERENT
C                             ANGLES (CM S-1)
C          (R*8)   XSEC()   = CHARGE EXCHANGE CROSS-SECTIONS FOR
C                             RELATIVE SPEEDS AT DIFFERENT ANGLES (CM2)
C
C
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          CESGCX     ADAS      INTERPOLATES CX CROSS-SECTION TABLES
C
C
C  AUTHOR:  H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C           JA8.08
C           TEL. 0141-553-4196
C  DATE:    02/11/95
C
C  UPDATE:  09/07/98  Martin O'Mullane 
C           CHANGED NAME FROM CXTHER TO CDEVTH. SIMILAR FUNCTIONALITY
C           BUT IS EXTENDED TO DEAL WITH EXTRA AVERAGING METHODS.
C
C VERSION:	1.1					DATE: 01-12-98
C MODIFIED: RICHARD MARTIN
C		- PUT UNDER SCCS CONTROL
C
C VERSION:	1.2					DATE: 24-03-99
C MODIFIED: MARTIN O'MULLANE
C		- SECOND VERSIONS
C
C VERSION:      1.3                                     DATE: 17-05-07
C MODIFIED: Allan Whiteford
C               - Updated comments as part of subroutine documentation
C                 procedure.
C-----------------------------------------------------------------------
      INTEGER     NGS           , NTHETA        , LTHETA
      INTEGER     L1
C-----------------------------------------------------------------------
      REAL*8      CMSAMU   , PI
C-----------------------------------------------------------------------
      PARAMETER ( NGS = 8  , NTHETA = 180  , LTHETA =NTHETA+1  )
      PARAMETER ( L1 = 1   )
      PARAMETER ( CMSAMU = 1.389209D6 , PI = 3.141592654       )
C-----------------------------------------------------------------------
      INTEGER     NDENR
      INTEGER     NENIN         , NENOUT
      INTEGER     IXD           , IXR           , I           ,
     &            IT            , ITHETA
      INTEGER     ILTYP         , IEXTYP        , IOPT
C-----------------------------------------------------------------------
      REAL*8      AMDON         , AMREC         , DREN
      REAL*8      ETHD          , ETHR          , HSIMP
      REAL*8      RATE
      REAL*8      THETA         , FAC           , FLAG
      REAL*8      XMDKG         , XMRKG         , VD          , VR
      REAL*8      PART1         , PART2         , PART3
      REAL*8      PART12        , PART23        , PART123
      REAL*8      VREL1         , XSEC1         , VAL
C-----------------------------------------------------------------------
      REAL*8      XGS(NGS)      , WGS(NGS)
      REAL*8      ENIN(NDENR)   , SGIN(NDENR)   ,
     &            ENOUT(NDENR)  , RCOUT(NDENR)
      REAL*8      VREL(LTHETA)  , XSEC(LTHETA)
C-----------------------------------------------------------------------
      LOGICAL     LSETX         , LPASS
C-----------------------------------------------------------------------
      CHARACTER   CATYP*2
C-----------------------------------------------------------------------
      DATA XGS/  0.170279632305   ,  0.903701776799  ,  2.251086629866,
     &           4.266700170288   ,  7.045905402393  , 10.758516010181,
     &          15.740678641278   , 22.863131736889   /
      DATA WGS/  3.69188589342D-01,  4.18786780814D-01,
     &           1.75794986637D-01,  3.33434922612D-02,
     &           2.79453623523D-03,  9.07650877336D-05,
     &           8.48574671627D-07,  1.04800117487D-09 /
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
C SET IOPT PARAMETER OF XXSPLN DEPENDING ON TYPE OF EXTRAPOLATION
C-----------------------------------------------------------------------
       
       IF (IEXTYP.EQ.1) THEN
          IOPT = 4
       ELSE
          IOPT = 0
       ENDIF

C-----------------------------------------------------------------------
C CHANGE UNITS OF INPUT VARIABLES
C-----------------------------------------------------------------------
       XMDKG  = 1.6606D-27 * AMDON
       XMRKG  = 1.6606D-27 * AMREC

       DO 50 IT = 1, NENOUT
         RATE    = 0.00D+00
         IF ( CATYP.EQ.'TT' ) THEN

C-----------------------------------------------------------------------
C  MAXWELLIAN DONOR  -  MAXWELLIAN RECEIVER
C-----------------------------------------------------------------------
           ETHD   = 1.6022D-19 * ENOUT(IT)
           ETHR   = 1.6022D-19 * ENOUT(IT)
           PART1   = 0.00D+00
           PART123 = 0.00D+00
C-----------------------------------------------------------------------
C  USE GAUSSIAN QUADRATURE TO INTEGRATE OVER FIRST DISTRIBUTION FUNCTION
C-----------------------------------------------------------------------
           DO 16 IXD = 1,NGS
             VD    = DSQRT(2.0 * ETHD * XGS(IXD) / XMDKG)
             PART1 = 4.0*PI *(1/(2.0*PI))*WGS(IXD)*DSQRT(XGS(IXD) / PI)
C-----------------------------------------------------------------------
C  USE GAUSSIAN QUADRATURE TO INTEGRATE OVER SECOND DISTRIBUTION
C  FUNCTION
C-----------------------------------------------------------------------
             PART2   =  0.00D+00
             PART23  =  0.00D+00
             DO 14 IXR = 1,NGS
               VR    =  DSQRT(2.0 * ETHR * XGS(IXR) / XMRKG)
               PART2 =  (1.0/(2.0*PI)) * WGS(IXR) * DSQRT(XGS(IXR) / PI)
C-----------------------------------------------------------------------
C  USE SIMPSONS RULE TO INTEGRATE VREL AND XSEC(VREL) IN THETA
C-----------------------------------------------------------------------
               PART3  = 0.00D+00
               HSIMP  = PI/NTHETA
               DO 10 ITHETA = 0,NTHETA
                 THETA = (PI * ITHETA) / 180.0
                 I = ITHETA + 1
                 VAL = VD**2 + VR**2 - 2.0*VD*VR*DCOS(THETA)
                 IF(VAL.LE.0.0D0)THEN
                     VREL(I)=0.0D0
                 ELSE
                     VREL(I) = 100.0 *DSQRT(VAL)
                 ENDIF
   10          CONTINUE
C-----------------------------------------------------------------------
C  OBTAIN  CROSS SECTIONS FROM SPLINE INTERPOLATION OF TABULAR VALUES
C-----------------------------------------------------------------------
               CALL CESGCX ( LSETX    , LPASS    , ILTYP    , IOPT    ,
     &                       NENIN    , ENIN     , SGIN     ,
     &                       NTHETA+1 , VREL     , XSEC
     &                     )
               DO 12 ITHETA = 0,NTHETA
                 IF(ITHETA.EQ.0 .OR. ITHETA.EQ.NTHETA) THEN
                     FAC  =  1.00D+00
                 ELSE
                     FLAG  = ( -1.0 ) ** ITHETA
                     IF ( FLAG.LT.0 ) THEN
                         FAC  =  4.00D+00
                     ELSEIF ( FLAG.GT.0 ) THEN
                         FAC  =  2.00D+00
                     ENDIF
                 ENDIF
                 THETA = (PI * ITHETA) / 180.0
                 I     = ITHETA + 1
                 PART3 = PART3 + (2.0*PI*FAC*VREL(I)*XSEC(I)*
     &                   DSIN(THETA))
   12          CONTINUE
               PART3  = PART3 * HSIMP/3.0
               PART23 = PART23 + (PART2 * PART3)
   14        CONTINUE
             PART123 = PART123 + (PART1 * PART23)
   16      CONTINUE
           RATE = PART123
           
           
       ELSEIF ( CATYP.EQ.'TR' ) THEN
C-----------------------------------------------------------------------------
C  MONOENERGETIC DONOR - MAXWELLIAN RECEIVER
C-----------------------------------------------------------------------------
           ETHD   = 1.6022D-19 * DREN
           ETHR   = 1.6022D-19 * ENOUT(IT)
           PART1   = 0.00D+00
           PART12  = 0.00D+00
C-----------------------------------------------------------------------------
C      USE GAUSSIAN QUADRATURE TO INTEGRATE OVER DISTRIBUTION FUNCTION
C      OF RECEIVER.
C-----------------------------------------------------------------------------
           DO 24 IXR = 1,NGS
             VR    = DSQRT(2.0 * ETHR * XGS(IXR) / XMRKG)
             PART1 = WGS(IXR) * DSQRT(XGS(IXR) / PI)
             VD    = DSQRT(2.0 * ETHD / XMDKG)
C-----------------------------------------------------------------------------
C      USE SIMPSON'S RULE TO INTEGRATE VREL AND XSEC(VREL) IN THETA
C-----------------------------------------------------------------------------
             PART2  = 0.00D+00
             HSIMP  = PI/NTHETA
             DO 20 ITHETA = 0,NTHETA
               THETA = (PI * ITHETA) / 180.0
               I     = ITHETA + 1
               VAL = VD**2 + VR**2 - 2.0*VD*VR*DCOS(THETA)
               IF(VAL.LE.0.0D0)THEN
                   VREL(I)=0.0D0
               ELSE
                   VREL(I) = 100.0 *DSQRT(VAL)
               ENDIF
   20        CONTINUE
C-----------------------------------------------------------------------------
C      INTRODUCE CROSS SECTIONS FROM TABLES
C-----------------------------------------------------------------------------
               CALL CESGCX ( LSETX    , LPASS    , ILTYP    , IOPT    ,
     &                       NENIN    , ENIN     , SGIN     ,
     &                       NTHETA+1 , VREL     , XSEC
     &                     )
             DO 22 ITHETA = 0,NTHETA
               IF(ITHETA.EQ.0.OR.ITHETA.EQ.NTHETA) THEN
                   FAC = 1.00D+00
               ELSE
                   FLAG = ( -1.0 )**ITHETA
                   IF(FLAG.LT.0) THEN
                       FAC = 4.00D+00
                   ELSEIF(FLAG.GT.0) THEN
                       FAC = 2.00D+00
                   ENDIF
               ENDIF
               THETA = (PI * ITHETA) / 180.0
               I     = ITHETA + 1
               PART2 = PART2 + (FAC*VREL(I)*XSEC(I)*DSIN(THETA))
   22        CONTINUE
             PART2 = PART2 * HSIMP/3.0
             PART12 = PART12 + (PART1 * PART2)
   24      CONTINUE
           RATE = PART12
           
           
       ELSEIF ( CATYP.EQ.'TD' ) THEN
C-----------------------------------------------------------------------
C      MAXWELLIAN DONOR - MONOENERGETIC RECEIVER
C-----------------------------------------------------------------------
           ETHD   = 1.6022D-19 * ENOUT(IT)
           ETHR   = 1.6022D-19 * DREN
           PART1   = 0.00D+00
           PART12  = 0.00D+00
C-----------------------------------------------------------------------
C      USE GAUSSIAN QUADRATURE TO INTEGRATE OVER DISTRIBUTION FUNCTION
C      OF DONOR
C-----------------------------------------------------------------------
           DO 34 IXD = 1,NGS
             VD    = DSQRT(2.0 * ETHD * XGS(IXD) / XMDKG)
             PART1 = WGS(IXD) * DSQRT(XGS(IXD) / PI)
             VR    = DSQRT(2.0 * ETHR / XMRKG)
C-----------------------------------------------------------------------
C      USE SIMPSON'S RULE TO INTEGRATE VREL AND XSEC(VREL) IN THETA
C-----------------------------------------------------------------------
             PART2  = 0.00D+00
             HSIMP  = PI/NTHETA
             DO 30 ITHETA = 0,NTHETA
               THETA = (PI * ITHETA) / 180.0
               I     = ITHETA + 1
               VAL = VD**2 + VR**2 - 2.0*VD*VR*DCOS(THETA)
               IF(VAL.LE.0.0D0)THEN
                   VREL(I)=0.0D0
               ELSE
                   VREL(I) = 100.0 *DSQRT(VAL)
               ENDIF
   30        CONTINUE
C-----------------------------------------------------------------------
C      INTRODUCE CROSS SECTIONS FROM TABLES
C-----------------------------------------------------------------------
               CALL CESGCX ( LSETX    , LPASS    , ILTYP    , IOPT    ,
     &                       NENIN    , ENIN     , SGIN     ,
     &                       NTHETA+1 , VREL     , XSEC
     &                     )
             DO 32 ITHETA = 0,NTHETA
               IF(ITHETA.EQ.0.OR.ITHETA.EQ.NTHETA) THEN
                   FAC = 1.00D+00
               ELSE
                   FLAG = ( -1.0 )**ITHETA
                   IF(FLAG.LT.0) THEN
                       FAC = 4.00D+00
                   ELSEIF(FLAG.GT.0) THEN
                       FAC = 2.00D+00
                   ENDIF
               ENDIF
               THETA = (PI * ITHETA) / 180.0
               I     = ITHETA + 1
               PART2 = PART2 + (FAC*VREL(I)*XSEC(I)*DSIN(THETA))
   32        CONTINUE
             PART2  = PART2 * HSIMP/3.0
             PART12 = PART12 + (PART1 * PART2)
   34      CONTINUE
           RATE = PART12
           
           
       ELSEIF ( CATYP.EQ.'ME' ) THEN
C-----------------------------------------------------------------------------
C  SPECIAL MONOENERGETIC CASE
C-----------------------------------------------------------------------------
       VREL(L1) = CMSAMU*DSQRT(ENOUT(IT))
C-----------------------------------------------------------------------
C      INTRODUCE CROSS SECTIONS FROM TABLES
C-----------------------------------------------------------------------
           CALL CESGCX ( LSETX    , LPASS    , ILTYP    , IOPT    ,
     &                   NENIN    , ENIN     , SGIN     ,
     &                   L1       , VREL     , XSEC
     &                )
           RATE = XSEC(L1)
       ENDIF
       
       RCOUT(IT) = RATE
       
   50  CONTINUE
C-----------------------------------------------------------------------
       RETURN
       END
