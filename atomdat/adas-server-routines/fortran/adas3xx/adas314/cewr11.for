CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas314/cewr11.for,v 1.5 2010/05/10 17:30:22 mog Exp $ Date $Date: 2010/05/10 17:30:22 $
CX
       SUBROUTINE CEWR11( DSFULL , DATE   ,
     &                    IUNIT  , MXNENG , MXNSHL ,
     &                    CATYP  , AMDON  , AMREC  , DREN   ,
     &                    SYMBR  , SYMBD  , IZR    , IZD    ,
     &                    INDD   , NENRGY , NMIN   , NMAX   ,
     &                    LPARMS , LSETL  , ENRGYA ,
     &                    ALPHAA , LFORMA , XLCUTA , PL2A   ,
     &                    PL3A   , SIGTA  , SIGNA  , SIGLA
     &                  )
       IMPLICIT NONE
C----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: CEWR11 *********************
C
C  PURPOSE:  TO OUTPUT DATA TO MODIFIED ADF01 FILE.
C
C            IF THE ADF01 FILE IS FOR THERMAL/THERMAL,
C            THERMAL/DONOR OR THERMAL/RECEIVER THEN THE RATE
C            (PASSED AS SIGMA) IS DIVIDED BY
C                    1.384D4 * 100 * DSQRT(TE)
C            IN ORDER TO ALLOW THE FILE TO BE USED WITH UNMODIFIED
C            SERIES 3 PROGRAMS.
C
C  CALLING PROGRAM: ADAS314
C
C  DATA:
C           THE UNITS USED IN THE DATA FILE ARE TAKEN AS FOLLOWS:
C           COLLISION ENERGIES  : KEV/AMU
C           ALPHA               :
C           TOTAL XSECTS.       : CM2
C           N-SHELL XSECTS.     : CM2
C           NL-SHELL DATA       : CM2
C           NLM-SHELL DATA      : CM2
C
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IUNIT     = INPUT  UNIT NUMBER FOR RESULTS
C  INPUT : (I*4)  MXNENG    = MAXIMUM NO. OF ENERGIES.
C  INPUT : (I*4)  MXNSHL    = MAXIMUM NO. OF N SHELLS.
C  INPUT : (C*80) TITLED    = NOT SET - TITLE FOR DATA SOURCE.
C  INPUT : (C*80) DSFULL    = SOURCE DATASET
C  INPUT : (C2)   CATYP     = 'TT' THERMAL/THERMAL (EQUAL TEMPERATURES
C                                  FOR DONOR AND RECEIVER ONLY)
C                             'TR' THERMAL RECEIVER, MONOENERGETIC DONOR
C                             'TD' THERMAL DONOR, MONOENERGETIC RECEIVER
C                             'ME' SPECIAL MONOENERGETIC CASE
C  INPUT : (R*8)  AMDON     = DONOR MASS NUMBER
C  INPUT : (R*8)  AMREC     = RECEIVER MASS NUMBER
C  INPUT : (R*8)  DREN      = DONOR ENERGY    ( 'TR' CASE )
C                             RECEIVER ENERGY ( 'TD' CASE )
C  INPUT : (C*2)  SYMBR     = READ - RECEIVER ION ELEMENT SYMBOL.
C  INPUT : (C*2)  SYMBD     = READ - DONOR ION ELMENT SYMBOL.
C  INPUT : (I*4)  IZR       = READ - ION CHARGE OF RECEIVER.
C  INPUT : (I*4)  IZD       = READ - ION CHARGE OF DONOR.
C  INPUT : (I*4)  INDD      = READ - DONOR STATE INDEX.
C  INPUT : (I*4)  NENRGY    = NUMBER OF ENERGIES READ.
C  INPUT : (I*4)  NMIN      = LOWEST N-SHELL FOR WHICH DATA READ.
C  INPUT : (I*4)  NMAX      = HIGHEST N-SHELL FOR WHICH DATA READ.
C  INPUT : (L*4)  LPARMS    = FLAGS IF L-SPLITTING PARAMETERS PRESENT.
C                             .TRUE.  => L-SPLITTING PARAMETERS PRESENT.
C                             .FALSE  => L-SPLITTING PARAMETERS ABSENT.
C  INPUT : (L*4)  LSETL     = FLAGS IF L-RESOLVED DATA PRESENT.
C                             .TRUE.  => L-RESOLVED DATA PRESENT.
C                             .FALSE  => L-RESOLVED DATA ABSENT.
C  INPUT : (L*4)  LSETM     = FLAGS IF M-RESOLVED DATA PRESENT.
C                             .TRUE.  => M-RESOLVED DATA PRESENT.
C                             .FALSE  => M-RESOLVED DATA ABSENT.
C  INPUT : (R*8)  ENRGYA()  = READ - COLLISION ENERGIES.
C                             UNITS: EV/AMU (READ AS KEV/AMU)
C                             DIMENSION: ENERGY INDEX
C  INPUT : (R*8)  ALPHAA()  = READ - EXTRAPOLATION PARAMETER ALPHA.
C                             DIMENSION: ENERGY INDEX
C  INPUT : (I*4)  LFORMA()  = READ - PARAMETERS FOR CALCULATING L-RES
C                                    X-SEC.
C                             DIMENSION: ENERGY INDEX
C  INPUT : (R*8)  XLCUTA()  = READ - PARAMETERS FOR CALCULATING L-RES
C                                    X-SEC.
C                             DIMENSION: ENERGY INDEX
C  INPUT : (R*8)  PL2A()    = READ - PARAMETERS FOR CALCULATING L-RES
C                                    X-SEC.
C                             DIMENSION: ENERGY INDEX
C  INPUT : (R*8)  PL3A()    = READ - PARAMETERS FOR CALCULATING L-RES
C                                    X-SEC.
C                             DIMENSION: ENERGY INDEX
C  INPUT : (R*8)  SIGTA()   = READ - TOTAL CHARGE EXCHANGE
C                                    CROSS-SECTION.
C                             UNITS: CM2
C                             DIMENSION: ENERGY INDEX
C  INPUT : (R*8)  SIGNA(,)  = READ - N-RESOLVED CHARGE EXCHANGE
C                                    CROSS-SECTIONS.
C                             UNITS: CM2
C                             1ST DIMENSION: ENERGY INDEX
C                             2ND DIMENSION: N-SHELL
C  INPUT : (R*8)  SIGLA(,)  = READ - L-RESOLVED CHARGE EXCHANGE
C                                    CROSS-SECTIONS.
C                             UNITS: CM2
C                             1ST DIMENSION: ENERGY INDEX
C                             2ND DIMENSION: INDEXED BY I4IDFL(N,L)
C  INPUT : (R*8)  SIGMA(,)  = READ - M-RESOLVED CHARGE EXCHANGE
C                                    CROSS-SECTIONS.
C                             UNITS: CM2
C                             1ST DIMENSION: ENERGY INDEX
C                             2ND DIMENSION: INDEXED BY I4IDFM(N,L,M)
C                                            WITH M >= 0 ONLY
C          (I*4)  NWIDTH   = NUMBER OF ENERGY VALUES PER LINE
C          (I*4)  IVALUE   = USED TO PARSE FOR END OF DATA FLAG (-1).
C          (I*4)  N        = N QUANTUM NUMBER.
C          (I*4)  L        = L QUANTUM NUMBER.
C          (I*4)  M        = M QUANTUM NUMBER.
C          (I*4)  I        = LOOP COUNTER.
C          (I*4)  K        = LOOP COUNTER.
C          (I*4)  IERR     = ERROR RETURN CODE.
C          (C*1)  INDD     = DONOR STATE INDEX.
C          (C*28) UID      = USER IDENTIFIER.
C          (C*8)  DATE     = CURRENT DATE.
C          (R*8)  FMUL     = MULTIPLER 1.384D4 * 100 
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4FCTN     ADAS      RETURNS CHARACTER STRING AS AN INTEGER.
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          I4IDFL     ADAS      RETURNS UNIQUE INDEX FROM QUANTUM
C                               NUMBERS N AND L.
C          I4IDFM     ADAS      RETURNS UNIQUE INDEX FROM QUANTUM
C                               NUMBERS N, L AND M.
C          XXIDTL     ADAS      INVERSE OF I4IDFL. RETURNS QUANTUM
C                               NUMBERS N AND L FROM INDEX.
C          XXIDTM     ADAS      INVERSE OF I4IDFM. RETURNS QUANTUM
C                               NUMBERS N, L AND M FROM INDEX.
C          XXNAME     ADAS      FINDS REAL NAME OF USER
C          XXSLEN     ADAS      FINDS NON BLANK PART OF STRING
C
C
C AUTHOR:  H. P. SUMMERS, UNIVERSITY OF STRATHCLYDE
C          JA8.08
C          TEL. 0141-553-4196
C DATE:    19/09/95
C
C
C UPDATE:  27/08/97  HP SUMMERS - CHANGED NAME FROM CCWR11 TO CDWR11
C                                 REMOVED REDUNDANT FORMATS 2000, 2006
C UPDATE:  09/07/98  Martin O'Mullane  - added DATE to input list and 
C                                        removed call to xxuid
C
C VERSION:	1.1					DATE: 01-12-98
C MODIFIED: RICHARD MARTIN
C		- PUT UNDER SCCS CONTROL
C
C VERSION:      1.2                                     DATE: 24-03-99
C MODIFIED: MARTIN O'MULLANE
C               - SECOND VERSION
C
C VERSION:      1.3                                     DATE: 17-05-07
C MODIFIED: Allan Whiteford
C               - Updated comments as part of subroutine documentation
C                 procedure.
C
C VERSION  : 1.4
C DATE     : 22-05-2007
C MODIFIED : Martin O'Mullane
C              - Remove unused m-subshell data possibility.
C
C VERSION  : 1.5
C DATE     : 10-05-2010
C MODIFIED : Martin O'Mullane
C              - Effective cross sections, not rates, are written.
C              - Output a stronger warning message.
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER   I4FCTN  , I4UNIT  , I4IDFL  , I4IDFM
C-----------------------------------------------------------------------
      INTEGER   IUNIT   , MXNSHL  , MXNENG   ,
     &          IZR     , IZD     , INDD     ,
     &          NENRGY  , NMIN    , NMAX
      INTEGER   NEMORE  , NWIDTH  , IVALUE   ,
     &          N       , L       , M        , I     , K       ,
     &          IERR
      INTEGER   L1      , L2
C-----------------------------------------------------------------------
      REAL*8    AMDON   , AMREC   , DREN    , fmul
C-----------------------------------------------------------------------
      LOGICAL   LPARMS  , LSETL   , LSETM
C-----------------------------------------------------------------------
      CHARACTER TITLED*80  , SYMBR*2    , SYMBD*2    , CATYP*2
      CHARACTER CLINE*133  , CIZR*2     , CIZD*2     , CINDD*1
      CHARACTER BLANKS*133
      CHARACTER DSFULL*80  , UID*28     , DATE*8     , REALNAME*30
C-----------------------------------------------------------------------
      INTEGER   LFORMA(MXNENG)
C-----------------------------------------------------------------------
      REAL*8    ENRGYA(NENRGY)  , ALPHAA(MXNENG)  , XLCUTA(MXNENG)  ,
     &          PL2A(MXNENG)    , PL3A(MXNENG)    , SIGTA(MXNENG)
      REAL*8    SIGNA(MXNENG,MXNSHL)                           ,
     &          SIGLA(MXNENG,(MXNSHL*(MXNSHL+1))/2)
C-----------------------------------------------------------------------
      DATA      BLANKS /' '/
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
C INITIALIZE VALUES.
C-----------------------------------------------------------------------

      CALL XXNAME(REALNAME)
      CALL XXSLEN(DSFULL,L1,L2)
     
      NWIDTH = 9
      
      IF ( CATYP.EQ.'ME') THEN
          fmul = 1.0
      ELSE
          fmul = 100.0 * 1.384D4
      ENDIF
      
C-----------------------------------------------------------------------
C WRITE OUT DATA
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
C FORM TITLE LINES OF DATA FILE.
C-----------------------------------------------------------------------
      CLINE = BLANKS
      WRITE(CLINE,1001) SYMBR, IZR, SYMBD, IZD, INDD
      IF(LPARMS.AND.(CATYP.EQ.'ME')) THEN
          CLINE(65:70)='LPARMS'
      ELSE
          CLINE(65:70)='      '
      ENDIF
      WRITE(IUNIT,1000) CLINE
      
C-----------------------------------------------------------------------
C START OF WRITE LOOP.
C-----------------------------------------------------------------------
      DO 100 K = 1,NENRGY,NWIDTH
        NEMORE = MIN0(NWIDTH,NENRGY-(K-1))
        IF ( CATYP.EQ.'ME') THEN
            WRITE(IUNIT,1002) NEMORE, NMIN, NMAX,
     &                        (ENRGYA(K-1+I)/1000, I=1,NEMORE)
        ELSE
            WRITE(IUNIT,1015) NEMORE, NMIN, NMAX,
     &                        (ENRGYA(K-1+I)/1000, I=1,NEMORE)
        ENDIF
        WRITE(IUNIT,1008) (ALPHAA(K-1+I), I=1,NEMORE)
        IF(LPARMS.AND.(CATYP.EQ.'ME')) THEN
            WRITE(IUNIT,1003) (LFORMA(K-1+I), I=1,NEMORE)
            WRITE(IUNIT,1009)(XLCUTA(K-1+I), I=1,NEMORE)
            WRITE(IUNIT,1010)(PL2A(K-1+I), I=1,NEMORE)
            WRITE(IUNIT,1011)(PL3A(K-1+I), I=1,NEMORE)
        ENDIF
        IF ( CATYP.EQ.'ME') THEN
            WRITE(IUNIT,1004)  (SIGTA(K-1+I), I=1,NEMORE)
            WRITE(IUNIT,1012)
        ELSE
            WRITE(IUNIT,1013)
     &           (SIGTA(K-1+I)/(fmul*DSQRT(ENRGYA(K-1+I))), 
     &            I=1,NEMORE)
            WRITE(IUNIT,1014)
        ENDIF
        DO 90 N = NMIN,NMAX
          IF ( CATYP.EQ.'ME') THEN
             WRITE(IUNIT,1005) N, (SIGNA(K-1+I,N), I=1,NEMORE)
          ELSE
             WRITE(IUNIT,1005) N, (SIGNA(K-1+I,N)/
     &                             (fmul*DSQRT(ENRGYA(K-1+I))), 
     &                             I=1,NEMORE)
          ENDIF
          IF(LSETL) THEN
              DO 80 L = 0,N-1
                IF ( CATYP.EQ.'ME') THEN
                   WRITE(IUNIT,1006) N, L,
     &                               (SIGLA(K-1+I,I4IDFL(N,L)),
     &                               I=1,NEMORE)
                ELSE
                   WRITE(IUNIT,1006) N, L,
     &                               (SIGLA(K-1+I,I4IDFL(N,L))/
     &                                (fmul*DSQRT(ENRGYA(K-1+I))),
     &                               I=1,NEMORE)
                ENDIF
   80        CONTINUE
          ENDIF
   90   CONTINUE
  100 CONTINUE
      
      
      IF ( CATYP.EQ.'ME')THEN
         WRITE(IUNIT,1016) 'Reprocessed cross-section data',
     &                      DSFULL(1:L2) , REALNAME    , DATE
      ELSEIF ( CATYP.EQ.'TD' ) THEN
         WRITE(IUNIT,2000)
         WRITE(IUNIT,1017) 'Reprocessed thermal donor',
     &                     ' rate coefficient data',
     &                      AMDON  , AMREC  , DREN  ,
     &                      DSFULL(1:L2) , REALNAME    , DATE
      ELSEIF ( CATYP.EQ.'TR' ) THEN
         WRITE(IUNIT,2000)
         WRITE(IUNIT,1018) 'Reprocessed thermal receiver ',
     &                     'rate coefficient data',
     &                      AMDON  , AMREC  , DREN  ,
     &                      DSFULL(1:L2) , REALNAME    , DATE
      ELSEIF ( CATYP.EQ.'TT' ) THEN
         WRITE(IUNIT,2000)
         WRITE(IUNIT,1019) 'Reprocessed thermal/thermal',
     &                     ' rate coefficient data',
     &                      AMDON  , AMREC  ,
     &                      DSFULL(1:L2) , REALNAME    , DATE
      ENDIF
      
C-----------------------------------------------------------------------
 1000 FORMAT(1A133)
 1001 FORMAT(1A2,'+',1I2,5X,1A2,'+',1I2,' (',1I1,')  / ',
     &       'receiver, donor (donor state index)    /        /')
 1002 FORMAT(1I5,16X,'/ number of energies                 '/
     &       1I5,16x,'/ nmin'/
     &       1I5,16x,'/ nmax'/
     &       10X,9F9.2,T92,   ' / energies         (keV/amu)')
 1003 FORMAT(10X,9I9  ,T92,   ' / lforma                    ')
 1004 FORMAT(10X,1P,9D9.2,T92,' / total xsects     (cm2)    ')
 1005 FORMAT(1I3,7X,1P,9D9.2)
 1006 FORMAT(2I3,4X,1P,9D9.2)
 1007 FORMAT(3I3,1X,1P,9D9.2)
 1008 FORMAT(10X,9F9.2,T92,   ' / alpha                     ')
 1009 FORMAT(10X,1P,9D9.2,T92,' / xlcuta                    ')
 1010 FORMAT(10X,1P,9D9.2,T92,' / pl2a                      ')
 1011 FORMAT(10X,1P,9D9.2,T92,' / pl3a                      ')
 1012 FORMAT('  n  l  m ',T92,' / partial xsects.  (cm2)    ')
 1013 FORMAT(10X,1P,9D9.2,T92,' / effective xsect. (cm2)')
 1014 FORMAT('  n  l  m ',T92,' / effective part. xsect.(cm2)')
 1015 FORMAT(1I5,16X,'/ number of temperatures             '/
     &       1I5,16x,'/ nmin'/
     &       1I5,16x,'/ nmax'/
     &       10X,9F9.2,T92,   ' / temperatures     (keV)    ')
 1016 FORMAT('C',109('-')/'C'/
     &       'C   ',A,/'C'/
     &       'C   Source data set: ',A,/
     &       'C   Processing code: ADAS314',/
     &       'C   Producer       : ',1A30,/
     &       'C   Date           : ',1A8,/
     &       'C'/
     &       'C',109('-'))
 1017 FORMAT('C   ',A,A,/'C'/
     &       'C   Donor mass number   : ',F5.2,/
     &       'C   Receiver mass number: ',F5.2,/
     &       'C   Receiver energy (eV): ',1P,D11.3,/
     &       'C'/
     &       'C   Source data set: ',A,/
     &       'C   Processing code: ADAS314',/
     &       'C   Producer       : ',1A30,/
     &       'C   Date           : ',1A8,/
     &       'C'/
     &       'C',109('-'))
 1018 FORMAT('C   ',A,A,/'C'/
     &       'C   Donor mass number   : ',F5.2,/
     &       'C   Receiver mass number: ',F5.2,/
     &       'C   Donor energy    (eV): ',1P,D11.3,/
     &       'C'/
     &       'C   Source data set: ',A,/
     &       'C   Processing code: ADAS314',/
     &       'C   Producer       : ',1A30,/
     &       'C   Date           : ',1A8,/
     &       'C'/
     &       'C',109('-'))
 1019 FORMAT('C   ',A,A,/'C'/
     &       'C   Donor mass number   : ',F5.2,/
     &       'C   Receiver mass number: ',F5.2,/
     &       'C'/
     &       'C   Source data set: ',A,/
     &       'C   Processing code: ADAS314',/
     &       'C   Producer       : ',1A30,/
     &       'C   Date           : ',1A8,/
     &       'C'/
     &       'C',109('-'))
 2000 format('C',109('-')/'C'/,
     &       'C   WARNING: This file contains effective cross sections',
     &       ' (xsec in cm2).',/,
     &       'C            For Te (in eV) the rate is:',/
     &       'C                  rate = xsec * 1.384E6 * sqrt(T)', 
     &       '  (cm3 s-1)',/
     &       'C',/,
     &       'C   Use the data with caution.', /
     &       'C')
C-----------------------------------------------------------------------
      RETURN
      END
