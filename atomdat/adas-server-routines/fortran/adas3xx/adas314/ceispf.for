CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas314/ceispf.for,v 1.2 2004/07/06 12:03:32 whitefor Exp $ Date $Date: 2004/07/06 12:03:32 $
CX
      SUBROUTINE CEISPF( LPEND  ,
     &                   NEDIM  , NDTIN  , EVALS  , 
     &                   TITLE  ,
     &                   AMD    , AMR    ,
     &                   CATYP  , DREN   ,      
     &                   IEXTYP , ITTYP  , ITVAL  , TIN     
     &                 )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C  ****************** FORTRAN77 SUBROUTINE: CEISPF *********************
C
C  PURPOSE: PIPE COMMUNICATIONS TO AND FROM IDL.
C
C  CALLING PROGRAM: ADAS314
C
C  SUBROUTINE:
C  I/O   : (L*4)   LPEND    = .TRUE.  => END ANALYSIS OF CURRENT DATA
C                                        SETS
C                           = .FALSE. => CONTINUE ANALYSIS WITH CURRENT
C                                        DATA SETS
C  INPUT : (I*4)   NEDIM    = MAX. NO. OF COLLISION ENERGIES ALLOWED
C  INPUT : (I*4)   NDTIN    = MAX. NUMBER OF INPUT TEMPERATURES
C  INPUT : (R*8)   EVALS( ) = INPUT DATA FILE: COLLISION ENERGIES(EV/AMU)
C                             1ST DIMENSION: ENERGY INDEX
C  OUTPUT: (C*40)  TITLE    = USER ENTERED GENERAL TITLE FOR PROGRAM RUN
C  OUTPUT: (R*8)   AMD      = DONOR MASS NUMBER
C  OUTPUT: (R*8)   AMR      = RECEIVER MASS NUMBER
C  OUTPUT: (C2)    CATYP    = 'TT' THERMAL/THERMAL (EQUAL TEMP. CASE)
C                             'TR' THERMAL RECEIVER, MONOENERGETIC DONOR
C                             'TD' THERMAL DONOR, MONOENERGETIC RECEIVER
C  OUTPUT: (R*8)   DREN     = DONOR ENERGY    ( 'TR' CASE )
C                             RECEIVER ENERGY ( 'TD' CASE )
C  OUTPUT: (I*4)  IEXTYP    = 1 => SET LOWER ENERGIES TO FIRST POINT IN DATA 
C                           = 2 => SET LOWER ENERGIES TO 0.0
C  OUTPUT: (I*4)   ITTYP    = 1 => UNSPECIFIED
C                           = 2 => UNSPECIFIED
C                           = 3 => UNSPECIFIED
C  OUTPUT: (I*4)   ITVAL    = NUMBER OF INPUT TEMPERATURES (1->12)
C  OUTPUT: (R*8)   TIN()    = USER ENTERED ISPF VALUES -
C                             TEMPERATURES (UNITS: SEE 'IETYP')
C                             DIMENSION: TEMPERATURE INDEX
C          (I*4)   PIPEIN   = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)   PIPEOU   = PARAMETER = UNIT NUMBER FOR INPUT TO PIPE
C          (I*4)   I        = LOOP COUNTER
C          (I*4)   ILOGIC   = RETURN VALUE FROM IDL WHICH IS USED TO
C                             REPRESENT A LOGICAL VARIABLE SINCE IDL DOES
C                             HAVE SUCH DATA TYPES.
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXFLSH    IDL-ADAS CALLS XXFLSH COMMAND TO CLEAR PIPE.
C
C AUTHOR   : Martin O'Mullane
C DATE     : 09/07/98
C VERSION  : 1.1                                        DATE: 09-07-98
C MODIFIED : Martin O'Mullane
C            FIRST VERSION
C
C VERSION:	1.1					DATE: 01-12-98
C MODIFIED: RICHARD MARTIN
C		- PUT UNDER SCCS CONTROL
C
C VERSION:      1.2                                     DATE: 24-03-99
C MODIFIED: MARTIN O'MULLANE
C               - SECOND VERSION
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER    I4UNIT        ,
     &           NEDIM         , NDTIN          
      INTEGER    IEXTYP        , ITTYP        , ITVAL        , IOPT      
C-----------------------------------------------------------------------
      REAL*8     AMD           , AMR           
      REAL*8     TDON          , TREC         , DREN         
C-----------------------------------------------------------------------
      LOGICAL    LPEND 
C-----------------------------------------------------------------------
      CHARACTER  TITLE*40
      CHARACTER  CATYP*2
C-----------------------------------------------------------------------
      REAL*8     TIN(NDTIN)    , EVALS(NEDIM)
C-----------------------------------------------------------------------
      INTEGER    PIPEIN        , PIPEOU       , I             , ILOGIC
      PARAMETER  (PIPEIN = 5, PIPEOU = 6)
C-----------------------------------------------------------------------
C  WRITE OUTPUTS TO PIPE FOR IDL TO READ
C-----------------------------------------------------------------------

      WRITE(PIPEOU,*)NEDIM
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*)NDTIN
      CALL XXFLSH(PIPEOU)
      DO I=1, NEDIM
     	WRITE(PIPEOU,'(E12.4)') EVALS(I)
     	CALL XXFLSH(PIPEOU)
      END DO

C-----------------------------------------------------------------------
C  READ INPUTS FROM PIPE FROM IDL
C-----------------------------------------------------------------------

      READ(PIPEIN, *) ILOGIC
      IF (ILOGIC .EQ. 1 ) THEN
         LPEND = .TRUE.
      ELSE
         LPEND = .FALSE.
      END IF
      READ(PIPEIN,'(A40)') TITLE
      READ(PIPEIN,*) AMD
      READ(PIPEIN,*) AMR
      READ(PIPEIN,*) IOPT
      READ(PIPEIN,*) TDON
      READ(PIPEIN,*) TREC
      READ(PIPEIN,*) IEXTYP
      READ(PIPEIN,*) ITTYP
      READ(PIPEIN,*) ITVAL
      DO  I=1, ITVAL
      READ(PIPEIN,*) TIN(I)
      END DO
      IF (IOPT.EQ.0) THEN
         CATYP = 'ME'
      ELSEIF (IOPT.EQ.1) THEN
         CATYP = 'TT'
      ELSEIF (IOPT.EQ.2) THEN
         CATYP = 'TD'
         DREN  = TREC
      ELSE
         CATYP = 'TR'
         DREN  = TDON
      ENDIF
        
C-----------------------------------------------------------------------
      RETURN
      END
