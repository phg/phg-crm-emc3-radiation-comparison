CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas314/cespf0.for,v 1.1 2004/07/06 12:03:54 whitefor Exp $ Date $Date: 2004/07/06 12:03:54 $
CX
      SUBROUTINE CESPF0( REP  , DSFULL)
      IMPLICIT NONE
C-----------------------------------------------------------------------
C  ****************** FORTRAN77 SUBROUTINE: CESPF0 *********************
C  PURPOSE: TO DISPLAY AND FETCH VALUES FROM IDL OR INFO FILE (STANDARD
C           INPUT) IF BATCH - INPUT DATA SET SPECIFICATIONS.
C  CALLING PROGRAM: ADAS314
C  SUBROUTINE:
C  OUTPUT: (C*3)   REP     = 'YES' => TERMINATE PROGRAM EXECUTION.
C                          = 'NO ' => CONTINUE PROGRAM EXECUTION.
C  OUTPUT: (C*80)  DSFULL  = INPUT DATA SET NAME 
C                            (IN FORM SUITABLE FOR DYNAMIC ALLOCATION)
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C AUTHOR  : Martin O'Mullane
C DATE    : 09/07/98
C VERSION : 1.1                          DATE: 09-07-98
C MODIFIED: Martin O'Mullane
C           FIRST VERSION.
C
C VERSION:	1.1					DATE: 01-12-98
C MODIFIED: RICHARD MARTIN
C		- PUT UNDER SCCS CONTROL
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER     PIPEIN
      PARAMETER ( PIPEIN=5          )
      CHARACTER   REP*3         , DSFULL*80
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C READ IN DATA FROM STANDARD INPUT (IDL OR INFO FILE)
C-----------------------------------------------------------------------
      READ(PIPEIN,'(A)')REP
      READ(PIPEIN,'(A)')DSFULL
      RETURN
      END
