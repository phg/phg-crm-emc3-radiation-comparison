CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas313/cdspf0.for,v 1.1 2004/07/06 12:02:54 whitefor Exp $ Date $Date: 2004/07/06 12:02:54 $
CX
      SUBROUTINE CDSPF0( REP    , DSFULL  )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: CCSPF0 *********************
C
C  PURPOSE: PIPE COMMUNICATION WITH IDL
C
C  CALLING PROGRAM: ADAS312
C
C  SUBROUTINE:
C
C  OUTPUT: (C*3)   REP     = 'YES' => TERMINATE PROGRAM EXECUTION.
C                          = 'NO ' => CONTINUE PROGRAM EXECUTION.
C
C  OUTPUT: (C*80)  DSFULL  = INPUT DATA SET NAME
C
C          (I*4)   PIPEIN  = UNIT NO. FOR OUTPUT TO PIPE
C	   (I*4)   PIPEOU  = UNIT NO. FOR INPUT TO PIPE
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C UNIX-IDL PORT:
C
C AUTHOR:  HARVEY ANDERSON (UNIVERSITY OF STARTHCLYDE)
C
C DATE:    06/05/98 ( FIRST VERSION )
C
C VERSION: 1.1						DATE: 16-03-99
C MODIFIED: RICHARD MARTIN
C		- PUT UNDER SCCS CONTROL.
C
C-----------------------------------------------------------------------
C
      CHARACTER   REP*3         , DSFULL*80
C
C-----------------------------------------------------------------------
C
      LOGICAL     LDSEL
C
C-----------------------------------------------------------------------
C
      INTEGER PIPEIN,PIPEOU
      PARAMETER (PIPEIN=5, PIPEOU=6)
C
C-----------------------------------------------------------------------
C***********************************************************************
C-----------------------------------------------------------------------

C READ FROM IDL PIPE

      READ(PIPEIN,'(A)') REP
      READ(PIPEIN,'(A)') DSFULL


C
C-----------------------------------------------------------------------
C
      RETURN
      END
