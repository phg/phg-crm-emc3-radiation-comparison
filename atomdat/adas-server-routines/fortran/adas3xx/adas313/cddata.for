CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas313/cddata.for,v 1.2 2004/07/06 12:02:25 whitefor Exp $ Date $Date: 2004/07/06 12:02:25 $
CX
       SUBROUTINE CDDATA( TERAY	  ,   NERAY	,  EBRAY      ,IZ      ,
     &			  INFILE  ,   INUNIT    ,  MAXNE      ,MAXTE   ,
     &                    MAXEB   ,   INCOUNT   ,  ITCOUNT    ,IECOUNT ,
     & 			  EBREF   ,   TEREF     ,  NEREF      ,INA     ,
     &			  IEA     ,   ITA       ,  GCRC1      ,GCRC2   ,
     &			  GCRC3   ,   GCRC4     ,  GCRC5      ,GCRC6   ,
     &			  GCRC7   ,   GCRC8     ,  GCRC9      ,NPQN    ,
     &			  LTPQN   ,   NSPIN     ,  BNL        ,NNBNL   ,
     &			  FI      ,   FII       ,  FIII       , CHOICE )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: CDDATA *********************
C
C      	PURPOSE:  TO  FETCH  DATA  FROM  BUNDLE-NL POPULATION 
C		 STRUCTURE FILES OF TYPE ADF26.
C
C      	CALLING ROUTINE : ADAS312
C	
C	INPUT	:
C
C		(CHR)	INFILE		: FILENAME FOR WHICH DATA HAS
C					  TO BE EXTRACTED FROM.
C		(I*4)	INUNIT		: FILENAME STREAM.
C		(I*4)	MAXNE		: MAXIUM NUMBER OF DENSITIES
C		(I*4)	MAXEB		: MAXIMUM NUMBER OF BEAM
C					  ENERGIES.
C		(I*4)	MAXTE		: MAXIMUM NUMBER OF TARGET
C					  TEMPERATURES
C		(I*4)	NPQN		: PRINCIPAL QUANTUM NUMBER.
C		(I*4)	LTPQN		: TOTAL ANGULAR MOMENTUM
C					  QUANTUM NUMBER.
C		(I*4)   CHOICE		: SWITCH TO SELECT COUPLING
C					  OR EMISSION COEFFICIENTS.
C					  CHOICE = 1 EXTRACT COUPLING
C					  COEFFICIENTS.
C					  CHOICE = 2 EXTRACT EMISSION
C					  COEFFICIENTS.
C		
C	OUTPUT  :
C
C		(R*8)   TERAY()		: TARGET TEMPERATURES (eV).
C		(R*8)	NERAY()		: ELECTRON DENSITY ( cm-3).
C		(R*8)	EBRAY()		: NEUTRAL BEAM ENERGY (eV/amu).
C		(R*8)	TEREF		: REFERENCE TEMPERATURE ( eV ).
C		(R*8)	NEREF		: REFERENCE DENSITY ( cm-3).
C		(R*8)	GCRC1()		: CROSS COUPLING COEFF.(cm-3s-1)
C		(R*8)	GCRC2()		: CROSS COUPLING COEFF.(cm-3s-1)
C		(R*8)	GCRC3()		: CROSS COUPLING COEFF.(cm-3s-1)
C		(R*8)	GCRC4()		: CROSS COUPLING COEFF.(cm-3s-1)
C		(R*8)	GCRC5()		: CROSS COUPLING COEFF.(cm-3s-1)
C		(R*8)	GCRC6()		: CROSS COUPLING COEFF.(cm-3s-1)
C		(R*8)	GCRC7()		: CROSS COUPLING COEFF.(cm-3s-1)
C		(R*8)	GCRC8()		: CROSS COUPLING COEFF.(cm-3s-1)
C		(R*8)	GCRC9()		: CROSS COUPLING COEFF.(cm-3s-1)
C		(R*8)	FI()		: CONTRIBUTION TO POPULATING THE
C					  SINGLETS RELATIVE TO THE 
C					  GROUND STATE DUE TO EXCITATION
C		(R*8)	FII()		: CONTRIBUTION TO POPULATING THE
C					  SINGLETS RELATIVE TO THE 2(1)S
C					  METASTABLE.
C		(R*8)	FIII()		: CONTRIBUTION TO POPULATING THE
C					  TRIPLETS RELATIVE TO THE 2(3)S
C					  METASTABLE.
C		(R*8)	BNL()		: SAHA-BOLTZMANN B-FACTOR
C		(R*8)   NNBNL()		: THE PRODUCT OF THE RELATIVE 
C					  POPULATION OF A PARTICULAR
C					  LEVEL TO THE FIRST IONISATION 
C					  STAGE AND THE RECIPROCAL OF
C					  THE SAHA-BOLTZMANN B-FACTOR.
C  
C		(I*4)	INA()		: REFERENCE ARRAY FOR DENSITY.
C		(I*4)	IEA()		: REFERENCE ARRAY FOR ENERGY.
C		(I*4)	ITA()		: REFERENCE ARRAY FOR TEMPERATURE.
C		(I*4)   NSPIN		: NUMBER OF SPIN SYSTEMS.
C		(I*4)	INCOUNT		: NUMBER OF TARGET DENSITIES.
C		(I*4)	IECOUNT		: NUMBER OF BEAM ENERGIES.
C		(I*4)	ITCOUNT		: NUMBER OF TEMPERATURES.
C		(I*4)	IZ		: NUCLEAR CHARGE.
C		
C
C 	ADDITIONAL ROUTINES:
C
C          ROUTINE       SOURCE            BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          CCFIND       ADAS312      ISOLATE DATA IN ADF26 TYPE FILE.
C
C
C
C  	CONTACT :   HARVEY ANDERSON
C	     	    UNIVERSITY OF STRATHCLYDE
C	     	    ANDERSON@PHYS.STRATH.AC.UK	     	    
C
C  	DATE    :   23/4/98  ( FIRST VERSION )
C
C
C VERSION: 1.1						DATE: 16-03-99
C MODIFIED: RICHARD MARTIN
C		- PUT UNDER SCCS CONTROL.
C
C VERSION: 1.2						DATE: 13-10-99
C MODIFIED: Martin O'Mullane
C		- With certain compilers array dimension variables 
C                 must be declared before they are used. Move the
C                 integer declerations before the array definitions.
C
C-----------------------------------------------------------------------
	INTEGER	ITCOUNT	  ,  INUNIT	,  IZ  		,    MAXTE,
     &		MAXNE	  ,  MAXEB	,  INCOUNT	, IECOUNT ,
     &		IT        ,  IN         ,  IE           ,      M  ,
     &		NPQN	  ,  LTPQN      ,  NSPIN        ,CHOICE           
C
	REAL*8	TEREF	  ,  EBREF      , NEREF        ,   TE       ,
     &          NE	  ,  EB         , Z            
C
	REAL*8	TERAY(MAXTE)    , EBRAY(MAXEB)	 , NERAY(MAXNE)     ,     
     &          GCRC1(MAXEB,MAXNE,MAXTE) , GCRC2(MAXEB,MAXNE,MAXTE) , 
     &          GCRC3(MAXEB,MAXNE,MAXTE) , GCRC4(MAXEB,MAXNE,MAXTE) , 
     &          GCRC5(MAXEB,MAXNE,MAXTE) , GCRC6(MAXEB,MAXNE,MAXTE) , 
     &          GCRC7(MAXEB,MAXNE,MAXTE) , GCRC8(MAXEB,MAXNE,MAXTE) , 
     &          GCRC9(MAXEB,MAXNE,MAXTE) , 
     &		BNL(MAXEB,MAXNE,MAXTE,NSPIN+1),
     &          NNBNL(MAXEB,MAXNE,MAXTE,NSPIN+1),
     &          FI(MAXEB,MAXNE,MAXTE,NSPIN+1),
     &		FII(MAXEB,MAXNE,MAXTE,NSPIN+1),
     &          FIII(MAXEB,MAXNE,MAXTE,NSPIN+1)
C
C
	INTEGER INA(MAXNE)   ,  IEA(MAXNE)    ,  ITA(MAXTE) , I	
C
        CHARACTER LINE*132     , INFILE*80    , NLEVEL(5)*2   ,
     &		  LTLEVEL(6)*2      
C
        DATA  NLEVEL/'1','2','3','4','5'/
     & 
        DATA  LTLEVEL/'0','1','2','3','4','5'/
     &
C-----------------------------------------------------------------------
C	
      ITCOUNT=0                                                         
      INCOUNT=0                                                      
      IECOUNT=0
C
      OPEN(UNIT=INUNIT,FILE=INFILE,STATUS='UNKNOWN')
C
 10   READ(INUNIT,'(1A132)',END=200) LINE                  
      IF(LINE(30:31).EQ.'TE') THEN                                    
        READ(LINE,1001) TE                                
        TE=TE/(1.16*10**4)                                      
        CALL CCFIND(TERAY,TE,ITCOUNT,IT)                     
      ELSE                                                           
        GO TO 10                                                      
      ENDIF
C   
 20   READ(INUNIT,'(1A132)') LINE                                          
      IF(LINE(30:31).EQ.'NE') THEN                                     
        READ(LINE,1002) NE                                         
        CALL CCFIND(NERAY,NE,INCOUNT,IN)                                 
      ELSE
        GO TO 20
      ENDIF
C                                                                     
 30   READ(INUNIT,'(1A132)') LINE                                         
      IF(LINE(7:8).EQ.'EH') THEN                                       
        READ(LINE,1003) EB                                              
        CALL CCFIND(EBRAY,EB,IECOUNT,IE)                                  
      ELSE
        GO TO 30
      ENDIF
 40   READ(INUNIT,'(1A132)') LINE                           
      IF(LINE(42:50).EQ.'EFFECTIVE') THEN                                 
        READ(INUNIT,'(1A132)') LINE 
C
        READ(INUNIT,'(1A132)') LINE                                   
        READ(LINE,'(42X,1P,3(1E14.7,1X))') GCRC1(IE,IN,IT) , 
     &              GCRC2(IE,IN,IT), GCRC3(IE,IN,IT)
C
        READ(INUNIT,'(1A132)') LINE                                   
        READ(LINE,'(42X,1P,3(1E14.7,1X))') GCRC4(IE,IN,IT) , 
     &              GCRC5(IE,IN,IT), GCRC6(IE,IN,IT)
C
        READ(INUNIT,'(1A132)') LINE                                   
        READ(LINE,'(42X,1P,3(1E14.7,1X))') GCRC7(IE,IN,IT) , 
     &              GCRC8(IE,IN,IT), GCRC9(IE,IN,IT)
                                   
       ELSE
        GO TO 40
       ENDIF
C
 50     READ(INUNIT,'(1A132)') LINE                                           
      IF(LINE(12:26).EQ.'MULTIPLICITY  1') THEN       
         M=1 
         IF( CHOICE.EQ.2 ) THEN
 51         READ(INUNIT,'(1A132)') LINE                                         
		IF(LINE(12:26).EQ.'MULTIPLICITY  3') THEN
		    GOTO 50
                ENDIF
          IF (LINE(10:10).EQ.NLEVEL(NPQN).AND.
     &       LINE(20:20).EQ.LTLEVEL(LTPQN+1)) THEN             
           READ(LINE,'(27X,3(1E15.7),30X,2(1E15.7))') FI(IE,IN,IT,M),
     &     FII(IE,IN,IT,M) , FIII(IE,IN,IT,M)  , BNL(IE,IN,IT,M),
     &	   NNBNL(IE,IN,IT,M)
          ELSE                                                            
             GO TO 51                                                      
          ENDIF
         ENDIF                                 
      ELSE                                                            
        GO TO 50                                                      
      ENDIF
C 
 55     READ(INUNIT,'(1A132)') LINE                                           
      IF(LINE(12:26).EQ.'MULTIPLICITY  3') THEN             
        M=3  
         IF ( CHOICE.EQ.2 ) THEN
 56        READ(INUNIT,'(1A132)') LINE  
		IF(LINE(12:26).EQ.'MULTIPLICITY  1') THEN
		    GOTO 55
                ENDIF
         IF (LINE(10:10).EQ.NLEVEL(NPQN).AND.
     &    LINE(20:20).EQ.LTLEVEL(LTPQN+1)) THEN             
          READ(LINE,'(27X,3(1E15.7),30X,2(1E15.7))') FI(IE,IN,IT,M),
     &     FII(IE,IN,IT,M) , FIII(IE,IN,IT,M)  , BNL(IE,IN,IT,M),
     &	   NNBNL(IE,IN,IT,M)
          ELSE                                                            
            GO TO 56                                                      
          ENDIF 
         ENDIF                            
      ELSE                                                            
        GO TO 55                                                      
      ENDIF
C
 60   READ(INUNIT,'(1A132)') LINE                                           
      IF(LINE(2:5).EQ.'ZEFF') THEN                                      
        READ(LINE,1005) Z                                            
        IZ = Z
      ELSE                                                            
        GO TO 60                                                      
      ENDIF 
  
                                                      
C-------------------------------------------------------------------
C  SET UP REFERENCE VALUES.
C-------------------------------------------------------------------
      IF ( ITCOUNT .EQ. 1 ) THEN
         TEREF = TE
         NEREF = NE
         EBREF = EB
      ELSE IF ( ITCOUNT .EQ. 2 ) THEN
         NEREF = NE
         EBREF = EB
      ENDIF   
C-------------------------------------------------------------------    
C  GO TO START OF MAIN LOOP                                             
C-------------------------------------------------------------------   
      GO TO 10                                                        
 200  CONTINUE
C
      CLOSE(UNIT=INUNIT)
C 
      RETURN
C-----------------------------------------------------------------------	
 1001 FORMAT(34X,1E8.2)                                                 
 1002 FORMAT(34X,1E8.2)                                                 
 1003 FORMAT(11X,1E8.2)                                                 
 1004 FORMAT(31X,1E11.5,56X,1E11.5)
 1005 FORMAT(8X,F3.1) 
 1006 FORMAT(21X,1E11.5,4X,1E11.5,4X,1E11.5,19X,1E11.5,4X,1E11.5) 
 1007 FORMAT(104X,2E14.7)                                                     
C-----------------------------------------------------------------------	
       END
