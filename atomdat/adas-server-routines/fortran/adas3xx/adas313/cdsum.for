CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas313/cdsum.for,v 1.2 2004/07/06 12:03:06 whitefor Exp $ Date $Date: 2004/07/06 12:03:06 $
CX
       SUBROUTINE CDSUM(  INUNIT   ,INFILE    ,TERAY   ,ITCOUNT,
     &			  INCOUNT  ,IECOUNT   ,TEREF   ,NEREF  ,
     &			  EBREF    ,ITREF     ,IEREF   ,INREF  ,
     &			  ITA       ,IEA      ,INA     ,EBRAY  ,
     &			  NERAY     , MAXTE   ,MAXEB   ,MAXNE  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: CDSUM *********************
C
C      	PURPOSE:  TO  FETCH  A SUMMARY OF THE DATA CONTAINED IN THE
C		  BUNDLE-NL POPULATION STRUCTURE FILES OF TYPE ADF26.
C
C      	CALLING ROUTINE : ADAS313
C	
C	INPUT	:
C
C		(CHR)	INFILE		: FILENAME FOR WHICH DATA HAS
C					  TO BE EXTRACTED FROM.
C		(I*4)	INUNIT		: FILENAME STREAM.
C		(I*4)	MAXNE		: MAXIUM NUMBER OF DENSITIES
C		(I*4)	MAXEB		: MAXIMUM NUMBER OF BEAM
C					  ENERGIES.
C		(I*4)	MAXTE		: MAXIMUM NUMBER OF TARGET
C					  TEMPERATURES
C		
C	OUTPUT  :
C
C		(R*8)   TERAY()		: TARGET TEMPERATURES (eV).
C		(R*8)	NERAY()		: ELECTRON DENSITY ( cm-3).
C		(R*8)	EBRAY()		: NEUTRAL BEAM ENERGY (eV/amu).
C		(R*8)	TEREF		: REFERENCE TEMPERATURE ( eV ).
C		(R*8)	NEREF		: REFERENCE DENSITY ( cm-3).
C		(R*8)   EBREF		: REFERNCE ENERGY ( eV amu-1 ).
C		(I*4)	INA()		: REFERENCE ARRAY FOR DENSITY.
C		(I*4)	IEA()		: REFERENCE ARRAY FOR ENERGY.
C		(I*4)	ITA()		: REFERENCE ARRAY FOR TEMPERATURE.
C		(I*4)	INREF		: ARRAY INDEX OF REFERENCE DENSITY.
C		(I*4)	ITREF		: ARRAY INDEX OF REFERENCE TEMP.
C		(I*4)	IEREF		: ARRAY INDEX OF REFERENCE ENERGY.
C		(I*4)	INCOUNT		: NUMBER OF TARGET DENSITIES.
C		(I*4)	IECOUNT		: NUMBER OF BEAM ENERGIES.
C		(I*4)	ITCOUNT		: NUMBER OF TEMPERATURES.
C		
C
C 	ADDITIONAL ROUTINES:
C
C          ROUTINE       SOURCE            BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          CCFIND       ADAS312      ISOLATE DATA IN ADF26 TYPE FILE.
C          CCFILL       ADAS312	     USED TO ORDER AND SORT ARRAYS. 
C          CCSORT       ADAS312      USED TO ORDER AND SORT ARRAYS.	
C
C
C
C  	CONTACT :   HARVEY ANDERSON
C	     	    UNIVERSITY OF STRATHCLYDE
C	     	    ANDERSON@PHYS.STRATH.AC.UK	     	    
C
C  	DATE    :   07/05/98  ( FIRST VERSION )
C
C VERSION: 1.1						DATE: 16-03-99
C MODIFIED: RICHARD MARTIN
C		- PUT UNDER SCCS CONTROL.
C
C VERSION: 1.2                                    DATE: 13-10-99
C MODIFIED: Martin O'Mullane
C		  - With certain compilers array dimension variables 
C			must be declared before they are used. Move the
C			integer declerations before the array definitions.
C
C-----------------------------------------------------------------------
C
	INTEGER	ITCOUNT	  ,  INUNIT     , I	       ,   MAXTE    ,
     &		MAXNE	  ,  MAXEB	, INCOUNT      ,   IECOUNT  ,
     &		IT        ,  IN         , IE                   
C
	REAL*8	TEREF	  ,  EBREF      , NEREF        ,   TE       ,
     &          NE	  ,  EB                   
C
	REAL*8	TERAY(MAXTE)    , EBRAY(MAXEB)	 , NERAY(MAXNE)        
C
	INTEGER INA(MAXNE)   ,  IEA(MAXNE)    ,  ITA(MAXTE)       ,
     &		ITREF	     ,  IEREF	      ,  INREF 	
C
        CHARACTER LINE*132     , INFILE*80   
C
C-----------------------------------------------------------------------
C	
      ITCOUNT=0                                                         
      INCOUNT=0                                                      
      IECOUNT=0
C
      OPEN(UNIT=INUNIT,FILE=INFILE,STATUS='UNKNOWN')
C
 10   READ(INUNIT,'(1A132)',END=200) LINE                  
      IF(LINE(30:31).EQ.'TE') THEN                                    
        READ(LINE,1001) TE                                
        TE=TE/(1.16*10**4)                                       
        CALL CCFIND(TERAY,TE,ITCOUNT,IT)                       
      ELSE                                                           
        GO TO 10                                                      
      ENDIF
C    
 20   READ(INUNIT,'(1A132)') LINE                                          
      IF(LINE(30:31).EQ.'NE') THEN                                     
        READ(LINE,1002) NE                                         
        CALL CCFIND(NERAY,NE,INCOUNT,IN)                                 
      ELSE
        GO TO 20
      ENDIF
C                                                                     
 30   READ(INUNIT,'(1A132)') LINE                                         
      IF(LINE(7:8).EQ.'EH') THEN                                       
        READ(LINE,1003) EB                                              
        CALL CCFIND(EBRAY,EB,IECOUNT,IE)                                  
      ELSE
        GO TO 30
      ENDIF
C                                                         
C-------------------------------------------------------------------
C  SET UP REFERENCE VALUES.
C-------------------------------------------------------------------
C
      IF ( ITCOUNT .EQ. 1 ) THEN
         TEREF = TE
         NEREF = NE
         EBREF = EB
      ELSE IF ( ITCOUNT .EQ. 2 ) THEN
         NEREF = NE
         EBREF = EB
      ENDIF 
C  
C-------------------------------------------------------------------    
C  GO TO START OF MAIN LOOP                                             
C-------------------------------------------------------------------
C   
      GO TO 10                                                        
 200  CONTINUE
C
      CLOSE(UNIT=INUNIT)
C                                             
C-------------------------------------------------------------------  
C                                                                    
C          SORTS EBRAY,NERAY AND TERAY IN INCREASING ORDER                
C                                                                      
C-------------------------------------------------------------------
C

      CALL CCFILL(IEA,IECOUNT)                                      
      CALL CCSORT(EBRAY,IEA,IECOUNT)                                  
      CALL CCFILL(INA,INCOUNT)                                      
      CALL CCSORT(NERAY,INA,INCOUNT)                                 
      CALL CCFILL(ITA,ITCOUNT)                                       
      CALL CCSORT(TERAY,ITA,ITCOUNT)  
C  
C-------------------------------------------------------------------
C
C     		FIND REFERENCE VALUES IN DATA
C
C-------------------------------------------------------------------
C
       ITREF = 0
       DO I=1,ITCOUNT
         IF (TERAY(I).EQ.TEREF) ITREF = ITA(I)
       END DO
       IF (ITREF.EQ.0) STOP 'REFERENCE TEMPERATURE NOT IN DATA!'
       IEREF = 0
       DO I=1,IECOUNT
         IF (EBRAY(I).EQ.EBREF) IEREF = IEA(I)
       END DO
       IF (IEREF.EQ.0) STOP 'REFERENCE BEAM ENERGY NOT IN DATA!'
       INREF = 0
       DO I=1,INCOUNT
         IF (NERAY(I).EQ.NEREF) INREF = INA(I)
       END DO
       IF (INREF.EQ.0) STOP 'REFERENCE DENSITY NOT IN DATA!' 


      RETURN
C
C-----------------------------------------------------------------------	
 1001 FORMAT(34X,1E8.2)                                                 
 1002 FORMAT(34X,1E8.2)                                                 
 1003 FORMAT(11X,1E8.2)                                                 
 1004 FORMAT(31X,1E11.5,56X,1E11.5)
 1005 FORMAT(8X,F3.1) 
 1006 FORMAT(21X,1E11.5,4X,1E11.5,4X,1E11.5,19X,1E11.5,4X,1E11.5) 
 1007 FORMAT(104X,2E14.7)                                                     
C-----------------------------------------------------------------------	
       END
