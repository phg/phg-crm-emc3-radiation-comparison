CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas313/cdout0.for,v 1.2 2004/07/06 12:02:48 whitefor Exp $ Date $Date: 2004/07/06 12:02:48 $
CX
      SUBROUTINE CDOUT0(  OFILE	,  OUNIT  , OPTION ,   IECOUNT   ,
     &                   INCOUNT,  ITREF  , IEREF  ,   INREF     ,
     &                    MAXTE ,  MAXNE  , MAXEB  ,   GCRC1     ,
     &			  GCRC2 ,  GCRC3  , GCRC4  ,   GCRC5     ,
     &			  GCRC6 ,  GCRC7  , GCRC8  ,   GCRC9     ,
     &		          EBRAY ,  NERAY  , TERAY  ,   DATE      ,
     &			  INFILE,  ITCOUNT, IEA    ,   ITA       ,
     &			  INA   ,  IZ     , TEREF  ,   CHOICE    ,
     &			  RELMET,  FI     , FII    ,   FIII      ,
     &			  BNL   ,  NNBNL  , NSPIN  ,   AVALUE    ,
     &                    MULTI ,  UN     , UL     ,   LN        ,
     &			  LL    )      
	IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  	********* FORTRAN77 SUBROUTINE: XXOUT0 ************
C
C  	PURPOSE:  TO WRITE OUTPUT TO DATA FORMAT ADF21/22 
C		  SPECIFICATIONS.
C	     
C	INPUT :
C		(R*8) GCRC1		: C-R COUPLING COEFFICIENT.
C		(R*8) GCRC2		: C-R COUPLING COEFFICIENT.
C		(R*8) GCRC3		: C-R COUPLING COEFFICIENT.
C		(R*8) GCRC4		: C-R COUPLING COEFFICIENT.
C		(R*8) GCRC5		: C-R COUPLING COEFFICIENT.
C		(R*8) GCRC6		: C-R COUPLING COEFFICIENT.
C		(R*8) GCRC7		: C-R COUPLING COEFFICIENT.
C		(R*8) GCRC8		: C-R COUPLING COEFFICIENT.
C		(R*8) GCRC9		: C-R COUPLING COEFFICIENT.
C		(R*8) EBRAY		: ARRAY CONTAINING THE BEAM 
C				          ENERGIES (eV/amu)
C		(R*8) NERAY		: ARRAY CONTAINING THE
C					  ELECTRON DENSITY (CM-3).
C		(R*8) TERAY		: ARRAY CONTAINING THE 
C					  TARGET TEMPERATURE (eV).
C		(R*8) TEREF		: REFERENCE TEMPERATURE (eV).
C		(R*8) FI		: THE CONTRIBUTION FROM
C					  EXCITATION TO POPULATE A
C					  LEVEL RELATIVE TO THE
C					  GROUND STATE METASTABLE
C		(R*8) FII		: THE CONTRIBUTION FROM 
C					  EXCITATION TO POPULATE A
C					  LEVEL RELATIVE TO THE
C					  2(1)S METASTABLE.
C		(R*8) FIII		: THE CONTRIBUTION FROM
C					  EXCITATION TO POPULATE A
C					  LEVEL RELATIVE TO THE
C					  2(3)S METASTABLE.
C		(R*8) AVALUE		: TRANSITION PROBABILTY.
C		(R*8) BNL		: SAHA-BOLTZMANN B-FACTOR.
C					  THIS QUANTITY IS NOT
C					  DIRECTLY USED IN THE 
C					  ROUTINE BUT IS RETAINED
C					  FOR FURTHER WORK.
C
C		(I*4) OUNIT		: UNIT NUMBER FOR THE OUPUT
C					  STREAM.
C		(I*4) OPTION		: SWITCH USED TO DETERMINE
C					  WHAT GCRC COEFFICIENT HAS
C					  BEEN SELECTED FOR EXTRACTION.
C		(I*4) IECOUNT		: NUMBER OF BEAM ENERGIES.
C		(I*4) INCOUNT		: NUMBER OF TARGET DENSITIES.
C		(I*4) ITREF		: INDEX TO THE REFERENCE
C					  TEMPERATURE.
C		(I*4) IEREF		: INDEX TO THE REFERENCE
C					  BEAM ENERGY.
C		(I*4) INREF		: INDEX TO THE REFERENCE
C					  TARGET DENSITY
C		(I*4) MAXTE		: MAXIMUM NUMBER OF
C					  TEMPERATURES.
C		(I*4) MAXNE		: THE MAXIMUM NUMBER OF
C					  TARGET DENSITIES
C		(I*4) MAXEB		: THE MAXIMUM NUMBER OF
C					  BEAM ENERGIES
C		(I*4) ITCOUNT		: NUMBER OF TARGET TEMPS.
C		(I*4) IEA		: ARRAY CONTAINING THE INDEXES
C					  FOR THE BEAM ENERGY ARRAY.
C		(I*4) ITA		: ARRAY CONTAINING THE INDEXES
C					  FOR THE TEMPERATURE ARRAY.
C		(I*4) INA		: ARRAY CONTAINING THE INDEXES
C					  FOR THE DENSITY ARRAY.
C		(I*4) IZ		: NUCLEAR CHARGE.
C		(I*4) NSPIN		: NUMBER OF SPIN SYSTEM.
C		(I*4) MULTI		: MULTIPLICITY.
C					  MULTI.EQ.0 SINGLET.
C					  MULTI.EQ.1 TRIPLET.
C		(I*4) UN		: UPPER PRINCIPAL QUANTUM
C				          NUMBER.
C		(I*4) UL		: UPPER TOTAL L QUANTUM NOS.
C		(I*4) LN		: LOWER PRINCIPAL QUANTUM
C				          NUMBER.
C		(I*4) LL		: LOWER TOTAL L QUANTUM NOS.
C
C
C	GENERAL :
C
C		(R*8) F1REF		: GENERAL VARIABLE.
C		(R*8) NNREF		: GENERAL VARIABLE.
C		(R*8) ECREF		: GENERAL VARIABLE.
C
C  	CALLING PROGRAM: ADAS313
C
C
C
C
C 	CONTACT : HARVEY ANDERSON
C	    	  UNIVERSITY OF STRATHCLYDE
C	    	  ANDERSON@PHYS.STRATH.AC.UK
C
C 	DATE    : 30/4/98
C		  (FIRST VERSION)
C
C VERSION: 1.1						DATE: 16-03-99
C MODIFIED: RICHARD MARTIN
C		- PUT UNDER SCCS CONTROL.
C
C  VERSION  : 1.2                          
C  DATE     : 23-01-2001
C
C  MODIFIED : Martin O'Mullane  
C             Missing comma in 2013 format statement upset the pgf77
C             compiler.
C
C-----------------------------------------------------------------------
C
      INTEGER   MAXTE       ,   MAXNE       ,   MAXEB           ,
     &		OUNIT       ,   OPTION      ,   IECOUNT         ,
     &		INCOUNT     ,   ITCOUNT     ,   IEREF	        ,
     &  	INREF	    ,   ITREF	    ,   IZ              ,
     &		I	    ,   J           ,   NSPIN
C
      INTEGER  CHOICE       ,   RELMET      ,   MULTI           ,
     &	       UN	    ,	UL	    ,	LN		,
     &	       LL           
C
      INTEGER  IEA(MAXEB)  ,   ITA(MAXTE)  ,    INA(MAXNE)	
C
	REAL*8	TERAY(MAXTE)    , EBRAY(MAXEB)	 , NERAY(MAXNE)     ,     
     &          GCRC1(MAXEB,MAXNE,MAXTE) , GCRC2(MAXEB,MAXNE,MAXTE) , 
     &          GCRC3(MAXEB,MAXNE,MAXTE) , GCRC4(MAXEB,MAXNE,MAXTE) , 
     &          GCRC5(MAXEB,MAXNE,MAXTE) , GCRC6(MAXEB,MAXNE,MAXTE) , 
     &          GCRC7(MAXEB,MAXNE,MAXTE) , GCRC8(MAXEB,MAXNE,MAXTE) , 
     &          GCRC9(MAXEB,MAXNE,MAXTE)                            , 
     &          BNL(MAXEB,MAXNE,MAXTE,NSPIN+1)                      ,         
     &          NNBNL(MAXEB,MAXNE,MAXTE,NSPIN+1)                    , 
     &		FI(MAXEB,MAXNE,MAXTE,NSPIN+1)                       , 
     &		FII(MAXEB,MAXNE,MAXTE,NSPIN+1)                      ,
     &		FIII(MAXEB,MAXNE,MAXTE,NSPIN+1)
 
C
	REAL*8	F1REF      ,  NNREF      , AVALUE     , ECREF       ,
     &          TEREF      
C
      CHARACTER*2  SPEC(10),  DATE*8    , LSPEC(6)    , LTSPEC(6)  
      CHARACTER   OFILE*80 ,  INFILE*80  , USERID*16  , META(3)*8
C
      DATA SPEC/'H ','HE','LI','BE','B ','C ','N ','O ','F ','NE'/
      DATA LSPEC/'s','p','d','f','g','h'/
      DATA LTSPEC/'S','P','D','F','G','H'/
      DATA META/'1s2(1)S ','1s2s(1)S','1s2s(3)S'/

C
C-------------------------------------------------------------------  
C                                                                    
C          SORTS EBRAY,NERAY AND TERAY IN INCREASING ORDER                
C                                                                      
C-------------------------------------------------------------------
C

      CALL CCFILL(IEA,IECOUNT)                                      
      CALL CCSORT(EBRAY,IEA,IECOUNT)                                  
      CALL CCFILL(INA,INCOUNT)                                      
      CALL CCSORT(NERAY,INA,INCOUNT)                                 
      CALL CCFILL(ITA,ITCOUNT)                                       
      CALL CCSORT(TERAY,ITA,ITCOUNT)  
C  
C-------------------------------------------------------------------
C




      OPEN(UNIT=OUNIT,FILE=OFILE,STATUS='UNKNOWN')
C 
      IF ( CHOICE .EQ. 1 ) THEN
      	  IF ( OPTION.EQ.1) THEN 
      		WRITE(OUNIT,2010) IZ,GCRC1(IEREF,INREF,ITREF),
     &		SPEC(IZ),DATE
          ENDIF    
C 
      	  IF ( OPTION.EQ.2) THEN 
      		WRITE(OUNIT,2020) IZ,GCRC2(IEREF,INREF,ITREF),
     &          SPEC(IZ),DATE
          ENDIF
C 
          IF ( OPTION.EQ.3) THEN 
      	        WRITE(OUNIT,2030) IZ,GCRC3(IEREF,INREF,ITREF),
     &          SPEC(IZ),DATE
          ENDIF
C 
          IF ( OPTION.EQ.4) THEN 
      	        WRITE(OUNIT,2040) IZ,GCRC4(IEREF,INREF,ITREF),
     &           SPEC(IZ),DATE
          ENDIF 
C 
          IF ( OPTION.EQ.5) THEN 
      	        WRITE(OUNIT,2050) IZ,GCRC5(IEREF,INREF,ITREF),
     &           SPEC(IZ),DATE
          ENDIF
C 
          IF ( OPTION.EQ.6) THEN 
      	        WRITE(OUNIT,2060) IZ,GCRC6(IEREF,INREF,ITREF),
     &           SPEC(IZ),DATE
          ENDIF
C 
          IF ( OPTION.EQ.7) THEN 
      	        WRITE(OUNIT,2070) IZ,GCRC7(IEREF,INREF,ITREF),
     &           SPEC(IZ),DATE
          ENDIF
C 
          IF ( OPTION.EQ.8) THEN 
      	        WRITE(OUNIT,2080) IZ,GCRC8(IEREF,INREF,ITREF),
     &           SPEC(IZ),DATE
          ENDIF
C 
          IF ( OPTION.EQ.9) THEN 
      	        WRITE(OUNIT,2090) IZ,GCRC9(IEREF,INREF,ITREF),
     &           SPEC(IZ),DATE
          ENDIF
      ENDIF
      IF ( CHOICE.EQ.2 ) THEN
C
C	TEMP STATEMENT H.A 11/8/98
C
C	AVALUE = 1.0
C

           IF ( RELMET.EQ.1 ) THEN
		 F1REF = FI(IEREF,INREF,ITREF,MULTI)
      	         NNREF = NNBNL(IEREF,INREF,ITREF,MULTI)  
      	         ECREF = NNREF * F1REF * AVALUE / NERAY(INREF)
      		 WRITE(OUNIT,2100) IZ,ECREF,SPEC(IZ),DATE
     	   ENDIF
	   IF ( RELMET.EQ.2 ) THEN
 		 F1REF = FII(IEREF,INREF,ITREF,MULTI)
      	         NNREF = NNBNL(IEREF,INREF,ITREF,MULTI) 
                 ECREF = NNREF * F1REF * AVALUE / NERAY(INREF)
		 WRITE(OUNIT,2200) IZ,ECREF,SPEC(IZ),DATE
	   ENDIF
	   IF ( RELMET.EQ.3 ) THEN
	         F1REF = FIII(IEREF,INREF,ITREF,MULTI)
      	         NNREF = NNBNL(IEREF,INREF,ITREF,MULTI) 
                 ECREF = NNREF * F1REF * AVALUE / NERAY(INREF)
		 WRITE(OUNIT,2300) IZ,ECREF,SPEC(IZ),DATE
           ENDIF       
      ENDIF
C 
C-----------------------------------------------------------------------
C       
      WRITE(OUNIT,2004)
      WRITE(OUNIT,2001) IECOUNT,INCOUNT,TEREF
      WRITE(OUNIT,2004)
      WRITE(OUNIT,2002) (EBRAY(I),I=1,IECOUNT)
      WRITE(OUNIT,2002) (NERAY(I),I=1,INCOUNT)
      WRITE(OUNIT,2004)
C   
      IF ( CHOICE.EQ.1 ) THEN
        IF ( OPTION.EQ.1.) THEN 
      	 DO I=1,INCOUNT
          WRITE(OUNIT,2002) (GCRC1(IEA(J),INA(I),ITREF),J=1,IECOUNT)
      	 ENDDO
        ENDIF
C   
        IF ( OPTION.EQ.2.) THEN 
      	 DO I=1,INCOUNT
          WRITE(OUNIT,2002) (GCRC2(IEA(J),INA(I),ITREF),J=1,IECOUNT)
      	 ENDDO
        ENDIF
        IF ( OPTION.EQ.3.) THEN 
      	 DO I=1,INCOUNT
          WRITE(OUNIT,2002) (GCRC3(IEA(J),INA(I),ITREF),J=1,IECOUNT)
      	 ENDDO
        ENDIF
        IF ( OPTION.EQ.4.) THEN 
      	 DO I=1,INCOUNT
          WRITE(OUNIT,2002) (GCRC4(IEA(J),INA(I),ITREF),J=1,IECOUNT)
      	 ENDDO
        ENDIF
        IF ( OPTION.EQ.5.) THEN 
      	 DO I=1,INCOUNT
          WRITE(OUNIT,2002) (GCRC5(IEA(J),INA(I),ITREF),J=1,IECOUNT)
      	 ENDDO
        ENDIF
        IF ( OPTION.EQ.6.) THEN 
      	 DO I=1,INCOUNT
          WRITE(OUNIT,2002) (GCRC6(IEA(J),INA(I),ITREF),J=1,IECOUNT)
      	 ENDDO
        ENDIF
        IF ( OPTION.EQ.7.) THEN 
      	 DO I=1,INCOUNT
          WRITE(OUNIT,2002) (GCRC7(IEA(J),INA(I),ITREF),J=1,IECOUNT)
      	 ENDDO
        ENDIF
        IF ( OPTION.EQ.8.) THEN 
      	 DO I=1,INCOUNT
          WRITE(OUNIT,2002) (GCRC8(IEA(J),INA(I),ITREF),J=1,IECOUNT)
      	 ENDDO
        ENDIF
        IF ( OPTION.EQ.9.) THEN 
      	 DO I=1,INCOUNT
          WRITE(OUNIT,2002) (GCRC9(IEA(J),INA(I),ITREF),J=1,IECOUNT)
      	 ENDDO
        ENDIF
      ENDIF
C
      IF ( CHOICE.EQ.2 ) THEN
	IF ( RELMET.EQ.1 ) THEN

   	   DO I=1,INCOUNT
        	WRITE(OUNIT,2002) ((FI(IEA(J),INA(I),ITREF,MULTI)*
     &                           NNBNL(IEA(J),INA(I),ITREF,MULTI)*
     &                        AVALUE / NERAY(I)) ,J=1,IECOUNT)

      	   ENDDO
        ENDIF
	IF ( RELMET.EQ.2 ) THEN
   	   DO I=1,INCOUNT
        	WRITE(OUNIT,2002) ((FII(IEA(J),INA(I),ITREF,MULTI)*
     &                           NNBNL(IEA(J),INA(I),ITREF,MULTI) *
     &                         AVALUE / NERAY(I)) ,J=1,IECOUNT)


      	   ENDDO
	ENDIF
        IF ( RELMET.EQ.3 ) THEN
   	   DO I=1,INCOUNT
        	WRITE(OUNIT,2002) ((FIII(IEA(J),INA(I),ITREF,MULTI)*
     &                             NNBNL(IEA(J),INA(I),ITREF,MULTI)*
     &                         AVALUE / NERAY(I)) ,J=1,IECOUNT)


      	   ENDDO

	ENDIF
      ENDIF
C 
C-----------------------------------------------------------------------
C
C
      WRITE(OUNIT,2004)
      WRITE(OUNIT,2003) ITCOUNT,EBRAY(IEREF),NERAY(INREF)
      WRITE(OUNIT,2004)
      WRITE(OUNIT,2002) (TERAY(I),I=1,ITCOUNT)
      WRITE(OUNIT,2004)
C
      IF ( CHOICE.EQ.1 ) THEN
        IF ( OPTION.EQ.1) THEN
      	  WRITE(OUNIT,2002) (GCRC1(IEREF,INREF,ITA(I)),I=1,ITCOUNT)
        ENDIF
        IF (OPTION.EQ.2) THEN
      	  WRITE(OUNIT,2002) (GCRC2(IEREF,INREF,ITA(I)),I=1,ITCOUNT)
        ENDIF
        IF (OPTION.EQ.3) THEN
      	  WRITE(OUNIT,2002) (GCRC3(IEREF,INREF,ITA(I)),I=1,ITCOUNT)
        ENDIF
        IF (OPTION.EQ.4) THEN
      	  WRITE(OUNIT,2002) (GCRC4(IEREF,INREF,ITA(I)),I=1,ITCOUNT)
        ENDIF
        IF (OPTION.EQ.5) THEN
      	  WRITE(OUNIT,2002) (GCRC5(IEREF,INREF,ITA(I)),I=1,ITCOUNT)
        ENDIF
        IF (OPTION.EQ.6) THEN
      	  WRITE(OUNIT,2002) (GCRC6(IEREF,INREF,ITA(I)),I=1,ITCOUNT)
        ENDIF
        IF (OPTION.EQ.7) THEN
      	  WRITE(OUNIT,2002) (GCRC7(IEREF,INREF,ITA(I)),I=1,ITCOUNT)
        ENDIF
        IF (OPTION.EQ.8) THEN
      	  WRITE(OUNIT,2002) (GCRC8(IEREF,INREF,ITA(I)),I=1,ITCOUNT)
        ENDIF
        IF (OPTION.EQ.9) THEN
      	  WRITE(OUNIT,2002) (GCRC9(IEREF,INREF,ITA(I)),I=1,ITCOUNT)
        ENDIF
C
	WRITE(OUNIT,2004)
        WRITE(OUNIT,2008)
        WRITE(OUNIT,2008)
        
        WRITE(OUNIT,2006)
        WRITE(OUNIT,2009) INFILE

      ENDIF
C
      IF ( CHOICE.EQ.2 ) THEN
	IF ( RELMET.EQ.1 ) THEN
         WRITE(OUNIT,2002) ((FI(IEREF,INREF,ITA(I),MULTI)*
     &                      NNBNL(IEREF,INREF,ITA(I),MULTI)*
     &                      AVALUE / NERAY(INREF)),I=1,ITCOUNT)

	ENDIF
	IF ( RELMET.EQ.2 ) THEN
	         WRITE(OUNIT,2002) ((FII(IEREF,INREF,ITA(I),MULTI)*
     &                             NNBNL(IEREF,INREF,ITA(I),MULTI)*
     &                     AVALUE / NERAY(INREF)),I=1,ITCOUNT)


	ENDIF
	IF ( RELMET.EQ.3 ) THEN

         	WRITE(OUNIT,2002) ((FIII(IEREF,INREF,ITA(I),MULTI)*
     &                             NNBNL(IEREF,INREF,ITA(I),MULTI)*
     &                     AVALUE / NERAY(INREF)),I=1,ITCOUNT)
	ENDIF
C
      	WRITE(OUNIT,2004) 
 	WRITE(OUNIT,2008)
        WRITE(OUNIT,2008)
        WRITE(OUNIT,2009) INFILE
        WRITE(OUNIT,2007)
        WRITE(OUNIT,2008)
        WRITE(OUNIT,2014) META(RELMET)
	WRITE(OUNIT,2012) UN,LSPEC(UL+1),MULTI,LTSPEC(UL+1),
     &  LN,LSPEC(LL+1),MULTI,LTSPEC(LL+1)
        WRITE(OUNIT,2013) AVALUE
        
      ENDIF
C 
C-----------------------------------------------------------------------
C      
      
      CALL GETENV('USER',USERID)
      WRITE(OUNIT,2008)
      WRITE(OUNIT,2011) USERID
      WRITE(OUNIT,2008)
      WRITE(OUNIT,2004)
      CLOSE(OUNIT)                                                                       C
      RETURN
C
C-----------------------------------------------------------------------
C
 2010 FORMAT(1X,I4,1X,'/GCRC1=',1PE9.3,
     +         1X,'/SPEC=',A2,1X,'/DATE=',A8,1X,'/CODE=ADAS313')

 2020 FORMAT(1X,I4,1X,'/GCRC2=',1PE9.3,
     +         1X,'/SPEC=',A2,1X,'/DATE=',A8,1X,'/CODE=ADAS313')
 2030 FORMAT(1X,I4,1X,'/GCRC3=',1PE9.3,
     +         1X,'/SPEC=',A2,1X,'/DATE=',A8,1X,'/CODE=ADAS313')
 2040 FORMAT(1X,I4,1X,'/GCRC4=',1PE9.3,
     +         1X,'/SPEC=',A2,1X,'/DATE=',A8,1X,'/CODE=ADAS313')
 2050 FORMAT(1X,I4,1X,'/GCRC5=',1PE9.3,
     +         1X,'/SPEC=',A2,1X,'/DATE=',A8,1X,'/CODE=ADAS313')
 2060 FORMAT(1X,I4,1X,'/GCRC6=',1PE9.3,
     +         1X,'/SPEC=',A2,1X,'/DATE=',A8,1X,'/CODE=ADAS313')
 2070 FORMAT(1X,I4,1X,'/GCRC7=',1PE9.3,
     +         1X,'/SPEC=',A2,1X,'/DATE=',A8,1X,'/CODE=ADAS313')
 2080 FORMAT(1X,I4,1X,'/GCRC8=',1PE9.3,
     +         1X,'/SPEC=',A2,1X,'/DATE=',A8,1X,'/CODE=ADAS313')
 2090 FORMAT(1X,I4,1X,'/GCRC9=',1PE9.3,
     +         1X,'/SPEC=',A2,1X,'/DATE=',A8,1X,'/CODE=ADAS313')
C
 2001 FORMAT(1X,I4,1X,I4,1X,'/TREF=',1PE9.3)
 2002 FORMAT(8(1X,1PE9.3),/,8(1X,1PE9.3))
 2003 FORMAT(1X,I4,1X,'/EREF=',1PE9.3,1X,'/NREF=',1PE9.3)
 2004 FORMAT(80('-'))   
 2005 FORMAT(1X,I4,1X,'/ECREF=',1PE9.3,
     +         1X,'/SPEC=',A2,1X,'/DATE=',A8,1X,'/CODE=ADAS313')
 2006 FORMAT('C',5X,'ADAS FILE TYPE : 	ADF21')
 2007 FORMAT('C',5X,'ADAS FILE TYPE : 	ADF22')
 2008 FORMAT('C')
 2009 FORMAT('C',5X,'SOURCE FILE    :',2X,   A60 ) 
 2011 FORMAT('C',5X,'USER ID	     :',2X,   A16)
 2012 FORMAT('C',5X,'TRANSITION     :',2X,'1s',I1,A1,'(',I1,')',
     &A1,'-','1s',I1,A1,'(',I1,')',A1)
 2013 FORMAT('C',5X,'A-VALUE        :',2X,1PE9.3)
 2014 FORMAT('C',5X,'REF. METASTABLE:',2X,A8)
 2100 FORMAT(1X,I4,1X,'/ECMT1=',1PE9.3,
     +         1X,'/SPEC=',A2,1X,'/DATE=',A8,1X,'/CODE=ADAS313')
 2200 FORMAT(1X,I4,1X,'/ECMT2=',1PE9.3,
     +         1X,'/SPEC=',A2,1X,'/DATE=',A8,1X,'/CODE=ADAS313')
 2300 FORMAT(1X,I4,1X,'/ECMT3=',1PE9.3,
     +         1X,'/SPEC=',A2,1X,'/DATE=',A8,1X,'/CODE=ADAS313')

C-----------------------------------------------------------------------
      END 
