CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas313/adas313.for,v 1.2 2004/07/06 10:36:21 whitefor Exp $ Date $Date: 2004/07/06 10:36:21 $
CX
	PROGRAM  ADAS313
C
C-----------------------------------------------------------------------
C
C  		****** FORTRAN77 PROGRAM: ADAS313 	********
C
C  	PURPOSE: INTERROGATING PROGRAM USED TO EXTRACT GENERALISED
C		 COLLISIONAL-RADIATIVE COUPLING COEFFICIENTS AS
C		 WELL AS EFFECIVE EMISSION COEFFICIENTS FOR ARBITRARY
C		 LINE.
C
C	NOTE   : THE PROGRAM OBTAINS TRANSITION PROBABILITIES BY
C		 ACCESSING THE ADF04 TYPE FILE :
C
C	
C                ../adf04/helike/helike_kvi97#he0.dat
C  
C			
C
C	ADDITIONAL ROUTINES:
C
C	    ROUTINE	 SOURCE		    DESCRIPTION
C	  -------------------------------------------------------
C	
C	   CDSUM	ADAS313	     OBTAINS SUMMARY OF INPUT FILE.  
C	   CDDATA	ADAS313	     INTERROGATES INPUT FILE
C	   CCFILL	ADAS312	     ISOLATES EXTRACTED DATA
C	   CCSORT	ADAS312	     SORTS INTO ASCENDING ORDER
C	   CDAVAL	ADAS313	     EXTRACTS A-VALUE FROM ADF04 FILE
C	   CDOUT0	ADAS313	     WRITES OUTPUT TO FILE.
C
C
C	GENERAL :
C
C		(R*8) TEREF	: REFERENCE TEMPERATURE (eV)
C		(R*8) EBREF	: REFERENCE BEAM ENERGY (eV/amu)
C		(R*8) NEREF	: REFERENCE TARGET DENSITY (cm-3)
C		(R*8) AVALUE	: TRANSITION PROBABILTY.
C		(R*8) TERAY	: ARRAY CONTAINING THE 
C			          TARGET TEMPERATURE (eV).
C		(R*8) EBRAY	: ARRAY CONTAINING THE BEAM 
C				  ENERGY RANGE (eV/amu)
C		(R*8) NERAY	: ARRAY CONTAINING THE
C			          ELECTRON DENSITY RANGE (CM-3).
C		(R*8) GCRC1	: C-R COUPLING COEFFICIENT.
C		(R*8) GCRC2	: C-R COUPLING COEFFICIENT.
C		(R*8) GCRC3	: C-R COUPLING COEFFICIENT.
C		(R*8) GCRC4	: C-R COUPLING COEFFICIENT.
C		(R*8) GCRC5	: C-R COUPLING COEFFICIENT.
C		(R*8) GCRC6	: C-R COUPLING COEFFICIENT.
C		(R*8) GCRC7	: C-R COUPLING COEFFICIENT.
C		(R*8) GCRC8	: C-R COUPLING COEFFICIENT.
C		(R*8) GCRC9	: C-R COUPLING COEFFICIENT.
C		(R*8) BNL()	: SAHA-BOLTZMANN B-FACTOR.
C			          THIS QUANTITY IS NOT
C			          DIRECTLY USED IN THE 
C			          PROGRAM BUT IS RETAINED
C			          FOR FURTHER WORK.
C		(R*8) NNBNL()	: THE PRODUCT OF THE RELATIVE 
C			          POPULATION OF A PARTICULAR
C			          LEVEL TO THE FIRST IONISATION 
C				  STAGE AND THE RECIPROCAL OF
C			          THE SAHA-BOLTZMANN B-FACTOR.
C		(R*8) FI()	: THE CONTRIBUTION FROM
C			          EXCITATION TO POPULATE A
C			          LEVEL ON THE SINGLET SIDE
C				  RELATIVE TO THE
C			          GROUND STATE METASTABLE
C		(R*8) FII()	: THE CONTRIBUTION FROM 
C			          EXCITATION TO POPULATE A
C			          LEVEL ON THE SINGLET
C				  SIDE RELATIVE TO THE
C			          2(1)S METASTABLE.
C		(R*8) FIII()	: THE CONTRIBUTION FROM
C			          EXCITATION TO POPULATE A
C			          LEVEL ON THE TRIPLET
C				  SIDE RELATIVE TO THE
C			          2(3)S METASTABLE.
C		(R*8) ZDATA()	: ARRAY CONTAINING THE DERIVED
C			          DATA AS A FUNCTION OF BEAM
C				  ENERGY AND TARGET DENSITY
C				  FOR A SELECTED TEMPERATURE.
C		(R*8) YDATA()	: ARRAY CONTAINING THE
C				  ELECTRON DENSITY (CM-3).
C		(R*8) XDATA()	: ARRAY CONTAINING THE BEAM 
C				  ENERGIES (eV/amu) 
C
C		(I*4) MAXNE	: THE MAXIMUM NUMBER OF
C			          TARGET DENSITIES
C		(I*4) MAXEB	: THE MAXIMUM NUMBER OF
C			          BEAM ENERGIES
C
C		(I*4) MAXTE	: MAXIMUM NUMBER OF
C				  TEMPERATURES.
C		(I*4) NSPIN	: NUMBER OF SPIN SYSTEMS.
C		(I*4) OPTION    : SWITCH USED TO DETERMINE
C			          WHAT GCRC COEFFICIENT HAS
C			          BEEN SELECTED FOR EXTRACTION.
C		(I*4) INUNIT    : UNIT NUMBER FOR INPUT STREAM.
C		(I*4) IZ	: NUCLEAR CHARGE
C		(I*4) INCOUNT	: NUMBER OF TARGET DENSITIES.
C		(I*4) IECOUNT	: NUMBER OF BEAM ENERGIES.
C		(I*4) ITCOUNT	: NUMBER OF TARGET TEMPERATURES.
C		(I*4) OUNIT	: UNIT NUMBER FOR OUTPUT STREAM.
C		(I*4) ITREF	: INDEX TO THE REFERENCE
C			          TEMPERATURE.
C		(I*4) IEREF	: INDEX TO THE REFERENCE
C			          BEAM ENERGY.
C		(I*4) INREF	: INDEX TO THE REFERENCE
C			          TARGET DENSITY
C		(I*4) CHOICE	: SWITCH USED TO DETERMINE IF
C				  GCRC INFORMATION OR EFFECTVE
C				  EMISSION COEFFICIENTS HAVE TO
C				  EXTRACTED FROM THE INPUT FILE.
C				  CHOICE.EQ.1 EXTRACT GCRC DATA.
C				  CHOICE.EQ.2 EXTRACT EMISSION DATA.
C		(I*4) UN	: UPPER PRINCIPAL QUANTUM NUMBER.
C		(I*4) UL	: LOWER TOTAL L QUANTUM NUMBER
C		(I*4) RELMET	: SPECIFIES THE RELATIVE METASTABLE. 
C				    RELMET.EQ.1 1S2(1)S METASTABLE
C				    RELMET.EQ.2 1S2S(1)S METASTABLE
C				    RELMET.EQ.3 1S2S(3)S METASTABLE
C		(I*4) LN	: LOWER PRINCIPAL QUANTUM NUMBER.
C		(I*4) LL	: LOWER TOTAL L QUANTUM NUMBER.
C		(I*4) SDUNIT	: UNIT NUMBER FOR STREAM DEALING WITH
C				  THE ADFO4 TYPE FILE.
C		(I*4) MULTI	: MULTIPLICITY.
C				  MULTI .EQ. 0 SINGLET.
C				  MULTI .EQ. 1 TRIPLET.
C		(I*4) INA	: ARRAY CONTAINING THE INDEXES
C			          FOR THE DENSITY ARRAY.
C		(I*4) IEA	: ARRAY CONTAINING THE INDEXES
C			          FOR THE BEAM ENERGY ARRAY.
C		(I*4) ITA	: ARRAY CONTAINING THE INDEXES
C			          FOR THE TEMPERATURE ARRAY.
C
C
C		(CHR) INFILE	: INPUT FILENAME
C		(CHR) OFILE     : OUTPUT FILENAME
C		(CHR) DATE	: DATE
C		(CHR) SDNAME    : FILENAME FOR THE ADF04 TYPE FILE.
C
C		
C	CONTACT : HARVEY ANDERSON
C		  UNIVERSITY OF STRATHCLYDE
C		  ANDERSON@PHYS.STRATH.AC.UK
C
C	DATE    : 30/4/98
C		 (FIRST VERSION)
C
C VERSION: 1.1						DATE: 16-03-99
C MODIFIED: RICHARD MARTIN
C		- PUT UNDER SCCS CONTROL.
C
C VERSION: 1.2                                                DATE: 13-10-99
C MODIFIED: Martin O'Mullane & Richard Martin
C             - Duplicate definition of SDUNIT. Remove second one.
C		    Modified Read statement for DEC's.
C
C-----------------------------------------------------------------------
C
	INTEGER	MAXNE	  ,  MAXEB      ,  MAXTE         , NSPIN    ,
     &		INUNIT    ,  OUNIT      ,  PIPEIN        , PIPEOUT  ,
     &		SDUNIT        
C
	PARAMETER ( MAXNE = 25 , MAXEB = 25 , MAXTE = 25 , NSPIN =2 ,
     &		    INUNIT = 3 , OUNIT = 4  , PIPEIN = 5 , 
     &		    PIPEOUT = 6, SDUNIT= 2 )
C
	REAL*8	TEREF	  ,  EBREF	, NEREF   ,  AVALUE
C
	REAL*8	TERAY(MAXTE)    , EBRAY(MAXEB)	 , NERAY(MAXNE)     ,     
     &          GCRC1(MAXEB,MAXNE,MAXTE) , GCRC2(MAXEB,MAXNE,MAXTE) , 
     &		GCRC3(MAXEB,MAXNE,MAXTE) , GCRC4(MAXEB,MAXNE,MAXTE) ,
     &		GCRC5(MAXEB,MAXNE,MAXTE) , GCRC6(MAXEB,MAXNE,MAXTE) ,
     &          GCRC7(MAXEB,MAXNE,MAXTE) , GCRC8(MAXEB,MAXNE,MAXTE) , 
     &		GCRC9(MAXEB,MAXNE,MAXTE) 			    , 
     &          BNL(MAXEB,MAXNE,MAXTE,NSPIN+1)                      , 
     &          NNBNL(MAXEB,MAXNE,MAXTE,NSPIN+1)                    , 
     &		FI(MAXEB,MAXNE,MAXTE,NSPIN+1)                       , 
     &		FII(MAXEB,MAXNE,MAXTE,NSPIN+1)                      ,
     &		FIII(MAXEB,MAXNE,MAXTE,NSPIN+1)			    ,
     &		ZDATA(MAXEB,MAXNE)  , YDATA(MAXNE)  , XDATA(MAXEB)
C
	INTEGER	OPTION	  ,  INREF	,  IZ  		, INCOUNT  , 
     &          IECOUNT   ,  ITCOUNT    ,  IEREF        , ITREF              
C
	INTEGER CHOICE	  ,  UN        , UL    ,  RELMET     ,
     &		LN        , LL       , MULTI
C
	INTEGER INA(25)   ,  IEA(25)    ,  ITA(25) , TEXTOUT ,
     &		GRAPHOUT  ,  TVAL	,  IDUMMY	
C
	CHARACTER INFILE*80  , OFILE*80 ,  DATE*8   , SDNAME*80
C
	CHARACTER REP*3     
C
C-----------------------------------------------------------------------
C
C		 SET MACHINE DEPENDENT ADAS CONFIGURATIONS
C
C-----------------------------------------------------------------------
C
	CALL XX0000
C
C-----------------------------------------------------------------------
C
C		 READ ADF04 TYPE FILE NAME
C
C-----------------------------------------------------------------------
C

	READ(PIPEIN,'(A)') SDNAME

C
C-----------------------------------------------------------------------
C
C		 	   GET CURRENT DATE
C
C-----------------------------------------------------------------------
C
	CALL XXDATE( DATE )
1111	CONTINUE
C
C-----------------------------------------------------------------------
C
C		 GET INPUT DATA FILE FROM IDL PIPE
C
C-----------------------------------------------------------------------
C
	CALL CDSPF0 ( REP , INFILE )
C
	IF ( REP.EQ.'YES') THEN
		GOTO 9999
        ENDIF
C
C
C-----------------------------------------------------------------------
C
C        ACCESS ADF26 TYPE FILE AND OBTAIN SUMMARY INFORMATION
C
C-----------------------------------------------------------------------
C
	CALL CDSUM(INUNIT,INFILE,TERAY,ITCOUNT,INCOUNT,IECOUNT,
     &		   TEREF,NEREF,EBREF,ITREF,IEREF,INREF,ITA,
     &		   IEA,INA,EBRAY,NERAY,MAXTE,MAXEB,MAXNE) 
C
C-----------------------------------------------------------------------
C
C          WRITE SUMMARY OF FILE VIA THE IDL PIPE AS OUTPUT
C	
C-----------------------------------------------------------------------
C
      	WRITE(PIPEOUT,*)  TEREF
      	CALL XXFLSH(PIPEOUT)
      	WRITE(PIPEOUT,*)  EBREF
      	CALL XXFLSH(PIPEOUT)
      	WRITE(PIPEOUT,*)  NEREF
      	CALL XXFLSH(PIPEOUT)
      	WRITE(PIPEOUT,*)  TERAY(1)
      	CALL XXFLSH(PIPEOUT)
      	WRITE(PIPEOUT,*)  TERAY(ITCOUNT)
      	CALL XXFLSH(PIPEOUT)
      	WRITE(PIPEOUT,*)  EBRAY(1)
      	CALL XXFLSH(PIPEOUT)
      	WRITE(PIPEOUT,*)  EBRAY(IECOUNT)
      	CALL XXFLSH(PIPEOUT)
      	WRITE(PIPEOUT,*)  NERAY(1)
      	CALL XXFLSH(PIPEOUT)
      	WRITE(PIPEOUT,*)  NERAY(INCOUNT)
      	CALL XXFLSH(PIPEOUT)
      	WRITE(PIPEOUT,*)  ITCOUNT
      	CALL XXFLSH(PIPEOUT)
      	DO I = 1, ITCOUNT
      		WRITE(PIPEOUT,*) TERAY(I)
      		CALL XXFLSH(PIPEOUT)
      	ENDDO	
C
2222	READ(PIPEIN,*) IDONE
	READ(PIPEIN,*) ICANCEL
	READ(PIPEIN,*) IMENU

	IF ( IDONE .EQ. 1 ) THEN
		READ(PIPEIN,*) CHOICE
            IF ( CHOICE.EQ.1 ) THEN
                 READ(PIPEIN,*)  OPTION
            ENDIF
C
            IF ( CHOICE.EQ.2 ) THEN
               READ(PIPEIN,*)   RELMET
               READ(PIPEIN,*)   UN  
               READ(PIPEIN,*)   UL
               READ(PIPEIN,*)   LN
               READ(PIPEIN,*)   LL
               READ(PIPEIN,*)   MULTI
	    ENDIF
	ENDIF
        IF ( ICANCEL .EQ. 1 ) THEN
		GOTO 1111
        ENDIF
	IF ( IMENU .EQ. 1 ) THEN
		GOTO 9999
	ENDIf

C                                             
C-------------------------------------------------------------------  
C                                                                    
C        ACCESS ADF26 TYPE FILE  & EXTRACT RELEVANT INFORMATION             
C                                                                      
C-------------------------------------------------------------------
C
	CALL CDDATA(TERAY,NERAY,EBRAY,IZ,INFILE,INUNIT,MAXNE,
     &              MAXTE,MAXEB,INCOUNT,ITCOUNT,IECOUNT,EBREF,
     &		    TEREF,NEREF,INA,IEA,ITA,GCRC1,GCRC2,GCRC3,
     &		    GCRC4,GCRC5,GCRC6,GCRC7,GCRC8,GCRC9,UN,
     &		    UL,NSPIN,BNL,NNBNL,FI,FII,FIII,CHOICE)

C
C-------------------------------------------------------------------
C
C       OBTAIN TRANSITION PROBABILITY FROM SELECTED ADF04 FILE
C
C-------------------------------------------------------------------
C
	WRITE(PIPEOUT,*)  IDUMMY
      	CALL XXFLSH(PIPEOUT)

	IF ( CHOICE.EQ.2 ) THEN
	  CALL CDAVAL(SDUNIT,SDNAME,UN,UL,LN,LL,MULTI,AVALUE)     	  
        ENDIF
3333	CONTINUE
	READ(PIPEIN,*) IDONE
	READ(PIPEIN,*) ICANCEL
	READ(PIPEIN,*) IMENU
C
	IF ( ICANCEL .EQ. 1 ) THEN
		GOTO 2222
	ENDIF
C
	IF ( IMENU .EQ. 1 ) THEN
		GOTO 9999
	ENDIF
C
	IF ( IDONE .EQ. 1 ) THEN
		READ(PIPEIN,*) TEXTOUT
		IF ( TEXTOUT .EQ. 1 ) THEN
			READ(PIPEIN,'(A)') OFILE
C
C-------------------------------------------------------------------
C
C     		WRITE OUTPUT TO ADF21/22 PRESCRIPTION
C
C-------------------------------------------------------------------
C

           CALL CDOUT0(OFILE,OUNIT,OPTION,IECOUNT,INCOUNT,ITREF,
     &		   IEREF,INREF,MAXTE,MAXNE,MAXEB,GCRC1,GCRC2,
     &		   GCRC3,GCRC4,GCRC5,GCRC6,GCRC7,GCRC8,GCRC9,
     &		   EBRAY,NERAY,TERAY,DATE, INFILE,ITCOUNT,
     &		   IEA,ITA,INA,IZ,TEREF,CHOICE,RELMET,FI,FII,
     &		   FIII,BNL,NNBNL,NSPIN,AVALUE,MULTI,UN,UL,LN,LL)
                ENDIF

               READ(PIPEIN,*) GRAPHOUT
		 IF ( GRAPHOUT .EQ. 1 ) THEN
			READ(PIPEIN,*) TVAL
C
C-------------------------------------------------------------------
C
C     	      INTERPOLATE DATA TO THE REQUIRED TEMPERATURE
C
C-------------------------------------------------------------------
C

 	   CALL CDINTP(CHOICE,OPTION,IECOUNT,INCOUNT,MAXNE,MAXTE,
     &	               MAXEB,ZDATA,GCRC1,GCRC2,GCRC3,GCRC4,GCRC5,
     &	               GCRC6,GCRC7,GCRC8,GCRC9,IEREF,INREF,ITREF,
     &	               TVAL,RELMET,FI,INA,IEA,FII,FIII,NSPIN,
     &	               NNBNL,AVALUE,NERAY,ITA,EBRAY,XDATA,YDATA,
     &		       MULTI )

C 
	    WRITE(PIPEOUT,*) IECOUNT
                    CALL XXFLSH(PIPEOUT)
            WRITE(PIPEOUT,*) INCOUNT
                    CALL XXFLSH(PIPEOUT)
             DO J = 1 ,INCOUNT
                 DO I = 1 , IECOUNT
                   WRITE(PIPEOUT,*) ZDATA(I,J)
                   CALL XXFLSH(PIPEOUT)
              	 ENDDO
             ENDDO
             DO I = 1 , IECOUNT
              	   WRITE(PIPEOUT,*) XDATA(I)
              	   CALL XXFLSH(PIPEOUT)
             ENDDO
             DO I = 1 , INCOUNT
              	   WRITE(PIPEOUT,*) YDATA(I)
              	   CALL XXFLSH(PIPEOUT)
             ENDDO
C	
	ENDIF
C
	READ(PIPEIN,*) IDONE
	READ(PIPEIN,*) ICANCEL
	READ(PIPEIN,*) IMENU
C
	IF ( ICANCEL .EQ. 1 ) THEN
	  GOTO 2222
	ENDIF
C
	IF ( IMENU .EQ. 1 ) THEN
		GOTO 9999
	ENDIF
	
	GOTO 3333

	ENDIF
	
9999   CONTINUE
	STOP
	END
