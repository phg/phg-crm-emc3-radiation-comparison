CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas313/cdintp.for,v 1.1 2004/07/06 12:02:38 whitefor Exp $ Date $Date: 2004/07/06 12:02:38 $
CX
	SUBROUTINE CDINTP(CHOICE    ,OPTION   ,IECOUNT  ,INCOUNT  ,
     &			  MAXNE	    ,MAXTE    ,MAXEB    ,ZDATA    ,
     &			  GCRC1     ,GCRC2    ,GCRC3    ,GCRC4    ,
     &			  GCRC5     ,GCRC6    ,GCRC7    ,GCRC8    ,
     &			  GCRC9     ,IEREF    ,INREF    ,ITREF    ,
     &			  TVAL      ,RELMET   ,FI	,INA      ,
     &			  IEA       ,FII      ,FIII     ,NSPIN    ,
     &			  NNBNL     ,AVALUE   ,NERAY    ,ITA      ,
     &			  EBRAY     ,XDATA    ,YDATA    ,MULTI  )
C
C-----------------------------------------------------------------------
C
C  		****** FORTRAN77 ROUTINE: CDINTP 	********
C
C  	PURPOSE: INTERPOLATE BETWEEN THE EFFECTIVE CLOSE COUPLING
C		 COEFFICIENTS OF EFFECTIVE EMISSION COEFFICIENTS.
C
C
C	CALLING PROGRAM : ADAS313
C			
C	INPUT  :
C
C		(R*8)	FI()		: THE CONTRIBUTION FROM
C					  EXCITATION TO POPULATE A
C					  LEVEL RELATIVE TO THE
C					  GROUND STATE METASTABLE
C		(R*8)	FII()		: THE CONTRIBUTION FROM 
C					  EXCITATION TO POPULATE A
C					  LEVEL RELATIVE TO THE
C					  2(1)S METASTABLE.
C		(R*8)	FIII()		: THE CONTRIBUTION FROM
C					  EXCITATION TO POPULATE A
C					  LEVEL RELATIVE TO THE
C					  2(3)S METASTABLE.
C		(R*8)   GCRC1		: C-R COUPLING COEFFICIENT.
C		(R*8)   GCRC2		: C-R COUPLING COEFFICIENT.
C		(R*8)   GCRC3		: C-R COUPLING COEFFICIENT.
C		(R*8)   GCRC4		: C-R COUPLING COEFFICIENT.
C		(R*8)   GCRC5		: C-R COUPLING COEFFICIENT.
C		(R*8)   GCRC6		: C-R COUPLING COEFFICIENT.
C		(R*8)   GCRC7		: C-R COUPLING COEFFICIENT.
C		(R*8)   GCRC8		: C-R COUPLING COEFFICIENT.
C		(R*8)   GCRC9		: C-R COUPLING COEFFICIENT.
C		(R*8)	NNBNL()		: THE PRODUCT OF THE RELATIVE 
C			                  POPULATION OF A PARTICULAR
C			          	  LEVEL TO THE FIRST IONISATION 
C				  	  STAGE AND THE RECIPROCAL OF
C			          	  THE SAHA-BOLTZMANN B-FACTOR.
C		(R*8)   AVALUE		: TRANSITION PROBABILTY.
C		(R*8)	NERAY()		: ARRAY CONTAINING THE
C					  ELECTRON DENSITY (CM-3).
C		(R*8)	EBRAY()		: ARRAY CONTAINING THE BEAM 
C				          ENERGIES (eV/amu)
C		(I*4)	CHOICE		: SWITCH USED TO INDICATE IF
C					  COUPLING COEFFICIENCTS OR
C					  EMISSION DATA IS BEING
C					  HANDLED. CHOICE EQ 1 THEN
C					  COUPLING DATA. CHOICE EQ 2
C	    				  THEN EMISSION DATA.
C		(I*4)	OPTION		: SIWTCH USED TO SELECT THE
C					  ACTUAL CROSS COUPLING
C					  COEFFICIENT.
C		(I*4)	IECOUNT		: NUMBER OF BEAM ENERGIES.
C		(I*4)	INCOUNT		: NUMBER OF TARGET DENSITIES.
C		(I*4)	MAXNE		: THE MAXIMUM NUMBER OF
C					  TARGET DENSITIES
C		(I*4)	MAXTE		: MAXIMUM NUMBER OF
C					  TEMPERATURES.
C		(I*4)	MAXEB		: THE MAXIMUM NUMBER OF
C					  BEAM ENERGIES
C		(I*4)	IEREF		: INDEX TO THE REFERENCE
C					  BEAM ENERGY.
C		(I*4)	INREF		: INDEX TO THE REFERENCE
C					  TARGET DENSITY
C		(I*4)	ITREF		: INDEX TO THE REFERENCE
C					  TEMPERATURE.
C		(I*4)	TVAL		: ARRAY INDEX OF TEMPERATURE
C					  AT WHICH DATA HAS TO BE
C					  GENERATED.
C		(I*4)   RELMET		: SPECIFIES THE RELATIVE METASTABLE. 
C				    	   RELMET.EQ.1 1S2(1)S METASTABLE
C				    	   RELMET.EQ.2 1S2S(1)S METASTABLE
C				    	   RELMET.EQ.3 1S2S(3)S METASTABLE
C		(I*4)	INA()		: ARRAY CONTAINING THE INDEXES
C					  FOR THE DENSITY ARRAY.
C		(I*4)	IEA()		: ARRAY CONTAINING THE INDEXES
C					  FOR THE BEAM ENERGY ARRAY.
C		(I*4)	ITA()		: ARRAY CONTAINING THE INDEXES
C					  FOR THE TEMPERATURE ARRAY.
C		(I*4)   NSPIN		: NUMBER OF SPIN SYSTEMS.
C
C	OUTPUT :
C
C		(R*8)	ZDATA()		: ARRAY CONTAINING THE DERIVED
C					  DATA AS A FUNCTION OF BEAM
C					  ENERGY AND TARGET DENSITY
C					  FOR A SELECTED TEMPERATURE.
C		(R*8)	YDATA()		: ARRAY CONTAINING THE
C					  ELECTRON DENSITY (CM-3).
C		(R*8)	XDATA()		: ARRAY CONTAINING THE BEAM 
C				          ENERGIES (eV/amu) 
C
C	CONTACT : HARVEY ANDERSON
C		  UNIVERSITY OF STRATHCLYDE
C		  ANDERSON@PHYS.STRATH.AC.UK
C
C	DATE    : 07/5/98
C		 (FIRST VERSION)
C
C
C VERSION: 1.1						DATE: 16-03-99
C MODIFIED: RICHARD MARTIN
C		- PUT UNDER SCCS CONTROL.
C
C-----------------------------------------------------------------------
C
		
	INTEGER	CHOICE	,  OPTION  ,  IECOUNT  ,  INCOUNT  ,
     &		MAXNE   ,  MAXTE   ,  MAXEB    ,  IEREF    ,
     &		INREF   ,  ITREF   ,  TVAL     ,  I	   ,
     &		J       ,  RELMET  ,  NSPIN	
C 
	INTEGER IEA(MAXEB)     ,  INA(MAXNE) ,  ITA(MAXTE) ,
     &		MULTI
C
	REAL*8  AVALUE
C
	REAL*8  ZDATA(MAXEB,MAXNE)       , NERAY(MAXNE)		    ,	
     &          GCRC1(MAXEB,MAXNE,MAXTE) , GCRC2(MAXEB,MAXNE,MAXTE) , 
     &          GCRC3(MAXEB,MAXNE,MAXTE) , GCRC4(MAXEB,MAXNE,MAXTE) , 
     &          GCRC5(MAXEB,MAXNE,MAXTE) , GCRC6(MAXEB,MAXNE,MAXTE) , 
     &          GCRC7(MAXEB,MAXNE,MAXTE) , GCRC8(MAXEB,MAXNE,MAXTE) , 
     &          GCRC9(MAXEB,MAXNE,MAXTE)                            ,
     &          NNBNL(MAXEB,MAXNE,MAXTE,NSPIN+1)                    , 
     &		FI(MAXEB,MAXNE,MAXTE,NSPIN+1)                       , 
     &		FII(MAXEB,MAXNE,MAXTE,NSPIN+1)                      ,
     &		FIII(MAXEB,MAXNE,MAXTE,NSPIN+1), EBRAY(MAXEB)       ,
     &		XDATA(MAXEB)  , YDATA(MAXNE) 

C
C-----------------------------------------------------------------------
C
       IF ( CHOICE .EQ. 1 ) THEN
      	  IF ( OPTION.EQ.1) THEN 
	    DO I = 1 , IECOUNT
	      DO J= 1 , INCOUNT
      		 ZDATA(I,J)=GCRC1(I,J,ITREF)*GCRC1(IEREF,INREF,TVAL)/
     &			    GCRC1(IEREF,INREF,ITREF)
	      ENDDO
	    ENDDO
          ENDIF    
C 
      	  IF ( OPTION.EQ.2) THEN 
	    DO I = 1 , IECOUNT
	      DO J= 1 , INCOUNT
      		 ZDATA(I,J)=GCRC2(I,J,ITREF)*GCRC2(IEREF,INREF,TVAL)/
     &			    GCRC2(IEREF,INREF,ITREF)
	      ENDDO
	    ENDDO
          ENDIF
C 
          IF ( OPTION.EQ.3) THEN
	    DO I = 1 , IECOUNT
	      DO J= 1 , INCOUNT
      		 ZDATA(I,J)=GCRC3(I,J,ITREF)*GCRC3(IEREF,INREF,TVAL)/
     &			    GCRC3(IEREF,INREF,ITREF)
	      ENDDO
	    ENDDO 
          ENDIF
C 
          IF ( OPTION.EQ.4) THEN 
	    DO I = 1 , IECOUNT
	      DO J= 1 , INCOUNT
      		 ZDATA(I,J)=GCRC4(I,J,ITREF)*GCRC4(IEREF,INREF,TVAL)/
     &			    GCRC4(IEREF,INREF,ITREF)
	      ENDDO
	    ENDDO
          ENDIF 
C 
          IF ( OPTION.EQ.5) THEN 
	    DO I = 1 , IECOUNT
	      DO J= 1 , INCOUNT
      		 ZDATA(I,J)=GCRC5(I,J,ITREF)*GCRC5(IEREF,INREF,TVAL)/
     &			    GCRC5(IEREF,INREF,ITREF)
	      ENDDO
	    ENDDO
          ENDIF
C 
          IF ( OPTION.EQ.6) THEN 
	    DO I = 1 , IECOUNT
	      DO J= 1 , INCOUNT
      		 ZDATA(I,J)=GCRC6(I,J,ITREF)*GCRC6(IEREF,INREF,TVAL)/
     &			    GCRC6(IEREF,INREF,ITREF)
	      ENDDO
	    ENDDO
          ENDIF
C 
          IF ( OPTION.EQ.7) THEN 
	    DO I = 1 , IECOUNT
	      DO J= 1 , INCOUNT
      		 ZDATA(I,J)=GCRC7(I,J,ITREF)*GCRC7(IEREF,INREF,TVAL)/
     &			    GCRC7(IEREF,INREF,ITREF)
	      ENDDO
	    ENDDO
          ENDIF
C 
          IF ( OPTION.EQ.8) THEN
	    DO I = 1 , IECOUNT
	      DO J= 1 , INCOUNT
      		 ZDATA(I,J)=GCRC8(I,J,ITREF)*GCRC8(IEREF,INREF,TVAL)/
     &			    GCRC8(IEREF,INREF,ITREF)
	      ENDDO
	    ENDDO
          ENDIF
C 
          IF ( OPTION.EQ.9) THEN
	    DO I = 1 , IECOUNT
	      DO J= 1 , INCOUNT
      		 ZDATA(I,J)=GCRC9(I,J,ITREF)*GCRC9(IEREF,INREF,TVAL)/
     &			    GCRC9(IEREF,INREF,ITREF)
	      ENDDO
	    ENDDO 
          ENDIF
C
      ENDIF
C
      IF ( CHOICE .EQ. 2 ) THEN
C
	IF ( RELMET .EQ. 1 ) THEN
	    DO J = 1 , INCOUNT
	      DO I= 1 , IECOUNT
C      		 ZDATA(I,J)= ((FI(IEA(I),INA(J),ITREF,MULTI)*
C     &                         NNBNL(IEA(I),INA(J),ITREF,MULTI)*
C     &                         AVALUE / NERAY(J)) )*
C     &			     ((FI(IEREF,INREF,ITA(TVAL),MULTI)*
C     &                         NNBNL(IEREF,INREF,ITA(TVAL),MULTI)*
C     &                         AVALUE / NERAY(J)) )
C     &				         /
C     &			     ((FI(IEREF,INREF,ITREF,MULTI)*
C     &                         NNBNL(IEREF,INREF,ITREF,MULTI)*
C     &                         AVALUE / NERAY(J)) )
C
      		 ZDATA(I,J)=  (FI(IEA(I),INA(J),ITREF,MULTI)*
     &                        NNBNL(IEA(I),INA(J),ITREF,MULTI)*
     &			      FI(IEREF,INREF,ITA(TVAL),MULTI)*
     &                        NNBNL(IEREF,INREF,ITA(TVAL),MULTI))
     &				         /
     &			      (FI(IEREF,INREF,ITREF,MULTI)*
     &                        NNBNL(IEREF,INREF,ITREF,MULTI))
	      ENDDO
	    ENDDO 
	ENDIF
C
	IF ( RELMET .EQ. 2 ) THEN
	    DO J = 1 , INCOUNT
	      DO I= 1 , IECOUNT
C      		 ZDATA(I,J)= ((FII(IEA(I),INA(J),ITREF,MULTI)*
C     &                         NNBNL(IEA(I),INA(J),ITREF,MULTI)*
C     &                         AVALUE / NERAY(J)) )*
C     &			     ((FII(IEREF,INREF,ITA(TVAL),MULTI)*
C     &                         NNBNL(IEREF,INREF,ITA(TVAL),MULTI)*
C     &                         AVALUE / NERAY(J)) )
C     &				         /
C     &			     ((FII(IEREF,INREF,ITREF,MULTI)*
C     &                         NNBNL(IEREF,INREF,ITREF,MULTI)*
C     &                         AVALUE / NERAY(J)) )
C
      		 ZDATA(I,J)= (FII(IEA(I),INA(J),ITREF,MULTI)*
     &                       NNBNL(IEA(I),INA(J),ITREF,MULTI)*
     &			     FII(IEREF,INREF,ITA(TVAL),MULTI)*
     &                       NNBNL(IEREF,INREF,ITA(TVAL),MULTI))
     &				         /
     &			     (FII(IEREF,INREF,ITREF,MULTI)*
     &                        NNBNL(IEREF,INREF,ITREF,MULTI))

	      ENDDO
	    ENDDO 
	ENDIF
C
	IF ( RELMET .EQ. 3 ) THEN
	    DO J = 1 , INCOUNT
	      DO I= 1 , IECOUNT
C      		 ZDATA(I,J)= ((FIII(IEA(I),INA(J),ITREF,MULTI)*
C     &                         NNBNL(IEA(I),INA(J),ITREF,MULTI)*
C     &                         AVALUE / NERAY(J)) )*
C     &			     ((FIII(IEREF,INREF,ITA(TVAL),MULTI)*
C     &                         NNBNL(IEREF,INREF,ITA(TVAL),MULTI)*
C     &                         AVALUE / NERAY(J)) )
C     &				         /
C     &			     ((FIII(IEREF,INREF,ITREF,MULTI)*
C     &                         NNBNL(IEREF,INREF,ITREF,MULTI)*
C     &                         AVALUE / NERAY(J)) )
C
      		 ZDATA(I,J)= (FIII(IEA(I),INA(J),ITREF,MULTI)*
     &                       NNBNL(IEA(I),INA(J),ITREF,MULTI)*
     &			     FIII(IEREF,INREF,ITA(TVAL),MULTI)*
     &                       NNBNL(IEREF,INREF,ITA(TVAL),MULTI))
     &				         /
     &			     (FIII(IEREF,INREF,ITREF,MULTI)*
     &                       NNBNL(IEREF,INREF,ITREF,MULTI))

	      ENDDO
	    ENDDO 
	ENDIF
C
      ENDIF
C
	DO I = 1 , IECOUNT
	    XDATA(I) = EBRAY(I)
	ENDDO
C
	DO J = 1 , INCOUNT
	   YDATA(J) = NERAY(J)
	ENDDO
C
	RETURN
C
	END
