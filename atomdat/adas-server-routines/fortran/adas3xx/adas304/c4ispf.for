CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas304/c4ispf.for,v 1.3 2004/07/06 11:51:16 whitefor Exp $ Date $Date: 2004/07/06 11:51:16 $
CX
      SUBROUTINE C4ISPF( MXIT   , MXREQ  , NSITYP , IPAN   ,
     &                   LPEND  , TITLE  , LFSEL  , LOSEL  ,
     &                   TOLVAL , SIFRAC , IFIT   , NREQ   ,
     &                   UBMENG , UTDENS , UTTEMP ,
     &                   BEMNMX , TDMNMX , TTMNMX ,
     &                   NBE    , NTDENS , NTTEMP ,
     &                   BEREF  , TDREF  , TTREF  ,
     &                   BE     , MXBE   , TTEMP  , MXTT   ,
     &                   TDENS  , MXTD   
     &                 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C4ISPF *********************
C
C  PURPOSE: PIPE COMMUNICATIONS WITH IDL
C
C  CALLING PROGRAM: ADAS304
C
C  INPUT : (I*4)   MXIT     = MAXIMUM NUMBER OF STOPPING ION TYPES.
C  INPUT : (I*4)   MXREQ    = MAXIMUM NO. OF USER REQUESTED TRIPLETS OF
C                             ENERGY, TEMPERATURE AND DENSITY.
C  INPUT : (I*4)   NSITYP   = NUMBER OF STOPPING IONS TYPES.
C
C  INPUT : (I*4)   BEMNMX() = RANGE OF MIN AND MAX FOR BEAM ENERGIES.
C                             INDICES ARE AS FOLLOWS:
C                             1 => LOWEST MINIMUM
C                             2 => HIGHEST MINIMUM
C                             3 => LOWEST MAXIMUM
C                             4 => HIGHEST MAXIMUM
C                             DIMENSION: 4
C  INPUT : (I*4)   TDMNMX() = RANGE OF MIN AND MAX FOR TARGET DENSITIES.
C                             INDICES AS FOR 'BEMNMX()'
C                             DIMENSION: 4
C  INPUT : (I*4)   TTMNMX() = RANGE OF MIN AND MAX FOR TARGET TEMPS.
C                             INDICES AS FOR 'BEMNMX()'
C                             DIMENSION: 4
C  INPUT : (I*4)   NBE()    = NUMBER OF BEAM ENERGIES.
C                             DIMENSION: MXIT
C  INPUT : (I*4)   NTDENS() = NUMBER OF TARGET DENSITIES.
C                             DIMENSION: MXIT
C  INPUT : (I*4)   NTTEMP() = NUMBER OF TARGET TEMPERATURES.
C                             DIMENSION: MXIT
C  INPUT : (R*8)   BEREF()  = REFERENCE BEAM ENERGIES.
C                             UNITS: EV/AMU
C                             DIMENSION: MXIT
C  INPUT : (R*8)   TDREF()  = REFERENCE TARGET DENSITIES.
C                             UNITS: CM-3
C                             DIMENSION: MXIT
C  INPUT : (R*8)   TTREF()  = REFERENCE TARGET TEMPERATURES.
C                             UNITS: EV
C                             DIMENSION: MXIT
C  INPUT : (R*8)   BE(,)    = BEAM ENERGIES.
C                             UNITS: EV/AMU
C                             1ST DIMENSION: MXBE
C                             2ND DIMENSION: MXIT
C  INPUT : (I*4)   MXBE     = MAX. NO. OF BEAM ENERGIES WHICH CAN BE
C                             READ FROM INPUT DATA SET.
C  INPUT : (R*8)   TTEMP(,) = TARGET TEMPERATURES.
C                             UNITS: EV
C                             1ST DIMENSION: MXTT
C                             2ND DIMENSION: MXIT
C  INPUT : (I*4)   MXTT     = MAX. NO. OF TARGET TEMPERATURES WHICH CAN
C                             BE READ FROM INPUT DATA SET.
C  INPUT : (R*8)   TDENS(,) = TARGET DENSITIES.
C                             UNITS: CM-3
C                             1ST DIMENSION: MXTD
C                             2ND DIMENSION: MXIT
C  INPUT : (I*4)   MXTD     = MAX. NO. OF TARGET DENSITIES WHICH CAN BE
C                             READ FROM INPUT DATA SET.
C
C  I/O   : (I*4)   IPAN     = ISPF PANEL NUMBER
C  I/O   : (L*4)   LPEND    = FLAGS IF END OF ANALYSIS REQUESTED.
C                             .TRUE. => CONTINUE PANALYSIS WITH CURRENT
C                                        DATA SETS
C                             .FALSE.  => END ANALYSIS OF CURRENT DATA
C                                        SETS
C
C  OUTPUT: (C*40)  TITLE    = ISPF ENTERED GENERAL TITLE FOR PROGRAM RUN
C  OUTPUT: (L*4)   LFSEL    = FLAGS WHETHER TO PERFORM MINIMAX
C                             POLYNOMIAL FITTING.
C                             .TRUE.  => PERFORM FIT.
C                             .FALSE. => DO NOT PERFORM FIT.
C  OUTPUT: (L*4)   LOSEL    = FLAGS WHETHER TO CALCULATE INTERPOLATED
C                             VALUES FOR OUTPUT (ALWAYS SET TO .TRUE.).
C                             .TRUE.  => INTERPOLATE VALUES.
C                             .FALSE. => DO NOT INTERPOLATE VALUES.
C  OUTPUT: (R*8)   TOLVAL   = FRACTIONAL TOLERANCE FOR ACCEPTANCE OF
C                             MINIMAX POLYNOMIAL FIT TO DATA.
C  OUTPUT: (R*8)   SIFRAC() = FRACTION OF EACH STOPPING ION.
C                             DIMENSION: NSITYP
C  OUTPUT: (I*4)   IFIT     = QUANTITY TO FIT AND DISPLAY.
C                             1 => BEAM ENERGY.
C                             2 => TARGET DENSITY.
C                             3 => TARGET TEMPERATURE.
C  OUTPUT: (I*4)   NREQ     = NUMBER OF USER REQUESTED TRIPLETS OF
C                             ENERGY, TEMPERATURE AND DENSITY.
C  OUTPUT: (R*8)   UBMENG() = USER REQESTED BEAM ENERGIES.
C                             DIMENSION: MXREQ
C  OUTPUT: (R*8)   UTDENS() = USER REQESTED TARGET DENSITITES.
C                             DIMENSION: MXREQ
C  OUTPUT: (R*8)   UTTEMP() = USER REQESTED TARGET TEMPERATURES.
C                             DIMENSION: MXREQ
C
C          (I*4)   I        = LOOP INDEX
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          XXFLSH     IDL-ADAS  FLUSH OUT UNIX PIPE
C
C AUTHOR:  JONATHAN NASH  (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    20/12/93
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 16-11-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C VERSION: 1.2                          DATE: 11-12-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - TIDIED UP COMMENTS AND CODE
C
C VERSION: 1.3 				DATE: 28-02-97
C MODIFIED: RICHARD MARTIN
C		- SWAPPED MEANING OF 'TRUE' AND 'FALSE' FOR LPEND
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    MXIT    , MXREQ   , NSITYP  , IPAN    , IFIT    ,
     &           NREQ    , MXBE    , MXTT    , MXTD
      INTEGER    I
      INTEGER    NBE(MXIT)     , NTDENS(MXIT)  ,  NTTEMP(MXIT)
C-----------------------------------------------------------------------
      REAL*8     TOLVAL  
      REAL*8     BEREF(MXIT)    , TDREF(MXIT)    , TTREF(MXIT)
      REAL*8     BEMNMX(4)      , TDMNMX(4)      , TTMNMX(4)
      REAL*8     BE(MXBE,MXIT)  , TDENS(MXTD,MXIT)
      REAL*8     TTEMP(MXTT,MXIT)
C-----------------------------------------------------------------------
      LOGICAL    LPEND   , LFSEL   , LOSEL   
C-----------------------------------------------------------------------
      CHARACTER  TITLE*40
C-----------------------------------------------------------------------
      REAL*8     SIFRAC(NSITYP)  , UBMENG(MXREQ)   , UTDENS(MXREQ)   ,
     &           UTTEMP(MXREQ)
C-----------------------------------------------------------------------
      INTEGER    LOGIC
      INTEGER    PIPEOU         , PIPEIN
      PARAMETER (PIPEOU=6       , PIPEIN=5)
C-----------------------------------------------------------------------
C  WRITE INPUTS TO PIPE
C-----------------------------------------------------------------------
C
      WRITE(PIPEOU,*) MXREQ
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) NBE(1)
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) NTDENS(1)
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) NTTEMP(1)
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) BEREF(1)
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) TDREF(1)
      CALL XXFLSH(PIPEOU)
      WRITE(PIPEOU,*) TTREF(1)
      CALL XXFLSH(PIPEOU)
      DO 2, I=1, 4
          WRITE(PIPEOU,*) BEMNMX(I)
          CALL XXFLSH(PIPEOU)
          WRITE(PIPEOU,*) TDMNMX(I)
          CALL XXFLSH(PIPEOU)
          WRITE(PIPEOU,*) TTMNMX(I)
          CALL XXFLSH(PIPEOU)
2     CONTINUE
      DO 3, I=1, NBE(1)
          WRITE(PIPEOU,*) BE(I,1)
          CALL XXFLSH(PIPEOU)
3     CONTINUE
      DO 4, I=1, NTDENS(1)
          WRITE(PIPEOU,*) TDENS(I,1)
          CALL XXFLSH(PIPEOU)
4     CONTINUE
      DO 5, I=1, NTTEMP(1)
          WRITE(PIPEOU,*) TTEMP(I,1)
          CALL XXFLSH(PIPEOU)
5     CONTINUE
C
C-----------------------------------------------------------------------
C     READ OUPUTS FROM PIPE
C-----------------------------------------------------------------------
C
      READ(PIPEIN, *) LOGIC
      IF (LOGIC.EQ.1) THEN
          LPEND = .FALSE.
      ELSE
          LPEND = .TRUE.
          READ(PIPEIN, '(A)') TITLE
          DO 1, I=1, NSITYP
              READ(PIPEIN,*) SIFRAC(I)
1         CONTINUE
          READ(PIPEIN, *) LOGIC
          IF (LOGIC.EQ.1) THEN
              LFSEL = .TRUE.
              READ(PIPEIN,*) TOLVAL
              TOLVAL = TOLVAL/100.0
	  ELSE
              LFSEL = .FALSE.
          ENDIF
          READ(PIPEIN,*) IFIT
          READ(PIPEIN,*) NREQ
          DO 6, I=1, NREQ
              READ(PIPEIN,*) UBMENG(I)
              READ(PIPEIN,*) UTDENS(I)
              READ(PIPEIN,*) UTTEMP(I)
6         CONTINUE
      ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN
      END
