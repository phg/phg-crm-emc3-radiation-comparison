CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas304/c4mnmx.for,v 1.2 2007/05/17 17:02:25 allan Exp $ Date $Date: 2007/05/17 17:02:25 $
CX
      SUBROUTINE C4MNMX( NSITYP , MXA , NA , A , AMNMX  )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C4MNMX *********************
C
C  PURPOSE: GIVEN A 2D ARRAY 'A' IT RETURNS THE MINIMUM AND MAXIMUM OF
C           BOTH 'A(1,I)' AND 'A(NA(I),I)'
C
C           WHERE I=1,NSITYP. ITS MAIN USE IS TO FIND THE RANGE ON
C           THE MINIMUM AND MAXIMUM OF THE BEAM ENERGIES, AND TARGET
C           DENSITIES AND TEMPERATURES ACROSS THE DIFFERENT ION TYPES.
C
C  CALLING PROGRAM: ADAS304
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  NSITYP    = NUMBER OF DIFFERENT ION TYPES.
C  INPUT : (I*4)  MXA       = SIZE OF FIRST DIMENSION OF 'A(,)'.
C  INPUT : (I*4)  NA()      = NO. OF ENTRIES IN EACH COLUMN OF 'A(,)'.
C                             DIMENSION: NSITYP
C  INPUT : (R*8)  A(,)      = INPUT ARRAY.
C                             1ST DIMENSION: MXA
C                             2ND DIMENSION: NSITYP
C
C  OUTPUT: (R*8)  AMNMX()   = MINIMUM/MAXIMUM VALUES.
C                             INDICES ARE AS FOLLOWS:
C                             1 => MIN OF 'A(1,I)
C                             2 => MAX OF 'A(1,I)
C                             3 => MIN OF 'A(NA(I),I)
C                             4 => MAX OF 'A(NA(I),I)
C                                  WHERE I=1,NSITYP.
C                             DIMENSION: 4
C
C          (I*4)  I         = LOOP INDEX.
C          (I*4)  J         = ARRAY INDEX.
C
C ROUTINES: NONE
C
C AUTHOR:  JONATHAN NASH  (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/87
C          JET EXT. 5183
C
C DATE:    17/12/93
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 16-11-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C VERSION: 1.2                          DATE: 17-05-07
C MODIFIED: Allan Whiteford
C               - Updated comments as part of subroutine documentation
C                 procedure.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    NSITYP  , MXA
      INTEGER    I       , J
C-----------------------------------------------------------------------
      INTEGER    NA(NSITYP)
C-----------------------------------------------------------------------
      REAL*8     AMNMX(4)
C-----------------------------------------------------------------------
      REAL*8     A(MXA,NSITYP)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C FIND RANGE OF MINIMUM ENERGIES.
C-----------------------------------------------------------------------
C
      AMNMX(1) = A(1,1)
      AMNMX(2) = A(1,1)
      DO 1 I = 2 , NSITYP
         IF ( A(1,I) .LT. AMNMX(1) ) AMNMX(1) = A(1,I)
         IF ( A(1,I) .GT. AMNMX(2) ) AMNMX(2) = A(1,I)
    1 CONTINUE
C
C-----------------------------------------------------------------------
C FIND RANGE OF MAXIMUM ENERGIES.
C-----------------------------------------------------------------------
C
      J = NA(1)
      AMNMX(3) = A(J,1)
      AMNMX(4) = A(J,1)
      DO 2 I = 2 , NSITYP
         J = NA(I)
         IF ( A(J,I) .LT. AMNMX(3) ) AMNMX(3) = A(J,I)
         IF ( A(J,I) .GT. AMNMX(4) ) AMNMX(4) = A(J,I)
    2 CONTINUE
C
C-----------------------------------------------------------------------
C
      RETURN
      END
