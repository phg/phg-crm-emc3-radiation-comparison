CX UNIX PORT - SCCS info: Module @(#)c4out0.for 1.3 Date 03/28/97
CX
      SUBROUTINE C4OUT0( IWRITE , MXREQ  , MXDEG  , DATE   ,
     &                   TITLE  , NSITYP , DSNIN  , TSYM   ,
     &                   ITZ    , SIFRAC , NREQ   , UBMENG ,
     &                   UTDENS , UTTEMP , BSTOT  , LINTOT ,
     &                   BSION  , LINION , LFSEL  , IFIT   ,
     &                   NCOEF  , FCOEF  , FINFO  , CADAS  ,
     &                   LINE1  , DATASET, FACT1  , TFLAG )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C4OUT0 *********************
C
C  PURPOSE: WRITES TEXT OUTPUT TO FILE FOR ADAS304.
C
C  CALLING PROGRAM: ADAS304
C
C  INPUT : (I*4)   IWRITE   = UNIT NO. FOR OUTPUT OF TEXT RESULTS.
C  INPUT : (I*4)   MXREQ    = MAX NO. OF USER REQUESTED TRIPLETS OF
C                             ENERGY, TEMPERATURE AND DENSITY.
C  INPUT : (I*4)   MXDEG    = MAX NO. DEGREES FOR MINIMAX FIT.
C  INPUT : (C*8)   DATE     = DATE STRING.
C  INPUT : (C*40)  TITLE    = ISPF ENTERED GENERAL TITLE FOR PROGRAM RUN
C  INPUT : (I*4)   NSITYP   = NUMBER OF STOPPING IONS TYPES.
C  INPUT : (C*80)  DSNIN()  = INPUT DATA SET NAMES (FULL MVS DSN).
C                             (IN FORM SUITABLE FOR DYNAMIC ALLOCATION)
C                             DIMENSION: NSITYP
C  INPUT : (C*2)   TSYM()   = TARGET ION ELEMENT SYMBOL.
C                             DIMENSION: NSITYP
C  INPUT : (I*4)   ITZ()    = TARGET ION CHARGE.
C                             DIMENSION: NSITYP
C  INPUT : (R*8)   SIFRAC() = FRACTION OF EACH STOPPING ION.
C                             DIMENSION: NSITYP
C  INPUT : (I*4)   NREQ     = NUMBER OF USER REQUESTED TRIPLETS OF
C                             ENERGY, TEMPERATURE AND DENSITY.
C  INPUT : (R*8)   UBMENG() = USER REQESTED BEAM ENERGIES.
C                             DIMENSION: NREQ
C  INPUT : (R*8)   UTDENS() = USER REQESTED TARGET DENSITITES.
C                             DIMENSION: NREQ
C  INPUT : (R*8)   UTTEMP() = USER REQESTED TARGET TEMPERATURES.
C                             DIMENSION: NREQ
C  INPUT : (R*8)   BSTOT()  = TOTAL BEAM STOPPING COEFFICIENTS.
C                             DIMENSION: NREQ
C  INPUT : (L*4)   LINTOT() = FLAGS IF INTERPOLATION OR EXTRAPOLATION
C                             USED IN SPLINING ROUTINE.
C                             .TRUE.  => ONLY INTERPOLATION USED.
C                             .FALSE. => AT LEAST ONE OF ENERGY, DENISTY
C                                        OR TEMPERATURE EXTRAPOLATED.
C                             DIMENSION: NREQ
C  INPUT : (R*8)   BSION(,) = BEAM STOPPING COEFFICIENTS FOR INDIVIDUAL
C                             IONS.
C                             1ST DIMENSION: MXREQ
C                             2ND DIMENSION: NSITYP
C  INPUT : (L*4)   LINION(,)= FLAGS IF INTERPOLATION OR EXTRAPOLATION
C                             USED IN SPLINING ROUTINE.
C                             .TRUE.  => ONLY INTERPOLATION USED.
C                             .FALSE. => AT LEAST ONE OF ENERGY, DENISTY
C                                        OR TEMPERATURE EXTRAPOLATED.
C                             1ST DIMENSION: MXREQ
C                             2ND DIMENSION: NSITYP
C  INPUT : (L*4)   LFSEL    = FLAGS MINIMAX POLYNOMIAL FITTING.
C                             .TRUE.  => FIT PERFORMED.
C                             .FALSE. => NO FIT.
C  INPUT : (I*4)   IFIT     = QUANTITY TO FIT AND DISPLAY.
C                             1 => BEAM ENERGY.
C                             2 => TARGET DENSITY.
C                             3 => TARGET TEMPERATURE.
C  INPUT : (I*4)   NCOEF()  = NUMBER OF TAYLOR COEFFICIENTS FROM FIT.
C                             DIMENSION: NSITYP
C  INPUT : (R*8)   FCOEF(,) = TAYLOR COEFFICIENTS FROM FIT.
C                             1ST DIMENSION: MXDEG
C                             2ND DIMENSION: NSITYP
C  INPUT : (C*80)  FINFO()  = INFORMATION STRING FROM FIT.
C                             INDICES 1 TO 'NSITYP' ARE FOR THE
C                             INDIVIDUAL IONS AND 'NSITYP+1' FOR THE
C                             TOTAL.
C                             DIMENSION: NSITYP+1
C  INPUT : I*4     TFLAG    = DETERMINES TITLE TYPE IN OUTPUT
C                             0 => 'BEAM STOPPING ... '
C                             1 => 'BEAM EMISSION ... '
C                             2 => 'BEAM DENSITY ... '
C                             3 => 'CANNOT TELL ... '
C
C          (I*4)   LEN      = LENGTH OF ION NAME STRINGS.
C          (I*4)   L1       = LENGTH OF 'LINE1' STRING.
C          (I*4)   L2       = LENGTH OF 'LINE2' STRING.
C          (I*4)   I        = LOOP / ARRAY INDEX.
C          (I*4)   J        = LOOP / ARRAY INDEX.
C
C          (C*80)  CADAS    = ADAS HEADER: INCLUDES RELEASE, PROGRAM,
C                             TIME.
C          (C*133) LINE1    = LINE OF TEXT.
C          (C*133) LINE2    = LINE OF TEXT.
C          (C*100) TITLG    = TITLE FOR GRAPH.
C          (C*4)   SION     = ION NAME STRING.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          XXSION     ADAS      RETURNS ION STRING.
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/87
C          JET EXT. 5183
C
C DATE:    23/12/93
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 16-11-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C VERSION: 1.2                          DATE: 11-12-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - TIDIED UP COMMENTS AND CODE
C
C VERSION: 1.3                          DATE: 12-03-97
C MODIFIED: RICHARD MARTIN
C               - ADDED 'TFLAG' AND 'BEAMEVENT'
C
C VERSION: 1.4                          DATE: 27-05-2004
C MODIFIED: Martin O'Mullane
C               - The output units are eV/amu for beam energy
C                 not eV. Change the label used.
C
C VERSION : 1.5                          
C DATE    : 10-12-2004
C MODIFIED: Martin O'Mullane
C               - Add beam density and unknown as TFLAG options.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    IWRITE  , MXREQ   , MXDEG   , NSITYP  , NREQ    ,
     &           IFIT    , TFLAG
      INTEGER    LEN     , L1      , L2      , I       , J  
C-----------------------------------------------------------------------
      LOGICAL    LFSEL
C-----------------------------------------------------------------------
      CHARACTER  DATE*8     , TITLE*40   , BEAMEVENT*13
      CHARACTER  CADAS*80   , LINE1*133  , LINE2*133 , SION*4
      CHARACTER  DATASET*44
C-----------------------------------------------------------------------
      INTEGER    ITZ(NSITYP)     , NCOEF(NSITYP)
C-----------------------------------------------------------------------
      REAL*8     SIFRAC(NSITYP)  , UBMENG(NREQ)    , UTDENS(NREQ)    ,
     &           UTTEMP(NREQ)    , BSTOT(NREQ)     , FACT1          
C-----------------------------------------------------------------------
      LOGICAL    LINTOT(NREQ)
C-----------------------------------------------------------------------
      CHARACTER  DSNIN(NSITYP)*80    , TSYM(NSITYP)*2      ,
     &           FINFO(NSITYP+1)*80  
C-----------------------------------------------------------------------
      REAL*8     BSION(MXREQ,NSITYP)      , FCOEF(MXDEG+1,NSITYP+1)
C-----------------------------------------------------------------------
      LOGICAL    LINION(MXREQ,NSITYP)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C GET ADAS HEADER. - NOW DONE IN C4SPF1.FOR
C-----------------------------------------------------------------------
C
CX    CALL XXADAS( CADAS )
C
C-----------------------------------------------------------------------
C WRITE TITLE INFORMATION.
C-----------------------------------------------------------------------
C
      IF (TFLAG.EQ.0) THEN
         BEAMEVENT='BEAM STOPPING'
      ELSEIF (TFLAG.EQ.1) THEN
         BEAMEVENT='BEAM EMISSION'
      ELSEIF (TFLAG.EQ.2) THEN
         BEAMEVENT='BEAM EXC POPN'
      ELSE
         BEAMEVENT='UNKNOWN DATA '
      ENDIF
      
      WRITE(IWRITE,1000) CADAS(2:80)
      WRITE(IWRITE,1001) BEAMEVENT, DATE
      WRITE(IWRITE,1002) TITLE
      WRITE(IWRITE,1003) BEAMEVENT
C
C-----------------------------------------------------------------------
C WRITE SUMMARY OF USER INPUT.
C-----------------------------------------------------------------------
C
      WRITE(IWRITE,1004) DATASET
      WRITE(IWRITE,1005) LINE1
C
C-----------------------------------------------------------------------
C WRITE TABLE FOR ION FRACTIONS.
C-----------------------------------------------------------------------
C
      WRITE(IWRITE,1006)
      DO 2 I = 1 , NSITYP
         WRITE(IWRITE,1007) TSYM(I) , ITZ(I) , SIFRAC(I)
    2 CONTINUE
C
C-----------------------------------------------------------------------
C WRITE TABLE FOR TOTAL COEFFICIENT.
C-----------------------------------------------------------------------
C
      WRITE(IWRITE,1008)BEAMEVENT,BEAMEVENT
      DO 3 I = 1 , NREQ
         IF ( LINTOT(I) ) THEN
            WRITE(IWRITE,1009) UBMENG(I) , UTDENS(I) , UTTEMP(I) ,
     &                         BSTOT(I) , ' '
         ELSE
            WRITE(IWRITE,1009) UBMENG(I) , UTDENS(I) , UTTEMP(I) ,
     &                         BSTOT(I) , '*'
         ENDIF
    3 CONTINUE
      WRITE(IWRITE,1010)BEAMEVENT
C
C-----------------------------------------------------------------------
C WRITE FITTING INFORMATION FOR TOTAL COEFFICIENT.
C-----------------------------------------------------------------------
C
      IF (LFSEL) THEN
C
         IF ( IFIT .EQ. 1 ) THEN
            WRITE(IWRITE,1011)BEAMEVENT
         ELSE IF ( IFIT .EQ. 2 ) THEN
            WRITE(IWRITE,1012)BEAMEVENT
         ELSE IF ( IFIT .EQ. 3 ) THEN
            WRITE(IWRITE,1013)BEAMEVENT
         ENDIF
         WRITE(IWRITE,1014) 'TOTAL', BEAMEVENT
C
         I = NSITYP + 1
         DO 4 J = 1 , NCOEF(I) , 2
            IF ( J .EQ. NCOEF(I) ) THEN
               WRITE(IWRITE,1015) J , FCOEF(J,I)
            ELSE
               WRITE(IWRITE,1016) J , FCOEF(J,I) , J+1 , FCOEF(J+1,I)
            ENDIF
    4    CONTINUE
C
         WRITE(IWRITE,1017) FINFO(I)
C
      ENDIF
C
C-----------------------------------------------------------------------
C WRITE TABLE FOR INDIVIDUAL IONS.
C-----------------------------------------------------------------------
C
      L1 = 1
      L2 = 2
      DO 5 I = 1 , NSITYP
C
         CALL XXSION( TSYM(I) , ITZ(I) , SION , LEN )
         IF ( LEN .EQ. 2 ) THEN
            WRITE(LINE1(L1:),1018) SION
         ELSE IF ( LEN .EQ. 3 ) THEN
            WRITE(LINE1(L1:),1019) SION
         ELSE IF ( LEN .EQ. 4 ) THEN
            WRITE(LINE1(L1:),1020) SION
         ENDIF
         L1 = L1 + 10
C
         WRITE(LINE2(L2-1:),1021) SIFRAC(I)
         L2 = L2 + 10
C
    5 CONTINUE
C
      WRITE(IWRITE,1022) BEAMEVENT, BEAMEVENT, LINE1(1:L1-1) , 
     &     LINE2(1:L2-1)
      DO 6 I = 1 , NREQ
         WRITE(LINE1,1023) UBMENG(I) , UTDENS(I) ,UTTEMP(I) 
         L1 = 28
         DO 7 J = 1 , NSITYP
            IF ( LINION(I,J) ) THEN
               WRITE(LINE1(L1:),1024) BSION(I,J) , ' '
            ELSE
               WRITE(LINE1(L1:),1024) BSION(I,J) , '*'
            ENDIF
            L1 = L1 + 10
    7    CONTINUE
         WRITE(IWRITE,1025) LINE1(1:L1-1)
    6 CONTINUE
      WRITE(IWRITE,1026)BEAMEVENT
C
C-----------------------------------------------------------------------
C WRITE FITTING INFORMATION FOR INDIVIDUAL IONS.
C-----------------------------------------------------------------------
C
      IF (LFSEL) THEN
C
         IF ( IFIT .EQ. 1 ) THEN
            WRITE(IWRITE,1011)
         ELSE IF ( IFIT .EQ. 2 ) THEN
            WRITE(IWRITE,1012)
         ELSE IF ( IFIT .EQ. 3 ) THEN
            WRITE(IWRITE,1013)
         ENDIF
C
         DO 8 I = 1 , NSITYP
C
            CALL XXSION( TSYM(I) , ITZ(I) , SION , LEN )
            WRITE(IWRITE,1014) SION(1:LEN)
C
            DO 9 J = 1 , NCOEF(I) , 2
               IF ( J .EQ. NCOEF(I) ) THEN
                  WRITE(IWRITE,1015) J , FCOEF(J,I)
               ELSE
                 WRITE(IWRITE,1016) J , FCOEF(J,I) , J+1 ,
     &                              FCOEF(J+1,I)
               ENDIF
    9       CONTINUE
C
            WRITE(IWRITE,1017) FINFO(I)
C
    8    CONTINUE
C
      ENDIF
C
C-----------------------------------------------------------------------
C
 1000 FORMAT( 1H  , A79 )
 1001 FORMAT( 1H  , 18( '*' ) , ' TABULAR OUTPUT FROM ',A13,
     &        ' COEFFICIENT INTERROGATION PROGRAM: ADAS304 - DATE: ' ,
     &        A8 , 1X , 19( '*' ) / )
 1002 FORMAT( 1H  , 19( '-' ) , 1X , A40 , 1X , 19( '-' ) // )
 1003 FORMAT( 1H  , A13,' COEFFICIENTS AS A FUNCTION OF ' ,
     &        'BEAM ENERGY AND TARGET DENSITY AND TEMPERATURE' /
     &        1H , '--------------------------------------------' ,
     &        '----------------------------------------------' /
     &        1H , 21X , 'DATA GENERATED USING PROGRAM: ADAS304' /
     &        1H , 21X , '-------------------------------------' // )
 1004 FORMAT( 1H  , 'INPUT DATA SET: ', A / )
 1005 FORMAT( '       MEMBERS: ', A)
 1006 FORMAT( // 1H  , 27X , '------------------------' /
     &        1H , 27( '-' ) , ' STOPPING ION FRACTIONS ' ,
     &             28( '-' ) /
     &        1H , 28X , '------------------------' //
     &        1H , ' ION  CHARGE  FRACTION' /
     &        1H , '----------------------' )
 1007 FORMAT( 1H , 2X , A2 , 4X , I2 , 6X , F5.3 )
 1008 FORMAT( /// 1H  , 22X , '----------------------------------' /
     &        1H , 22( '-' ) , ' TOTAL ',A13,' COEFFICIENTS ' ,
     &             23( '-' ) /
     &        1H , 22X , '----------------------------------' //
     &        1H , '  BEAM    TARGET   TARGET  ',A13, /
     &        1H , ' ENERGY   DENSITY   TEMP.   COEFFICIENT ' /
     &        1H , '(eV/amu)  (cm-3)    (eV)     (cm3 s-1)  ' /
     &        1H , '----------------------------------------' )
 1009 FORMAT( 1H , 1P , D8.2 , 2( 1X , D8.2 ) , 2X , D9.2 , A1 )
 1010 FORMAT( /1H , 89( '-' ) /
     &        1H , 'NOTE: * => ',A13,' COEFFICIENT ' ,
     &        'EXTRAPOLATED FOR ENERGY/DENSITY/TEMPERATURE TRIPLET.' /
     &        1H , 89( '-' ) )
 1011 FORMAT( // 1H  ,'MINIMAX POLYNOMIAL FIT TO BEAM ENERGIES ' ,
     &                '- TAYLOR COEFFICIENTS:' /
     &        1H , 79( '-' ) /
     &        1H , '- LOG10(',A13,' COEFFT. <CM3 S-1>) ',
     &             'VERSUS LOG10(BEAM ENERGY <EV>) -' /
     &        1H , 79( '-' ) )
 1012 FORMAT( // 1H  ,'MINIMAX POLYNOMIAL FIT TO TARGET DENSITIES ' ,
     &                '- TAYLOR COEFFICIENTS:' /
     &        1H , 79( '-' ) /
     &        1H , '- LOG10(',A13,' COEFFT. <CM3 S-1>) ',
     &             'VERSUS LOG10(TARGET DENSITY <CM-3>) -' /
     &        1H , 79( '-' ) )
 1013 FORMAT( // 1H  ,'MINIMAX POLYNOMIAL FIT TO TARGET TEMPERATURES' ,
     &                '- TAYLOR COEFFICIENTS:' /
     &        1H , 79( '-' ) /
     &        1H , '- LOG10(',A13,' COEFFT. <CM3 S-1>) ',
     &             'VERSUS LOG10(TARGET TEMPERATURE <EV>) -' /
     &        1H , 79( '-' ) )
 1014 FORMAT( / 1H , 'FIT FOR ' , A , ' ',A13,' COEFFICIENTS:' / )
 1015 FORMAT( 12X , 'A(' , I2 , ') = ' , 1P , D17.9 )
 1016 FORMAT( 2( 12X ,'A(' , I2 , ') = ' , 1P , D17.9 ) )
 1017 FORMAT( / 1H , A79 / 1H , 79( '-' ) )
 1018 FORMAT( 4X , A2 , 4X )
 1019 FORMAT( 4X , A3 , 3X )
 1020 FORMAT( 3X , A4 , 3X )
 1021 FORMAT( 1X , '(' , F4.2 , ')' , 1X )
 1022 FORMAT( /// 1H  , 18X ,
     &        '-------------------------------------------' /
     &        1H , 18( '-' ) ,
     &        ' INDIVIDUAL ION ',A13,' COEFFICIENTS ' ,
     &        18( '-' ) /
     &        1H , 18X ,
     &        '-------------------------------------------' //
     &        1H , '  BEAM    TARGET   TARGET ' , 1X , 31( '-' ) ,
     &        ' ',A13,' COEFFICIENT (CM3 S-1) ' , 32( '-' ) /
     &        1H , ' ENERGY   DENSITY   TEMP. ' , 1X , A /
     &        1H , '(eV/amu)  (cm-3)    (eV)  ' , 1X , A /
     &        1H , 127( '-' ) )
 1023 FORMAT( 1P , D8.2 , 2( 1X , D8.2 ) )
 1024 FORMAT( 1P , D9.2 , A1 )
 1025 FORMAT( 1H , A )
 1026 FORMAT( /1H , 92( '-' ) /
     &        1H , 'NOTE: 1) * => ',A13,' COEFFICIENT ' ,
     &        'EXTRAPOLATED FOR ENERGY/DENSITY/TEMPERATURE TRIPLET.' /
     &        1H , '      2) ION FRACTIONS GIVEN IN BRACKETS BELOW ' ,
     &        'ION SYMBOLS.' /
     &        1H , 92( '-' ) )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
