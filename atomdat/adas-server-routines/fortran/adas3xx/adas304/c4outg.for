CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas304/c4outg.for,v 1.2 2004/07/06 11:51:38 whitefor Exp $ Date $Date: 2004/07/06 11:51:38 $
CX
      SUBROUTINE C4OUTG( LGHOST , LGRD1  , TITLE  , DATE   ,
     &                   IFIT   , NSITYP , DSNIN  , TSYM   ,
     &                   ITZ    , SIFRAC , MXREQ  , NREQ   ,
     &                   UBMENG , UTDENS , UTTEMP , BSTOT  ,
     &                   BSION  , LFSEL  , NFIT   , XFIT   ,
     &                   YFIT   , FINFO  , LDEF1  , XMIN   ,
     &                   XMAX   , YMIN   , YMAX
     &                 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C4OUTG *********************
C
C  PURPOSE:  COMMUNICATES GRAPHICS DATA TO IDL
C
C            PLOT IS LOG10(BEAM STOPPING COEFFT. (CM3 S-1) VERSUS
C                    LOG10(BEAM ENERGY (EV)) OR
C                    LOG10(TARGET DENSITY (CM-3)) OR
C                    LOG10(TARGET TEMPERATURE (EV))
C
C            BOTH THE CONTRIBUTION OF THE INDIVIDUAL IONS AND THE
C            TOTAL STOPPING COEFFICIENTS ARE PLOTTED.
C
C            FOR EACH SET OF DATA POINTS THE FOLLOWING IS DRAWN:
C               SPLINE INTERPOLATED POINTS: CROSSES
C               CURVE THROUGH SPLINE POINTS: FULL CURVE
C               MINIMAX FIT TO SPLINE DATA : DASH CURVE
C
C
C  CALLING PROGRAM: ADAS304
C
C  I/O   : (L*4)   LGHOST   = INITIALISATION FLAG FOR GHOST80.
C                             .TRUE.  => GHOST80 INITIALISED.
C                             .FALSE. => GHOST80 NOT INITIALISED.
C
C  INPUT : (L*4)   LGRD1    = FLAGS WHETHER TO PUT GRAPHS IN GRID FILE.
C                             .TRUE.  => PUT GRAPHS IN GRID FILE
C                             .FALSE. => DO NOT PUT GRAPHS IN GRID FILE
C  INPUT : (C*40)  TITLE    = ISPF ENTERED GENERAL TITLE FOR PROGRAM RUN
C  INPUT : (C*8)   DATE     = DATE STRING.
C  INPUT : (I*4)   IFIT     = QUANTITY TO FIT AND DISPLAY.
C                             1 => BEAM ENERGY.
C                             2 => TARGET DENSITY.
C                             3 => TARGET TEMPERATURE.
C  INPUT : (I*4)   NSITYP   = NUMBER OF STOPPING IONS TYPES.
C  INPUT : (C*80)  DSNIN()  = INPUT DATA SET NAMES (FULL MVS DSN).
C                             (IN FORM SUITABLE FOR DYNAMIC ALLOCATION)
C                             DIMENSION: NSITYP
C  INPUT : (C*2)   TSYM()   = TARGET ION ELEMENT SYMBOL.
C                             DIMENSION: NSITYP
C  INPUT : (I*4)   ITZ()    = TARGET ION CHARGE.
C                             DIMENSION: NSITYP
C  INPUT : (R*8)   SIFRAC() = FRACTION OF EACH STOPPING ION.
C                             DIMENSION: NSITYP
C  INPUT : (I*4)   MXREQ    = MAX NO. OF USER REQUESTED TRIPLETS OF
C                             ENERGY, TEMPERATURE AND DENSITY.
C  INPUT : (I*4)   NREQ     = NUMBER OF USER REQUESTED TRIPLETS OF
C                             ENERGY, TEMPERATURE AND DENSITY.
C  INPUT : (R*8)   UBMENG() = USER REQESTED BEAM ENERGIES.
C                             DIMENSION: NREQ
C  INPUT : (R*8)   UTDENS() = USER REQESTED TARGET DENSITITES.
C                             DIMENSION: NREQ
C  INPUT : (R*8)   UTTEMP() = USER REQESTED TARGET TEMPERATURES.
C                             DIMENSION: NREQ
C  INPUT : (R*8)   BSTOT()  = TOTAL BEAM STOPPING COEFFICIENTS.
C                             DIMENSION: NREQ
C  INPUT : (R*8)   BSION(,) = BEAM STOPPING COEFFICIENTS FOR INDIVIDUAL
C                             IONS.
C                             1ST DIMENSION: MXREQ
C                             2ND DIMENSION: NSITYP
C  INPUT : (L*4)   LFSEL    = FLAGS MINIMAX POLYNOMIAL FITTING.
C                             .TRUE.  => FIT PERFORMED.
C                             .FALSE. => NO FIT.
C  INPUT : (I*4)   NFIT     = NUMBER OF POINTS IN FIT.
C  INPUT : (R*8)   XFIT(,)  = X COORDINATES OF POINTS FROM FIT.
C                             FOR THE 2ND INDEX INDICES 1 TO 'NSITYP'
C                             ARE FOR THE INDIVIDUAL IONS AND 'NSITYP+1'
C                             FOR THE TOTAL.
C                             1ST DIMENSION: NFIT
C                             2ND DIMENSION: NSITYP+1
C  INPUT : (R*8)   YFIT(,)  = Y COORDINATES OF POINTS FROM FIT.
C                             FOR THE 2ND INDEX INDICES 1 TO 'NSITYP'
C                             ARE FOR THE INDIVIDUAL IONS AND 'NSITYP+1'
C                             FOR THE TOTAL.
C                             1ST DIMENSION: NFIT
C                             2ND DIMENSION: NSITYP+1
C  INPUT : (C*80)  FINFO()  = INFORMATION STRING FROM FIT.
C                             INDICES 1 TO 'NSITYP' ARE FOR THE
C                             INDIVIDUAL IONS AND 'NSITYP+1' FOR THE
C                             TOTAL.
C                             DIMENSION: NSITYP+1
C  INPUT : (L*4)   LDEF1    = FLAGS DEFAULT GRAPH SCALING.
C                             .TRUE.  => USE DEFAULT GRAPH SCALING.
C                             .FALSE. => DO NOT USE DEFAULT SCALING.
C  INPUT : (R*8)   XMIN     = LOWER LIMIT FOR X-AXIS OF GRAPH.
C  INPUT : (R*8)   XMAX     = UPPER LIMIT FOR X-AXIS OF GRAPH.
C  INPUT : (R*8)   YMIN     = LOWER LIMIT FOR Y-AXIS OF GRAPH.
C  INPUT : (R*8)   YMAX     = UPPER LIMIT FOR Y-AXIS OF GRAPH.
C
C  PARAM : (R*8)   GHZERO   = VALUE BELOW WHICH GHOST80 TAKES NUMBERS AS
C                             BEING ZERO = 1.0E-36.
C  PARAM : (R*8)   YDMIN    = MINIMUM ALLOWED Y-VALUE FOR PLOTTING.
C                             (USED FOR DEFAULT GRAPH SCALING)
C                             (SET TO 'GHZERO'/ZERO TO REMOVE)
C
C          (I*4)   I        = LOOP / ARRAY INDEX.
C          (I*4)   J        = ARRAY INDEX.
C
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          XXFLSH     IDL_ADAS  FLUSHES OUT THE UNIX PIPE
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/87
C          JET EXT. 5183
C
C DATE:    22/12/93
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 16-11-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C VERSION: 1.2                          DATE: 11-12-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - TIDIED UP COMMENTS AND CODE
C
C-----------------------------------------------------------------------
      REAL*4     GHZERO            , YDMIN
      PARAMETER( GHZERO = 1.0E-36  , YDMIN = 1.0E-20 )
C-----------------------------------------------------------------------
      INTEGER    IFIT    , NSITYP  , MXREQ   , NREQ    , NFIT
      INTEGER    I       , J 
C-----------------------------------------------------------------------
      REAL*8     XMIN    , XMAX    , YMIN    , YMAX
C-----------------------------------------------------------------------
      LOGICAL    LGHOST  , LGRD1   , LFSEL   , LDEF1
C-----------------------------------------------------------------------
      CHARACTER  TITLE*40   , DATE*8
C-----------------------------------------------------------------------
      INTEGER    ITZ(NSITYP)
      INTEGER    PIPEIN   , PIPEOU
      PARAMETER( PIPEIN=5      , PIPEOU=6  )
C-----------------------------------------------------------------------
      REAL*8     SIFRAC(NSITYP)  , UBMENG(NREQ)    , UTDENS(NREQ)    ,
     &           UTTEMP(NREQ)    , BSTOT(NREQ)
C-----------------------------------------------------------------------
      CHARACTER  DSNIN(NSITYP)*80    , TSYM(NSITYP)*2      ,
     &           FINFO(NSITYP+1)*80
C-----------------------------------------------------------------------
      REAL*8     BSION(MXREQ,NSITYP)  , XFIT(NFIT,NSITYP+1)  ,
     &           YFIT(NFIT,NSITYP+1)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C WRITE OUT DATA TO IDL VIA UNIX PIPE
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C SPLINE DATA
C-----------------------------------------------------------------------
C
      DO 3, I=1,NREQ
          DO 4, J=1,NSITYP
              IF (IFIT.EQ.1) THEN  
                  WRITE(PIPEOU,*) UBMENG(I)
              ELSE IF (IFIT.EQ.2) THEN
                  WRITE(PIPEOU,*) UTDENS(I)
              ELSE
                  WRITE(PIPEOU,*) UTTEMP(I)
              ENDIF
              CALL XXFLSH(PIPEOU)
              WRITE(PIPEOU,*) BSION(I,J)
              CALL XXFLSH(PIPEOU)
4         CONTINUE
3     CONTINUE
      DO 5, I=1,NREQ
          WRITE(PIPEOU,*) BSTOT(I)
          CALL XXFLSH(PIPEOU)
5     CONTINUE

C
C-----------------------------------------------------------------------
C MINIMAX DATA
C-----------------------------------------------------------------------
C
      IF (LFSEL) THEN
          WRITE(PIPEOU,*) NFIT
          CALL XXFLSH(PIPEOU)
          DO 1, I=1, NSITYP+1
              DO 2, J=1, NFIT
                  WRITE(PIPEOU,*) XFIT(J,I)
                  CALL XXFLSH(PIPEOU)
                  WRITE(PIPEOU,*) YFIT(J,I)
                  CALL XXFLSH(PIPEOU)
2             CONTINUE
              WRITE(PIPEOU,'(A)') FINFO(I)
              CALL XXFLSH(PIPEOU)
1         CONTINUE
      ENDIF

C
c-----------------------------------------------------------------------
C
      RETURN
      END
