CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas304/c4chkz.for,v 1.2 2004/07/06 11:50:56 whitefor Exp $ Date $Date: 2004/07/06 11:50:56 $
CX
      SUBROUTINE C4CHKZ( DSNAME , TSYM , ITZ )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C4CHKZ *********************
C
C  PURPOSE: TO CHECK THAT THE STOPPING ION ELEMENT SYMBOL/CHARGE AS READ
C           FROM THE INPUT DATA SET AGREE WITH THOSE OBTAINED FROM THE
C           INPUT DATA SET MEMBER NAME.
C
CA          NOTE THAT THE INFORMATION WHICH WAS PREVIOUSLY DETERMINED
CA          FROM THE DATASET NAME IS NOW PASSED VIA UNIX PIPE FROM THE
CA          IDL ROUTINE c4chkz.pro
CA
C  CALLING PROGRAM: ADAS304
C
C  SUBROUTINE:
C
C  INPUT : (C*(*)) DSNAME  = MVS DATA SET NAME - WHERE INPUT VALUES
C                            ORIGINATE
C  INPUT : (C*2)   TSYM    = INPUT FILE DATA:EMITTING ION ELEMENT SYMBOL
C  INPUT : (I*4)   ITZ     = INPUT FILE DATA:EMITTING ION CHARGE
C
C          (I*4)   ITZM    = MEMBER NAME:EMITTING ION CHARGE
C
C          (C*2)   TSYMM   = MEMBER NAME:EMITTING ION ELEMENT SYMBOL
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          I4UNIT     ADAS      RETURNS UNIT NO. FOR OUTPUT OF MESSAGES
C
C AUTHOR:  JONATHAN NASH  (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/87
C          JET EXT. 5183
C
C DATE:    16/12/93
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 16-11-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C VERSION: 1.2                          DATE: 11-12-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - TIDIED UP COMMENTS AND CODE
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER     I4UNIT
      INTEGER     PIPEIN        , PIPEOU 
      PARAMETER(  PIPEIN=5      , PIPEOU=6  )
C-----------------------------------------------------------------------
      INTEGER    ITZ
      INTEGER    ITZM    
C-----------------------------------------------------------------------
      CHARACTER  DSNAME*(*)  , TSYM*2
      CHARACTER  TSYMM*2     
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C READ TARGET SYMBOL AND CHARGE FROM IDL
C-----------------------------------------------------------------------
C
       READ(PIPEIN, '(A)') TSYMM
       READ(PIPEIN, *) ITZM
C
C-----------------------------------------------------------------------
C COMPARE INPUT SYMBOL AND CHARGE WTIH THOSE FROM MEMBER NAME.
C-----------------------------------------------------------------------
C
      IF ( ( TSYM .NE. TSYMM ) .OR. ( ITZ .NE. ITZM ) ) THEN
         WRITE(I4UNIT(-1),1000) DSNAME
         WRITE(I4UNIT(-1),1001) TSYM , TSYMM , ITZ , ITZM
         WRITE(I4UNIT(-1),1002)
         STOP
      ENDIF
 
C
C-----------------------------------------------------------------------
C
 1000 FORMAT( 1X ,32('*') , ' C4CHKZ ERROR ' , 32('*') //
     &        1X , 'INPUT DATA SET NAME: ' , A / )
 1001 FORMAT( 1X  , 'ERROR TYPE: ELEMENT AND/OR CHARGE-STATE ' ,
     &              'DISCREPANCY.' //
     &        4X  , 'STOPPING ION ELEMENT SYMBOL: ' ,
     &              'READ FROM DATA SET    = ' , A2 /
     &        33X , 'READ FROM MEMBER NAME = ' , A2 /
     &        4X  , 'STOPPING ION CHARGE-STATE  : ' ,
     &              'READ FROM DATA SET    =' , I3 /
     &        33X , 'READ FROM MEMBER NAME =' , I3 )
 1002 FORMAT( 1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
