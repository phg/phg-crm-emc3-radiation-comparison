CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas304/c4spf0.for,v 1.2 2004/07/06 11:51:50 whitefor Exp $ Date $Date: 2004/07/06 11:51:50 $
CX
      SUBROUTINE C4SPF0( MXSIT , REP , NSITYP , DSFULL )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C4SPF0 *********************
C
C  PURPOSE: PIPE COMMUNICATION WITH IDL
C           (INPUT DATA SET SPECIFICATIONS).
C
C  CALLING PROGRAM: ADAS304
C
C  SUBROUTINE:
C
C  INPUT:  (I*4)  MXSIT     = MAX NO. OF STOPPING ION TYPES.
C  OUTPUT: (C*3)  REP       = REQUEST FOR PROGRAM TERMINATION.
C                             'YES' => TERMINATE PROGRAM EXECUTION.
C                             'NO'  => CONTINUE PROGRAM EXECUTION.
C  OUTPUT: (I*4)  NSITYP    = NO. OF STOPPING ION TYPES.
C  OUTPUT: (C*80) DSFULL()  = INPUT DATA SET NAMES (FULL MVS DSN).
C                             (IN FORM SUITABLE FOR DYNAMIC ALLOCATION)
C                             DIMENSION: MXSIT
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C
C AUTHOR:  JONATHAN NASH  (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/87
C          JET EXT. 5183
C
C DATE:    16/12/93
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 16-11-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C VERSION: 1.2                          DATE: 11-12-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - TIDIED UP COMMENTS AND CODE
C
C-----------------------------------------------------------------------
      INTEGER    MXSIT   , NSITYP  , I
      INTEGER     PIPEIN        , PIPEOU        , ONE      , ZERO
      PARAMETER(  PIPEIN=5      , PIPEOU=6      , ONE=1    , ZERO=0)
C-----------------------------------------------------------------------
      CHARACTER   REP*3
C-----------------------------------------------------------------------
      CHARACTER   DSFULL(MXSIT)*80
C-----------------------------------------------------------------------
C  READ BASIC OUTPUTS FROM PIPE
C-----------------------------------------------------------------------
C
      READ(PIPEIN,'(A)') REP
      IF (REP.EQ.'NO') THEN 
          READ(PIPEIN,*) NSITYP
          DO 1, I=1, NSITYP
              READ(PIPEIN,'(A)') DSFULL(I)
1         CONTINUE
      ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN
      END
