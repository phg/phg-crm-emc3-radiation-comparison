CX UNIX PORT - SCCS info: Module @(#)c4spln.for	1.2 Date 03/28/97
CX
      SUBROUTINE C4SPLN( MXBE   , MXTD   , MXTT   , MXREQ  ,
     &                   NREQ   , BMENGA , DENSA  , TIA    ,
     &                   NSITYP , SVREF  , NBE    , BE     ,
     &                   NTDENS , TDENS  , NTTEMP , TTEMP  ,
     &                   SVT    , SVED   , SVREQ  , LIBMA  ,
     &                   LIDNA  , LITIA  , ZEFF   , ITZ    ,
     &                   LSET   )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C4SPLN *********************
C
C  PURPOSE: CALCULATES THE BEAM STOPPING COEFFICIENT FOR EACH TRIPLET
C           OF BEAM ENERGY, ION DENSITY AND ION TEMPERATURE. IT USES
C           A ONE-WAY CUBIC SPLINE FOR THE TEMPERATURE AND A TWO-WAY
C           CUBIC SPLINE FOR THE ENERGY/DENSITY PAIR TO DETERMINE THE
C           STOPPING COEFFICIENT FROM THE INPUT DATA SET. IF A VALUE
C           CANNOT BE INTERPOLATED USING SLPINES THEN IT IS
C           EXTRAPOLATED BY 'XXSPLE'.
C
C  CALLING PROGRAM: CXBMS / ADAS304
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  MXBE      = MAXIMUM NUMBER OF BEAM ENERGIES WHICH CAN
C                             BE READ.
C  INPUT : (I*4)  MXTD      = MAXIMUM NUMBER OF TARGET DENSITIES WHICH
C                             CAN BE READ.
C  INPUT : (I*4)  MXTT      = MAXIMUM NUMBER OF TARGET TEMPERATURES
C                             WHICH CAN BE READ.
C  INPUT : (I*4)  MXREQ     = MAXIMUM NUMBER OF REQUESTED TRIPLETS OF
C                             BEAM ENERGY, ION DENSITY AND ION TEMP.
C  INPUT : (I*4)  NREQ      = NUMBER OF REQUESTED TRIPLETS OF BEAM
C                             ENERGY, ION DENSITY AND ION TEMP.
C  INPUT : (R*8)  BMENGA()  = REQUESTED BEAM ENERGIES.
C                             UNITS: EV/AMU
C                             DIMENSION: NREQ
C  INPUT : (R*8)  DENSA()   = REQUESTED ION DENSITIES.
C                             UNITS: CM-3
C                             DIMENSION: NREQ
C  INPUT : (R*8)  TIA()     = REQUESTED ION TEMPERATURES.
C                             UNITS: EV
C                             DIMENSION: NREQ
C  INPUT : (I*4)  NSITYP    = NUMBER OF STOPPING ION TYPES.
C  INPUT : (R*8)  SVREF()   = STOPPING COEFFT. AT REFERENCE BEAM ENERGY,
C                             TARGET DENSITY AND TEMPERATURE.
C                             UNITS: CM3 S-1
C                             DIMENSION: NSITYP
C  INPUT : (I*4)  NBE()     = NUMBER OF BEAM ENERGIES.
C                             DIMENSION: NSITYP
C  INPUT : (R*8)  BE(,)     = BEAM ENERGIES.
C                             UNITS: EV/AMU
C                             1ST DIMENSION: MXBE
C                             2ND DIMENSION: NSITYP
C  INPUT : (I*4)  NTDENS()  = NUMBER OF TARGET DENSITIES.
C                             DIMENSION: NSITYP
C  INPUT : (R*8)  TDENS(,)  = TARGET DENSITIES.
C                             UNITS: CM-3
C                             1ST DIMENSION: MXTD
C                             2ND DIMENSION: NSITYP
C  INPUT : (I*4)  NTTEMP()  = NUMBER OF TARGET TEMPERATURES.
C                             DIMENSION: NSITYP
C  INPUT : (R*8)  TTEMP(,)  = TARGET TEMPERATURES.
C                             UNITS: EV
C                             1ST DIMENSION: MXTT
C                             2ND DIMENSION: NSITYP
C  INPUT : (R*8)  SVT(,)    = STOPPING COEFFT. AT REFERENCE BEAM ENERGY
C                             AND TARGET DENSITY.
C                             UNITS: CM3 S-1
C                             1ST DIMENSION: MXTT
C                             2ND DIMENSION: NSITYP
C  INPUT : (R*8)  SVED(,,)  = STOPPING COEFFT. AT REFERENCE TARGET
C                             TEMPERATURE.
C                             UNITS: CM3 S-1
C                             1ST DIMENSION: MXBE
C                             2ND DIMENSION: MXTD
C                             3RD DIMENSION: NSITYP
C  OUTPUT: (R*8)  SVREQ(,)  = STOPPING COEFFT. AT REQUESTED BEAM ENERGY,
C                             ION DENSITY AND ION TEMPERATURE.
C                             UNITS: CM3 S-1
C                             1ST DIMENSION: MXREQ
C                             2ND DIMENSION: NSITYP
C  OUTPUT: (L*4)  LIBMA(,)  = FLAGS IF INTERPOLATION OR EXTRAPOLATION
C                             USED FOR REQUESTED BEAM ENERGIES.
C                             .TRUE.  => INTERPOLATION USED.
C                             .FALSE. => EXTRAPOLATION USED.
C                             1ST DIMENSION: MXREQ
C                             2ND DIMENSION: NSITYP
C  OUTPUT: (L*4)  LIDNA(,)  = FLAGS IF INTERPOLATION OR EXTRAPOLATION
C                             USED FOR REQUESTED ION DENSITIES.
C                             .TRUE.  => INTERPOLATION USED.
C                             .FALSE. => EXTRAPOLATION USED.
C                             1ST DIMENSION: MXREQ
C                             2ND DIMENSION: NSITYP
C  OUTPUT: (L*4)  LITIA(,)  = FLAGS IF INTERPOLATION OR EXTRAPOLATION
C                             USED FOR REQUESTED ION TEMPERATURES.
C                             .TRUE.  => INTERPOLATION USED.
C                             .FALSE. => EXTRAPOLATION USED.
C                             1ST DIMENSION: MXREQ
C                             2ND DIMENSION: NSITYP
C  INPUT:  (R*8)  ZEFF()    = USED AS A WEIGHTING FACTOR ASSOCIATED
C                             WITH THE EVALUATION OF AN EFFECTIVE
C                             DENSITY.
C  INPUT:  (I*4)  ITZ()	    = ARRAY CONTAINING THE NUCLEAR CHARGE OF
C			      EACH IMPURITY CONSIDERED.
C  INPUT:  (L)    LSET      = LOGICAL FLAGGING WHETHER OR NOT THE INPUT
C                             DATASET VECTOR HAS CHANGED. IF SO, A
C                             REQUEST TO REDO THE SPLINES IS PASSED TO
C                             'XXSPLF'.
C
C  PARAM : (I*4)  MXI       = MAX. NO. OF STOPPING ION TYPES >= NSITYP.
C  PARAM : (I*4)  MXIN      = MAX. NO. OF INPUT DATA SET VALUES
C                             >= MXBE , MXTD , MXTT.
C  PARAM : (I*4)  MXOUT     = MAX. NO. OF OUTPUT VALUES >= NREQ.
C
C          (I*4)  IOPT      = DEFINES THE BOUNDARY DERIVATIVES FOR THE
C                             SPLINE ROUTINE 'XXSPLE'. SEE 'XXSPLE'.
C                             ( VALID VALUES = <0, 0, 1, 2, 3, 4)
C          (I*4)  NOUT      = NUMBER OF OUTPUT VALUES FOR SPLINE.
C          (I*4)  I         = LOOP INDEX.
C          (I*4)  J         = LOOP INDEX.
C          (I*4)  K         = LOOP INDEX.
C
C          (L*4)  LSETX     = FLAGS TO SPLINE ROUTINE 'XXSPLF' IF
C                             'X' SPLINE PARAMETERS SHOULD BE SET UP.
C                             .TRUE.  => SET UP SPLINE PARAMS.
C                             .FALSE. => DO NOT SET UP SPLINE PARAMS.
C          (L*4)  LSETY     = FLAGS TO SPLINE ROUTINE 'XXSPLE' IF
C                             'Y' SPLINE PARAMETERS SHOULD BE SET UP.
C                             .TRUE.  => SET UP SPLINE PARAMS.
C                             .FALSE. => DO NOT SET UP SPLINE PARAMS.
C
C          (R*8)  DYT(,)    = DERIVATIVES FOR SPLINE INTERPOLATION OVER
C                             TEMPERATURE. ONE VECTOR FOR EACH TARGET
C                             ION.  SAVED FOR SPEED ON MULTIPLE CALLS
C                             DIMENSION: (MXIN,MXI)
C          (R*8)  QT(,)     = SPLINE INTERPOLATED SECOND DERIVATIVES.
C          (R*8)  D1T(,)    = MULTIPLICATION FACTOR USED IN XXSPLF.
C          (R*8)  D2T(,)    = MULTIPLICATION FACTOR USED IN XXSPLF.
C          (R*8)  D3T(,)    = MULTIPLICATION FACTOR USED IN XXSPLF.
C          (R*8)  DYE(,,)   = DERIVATIVES FOR SPLINE INTERPOLATION OVER
C                             ENERGY. ONE VECTOR FOR EACH INPUT ENERGY
C                             AND TARGET ION.  SAVED FOR SPEED ON
C                             MULTIPLE CALLS
C                             DIMENSION: (MXIN,MXIN,MXI)
C          (R*8)  QE(,)     = SPLINE INTERPOLATED SECOND DERIVATIVES.
C          (R*8)  D1E(,)    = MULTIPLICATION FACTOR USED IN XXSPLF.
C          (R*8)  D2E(,)    = MULTIPLICATION FACTOR USED IN XXSPLF.
C          (R*8)  D3E(,)    = MULTIPLICATION FACTOR USED IN XXSPLF.
C          (R*8)  DYD(,)    = DERIVATIVES FOR SPLINE INTERPOLATION OVER
C                             DENSITY. ONE VECTOR FOR EACH TARGET ION.
C                             SAVED FOR SPEED ON MULTIPLE CALLS
C                             DIMENSION: (MXIN,MXI)
C          (R*8)  QD(,)     = SPLINE INTERPOLATED SECOND DERIVATIVES.
C          (R*8)  D1D(,)    = MULTIPLICATION FACTOR USED IN XXSPLF.
C          (R*8)  D2D(,)    = MULTIPLICATION FACTOR USED IN XXSPLF.
C          (R*8)  D3D(,)    = MULTIPLICATION FACTOR USED IN XXSPLF.
C
C          (R*8)  YOUT()    = Y OUTPUT ARRAY FROM SPLINE ROUTINE.
C                             DIMENSION: MXOUT
C
C          (R*8)  SVTO(,)   = STOPPING COEFFICIENTS AT REQUESTED ION
C                             TEMPERATURES.
C                             1ST DIMENSION: MXOUT
C                             2ND DIMENSION: MXI
C          (R*8)  SVEDO(,)  = STOPPING COEFFICIENTS AT REQUESTED BEAM
C                             ENERGIES AND ION DENSITY.
C                             1ST DIMENSION: MXOUT
C                             2ND DIMENSION: MXI
C          (R*8)  YPASS(,)  = STOPPING COEFFICIENTS AT REQUESTED BEAM
C                             ENERGIES.
C                             1ST DIMENSION: MXIN
C                             2ND DIMENSION: MXOUT
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4UNIT     ADAS      RETURNS UNIT NO. FOR OUTPUT OF MESSAGES.
C          XXSPLF     ADAS      SPLINE SUBROUTINE (EXTENDED DIAGNOSTICS)
C          R8FUN1     ADAS      PERFORMS TRANSFORMATION ( X -> X )
C
C AUTHOR:  JONATHAN NASH  (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/87
C          JET EXT. 5183
C
C DATE:    10/12/93
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 16-11-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C VERSION: 1.2
C MODIFIED: HARVEY ANDERSON
C	  	- THE BEAM STOPPING COEFFICIENT FOR EACH
C		  INDIVIDUAL IMPURITY WAS BEING EVALUATED
C		  AT THE WRONG DENSITY. THE BEAM STOPPING
C		  COEFFICIENT SHOULD BE EVALUATED AT AN
C		  EFFECTIVE DENSITY. THIS HAS BEEN CORRECTED.
C		- THE TARGET DENSITY READ FROM ADF21 FILE IS THE
C		  ELECTRON DENSITY. THE CORRECTION TO THE EVALUATION
C		  OF THE EFFECTIVE DENSITY WAS DONE IN TERMS OF THE
C		  ION DENSITY. THIS WAS CORRECTED SO THAT THE EFFECTIVE
C		  DENSITY IS EVALUATED IN TERMS OF THE ELECTRON DENSITY.
C		  20/12/96
C		- INTRODUCED THE PARAMETER FACT2, TO ENABLE THE 
C		  EFFECTIVE ELECTRON DENSITY TO BE EVALUATED.ORIGINALY
C		  THE USER WOULD ENTER THE TOTAL ION DENSITY AND
C		  THE STOPPING COEFFICIENTS WOULD BE EVALUATED AT AN
C		  EFFECTIVE ELECTRON DENSITY. NOW THE CODE HAS BEEN
C		  CHANGED TO ALLOW THE USER TO ENTER THE TOTAL ELECTRON
C		  DENSITY.
C
C VERSION: 1.3                           DATE: 19-03-03
C MODIFIED: LORNE HORTON
C               - INCREASED MXOUT TO ALLOW UP TO 1024 EVALUATIONS PER
C                 CALL.
C               - IMPLEMENTED XXSPLF TO SPEED BICUBIC SPLINING BY
C                 HOLDING AS MUCH AS POSSIBLE IN GLOBAL VARIABLES.
C               - REPLACED FACT1 AND FACT2 WITH ZEFF
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    I4UNIT
C-----------------------------------------------------------------------
      EXTERNAL   R8FUN1
      REAL*8     R8FUN1
C-----------------------------------------------------------------------
      INTEGER    MXI       , MXIN       , MXOUT
      PARAMETER( MXI = 10  , MXIN = 25  , MXOUT = 1024 )
C-----------------------------------------------------------------------
      INTEGER    MXBE    , MXTD    , MXTT    , MXREQ   ,
     &           NREQ    , NSITYP
      INTEGER    IOPT    , NOUT    , I       , J       , K
C-----------------------------------------------------------------------
      LOGICAL    LSET    , LSETX   , LSETY
C-----------------------------------------------------------------------
      INTEGER    NBE(NSITYP)     , NTDENS(NSITYP)  , NTTEMP(NSITYP)  ,
     &		 ITZ(NSITYP)
C-----------------------------------------------------------------------
      REAL*8     BMENGA(NREQ)    , DENSA(NREQ)     , TIA(NREQ)       ,
     &           SVREF(NSITYP)
      REAL*8     YOUT(MXOUT)     , ZEFF(NREQ)
C-----------------------------------------------------------------------
      REAL*8     DYT(MXIN,MXI)       , QT(MXIN,MXI)        ,
     &           D1T(MXIN,MXI)       , D2T(MXIN,MXI)       ,
     &           D3T(MXIN,MXI)
      REAL*8     DYE(MXIN,MXIN,MXI)  , QE(MXIN,MXI)        ,
     &           D1E(MXIN,MXI)       , D2E(MXIN,MXI)       ,
     &           D3E(MXIN,MXI)
      REAL*8     DYD(MXIN,MXI)       , QD(MXIN,MXI)        ,
     &           D1D(MXIN,MXI)       , D2D(MXIN,MXI)       ,
     &           D3D(MXIN,MXI)
C-----------------------------------------------------------------------
      REAL*8     BE(MXBE,NSITYP)      , TDENS(MXTD,NSITYP)   ,
     &           TTEMP(MXTT,NSITYP)   , SVT(MXTT,NSITYP)     ,
     &           SVREQ(MXREQ,NSITYP)
      REAL*8     SVTO(MXOUT,MXI)      , SVEDO(MXOUT,MXI)     ,
     &           YPASS(MXIN,MXOUT)
C-----------------------------------------------------------------------
      LOGICAL    LIBMA(MXREQ,NSITYP)  , LIDNA(MXREQ,NSITYP)  ,
     &           LITIA(MXREQ,NSITYP)
C-----------------------------------------------------------------------
      REAL*8     SVED(MXBE,MXTD,NSITYP)
C----------------------------------------------------------------------
      SAVE       DYT, QT, D1T, D2T, D3T
      SAVE       DYE, QE, D1E, D2E, D3E
      SAVE       DYD, QD, D1D, D2D, D3D
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C CHECK INTERNAL PARAMETERS 'MXI', 'MXIN', AND 'MXOUT'.
C-----------------------------------------------------------------------
C
      IF ( MXI .LT. NSITYP ) THEN
         WRITE(I4UNIT(-1),1000) 'MXI' , 'NSITYP' , MXI , NSITYP , 'MXI'
         STOP
      ENDIF
C
      IF ( MXIN .LT. MXBE ) THEN
         WRITE(I4UNIT(-1),1000) 'MXIN' , 'MXBE' , MXIN , MXBE , 'MXIN'
         STOP
      ENDIF
C
      IF ( MXIN .LT. MXTD ) THEN
         WRITE(I4UNIT(-1),1000) 'MXIN' , 'MXTD' , MXIN , MXTD , 'MXIN'
         STOP
      ENDIF
C
      IF ( MXIN .LT. MXTT ) THEN
         WRITE(I4UNIT(-1),1000) 'MXIN' , 'MXTT' , MXIN , MXTT , 'MXIN'
         STOP
      ENDIF
C
      IF ( MXOUT .LT. NREQ ) THEN
         WRITE(I4UNIT(-1),1000) 'MXOUT' , 'NREQ' , MXOUT , NREQ ,
     &                          'MXOUT'
         STOP
      ENDIF
C
C-----------------------------------------------------------------------
C DETERMINE STOPPING COEFFT. AT REQUESTED TEMPERATURES.
C-----------------------------------------------------------------------
C
      DO 1 I = 1 , NSITYP
         IF (LSET) LSETX = .TRUE.
         IOPT = 0
         CALL XXSPLF( LSETX      , LSETY      , IOPT       ,
     &                R8FUN1     , NTTEMP(I)  , TTEMP(1,I) ,
     &                SVT(1,I)   , NREQ       , TIA        ,
     &                SVTO(1,I)  , TTEMP(1,I) , DYT(1,I)   ,
     &                QT(1,I)    , D1T(1,I)   , D2T(1,I)   ,
     &                D3T(1,I)   , LITIA(1,I) )
    1 CONTINUE
C
C-----------------------------------------------------------------------
C DETERMINE STOPPING COEFFT. AT REQUESTED ENERGIES FOR EACH REQUESTED
C DENSITY FOR EACH STOPPING ION TYPE.
C-----------------------------------------------------------------------
C
      DO 2 I = 1 , NSITYP
         IF (LSET) LSETX = .TRUE.
         IOPT = 0
         DO 3 J = 1 , NTDENS(I)
            IF (LSET) LSETY = .TRUE.
            CALL XXSPLF( LSETX       , LSETY       , IOPT        ,
     &                   R8FUN1      , NBE(I)      , BE(1,I)     ,
     &                   SVED(1,J,I) , NREQ        , BMENGA      ,
     &                   YOUT        , BE(1,I)     , DYE(1,J,I)  ,
     &                   QE(1,I)     , D1E(1,I)    , D2E(1,I)    ,
     &                   D3E(1,I)    , LIBMA(1,I)  )
            DO 4 K = 1 , NREQ
               YPASS(J,K) = YOUT(K)
    4       CONTINUE

    3    CONTINUE
C
C-----------------------------------------------------------------------
C DETERMINE STOPPING COEFFT. AT EQUIVALENT DENSITY POINTS FOR
C EACH STOPPING ION TYPE.
C-----------------------------------------------------------------------
C
         LSETX = .TRUE.
         IOPT = 0
         NOUT = 1
         DO 5 J = 1 , NREQ
            LSETY = .TRUE.
            CALL XXSPLF( LSETX      , LSETY      , IOPT       ,
     &                   R8FUN1     , NTDENS(I)  , TDENS(1,I) ,
     &                   YPASS(1,J) , NOUT       ,
     &                   DENSA(J)*ZEFF(J)/ITZ(I) ,
     &	                 SVEDO(J,I) , TDENS(1,I) , DYD(1,I)   ,
     &                   QD(1,I)    , D1D(1,I)   , D2D(1,I)   ,
     &                   D3D(1,I)   , LIDNA(J,I) )
    5    CONTINUE
    2 CONTINUE
C
C-----------------------------------------------------------------------
C DETERMINE TOTAL STOPPING COEFFT. AT EACH TRIPLET OF ENERGY , DENSITY
C AND TEMPERATURE FOR EACH STOPPING ION TYPE.
C-----------------------------------------------------------------------
C
      DO 6 I = 1 , NSITYP
         DO 7 J = 1 , NREQ
            SVREQ(J,I) = SVEDO(J,I) * SVTO(J,I) / SVREF(I)
    7    CONTINUE
    6 CONTINUE
C
C-----------------------------------------------------------------------
C SET LSET = .FALSE. TO PERMIT FAST INTERPOLATION ON NEXT CALL IF
C DSNAMES HAVEN'T CHANGED
C-----------------------------------------------------------------------
      LSET = .FALSE.
C
C-----------------------------------------------------------------------
C
 1000 FORMAT( 1X, 32('*'), ' C4SPLN ERROR ', 32('*') //
     &        2X, 'INTERNAL PARAMETER ''', A,
     &            ''' IS LESS THAN INPUT ARGUMENT ''', A, '''.'/
     &        2X, 'INTERNAL PARAM = ', I3 , '   INPUT ARGUMENT = ', I3/
     &        2X, 'INCREASE PARAMETER ''', A,
     &            ''' IN SUBROUTINE C4SPLN.' //
     &        1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
