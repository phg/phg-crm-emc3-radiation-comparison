C SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas304/adas304.for,v 1.6 2004/12/10 19:48:10 mog Exp $ Date $Date: 2004/12/10 19:48:10 $
CX
      PROGRAM ADAS304

      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 PROGRAM: ADAS304 **********************
C
C  VERSION:  1.0 (ADAS91)
C
C  PURPOSE:  TO INTERPOLATE AND GRAPH SELECTED DATA FROM COMPILATIONS OF
C            BEAM STOPPING COEFFICIENTS.
C
C            THE PROGRAM ALLOWS SELECTION OF MULTIPLE MEMBERS FROM A
C            SINGLE DATA SET, EACH PROVIDING INPUT DATA ON A PARTICULAR
C            ION TYPE. THE USER SPECIFIES THE FRACTION OF EACH ION TYPE
C            SELECTED. FROM THESE INPUT DATA A CUBIC SPLINE ROUTINE IS
C            USED TO INTERPOLATE THE BEAM STOPPING COEFFICIENTS TO
C            USER-DEFINED TRIPLETS OF BEAM ENERGY, TARGET DENSITY AND
C            TARGET TEMPERATURE. TRIPLETS OUTSIDE THE INPUT DATA ARE
C            EXTRAPOLATED.
C
C            OUTPUT OF THE PROGRAM IS A GRAPH OF THE INTERPOLATED BEAM
C            STOPPING COEFFICIENTS VERSUS EITHER BEAM ENERGY, TARGET
C            DENSITY, OR TARGET TEMPERATURE FOR BOTH THE INDIVIDUAL IONS
C            AND THEIR TOTAL. IF REQUESTED MINIMAX POLYNOMIALS ARE
C            FITTED THROUGH ALL THE DATA AND GRAPHICALLY DISPLAYED. THE
C            RESULTS ARE ALSO GIVEN AS TABLES IN A TEXT FILE.
C
C            IF REQUESTED THE GRAPH AND TEXT FILE ARE OUTPUT TO
C            HARDCOPY.
C
C  DATA:     THE SOURCE DATA IS CONTAINED AS MEMBERS IN A PARTITIONED
C            DATA SET AS FOLLOWS:-
C
C                   'JETUID.<GROUP PREFIX>#<BEAM SYMBOL>.<TYPE>
C                    (<MEMBER PREFIX>#<ION SYMBOL AND CHARGE>)'
C
C            (IF THERE IS NO MEMBER PREFIX THEN THE MEMBER NAME IS JUST
C             <ION SYMBOL AND CHARGE>).
C
C            EACH DATA SET MEMBER IS A TWO WAY TABLE OVER BEAM ENERGY
C            AND TARGET DENSITY AT A REFERENCE TEMPERATURE, AND A ONE
C            WAY TABLE OF TARGET TEMPERATURE AT A REFERENCE ENERGY AND
C            DENSITY.
C
C            THE UNITS USED IN THE DATA FILE ARE TAKEN AS FOLLOWS:
C            BEAM ENERGY           : EV
C            TARGET DENSITY        : CM-3
C            TARGET TEMPERATURE    : EV
C            BEAM STOPPING COEFFT. : CM3 S-1
C
C  PROGRAM:
C
C  PARAM : (I*4)   IUNT07   = UNIT NO. FOR OUTPUT OF TEXT RESULTS.
C  PARAM : (I*4)   IUNT10   = UNIT NO. FOR INPUT DATA SETS.
C  PARAM : (I*4)   MXIT     = MAX. NO. OF STOPPING ION TYPES >= NSITYP.
C  PARAM : (I*4)   MXBE     = MAX. NO. OF BEAM ENERGIES WHICH CAN BE
C                             READ FROM INPUT DATA SET.
C  PARAM : (I*4)   MXTD     = MAX. NO. OF TARGET DENSITIES WHICH CAN BE
C                             READ FROM INPUT DATA SET.
C  PARAM : (I*4)   MXTT     = MAX. NO. OF TARGET TEMPERATURES WHICH CAN
C                             BE READ FROM INPUT DATA SET.
C  PARAM : (I*4)   MXREQ    = MAX. NO. OF REQUESTED ENERGIES >= NBMENG.
C  PARAM : (I*4)   MXDEG    = MAX NO. DEGREES FOR MINIMAX POLY. FIT.
C  PARAM : (I*4)   NFIT     = NUMBER OF POINTS IN MINIMAX POLY. FIT.
C
C  PARAM : (I*4)   LMLOG    = .TRUE. => FIT MINIMAX POLYNOMIAL TO LOG10
C                                       TRANSFORMATION OF INPUT DATA.
C
C          (I*4)   IPAN     = ISPF PANEL NUMBER
C          (I*4)   NSITYP   = NUMBER OF STOPPING IONS TYPES.
C          (I*4)   IFIT     = QUANTITY TO FIT AND DISPLAY.
C                             1 => BEAM ENERGY.
C                             2 => TARGET DENSITY.
C                             3 => TARGET TEMPERATURE.
C          (I*4)   NREQ     = NUMBER OF USER REQUESTED TRIPLETS OF
C                             ENERGY, TEMPERATURE AND DENSITY.
C          (I*4)   I        = LOOP INDEX.
C          (I*4)   J        = LOOP INDEX.
C
C          (R*8)   TOLVAL   = FRACTIONAL TOLERANCE FOR ACCEPTANCE OF
C                             MINIMAX POLYNOMIAL FIT TO DATA.
C          (R*8)   XL1      = LOWER LIMIT FOR X-AXIS OF GRAPH.
C          (R*8)   XU1      = UPPER LIMIT FOR X-AXIS OF GRAPH.
C          (R*8)   YL1      = LOWER LIMIT FOR Y-AXIS OF GRAPH.
C          (R*8)   YU1      = UPPER LIMIT FOR Y-AXIS OF GRAPH.
C
C          (L*4)   LGHOST   = INITIALISATION FLAG FOR GHOST80.
C                             .TRUE.  => GHOST80 INITIALISED.
C                             .FALSE. => GHOST80 NOT INITIALISED.
CX         (L*4)   LDSEL    = FLAGS WHETHER TO DISPLAY TEXT IN INPUT
CX                            DATA SET.
CX                            .TRUE.  => DISPLAY TEXT IN INPUT DATA SET.
CX                            .FALSE. => DO NOT DISPLAY TEXT IN INPUT
CX                                       DATA SET.
C          (L*4)   LPEND    = FLAGS IF END OF ANALYSIS REQUESTED.
C                             .TRUE.  => CONTINUE ANALYSIS OF CURRENT DATA
C                                        SETS
C                             .FALSE. => END ANALYSIS OF CURRENT
C                                        DATA SETS
C          (L*4)   LFSEL    = FLAGS MINIMAX POLYNOMIAL FITTING.
C                             .TRUE.  => FIT PERFORMED.
C                             .FALSE. => NO FIT.
C          (L*4)   LOSEL    = FLAGS WHETHER TO CALCULATE INTERPOLATED
C                             VALUES FOR OUTPUT (ALWAYS SET TO .TRUE.).
C                             .TRUE.  => INTERPOLATE VALUES.
C                             .FALSE. => DO NOT INTERPOLATE VALUES.
C          (L*4)   LGRD1    = FLAGS WHETHER TO PUT GRAPHS IN GRID FILE.
C                             .TRUE.  => PUT GRAPHS IN GRID FILE
C                             .FALSE. => DO NOT PUT GRAPHS IN GRID FILE
C          (L*4)   LDEF1    = FLAGS DEFAULT GRAPH SCALING.
C                             .TRUE.  => USE DEFAULT GRAPH SCALING.
C                             .FALSE. => DO NOT USE DEFAULT SCALING.
C
C          (C*8)   DATE     = DATE STRING.
C          (C*3)   REP      = REQUEST FOR PROGRAM TERMINATION.
C                             'YES' => TERMINATE PROGRAM EXECUTION.
C                             'NO'  => CONTINUE PROGRAM EXECUTION.
C          (C*40)  TITLE    = ISPF ENTERED GENERAL TITLE FOR PROGRAM RUN
C
C          (I*4)   ITZ()    = TARGET ION CHARGE.
C                             DIMENSION: MXIT
C          (I*4)   NBE()    = NUMBER OF BEAM ENERGIES.
C                             DIMENSION: MXIT
C          (I*4)   NTDENS() = NUMBER OF TARGET DENSITIES.
C                             DIMENSION: MXIT
C          (I*4)   NTTEMP() = NUMBER OF TARGET TEMPERATURES.
C                             DIMENSION: MXIT
C          (I*4)   NCOEF()  = NUMBER OF TAYLOR COEFFICIENTS FROM FIT.
C                             DIMENSION: MXIT
CA         (I*4)   ISTOP    = FLAG USED BY IDL TO SIGNAL  IMMEDIATE END
CA                            TO THE ROUTINE FROM ANY POINT RATHER THAN
CA                            RETURN TO THE PREVIOUS SCREEN. IF 1 THEN
CA                            PROGRAM ENDS, IF 0 THEN IT CONTINUES.
C          (I*4)   TFLAG    = FLAG DETERMINES WHETHER 'ADF21' OR 'ADF22'
C                             DATACLASS WAS USED.
C          (R*8)   BEREF()  = REFERENCE BEAM ENERGIES.
C                             UNITS: EV/AMU
C                             DIMENSION: MXIT
C          (R*8)   TDREF()  = REFERENCE TARGET DENSITIES.
C                             UNITS: CM-3
C                             DIMENSION: MXIT
C          (R*8)   TTREF()  = REFERENCE TARGET TEMPERATURES.
C                             UNITS: EV
C                             DIMENSION: MXIT
C          (R*8)   SVREF()  = STOPPING COEFFT. AT REFERENCE BEAM ENERGY,
C                             TARGET DENSITY AND TEMPERATURE.
C                             UNITS: CM3 S-1
C                             DIMENSION: MXIT
C          (I*4)   BEMNMX() = RANGE OF MIN AND MAX FOR BEAM ENERGIES.
C                             INDICES ARE AS FOLLOWS:
C                             1 => LOWEST MINIMUM
C                             2 => HIGHEST MINIMUM
C                             3 => LOWEST MAXIMUM
C                             4 => HIGHEST MAXIMUM
C                             DIMENSION: 4
C          (I*4)   TDMNMX() = RANGE OF MIN AND MAX FOR TARGET DENSITIES.
C                             INDICES AS FOR 'BEMNMX()'
C                             DIMENSION: 4
C          (I*4)   TTMNMX() = RANGE OF MIN AND MAX FOR TARGET TEMPS.
C                             INDICES AS FOR 'BEMNMX()'
C                             DIMENSION: 4
C          (R*8)   SIFRAC() = FRACTION OF EACH STOPPING ION.
C                             DIMENSION: MXIT
C          (R*8)   UBMENG() = USER REQESTED BEAM ENERGIES.
C                             DIMENSION: MXREQ
C          (R*8)   UTDENS() = USER REQESTED TARGET DENSITITES.
C                             DIMENSION: MXREQ
C          (R*8)   UTTEMP() = USER REQESTED TARGET TEMPERATURES.
C                             DIMENSION: MXREQ
C          (R*8)   BSTOT()  = TOTAL BEAM STOPPING COEFFICIENTS.
C                             DIMENSION: MXREQ
C
C          (L*4)   LINTOT() = FLAGS IF INTERPOLATION OR EXTRAPOLATION
C                             USED IN SPLINING ROUTINE.
C                             .TRUE.  => ONLY INTERPOLATION USED.
C                             .FALSE. => AT LEAST ONE OF ENERGY, DENISTY
C                                        OR TEMPERATURE EXTRAPOLATED.
C                             DIMENSION: MXREQ
C
C          (C*80)  DSNIN()  = INPUT DATA SET NAMES (FULL MVS DSN).
C                             (IN FORM SUITABLE FOR DYNAMIC ALLOCATION)
C                             DIMENSION: MXIT
C          (L*4)   LSET     = FLAG TO REQUEST FULL RE-SPLINE OF INPUT
C                             DATA WHEN INPUT FILE NAMES HAVE CHANGED
C          (C*80)  DSNSAV() = INPUT DATA SET NAMES FROM LAST CALL TO
C                             C4DATA.  USED TO CHECK IF NEW SPLINES
C                             ARE NECESSARY
C                             DIMENSION: MXIT
C          (C*2)   TSYM()   = TARGET ION ELEMENT SYMBOL.
C                             DIMENSION: MXIT
C          (C*80)  FINFO()  = INFORMATION STRING FROM FIT.
C                             INDICES 1 TO 'NSITYP' ARE FOR THE
C                             INDIVIDUAL IONS AND 'NSITYP+1' FOR THE
C                             TOTAL.
C                             DIMENSION: MXIT+1
C
C          (R*8)   BE(,)    = BEAM ENERGIES.
C                             UNITS: EV/AMU
C                             1ST DIMENSION: MXBE
C                             2ND DIMENSION: MXIT
C          (R*8)   TDENS(,) = TARGET DENSITIES.
C                             UNITS: CM-3
C                             1ST DIMENSION: MXTD
C                             2ND DIMENSION: MXIT
C          (R*8)   TTEMP(,) = TARGET TEMPERATURES.
C                             UNITS: EV
C                             1ST DIMENSION: MXTT
C                             2ND DIMENSION: MXIT
C          (R*8)   SVT(,)   = STOPPING COEFFT. AT REFERENCE BEAM ENERGY
C                             AND TARGET DENSITY.
C                             UNITS: CM3 S-1
C                             1ST DIMENSION: MXTT
C                             2ND DIMENSION: MXIT
C          (R*8)   BSION(,) = BEAM STOPPING COEFFICIENTS FOR INDIVIDUAL
C                             IONS.
C                             1ST DIMENSION: MXREQ
C                             2ND DIMENSION: MXIT
C          (R*8)   XFIT(,)  = X COORDINATES OF POINTS FROM FIT.
C                             FOR THE 2ND INDEX INDICES 1 TO 'NSITYP'
C                             ARE FOR THE INDIVIDUAL IONS AND 'NSITYP+1'
C                             FOR THE TOTAL.
C                             1ST DIMENSION: NFIT
C                             2ND DIMENSION: MXIT+1
C          (R*8)   YFIT(,)  = Y COORDINATES OF POINTS FROM FIT.
C                             FOR THE 2ND INDEX INDICES 1 TO 'NSITYP'
C                             ARE FOR THE INDIVIDUAL IONS AND 'NSITYP+1'
C                             FOR THE TOTAL.
C                             1ST DIMENSION: NFIT
C                             2ND DIMENSION: MXIT+1
C          (R*8)   FCOEF(,) = TAYLOR COEFFICIENTS FROM FIT.
C                             1ST DIMENSION: MXDEG
C                             2ND DIMENSION: MXIT
C
C          (L*4)   LIBMA(,) = FLAGS IF INTERPOLATION OR EXTRAPOLATION
C                             USED FOR REQUESTED BEAM ENERGIES.
C                             .TRUE.  => INTERPOLATION USED.
C                             .FALSE. => EXTRAPOLATION USED.
C                             1ST DIMENSION: MXREQ
C                             2ND DIMENSION: MXIT
C          (L*4)   LIDNA(,) = FLAGS IF INTERPOLATION OR EXTRAPOLATION
C                             USED FOR REQUESTED ION DENSITIES.
C                             .TRUE.  => INTERPOLATION USED.
C                             .FALSE. => EXTRAPOLATION USED.
C                             1ST DIMENSION: MXREQ
C                             2ND DIMENSION: MXIT
C          (L*4)   LITIA(,) = FLAGS IF INTERPOLATION OR EXTRAPOLATION
C                             USED FOR REQUESTED ION TEMPERATURES.
C                             .TRUE.  => INTERPOLATION USED.
C                             .FALSE. => EXTRAPOLATION USED.
C                             1ST DIMENSION: MXREQ
C                             2ND DIMENSION: MXIT
C          (L*4)   LINION(,)= FLAGS IF INTERPOLATION OR EXTRAPOLATION
C                             USED IN SPLINING ROUTINE.
C                             .TRUE.  => ONLY INTERPOLATION USED.
C                             .FALSE. => AT LEAST ONE OF ENERGY, DENISTY
C                                        OR TEMPERATURE EXTRAPOLATED.
C                             1ST DIMENSION: MXREQ
C                             2ND DIMENSION: MXIT
C
C          (R*8)   SVED(,,) = STOPPING COEFFT. AT REFERENCE TARGET
C                             TEMPERATURE.
C                             UNITS: CM3 S-1
C                             1ST DIMENSION: MXBE
C                             2ND DIMENSION: MXTD
C                             3RD DIMENSION: MXIT
C          (R*8)  FACT1     = FACTOR USED IN CALCULATING ZEFF
C          (R*8)  FACT2     = SIMILAR TO FACT1.
C          (R*8)  ZEFF()    = USED SO THAT THE BEAM STOPPING
C                             COEFFICIENTS FOR INDIVUDUAL
C                             IMPURITY IONS ARE EVALUATED AT THE
C                             CORRECT EFFECTIVE ELECTRON DENSITY.
C          (R*8)   WT       = WEIGHTING FACTOR.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          C4SPF0     ADAS      HANDELS ISPF PANEL FOR DATA SET INPUT.
C          XXDATA_21  ADAS      READS INPUT DATA SET IN ADF21 FORMAT.
C          C4CHKZ     ADAS      CHECKS CONSISTANCY OF DATA SET.
C          C4MNMX     ADAS      FINDS RANGE OF MINS AND MAXS OF ENERGIES
C                               DENSITIES AND TEMPS IN INPUT DATA SETS.
C          C4ISPF     ADAS      HANDELS ISPF PANELS FOR USER INPUT.
C          C4SPLN     ADAS      PERFORMS SPLINE ON INPUT DATA.
C          C4OUTG     ADAS      CONTROLS GRAPHICAL OUTPUT FOR ADAS304.
C          C4OUT0     ADAS      CONTROLS TEXT OUTPUT FOR ADAS304.
C          XX0000     ADAS      SETS MACHINE DEPENDENT ADAS
C                               CONFIGURATION.
C          XXDATE     ADAS      RETURNS CURRENT DATE.
C          XXMNMX     ADAS      PERFORMS MINIMAX POLYNOMIAL FIT.
C          I4UNIT     ADAS      RETURNS UNIT NO. FOR OUTPUT OF MESSAGES.
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/87
C          JET EXT. 5183
C
C DATE:    23/12/93
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 16-11-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C VERSION: 1.2                          DATE: 11-12-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - TIDIED UP COMMENTS AND CODE
C
C VERSION: 1.3                          DATE: 06-08-96
C MODIFIED: TIM HAMMOND 
C               - CHANGED OUTPUT UNIT IUNT07 FROM 7 TO 17 TO AVOID
C                 CLASHING WITH STANDARD ERROR STREAM 7 ON HP 
C                 WORKSTATIONS.
C VERSION: 1.4
C MODIFIED: HARVEY ANDERSON & RICHARD MARTIN
C               - THE TOTAL BEAM STOPPING COEFFICIENT WAS BEING
C                 ASSEMBLED AS IF THE STOPPPING COEFFICIENTS PRODUCED
C                 BY ADAS310 WAS CALCULATED IN TERMS OF THE ION DENSITY.
C                 THIS WAS INCORRECT BECAUSE ADAS310 PRODUCES BEAM
C                 STOPPING COEFFICIENTS IN TERMS OF THE ELECTRON
C                 DENSITY THEREFORE THE WRONG RECIPE WAS USED TO 
C                 ASSEMBLE THE TOTAL BEAM STOPPING COEFFICIENT. THIS
C                 WAS CORRECTED.
C               - THE DENSITY AT WHICH THE BEAM STOPPING COEFFICIENTS
C                 WERE BEING EVALUATED AT WAS INCORRECT. AN EFFECTIVE
C                 DENISITY SHOULD HAVE BEEN EMPLOYED. THIS WAS CORRECTED.
C               - ONE SHOULD BE CAREFUL ABOUT THIS CODE BECAUSE THE 
C                 DENSITY IN THE ADF21 TYPE FILE IS THE ELECTRON DENSITY,
C                 HOWEVER THE BEAM STOPPING COEFFICIENT IS GIVEN IN TERMS
C                 OF THE ELECTRON DENSITY.
C               - THE TARGET DENSITY READ FROM ADF21 FILE IS THE
C                 ELECTRON DENSITY. THE CORRECTION TO THE EVALUATION
C                 OF THE EFFECTIVE DENSITY WAS DONE IN TERMS OF THE
C                 ION DENSITY. THIS WAS CORRECTED SO THAT THE EFFECTIVE
C                 DENSITY IS EVALUATED IN TERMS OF THE ELECTRON DENSITY.
C                 SEE THE SUBROUTINE C4SPLN.FOR.   20/12/96
C               - MODIFIED THE CALLING STRUCTURE OF THE SUBROUTINE
C                 C4OUT0 TO ENABLE THE PARAMETER FACT1 TO BE PASSED
C                 TO THE SUBROUTINE.  9/1/96 
C               - INCREASED THE PARAMETER MXBE FROM 15 TO 20 TO ALLOW
C                 MORE BEAM ENERGIES TO BE READ FROM THE ADF21 TYPE
C                 FILE. I SET THE VALUE TO 20 BECAUSE THIS IS THE
C                 MAXIMUM NUMBER OF VALUES THAT CAN BE ENTERED USING
C                 THE IDL TABLE.
C                 14/01/96
C               - INCREASED THE PARAMERS MXTT AND MXTD FROM 10 TO
C                 25. ALSO INCREASED THE SIVE OF MXBE AND MXREQ.
C               - INTRODUCED THE PARAMETER FACT2.
C               - SWAPPED MEANING OF 'TRUE' AND 'FALSE' FOR LPEND.
C               - FIXED BUG SO THAT MULTIPLE RUNS CAN BE MADE WITHOUT CRASHING
C               - ADDED TFLAG. 28/2/97
C
C VERSION  : 1.5
C DATE     : 18-03-2003
C MODIFIED : LORNE HORTON
C               - ADDED LSET FLAG TO ALLOW FASTER SPLINES ON REPEATED
C                 CALLS TO C4SPLN.  THIS MOSTLY FOR COMPATIBILITY WITH
C                 CXBMS.
C
C VERSION  : 1.6                          
C DATE     : 06-02-2004
C MODIFIED : Martin O'Mullane
C              - Replace c4data with xxdata_21. Note that the adf21
C                file is now closed in the reading routine.
C              - Cosmetic tidy-up of code.
C
C VERSION  : 1.7
C DATE     : 10-12-2004
C MODIFIED : Martin O'Mullane
C              - Merge v1.5 and v1.6 from separate developments. 
C              - Cosmetic code clean-up. 
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    IUNT07       , IUNT10       , MXIT         ,
     &           MXBE         , MXTD         , MXTT         ,
     &           MXREQ        , MXDEG        , NFIT
      INTEGER     PIPEIN  
C-----------------------------------------------------------------------
      LOGICAL    LMLOG
C-----------------------------------------------------------------------
      PARAMETER( IUNT07 = 17  , IUNT10 = 10  , MXIT = 10    ,
     &           MXBE   = 25  , MXTD   = 25  , MXTT = 25    ,
     &           MXREQ  = 25  , MXDEG  = 19  , NFIT = 100   )
      PARAMETER (PIPEIN = 5   )
C-----------------------------------------------------------------------
      PARAMETER( LMLOG = .TRUE. )
C-----------------------------------------------------------------------
      INTEGER    IPAN    , NSITYP  , IFIT    , NREQ    , I       ,
     &           J       , TFLAG
      INTEGER    ISTOP   ,I4UNIT
C-----------------------------------------------------------------------
      REAL*8     TOLVAL  , XL1     , XU1     , YL1     , YU1     ,
     &           FACT1   , WT      , FACT2          
C-----------------------------------------------------------------------
      LOGICAL    LGHOST  , LPEND   , LFSEL   , LOSEL   ,
     &           LGRD1   , LDEF1
      LOGICAL    LGRAPH  , L2FILE  , LSET
C-----------------------------------------------------------------------
      CHARACTER  DATE*8    , REP*3     , TITLE*40 , CADAS*80
      CHARACTER  SAVFIL*80 , LINE1*133 , DATASET*44
C-----------------------------------------------------------------------
      INTEGER    ITZ(MXIT)     , NBE(MXIT)     , NTDENS(MXIT)  ,
     &           NTTEMP(MXIT)  , NCOEF(MXIT)
C-----------------------------------------------------------------------
      LOGICAL    LINTOT(MXREQ)
      LOGICAL    LIBMA(MXREQ,MXIT)      , LIDNA(MXREQ,MXIT)      ,
     &           LITIA(MXREQ,MXIT)      , LINION(MXREQ,MXIT)
C-----------------------------------------------------------------------
      REAL*8     BEREF(MXIT)    , TDREF(MXIT)    , TTREF(MXIT)    ,
     &           SVREF(MXIT)    , BEMNMX(4)      , TDMNMX(4)      ,
     &           TTMNMX(4)      , SIFRAC(MXIT)   , UBMENG(MXREQ)  ,
     &           UTDENS(MXREQ)  , UTTEMP(MXREQ)  , BSTOT(MXREQ)   ,
     &           ZEFF(MXREQ)     
      REAL*8     BE(MXBE,MXIT)          , TDENS(MXTD,MXIT)       ,
     &           TTEMP(MXTT,MXIT)       , SVT(MXTT,MXIT)         ,
     &           BSION(MXREQ,MXIT)      , XFIT(NFIT,MXIT+1)      ,
     &           YFIT(NFIT,MXIT+1)      , FCOEF(MXDEG+1,MXIT+1)
      REAL*8     SVED(MXBE,MXTD,MXIT)
C-----------------------------------------------------------------------
      CHARACTER  DSNIN(MXIT)*80    , TSYM(MXIT)*2      ,
     &           FINFO(MXIT+1)*80  , DSNSAV(MXIT)*80
C-----------------------------------------------------------------------
      DATA       DSNSAV /MXIT*' '/
      SAVE       DSNSAV
      SAVE       ITZ      , TSYM,
     &           BEREF    , TDREF    , TTREF    , SVREF   ,
     &           NBE      , BE       , NTDENS   , TDENS   ,
     &           NTTEMP   , TTEMP    , SVT      , SVED
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C *************************** MAIN PROGRAM ****************************
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C SET MACHINE DEPENDANT ADAS CONFIGURATION VALUES.
C-----------------------------------------------------------------------

      CALL XX0000

C-----------------------------------------------------------------------
C GET CURRENT DATE.
C-----------------------------------------------------------------------

      CALL XXDATE( DATE )

C-----------------------------------------------------------------------
C INITIALIZE VALUES.
C-----------------------------------------------------------------------

      LGHOST = .FALSE.
      LSET   = .TRUE.

C-----------------------------------------------------------------------
C START OF CONTROL LOOP.
C-----------------------------------------------------------------------

    1 CONTINUE
      IPAN = 0

C-----------------------------------------------------------------------
C GET INPUT DATA SET NAME FROM ISPF PANELS.
C-----------------------------------------------------------------------

      CALL C4SPF0( MXIT , REP , NSITYP , DSNIN )

C-----------------------------------------------------------------------
C IF PROGRAM RUN IS COMPLETED: END RUN.
C-----------------------------------------------------------------------

      IF ( REP .EQ. 'YES' ) THEN
         GOTO 9999
      ENDIF

C-----------------------------------------------------------------------
C CHECK TO SEE IF ANY OF THE INPUT FILES HAVE CHANGED. IF SO, FORCE
C A COMPLETE RE-READ AND RE-SPLINE
C-----------------------------------------------------------------------
C
      DO I = 1, NSITYP
         IF (DSNIN(I) .NE. DSNSAV(I)) LSET = .TRUE.
         DSNSAV(I) = DSNIN(I)
      END DO

C-----------------------------------------------------------------------
C FETCH DATA FROM SELECTED DATA SETS AND CHECK CONSISTENT WITH MEMBER
C NAME.
C-----------------------------------------------------------------------

      IF (LSET) THEN
      
         DO 3 I = 1 , NSITYP

            CALL xxdata_21( IUNT10      , MXBE        , MXTD        ,
     &                      MXTT        , ITZ(I)      , TSYM(I)     ,
     &                      BEREF(I)    , TDREF(I)    , TTREF(I)    ,
     &                      SVREF(I)    , NBE(I)      , BE(1,I)     ,
     &                      NTDENS(I)   , TDENS(1,I)  , NTTEMP(I)   ,
     &                      TTEMP(1,I)  , SVT(1,I)    , SVED(1,1,I) ,
     &                      DSNIN(I)   )


            CALL C4CHKZ( DSNIN(I) , TSYM(I) , ITZ(I) )

    3    CONTINUE
      ENDIF

C-----------------------------------------------------------------------
C FIND RANGE OF MINS AND MAXS FOR BEAM ENERGIES, AND TARGET DENSITY AND
C TEMPERATURE.
C-----------------------------------------------------------------------

      CALL C4MNMX( NSITYP , MXBE , NBE    , BE    , BEMNMX  )
      CALL C4MNMX( NSITYP , MXTD , NTDENS , TDENS , TDMNMX  )
      CALL C4MNMX( NSITYP , MXTT , NTTEMP , TTEMP , TTMNMX  )

C-----------------------------------------------------------------------
C GET USER-DEFINED VALUES (FROM ISPF PANELS).
C-----------------------------------------------------------------------

    4 CONTINUE

      CALL C4ISPF( MXIT   , MXREQ  , NSITYP , IPAN   ,
     &             LPEND  , TITLE  , LFSEL  , LOSEL  ,
     &             TOLVAL , SIFRAC , IFIT   , NREQ   ,
     &             UBMENG , UTDENS , UTTEMP , 
     &             BEMNMX , TDMNMX , TTMNMX ,
     &             NBE    , NTDENS , NTTEMP , 
     &             BEREF  , TDREF  , TTREF  ,
     &             BE     , MXBE   , TTEMP  , MXTT   ,
     &             TDENS  , MXTD )

C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------

      READ(PIPEIN,*) ISTOP
      IF (ISTOP.EQ.1) GOTO 9999
      IF ( .NOT.LPEND ) GOTO 1

C-----------------------------------------------------------------------
C PRODUCED CORRECT WIEGHT TO ENABLE THE BEAM STOPPING
C COEFFICIENT TO BE EVALUATED AT THE EFFECTIVE DENSITY.
C
C HERE ZEFF IS NOT ALLOWED TO VARY ALONG THE REQUEST VECTOR.
C FOR COMPATIBILITY WITH OTHER ROUTINES CALLING C4SPLN (READADF21)
C PASS ZEFF AS A VECTOR.
C-----------------------------------------------------------------------

      FACT1 = 0.0D0
      FACT2 = 0.0D0
      DO   I = 1, NSITYP                
                FACT1= ( ITZ(I)**2 ) * SIFRAC(I) + FACT1
                FACT2=   ITZ(I)*SIFRAC(I) + FACT2
      END DO  
      DO I = 1, NREQ
         ZEFF(I) = FACT1/FACT2
      END DO

C-----------------------------------------------------------------------
C PERFORM SPLINE ON USER-DEFINED VALUES.
C-----------------------------------------------------------------------

      CALL C4SPLN( MXBE   , MXTD   , MXTT   , MXREQ  ,
     &             NREQ   , UBMENG , UTDENS , UTTEMP ,
     &             NSITYP , SVREF  , NBE    , BE     ,
     &             NTDENS , TDENS  , NTTEMP , TTEMP  ,
     &             SVT    , SVED   , BSION  , LIBMA  ,
     &             LIDNA  , LITIA  , ZEFF   , ITZ    ,
     &             LSET   )

C-----------------------------------------------------------------------
C WEIGHT STOPPING ION COEFFICIENTS 
C
C THE TOTAL STOPPING COEFFICIENT IS
C EVALUATED IN TERMS OF THE ELECTRON
C DENSITY SINCE ADAS310 PRODUCES STOPPING COEFFICIENTS
C IN TERMS OF THE ELECTRON DENSITY
C-----------------------------------------------------------------------

      DO 6 J=1,NREQ
        WT=0.0D0
        DO 5 I=1,NSITYP
            BSION(J,I)=BSION(J,I)*SIFRAC(I)*ITZ(I)
            WT=WT+( ITZ(I)*SIFRAC(I) )
    5   CONTINUE
        DO 22 I=1,NSITYP
            BSION(J,I)=BSION(J,I)/WT
            LINION(J,I) = LIBMA(J,I) .AND. LIDNA(J,I) .AND. LITIA(J,I)
   22   CONTINUE
    6 CONTINUE  

C-----------------------------------------------------------------------
C CALCULATE TOTAL STOPPING ION COEFFICIENT AT EACH REQUESTED TRIPLET.
C-----------------------------------------------------------------------

      DO 7 I = 1 , NREQ
         BSTOT(I) = 0.0D0
         LINTOT(I) = .TRUE.
         DO 8 J = 1 , NSITYP
            BSTOT(I) = BSTOT(I) + BSION(I,J)
            LINTOT(I) = LINTOT(I) .AND. LINION(I,J)
    8    CONTINUE
    7 CONTINUE

C-----------------------------------------------------------------------
C FIT MINIMAX POLYNOMIAL IF REQUESTED ( BUT ONLY IF 'NREQ' > 2).
C-----------------------------------------------------------------------

      IF ( NREQ .LE. 2 ) LFSEL = .FALSE.

      IF ( LFSEL ) THEN

         DO 9 I = 1 , NSITYP
            IF ( IFIT .EQ. 1 ) THEN
               CALL XXMNMX( LMLOG      , MXDEG      , TOLVAL     ,
     &                      NREQ       , UBMENG     , BSION(1,I) ,
     &                      NFIT       , XFIT(1,I)  , YFIT(1,I)  ,
     &                      NCOEF(I)   , FCOEF(1,I) , FINFO(I)    )
            ELSE IF ( IFIT .EQ. 2 ) THEN
               CALL XXMNMX( LMLOG      , MXDEG      , TOLVAL     ,
     &                      NREQ       , UTDENS     , BSION(1,I) ,
     &                      NFIT       , XFIT(1,I)  , YFIT(1,I)  ,
     &                      NCOEF(I)   , FCOEF(1,I) , FINFO(I)     )
            ELSE IF ( IFIT .EQ. 3 ) THEN
               CALL XXMNMX( LMLOG      , MXDEG      , TOLVAL     ,
     &                      NREQ       , UTTEMP     , BSION(1,I) ,
     &                      NFIT       , XFIT(1,I)  , YFIT(1,I)  ,
     &                      NCOEF(I)   , FCOEF(1,I) , FINFO(I)     )
            ENDIF
            WRITE(I4UNIT(-1),1000) FINFO(I)(1:79)
    9    CONTINUE

         I = NSITYP + 1
         IF ( IFIT .EQ. 1 ) THEN
            CALL XXMNMX( LMLOG      , MXDEG      , TOLVAL     ,
     &                   NREQ       , UBMENG     , BSTOT      ,
     &                   NFIT       , XFIT(1,I)  , YFIT(1,I)  ,
     &                   NCOEF(I)   , FCOEF(1,I) , FINFO(I)     )
         ELSE IF ( IFIT .EQ. 2 ) THEN
            CALL XXMNMX( LMLOG      , MXDEG      , TOLVAL     ,
     &                   NREQ       , UTDENS     , BSTOT      ,
     &                   NFIT       , XFIT(1,I)  , YFIT(1,I)  ,
     &                   NCOEF(I)   , FCOEF(1,I) , FINFO(I)     )
         ELSE IF ( IFIT .EQ. 3 ) THEN
            CALL XXMNMX( LMLOG      , MXDEG      , TOLVAL     ,
     &                   NREQ       , UTTEMP     , BSTOT      ,
     &                   NFIT       , XFIT(1,I)  , YFIT(1,I)  ,
     &                   NCOEF(I)   , FCOEF(1,I) , FINFO(I)     )
         ENDIF
         WRITE(I4UNIT(-1),1000) FINFO(I)(1:79)

      ENDIF

C-----------------------------------------------------------------------
C ADDED THE NEW CALL TO C4SPF1 ROUTINE WHICH COMMUNICATES WITH IDL VIA
C A PIPE TO GET USER SELECTED OUTPUT OPTIONS
C-----------------------------------------------------------------------

300   CONTINUE
      CALL C4SPF1( LFSEL         , 
     &             LGRAPH        , L2FILE        , SAVFIL   ,
     &             LPEND         , CADAS         , LINE1    ,
     &             DATASET)

C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------

      READ(PIPEIN,*) ISTOP      
      IF (ISTOP.EQ.1) GOTO 9999
      IF (.NOT.LPEND) GOTO 4
      

C-----------------------------------------------------------------------
C READ FLAG DETERMINING IF ADF21 (0) OR ADF22 (1) DATACLASS USED
C-----------------------------------------------------------------------

      READ(PIPEIN,*) TFLAG

C-----------------------------------------------------------------------
C OUTPUT TEXT RESULTS.
C-----------------------------------------------------------------------

      IF (L2FILE) THEN
          OPEN(UNIT=IUNT07, FILE = SAVFIL, STATUS='UNKNOWN')
          CALL C4OUT0( IUNT07 , MXREQ  , MXDEG  , DATE   ,
     &                 TITLE  , NSITYP , DSNIN  , TSYM   ,
     &                 ITZ    , SIFRAC , NREQ   , UBMENG ,
     &                 UTDENS , UTTEMP , BSTOT  , LINTOT ,
     &                 BSION  , LINION , LFSEL  , IFIT   ,
     &                 NCOEF  , FCOEF  , FINFO  , CADAS  ,
     &                 LINE1  , DATASET, FACT1  , TFLAG  )
          CLOSE(IUNT07)
      ENDIF

C-----------------------------------------------------------------------
C OUTPUT GRAPHICAL RESULTS.
C-----------------------------------------------------------------------

      IF (LGRAPH) THEN
          CALL C4OUTG( LGHOST , LGRD1  , TITLE  , DATE   ,
     &                 IFIT   , NSITYP , DSNIN  , TSYM   ,
     &                 ITZ    , SIFRAC , MXREQ  , NREQ   ,
     &                 UBMENG , UTDENS , UTTEMP , BSTOT  ,
     &                 BSION  , LFSEL  , NFIT   , XFIT   ,
     &                 YFIT   , FINFO  , LDEF1  , XL1    ,
     &                 XU1    , YL1    , YU1               )
      ENDIF

C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------

          READ(PIPEIN,*) ISTOP
          IF (ISTOP.EQ.1) GOTO 9999

C-----------------------------------------------------------------------

      GOTO 300

C-----------------------------------------------------------------------
C CLOSE I/O FILES.
C-----------------------------------------------------------------------

 9999 CONTINUE

      STOP

C-----------------------------------------------------------------------

 1000 FORMAT( / 1X , 'MINIMAX INFORMATION:' / 1X , 20('-')
     &        / 1X , 1A79 / )

C-----------------------------------------------------------------------

      END
