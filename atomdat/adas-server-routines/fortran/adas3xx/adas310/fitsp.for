CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas310/fitsp.for,v 1.1 2004/07/06 13:55:05 whitefor Exp $ Date $Date: 2004/07/06 13:55:05 $
CX
       SUBROUTINE FITSP(X,XA,N,YAA,Y,DY,I0,C1,C2,C3,C4,ISW)             
C
       IMPLICIT REAL*8(A-H,O-Z)                                        
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: FITSP **********************
C
C PURPOSE UNKNOWN
C
C NOTES: THIS ROUTINE IS NOT YET PROPERLY ANNOTATED
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 18-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER SCCS CONTROL
C
C-----------------------------------------------------------------------
C
       DIMENSION YAA(10),TA(10),XA(10)                                
       DIMENSION C1(10,9),C2(10,9),C3(10,9),C4(10,9)                 
       DIMENSION CT1(9),CT2(9),CT3(9),CT4(9)                        
       DO 3 I=1,N                                                  
       T=YAA(I+I0)                                                
       IF(ISW.GT.0)T=DLOG(T)                                     
    3  TA(I)=T                                                  
       DO 24 J=2,N                                             
       J1=J-1                                                 
       CT1(J1)=0.0                                           
       CT2(J1)=0.0                                          
       CT3(J1)=0.0                                         
       CT4(J1)=0.0                                        
       DO 23 I=1,N                                       
       CT1(J1)=CT1(J1)+C1(I,J1)*TA(I)                   
       CT2(J1)=CT2(J1)+C2(I,J1)*TA(I)                  
       CT3(J1)=CT3(J1)+C3(I,J1)*TA(I)                 
   23  CT4(J1)=CT4(J1)+C4(I,J1)*TA(I)                
   24  CONTINUE                                     
       DO 27 J=2,N                                 
       IF(X.GT.XA(J))GO TO 27                     
       XB=0.5D0*(XA(J-1)+XA(J))                  
       J1=J-1                                   
       GO TO 25                                
   27  CONTINUE                               
       XB=0.5*(XA(N-1)+XA(N))                
       J1=N-1                               
   25  XB=X-XB                             
       Y=CT1(J1)+XB*(CT2(J1)+XB*(CT3(J1)+XB*CT4(J1)))                   
       DY=CT2(J1)+XB*(2.0D0*CT3(J1)+XB*3.0D0*CT4(J1))                  
       IF(ISW.LE.0)GO TO 26                                           
       Y=DEXP(Y)                                                     
       DY=DY*Y                                                      
   26  RETURN                                                      
       END                                                        
