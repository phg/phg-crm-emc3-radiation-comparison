CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas310/abi.for,v 1.2 2007/05/17 17:03:02 allan Exp $ Date $Date: 2007/05/17 17:03:02 $
CX
       FUNCTION ABI(OA,YA,FA,A,B,N)                                     
C
       IMPLICIT REAL*8(A-H,O-Z)                                        
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 FUNCTION: ABI **************************
C
C-----------------------------------------------------------------------
C  PURPOSE: EVALUATE INNER INTEGRALS FOR RATE COEFFICIENTS.
C  
C  ABI IS CALLED BY QHIOCH. THE PROCEDURES ALLOW FOR PROJECTILE
C  AND TARGET MASSES.
C
C  ********** H.P. SUMMERS,JET            18 FEB 1987  **************** 
C  INPUT                                                               
C      OA=VECTOR OF CROSS-SECTIONS (CM2)                              
C      YA=VECTOR OF REDUCED RELATIVE SPEEDS. 1ST VALUE IS AT THRESHOLD  
C      FA=VECTOR OF ALPHAS  (12TH VALUE IS FOR EXTRAPOLATION, PROVIDED 
C         EXPLICITLY. EXTRAPOLATION BELOW YA(1) IS BASED ON FA(1))    
C         N.B. OA(1) MUST BE NON-ZERO.                               
C             (OA,YA AND FA ARE OF FIXED LENGTH =12)                
C                                                                  
C      A=LOWER INTEGRAL LIMIT                                     
C      B=UPPER INTEGRAL LIMIT                                    
C      N=NUMBER OF CROSS-SECTIONS
C  OUTPUT                                                       
C      ABI=DEFINITE INTEGRAL.                                  
C
C NOTES: THIS ROUTINE IS NOT YET PROPERLY ANNOTATED
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 18-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER SCCS CONTROL
C
C VERSION: 1.2				DATE: 16-05-07
C MODIFIED: Allan Whiteford
C	    - Updated comments as part of subroutine documentation
C             procedure.
C
C-----------------------------------------------------------------------
C
       DIMENSION OA(24),YA(24),FA(24)                                  
       I=0                                                            
   10  I=I+1                                                         
       IF(I.EQ.N+1)GO TO 15                                         
       IF(A.GT.YA(I))GO TO 10                                      
   15  I=I-1                                                      
       IF(I.EQ.0)I=1                                             
       J=0                                                      
   20  J=J+1                                                   
       IF(J.EQ.N+1)GO TO 25                                   
       IF(B.GT.YA(J))GO TO 20                                
   25  J=J-1                                                
       IF(J.EQ.0)J=1                                       
       IF(I.EQ.J)GO TO 50                                 
       T1=(OA(I+1)*YA(I+1)**3-OA(I)*(YA(I)/A)**(2.0D0*FA(I))*A**3)/     
     &(3.0D0-2.0D0*FA(I))                                              
       T2=(OA(J)*(YA(J)/B)**(2.0D0*FA(J))*B**3-OA(J)*YA(J)**3)/       
     &(3.0D0-2.0D0*FA(J))                                            
       T3=0.0D0                                                     
       IF(I.GT.J-2)GO TO 45                                        
       DO 40 K=I+1,J-1                                            
   40  T3=T3+(OA(K+1)*YA(K+1)**3-OA(K)*YA(K)**3)/(3.0D0-2.0D0*FA(K))    
   45  ABI=T1+T2+T3                                                    
       RETURN                                                         
   50  ABI=OA(I)*YA(I)**(2.0D0*FA(I))*(B**(3.0D0-2.0D0*FA(I))-      
     &A**(3.0D0-2.0D0*FA(I)))/(3.0D0-2.0D0*FA(I))                  
       RETURN                                                     
       END                                                        
