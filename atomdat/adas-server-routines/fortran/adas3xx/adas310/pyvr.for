CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas310/pyvr.for,v 1.2 2007/05/17 17:03:02 allan Exp $ Date $Date: 2007/05/17 17:03:02 $
CX
       SUBROUTINE PYVR(Y,Z1,PY)                                         
C
       IMPLICIT REAL*8(A-H,O-Z)                                        
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: PYVR ***********************
C
C  PURPOSE: CALCULATES VAN REGEMORTER'S P FACTOR FOR ELECTRON
C           COLLISIONS WITH ATOMS AND IONS.                                                  
C
C  INPUT                                                            
C      Y=ATE*(1/V1**2+1/V2**2)  WHERE                              
C                ATE=1.5789D5*Z1*Z1/TE                            
C                TE=ELECTRON TEMPERATURE (K)                     
C                V1=INITIAL EFFECTIVE PRINCIPAL QUANTUM NUMBER  
C                V2=FINAL EFFECTIVE PRINCIPAL QUANTUM NUMBER   
C      Z1=TARGET ION CHARGE+1                                 
C      PY=P-FACTOR                                           
C
C NOTES: THIS ROUTINE IS NOT YET PROPERLY ANNOTATED
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 16-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C VERSION: 1.2                          DATE: 16-05-07
C MODIFIED: Allan Whiteford
C             - Updated comments as part of subroutine documentation
C               procedure.
C-----------------------------------------------------------------------
C
       IF(Y-0.04)9,9,10                                     
    9  PY=-0.2756644*(DLOG(Y)+0.41)                        
       GO TO 15                                           
   10  IF(Z1-1.0)11,11,14                                
   11  IF(Y-1.0)12,12,13                                
   12  PY=(0.0196*Y+0.0882)/(Y+0.075)                  
       GO TO 15                                       
   13  Y1=0.066/DSQRT(Y)                             
       PY=Y1*(1.0+8.0*Y1)                           
       GO TO 15                                    
   14  Y1=1.0/Y                                   
       PY=0.2+Y1*(0.04-0.00068*Y1)               
   15  RETURN                                   
       END                                      
