CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas310/pypr.for,v 1.2 2007/05/17 17:03:02 allan Exp $ Date $Date: 2007/05/17 17:03:02 $
CX
       SUBROUTINE PYPR(E,E11,N,N11,EM,Z1,PHI,WI,WJ,TE,INTD,PY,RDEXC)   
C
       IMPLICIT REAL*8(A-H,O-Z)                                         
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: PYPR ***********************
C
C  PURPOSE: CALCULATES PY FACTOR (CF. VAN REGEMORTER,1962) USING
C           PERCIVAL,RICHARD AND COWORKER CROSS-SECTIONS.                                       
C
C  VALID ONLY FOR ELECTRON INDUCED TRANSITIONS BETWEEN WHOLE PRINCIPAL
C  QUANTUM SHELLS IN HYDROGEN AND HYDROGENIC IONS, FOR N,N11>4       
C  HOWEVER ADJUSTMENTS MADE TO ALLOW USE OF FORMULAE FOR N<4        
C  ********* H.P.SUMMERS, JET  12 NOVEMBER 1984  ***************   
C  INPUT                                                          
C      E=1/V**2 WITH V THE INITIAL EFFECTIVE PRINCIPAL QUANTUM NUMBER   
C      E11=1/V11**2 WITH V11 THE FINAL EFFECTIVE PRINCIPAL QUANTUM NUMBE
C      N=INITIAL PRINCIPAL QUANTUM NUMBER                              
C      N11=FINAL PRINCIPAL QUANTUM NUMBER (REQUIRE N11>N AND V11>V)   
C      EM=REDUCED MASS OF COLLIDING PARTICLE (MUST BE 1.0 IN THIS CASE)
C      Z1=TARGET ION CHARGE +1                                        
C      PHI=(IH/EIJ)F WITH EIJ=TRANSITION ENERGY, F=ABS. OSCILL. STRENGTH
C      WI=STATISTICAL WEIGHT OF INITIAL LEVEL                           
C      WJ=STATISTICAL WEIGHT OF FINAL LEVEL                            
C      TE=ELECTRON TEMPERATURE(K)                                     
C      INTD=<3 FOR TWO POINT GAUSSIAN QUADRATURE.                    
C          = 3 FOR THREE POINT GAUSSIAN QUADRATURE                  
C          =>3 FOR FOUR POINT GAUSSIAN QUADRATURE                  
C  OUTPUT                                                         
C      PY=P FACTOR                                               
C      RDEXC=DEXCITATION RATE COEFFICIENT (CM+3 SEC-1)              
C
C NOTES: THIS ROUTINE IS NOT YET PROPERLY ANNOTATED
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 16-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C VERSION: 1.2                          DATE: 16-05-07
C MODIFIED: Allan Whiteford
C             - Updated comments as part of subroutine documentation
C               procedure.
C
C-----------------------------------------------------------------------
C
       DIMENSION A(4),H(4)                                         
       D=E-E11                                                    
       EIJ=D*Z1*Z1                                               
       ZCOL=Z1-1.0                                              
       PY=0.0                                                  
       IF(INTD-3)10,20,30                                     
   10  A(1)=0.585786437                                      
       A(2)=3.414213562                                     
       H(1)=0.853553390                                    
       H(2)=0.146446609                                   
       GO TO 40                                          
   20  A(1)=0.415774556                                 
       A(2)=2.294280360                                
       A(3)=6.289945083                                              
       H(1)=0.711093010                                               
       H(2)=0.278517733                                                
       H(3)=0.010389256                                                 
       GO TO 40                                        
   30  A(1)=0.322547689                                 
       A(2)=1.745761101                                  
       A(3)=4.536620297                                   
       A(4)=9.395070912                                    
       H(1)=0.603154104                                     
       H(2)=0.357418692                                      
       H(3)=0.038887908                                       
       H(4)=0.000539294                                        
   40  DO 50 I=1,INTD                                           
       EI=A(I)*TE/157890.0+EIJ                                   
       EIQ=EI*QPR78(Z1,N,N11,EI,PHI)                              
       PY=PY+H(I)*0.552*EIQ/(8.0*PHI)                              
   50  CONTINUE                                                     
       EM2=DSQRT(EM)                                                 
       PY=PY/EM2                                                      
       RDEXC=3.15D-7*DSQRT(1.5789D5/TE)*PHI*WI*PY/WJ                   
       RETURN                                                           
       END                                                               
