CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas310/rqlnew.for,v 1.3 2007/05/17 17:03:02 allan Exp $ Date $Date: 2007/05/17 17:03:02 $
CX
       FUNCTION RQLNEW(Z,N11,N,PHI,ZP, AMSIMP,TP,VDISP)
C
       IMPLICIT REAL*8(A-H,O-Z)
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 FUNCTION: RQLNEW ***********************
C
C-----------------------------------------------------------------------
C  PURPOSE: EVALUATES ION IMPACT RATE COEFFICIENTS OF LODGE,
C           PERCIVAL & RICHARDS
C
C  (ALTERNATIVE TO RQLPR  WITH BETTER MAXWELL AVERAGING BUT SLOWER)
C
C  ROUTINE MUST RETURN EXCITATION RATE COEFFICIENT IF N11.LT.N   AND
C  DEXCITATION RATE COEFFICIENT IF N11.GT.N
C
C  NOTE THAT THE RELATION BETWEEN INVERSE PROCESSES IS DETERMINED BY THE
C  TEMPERATURE TP AND THE SPEED VDISP
C  THE TREATMENT GIVEN IS APPROXIMATE EXCEPT IN THE LIMITS VDISP=0 OR
C  VDISP >> DSQRT(2*TP/AMSIMP)
C
C  INPUT
C      Z      = TARGET ION CHARGE+1
C      N11    = PRINCIPAL QUANTUM  NUMBER OF INITIAL LEVEL
C      N      = PRINCIPAL QUANTUM  NUMBER OF FINAL   LEVEL
C      PHI    = (IH/EIJ)F(N ---> N'')
C      ZP     = PROJECTILE CHARGE
C      AMSIMP = PROJECTILE MASS (PROTON UNITS)
C      TP     = PROJECTILE ION TEMPERATURE (K)
C      VDISP  = CONSTANT MEAN SPEED SHIFT FOR THE COLLISION (CM/SEC)
C                     (DESCRIBES BEAM PLASMA SITUATIONS)
C
C  OUTPUT
C      RQLNEW = RATE COEFFICIENT (CM**3 SEC-1)
C
C
C  ********* H.P. SUMMERS, JET                 2 JUL 1991   ************
C
C NOTES: THIS ROUTINE IS NOT YET PROPERLY ANNOTATED
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 16-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C VERSION: 1.2                          DATE: 08-02-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - REMOVED SUPERFLUOUS VARIABLES
C
C VERSION: 1.3                          DATE: 16-05-07
C MODIFIED: Allan Whiteford
C             - Updated comments as part of subroutine documentation
C               procedure.
C
C-----------------------------------------------------------------------
C
       DIMENSION XA(9),WXA(9)
       DIMENSION EA(24),OA(24),YA(24),FA(24)
C----------------------------------------------------------------------
C  GAUSS-LAGUERRE DATA (9PT)
C----------------------------------------------------------------------
       DATA XA/0.1523222277D0,0.8072200227D0,2.0051351556D0,
     &         3.7834739733D0,6.2049567778D0,9.3729852516D0,
     &         13.4662369110D0,18.8335977889D0,26.3740718909D0/
       DATA WXA/3.36126421798D-1,4.11213980424D-1,1.99287525371D-1,
     &          4.74605627657D-2,5.59962661079D-3,3.05249767093D-4,
     &          6.59212302608D-6,4.11076933035D-8,3.29087403035D-11/
C----------------------------------------------------------------------
       DATA LCK,LWRK/28,160/
C----------------------------------------------------------------------
       DATA NN/22/
       DATA (EA(I),I=1,24)/1.00D3,1.50D3,2.00D3,3.00D3,5.00D3,7.00D3,
     &                     1.00D4,1.50D4,2.00D4,3.00D4,4.00D4,5.00D4,
     &                     6.00D4,7.00D4,8.00D4,1.00D5,1.50D5,2.00D5,
     &                     3.00D5,5.00D5,7.00D5,1.00D6,0.00D0,0.00D0/
       DATA IORD,EM1,ALFA /1,2.0D0,1.0D0/
C----------------------------------------------------------------------
       IF(Z.EQ.ZS .AND. N11.EQ.NS .AND.  N.EQ.N11S .AND.  PHI.EQ.PHIS
     &            .AND. ZP.EQ.ZPS .AND. TP.EQ.TPS .AND. VDISP.EQ.VDISPS
     &            .AND. AMSIMP.EQ.AMSIMS) THEN
         IFLAG=-1*IFLAG
         GOTO 5000
       ENDIF
C----------------------------------------------------------------------
C  SET PARAMETERS
C----------------------------------------------------------------------
       EMP=1.67265D-27
       TEV=TP/1.1605D+04
       TRH=TEV/13.6042
C
       IF(N11.GT.N) THEN
         NU=N11
         NL=N
         IFLAG=-1
       ELSE
         NU=N
         NL=N11
         IFLAG=1
       ENDIF
       ENU=NU*NU
       ENL=NL*NL
       EIJRH=Z*Z*(1.0/ENL-1.0/ENU)
       EDRH=0.5*AMSIMP*EMP*VDISP**2*1.0D-04/2.179D-18
C
       REXC=0.0D0
       EPRO=5.2235D-13*VDISP*VDISP
       V=VDISP
       EM2=AMSIMP
       TTAR=TEV
       ETHRES=13.6048D0*EIJRH
C-----------------------------------------------------------------------
C  EVALUATE RATE AT PRECISE ENERGY IF TTAR=0  NB.
C                NB. XENER = ENERGY OF ELECTRON(IN RYD) OF SAME SPEED AS
C                            COLLIDING ION = EPRO/25KEV APPROX.
C-----------------------------------------------------------------------
       IF(TTAR.LE.0.0D0)THEN
           XENER=4.00346D-5*EPRO
           REXC=VDISP*QLPR(Z,NL,NU,XENER,ZP,AMSIMP)
           GO TO 5000
       ENDIF
C-----------------------------------------------------------------------
C  FILL THE CROSS-SECTION VECTOR
C-----------------------------------------------------------------------
       DO 40 I=1,NN
        YA(I)=EA(I)
        XENER=4.00346D-5*EA(I)
        OA(I)=8.7972D-17*QLPR(Z,NL,NU,XENER,ZP,AMSIMP)
   40   CONTINUE
C
C-----------------------------------------------------------------------
C  COMPUTE ALPHAS AND REDUCED SPEEDS
C-----------------------------------------------------------------------
       IF(IORD.EQ.1)EMT=EM2
       IF(IORD.EQ.2)EMT=EM1
       V=1.38377D6*DSQRT(EPRO)
       U=1.38377D6*DSQRT(TTAR/EMT)
       VTHR=1.38377D6*DSQRT(ETHRES*(EM1+EM2)/(EM1*EM2))
       X=V/U
       XRMIN=VTHR/U
       FA(NN)=ALFA
       DO 10 I=1,NN-1
       FA(I)=-DLOG(OA(I)/OA(I+1))/DLOG(YA(I)/YA(I+1))
   10  YA(I)=1.38377D6*DSQRT(YA(I))/U
       YA(NN)=1.38377D6*DSQRT(YA(NN))/U
       SUM=0.0D0
       DO 100 I=1,9
       XI=XA(I)
       SXI=DSQRT(XI)
       ISWIT=1
       IF(X.LT.XRMIN)ISWIT=2
       IF(X.EQ.0.0D0)ISWIT=3
       GO TO (30,50,70),ISWIT
   30  IF(SXI.GE.0.0D0.AND.SXI.LE.X-XRMIN)
     &    F=ABINEW(OA,YA,FA,X-SXI,X+SXI,NN)
       IF(SXI.GT.X-XRMIN.AND.SXI.LE.X+XRMIN)
     &    F=ABINEW(OA,YA,FA,XRMIN,X+SXI,NN)
       IF(SXI.GT.X+XRMIN)F=ABINEW(OA,YA,FA,SXI-X,X+SXI,NN)
       GO TO 100
   50  XXI=XI+(XRMIN-X)**2
       SXXI=DSQRT(XXI)
       IF(SXXI.GE.XRMIN-X.AND.SXXI.LE.X+XRMIN)
     &    F=ABINEW(OA,YA,FA,XRMIN-X,SXXI+X,NN)
       IF(SXXI.GT.X+XRMIN)F=ABINEW(OA,YA,FA,SXXI-X,X+SXXI,NN)
       GO TO 100
   70  XXI=XI+XRMIN**2
       SXXI=DSQRT(XXI)
       K=0
   75  K=K+1
       IF(K.EQ.NN+1)GO TO 80
       IF(SXXI.GT.YA(K))GO TO 75
   80  K=K-1
       IF(K.EQ.0)K=1
       F=XXI*OA(K)*(YA(K)/SXXI)**(2.0D0*FA(K))
       GO TO 100
  100  SUM=SUM+F*WXA(I)
       GO TO (110,120,130),ISWIT
  110  REXC=5.64190D-1*U*SUM/X
       GO TO 5000
  120  REXC=5.64190D-1*U*DEXP(-(XRMIN-X)**2)*SUM/X
       GO TO 5000
  130  REXC=1.12838D0*U*DEXP(-XRMIN*XRMIN)*SUM
C
C---------------------
 5000  IF(IFLAG.LT.0) THEN
       RDEXC=ENL/ENU*REXC*DEXP(EIJRH/(TRH+EDRH))
          RQLNEW=RDEXC
       ELSE
          RQLNEW=REXC
       ENDIF
C
       ZS=Z
       N11S=N11
       NS=N
       PHIS=PHI
       ZPS=ZP
       AMSIMS=AMSIMP
       TPS=TP
       VDISPS=VDISP
C
       RETURN
       END
