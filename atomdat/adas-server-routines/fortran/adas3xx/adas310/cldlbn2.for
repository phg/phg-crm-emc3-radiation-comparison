CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas310/cldlbn2.for,v 1.6 2007/06/27 13:20:38 mog Exp $ Date $Date: 2007/06/27 13:20:38 $
CX
       SUBROUTINE CLDLBN2(exfile,Z0,Z1,ZEFF,DENS,TE,DENSP,TP,BMENER,
     &                    DENSH,W1,NMIN,NMAX,NREP,IMAX,ARED,
     &                    RHS,CIONPT,DRECPT,RRECPT,XRECPT,IECION,
     &                    IEDREC,IERREC,IEXREC,DVEC,ACNST,A1CNST, 
     &                    lpass)
C
       IMPLICIT REAL*8(A-H,O-Z)
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: CLDLBN2 ********************
C
C-----------------------------------------------------------------------
C  PURPOSE: ESTABLISH THE PROJECTED INFLUENCE OF HIGH N-SHELLS IN
C  THE BUNDLE-N COLLISIONAL DIELECTRONIC MODEL ON A SET OF LS OR LSJ
C  RESOLVED LOW LEVEL POPULATION EQUATIONS
C
C  BOTH THE RECOMBINATION AND IONISATION PATHWAYS THROUGH THE HIGH
C  LEVELS ARE TAKEN INTO ACCOUNT AS WELL AS THE INDIRECT COUPLINGS OF
C  LOW RESOLVED LEVELS VIA THE HIGH BUNDLE-N LEVELS.
C
C  THE SUBROUTINE IS USED AS AN ARBITRARY CALL FROM WITHIN THE
C  CONVENTIONAL BNDLEN ROUTINE FOLLOWING ESTABLISHMENT OF THE
C  CONDENSED COLLISIONAL-DIELECTRONIC MATRIX AND RIGHT-HAND SIDE
C
C  THE EXPANSION OF THE PROJECTED MATRICES OVER THE RESOLVED LOWER
C  LEVELS IS DEFINED THROUGH RESOLVED - BUNDLEN INDEXING AND
C  WEIGHTING FRACTION TABLES STORED IN DATA STATEMENTS.
C  THESE ARE HELD FOR COMBINATIONS BASED ON THE A-D-A-S DATA BASE
C  MEMBERS.
C
C  THE ROUTINE PROVIDES TABULAR OUTPUT AND FOR THE MOMENT PREPARES A
C  PASSING FILE FOR FURTHER PROCESSING IN THE A-D-A-S STRUCTURE
C
C  PROCESSING CONTINUES WITH EXECUTION OF THE LOW LEVEL POPULATION
C  CALCULATION PROVIDED THE LOW LEVEL DATA FILE 'REFMEM' IS NON-BLANK
C  OTHERWISE ONLY THE PASSING FILE IS PRODUCED
C
C  INPUT
C      exfile   = DATA SET NAME OF EXPANSION FILE
C      Z0       = NUCLEAR CHARGE
C      Z1       = RECOMBINING ION CHARGE
C      ZEFF     = PLASMA Z EFFECTIVE
C      DENS     = ELECTRON DENSITY (CM-3)
C      TE       = ELECTRON TEMPERATURE (K)
C      DENSP    = PROTON   DENSITY (CM-3)
C      TP       = PROTON   TEMPERATURE (K)
C      BMENER   = NEUTRAL HYDROGEN BEAM ENERGY (EV/AMU)
C      DENSH    = NEUTRAL BEAM HYDROGEN DENSITY (CM-3)
C      W1       = GROUND STATE RADIATION DILUTION FACTOR
C      NMIN     = LOWEST N-SHELL
C      NMAX     = HIGHEST N-SHELL
C      NREP(I)  = SET OF REPRESENTATIVE LEVELS
C      IMAX     = NUMBER OF REPRESENTATIVE LEVELS
C      ARED(I,J)= CONDENSED COLLISONAL-DIELECTRONIC MATRIX (CN SOLUTION)
C      RHS(I)   = CONDENSED RIGHT-HAND-SIDE                (CN SOLUTION)
C      CIONPT(I)= COLLISIONAL IONISATION CONTRIBUTION TO ARED(I,I)
C      DRECPT(I)= DIELECTRONIC RECOMBINATION CONTRIBUTION TO RHS(I)
C      RRECPT(I)= RADIATIVE    RECOMBINATION CONTRIBUTION TO RHS(I)
C      XRECPT(I)= CHARGE EXCHANGE RECOMB.    CONTRIBUTION TO RHS(I)
C      IECION   = 0 ELIMINATE CIONPT FOR LOW LEVELS IN PROJECTION
C                 1 DO NOT ELIMINATE CIONPT.
C      IEDREC   = 0 ELIMINATE DRECPT FOR LOW LEVEL PROJECTION
C                 1 DO NOT ELIMINATE DRECPT.
C      IERREC   = 0 ELIMINATE RRECPT FOR LOW LEVEL PROJECTION
C                 1 DO NOT ELIMINATE RRECPT.
C      IEXREC   = 0 ELIMINATE XRECPT FOR LOW LEVEL PROJECTION
C                 1 DO NOT ELIMINATE XRECPT.
C      DVEC(I)  = CONVERSION FACTOR FOR BN --> POPULATION
C      ACNST    = 1.03928D-13*Z*ATE*DSQRT(ATE)
C      A1CNST   = 6.60074D-24*DENS*(157890.0/TE)**1.5
C
C  OUTPUT
C
C
C  ********** H.P. SUMMERS, JET               8 FEB 1990    ************
C  **********                                24 APR 1990    ************
C  **********                                18 JUL 1991    ************
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C
C UPDATE:  19/01/94 - JONATHAN NASH - TESSELLA SUPPORT SERVICES PLC
C
C          THE FOLLOWING MODIFICATIONS HAVE BEEN MADE TO THE SUBROUTINE:
C
C          1) THE COMPLETE EXPANSION FILE DATA SET NAME IS NOW PASSED
C             INTO THE ROUTINE RATHER THAN JUST THE MEMBER NAME.
C
C          2) THE OUTPUT FILE (UNIT 18) IS NOW OPENED EXTERNAL TO THIS
C             ROUTINE.
C
C  NOTES:  NO ATTEMPT HAS BEEN MADE TO RESTRUCTURE THE ROUTINE. RATHER
C          THE MINIMUM AMOUNT OF WORK TO INTEGRATE THE ROUTINE INTO
C          ADAS310 HAS BEEN COMPLETED.
C
C-----------------------------------------------------------------------
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 16-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C VERSION: 1.2                          DATE: 17-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - CHANGED ACTION=READ TO STATUS=UNKNOWN IN OPEN
C                 STATEMENT
C
C VERSION: 1.3                          DATE: 24-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - COMMENTED OUT CALL TO USINFO AS NO LONGER NEEDED
C
C VERSION: 1.4                          DATE: 08-02-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - REMOVED SUPERFLUOUS VARIABLES
C
C VERSION: 1.5                          DATE: 16-05-07
C MODIFIED: Allan Whiteford
C               -  Updated comments as part of subroutine documentation
C                  procedure.
C
C VERSION : 1.6                          
C DATE    : 26-06-2007
C MODIFIED: Hugh Summers
C               - Revised for heavy species cx. Changed
C                 exmemb to full file name exfile.
C               - Add LPASS to turn on/off output to unit 18 depending on
C                 calling routine; adas310 and adas316 have different
C                 approaches to file management.
C
C-----------------------------------------------------------------------
C
       PARAMETER (NDIM=30,NLDIM=30,NDMAX=550,NDMET=5)
       CHARACTER SEQ*2,SYMBA*2,REFMEM*8
       CHARACTER exfile*(*)
       CHARACTER STRING*80,STRNG1*80,PTSYMA*11,SUBSTRG*11,LVSYMA*26
       CHARACTER LSTRING*133,STRNG2*60
       
       logical lpass
C
C
       DIMENSION NREP(NDIM+1),ARED(NDIM,NDIM),RHS(NDIM),DVEC(NDIM)
CX     DIMENSION ARED2(NDIM,NDIM),RHS2(NDIM)
       DIMENSION CIONPT(NDIM),DRECPT(NDIM),RRECPT(NDIM),XRECPT(NDIM)
       DIMENSION AREDL(NDIM,NDIM),AREDH(NDIM,NDIM),RHSL(NDIM),RHSH(NDIM)
       DIMENSION BREDL(NDIM,NDIM),VEC(NDIM)
       DIMENSION IPOINTA(NDMAX)
       DIMENSION PCRMAT(NDIM,NDIM),PCRRHS(NDIM),LVSYMA(NDIM)
       DIMENSION PEXMAT(NLDIM,NLDIM),PEXRHS(NLDIM)
       DIMENSION AMAT(NDIM,NDIM),RS(NDIM),CHMAT(NDIM,5),RH(NDIM)
       DIMENSION PCION(NDIM),PDREC(NDIM),PRREC(NDIM),PXREC(NDIM)
       DIMENSION PECION(NLDIM),PEDREC(NLDIM),PERREC(NLDIM),PEXREC(NLDIM)
       DIMENSION LSPA(NLDIM),LSHA(NLDIM),LPTA(NLDIM),WGHTA(NLDIM,5)
       DIMENSION SYMBA(30),PTSYMA(5),NSHELA(5),NSPNA(5,5),NLWSTA(5,5)
       DIMENSION PLWSTA(5,5),NPTSPA(5),NSPSYS(5),NCUTP(5),DEPA(5)
       DIMENSION IMETR(NDMET+1)
       NAMELIST /SEQINF/SEQ,REFMEM,NPARNT,NSHEL,NLEV
       DATA LSTRING/'
     &
     &                '/
       DATA IPASS/0/
       DATA SYMBA/'H ','HE','LE','BE','B ','C ','N ','O ','F ','NE',
     &            'NA','MG','AL','SI','P ','S ','CL','AR','K ','CA',
     &            'SC','TI','V ','CR','MN','FE','CO','NI','CU','ZN'/
C-----------------------------------------------------------------------
C  OPEN EXPANSION TABLE FILE FOR REQUIRED ION
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C  CALL SYS6.LOAD(USINFO) TO OBTAIN USER I.D.
C-----------------------------------------------------------------------
CX     CALL USINFO(JOBNAME,DIVUID,ACCT,POINT,SECLEV,IRC)
       IZ0=Z0+0.001
       IZ1=Z1+0.001
       NEL=IZ0-IZ1+1
       OPEN(UNIT=17,FILE=exfile,STATUS='UNKNOWN',ERR=996)
C
       READ(17,SEQINF)
C
       IF(REFMEM.EQ.' ') THEN
           NO7OUT=0
       ELSE
           NO7OUT=1
       ENDIF
C
       READ(17,2004)STRING,STRING
       DO 1 K=1,NPARNT
        PTSYMA(K)=STRING(9+12*K:19+12*K)
    1  CONTINUE
       READ(17,2004)STRING
       DO 2 K=1,NPARNT
        SUBSTRG=STRING(9+12*K:19+12*K)
        READ(SUBSTRG,*)NPTSPA(K)
    2  CONTINUE
       READ(17,2004)STRING
       DO 42 K=1,NPARNT
        SUBSTRG=STRING(9+12*K:19+12*K)
        READ(SUBSTRG,*)NSPSYS(K)
   42  CONTINUE
       READ(17,2004)STRING
       DO 43 K=1,NPARNT
        SUBSTRG=STRING(9+12*K:19+12*K)
        READ(SUBSTRG,*)NCUTP(K)
        DEPA(K)=(Z1/NCUTP(K))**2
   43  CONTINUE
       READ(17,2004)STRING,STRING
       DO 3 K=1,NSHEL
        SUBSTRG=STRING(9+12*K:19+12*K)
        READ(SUBSTRG,*)NSHELA(K)
    3  CONTINUE
       DO 44 IPARNT=1,NPARNT
        READ(17,2004)(STRING,K=1,4)
        NSPIN=NSPSYS(IPARNT)
        DO 4 K=1,NSPIN
         SUBSTRG=STRING(9+12*K:19+12*K)
         READ(SUBSTRG,*)NSPNA(K,IPARNT)
    4   CONTINUE
        READ(17,2004)STRING
        DO 5 K=1,NSPIN
         SUBSTRG=STRING(9+12*K:19+12*K)
         READ(SUBSTRG,*)NLWSTA(K,IPARNT)
    5   CONTINUE
        READ(17,2004)STRING
        DO 6 K=1,NSPIN
         SUBSTRG=STRING(9+12*K:19+12*K)
         READ(SUBSTRG,*)PLWSTA(K,IPARNT)
    6   CONTINUE
   44  CONTINUE
       READ(17,2004)(STRING,K=1,4),STRNG1,STRING
C
       IF(NO7OUT.EQ.1)THEN
           WRITE(7,2002)exfile(1:80),IZ0,IZ1,STRNG1
       ENDIF
C
       if (lpass) WRITE(18,2002)exfile(1:80),IZ0,IZ1,STRNG1
       NMET=0
       DO 8 K=1,NLEV
        READ(17,2004)STRING
        READ(STRING,2005)IND,LVSYMA(K),LSPA(K),LSHA(K),LPTA(K),
     &  (WGHTA(K,J),J=1,NSHEL)
        IF(LVSYMA(K)(1:1).EQ.'*')THEN
            NMET=NMET+1
            IMETR(NMET)=K
        ENDIF
C
        IF(NO7OUT.EQ.1)THEN
            WRITE(7,2005)IND,LVSYMA(K),LSPA(K),LSHA(K),LPTA(K),
     &                   (WGHTA(K,J),J=1,NSHEL)
        ENDIF
C
        if (lpass) WRITE(18,2005)IND,LVSYMA(K),LSPA(K),LSHA(K),LPTA(K),
     &  (WGHTA(K,J),J=1,NSHEL)
    8  CONTINUE
    9  CONTINUE
       CLOSE(17)
C
       IF(NO7OUT.EQ.1)THEN
           WRITE(7,2021)IECION,IEDREC,IERREC,IEXREC
       ENDIF
C
       DO 10 I=1,IMAX
        IPOINTA(NREP(I))=I
   10  CONTINUE
       if (lpass) WRITE(18,2017)DENS,TE,DENSP,TP,BMENER,DENSH,W1
C
C
C      IF(NO7OUT.EQ.1)THEN
C          WRITE(7,2007)
C          WRITE(7,2008)(IPOINTA(I),I=1,IMAX)
C      ENDIF
C
C-----------------------------------------------------------------------
C  ZERO FINAL EXPANDED LOW LEVEL MATRIX AND RIGHT HAND SIDE IN
C  POPULATION REPRESENTATION
C-----------------------------------------------------------------------
       DO 101 I=1,NLEV
        PEXRHS(I)=0.0D0
        DO 100 J=1,NLEV
         PEXMAT(I,J)=0.0D0
  100   CONTINUE
  101  CONTINUE
C-----------------------------------------------------------------------
C  CYCLE THROUGH SPIN SYSTEMS AND PARENTS
C-----------------------------------------------------------------------
       DO 200 IPARNT=1,NPARNT
        NSPIN=NSPSYS(IPARNT)
        DO 190 ISPIN=1,NSPIN
         PHSFAC=PLWSTA(ISPIN,IPARNT)
         RPHSFC=(PHSFAC-1.0D0)/PHSFAC
         NRESL=NSHELA(NSHEL)
         NRESU=NSHELA(1)
         DO 11 K=1,NLEV
          IF(LSPA(K).EQ.NSPNA(ISPIN,IPARNT).AND.LPTA(K).EQ.IPARNT)THEN
              NRESL=MIN0(NRESL,LSHA(K))
              NRESU=MAX0(NRESU,LSHA(K))
          ENDIF
   11    CONTINUE
         SSYSWT=NSPNA(ISPIN,IPARNT)/(2.0D0*NPTSPA(IPARNT))
         IL=0
   12    IL=IL+1
         IF(IL.LT.IMAX.AND.NRESL.NE.NREP(IL))GO TO 12
         IU=IL-1
   13    IU=IU+1
         IF(NRESU.NE.NREP(IU).AND.IU.LT.IMAX)GO TO 13
         IF(IU.EQ.IMAX)THEN
C
             IF(NO7OUT.EQ.1)THEN
                 WRITE(7,2001)
             ENDIF
C
                 GO TO 190
         ENDIF
         IMIN=IPOINTA(NRESL)
         ITOT=IMAX-IMIN+1
         ILMAX=IU-IL+1
C
         IF(NO7OUT.EQ.1)THEN
             WRITE(7,2007)
             WRITE(7,2009)ISPIN,IPARNT,NRESL,NRESU,IL,IU,ILMAX,
     &                    IMIN,IMAX,ITOT,SSYSWT
         ENDIF
C
         if (lpass) WRITE(18,2009)ISPIN,IPARNT,NRESL,NRESU,IL,IU,ILMAX,
     &   IMIN,IMAX,ITOT,SSYSWT
C-----------------------------------------------------------------------
C  ELIMINATE COUPLINGS BETWEEN N-SHELLS BELOW NRESL AND
C  ALL HIGHER N-SHELLS UP TO IMAX
C-----------------------------------------------------------------------
C
C        IF(NO7OUT.EQ.1)THEN
C            WRITE(7,2007)
C            DO 15 I=1,IMAX
C             WRITE(7,2006)(ARED(I,J),J=1,IMAX),RHS(I)
C  15        CONTINUE
C        ENDIF
C
         DO 30 I=1,IMAX
          RHSH(I)=RHS(I)
          DO 20 J=1,IMAX
           AREDH(I,J)=ARED(I,J)
   20     CONTINUE
   30    CONTINUE
         IF(IMIN.GT.1)THEN
             DO 35 K=1,IMIN-1
              DO 33 I=IMIN,IMAX
               AREDH(I,I)=AREDH(I,I)+ARED(K,I)
               RHSH(I)=RHSH(I)-ARED(K,I)+ARED(I,K)
               AREDH(K,I)=0.0D0
               AREDH(K,K)=AREDH(K,K)+ARED(I,K)
               RHSH(K)=RHSH(K)-ARED(I,K)+ARED(K,I)
               AREDH(I,K)=0.0D0
   33         CONTINUE
   35        CONTINUE
         ENDIF
C
C        IF(NO7OUT.EQ.1)THEN
C            WRITE(7,2007)
C            DO 37 I=1,IMAX
C             WRITE(7,2006)(AREDH(I,J),J=1,IMAX),RHSH(I)
C  37        CONTINUE
C        ENDIF
C
C-----------------------------------------------------------------------
C  ELIMINATE COUPLINGS BETWEEN N-SHELLS BELONGING TO LOW LEVELS.
C
C  OPTIONALLY ELIMINATE IONISATION, DIELECTRONIC, RADIATIVE AND
C  CHARGE EXCHANGE RECOMBINATION PARTS BELONGING TO THE LOW LEVELS.
C
C  CN-REPRESENTATION.
C-----------------------------------------------------------------------
       DO 50 I=IL,IU
        DO 40 J=IL,IU
         IF(J.NE.I)THEN
            AREDH(I,I)=AREDH(I,I)+ARED(J,I)
            RHSH(I)=RHSH(I)-ARED(J,I)+ARED(I,J)
            AREDH(J,I)=0.0D0
         ENDIF
   40   CONTINUE
        IF(IECION.EQ.0)THEN
            AREDH(I,I)=AREDH(I,I)-CIONPT(I)
        ENDIF
        IF(IEDREC.EQ.0)THEN
            RHSH(I)=RHSH(I)-DRECPT(I)
        ENDIF
        IF(IERREC.EQ.0)THEN
            RHSH(I)=RHSH(I)-RRECPT(I)
        ENDIF
        IF(IEXREC.EQ.0)THEN
            RHSH(I)=RHSH(I)-XRECPT(I)
        ENDIF
   50  CONTINUE
C-----------------------------------------------------------------------
C  ADJUST TRANSITION RATES TO IMIN FOR PHASE SPACE OCCUPANCY FACTOR
C-----------------------------------------------------------------------
       DO 51 J=IMIN+1,IMAX
        AREDH(1,J)=PHSFAC*AREDH(1,J)
        AREDH(J,J)=AREDH(J,J)-RPHSFC*AREDH(1,J)
        RHSH(1)=RHSH(1)-RPHSFC*AREDH(1,J)
        RHSH(J)=RHSH(J)+RPHSFC*AREDH(1,J)
   51  CONTINUE
C
C      IF(NO7OUT.EQ.1)THEN
C          WRITE(7,2007)
C          DO 52 I=1,IMAX
C           WRITE(7,2006)(AREDH(I,J),J=1,IMAX),RHSH(I)
C  52      CONTINUE
C      ENDIF
C
C-----------------------------------------------------------------------
C  OBTAIN PARTS OF ARED,RHS EQUATIONS NOT INCLUDED IN THE PROJECTION
C-----------------------------------------------------------------------
       DO 70 I=1,IMAX
        RHSL(I)=RHS(I)-RHSH(I)
        DO 60 J=1,IMAX
         AREDL(I,J)=ARED(I,J)-AREDH(I,J)
   60   CONTINUE
   70  CONTINUE
C
C      IF(NO7OUT.EQ.1)THEN
C          WRITE(7,2007)
C          DO 72 I=1,IMAX
C           WRITE(7,2006)(AREDL(I,J),J=1,IMAX),RHSL(I)
C  72      CONTINUE
C      ENDIF
C
C-----------------------------------------------------------------------
C  FIRST INVERSION - HIGH LEVEL MATRIX ONLY AND SOLVE N-SHELL EQUATIONS.
C                    TRANSCRIBE RELEVANT ROWS AND COLUMNS INTO AMAT & RS
C-----------------------------------------------------------------------
       DO 76 I=IMIN,IMAX
        RS(I-IMIN+1)=RHSH(I)
        DO 74 J=IMIN,IMAX
         AMAT(I-IMIN+1,J-IMIN+1)=AREDH(I,J)
   74   CONTINUE
   76  CONTINUE
       CALL MATINV(AMAT,ITOT,RS,1,DETERM)
       DO 90 I=1,ILMAX
        VEC(I)=0.0D0
        DO 80 J=1,ILMAX
         BREDL(I,J)=AMAT(I,J)
   80   CONTINUE
   90  CONTINUE
C
C      IF(NO7OUT.EQ.1)THEN
C          WRITE(7,2007)
C          DO 92 I=1,ITOT
C           WRITE(7,2006)(AMAT(I,J),J=1,ITOT),RS(I)
C  92      CONTINUE
C      ENDIF
C
C
C-----------------------------------------------------------------------
C  SECOND INVERSION - HIGH LEVEL CONTRIBUTIONS TO LOW N-SHELLS.
C-----------------------------------------------------------------------
C
C      IF(NO7OUT.EQ.1)THEN
C          WRITE(7,2007)
C          DO 94 I=1,ILMAX
C           WRITE(7,2006)(BREDL(I,J),J=1,ILMAX),VEC(I)
C  94      CONTINUE
C      ENDIF
C
       CALL MATINV(BREDL,ILMAX,VEC,1,DETERM)
C
C      IF(NO7OUT.EQ.1)THEN
C          WRITE(7,2007)
C      ENDIF
C
       DO 96 I=1,ILMAX
        VEC(I)=0.0D0
        DO 95 J=1,ILMAX
         VEC(I)=VEC(I)+BREDL(I,J)*RS(J)
   95   CONTINUE
C
C       IF(NO7OUT.EQ.1)THEN
C           WRITE(7,2006)(BREDL(I,J),J=1,ILMAX),VEC(I)
C       ENDIF
C
   96  CONTINUE
C
C-----------------------------------------------------------------------
C  CONVERT TO POPULATION REPRESENTATION OF THE PROJECTION MATRIX AND
C  RIGHT HAND SIDE
C-----------------------------------------------------------------------
       DO 98 I=1,ILMAX
        PCRRHS(I)=0.0D0
        DO 97 J=1,ILMAX
         PCRMAT(I,J)=ACNST*BREDL(I,J)/(A1CNST*DVEC(J+IL-1))
         PCRRHS(I)=PCRRHS(I)+BREDL(I,J)*(RS(J)+1.0D0)
   97   CONTINUE
        PCRRHS(I)=ACNST*PCRRHS(I)
        PCION(I)=ACNST*CIONPT(I+IL-1)/(A1CNST*DVEC(I+IL-1))
        PDREC(I)=ACNST*DRECPT(I+IL-1)
        PRREC(I)=ACNST*RRECPT(I+IL-1)
        PXREC(I)=ACNST*XRECPT(I+IL-1)
   98  CONTINUE
 
       if (lpass) WRITE(18,2018)
       DO 99 I=1,ILMAX
        WRITE(STRNG2,2019)NREP(I+IL-1),(PCRMAT(I,J),J=1,ILMAX)
        if (lpass) WRITE(18,2020)STRNG2,PCRRHS(I),PCION(I),PDREC(I),
     &                           PRREC(I),PXREC(I)
   99  CONTINUE
C-----------------------------------------------------------------------
C  MATRIX EXPANSION IN POPULATION REPRESENTATION -
C  HIGH LEVEL CONTRIBUTIONS TO LOW RESOLVED LEVELS.
C-----------------------------------------------------------------------
       DO 104 I=1,NLEV
        IF(LSPA(I).EQ.NSPNA(ISPIN,IPARNT).AND.LPTA(I).EQ.IPARNT)THEN
            IPT=IPOINTA(LSHA(I))
            PEXRHS(I)=SSYSWT*WGHTA(I,IPT)*PCRRHS(IPT-IL+1)
            PEDREC(I)=SSYSWT*WGHTA(I,IPT)*PDREC(IPT-IL+1)
            PERREC(I)=SSYSWT*WGHTA(I,IPT)*PRREC(IPT-IL+1)
            PEXREC(I)=SSYSWT*WGHTA(I,IPT)*PXREC(IPT-IL+1)
            PECION(I)=PCION(IPT-IL+1)
            DO 102 J=1,NLEV
             IF(LSPA(J).EQ.NSPNA(ISPIN,IPARNT).
     &       AND.LPTA(J).EQ.IPARNT)THEN
                 JPT=IPOINTA(LSHA(J))
                 IF(IPT.EQ.JPT)THEN
                     IF(I.EQ.J)THEN
                         PEXMAT(I,J)=PCRMAT(IPT-IL+1,JPT-IL+1)
                     ELSE
                         PEXMAT(I,J)=0.0D0
                     ENDIF
                 ELSE
                     PEXMAT(I,J)=WGHTA(I,IPT)*
     &               PCRMAT(IPT-IL+1,JPT-IL+1)
                 ENDIF
             ENDIF
  102       CONTINUE
        ENDIF
  104  CONTINUE
C
       if (lpass) WRITE(18,2012)
       DO 106 I=1,NLEV
        if (lpass) WRITE(18,2006)(PEXMAT(I,J),J=1,NLEV),PEXRHS(I)
  106  CONTINUE
C-----------------------------------------------------------------------
C  CONTINUE CN-REPRESENTATION SOLUTION TO OBTAIN DEPENDENCE OF
C  HIGH NSHELLS ON THE LOW N-SHELLS AND RECOMBINATION EXPLICITLY.
C-----------------------------------------------------------------------
       DO 114 I=ILMAX+1,ITOT
        RH(I)=RS(I)
        DO 113 J=1,ILMAX
         CHMAT(I,J)=0.0D0
         DO 112 K=1,ILMAX
          CHMAT(I,J)=CHMAT(I,J)+AMAT(I,J)*BREDL(J,K)
  112    CONTINUE
         RH(I)=RH(I)-CHMAT(I,J)*RS(J)
  113   CONTINUE
  114  CONTINUE
C
       IF(NO7OUT.EQ.1)THEN
           WRITE(7,2013)
       ENDIF
C
       if (lpass) WRITE(18,2013)
       WRITE(LSTRING,2014)(NREP(I),I=IL,IU)
       LSTRING(29+14*ILMAX:31+14*ILMAX)='RS'
C
       IF(NO7OUT.EQ.1)THEN
       WRITE(7,2015)LSTRING
       ENDIF
C
       if (lpass) WRITE(18,2015)LSTRING
       DO 115 I=ILMAX+1,ITOT
C
        IF(NO7OUT.EQ.1)THEN
        WRITE(7,2016)I+IL-1,NREP(I+IL-1),(CHMAT(I,J),J=1,ILMAX),RH(I)
        ENDIF
C
        if (lpass) then
         WRITE(18,2016)I+IL-1,NREP(I+IL-1),(CHMAT(I,J),J=1,ILMAX),RH(I)
        endif
  115  CONTINUE
C-----------------------------------------------------------------------
C  FINAL CHECK INVERSION OF ORIGINAL MATRIX
C-----------------------------------------------------------------------
C      DO 144 I=1,IMAX
C       RHS2(I)=RHS(I)
C       DO 143 J=1,IMAX
C        ARED2(I,J)=ARED(I,J)
C 143   CONTINUE
C 144  CONTINUE
C      CALL MATINV(ARED2,IMAX,RHS2,1,DETERM)
C
C      IF(NO7OUT.EQ.1)THEN
C          WRITE(7,2007)
C          DO 145 I=1,IMAX
C           WRITE(7,2006)(ARED2(I,J),J=1,IMAX),RHS2(I)
C 145      CONTINUE
C      ENDIF
C
  190  CONTINUE
  200  CONTINUE
C-----------------------------------------------------------------------
C  TABULAR OUTPUT OF DIAGONAL AND RHS COMPONENTS OF SOLUTION
C-----------------------------------------------------------------------
C
       IF(NO7OUT.EQ.1)THEN
           WRITE(7,2010)
       ENDIF
C
       if (lpass) WRITE(18,2010)
       DO 210 I=1,NLEV
C
       IF(NO7OUT.EQ.1)THEN
           WRITE(7,2011)I,LVSYMA(I),LSPA(I),LSHA(I),LPTA(I),
     &     PEXMAT(I,I),PEXRHS(I),PECION(I),PEDREC(I),PERREC(I),PEXREC(I)
       ENDIF
C
       if (lpass) WRITE(18,2011)I,LVSYMA(I),LSPA(I),LSHA(I),LPTA(I),
     & PEXMAT(I,I),PEXRHS(I),PECION(I),PEDREC(I),PERREC(I),PEXREC(I)
  210  CONTINUE
C
       IF(NO7OUT.EQ.1)THEN
C-----------------------------------------------------------------------
C  CALL LOW LEVEL POPULATION SUBROUTINE LOWPOP
C-----------------------------------------------------------------------
cx        CALL LOWPOP(SEQ,REFMEM,Z0,Z1,ZEFF,NMET,IMETR,TE,DENS,TP,DENSP,
cx   &                DENSH,NPARNT,DEPA,LPTA,
cx   &                PEXMAT,PEXRHS,PECION,PEDREC,PERREC,PEXREC,
cx   &                IECION,IEDREC,IERREC,IEXREC)
C-----------------------------------------------------------------------
       ENDIF
C
  999  RETURN
  996  WRITE(7,2000)
       GO TO 999
 2000  FORMAT(1H ,'FILE OPEN FAILURE IN CLDLBN2  -  CASE IGNORED!')
 2001  FORMAT(1H ,'NRESL OR NRESU INAPPROPRIATE  -  CASE IGNORED!')
 2002  FORMAT(1H ,1A80,15X,'Z0 = ',I2,5X,'Z1 = ',I2/1H ,1A80)
 2003  FORMAT(1H ,1A80)
 2004  FORMAT(1A80)
 2005  FORMAT(I3,1A26,3I3,3X,F7.4,1X,F7.4,1X,F7.4,1X,F7.4,1X,F7.4)
 2006  FORMAT(1H ,1P,11D12.4)
 2007  FORMAT(1H )
 2008  FORMAT(1H ,6I12)
 2009  FORMAT(1H ,'ISPIN=',I3,3X,'IPARNT=',I3,3X,
     & 'NRESL=',I3,3X,'NRESU=',I3,3X,'IL=',I3,3X,'IU=',I3,3X,
     & 'ILMAX=',I3,3X,'IMIN=',I3,3X,'IMAX=',I3,3X,'ITOT=',I3,3X,
     & 'SSYSWT=',F6.2)
 2010  FORMAT(1H ,'PRES- REPRESENTATION  : PRES COMPONENTS'/
     &        1H ,'INDX CODE                     SP SH PT      PEXMAT(I,
     &I)    PEXRHS(I)     PECION(I)     PEDREC(I)     PERREC(I)     PEXR
     &EC(I)')
 2011  FORMAT(1H ,I3,1A26,3I3,3X,1P,6D14.5)
 2012  FORMAT(1H ,'PRES- REPRESENTATION  : PRES <---> PRES AND RHS')
 2013  FORMAT(1H ,'CN  - REPRESENTATION  : CN <--- CNLOW  DEPENDENCE')
 2014  FORMAT(1H ,'    I    N         NLOW=',5(I4,10X))
 2015  FORMAT(1A133)
 2016  FORMAT(1H ,2I5,10X,1P,5D14.5)
 2017  FORMAT(1H ,'DENS=',1P,D10.2,2X,'TE=',1P,D10.2,2X,'DENSP=',
     & 1P,D10.2,2X,'TP=',1P,D10.2,2X,'BMENER=',1P,D10.2,2X,'DENSH=',
     & 1P,D10.2,2X,'W1=',1P,D10.2)
 2018  FORMAT(1H ,'PN  - REPRESENTATION  :' /
     &        1H ,'   N    PN <---> PN   MATRIX
     &             PCRRHS(N)     PECION(N)     PDREC(N)      PRREC(N)
     &   PXREC(N)')
 2019  FORMAT(1H ,I3,1P,4D14.5)
 2020  FORMAT(1A60,3X,1P,5D14.5)
 2021  FORMAT(1H ,'IECION = ',I2,5X,'IEDREC = ',I2,5X,'IERREC = ',I2,5X,
     & 'IEXREC = ',I2)
      END
