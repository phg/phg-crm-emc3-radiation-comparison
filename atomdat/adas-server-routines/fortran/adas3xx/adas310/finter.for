CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas310/finter.for,v 1.1 2004/07/06 13:54:46 whitefor Exp $ Date $Date: 2004/07/06 13:54:46 $
CX
       FUNCTION FINTER(EN)                         
       IMPLICIT REAL*8 (A-H,O-Z)                  
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 FUNCTION: FINTER ***********************
C
C  PURPOSE:  UNKNOWN
C
C NOTES: THIS ROUTINE IS NOT YET PROPERLY ANNOTATED
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 16-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C-----------------------------------------------------------------------
C
      FINTER=1.0/((EN+10.0)*(EN+10.0))          
      RETURN                                   
      END                                      
