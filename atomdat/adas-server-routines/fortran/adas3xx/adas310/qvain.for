CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas310/qvain.for,v 1.2 2007/05/17 17:03:02 allan Exp $ Date $Date: 2007/05/17 17:03:02 $
CX
       FUNCTION QVAIN(Z1,N1,N2,VION,PHI,ZP)               
C
       IMPLICIT REAL*8(A-H,O-Z)                          
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 FUNCTION: QVAIN ************************
C
C  PURPOSE: CALCULATES ION COLLISION CROSS-SECTIONS FOR TRANSITIONS
C           BETWEEN PRINCIPAL QUANTUM SHELLS IN HYDROGEN AND
C           HYDROGENIC IONS.                
C
C  INPUT                                                          
C      Z1   = TARGET ION CHARGE +1                               
C      N1   = INITIAL PRINCIPAL QUANTUM NUMBER                  
C      N2   = FINAL PRINCIPAL QUANTUM NUMBER                   
C      VION = VELOCITY OF INCIDENT ION (CM/SEC)               
C      ZP   = PROJECTILE CHARGE                              
C  OUTPUT                                                   
C      QVAIN=CROSS-SECTION IN PI*A0**2 UNITS               
C
C NOTES: THIS ROUTINE IS NOT YET PROPERLY ANNOTATED
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 16-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C VERSION: 1.2                          DATE: 16-05-07
C MODIFIED: Allan Whiteford
C             - Updated comments as part of subroutine documentation
C               procedure.
C-----------------------------------------------------------------------
C
       ZZ1=Z1*Z1                                                    
       ZZP=ZP*ZP                                                   
       EN1=N1                                                     
       EN2=N2                                                    
       EIJRH=DABS(1/(EN1*EN1) - 1/(EN2*EN2))                    
       ALFA=1.0/137.036                                        
       CLIG=2.998D+10                                         
       CFAC=(CLIG/VION)**2                                            
       BETA=0.5*ALFA*ALFA*CFAC*DSQRT(PHI)*EIJRH*ZP                   
       XFAC1=2*ALFA*ALFA*CFAC*PHI*ZP*ZP                             
       QVAIN=XFAC1*DEXP(-2*DSQRT(BETA))*RIFAC(BETA)                
       RETURN                                                     
       END                                                       
C                                                               
       FUNCTION RIFAC(BETAIN)                                  
       IMPLICIT REAL*8(A-H,O-Z)                               
       PARAMETER(NBET=12,PI=3.1415927)                       
       DIMENSION SIFAC(NBET),SBETA(NBET)                    
       DATA SBETA /0.01,0.02,0.04,0.08,0.16,0.32,0.64,1.28,2.56,5.12,
     &            10.24,20.00/                             
       DATA SIFAC /45.45,21.6,15.9,10.7,6.25,2.94,1.10,2.68D-1,8.29D-2,
     &            2.73D-2,8.83D-3,3.106D-3/                      
       IF(BETAIN.LT.SBETA(1)) THEN                                
          RIFAC=PI*PI*DLOG(1/BETAIN)                               
       ELSEIF(BETAIN.GT.SBETA(NBET)) THEN                           
          RIFAC=0.125*(2*DSQRT(BETAIN)+1)/(BETAIN**2)                
       ELSE                                         
          DO 100 IB=2,NBET                           
          IF(BETAIN.LE.SBETA(IB)) THEN                
            DELX=SBETA(IB)-SBETA(IB-1)                 
            DELY=SIFAC(IB)-SIFAC(IB-1)                  
            Y1  =SIFAC(IB-1)                             
            RIFAC=Y1+((BETAIN-SBETA(IB-1))/DELX)*DELY     
            GOTO 110                                       
          ENDIF                                             
  100     CONTINUE                                           
  110     CONTINUE                                            
       ENDIF                                                   
C      WRITE(6,1001) BETAIN,RIFAC                               
 1001  FORMAT(1H ,'BETAIN,RIFAC : ',1P,2D10.2)                   
       END                                                        
