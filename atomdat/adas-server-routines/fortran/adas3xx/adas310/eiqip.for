CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas310/eiqip.for,v 1.1 2004/07/06 13:48:36 whitefor Exp $ Date $Date: 2004/07/06 13:48:36 $
CX
       SUBROUTINE EIQIP(EI,EIJ,EM,Z,PHI,SC,WI,WJ,R,EIQ,FLAG)            
C
       IMPLICIT REAL*8 (A-H,O-Z)                                      
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: EIQIP **********************
C
C PURPOSE UNKNOWN
C
C NOTES: THIS ROUTINE IS NOT YET PROPERLY ANNOTATED
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 16-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C-----------------------------------------------------------------------
C
       FLAG=1.0                                                       
       EF=EI-EIJ                                                     
       T=EF/EI                                                      
       C=1.0                                                       
       IF(T-0.2)1,4,4                                             
    1  IF(Z-0.01)2,3,3                                           
    2  C=DSQRT(4.0*EF/EIJ)                                      
       GO TO 21                                                
    3  IF(EM-1.5)21,21,20                                     
   20     C=DABS(EIJ)/(EI*DSQRT(EF)+EF*DSQRT(EI))            
       C=Z*DSQRT(EM)*(1.1056/DSQRT(EIJ)-C)                  
       C=DEXP(3.142*C)                                     
   21     EI=1.25*EIJ                                     
       EF=0.25*EIJ                                       
       GO TO 8                                          
    4  IF(T-5.0)8,8,5                                  
    5  IF(Z-0.01)6,6,7                                
    6  C=DSQRT(-4.0*EI/EIJ)                          
       GO TO 23                                     
    7  IF(EM-1.5)23,23,22                          
   22     C=DABS(EIJ)/(EI*DSQRT(EF)+EF*DSQRT(EI)) 
       C=Z*DSQRT(EM)*(1.1056/DSQRT(-EIJ)-C)        
       C=DEXP(3.142*C)                              
   23     EI=-0.25*EIJ                               
       EF=-1.25*EIJ                                   
    8  TI=DSQRT(EI)                                    
       TF=DSQRT(EF)                                     
       TIF=DABS(EIJ)/(TI+TF)                             
       XI=Z*TIF/(TI*TF)                                   
       D=TIF*R                                             
       E=TI*TF                                              
       IF(EM-1.5)25,24,24                                    
   24  EM2=DSQRT(EM)                                          
          TIF=EM2*TIF                                          
          XI=-EM2*XI                                            
          D=EM2*D                                                
   25  CONTINUE                                                   
       T=Z+E*R                                                     
       T1=XIP(XI,D)/(T*T)                                           
       T2=4.0*PHI*E*SC*EM                                            
       IF(WI-WJ)10,10,9                                               
    9  T2=T2*WI/WJ                                                     
   10  P=T2*T1                                                          
       EIQW=8.0*C*PHI*YIP(XI,D)*EM                  
       IF(P-0.5)15,15,11                             
   11  R1=R                                           
   12  A=R1                                            
       FLAG=0.0                                         
       VA=0.5-P                                          
       R1=R1+R1                                           
       T=Z+E*R1                                            
       D=TIF*R1                                             
       P=T2*XIP(XI,D)/(T*T)                                  
       IF(P-0.5)13,14,12                                      
   13  B=R1                                                    
       VB=0.5-P                                                 
       CALL ZERO1(A,B,VA,VB,0.01D0,R1,XI,Z,E,TIF,T2)             
   14  T=Z+E*R1                                                   
       D=TIF*R1                                                    
       T1=XIP(XI,D)/(T*T)                                           
   15  EIQ=8.0*C*PHI*(YIP(XI,D)+0.5*(T*T-Z*Z)*T1)*EM                 
       EIQ=DMIN1(EIQ,EIQW)                                            
       RETURN                                                          
       END                                                               
