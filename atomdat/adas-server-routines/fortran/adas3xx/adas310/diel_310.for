CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas310/diel_310.for,v 1.2 2004/07/06 13:32:05 whitefor Exp $ Date $Date: 2004/07/06 13:32:05 $
CX
       SUBROUTINE DIEL_310(Z,EIJ,F,T,COR,JCOR,N,AD)              
       IMPLICIT REAL*8 (A-H,O-Z)                            
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 PROGRAM: DIEL_310 **************************
C
C PURPOSE UNKNOWN
C
C NOTES: THIS ROUTINE IS NOT YET PROPERLY ANNOTATED
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 16-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C VERSION: 1.2					DATE: 04-04-2000
C MODIFIED: RICHARD MARTIN
C		  - CHANGED NAME FROM DIEL.FOR TO DIEL_310.FOR
C		    (TO AVOID NAME CONFLICT WITH ADAS801)
C
C-----------------------------------------------------------------------
C
       DIMENSION THETA(1000),COR(20)                       
       AD=0.0                                             
       ZZ=Z*Z                                            
       Z1=(Z+1.0)*(Z+1.0)                               
       EN=N                                            
       T1=Z1*EIJ/ZZ-1.0/(EN*EN)                       
       E=T1*ZZ                                        
       IF(T1)4,4,1                                   
    1  A=DSQRT(T1)                                  
       CALL BF(THETA,N,A)                          
       B=0.777135E-6*ZZ*ZZ                        
       J1=JCOR                                   
       IF(N-JCOR)5,6,6                          
    5  J1=N                                            
    6  DO 2 J=1,J1                                      
    2  THETA(J)=COR(J)*THETA(J)                          
       C3=0.0                                             
       DO 3 J=1,N                                          
       TJ=J                                                 
       TH=THETA(J)*TJ*EN*EN                                  
       TJ=TJ+TJ-1.0                                           
       T3=TJ*TH/(B*TJ*(1.0+T)+TH)                              
       THETA(J)=TH/(B*TJ*(1.0+T))                               
    3  C3=C3+T3                                                  
       Z2=Z1*EIJ/ZZ                        
       AD=0.51013*Z2*Z2*F*C3                
       IF(N-10)7,8,8                         
    7  NP=N                                   
       GO TO 9                                 
    8  NP=10                                    
    9  CONTINUE                                  
C      WRITE(6,100)(THETA(J),J=1,NP)              
C 100  FORMAT(1P,10D12.2)                          
    4  RETURN                                       
      END                                            
