CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas310/lowpop.for,v 1.4 2007/05/17 17:03:02 allan Exp $ Date $Date: 2007/05/17 17:03:02 $
CX
       SUBROUTINE LOWPOP(SEQ,REFMEM,Z0,Z1,ZEFF,NMET,IMETR,TE,DENS,TP,
     &                   DENSP,DENSH,NPARNT,DEPA,LPTA,
     &                   PEXMAT,PEXRHS,PECION,PEDREC,PERREC,
     &                   PEXREC,IECION,IEDREC,IERREC,IEXREC)
       IMPLICIT REAL*8(A-H,O-Z)
C ----------------------------------------------------------------------
C  PURPOSE: CALCULATE POPULATIONS OF LOW EXCITED POPULATIONS OF IONS
C
C  INCLUDING
C
C           (A) COUPLING TO HIGH LEVELS AND CONTINUUM VIA PRELIMINARY
C               BUNDLE-N CALCULATION (V2BNDLN) AND PROJECTION/EXPANSION
C               MATRIX CALCULATION (CLDLBN2)
C
C           (B) DEPENDENCE ON METASTABLES.
C
C
C  PROCESSES CAN INCLUDE ELECTRON & PROTON IMPACT, SPONTANEOUS EMISSION,
C  FREE ELECTRON RECOMBINATION AND CHARGE EXCHANGE RECOMBINATION
C  DEPENDING ON THE INPUT DATA SET
C
C  THE BASIC LOW LEVEL ATOMIC DATA IS ENTERED FROM COMPILATION
C  DATA SETS OF THE FORM
C
C             'JETSHP.<SE>LIKE.DATA(<TITLE><EL>)'
C
C       WHERE <SE> DENOTES THE ISOLELECTRONIC SEQUENCE, <TITLE> IS AN
C       ARBITRARY IDENTIFIER FOR THE SOURCE AND YEAR, <EL> IS THE
C       ELEMENT SYMBOL. <SE> AND <TITLE><EL> ARE PRESENTED TO THE
C       SUBROUTINE AS PARAMETERS FROM CLDLBN2
C
C
C  MAIN OUTPUT TABLES ARE GENERATED ON STREAM 7
C  POPULATION DATA FOR DIAGNOSTIC USE ON STREAM 19
C
C  THE SUBROUTINE WAS DEVELOPED FROM THE ORIGINAL PROGRAM SPMETPOP
C
C
C  INPUT
C      Z0      = NUCLEAR CHARGE
C      Z1      = RECOMBINING ION CHARGE
C      ZEFF    = PLASMA Z EFFECTIVE
C      NMET    = NUMBER OF METASTABLES (1.LE.NMET.LE.5)
C      IMETR(I)= INDEX OF METASTABLE I IN COMPLETE LEVEL LIST
C      TE      = ELECTRON TEMPERATURE (K)
C      DENS    = ELECTRON DENSITY (CM-3)
C      TP      = PROTON TEMPERATURE   (K)
C      DENSP   = PROTON DENSITY (CM-3)
C      DENSH   = NEUTRAL HYDROGEN DENSITY(IN BEAMS)  (CM-3)
C      PEXMAT(I,J) = EXPANDED PROJECTION MATRIX FOR LOW LEVELS
C      PEXRHS(I)   = EXPANDED PROJECTED RIGHT HAND SIDE VECTOR
C      PECION(I)   = DENS*IONISATION RATE COEFFT  FOR LOW LEVEL I
C      PEDREC(I)   = DIELECTRONIC RECOMB. COEFFT. FOR LOW LEVEL I
C      PERREC(I)   = RADIATIVE RECOMB. COEFFT.    FOR LOW LEVEL I
C      PEXREC(I)   = DENSH*(CX RATE COEFFT)/DENS  FOR LOW LEVEL I
C      IECION      = 0  IONISATION     ELIMINATED     FROM LEVEL I
C                  = 1  IONISATION     NOT ELIMINATED FROM LEVEL I
C      IEDREC      = 0  DIEL. RECOMB. ELIMINATED     FOR LEVEL I
C                  = 1  DIEL. RECOMB. NOT ELIMINATED FOR LEVEL I
C      IERREC      = 0  RAD.  RECOMB. ELIMINATED     FOR LEVEL I
C                  = 1  RAD.  RECOMB. NOT ELIMINATED FOR LEVEL I
C      IEXREC      = 0  CX.   RECOMB. ELIMINATED     FOR LEVEL I
C                  = 1  CX.   RECOMB. NOT ELIMINATED FOR LEVEL I
C
C
C
C  ********** H.P. SUMMERS, JET                26 APR 1990  **********
C         OPEN FILE ON STREAM 9 UNDER DIVUID   19 FEB 1991
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C
C UPDATE:  19/01/94 - JONATHAN NASH - TESSELLA SUPPORT SERVICES PLC
C
C          THE FOLLOWING MODIFICATIONS HAVE BEEN MADE TO THE SUBROUTINE:
C
C          1) THE INPUT UNIT NUMBER WAS CHANGE FROM 10 TO 9 AND THE
C             DIAGNOSTIC OUTPUT UNIT FROM 11 TO 19.
C
C  NOTES:  NO ATTEMPT HAS BEEN MADE TO RESTRUCTURE THE ROUTINE. RATHER
C          THE MINIMUM AMOUNT OF WORK TO INTEGRATE THE ROUTINE INTO
C          ADAS310 HAS BEEN COMPLETED.
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 18-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER SCCS CONTROL
C
C VERSION: 1.2                          DATE: 18-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - COMMENTED OUT LINE 'EXTERNAL GAMMA' AS IT APPEARS
C                 TO SERVE NO PURPOSE AND THERE IS NO CORRESPONDING
C                 GAMMA ROUTINE.
C
C VERSION: 1.3                          DATE: 18-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - COMMENTED OUT LINE 'CALL ERRSET'
C
C VERSION: 1.4                          DATE: 16-05-07
C MODIFIED: Allan Whiteford
C             - Updated comments as part of subroutine documentation
C               procedure.
C
C-----------------------------------------------------------------------
       PARAMETER (NDLEV=30,NDTEM=20,NDDEN=20,NDMET=5,NDTRN=300)
       PARAMETER (NDSPL=10)
       PARAMETER (ICODE=0)
C-----------------------------------------------------------------------
       CHARACTER*40 TITLE,GTIT1,STRG1
       CHARACTER*20 TIT2
       CHARACTER*1 LCODE,GRAPH,TEXT,TCODE
       CHARACTER ANS*3,CSTRGA*12,STRGA*22,ITITLE*3
       CHARACTER SEQ*2,REFMEM*8,DSNAME*30
       CHARACTER STRGB*36,STRGC*36,STRGD*36,BLANKS*36
C
       CHARACTER JOBNAME*8,DIVUID*6,ACCT*4,POINT*2,SECLEV*2
       DOUBLE PRECISION LOGTEA,LOGTPA,LOGTHA
       REAL*4 XL14,XU14,YL14,YU14,SDENSA,STK4,STEV
C
       DIMENSION IA(NDLEV),ISA(NDLEV),ILA(NDLEV),XJA(NDLEV),WA(NDLEV)
       DIMENSION ER(NDLEV),RHS(NDLEV),XIA(NDLEV),CIE(NDLEV)
       DIMENSION CRHS(NDLEV,5),LPTA(NDLEV),DEPA(5)
       DIMENSION CRA(NDLEV,NDLEV),CRCE(NDLEV,NDLEV),CRCP(NDLEV,NDLEV)
       DIMENSION IORDR(NDLEV),CC(NDLEV,NDLEV),CMAT(NDLEV,NDLEV)
       DIMENSION POPAR(NDLEV),CSTRGA(NDLEV)
       DIMENSION TEA(NDTEM),TPA(NDTEM),TEVA(NDTEM),TPVA(NDTEM)
       DIMENSION THA(NDTEM),THVA(NDTEM)
       DIMENSION ATEA(NDTEM),ATPA(NDTEM),ATHA(NDTEM)
       DIMENSION LOGTEA(NDTEM),LOGTPA(NDTEM),LOGTHA(NDTEM)
       DIMENSION SQATEA(NDTEM),SQATPA(NDTEM),SQATHA(NDTEM)
       DIMENSION EXPTE(NDLEV,NDTEM),EXPTP(NDLEV,NDTEM)
       DIMENSION VECH(NDTEM,NDLEV),VECR(NDTEM,NDLEV)
       DIMENSION DENSA(NDDEN),DENSPA(NDDEN),RATHA(NDDEN),RATIA(NDDEN)
       DIMENSION SDENSA(NDDEN),STK4(NDLEV,NDMET,NDDEN)
       DIMENSION SIONA(NDMET),STVRH(NDMET,5),ALFG(5)
       DIMENSION IMETR(NDMET+1),VRHRED(NDMET,5)
       DIMENSION CRED(NDMET,NDMET)
       DIMENSION IE1A(NDTRN),IE2A(NDTRN),IP1A(NDTRN),IP2A(NDTRN)
       DIMENSION AA(NDTRN)
       DIMENSION DEXCRA(NDTEM,NDTRN),DEXCRP(NDTEM,NDTRN)
       DIMENSION STACK(NDLEV,NDMET,NDTEM,NDDEN)
       DIMENSION STCKM(NDMET,NDTEM,NDDEN)
       DIMENSION STVRHM(NDMET,5)
       DIMENSION C1(NDSPL,NDSPL-1),C2(NDSPL,NDSPL-1)
       DIMENSION C3(NDSPL,NDSPL-1),C4(NDSPL,NDSPL-1)
       DIMENSION XA(NDSPL)
       DIMENSION SCEF(8),SCOM(8),ERG(4),OM(4)
       DIMENSION PAR(18,17)
       DIMENSION STRGA(NDLEV)
       DIMENSION PEXMAT(NDLEV,NDLEV),PEXRHS(NDLEV),PECION(NDLEV)
       DIMENSION PEDREC(NDLEV),PERREC(NDLEV),PEXREC(NDLEV)
C
CX     EXTERNAL GAMMA
C
       DATA TIT2/'SPDFGHIJK           '/
       DATA BLANKS/'                                    '/
C
CX     CALL ERRSET(208,256,-1)
C-----------------------------------------------------------------------
C  SUPPRESS PROTON RATE CALCULATIONS COMPLETELY FOR THE MOMENT BY
C  PUTTING COMMENT ON FOLLOWING LINE AND SETTING IPROT=0
C  IPROT=0 ALLOWS PROTON COLLISIONS TO BE SWITCHED OFF
C      READ(9,*)PAR
C-----------------------------------------------------------------------
       IPROT=0
C-----------------------------------------------------------------------
C  CALL SYS6.LOAD(USINFO) TO OBTAIN USER ID.
C-----------------------------------------------------------------------
       CALL USINFO(JOBNAME,DIVUID,ACCT,POINT,SECLEV,IRC)
C-----------------------------------------------------------------------
C  OPEN LOW LEVEL ATOMIC DATA FILE ON STREAM 9
C-----------------------------------------------------------------------
       NLETT1=LENSTR(SEQ)
       NLETT2=LENSTR(REFMEM)
       DSNAME='/'//DIVUID//'.'//SEQ(1:NLETT1)//'LIKE.DATA('
     &         //REFMEM(1:NLETT2)//')'
       OPEN(UNIT=9,FILE=DSNAME,STATUS='UNKNOWN')
C-----------------------------------------------------------------------
C  INPUT OF ION SPECIFICATION
C-----------------------------------------------------------------------
       READ(9,1000)ITITLE,IZ,IZ0,IZ1,BWNO
       IF(BWNO.LE.0.0D0)STOP
       WRITE(7,1001)ITITLE,IZ,IZ0,IZ1,BWNO
       WRITE(7,1002)
       ZZ0=IZ0
       ZZ=IZ
       ZZ1=IZ1
       IF(ZZ0.NE.Z0.OR.ZZ1.NE.Z1)THEN
           WRITE(6,*)'*** PROGRAM ABORTED - CHARGE STATE MISMATCH '
           STOP
       ENDIF
       Z=ZZ
C-----------------------------------------------------------------------
C  INPUT INDEXED ENERGY LEVELS AND WAVE NUMBERS RELATIVE TO GROUND
C  ******** N.B. CHANGE IN INPUT FORMATS 1003,1004 ON 26/4/85 **********
C-----------------------------------------------------------------------
       I=1
    5  READ(9,1003)IA(I),CSTRGA(I),ISA(I),ILA(I),XJA(I),WA(I)
       IF(IA(I).GT.0)THEN
           BW=BWNO-WA(I)
           ER(I)=9.11269D-06*WA(I)
           XIA(I)=BW*9.11269D-6
           WRITE(7,1004)IA(I),CSTRGA(I),ISA(I),ILA(I),XJA(I),
     &     WA(I),BW,ER(I)
           WRITE(STRGA(I),1062)CSTRGA(I),ISA(I),
     &     TIT2(ILA(I)+1:ILA(I)+1),XJA(I)
           I=I+1
           GO TO 5
       ENDIF
       IL=I-1
C-----------------------------------------------------------------------
C  SET UP REQUIRED VALUES TO RUN LOW LEVEL POPULATION CODE
C-----------------------------------------------------------------------
       MAXT=1
       TEVA(1)=TE/11605.4
       TPVA(1)=TP/11605.4
       THVA(1)=TPVA(1)
       MAXD=1
       DENSA(1)=DENS
       DENSPA(1)=DENSP
       RATHA(1)=DENSH/DENS
       RATIA(1)=1.0
C-----------------------------------------------------------------------
C  CONTROL PARAMETERS
C  ------------------
C
C      IPSEL   = 0 DO NOT INCLUDE PROTON COLLISIONS
C              = 1        INCLUDE PROTON COLLISIONS
C      IHSEL   = 0 DO NOT INCLUDE CHARGE TRANSFER FROM NEUTRAL HYDROGEN
C              = 1        INCLUDE CHARGE TRANSFER FROM NEUTRAL HYDROGEN
C      IRSEL   = 0 DO NOT INCLUDE FREE ELECTRON RECOMBINATION
C              = 1        INCLUDE FREE ELECTRON RECOMBINATION
C      IZSEL   = 0 DO NOT SCALE PROTON COLLISIONS WITH PLASMA ZEFF
C              = 1        SCALE PROTON COLLISIONS WITH PLASMA ZEFF
C      IADD    = 0 DO NOT ADD ON EXPANDED PROJECTION DATA
C              = 1        ADD ON EXPANDED PROJECTION DATA
C-----------------------------------------------------------------------
       IPSEL=1
       IHSEL=1
       IRSEL=1
       IZSEL=1
       IADD =1
C
       NORD=IL-NMET
       WRITE(7,1049)NMET,NORD
       DO 7 I=1,NMET
        WRITE(7,1048)I,IMETR(I),STRGA(I)
    7  CONTINUE
       DO 2 I=1,MAXT
        TEA(I)=1.16054D4*TEVA(I)
        TPA(I)=1.16054D4*TPVA(I)
        THA(I)=1.16054D4*THVA(I)
        IF(TEA(I).GT.0.0D0) THEN
           LOGTEA(I)=DLOG(TEA(I))
             ATEA(I)=1.5789D5/TEA(I)
           SQATEA(I)=DSQRT(ATEA(I))
        ENDIF
        IF(TPA(I).GT.0.0D0) THEN
           LOGTPA(I)=DLOG(TPA(I))
             ATPA(I)=1.5789D5/TPA(I)
           SQATPA(I)=DSQRT(ATPA(I))
        ENDIF
        IF(THA(I).GT.0.0D0) THEN
           LOGTHA(I)=DLOG(THA(I))
             ATHA(I)=1.5789D5/THA(I)
           SQATHA(I)=DSQRT(ATHA(I))
        ENDIF
    2  CONTINUE
C
       DO 3 I=1,MAXT
        DO 3 J=1,IL
         EXPTE(J,I)=DEXP(ATEA(I)*ER(J))
         EXPTP(J,I)=DEXP(ATPA(I)*ER(J))
    3  CONTINUE
       DO 4 I=1,MAXD
        SDENSA(I)=DENSA(I)
    4  CONTINUE
C-----------------------------------------------------------------------
C  INITIALISE VECR, VECH AND CIE TO ZERO.
C-----------------------------------------------------------------------
       DO 6 I=1,NDLEV
        CIE(I)=0.0D0
        DO 6 J=1,NDTEM
         VECR(J,I)=0.0D0
         VECH(J,I)=0.0D0
    6  CONTINUE
C-----------------------------------------------------------------------
C  PRINT OUT OF INPUT DATA TO STREAM 7
C-----------------------------------------------------------------------
       WRITE(7,1036)MAXT,MAXD
       WRITE(7,1011)(TEVA(I),I=1,MAXT)
       WRITE(7,1037)
       WRITE(7,1011)(TPVA(I),I=1,MAXT)
       WRITE(7,1038)
       WRITE(7,1011)(DENSA(I),I=1,MAXD)
       WRITE(7,1039)
       WRITE(7,1011)(DENSPA(I),I=1,MAXD)
       WRITE(7,1040)
       WRITE(7,1011) (RATHA(I),I=1,MAXD)
       WRITE(7,1041)
       WRITE(7,1011) (RATIA(I),I=1,MAXD)
C
       IMETR(NMET+1)=0
       IO=1
       IM=1
       DO 24 I=1,IL
        IF(I.NE.IMETR(IM))THEN
            IORDR(IO)=I
            IO=IO+1
        ELSE
            IM=IM+1
        ENDIF
   24  CONTINUE
       ICNTE=0
       ICNTP=0
       ICNTR=0
       ICNTH=0
       MAXLEV=0
C-----------------------------------------------------------------------
C  INPUT EINSTEIN COEFFICIENTS AND COLLISION STRENGTH DATA
C
C  INPUT LINE.
C      ZF     = ION CHARGE + 1
C     IQS     = X-SECT DATA FORMAT SELECTOR (IQS=3 ONLY ALLOWED IN THIS
C               VERSION
C     SCEF(I) = ELECTRON TEMPERATURES (K) (TE=TP=TH IS ASSUMED)
C-----------------------------------------------------------------------
       IBLOCK=0
   21  READ(9,1006)ZF,IQS,(SCEF(I),I=1,8)
       IF(IQS.NE.3) THEN
          WRITE(6,*) 'FAULT IN SPECIFIC ION DATA FILE - IQS.NE.3      -
     &    PROGRAMME TERMINATED'
          STOP
       ENDIF
C
   49  DO 51 NV=8,1,-1
        IF(SCEF(NV).GT.0.0D0) GOTO 52
   51  CONTINUE
   52  IF(NV.LE.0) THEN
          WRITE(6,*) 'FAULT IN SPECIFIC ION DATA FILE - NV.LE.0    -
     &    PROGRAMME TERMINATED'
          STOP
       ENDIF
       IBLOCK=IBLOCK+1
       WRITE(7,1009)IBLOCK,ZF,IQS,(SCEF(I),I=1,NV)
C-----------------------------------------------------------------------
C  FIT SPLINE COEFFICIENTS THROUGH TEMPERATURE DATA.
C-----------------------------------------------------------------------
       DO 55 I=1,NV
        XA(I)=DLOG(SCEF(I))
   55  CONTINUE
       CALL GSPC(XA,NV,C1,C2,C3,C4)
C-----------------------------------------------------------------------
C  SET UP COEFFICIENTS FOR IONISATION RATES.
C-----------------------------------------------------------------------
       ZION=ZF-1.0D0
       BETA=0.25D0*(DSQRT((1.0D2*ZION+9.1D1)/(4.0D0*ZION+3.0D0))-5.0D0)
       ZETA=1.0D0
          C=2.3D0
   53  READ(9,1008)TCODE,J2,J1,A,(SCOM(I),I=1,NV)
C-----------------------------------------------------------------------
C DATA ORGANISATION FOR EACH TRANSITION
C
C   TCODE = DATA TYPE POINTER.
C           ' ' ELECTRON IMPACT TRANSITION
C           'P' PROTON IMPACT TRANSITION
C           'H' CHARGE EXCHANGE RECOMBINATION
C           'R' FREE ELECTRON RECOMBINATION
C
C      J2      = UPPER LEVEL INDEX     (CASE ' ' & 'P')
C                CAPTURING LEVEL INDEX (CASE 'H' & 'R')
C      J1      = LOWER LEVEL INDEX     (CASE ' ' & 'P')
C      A       = A-VALUE               (CASE ' ')
C                NEUTRAL BEAM ENERGY   (CASE 'H')
C                NOT USED              (CASE 'P' & 'R')
C      SCOM(I) = GAMMA'S               (CASE ' ' & 'P')
C
C  NB. RATE COEFF. GIVEN IN (CM3 SEC-1)(CASE 'H' & 'R')
C-----------------------------------------------------------------------
       IF(J2.GT.0) THEN
C-----------------------------------------------------------------------
C  ELECTRON EXCITATION DATA OPTION
C-----------------------------------------------------------------------
           IF(TCODE.EQ.' ') THEN
               ICNTE=ICNTE+1
               DO 93 IT=1,MAXT
                CALL FITSP(LOGTEA(IT),XA,NV,SCOM,GAM,DGAM,
     &          0,C1,C2,C3,C4,1)
                DEXCRA(IT,ICNTE)=2.1711D-8*SQATEA(IT)*GAM
   93          CONTINUE
               IF(WA(J2).LE.WA(J1)) THEN
                   I1=J2
                   I2=J1
               ELSE
                   I1=J1
                   I2=J2
               ENDIF
               IE1A(ICNTE)=I1
               IE2A(ICNTE)=I2
               IF(I1.GT.MAXLEV) MAXLEV=I1
               IF(I2.GT.MAXLEV) MAXLEV=I2
               AA(ICNTE)=A
C-----------------------------------------------------------------------
C  PROTON EXCITATION DATA OPTION
C-----------------------------------------------------------------------
           ELSE IF(TCODE.EQ.'P'.AND.IPSEL.EQ.1) THEN
               ICNTP=ICNTP+1
               DO 94 IT=1,MAXT
                CALL FITSP(LOGTPA(IT),XA,NV,SCOM,GAM,DGAM,
     &          0,C1,C2,C3,C4,1)
                IF(IZSEL.EQ.1)THEN
                    GAM=ZEFF*ZEFF*GAM
                ENDIF
                DEXCRP(IT,ICNTP)=2.1711D-08*SQATPA(IT)*GAM
   94          CONTINUE
               IF(WA(J2).LE.WA(J1)) THEN
                   I1=J2
                   I2=J1
               ELSE
                   I1=J1
                   I2=J2
               ENDIF
               IF(I1.GT.MAXLEV) MAXLEV=I1
               IF(I2.GT.MAXLEV) MAXLEV=I2
               IP1A(ICNTP)=I1
               IP2A(ICNTP)=I2
C----------------------------------------------------------------------
C  FREE ELECTRON RECOMBINATION DATA OPTION
C----------------------------------------------------------------------
           ELSE IF(TCODE.EQ.'R'.AND.IRSEL.EQ.1) THEN
               ICNTR=ICNTR+1
               DO 95 IT=1,MAXT
                CALL FITSP(LOGTEA(IT),XA,NV,SCOM,RCOEF,DRCOEF,
     &          0,C1,C2,C3,C4,1)
                VECR(IT,J2)=RCOEF
   95          CONTINUE
C----------------------------------------------------------------------
C  CHARGE EXCHANGE RECOMBINATION DATA OPTION
C----------------------------------------------------------------------
           ELSE IF(TCODE.EQ.'H'.AND.IHSEL.EQ.1) THEN
               EBEAM=A
               ICNTH=ICNTH+1
               DO 96 IT=1,MAXT
                CALL FITSP(LOGTHA(IT),XA,NV,SCOM,RCOEF,DRCOEF,
     &          0,C1,C2,C3,C4,1)
                VECH(IT,J2)=RCOEF
   96          CONTINUE
           ENDIF
          GOTO 53
       ENDIF
       WRITE(7,1012)ICNTE,ICNTP,ICNTR,ICNTH
       WRITE(7,1013)IPSEL,IRSEL,IHSEL,IZSEL,IADD,ZEFF
       DO 290 IT=1,MAXT
C-----------------------------------------------------------------------
C  ZERO ARRAYS CRA,CRCE,CRCP,CRHS
C-----------------------------------------------------------------------
        DO 20 I=1,IL
         DO 18 J=1,IL
          CRA(I,J) =0.0D0
          CRCE(I,J)=0.0D0
          CRCP(I,J)=0.0D0
   18    CONTINUE
         DO 19 IPRNT=1,NPARNT
          CRHS(I,IPRT)=0.0D0
   19    CONTINUE
   20   CONTINUE
C
        DO 85 IC=1,ICNTE
         T2=DEXCRA(IT,IC)*EXPTE(IE1A(IC),IT)/(EXPTE(IE2A(IC),IT)*
     &      (2.0D0*XJA(IE1A(IC))+1.0D0))
         T1=DEXCRA(IT,IC)/(2.0D0*XJA(IE2A(IC))+1.0D0)
         CRA(IE1A(IC),IE2A(IC))=CRA(IE1A(IC),IE2A(IC))+AA(IC)
         CRA(IE2A(IC),IE2A(IC))=CRA(IE2A(IC),IE2A(IC))-AA(IC)
         CRCE(IE1A(IC),IE2A(IC))=CRCE(IE1A(IC),IE2A(IC))+T1
         CRCE(IE2A(IC),IE2A(IC))=CRCE(IE2A(IC),IE2A(IC))-T1
         CRCE(IE2A(IC),IE1A(IC))=CRCE(IE2A(IC),IE1A(IC))+T2
         CRCE(IE1A(IC),IE1A(IC))=CRCE(IE1A(IC),IE1A(IC))-T2
   85   CONTINUE
C
        DO 90 IC=1,ICNTP
         T2=DEXCRP(IT,IC)*EXPTP(IP1A(IC),IT)/(EXPTP(IP2A(IC),IT)*
     &      (2.0D0*XJA(IP1A(IC))+1.0D0))
         T1=DEXCRP(IT,IC)/(2.0D0*XJA(IP2A(IC))+1.0D0)
         CRCP(IP1A(IC),IP2A(IC))=CRCP(IP1A(IC),IP2A(IC))+T1
         CRCP(IP2A(IC),IP2A(IC))=CRCP(IP2A(IC),IP2A(IC))-T1
         CRCP(IP2A(IC),IP1A(IC))=CRCP(IP2A(IC),IP1A(IC))+T2
         CRCP(IP1A(IC),IP1A(IC))=CRCP(IP1A(IC),IP1A(IC))-T2
   90   CONTINUE
C-----------------------------------------------------------------------
C  SET UP VECTOR OF IONISATION RATE COEFFICIENTS.
C
C      NB. SHIFT IONISATION POTENTIAL ACCORDING TO PARENT
C
C-----------------------------------------------------------------------
        DO 100 I=1,MAXLEV
         CIE(I)=0.0D0
         IF(LPTA(I).NE.0)THEN
             XIAS=XIA(I)
             XIAS=XIAS+DEPA(LPTA(I))
             Y=ATEA(IT)*XIAS
             IF(Y.LT.150.0) THEN
                 T1= ZETA*DSQRT(Y)*DEXP(-Y)*EEI(Y)/XIAS**1.5
                 P = 1.0D0+1.0D0/Y
                 W = (DLOG(P))**(BETA/P)
                 CIE(I)=2.1715D-8*C*T1*W
             ENDIF
         ENDIF
  100   CONTINUE
        DO 280 IN=1,MAXD
C-----------------------------------------------------------------------
C  SET UP  WHOLE ARRAY AND RIGHT-HAND-SIDE EXCLUDING IONISATION
C-----------------------------------------------------------------------
         DO 104 I=1,IL
          IF(LPTA(I).NE.0)THEN
              CRHS(I,LPTA(I))=VECR(IT,I)+RATHA(1)*VECH(IT,I)
          ENDIF
          DO 103 J=1,IL
           CC(I,J)=CRA(I,J)+DENSA(IN)*CRCE(I,J)+DENSPA(IN)*CRCP(I,J)
  103     CONTINUE
  104    CONTINUE
C-----------------------------------------------------------------------
C  OUTPUT DIAGONAL ELEMENTS , CRHS AND OTHER VECTORS BEFORE ADJUSTMENT
C-----------------------------------------------------------------------
         STRGB=BLANKS
         WRITE(STRGB,2005)('  CRHS(I,',IPRT,') ',IPRT=1,NPARNT)
         WRITE(7,2002)STRGB
         DO 106 I=1,IL
          STRGB=BLANKS
          WRITE(STRGB,2006)(CRHS(I,IPRT),IPRT=1,NPARNT)
          WRITE(7,2003)I,STRGA(I),CC(I,I),STRGB,CIE(I),VECR(IT,I),
     &                 VECH(IT,I)
  106    CONTINUE
C-----------------------------------------------------------------------
C  COMBINE EXPANDED-PROJECTED DATA AND THE DIRECT LOW LEVEL DATA
C
C      NB.  PEXMAT ARRAY IS OPPOSITELY SIGNED TO CC ARRAY
C           PECION INCLUDES DENS AS A MULTIPLIER
C           PEXREC INCLUDES DENSH/DENS AS A MULTIPLIER
C
C-----------------------------------------------------------------------
       DO 108 I=1,IL
        IF(IECION.EQ.0)THEN
            CC(I,I)=CC(I,I)-DENS*CIE(I)
        ENDIF
        IF(IEDREC.EQ.0.AND.IERREC.EQ.0)THEN
            IF(VECR(IT,I).LE.0.0D0)THEN
                VECR(IT,I)=PEDREC(I)+PERREC(I)
            ENDIF
        ENDIF
        IF(IEXREC.EQ.0)THEN
            IF(VECH(IT,I).LE.0.0D0.AND.RATHA(1).NE.0.0D0)THEN
                VECH(IT,I)=PEXREC(I)/RATHA(1)
            ENDIF
        ENDIF
        IF(LPTA(I).NE.0)THEN
            CRHS(I,LPTA(I))=VECR(IT,I)+RATHA(1)*VECH(IT,I)
        ENDIF
        IF(IADD.EQ.1)THEN
            IF(LPTA(I).NE.0)THEN
                CRHS(I,LPTA(I))=CRHS(I,LPTA(I))+PEXRHS(I)
            ENDIF
            DO 107 J=1,IL
             CC(I,J)=CC(I,J)-PEXMAT(I,J)
  107       CONTINUE
        ENDIF
  108  CONTINUE
C-----------------------------------------------------------------------
C  OUTPUT DIAGONAL ELEMENTS , CRHS AND OTHER VECTORS AFTER ADJUSTMENT
C-----------------------------------------------------------------------
         STRGB=BLANKS
         WRITE(STRGB,2005)('  CRHS(I,',IPRT,') ',IPRT=1,NPARNT)
         WRITE(7,2004)STRGB
         DO 109 I=1,IL
          STRGB=BLANKS
          WRITE(STRGB,2006)(CRHS(I,IPRT),IPRT=1,NPARNT)
          WRITE(7,2003)I,STRGA(I),CC(I,I),STRGB,CIE(I),VECR(IT,I),
     &                 VECH(IT,I)
  109    CONTINUE
C-----------------------------------------------------------------------
C  STACK UP NON METASTABLE ARRAY
C-----------------------------------------------------------------------
         DO 110 I=1,NORD
          DO 105 J=1,NORD
           CMAT(I,J)=CC(IORDR(I),IORDR(J))
  105     CONTINUE
  110    CONTINUE
C-----------------------------------------------------------------------
C  INVERT MATRIX.
C-----------------------------------------------------------------------
         CALL MATINV(CMAT,NORD,RHS,0,DETERM)
         DO 120 I=1,NORD
          DO 114 J=1,NMET
           POP=0.0D0
           DO 112 K=1,NORD
            POP=POP-CMAT(I,K)*CC(IORDR(K),IMETR(J))
  112      CONTINUE
C-----------------------------------------------------------------------
C  STACK UP POPULATION COMPONENTS
C-----------------------------------------------------------------------
           STACK(I,J,IT,IN)=POP
  114     CONTINUE
C-----------------------------------------------------------------------
C  STACK UP RECOMBINATION + CHARGE EXCHANGE PARTS FOR EACH PARENT
C-----------------------------------------------------------------------
          DO 116 IPRT=1,NPARNT
           STVRH(I,IPRT)=0.0D0
           DO 115 J=1,NORD
            STVRH(I,IPRT)=STVRH(I,IPRT)-CMAT(I,J)*CRHS(IORDR(J),IPRT)
  115      CONTINUE
  116     CONTINUE
  120    CONTINUE
C-----------------------------------------------------------------------
C  NOW DO METASTABLE CONTRIBUTIONS.
C-----------------------------------------------------------------------
         IF(NMET.LE.1)GO TO 180
         DO 130 I=1,NMET
          DO 126 J=1,NMET
           CRED(I,J)=CC(IMETR(I),IMETR(J))
           DO 125 K=1,NORD
            CRED(I,J)=CRED(I,J)+CC(IMETR(I),IORDR(K))*STACK(K,J,IT,IN)
  125      CONTINUE
  126     CONTINUE
          DO 128 IPRT=1,NPARNT
           VRHRED(I,IPRT)=CRHS(IMETR(I),IPRT)
           DO 127 J=1,NORD
            VRHRED(I,IPRT)=VRHRED(I,IPRT)+
     &                     CC(IMETR(I),IORDR(J))*STVRH(J,IPRT)
  127      CONTINUE
  128     CONTINUE
  130    CONTINUE
          DO 132 I=1,NMET
           SIONA(I)=0.0D0
           DO 131 J=1,NMET
            SIONA(I)=SIONA(I)-CRED(J,I)
  131      CONTINUE
           SIONA(I)=SIONA(I)/DENS
  132     CONTINUE
          WRITE(7,2007)TE,DENS,DENSH
          STRGC=BLANKS
          WRITE(STRGC,2008)('JMET=',J,J=1,NMET)
          STRGD=BLANKS
          WRITE(STRGD,2008)('IPRT=',J,J=1,NPARNT)
          WRITE(7,2009)STRGC,STRGD
          DO 140 I=1,NMET
           STRGC=BLANKS
           WRITE(STRGC,2006)(CRED(I,J),J=1,NMET)
           STRGD=BLANKS
           WRITE(STRGD,2006)(VRHRED(I,IPRT),IPRT=1,NPARNT)
           WRITE(7,2010)I,SIONA(I),STRGC,STRGD
  140     CONTINUE
C-----------------------------------------------------------------------
C  DISPLACE ARRAY DOWN FOR INVERSION
C-----------------------------------------------------------------------
        DO 160 I=2,NMET
         DO 150 J=2,NMET
          CMAT(I-1,J-1)=CRED(I,J)
  150    CONTINUE
         RHS(I-1)=-CRED(I,1)
  160   CONTINUE
C-----------------------------------------------------------------------
C  INVERT MATRIX AND SOLVE EQUATIONS.
C-----------------------------------------------------------------------
        NMET1=NMET-1
        CALL MATINV(CMAT,NMET1,RHS,1,DETERM)
C-----------------------------------------------------------------------
C  STACK METASTABLE POPULATIONS
C-----------------------------------------------------------------------
        DO 170 I=2,NMET
         STCKM(I,IT,IN)=RHS(I-1)
  170   CONTINUE
C-----------------------------------------------------------------------
C  CALCULATE RECOMBINATION + CHARGE EXCHANGE PARTS FOR EACH PARENT
C-----------------------------------------------------------------------
        DO 176 I=2,NMET
         DO 175 IPRT=1,NPARNT
          STVRHM(I,IPRT)=0.0D0
          DO 174 J=2,NMET
           STVRHM(I,IPRT)=STVRHM(I,IPRT)-CMAT(I-1,J-1)*VRHRED(J,IPRT)
  174     CONTINUE
  175    CONTINUE
  176   CONTINUE
        SG=-CRED(1,1)
        DO 177 J=2,NMET
         SG=SG-CRED(1,J)*STCKM(J,IT,IN)
  177   CONTINUE
         SG=SG/DENS
        DO 179 IPRT=1,NPARNT
         ALFG(IPRT)=VRHRED(1,IPRT)
         STVRHM(1,IPRT)=0.0D0
         DO 178 J=2,NMET
          ALFG(IPRT)=ALFG(IPRT)+CRED(1,J)*STVRHM(J,IPRT)
  178    CONTINUE
         STVRHM(1,IPRT)=ALFG(IPRT)/(SG*DENS)
  179   CONTINUE
        STCKM(1,IT,IN)=1.0D0
        WRITE(7,2011)
        WRITE(7,2012)('IPRT=',IPRT,IPRT=1,NPARNT)
        WRITE(7,2013)SG,(ALFG(IPRT),IPRT=1,NPARNT)
  180   CONTINUE
  280   CONTINUE
C-----------------------------------------------------------------------
C  WRITE OUT DATA VALUES.
C-----------------------------------------------------------------------
        DO 289 IN=1,MAXD
          WRITE(7,2014)TE,DENS,DENSH
          STRGC=BLANKS
          WRITE(STRGC,2008)('JMET=',J,J=1,NMET)
          STRGD=BLANKS
          WRITE(STRGD,2008)('IPRT=',J,J=1,NPARNT)
          WRITE(7,2009)STRGC,STRGD
        DO 282 I=1,NMET
         STRGC=BLANKS
         WRITE(STRGC,2006)STCKM(I,IT,IN)
         STRGD=BLANKS
C**** NOTE STVRHM MULTIPLIED BY DENSITY  *****
         WRITE(STRGD,2006)(DENS*STVRHM(I,IPRT),IPRT=1,NPARNT)
         WRITE(7,2015)I,IMETR(I),STRGC,STRGD
  282   CONTINUE
         WRITE(7,2016)
         DO 286  J=1,NORD
          STRGC=BLANKS
          WRITE(STRGC,2006)(STACK(J,I,IT,IN),I=1,NMET)
          STRGD=BLANKS
C**** NOTE STVRH MULTIPLIED BY DENSITY  *****
          WRITE(STRGD,2006)(DENS*STVRH(J,IPRT),IPRT=1,NPARNT)
          WRITE(7,2015)J,IORDR(J),STRGC,STRGD
  286   CONTINUE
  289  CONTINUE
  290  CONTINUE
C-----------------------------------------------------------------------
C  OUTPUT TO STREAM 19 FOR REINPUT TO CONTOR PROGRAM
C-----------------------------------------------------------------------
       WRITE(19,1050)ITITLE,IZ,IZ0,IZ1,BWNO
       WRITE(19,1051)IL,NMET,NORD,MAXT,MAXD,ICNTR,ICNTH
       WRITE(19,1054)(IA(I),CSTRGA(I),ISA(I),ILA(I),XJA(I),I=1,IL)
       WRITE(19,1051)(IMETR(I),I=1,NMET)
       WRITE(19,1051)(IORDR(I),I=1,NORD)
       WRITE(19,1052)(TEA(I),I=1,MAXT)
       WRITE(19,1052)(DENSA(I),I=1,MAXD)
       WRITE(19,1053)((((STACK(I,J,K,L),I=1,NORD),J=1,NMET),K=1,MAXT),L=
     &1,MAXD)
       WRITE(19,1053)(((STCKM(I,J,K),I=1,NMET),J=1,MAXT),K=1,MAXD)
  300  CONTINUE
C-----------------------------------------------------------------------
C  CALCULATE THE POPULATIONS OF EACH LEVEL.
C-----------------------------------------------------------------------
C      DO 500 IT=1,MAXT
C       DO 500 IN=1,MAXD
C-----------------------------------------------------------------------
C  WORK OUT THE METASTABLE LEVELS FIRST.
C-----------------------------------------------------------------------
C        POPAR(1)=1.0D0
C        IF(NMET.EQ.1) GOTO 400
C        DO 410 I=2,NMET
C         POPAR(IMETR(I))=STCKM(I,IT,IN)+DENSA(IN)*RATIA(IN)*
C    &                    STVRM(I,IT,IN)+DENSA(IN)*RATHA(IN)*
C    &                    RATIA(IN)*STVHM(I,IT,IN)
C 410    CONTINUE
C-----------------------------------------------------------------------
C  WORK OUT THE POPULATIONS OF EACH LEVEL.
C-----------------------------------------------------------------------
C 400    DO 430 I=1,NORD
C         POPAR(IORDR(I))=DENSA(IN)*RATIA(IN)*STVR(I,IT,IN)+
C    &                    DENSA(IN)*RATHA(IN)*RATIA(IN)*STVH(I,IT,IN)
C         DO 420 J=1,NMET
C          POPAR(IORDR(I))=POPAR(IORDR(I))+STACK(I,J,IT,IN)
C    &                     *POPAR(IMETR(J))
C 420     CONTINUE
C 430    CONTINUE
C-----------------------------------------------------------------------
C  WRITE OUT THE POPULATIONS.
C-----------------------------------------------------------------------
C       WRITE(7,442) TEVA(IT),DENSA(IN)
C 442   FORMAT(/1X,' TE = ',1D13.5,'   NE = ',1D13.5)
C       DO 440 I=1,IL
C        WRITE(7,450) I,POPAR(I)
C 440   CONTINUE
C 450   FORMAT(1X,'LEVEL = ',I4,' POPULATION = ',1D13.5)
C 500  CONTINUE
       CLOSE(9)
       RETURN
 1000  FORMAT(1A3,I2,2I10,F15.0)
 1001  FORMAT(1H ,1X,'ION',9X,'Z0',8X,'Z1',5X,'ION.POT.(W.NO.)'/1X,1A3,I
     &2,2I10,F15.0)
 1002  FORMAT(1H ,'ENERGY LEVELS'/1X,'   I ',4X,'CONFIG.',4X,
     1'(2S+1)L(J)',10X,'W.NO.',8X,'B.W.NO.',6X,'ENER.(RYD)')
 1003  FORMAT(I5,3X,1A12,5X,I1,1X,I1,1X,F4.1,3X,F15.0)
 1004  FORMAT(I5,3X,1A12,1X,'(',I1,')',I1,'(',F4.1,')',2F15.0,
     1F15.7)
 1006  FORMAT(F5.2,I5,6X,8D8.2)
 1008  FORMAT(A1,I3,I4,9D8.2)
 1009  FORMAT(1H /1H ,'DATA BLOCK ',I1,5X,'ZF=',F5.1,5X,'IQS=',I2,5X,
     & 'TE(K)=',1P,8D10.2)
 1011  FORMAT(1P,10D11.3)
 1012  FORMAT(1H ,17X,'ICNTE = ',I3,5X,'ICNTP = ',I3,5X,'ICNTR = ',I3,
     & 5X,'ICNTH = ',I3)
 1013  FORMAT(1H ,17X,'IPSEL = ',I3,5X,'IRSEL = ',I3,5X,'IHSEL = ',I3,
     & 5X,'IZSEL = ',I3,5X,'IADD = ',I3,5X,'ZEFF = ',F5.1)
 1016  FORMAT(2I5)
 1023  FORMAT(9I5,D10.2)
 1036  FORMAT(1H /1H ,'PLASMA PARAMETERS',10X,'MAXT = ',I2,5X,
     & 'MAXD = ',I2/1H ,'ELECTRON TEMPERATURES (EV)')
 1037  FORMAT(1H ,'PROTON TEMPERATURES (EV)')
 1038  FORMAT(1H ,'ELECTRON DENSITIES')
 1039  FORMAT(1H ,'PROTON DENSITIES')
 1040  FORMAT(1H ,'NH / NE')
 1041  FORMAT(1H ,'N(Z+1) / N(Z)')
 1045  FORMAT(2I5,1P,10E12.4)
 1046  FORMAT(1H ,'EQUILIBRIUM METASTABLE POPULATION DEPENDENCE ON DENSI
     &TY AT TE =',1P,E10.2/1H ,'  IMET  I',3X,2I5)
 1047  FORMAT(1H ,'POPULATION DEPENDENCE ON DENSITY AND METASTABLE',I3/1
     &H ,'  IORD  I')
 1048  FORMAT(1H ,I4,7X,I4,12X,1A22)
 1049  FORMAT(1H /1H ,'GROUND AND METASTABLE DETAILS',10X,'NMET = ',I2,
     & 5X,'NORD = ',I3/1H ,'MET.IND.',5X,'IND.',10X,'DESIGNATION')
 1050  FORMAT(1A3,I2,2I10,F15.0)
 1051  FORMAT(16I5)
 1052  FORMAT(1P,8E10.2)
 1053  FORMAT(1P,6E12.4)
 1054  FORMAT(I5,3X,1A12,5X,I1,1X,I1,1X,F4.1)
 1055  FORMAT(1A3,I2,'       ELEC.TEMP. = ',1PE10.2,' (EV)')
 1062  FORMAT(1A12,'(',I1,')',1A1,'(',F4.1,')')
 2001  FORMAT(10D13.5)
 2002  FORMAT(1H ,'LOWPOP  ARRAYS        : BEFORE ADJUSTMENT'/
     &        1H ,'IND     DESIG      QUANT.NOS.       CMAT(I,I) ',
     & 1A36,'  CIE(I)      VECR(I)     VECH(I)')
 2003  FORMAT(1H ,I3,4X,1A22,4X,1P,1D12.4,1A36,1P,3D12.4)
 2004  FORMAT(1H ,'LOWPOP  ARRAYS        : AFTER  ADJUSTMENT'/
     &        1H ,'IND     DESIG      QUANT.NOS.       CMAT(I,I) ',
     & 1A36,'  CIE(I)      VECR(I)     VECH(I)')
 2005  FORMAT(1A9,I1,1A2)
 2006  FORMAT(1P,3D12.4)
 2007  FORMAT(1H ,'METASTABLE/PARENT COUPLINGS    FOR TE(K)=',1PD10.2,
     &        5X,'DENS(CM-3)=',1PD10.2,5X,'DENSH(CM-3)=',1PD10.2/
     &        1H ,'  IMET       SION       COUPLING MATRIX (INCL.SION)
     &                 RECOMBINATION COEFFTS.')
 2008  FORMAT(3(1A5,I1,6X))
 2009  FORMAT(1H ,24X,1A36,10X,1A36)
 2010  FORMAT(1H ,I4,4X,1PD10.2,4X,1A36,10X,1A36)
 2011  FORMAT(1H ,'GROUND/PARENT COUPLINGS'/
     & '         SION        RECOMBINATION COEFFT.')
 2012  FORMAT(1H ,20X,3(1A5,I2,7X))
 2013  FORMAT(1H ,4X,1PD10.2,4X,3(1PD12.4))
 2014  FORMAT(1H ,'EQUIL. POPULATION DEPENDENCES  FOR TE(K)=',1PD10.2,
     &        5X,'DENS(CM-3)=',1PD10.2,5X,'DENSH(CM-3)=',1PD10.2/
     &        1H ,'  IMET       I          EXCITATION PART
     &                 RECOMBINATION PART')
 2015  FORMAT(1H ,I4,7X,I4,7X,1A36,10X,1A36)
 2016  FORMAT(1H ,'  IORD       I')
      END
