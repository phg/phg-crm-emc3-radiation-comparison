CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas310/catmpf.for,v 1.2 2007/05/17 17:03:02 allan Exp $ Date $Date: 2007/05/17 17:03:02 $
CX
      SUBROUTINE CATMPF( IUTMP  , NUCCHG , DSNEX  , DSNCX  ,
     &                   JDENSM , JTEM   , TS     , W      ,
     &                   Z      , CION   , CPY    , W1     ,
     &                   NIP    , INTD   , IPRS   , ILOW   ,
     &                   IONIP  , NIONIP , ILPRS  , IVDISP ,
     &                   ZEFF   , NOSCAN , NIMP   , ZIMPA  ,
     &                   AMIMPA , FRIMPA , DENSA  , TEA    ,
     &                   DENPA  , TPA    , BMENER , DENSH  ,
     &                   NMIN   , NMAX   , IMAX   , NREP   ,
     &                   WBREP  , JCOR   , COR    , JMAX   ,
     &                   EPSIL  , FIJ    , WIJ    , JDEF   ,
     &                   DEFECT
     &                 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C ******************* FORTRAN77 SUBROUTINE: CATMPF *********************
C
C PURPOSE: TO WRITE TEMPORARY FILE CONTAINING PARAMETERS TO BE READ BY
C          SUBROUTINE 'V2BNDLN'.
C
C          THIS ROUTINE IS A TEMPORARY MEASURE TO ALLOW 'V2BNDLN' TO
C          RUN WITHIN THE FRAMEWORK OF ADAS WITHOUT HAVING TO EDIT
C          'V2BNDLN'. THE PARAMETERS SHOULD REALLY BE PASSED INTO
C          'V2BNDLN' THROUGH ITS ARGUMENT LIST.
C
C CALLING PROGRAM: ADAS310
C
C  INPUT : (I*4)   IUTMP    = UNIT NUMBER OF TEMPORARY FILE.
C  INPUT : (I*4)   NUCCHG   = NUCLEAR CHARGE.
C  INPUT : (C*80)  DSNEX    = FULL MVS DATA SET NAME FOR EXPANSION FILE
C                             (SUITABLE FOR DYNAMIC ALLOCATION).
C  INPUT : (C*80)  DSNCX    = FULL MVS DATA SET NAME FOR CHARGE EXCHANGE
C                             DATA SET (SUITABLE FOR DYNAMIC ALLOCATION)
C  INPUT : (I*4)   JDENSM   = NUMBER OF DENSITIES.
C  INPUT : (I*4)   JTEM     = NUMBER OF TEMPERATURES.
C  INPUT : (R*8)   TS       = EXTERNAL RADIATION FIELD TEMPERATURE.
C                             UNITS: K
C  INPUT : (R*8)   W        = EXTERNAL RADIATION FIELD DILUTION FACTOR
C                             (HIGHER LEVELS).
C  INPUT : (R*8)   Z        = RECOMBINING ION CHARGE.
C  INPUT : (R*8)   CION     = MULTIPLIER OF GROUND LEVEL ELECTRON IMPACT
C                             IONISATION RATE COEFFICIENT.
C  INPUT : (R*8)   CPY      = MULTIPLIER OF ELECTRON EXCITATION RATE
C                             COEFFICIENT FROM THE GOUND LEVEL.
C  INPUT : (R*8)   W1       = EXTERNAL RADIATION FIELD DILUTION FACTOR
C                             FOR PHOTO-IONISATION FROM THE GROUND
C                             LEVEL.
C  INPUT : (R*8)   NIP      = RANGE OF DELTA N FOR IMPACT PARAMETER
C                             XSECTS. (.LE. 4)
C  INPUT : (R*8)   INTD     = ORDER OF MAXWELL QUADRATURE FOR XSECTS.
C                             (.LE. 3)
C  INPUT : (R*8)   IPRS     = CONTROLS XSECTS BEYOND NIP RANGE.
C                             0 => DEFAULT TO VAN REGEMORTER XSECTS.
C                             1 => USE PERCIVAL-RICHARDS XSECTS.
C  INPUT : (R*8)   ILOW     = CONTROLS ACCESS OF SPECIAL LOW LEVEL DATA.
C                             0 => NO SPECIAL LOW LEVEL DATA ACCESSED.
C                             1 => SPECIAL LOW LEVEL DATA ACCESSED.
C  INPUT : (R*8)   IONIP    = CONTROLS INCLUSION OF ION IMPACT
C                             COLLISIONS.
C                             0 => NO ION IMPACT COLLISIONS INCLUDED.
C                             1 => ION IMPACT EXCITATION AND IONISATION
C                                  INCLUDED.
C  INPUT : (R*8)   NIONIP   = RANGE OF DELTA N FOR ION IMPACT EXCITATION
C                             XSECTS.
C  INPUT : (R*8)   ILPRS    = CONTROLS USE OF LODGE-PERCIVAL-RICHARDS
C                             XSECTS.
C                             0 => DEFAULT TO VAINSHTEIN XSECTS.
C                             1 => USE LODGE-PERCIVAL-RICHARDS XSECTS.
C  INPUT : (R*8)   IVDISP   = CONTROLS USE OF BEAM ENERGY IN CALCULATION
C                             OF XSECTS.
C                             0 => ION IMPACT AT THERMAL MAXWELLIAN
C                                  ENERGIES.
C                             1 => ION IMPACT AT DISPLACED THERMAL
C                                  ENERGIES ACCORDING TO THE NEUTRAL
C                                  BEAM ENERGY PARAMETER.
C                             NB:  IF IVDISP=0 THEN SPECIAL LOW LEVEL
C                                  DATA FOR ION IMPACT IS NOT
C                                  SUBSTITUTED - ONLY VAINSHTEIN AND
C                                  LODGE ET AL. OPTIONS ARE OPEN.
C                                  ELECTRON IMPACT DATA SUBSTITUTION
C                                  DOES OCCUR.
C  INPUT : (R*8)   ZEFF     = NUCLEAR CHARGE OF IMPURITY.
C                             (ONLY SET IF 'NOSCAN'=0 )
C  INPUT : (I*4)   NOSCAN   = CONTROLS MODE OF OPERATION.
C                             0 => SINGLE IMPURITY.
C                             1 => MULTIPLE IMPURITIES.
C  INPUT : (I*4)   NIMP     = NUMBER OF IMPURITY SPECIES
C                             (ONLY SET IF 'NOSCAN'=1 )
C  INPUT : (R*8)   ZIMPA()  = NUCLEAR CHARGE OF IMPURITIES.
C                             (ONLY SET IF 'NOSCAN'=1 )
C                             DIMENSION: NIMP
C  INPUT : (R*8)   AMIMPA() = ATOMIC MASS NUMBERS OF IMPURITIES.
C                             (ONLY SET IF 'NOSCAN'=1 )
C                             DIMENSION: NIMP
C  INPUT : (R*8)   FRIMPA() = IMPURITY FRACTIONS.
C                             (ONLY SET IF 'NOSCAN'=1 )
C                             DIMENSION: NIMP
C  INPUT : (R*8)   DENSA()  = ELECTRON DENSITIES.
C                             UNITS: CM-3
C                             DIMENSION: JDENSM
C  INPUT : (R*8)   TEA()    = ELECTRON TEMPERATURES.
C                             UNITS: K
C                             DIMENSION: JTEM
C  INPUT : (R*8)   DENPA()  = PROTON DENSITIES.
C                             UNITS: CM-3
C                             DIMENSION: JDENSM
C  INPUT : (R*8)   TPA()    = PROTON TEMPERATURES.
C                             UNITS: K
C                             DIMENSION: JTEM
C  INPUT : (R*8)   BMENER   = NEUTRAL BEAM PARTICLE ENERGY.
C                             UNITS: EV / AMU
C  INPUT : (R*8)   DENSH    = NEUTRAL HYDROGEN DENSITY IN BEAM.
C                             UNITS: CM-3
C  INPUT : (I*4)   NMIN     = LOWEST N-SHELL.
C  INPUT : (I*4)   NMAX     = HIGHEST N-SHELL.
C  INPUT : (I*4)   IMAX     = NUMBER OF REPRESENTATIVE N-SHELL LEVELS.
C  INPUT : (I*4)   NREP()   = SET OF REPRESENTATIVE N-SHELL LEVELS.
C                             DIMENSION: IMAX
C  INPUT : (R*8)   WBREP()  =
C                             DIMENSION: IMAX
C  INPUT : (I*4)   JCOR     =
C  INPUT : (R*8)   COR()    =
C                             DIMENSION: JCOR
C  INPUT : (I*4)   JMAX     =
C  INPUT : (R*8)   EPSIL()  =
C                             DIMENSION: JMAX
C  INPUT : (R*8)   FIJ()    =
C                             DIMENSION: JMAX
C  INPUT : (R*8)   WIJ()    =
C                             DIMENSION: JMAX
C  INPUT : (I*4)   JDEF     = NUMBER OF QUAMTUM DEFECTS.
C  INPUT : (R*8)   DEFECT() = SET OF QUANTUM DEFECT.
C                             DIMENSION: JDEF
C
C          (I*4)   I        = ARRAY INDEX.
C
C          (L*4)   LOPEN    = FLAGS IF SCRATCH FILE OPEN.
C                             .TRUE.  => SCRATCH FILE OPEN.
C                             .FALSE. => SCRATCH FILE CLOSED.
C
C AUTHOR:  JONATHAN NASH  (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    17/01/94
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 08-02-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C VERSION: 1.2                          DATE: 17-05-07
C MODIFIED: Allan Whiteford
C               - Updated comments as part of subroutine documentation
C                 procedure.
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    IUTMP   , NUCCHG  , JDENSM  , JTEM    , NIP     ,
     &           INTD    , IPRS    , ILOW    , IONIP   , NIONIP  ,
     &           ILPRS   , IVDISP  , NOSCAN  , NIMP    , NMIN    ,
     &           NMAX    , IMAX    , JCOR    , JMAX    , JDEF
      INTEGER    I
C-----------------------------------------------------------------------
      REAL*8     TS      , W       , Z       , CION    , CPY     ,
     &           W1      , ZEFF    , BMENER  , DENSH
C-----------------------------------------------------------------------
      LOGICAL    LOPEN
C-----------------------------------------------------------------------
      CHARACTER  DSNEX*80   , DSNCX*80
C-----------------------------------------------------------------------
      INTEGER    NREP(IMAX)
C-----------------------------------------------------------------------
      REAL*8     ZIMPA(NIMP)    , AMIMPA(NIMP)   , FRIMPA(NIMP)   ,
     &           DENSA(JDENSM)  , TEA(JTEM)      , DENPA(JDENSM)  ,
     &           TPA(JTEM)      , WBREP(IMAX)    , COR(JCOR)      ,
     &           EPSIL(JMAX)    , FIJ(JMAX)      , WIJ(JMAX)      ,
     &           DEFECT(JDEF)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C OPEN TEMPORARY OUTPUT FILE.
C-----------------------------------------------------------------------
C
      INQUIRE( IUTMP , OPENED=LOPEN )
      IF ( LOPEN ) THEN
         REWIND( UNIT=IUTMP )
      ELSE
         OPEN( UNIT=IUTMP , STATUS='SCRATCH' , FORM='FORMATTED' ,
     &         ACCESS='SEQUENTIAL' )
      ENDIF
C
C-----------------------------------------------------------------------
C WRITE PARAMETERS TO TEMPORARY FILE.
C-----------------------------------------------------------------------
C
      WRITE(IUTMP,1000) NUCCHG , DSNEX , DSNCX , 0
      WRITE(IUTMP,1001) JDENSM , JTEM , TS , W , Z , CION , CPY , W1
      WRITE(IUTMP,1002) NIP , INTD , IPRS , ILOW , IONIP , NIONIP ,
     &                  ILPRS , IVDISP , ZEFF , NOSCAN , NIMP
      IF ( NOSCAN .EQ. 1 ) THEN
         DO 1 I = 1 , NIMP
            WRITE(IUTMP,1003) I , ZIMPA(I) , AMIMPA(I) ,
     &                        FRIMPA(I)
    1    CONTINUE
      ENDIF
      WRITE(IUTMP,1004) ( DENSA(I) , I=1,JDENSM )
      WRITE(IUTMP,1004) ( TEA(I) , I=1,JTEM )
      WRITE(IUTMP,1004) ( DENPA(I) , I=1,JDENSM )
      WRITE(IUTMP,1004) ( TPA(I) , I=1,JTEM )
      WRITE(IUTMP,1004) BMENER , DENSH
      WRITE(IUTMP,1005) NMIN , NMAX , IMAX
      WRITE(IUTMP,1006) ( NREP(I) , I=1,IMAX )
      WRITE(IUTMP,1004) ( WBREP(I) , I=1,IMAX )
      WRITE(IUTMP,1007) JCOR
      WRITE(IUTMP,1008) (COR(I) , I=1,JCOR )
      WRITE(IUTMP,1007) JMAX
      WRITE(IUTMP,1008) ( EPSIL(I) , I=1,JMAX )
      WRITE(IUTMP,1008) ( FIJ(I) , I=1,JMAX )
      WRITE(IUTMP,1004) ( WIJ(I) , I=1,JMAX )
      WRITE(IUTMP,1007) JDEF
      WRITE(IUTMP,1008) ( DEFECT(I) , I=1,JDEF )
      WRITE(IUTMP,1009)
C
C-----------------------------------------------------------------------
C
 1000 FORMAT( ' &FILINFO' / ' NUCCHG=' , I2 , ',' /
     &        ' EXMEMB=''' , A , ''',' / ' CXMEMB=''' , A , ''',' /
     &        ' IBLOCK=' , I1 / ' &END' )
 1001 FORMAT( 2I5 , 1P , 6E10.2 )
 1002 FORMAT( 8I5 , F5.1 , 2I5 )
 1003 FORMAT( I5 , 3F10.5 )
 1004 FORMAT( 1P , 7E10.2 )
 1005 FORMAT( 3I5 )
 1006 FORMAT( 14I5 )
 1007 FORMAT( I5 )
 1008 FORMAT( 7F10.5 )
 1009 FORMAT( '   -1' )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
