CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas310/capasf.for,v 1.2 2004/07/06 11:59:23 whitefor Exp $ Date $Date: 2004/07/06 11:59:23 $
CX
      SUBROUTINE CAPASF( IUPS1  , IUPS2  , IUPS3  , IUPS4  , IURUN ,
     &                   DSNPS1 , DSNPS2 , DSNPS3 , DSNPS4 , DSNRUN,
     &                   LPASS1 , LPASS2 , LPASS3 , LPASS4 , LRUN  ,
     &                   IRCODE  )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: CAPASF *********************
C
C  PURPOSE: HANDLES OPENING OF OUTPUT PASSING FILES.
C
C  CALLING PROGRAM: ADAS310
C
C  SUBROUTINE:
C
C  INPUT : (I*4)   IUPS1    = UNIT NO. FOR FIRST PASSING FILE.
C  INPUT : (I*4)   IUPS2    = UNIT NO. FOR SECOND PASSING FILE.
C  INPUT : (I*4)   IUPS3    = UNIT NO. FOR THIRD PASSING FILE.
C  INPUT : (I*4)   IUPS4    = UNIT NO. FOR FOURTH PASSING FILE.
C  INPUT : (I*4)   IURUN    = UNIT NO. FOR RUN SUMMARY FILE.
C  INPUT : (C*80)  DSNPS1   = FIRST PASSING FILE DATA SET NAME.
C  INPUT : (C*80)  DSNPS2   = SECOND PASSING FILE DATA SET NAME.
C  INPUT : (C*80)  DSNPS3   = THIRD PASSING FILE DATA SET NAME.
C  INPUT : (C*80)  DSNPS4   = FOURTH PASSING FILE DATA SET NAME.
C  INPUT : (C*80)  DSNRUN   = RUN SUMMARY FILE DATA SET NAME.
C  INPUT : (L*4)   LPASS1   = FLAG FOR WHETHER OR NOT 1ST PASSING FILE
C                             HAS BEEN REQUESTED
C  INPUT : (L*4)   LPASS2   = FLAG FOR WHETHER OR NOT 2ND PASSING FILE
C                             HAS BEEN REQUESTED
C  INPUT : (L*4)   LPASS3   = FLAG FOR WHETHER OR NOT 3RD PASSING FILE
C                             HAS BEEN REQUESTED
C  INPUT : (L*4)   LPASS4   = FLAG FOR WHETHER OR NOT 4TH PASSING FILE
C                             HAS BEEN REQUESTED
C
C  OUTPUT: (I*4)   IRCODE   = RETURN CODE AFTER ATTEMPTING TO OPEN FILES.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    17/01/94
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 08-02-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C VERSION: 1.2                          DATE: 02-04-96
C MODIFIED: TIM HAMMOND/PAUL BRIDEN
C               - REMOVED FILE= ARGUMENT FROM SCRATCH FILE OPEN 
C                 STATEMENTS AS IT CAN CAUSE A COMPILATION ERROR OF 
C                 WARNING MESSAGE UNDER AIX.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER     IUPS1   , IUPS2   , IUPS3   , IUPS4 , IURUN
      INTEGER     IRCODE
C-----------------------------------------------------------------------
      CHARACTER   DSNPS1*80  , DSNPS2*80  , DSNPS3*80  , DSNPS4*80,
     &            DSNRUN*80
C-----------------------------------------------------------------------
      LOGICAL    LPASS1  , LPASS2  , LPASS3  , LPASS4 , LRUN
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C OPEN PASSING FILES.
C-----------------------------------------------------------------------
C
      IRCODE = 0
      IF (LPASS1) THEN
          OPEN( UNIT=IUPS1 , FILE=DSNPS1 , STATUS='UNKNOWN' , ERR=9999)
      ELSE
          OPEN( UNIT=IUPS1 ,               STATUS='SCRATCH' , ERR=9999)
      ENDIF
      IF (LPASS2) THEN
          OPEN( UNIT=IUPS2 , FILE=DSNPS2 , STATUS='UNKNOWN' , ERR=9999)
      ELSE
          OPEN( UNIT=IUPS2 ,               STATUS='SCRATCH' , ERR=9999)
      ENDIF
      IF (LPASS3) THEN
          OPEN( UNIT=IUPS3 , FILE=DSNPS3 , STATUS='UNKNOWN' , ERR=9999)
      ELSE
          OPEN( UNIT=IUPS3 ,               STATUS='SCRATCH' , ERR=9999)
      ENDIF
      IF (LPASS4) THEN
          OPEN( UNIT=IUPS4 , FILE=DSNPS4 , STATUS='UNKNOWN' , ERR=9999)
      ELSE
          OPEN( UNIT=IUPS4 ,               STATUS='SCRATCH' , ERR=9999)
      ENDIF
      IF (LRUN) THEN
          OPEN( UNIT=IURUN , FILE=DSNRUN , STATUS='UNKNOWN' , ERR=9999)
      ELSE
          OPEN( UNIT=IURUN ,               STATUS='SCRATCH' , ERR=9999)
      ENDIF
      RETURN
C
C-----------------------------------------------------------------------
C PROGRAM RETURNS FROM THIS POINT IF THERE WAS AN ERROR
C-----------------------------------------------------------------------
C

9999  IRCODE = 1
      RETURN

      END
