CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas310/v2bnmod.for,v 1.11 2019/04/18 09:05:05 mog Exp $ Date $Date: 2019/04/18 09:05:05 $
CX
       SUBROUTINE V2BNMOD(IPOSNT , JDENS  , JTE    , NREP   , F1     ,
     &                    F2     , F3     , BNCALC , BNACT  , XPOP   , 
     &                    IMAX   , DENSH  , DENS   , DENSP  , TE     ,
     &                    TP     , BMENER , FLUX   , DEXPTE , ALFA   ,
     &                    S      , DSLPATH, NIMP   , ZIMPA  , ZEFF   ,
     &                    DNIMPA , lnoion , lnocx  
     &                   )

       IMPLICIT REAL*8 (A-H,O-Z)

C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: V2BNMOD ********************
C
C  VERSION:  (ADAS91) - SEE SC CS COMMENTS FOR VERSION NO.
C
C  PURPOSE:  THIS CODE PERFORMS THE ACTUAL CALCULATIONS FOR ADAS 310
C            IT IS IN AN INCOMPLETE VERSION AND THESE COMMENTS DO NOT
C            YET FOLLOW THE STANDARD ADAS PATTERN.
C-----------------------------------------------------------------------
C      CALCULATION OF BN -1 CASE A,B
C
C      EXCIT. XSECT. OPTIONS:
C      ----------------------
C                     (A) VAN REGEMORTER    - ELECTRONS
C                     (B) IMPACT PARAMETER  - ELECTRONS
C                                             PROTONS
C                     (C) PERCIVAL-RICHARDS - ELECTRONS
C                                           - PROTONS & ZIMP IONS
C                     (D) VAINSHTEIN        - PROTONS & ZIMP IONS
C                     (E) SPECIAL LOW LEVEL - ELECTRONS
C                                           - PROTONS & ZIMP IONS
C      IONIS. XSECT. OPTIONS:
C      ----------------------
C                     (A) ECIP              - ELECTRONS
C                     (B) PERCIVAL-RICHARDS - PROTONS & ZIMP IONS
C                     (C) SPECIAL LOW LEVEL - ELECTRONS
C                                           - PROTONS & ZIMP IONS
C      CX RECOM. XSECT. OPTIONS:
C      -------------------------
C                     (A) SPECIAL           - H(1S) DONOR
C
C
C  NOTES
C  -----
C      (A) SPECIAL LOW LEVEL DATA ACCESSED BY SPECIFIC ION ROUTINE
C
C          ION   ROUTINE    ACCESSED FILE              ACQUIS. ROUTINE
C          ---   -------    -------------              ---------------
C           H0    NSUPH1    IONATOM.DATA(H)                 QH
C                           HLIKE.DATA(AGG1984)              -
C
C
C      (B) SPECIAL CHARGE EXCHANGE DATA FROM  CHEXDATA.DATA
C
C          ION    DONOR     ROUTINE    DATA MEMBER     ACQUIS. ROUTINE
C          ---    -----     -------    -----------     ---------------
C           H0    H(1S)     BNQCTB       H1NEW1            BNQCTB
C
C
C
C
C  INPUT
C      IPOSNT   = 0  BYPASS
C               = 1  LEAVE W1.NE.0 AND FORCE DENSH.EQ.0
C               = 2  FORCE W1.EQ.0 AND FORCE DENSH.EQ.0
C               = 3  LEAVE W1.NE.0 AND LEAVE DENSH.NE.0
C               = 4  FORCE W1.EQ.0 AND LEAVE DENSH.NE.0
C               = 5  BYPASS
C      JDENS    = DENSITY     SELECTOR
C      JTE      = TEMPERATURE SELECTOR
C      NREP(I)  = PRINCIPAL QUANTUM NUMBER OF ITH REPRESENTATIVE LEVEL
C      XPOP(I)  = POPULATIONS OF REPRESENTATIVE LEVELS
C      IMAX     = NUMBER OF REPRESENTATIVE LEVELS
C      DENSH    = NEUTRAL HYDROGEN DENSITY IN BEAM  (CM-3)
C      DENS     = ELECTRON DENSITY  (CM-3)
C      DENSP    = PROTON/DEUTERON DENSITY  (CM-3)
C      TE       = ELECTRON TEMPERATURE (K)
C      TP       = PROTON/DEUTERON TEMPERATURE (K)   (SAME FOR ZIMP IONS)
C      BMENER   = NEUTRAL BEAM PARTICLE ENERGY  (EV/AMU)
C      FLUX     = NEUTRAL BEAM FLUX  (CM-2 SEC-1)
C      DEXPTE(I)= EXP(ATE/NREP(I)**2) FOR ITHE REPRESENTATIVE LEVEL
C      DSLPATH  = STRING CONTAINING PATH TO THE REQUIRED INPUT FILES
C                 CALLED BY NSUPH1.FOR
C
C  OUTPUT
C      F1       =
C      F2       =
C      F3       =
C      BNCALC   =
C      BNACT    =
C      ALPHA    =
C      S        =
C
C  INPUT SPECIFICATION FOR STREAM 51 DATA FILE
C
C      NUCCHG = NUCLEAR CHARGE
C      EXMEMB = DATA SET NAME OF EXPANSION FILE USED BY CLDLBN2
C      CXMEMB = DATA SET NAME FOR CHARGE EXCHANGE DATA TO BE USED BY
C               BNQCTB
C      IBLOCK = SUB-BLOCK SELECTOR FOR CXMEMB
C
C      JDENSM = NUMBER OF DENSITIES
C      JTEM   = NUMBER OF TEMPERATURES
C      TS     = EXTERNAL RADIATION FIELD TEMPERATURE (K)
C      W      = EXTERNAL RADIATION FIELD DILUTION FACTOR (HIGH LEVELS)
C      Z      = RECOMBINING ION CHARGE  (Z1 IN USUAL NOTATION)
C      CION   = MULTIPLIER OF GROUND LEVEL ELECTRON IMPACT IONISATION
C               RATE COEFFICIENT
C      CPY    = MULTIPIER ON ELECTRON IMPACT EXCITATION RATE COEFFICIENT
C               FORM THE GROUND LEVEL
C      W1     = EXTERNAL RADIATION FIELD DILUTION FACTOR FOR PHOTO-IONI
C               SATION FORM THE GROUND LEVEL.
C
C      NIP    = RANGE OF DELTA N FOR IMPACT PARAMETER XSECTS. (LE.4)
C      INTD   = ORDER OF MAXWELL QUADRATURE FOR XSECTS. (LE.3)
C      IPRS   = 0   DEFAULT TO VAN REGEMORTER XSECTS. BEYOND NIP RANGE
C               1   USE PERCIVAL-RICHARDS XSECTS.     BEYOND NIP RANGE
C      ILOW   = 0   NO SPECIAL LOW LEVEL DATA ACCESSED
C               1      SPECIAL LOW LEVEL DATA ACCESSED
C      IONIP  = 0   NO ION IMPACT COLLISIONS INCLUDED
C               1      ION IMPACT EXCITATION AND IONISATION INCLUDED
C      NIONIP = RANGE OF DELTA N FOR ION IMPACT EXCITATION XSECTS.
C      ILPRS  = 0   DEFAULT TO VAINSHTEIN XSECTS.
C               1   USE LODGE-PERCIVAL-RICHARDS XSECTS.
C      IVDISP = 0   ION IMPACT AT THERMAL MAXWELLIAN ENERGIES
C               1   ION IMPACT AT DISPLACED THERMAL ENERGIES ACCORDING
C                   TO THE NEUTRAL BEAM ENERGY PARAMETER
C                   *   IF(IVDISP=0 THEN SPECIAL LOW LEVEL DATA FOR ION
C                       IMPACT IS NOT SUBSTITUTED - ONLY VAINSHTEIN AND
C                       LODGE ET AL. OPTIONS ARE OPEN.  ELECTRON IMPACT
C                       DATA SUBSTITUTION DOES OCCUR.
C      1   = PLASMA Z EFFECTIVE
C      NOSCAN = 0   EXECUTE SCANNING VERSION OF CODE
C               1   EXECUTE SIMULTANEOUS IMPURITY NO SCAN RUN
C      NIMP   = NUMBER OF IMPURITIES (EXCL.H+) IN NO SCAN CASE
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          MATINV     ADAS      MATRIX INVERSION WITH ACCOMPANYING 
C                               SOLUTION OF LINEAR EQUATIONS
C          DIEL_310    ????      
C
C
C
C  ********* H.P.SUMMERS, JET               11 APR 1990  ***************
C  *********                           ALT. 17 JUL 1991  SUB. RQINEW,
C                                                        RQLNEW, RQVNEW
C  *********                           ALT. 10 JAN 1994  ALLOW MULTIPLE
C                                                        SIMULT.IMPURITY
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C
C UPDATE:  19/01/94 - JONATHAN NASH - TESSELLA SUPPORT SERVICES PLC
C
C          THE FOLLOWING MODIFICATIONS HAVE BEEN MADE TO THE SUBROUTINE:
C
C          1) THE INPUT FILE UNIT NUMBER HAS BEEN CHANGED FROM 5 TO 51.
C
C          2) THE SIZES OF 'EXMEMB' AND 'CXMEMB' HAVE BEEN INCREASED
C             AS THEY NOW CONTAIN THE FULL DATA SET NAMES RATHER THAN
C             JUST THE MEMBER NAMES.
C
C          3) A PARAMETER FLAG HAS BEEN ADDED TO SWITCH ON/OFF
C             DIAGNOSTIC PRINTING (UNIT 6).
C
C  NOTES:  NO ATTEMPT HAS BEEN MADE TO RESTRUCTURE THE ROUTINE. RATHER
C          THE MINIMUM AMOUNT OF WORK TO INTEGRATE THE ROUTINE INTO
C          ADAS310 HAS BEEN COMPLETED.
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 12-12-95
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C               - REPLACED ALL HOLLERITH CONSTANTS H0 ANF H1 WITH 
C                 H<SPACE>
C               - REPLACED HOLLERITH CONSTANTS WITH STANDARD STRINGS
C                 IN FORMAT STATEMENTS 117, 128 AND 129.
C               - TIDIED UP PARTS OF THE COMMENTS AND CODE
C               - ADDED STRING DSLPATH TO BE USED TO CONSTRUCT UNIX
C                 STYLE FILENAMES IN NSUPH1.FOR
C
C VERSION: 1.2                          DATE: 24-01-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - ADDED NBENG TO INPUT LIST AND CALL TO BNQCTB
C
C VERSION: 1.3                          DATE: 24-01-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - REVERSED ABOVE CHANGE
C
C VERSION: 1.4                          DATE: 27-09-96
C MODIFIED: WILLIAM OSBORN + HUGH SUMMERS
C               - REMOVED MISTAKEN TEST FOR 1.LE.0.0D0 IN
C                 THE CASE OF MULTIPLE IMPURITIES.
C VERSION: 1.5
C MODIFIED: HARVEY ANDERSON
C               - IMPROVED THE HANDLING OF MULTIPLE IMPURITIES TO
C                 INCLUDE HYDROGEN.
C               - ALTERED THE CALLING STRUCTURE OF THIS ROUTINE TO
C                 ALLOW ADDITIONAL VARIABLES TO BE PASSED TO RUN310.
C               - INCREASED THE SIZE OF THE ARRAYS DENSA, DENPA, TEA
C                 AND TPA FROM 10 TO 25.
C
C VERSION : 1.6                                                  
C DATE    : 04-04-2000
C MODIFIED: RICHARD MARTIN
C               - CHANGED NAME OF DIEL SUBROUTINE TO DIEL_310
C
C VERSION : 1.7                                                  
C DATE    : 22-02-2005
C MODIFIED: Martin O'Mullane
C               - A real*8 parameter of cldlbn2 which was passed as an 
C                 integer (1) to cldlbn2 is changed to a double (1.0D0).
C               - Direct debug output stream to unit 0.
C               - Remove IBM error underflow and errset routines.
C
C VERSION : 1.8                                                  
C DATE    : 27-06-2007
C MODIFIED: Martin O'Mullane
C               - Add lpass as argument to cldlbn2. Set to .TRUE.
C
C VERSION : 1.9                        
C DATE    : 18-02-2010
C MODIFIED: Martin O'Mullane
C           - Pass in as arguments lnoion and lnocx.
C
C VERSION : 1.10
C DATE    : 30-06-2010
C MODIFIED: Martin O'Mullane
C           - Replace 1.0e-50 with 1.0D-50 to enable use of Lahey compiler.
C
C VERSION : 1.10
C DATE    : 17-04-2019
C MODIFIED: Martin O'Mullane
C           - Write CR matrix to the second passing file.
C
C-----------------------------------------------------------------------
       PARAMETER (NDLOW=10)
       CHARACTER ELSYMB*2,SYMBA*2,EXMEMB*80,CXMEMB*80
       CHARACTER DSLPATH*80
       DIMENSION EN2(550),EN3(550),DEXPTE(550),DEXPTS(550),OGARL(1100)
       DIMENSION A(550),ARED(30,30),AGRL(550,3),NREP(31),INTER(550)
       DIMENSION RHS(30),BREP(30),DEXPBN(30),EN23(550),FLAG(550)
       DIMENSION COR(20),EPSIL(10),FIJ(10),POP(30),DEFECT(10)
       DIMENSION DENSA(25),TEA(25),DENPA(25),TPA(25)
       DIMENSION WIJ(10),EEIJ(10),WEIJ(10)
       DIMENSION PYMAT(550,4,2),DELCN(30),WBREP(30),WB(550),WBLOG(30)
       DIMENSION BNCALC(30),BNACT(30),F1(30),F2(30),F3(30),XPOP(2)
       DIMENSION SYMBA(30),QTHREP(31)
       DIMENSION BMENA(6),BMFRA(6)
       DIMENSION DVEC(30),CIONPT(30),DRECPT(30),RRECPT(30),XRECPT(30)
       DIMENSION IMPA(10),ZIMPA(10),AMIMPA(10),FRIMPA(10),DNIMPA(10)
C
       DIMENSION XTBE(NDLOW,NDLOW),LXTBE(NDLOW,NDLOW)
       DIMENSION XTBP(NDLOW,NDLOW),LXTBP(NDLOW,NDLOW)
       DIMENSION XTBZ(NDLOW,NDLOW),LXTBZ(NDLOW,NDLOW)
       DIMENSION PXTBE(NDLOW),LPXTBE(NDLOW)
       DIMENSION PXTBP(NDLOW),LPXTBP(NDLOW)
       DIMENSION PXTBZ(NDLOW),LPXTBZ(NDLOW)
       DIMENSION STBE(NDLOW),LSTBE(NDLOW)
       DIMENSION STBP(NDLOW),LSTBP(NDLOW)
       DIMENSION STBZ(NDLOW),LSTBZ(NDLOW)
       LOGICAL   DEBUG, lpass
       logical lnoion, lnocx
C
       DATA SYMBA/'H ','HE','LI','BE','B ','C ','N ','O ','F ','NE',
     &            'NA','MG','AL','SI','P ','S ','CL','AR','K ','CA',
     &            'SC','TI','V ','CR','MN','FE','CO','NI','CU','ZN'/
       NAMELIST /FILINFO/NUCCHG,EXMEMB,CXMEMB,IBLOCK
       DEBUG = .FALSE.
       lpass = .TRUE.

C-----------------------------------------------------------------------
C  FORCE TERMINATION IF FILES CANNOT BE OPENED
C-----------------------------------------------------------------------
       REWIND( UNIT=51 )
       EM=918.0
       CUT=170.0
       IPOSS=0
       READ(51,FILINFO)
       IZ0 = NUCCHG
       Z0 = IZ0
       ELSYMB=SYMBA(IZ0)
  197  READ(51,100)JDENSM,JTEM,TS,W,Z,CION,CPY,W1
       IF(IPOSS.EQ.1.OR.IPOSNT.GT.4)GOTO 411
       IF(W1.NE.0.00D+00)GOTO 404
       write(0,1997)W1
       STOP
  404  IF(IPOSNT.EQ.0)GOTO 411
       IF(IPOSNT.EQ.2.OR.IPOSNT.EQ.4)W1=0.00D+00
  411  IF(JDENSM)198,199,199
  198  return
  199  IF (DEBUG) write(0,117)
       IF (DEBUG) write(0,101)JDENSM,JTEM,TS,W,Z,CION,CPY,W1
       READ(51,2000)NIP,INTD,IPRS,ILOW,IONIP,NIONIP,ILPRS,IVDISP,ZEFF,
     &             NOSCAN,NIMP
       IF (DEBUG) write(0,137)
       IF (DEBUG) write(0,1999)NIP,INTD,IPRS,ILOW,IONIP,NIONIP,
     &                         ILPRS,IVDISP,ZEFF,NOSCAN,NIMP
C
       IF(NOSCAN.EQ.1) THEN
           IF (DEBUG) write(0,146)
           ZSUM1=0.0
           ZSUM2=0.0
           DO 20 IMP=1,NIMP
             READ(51,145)IMPA(IMP),ZIMPA(IMP),AMIMPA(IMP),FRIMPA(IMP)
             IF (DEBUG) write(0,145)IMPA(IMP),ZIMPA(IMP),AMIMPA(IMP),
     &                              FRIMPA(IMP)
   20      CONTINUE
       ENDIF
C
       READ(51,134)(DENSA(J),J=1,JDENSM)
       IF (DEBUG) write(0,135)
       IF (DEBUG) write(0,134)(DENSA(J),J=1,JDENSM)
       READ(51,134)(TEA(J),J=1,JTEM)
       IF (DEBUG) write(0,136)
       IF (DEBUG) write(0,134)(TEA(J),J=1,JTEM)
       READ(51,134)(DENPA(J),J=1,JDENSM)
       IF (DEBUG) write(0,141)
       IF (DEBUG) write(0,134)(DENPA(J),J=1,JDENSM)
       READ(51,134)(TPA(J),J=1,JTEM)
       IF (DEBUG) write(0,142)
       IF (DEBUG) write(0,134)(TPA(J),J=1,JTEM)
C-----------------------------------------------------------------------
C  INPUT NEUTRAL HYDROGEN DENSITY AND INJECTION ENERGY (EV/AMU).
C  COMPUTE FLUX AND PRINT OUT.
C-----------------------------------------------------------------------
       READ(51,134)BMENER,DENSH
       IF(DENSH.NE.0.00D+00)GOTO 405
       IF (DEBUG) write(0,1998)DENSH
       STOP
  405  IF(IPOSNT.EQ.0)GOTO 771
       IF(IPOSNT.EQ.1.OR.IPOSNT.EQ.2)DENSH=0.00D+00
  771  FLUX=1.38317498E6*DSQRT(BMENER)*DENSH
       IF (DEBUG) write(0,147)
       IF (DEBUG) write(0,148)BMENER,DENSH,FLUX
       READ (51,102)NMIN,NMAX,IMAX
       IF (DEBUG) write(0,118)
       IF (DEBUG) write(0,102)NMIN,NMAX,IMAX
       READ (51,103)(NREP(I),I=1,IMAX)
       IF (DEBUG) write(0,119)
       IF (DEBUG) write(0,103)(NREP(I),I=1,IMAX)
       READ (51,134)(WBREP(I),I=1,IMAX)
       IF (DEBUG) write(0,140)
       IF (DEBUG) write(0,134)(WBREP(I),I=1,IMAX)
       READ (51,113)JCOR
       IF (DEBUG) write(0,120)
       IF (DEBUG) write(0,113)JCOR
       READ (51,114)(COR(J),J=1,JCOR)
       IF (DEBUG) write(0,121)
       IF (DEBUG) write(0,114)(COR(J),J=1,JCOR)
       READ (51,113)JMAX
       IF (DEBUG) write(0,122)
       IF (DEBUG) write(0,113)JMAX
       READ (51,115)(EPSIL(J),J=1,JMAX)
       IF (DEBUG) write(0,123)
       IF (DEBUG) write(0,115)(EPSIL(J),J=1,JMAX)
       READ (51,115)(FIJ(J),J=1,JMAX)
       IF (DEBUG) write(0,124)
       IF (DEBUG) write(0,115)(FIJ(J),J=1,JMAX)
       READ(51,134)(WIJ(J),J=1,JMAX)
       IF (DEBUG) write(0,144)
       IF (DEBUG) write(0,134)(WIJ(J),J=1,JMAX)
       READ (51,113)JDEF
       IF (DEBUG) write(0,125)
       IF (DEBUG) write(0,113)JDEF
       READ (51,115)(DEFECT(J),J=1,JDEF)
       IF (DEBUG) write(0,126)
       IF (DEBUG) write(0,115)(DEFECT(J),J=1,JDEF)
C-----------------------------------------------------------------------
C  FETCH CHARGE EXCHANGE DATA FROM ARCHIVED DATA SET
C-----------------------------------------------------------------------
       NBEAM=1
       BMENA(1)=BMENER
       BMFRA(1)=1.0D0
       CALL BNQCTB(Z0,Z,NMIN,NMAX,IMAX,NREP,NBEAM,BMENA,BMFRA,
     & CXMEMB,IBLOCK,QTHREP,ALPHA)
C
       DO 76 J=1,JMAX
       IF(WIJ(J)*FIJ(J)-1.0D-50)71,72,72
   71  WEIJ(J)=0.0
       GO TO 76
   72  X=Z+1.0
       X=X*X*EPSIL(J)*157890.0/TS
       IF(X-CUT)73,71,71
   73  IF(X-0.001)74,75,75
   74  WEIJ(J)=WIJ(J)/(X*(1.0+X*(0.5+0.16666667*X)))
       GO TO 76
   75  WEIJ(J)=WIJ(J)/(DEXP(X)-1.0)
   76  CONTINUE
       DENS=DENSA(JDENS)
       DENSP=DENPA(JDENS)
C-----------------------------------------------------------------------
C  COMPUTE CHARGE OF EFFECTIVE IMPURITY, ITS DENSITY AND ATOMIC MASS
C
C  EXTENSION FOR MULTIPLE SIMULTANEOUS NAMED IMPURITIES
C
C  COMPUTE DISPLACED SPEED FOR ION IMPACT MAXWELLIAN AVERAGING
C-----------------------------------------------------------------------
C
C----------------------------------------------------------------------
C
C       BEGINING OF CODE THAT WAS INSERTED BY HARVEY ANDERSON 16/12/96
C
C----------------------------------------------------------------------
C
        IF ( NOSCAN .EQ. 1 ) THEN
            IF ( DENS .LE. DENSP  ) THEN
               NIMP       =   0
               ZIMPA(1)   =   0.0D0
               DNIMPA(1)  =   0.0D0
               AMIMPA(1)  =   1.0D0
               FRIMPA(1)  =   0.0
               DENIMP     =   DNIMPA(1)
               ZEFF       =   1.0
             ELSE IF ( NIMP .EQ. 1 ) THEN
C
C----------------------------------------------------------------------------
C
C       IF THERE IS ONLY ONE IMPURITY AND IT IS HYDROGEN ADJUST THE PROTON
C       DENSITY AND TREAT PROBLEM AS IF THERE IS NO IMPURITY.SET THE FRACTION
C       TO ONE IN CASE USER HAS NOT.
C
C----------------------------------------------------------------------------
C
                    IF ( ZIMPA(1) .EQ. 1.0 ) THEN
                         FRIMPA(1)=1.0
                         DENIMP=(DENS-DENSP)/( ZIMPA(1)*FRIMPA(1) )
                         DENSP=DENSP+DENIMP
                         DNIMPA(1)=0.0D0
                         FRIMPA(1)=0.0
                         DENIMP=0.0D0
                         ZEFF=1.0
                         ZIMPA(1)=0.0
                         NIMP=0
                    ELSE 
C
C----------------------------------------------------------------------------
C
C       IF THE SINGLE IMPURITY IS NOT HYDROGEN THEN SET THE FRACTION TO
C       ONE INCASE THE USER HAS NOT. TREAT PROBLEM AS IF IT IS A SINGLE
C       IMPURITY.
C
C----------------------------------------------------------------------------
C
                          FRIMPA(1)=1.0
                          DENIMP=(DENS-DENSP)/(ZIMPA(1)*FRIMPA(1))                      
                          ZEFF=(DENSP+DENIMP*( ZIMPA(1)**2*FRIMPA(1)))
     &                         / DENS
                          ZIMPA(1)=(ZEFF*DENS-DENSP)/(DENS-DENSP)
                          DENIMP=(DENS-DENSP)/ZIMPA(1)
                          DNIMPA(1)=DENIMP*FRIMPA(1)
                    ENDIF
C
C----------------------------------------------------------------------------
C
C       IF THERE ARE A NUMBER OF IMPURITIES THEN FIRSTLY CHECK THAT THE
C       TOTAL FRACTION ARE EQUAL TO ONE. IF THIS IS NOT THE CASE RENORMALISE
C       THE FRACTIONS IF REQUIRED.
C
C----------------------------------------------------------------------------
C
             ELSE IF ( NIMP .GT. 1 ) THEN
                        TFRMPA=0.0 
                        DO IMP=1,NIMP
                           TFRMPA=TFRMPA+FRIMPA(IMP)
                        END DO
                        IF ( TFRMPA .NE. 1.0 ) THEN
                                FSUM1=0.0
                                DO IMP=1,NIMP
                                        FSUM1=FSUM1+FRIMPA(IMP)
                                END DO
                                DO IMP=1,NIMP
                                        FRIMPA(IMP)=FRIMPA(IMP)/FSUM1
                                END DO
                        ENDIF
C
C----------------------------------------------------------------------------
C
C       CHECK TO SEE IF THERE IS HYDROGEN IN THE IMPURITY LIST. IF THERE
C       IS MARK THE COUNTER J WITH A NON ZERO VALUE WHICH WOULD CORRESPOND
C       TO THE HYDROGEN LOCATION IN THE LIST.
C
C----------------------------------------------------------------------------
C
                        J=0
                        DO IMP=1,NIMP
                          IF ( ZIMPA(IMP) .EQ. 1.0 ) J=IMP
                        END DO
C
C----------------------------------------------------------------------------
C
C       IF THERE IS NO HYDROGEN IN THE LIST THEN CONSIDER THE PROBLEM
C       TO BE A BASIC MULTIPLE IMPURITY CASE
C
C----------------------------------------------------------------------------
C
                       IF ( J .EQ. 0 ) THEN
                        ZSUM1=0.0
                        ZSUM2=0.0
                        DO IMP=1,NIMP
                           ZSUM1=ZSUM1+ZIMPA(IMP)*FRIMPA(IMP)
                           ZSUM2=ZSUM2+( ZIMPA(IMP)**2*FRIMPA(IMP) )
                        END DO
                        DENIMP=( DENS-DENSP ) / ZSUM1
                        ZEFF=( DENSP + (DENIMP*ZSUM2) ) / DENS
                        DO IMP=1,NIMP
                                DNIMPA(IMP)=DENIMP*FRIMPA(IMP)
                        END DO
C
C----------------------------------------------------------------------------
C
C       IF THERE IS HYDROGEN IN THE IMPURITY LIST EVALUATE THE CONCENTRATION
C       DUE TO THE HYDROGEN AND ADD IT TO THE INITIAL CONCENTRATION OF PROTONS.
C       THEN RENORMALISE THE IMPURITY FRACTIONS AND CONSIDER THE PROBLEM TO
C       BE THAT OF THE BASIC MULTIPLE IMPURITY CASE OR A SINGLE IMPURITY
C       CASE.
C
C----------------------------------------------------------------------------
C
                        ELSE
                           IF ( NIMP .EQ. 2 ) THEN
                              ZSUM1=0.0
                              DO IMP=1,NIMP
                                ZSUM1=ZSUM1+ZIMPA(IMP)*FRIMPA(IMP)
                              END DO
                              DENIMP=(DENS-DENSP)/ZSUM1
                              DENSP=DENSP+DENIMP*FRIMPA(J)
                              DO IMP=1,NIMP
                                 IF ( IMP .NE. J ) THEN
                                      ZIMPA(1)=ZIMPA(IMP)
                                      AMIMPA(1)=AMIMPA(IMP)
                                 ENDIF
                              END DO
                              FRIMPA(1)=1.0
                              DENIMP=(DENS-DENSP)/(ZIMPA(1)*FRIMPA(1))
                              ZEFF=(DENSP+DENIMP*
     &                              (ZIMPA(1)**2*FRIMPA(1)))/DENS
                              ZIMPA(1)=(ZEFF*DENS-DENSP)/(DENS-DENSP)
                              DENIMP=(DENS-DENSP)/ZIMPA(1)
                              DNIMPA(1)=DENIMP
                              NIMP=1
                           ELSE
                             ZSUM1=0.0
                             DO IMP=1,NIMP
                                ZSUM1=ZSUM1+ZIMPA(IMP)*FRIMPA(IMP)
                             END DO
                             DENIMP=(DENS-DENSP)/ZSUM1
                             DENSP=DENSP+DENIMP*FRIMPA(J)    
                             I=1
                             DO IMP=1,NIMP
                              IF ( IMP.NE.J ) THEN
                                     FRIMPA(I)=FRIMPA(IMP)
                                     ZIMPA(I)=ZIMPA(IMP)
                                     AMIMPA(I)=AMIMPA(IMP)
                                     I=I+1
                              ENDIF
                             END DO
                             NIMP=NIMP-1
                             ZSUM1=0.0
                             ZSUM2=0.0
                             FSUM1=0.0
                             DO IMP=1,NIMP
                               FSUM1=FSUM1+FRIMPA(IMP)
                             END DO
                             DO IMP=1,NIMP
                               FRIMPA(IMP)=FRIMPA(IMP)/FSUM1
                               ZSUM1=ZSUM1+ZIMPA(IMP)*FRIMPA(IMP)
                               ZSUM2=ZSUM2+(ZIMPA(IMP)**2*FRIMPA(IMP))
                             END DO
                              DENIMP=(DENS-DENSP)/ZSUM1
                              ZEFF=( DENSP + (DENIMP*ZSUM2) ) / DENS
                             DO IMP=1,NIMP
                                DNIMPA(IMP)=DENIMP*FRIMPA(IMP)
                             END DO
                             
                        ENDIF
                    ENDIF
           ENDIF
        ENDIF
C
C----------------------------------------------------------------------
C
C       END OF CODE THAT WAS INSERTED BY HARVEY ANDERSON 16/12/96
C
C----------------------------------------------------------------------
C
       IF(IVDISP.EQ.1)THEN
           VDISP=1.38317498E6*DSQRT(BMENER)
       ELSE
           VDISP=0.0D0
       ENDIF
C
       PY=0.0
       PYP=0.0
       TE=TEA(JTE)
       TP=TPA(JTE)
C-----------------------------------------------------------------------
C  ZERO LOOKUP ARRAYS AND POINTER ARRAYS ASSOCIATED WITH LOW LEVEL DATA
C-----------------------------------------------------------------------
       ITYP1=1
       ITYP2=1
       ITYP3=1
       ITYP4=1
       ITYP5=1
       ITYP6=1
       PSTBE=1.0
       PSTBP=1.0
       PSTBZ=1.0
       LPSTBE=0
       LPSTBP=0
       LPSTBZ=0
       DO 162 IDL=1,NDLOW
        STBE(IDL)=0
        STBP(IDL)=0
        STBZ(IDL)=0
        LSTBE(IDL)=0
        LSTBP(IDL)=0
        LSTBZ(IDL)=0
        PXTBE(IDL)=1.0
        PXTBP(IDL)=1.0
        PXTBZ(IDL)=1.0
        LPXTBE(IDL)=0
        LPXTBP(IDL)=0
        LPXTBZ(IDL)=0
        DO 160 IDL1=1,NDLOW
         XTBE(IDL,IDL1)=0
         XTBP(IDL,IDL1)=0
         XTBZ(IDL,IDL1)=0
         LXTBE(IDL,IDL1)=0
         LXTBP(IDL,IDL1)=0
         LXTBZ(IDL,IDL1)=0
  160   CONTINUE
  162  CONTINUE
C-----------------------------------------------------------------------
C  ACQUIRE SPECIAL LOW LEVEL DATA FOR THE PARTICULAR ION
C
C      *  ION COLLISIONS AT VDISP CORRESPONDING TO THE BEAM SPEED IS
C         FORCED INDEPENDENT OF THE IVDISP PARAMETER
C-----------------------------------------------------------------------
       IZ=Z
       TEV=TE/11605.4
       TPEV=TP/11605.4
       IF(ILOW.GT.0)THEN
           IF(IZ0.EQ.1.AND.IZ.EQ.1)THEN
               CALL NSUPH1(TEV,BMENER,TPEV,NIMP ,ZIMPA ,FRIMPA,AMIMPA,
     &                      ITYP1 ,ITYP2 ,ITYP3 ,ITYP4 ,ITYP5 ,ITYP6 ,
     &                      XTBE  , XTBP , XTBZ , STBE , STBP , STBZ ,
     &                      LXTBE ,LXTBP ,LXTBZ ,LSTBE ,LSTBP ,LSTBZ ,
     &                      PXTBE ,PXTBP ,PXTBZ ,PSTBE ,PSTBP ,PSTBZ ,
     &                      LPXTBE,LPXTBP,LPXTBZ,LPSTBE,LPSTBP,LPSTBZ,
     &                      DSLPATH, lnoion, lnocx)
           ELSE
               CONTINUE
           ENDIF
       ENDIF
C
C      WRITE(7,148)
C 148  FORMAT(1H ,4X,'I',4X,'N',2X,'DEXPTE(N)',7X,'ENC',10X,'X3',
C    *11X,'AI')
       IF (DEBUG) write(0,131)
       IF (DEBUG) write(0,132)DENS,TE
       IF (DEBUG) write(0,143)
       IF (DEBUG) write(0,132)DENSP,TP
       X=157890.0*Z*Z
       ATE=X/TE
       ATP=X/TP
       ATS=X/TS
       RHO=1.27787E-17/Z**7
       RHOP=RHO*DENSP*DSQRT(X/TP)
       RHOIMP=RHO*DENIMP*DSQRT(X/TP)
       RHO=RHO*DENS*DSQRT(ATE)
       PY0=-DLOG(ATE)-0.41
       DO 79 J=1,JMAX
       IF(FIJ(J)-0.000001)77,77,78
   77  EEIJ(J)=0.0
       GO TO 79
   78  X=Z+1.0
       X=X*X*EPSIL(J)*157890.0/TE
       EEIJ(J)=DEXP(-X)
   79  CONTINUE
       X=NMAX
       ENMAX2=1.0/((X+0.5)*(X+0.5))
C      ENMAX2=0.00D+00
       EYE=DEXP(ATE*ENMAX2)
       IF (DEBUG) write(0,127)
       IF (DEBUG) write(0,104)ATE,ATS,RHO
       NMAX1=NMAX+NIP
       DO 64 N=1,NMIN
       EN=N
       EN23(N)=EN**(-0.6666667)
   64  CONTINUE
       DO 202 N=NMIN,NMAX1
       EN=N
       DEF=0.0
       J=N+1-NMIN
       IF(J-JDEF)191,191,192
  191  DEF=DEFECT(J)
  192  A1=1.0/(EN-DEF)
       EN2(N)=A1*A1
       EN3(N)=EN2(N)*A1
       EN23(N)=EN**(-0.6666667)
       FLAG(N)=FINTER(EN)
       DEXPTE(N)=DEXP(ATE*EN2(N))
       X=ATS*EN2(N)
       IF(X-CUT)201,200,200
  200  DEXPTS(N)=0.0
       GO TO 202
  201  DEXPTS(N)=DEXP(X)
  202  CONTINUE
       DO 1 N=1,NMAX
       EN=N
       OGARL(N)=DLOG(EN)
       N1=NMAX+N
       EN1=N1
    1  OGARL(N1)=DLOG(EN1)
       NREP(IMAX+1)=NMAX+NMAX
       I=3
       N1=NMIN+2
       DO 4 N=N1,NMAX
       IF(N+N-NREP(I)-NREP(I+1))3,3,2
    2  I=I+1
    3  INTER(N)=I
    4  CONTINUE
       INTER(NMIN)=2
       INTER(NMIN+1)=2
       DO 8 N=NMIN,NMAX
       X=FLAG(N)
       I=INTER(N)
       I1=NREP(I-1)
       I2=NREP(I)
       X1=FLAG(I1)
       X2=FLAG(I2)
       IF(IMAX-I)6,6,5
    5  I3=NREP(I+1)
       X3=FLAG(I3)
       GO TO 7
    6  X3=0.0
    7  AGRL(N,1)=(X-X2)*(X-X3)/((X1-X2)*(X1-X3))
       AGRL(N,2)=(X-X1)*(X-X3)/((X2-X1)*(X2-X3))
       AGRL(N,3)=(X-X1)*(X-X2)/((X3-X1)*(X3-X2))
    8  CONTINUE
C-----------------------------------------------------------------------
C  EVALUATE AND STORE IMPACT PARAMETER CROSS-SECTIONS FOR ELECTRONS AND
C  PROTONS
C-----------------------------------------------------------------------
       ZCOL=Z-1.0
       IF(NIP.GT.0)THEN
           DO 500 N=NMIN,NMAX
            EN=N
            DO 501 I=1,NIP
             N11=N+I
             EN11=N11
             D=EN2(N)-EN2(N11)
             D1=1.0/(D*D)
             PHI=1.56*EN3(N)*EN2(N)*EN3(N11)*D1*D1/(Z*Z)
             WI=2.0*EN*EN
             WJ=2.0*EN11*EN11
             R=0.25*(5.0*EN*EN+1.0)/Z
             IF(DENS.GT.0.0D0)THEN
                 CALL PYIP(D,1.0D0,ZCOL,PHI,CPY,WI,WJ,R,TE,INTD,PY)
             ELSE
                 PY=0.0D0
             ENDIF
             PYMAT(N,I,1)=PY
             IF(DENSP.GT.0.0D0)THEN
                 CALL PYIP(D,EM,ZCOL,PHI,CPY,WI,WJ,R,TP,INTD,PYP)
             ELSE
                 PYP=0.0D0
             ENDIF
             PYMAT(N,I,2)=PYP
  501       CONTINUE
  500      CONTINUE
       ENDIF
C
       IF(WBREP(1))60,60,62
   60  DO 61 N=NMIN,NMAX
       WB(N)=W
   61  CONTINUE
       GO TO 63
   62  DO 49 I=1,IMAX
       WBLOG(I)=DLOG(WBREP(I))
   49  CONTINUE
       N1=NREP(IMAX-1)-1
       DO 50 N=NMIN,N1
       I=INTER(N)
       W2=WBLOG(I-1)*AGRL(N,1)+WBLOG(I)*AGRL(N,2)+WBLOG(I+1)
     1*AGRL(N,3)
       WB(N)=DEXP(W2)
   50  CONTINUE
       W2=WBREP(IMAX)
       N1=N1+1
       DO 48 N=N1,NMAX
       WB(N)=W2
   48  CONTINUE
   63  CONTINUE
C      IF (DEBUG) write(0,134)(WB(N),N=NMIN,NMAX)
       IF (DEBUG) write(0,128)
       DO 30 I=1,IMAX
       RHS(I)=0.0
       N=NREP(I)
       A(N)=0.0
       EN=N
       DVEC(I)=EN*EN*DEXPTE(N)
       N1=N+1
       DO 16 N11=N1,NMAX
       W2=W
       IF(N.LE.NMIN)THEN
           W2=WB(N11)
       ENDIF
       D=EN2(N)-EN2(N11)
       Y=ATE*D
       YS=ATS*D
       D=1.0/D
       EN11=N11
       X2=EN2(N11)
       N2=N11-N
       X=EN23(N)*EN23(N2)/EN23(N11-1)
       G=GBB(EN11,EN,X2,X)
       A1=EN3(N)*EN3(N11)*D*G
C-----------------------------------------------------------------------
C  IMPACT EXCITATION DATA
C-----------------------------------------------------------------------
       EI=EN2(N)
       WI=2.0D0*EN*EN
       EJ=EN2(N11)
       WJ=2.0D0*EN11*EN11
       F=1.96028D0*G*D*D*D*EN3(N)*EN2(N)*EN3(N11)
       PHI=F*D/(Z*Z)
C-----------------------------------------------------------------------
C      BASIC ELECTRON DATA  - VAN REGEMORTER OR PERCIVAL-RICHARDS
C-----------------------------------------------------------------------
       IF(IPRS.LE.0)THEN
           CALL PYVR(Y,Z,PY)
C***********
C  DIAGNOSTIC PRINT
       IF(DEBUG.AND.N.LE.3.AND.N11.LE.5)
     &                        write(0,*)'AT PYVR  ', '  N=',N,'  N11=',
     &                                  N11,'  PY=',PY
C***********
       ELSE
           CALL PYPR(EI,EJ,N,N11,1.0D0,Z,PHI,WI,WJ,TE,INTD,PY,RDEXC)
C***********
C  DIAGNOSTIC PRINT
       IF(DEBUG.AND.N.LE.3.AND.N11.LE.5)
     &                        write(0,*)'AT PYPR  ', '  N=',N,'  N11=',
     &                                  N11,'  PY=',PY
C***********
       ENDIF
C-----------------------------------------------------------------------
C                         SUBSTITUTE IMPACT PARAMETER IF REQUESTED
C-----------------------------------------------------------------------
       NTEST=N11-N
       IF(NTEST.LE.NIP)THEN
           PY=PYMAT(N,NTEST,1)
C***********
C  DIAGNOSTIC PRINT
       IF(DEBUG.AND.N.LE.3.AND.N11.LE.5)
     &                        write(0,*)'AT PYMAT ', '  N=',N,'  N11=',
     &                                  N11,'  PY=',PY
C***********
       ENDIF
       PYD=PY
       PYU=PY
C-----------------------------------------------------------------------
C                         SUBSTITUTE SPECIAL LOW LEVEL DATA IF AVAILABLE
C                         NB. DE-EXCITATION AND EXCITATION RATE COEFFTS.
C                             SUPPLIED SEPARATELY
C-----------------------------------------------------------------------
       IF(ILOW.GT.0.AND.N.LE.NDLOW.AND.N11.LE.NDLOW)THEN
           IF(LXTBE(N,N11).EQ.1)THEN
               PYD=XTBE(N11,N)/
     &             (3.15D-7*DSQRT(1.57890D5/TE)*PHI*
     &             (EN/EN11)**2)
               PYU=XTBE(N,N11)/
     &            (3.15D-7*DSQRT(1.57890D5/TE)*PHI*
     &            DEXPTE(N11)/DEXPTE(N))
C***********
C  DIAGNOSTIC PRINT
       IF(DEBUG.AND.N.LE.3.AND.N11.LE.5)
     &                        write(0,*)'AT SUPP E', '  N=',N,'  N11=',
     &                                  N11,'  PYD=',PYD,'  PYU=',PYU
C***********
            ENDIF
       ENDIF
C-----------------------------------------------------------------------
C      BASIC ION  DATA      - VAINSHTEIN OR LODGE-PERCIVAL-RICHARDS
C                             NB. EXCITATION AND DEEXCITATION COEFFTS.
C                                 MUST BE SUPPLIED SEPARATELY
C-----------------------------------------------------------------------
       PYPD=0.0D0
       PYPU=0.0D0
       PYIMPD=0.0D0
       PYIMPU=0.0D0
       IF(IONIP.EQ.1)THEN
           IF(ILPRS.LE.0)THEN
               IF(DENSP.GT.0.0D0.AND.NTEST.LE.NIONIP)THEN
                   PYPD=RQVNEW(Z,N11,N,PHI,1.0D0,2.0D0,TP,VDISP)/
     &                  (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                  (EN/EN11)**2)
                   PYPU=RQVNEW(Z,N,N11,PHI,1.0D0,2.0D0,TP,VDISP)/
     &                  (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                  DEXPTE(N11)/DEXPTE(N))
C***********
C  DIAGNOSTIC PRINT
       IF(DEBUG.AND.N.LE.3.AND.N11.LE.5)
     &                        write(0,*)'AT RQVNEW', '  N=',N,'  N11=',
     &                               N11,'  PYPD=',PYPD,'  PYPU=',PYPU
C***********
               ELSE
                   PYPD=0.0D0
                   PYPU=0.0D0
               ENDIF
               PYIMPD=0.0D0
               PYIMPU=0.0D0
               IF(DENIMP.GT.0.0D0.AND.NTEST.LE.NIONIP)THEN
                   DO 41 IMP=1,NIMP
                   IF(FRIMPA(IMP).EQ.0.0D0)GO TO 41
                   PYIMPD=PYIMPD+FRIMPA(IMP)*RQVNEW(Z,N11,N,PHI,
     &                    ZIMPA(IMP),AMIMPA(IMP),TP,VDISP)/
     &                    (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                    (EN/EN11)**2)
                   PYIMPU=PYIMPU+FRIMPA(IMP)*RQVNEW(Z,N,N11,PHI,
     &                    ZIMPA(IMP),AMIMPA(IMP),TP,VDISP)/
     &                     (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                     DEXPTE(N11)/DEXPTE(N))
C***********
C  DIAGNOSTIC PRINT
       IF(DEBUG.AND.N.LE.3.AND.N11.LE.5)
     &                        write(0,*)'AT RQVNEW', '  N=',N,'  N11=',
     &                        N11,'  PYIMPD=',PYIMPD,'  PYIMPU=',PYIMPU
C***********
   41              CONTINUE
               ELSE
                   PYIMPD=0.0D0
                   PYIMPU=0.0D0
               ENDIF
           ELSE
               IF(DENSP.GT.0.0D0.AND.NTEST.LE.NIONIP)THEN
                   PYPD=RQLNEW(Z,N11,N,PHI,1.0D0,2.0D0,TP,VDISP)
C***********
C  DIAGNOSTIC PRINT
       IF(DEBUG.AND.N.LE.3.AND.N11.LE.5)
     &                        write(0,*)'AT RQLNEW ', '  N11=',N11,
     &                        '  N=',N,'  RQLNEW=',PYPD,'  Z=',Z,
     &                        '  PHI=',PHI,
     &                        '  TP=',TP,'  VDISP=',VDISP
C***********
                   PYPD=PYPD/
     &                  (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                  (EN/EN11)**2)
                   PYPU=RQLNEW(Z,N,N11,PHI,1.0D0,2.0D0,TP,VDISP)
C***********
C  DIAGNOSTIC PRINT
       IF(DEBUG.AND.N.LE.3.AND.N11.LE.5)
     &                        write(0,*)'AT RQLNEW ', '  N=',N,'  N11=',
     &                        N11,'  RQLNEW=',PYPU,'  Z=',Z,'  PHI=',
     &                        PHI,'  TP=',TP,'  VDISP=',VDISP
C***********
                   PYPU=PYPU/
     &                  (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                  DEXPTE(N11)/DEXPTE(N))
C***********
C  DIAGNOSTIC PRINT
       IF(DEBUG.AND.N.LE.3.AND.N11.LE.5)
     &                        write(0,*)'AT RQLNEW ', '  N=',N,'  N11=',
     &                        N11,'  PYPD=',PYPD,'  PYPU=',PYPU
C***********
               ELSE
                   PYPD=0.0D0
                   PYPU=0.0D0
               ENDIF
               IF(DENIMP.GT.0.0D0.AND.NTEST.LE.NIONIP)THEN
                   PYIMPD=0.0
                   PYIMPU=0.0
                   DO 42 IMP=1,NIMP
                    IF(FRIMPA(IMP).EQ.0.0D0)GO TO 42
                    PYDVAL=FRIMPA(IMP)*RQLNEW(Z,N11,N,PHI,
     &                     ZIMPA(IMP),AMIMPA(IMP),TP,VDISP)
C***********
C  DIAGNOSTIC PRINT
       IF(DEBUG.AND.N.LE.3.AND.N11.LE.5)
     &                        write(0,*)'AT RQLNEW ', '  N11=',N11,
     &                        '  N=',N,'  RQLNEW=',PYIMPD,'  Z=',Z,
     &                        '  PHI=',PHI,'  ZIMP=',ZIMPA(IMP),
     &                        '  AMSIMP=',AMIMPA(IMP),
     &                        '  TP=',TP,'  VDISP=',VDISP
C***********
                    PYIMPD=PYIMPD+PYDVAL/
     &                     (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                     (EN/EN11)**2)
                    PYUVAL=FRIMPA(IMP)*RQLNEW(Z,N,N11,PHI,
     &                     ZIMPA(IMP),AMIMPA(IMP),TP,VDISP)
C***********
C  DIAGNOSTIC PRINT
       IF(DEBUG.AND.N.LE.3.AND.N11.LE.5)
     &                        write(0,*)'AT RQLNEW ', '  N=',N,
     &                        '  N11=',N11,'  RQLNEW=',PYIMPU,'  Z=',Z,
     &                        '  PHI=',PHI,'  ZIMP=',ZIMPA(IMP),
     &                        '  AMSIMP=',AMIMPA(IMP),
     &                        '  TP=',TP,'  VDISP=',VDISP
C***********
                    PYIMPU=PYIMPU+PYUVAL/
     &                     (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                     DEXPTE(N11)/DEXPTE(N))
C***********
C  DIAGNOSTIC PRINT
       IF(DEBUG.AND.N.LE.3.AND.N11.LE.5)
     &                        write(0,*)'AT RQLNEW ', '  N=',N,'  N11=',
     &                        N11,'  PYIMPD=',PYIMPD,'  PYIMPU=',PYIMPU
C***********
   42               CONTINUE
               ELSE
                   PYIMPD=0.0D0
                   PYIMPU=0.0D0
               ENDIF
           ENDIF
C-----------------------------------------------------------------------
C                         SUBSTITUTE SPECIAL LOW LEVEL DATA IF AVAILABLE
C                         NB. EXCITATION AND DEEXCITATION COEFFTS. ARE
C                             SEPARATELY SUPPLIED
C-----------------------------------------------------------------------
           IF(ILOW.GT.0.AND.N.LE.NDLOW.AND.N11.LE.NDLOW.AND.
     &                                     IVDISP.EQ.1)THEN
               IF(DENSP.GT.0.0D0)THEN
                   IF(LXTBP(N,N11).EQ.1)THEN
                       PYPD=XTBP(N11,N)/
     &                      (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                      (EN/EN11)**2)
                       PYPU=XTBP(N,N11)/
     &                      (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                      DEXPTE(N11)/DEXPTE(N))
C***********
C  DIAGNOSTIC PRINT
       IF(DEBUG.AND.N.LE.3.AND.N11.LE.5)
     &                        write(0,*)'AT SUPP P', '  N=',N,'  N11=',
     &                        N11,'  PYPD=',PYPD,'  PYPU=',PYPU
C***********
                   ENDIF
               ENDIF
               IF(DENIMP.GT.0.0D0)THEN
                   IF(LXTBZ(N,N11).EQ.1)THEN
                       PYIMPD=XTBZ(N11,N)/
     &                        (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                        (EN/EN11)**2)
                       PYIMPU=XTBZ(N,N11)/
     &                        (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                        DEXPTE(N11)/DEXPTE(N))
C***********
C  DIAGNOSTIC PRINT
       IF(DEBUG.AND.N.LE.3.AND.N11.LE.5)
     &                        write(0,*)'AT SUPP I', '  N=',N,'  N11=',
     &                        N11,'  PYIMPD=',PYIMPD,'  PYIMPU=',PYIMPU
C***********
                   ENDIF
                ENDIF
           ENDIF
       ENDIF
C***********
C  DIAGNOSTIC PRINT
       IF(DEBUG.AND.N.LE.3.AND.N11.LE.5)
     &                        write(0,*)'AT SUM   ', '  PYD=',PYD,
     &          '  PYU=',PYU,'  PYPD=',PYPD,'  PYPU=',PYPU,'  PYIMPD=',
     &          PYIMPD,'  PYIMPU=',PYIMPU,'  RHO=',RHO,'  RHOP=',RHOP,
     &          '  RHOIMP=',RHOIMP,'  ZEFF1=',1
C***********
       PYD=PYD*RHO+PYPD*RHOP+PYIMPD*RHOIMP
       PYU=PYU*RHO+PYPU*RHOP+PYIMPU*RHOIMP
C
       IF(YS-0.001)203,203,204
  203  A2=W2/(YS+0.5*YS*YS)
       GO TO 209
  204  IF(DEXPTS(N))205,205,208
  205  IF(YS-CUT)207,206,206
  206  A2=0.0
       GO TO 209
  207  A2=W2/(DEXP(YS)-1.0)
       GO TO 209
  208  A2=W2/(DEXPTS(N)/DEXPTS(N11)-1.0)
  209  A3D=3.06998*D*D*D*PYD
       A3U=3.06998*D*D*D*PYU
       A(N11)=-A1*(1.0+A2+A3D)*DEXPTE(N11)
       A(N)=A(N)+A1*(A2*DEXPTE(N)+A3U*DEXPTE(N11))
       RHS(I)=RHS(I)+A1*(DEXPTE(N11)  +A2*(DEXPTE(N11)-DEXPTE(N)))
   16  CONTINUE
       YE=ATE*(EN2(N)-ENMAX2)
       YS=ATS*(EN2(N)-ENMAX2)
C-----------------------------------------------------------------------
C  IMPACT DIRECT AND CHARGE EXCHANGE IONISATION DATA
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C      BASIC ELECTRON DATA  - ECIP
C-----------------------------------------------------------------------
       ENC=1.0/DSQRT(EN2(N)-ENMAX2)
       CALL COLINT(YE,Z,ENC,AI)
       AI=AI/CION
       W2=W
       IF(N.LE.NMIN)THEN
           AI=CION*AI
           W2=W1
       ENDIF
       SECIP=PSTBE*0.43171*RHO*AI*ENC*ENC*1.57456D10*Z**4/
     &       (DENS*DEXPTE(N))
C-----------------------------------------------------------------------
C                        SUBSTITUTE SPECIAL ELECTRON DATA
C-----------------------------------------------------------------------
       IF(ILOW.GT.0.AND.N.LE.NDLOW)THEN
           SION=SECIP
           IF(LSTBE(N).EQ.1)THEN
               SION=STBE(N)
               IF(LPSTBE.EQ.N)PSTBE=SION/SECIP
C***********
C  DIAGNOSTIC PRINT
       IF(DEBUG.AND.N.LE.5)
     &           write(0,*)'AT SUPP IONIS E', '  N=',N,'  SION=',SION,
     &                           '  SECIP=',SECIP,'  PSTBE=',PSTBE
C***********
           ENDIF
C-----------------------------------------------------------------------
C      BASIC ION  DATA      - PERCIVAL-RICHARDS DIRECT ONLY
C-----------------------------------------------------------------------
           IF(IONIP.GT.0)THEN
               IF(DENSP.GT.0.0D0)THEN
                   SIONP=PSTBP*RQINEW(Z,N,1.0D0,1.0D0,TP,VDISP)
C***********
C  DIAGNOSTIC PRINT
       IF(DEBUG.AND.N.LE.5)
     &           write(0,*)'AT PR IONIS P', '  N=',N,'  SIONP=',
     &                      SIONP,'  SECIP=',SECIP,'  PSTBP=',PSTBP
C***********
               ELSE
                   SIONP=0.0D0
               ENDIF
               IF(DENIMP.GT.0.0D0)THEN
                   SIONVAL=0.0
                   DO 43 IMP=1,NIMP
                     IF(FRIMPA(IMP).EQ.0.0D0)GO TO 43
                     SIONVAL=SIONVAL +FRIMPA(IMP)*RQINEW(Z,N,
     &                       ZIMPA(IMP),AMIMPA(IMP),TP,VDISP)
   43              CONTINUE
                   SIONZ=PSTBZ*SIONVAL
C***********
C  DIAGNOSTIC PRINT
       IF(DEBUG.AND.N.LE.5)
     &           write(0,*)'AT PR IONIS Z', '  N=',N,'  SIONZ=',
     &                      SIONZ,'  SECIP=',SECIP,'  PSTBZ=',PSTBZ
C***********
               ELSE
                   SIONZ=0.0D0
               ENDIF
           ENDIF
C-----------------------------------------------------------------------
C                        SUBSTITUTE SPECIAL ION DATA
C-----------------------------------------------------------------------
           IF(IONIP.GT.0.AND.IVDISP.GT.0)THEN
               IF(DENSP.GT.0.0D0)THEN
                   IF(LSTBP(N).EQ.1)THEN
                       IF(LPSTBP.EQ.N)PSTBP=STBP(N)/SIONP
                       SIONP=STBP(N)
C***********
C  DIAGNOSTIC PRINT
       IF(DEBUG.AND.N.LE.5)
     &           write(0,*)'AT SUPP IONIS P', '  N=',N,'  SIONP=',
     &                      SIONP,'  SECIP=',SECIP,'  PSTBP=',PSTBP
C***********
                   ENDIF
               ENDIF
               IF(DENIMP.GT.0.0D0)THEN
                   IF(LSTBZ(N).EQ.1)THEN
                       IF(LPSTBZ.EQ.N)PSTBZ=STBZ(N)/SIONZ
                       SIONZ=STBZ(N)
C***********
C  DIAGNOSTIC PRINT
       IF(DEBUG.AND.N.LE.5)
     &           write(0,*)'AT SUPP IONIS I', '  N=',N,'  SIONZ=',
     &                      SIONZ,'  SECIP=',SECIP,'  PSTBZ=',PSTBZ
C***********
                   ENDIF
               ENDIF
           ENDIF
           AI=AI*(SION+(DENSP*SIONP+DENIMP*SIONZ)/DENS)/SECIP
       ENDIF
C
       IF(YS-CUT)223,223,220
  220  X=YS-ATE*EN2(N)
       IF(X-CUT)222,222,221
  221  X1=0.0
       X2=0.0
       GO TO 224
  222  X1=DEXP(-X)/(YS+1.0)
       X2=X1*DEXP(-YE)
       GO TO 224
  223  CALL PHOTO(PION,PREC,PSTIM,Z,TE,TS,ENC,1,0,1)
       X1=DEXPTE(N)*PION
       X2=DEXPTE(N)*PSTIM
  224  A(N)=A(N)+0.43171*RHO*AI*EN*EN*ENC*ENC+0.5*W2*EN3(N)*X1
       CIONPT(I)=0.43171*RHO*AI*EN*EN*ENC*ENC
       ADS=0.0
       IF(N-NMIN)303,303,302
  302  DO 301 J=1,JMAX
       F=FIJ(J)
       EIJ=EPSIL(J)
       WEIJJ=WEIJ(J)
       IF(F-0.000001)301,301,300
C 300  CALL TDIEL(Z,EIJ,TE,N,AD)
  300  CALL DIEL_310(Z,EIJ,F,WEIJJ,COR,JCOR,N,AD)
       ADS=ADS+AD*(EEIJ(J)-WEIJJ*(1.0-EEIJ(J)))*DEXPTE(N)
       A(N)=A(N)+AD*WEIJJ*DEXPTE(N)
  301  CONTINUE
  303  CONTINUE
       CALL PHOTO(PION,PREC,PSTIM,Z,TE,TS,ENC,0,1,0)
       X3=DEXPTE(N)*PREC
       X4=QTHREP(I)
  494  X4=9.62134D12*DENSH*X4/(DENS*Z*ATE**1.5D0)
       RHS(I)=RHS(I)+0.5*EN3(N)*(W*X2-W2*X1 +X3)+ADS+X4
       DRECPT(I)=ADS
       RRECPT(I)=0.5*EN3(N)*X3
       XRECPT(I)=X4
C      WRITE(7,147)I,N,DEXPTE(N),ENC,X3,AI
C 147  FORMAT(2I5,1P,4E12.4)
       IF(N-NMIN)26,26,17
   17  N2=N-1
       DO 25 N1=NMIN,N2
       W2=W
       IF(N1-NMIN)53,53,54
   53  W2=WB(N)
   54  CONTINUE
       D=EN2(N1)-EN2(N)
       Y=ATE*D
       YS=ATS*D
       D=1.0/D
       EN1=N1
       X2=EN2(N)
       N3=N-N1
       X=EN23(N1)*EN23(N3)/EN23(N-1)
       G=GBB(EN,EN1,X2,X)
       A1=EN3(N)*EN3(N1)*D*G
C-----------------------------------------------------------------------
C  IMPACT EXCITATION DATA
C-----------------------------------------------------------------------
       EI=EN2(N1)
       WI=2.0D0*EN1*EN1
       EJ=EN2(N)
       WJ=2.0D0*EN*EN
       F=1.96028D0*G*D*D*D*EN3(N1)*EN2(N1)*EN3(N)
       PHI=F*D/(Z*Z)
C-----------------------------------------------------------------------
C      BASIC ELECTRON DATA  - VAN REGEMORTER OR PERCIVAL-RICHARDS
C-----------------------------------------------------------------------
       IF(IPRS.LE.0)THEN
           CALL PYVR(Y,Z,PY)
C***********
C  DIAGNOSTIC PRINT
       IF(DEBUG.AND.N1.LE.3.AND.N.LE.5)
     &                       write(0,*)'AT PYVR  ', '  N1=',N1,'  N=',
     &                                  N,'  PY=',PY
C***********
       ELSE
           CALL PYPR(EI,EJ,N1,N,1.0D0,Z,PHI,WI,WJ,TE,INTD,PY,RDEXC)
C***********
C  DIAGNOSTIC PRINT
       IF(DEBUG.AND.N1.LE.3.AND.N.LE.5)
     &                       write(0,*)'AT PYPR  ', '  N1=',N1,'  N=',
     &                                  N,'  PY=',PY
C***********
       ENDIF
C-----------------------------------------------------------------------
C                         SUBSTITUTE IMPACT PARAMETER IF REQUESTED
C-----------------------------------------------------------------------
       NTEST=N-N1
       IF(NTEST.LE.NIP)THEN
           PY=PYMAT(N1,NTEST,1)
C***********
C  DIAGNOSTIC PRINT
       IF(DEBUG.AND.N1.LE.3.AND.N.LE.5)
     &                       write(0,*)'AT PYMAT ', '  N1=',N1,'  N=',
     &                                  N,'  PY=',PY
C***********
       ENDIF
       PYD=PY
       PYU=PY
C-----------------------------------------------------------------------
C                         SUBSTITUTE SPECIAL LOW LEVEL DATA IF AVAILABLE
C                         NB. EXCITATION AND DEEXCITATION COEFFTS.
C                             SEPARATELY SUPPLIED
C-----------------------------------------------------------------------
       IF(ILOW.GT.0.AND.N1.LE.NDLOW.AND.N.LE.NDLOW)THEN
           IF(LXTBE(N1,N).EQ.1)THEN
               PYU=XTBE(N1,N)/
     &             (3.15D-7*DSQRT(1.57890D5/TE)*PHI*
     &             DEXPTE(N)/DEXPTE(N1))
               PYD=XTBE(N,N1)/
     &             (3.15D-7*DSQRT(1.57890D5/TE)*PHI*
     &             (EN1/EN)**2)
C***********
C  DIAGNOSTIC PRINT
       IF(DEBUG.AND.N1.LE.3.AND.N.LE.5)
     &                       write(0,*)'AT SUPP E', '  N1=',N1,'  N=',
     &                       N,'  PYU=',PYU,'  PYD=',PYD
C***********
            ENDIF
       ENDIF
C-----------------------------------------------------------------------
C      BASIC ION  DATA      - VAINSHTEIN OR LODGE-PERCIVAL-RICHARDS
C                             NB. EXCITATION AND DEEXCITATION COEFFTS.
C                                 MUST BE SUPPLIED SEPARATELY
C-----------------------------------------------------------------------
       PYPU=0.0D0
       PYPD=0.0D0
       PYIMPU=0.0D0
       PYIMPD=0.0D0
       IF(IONIP.EQ.1)THEN
           IF(ILPRS.LE.0)THEN
               IF(DENSP.GT.0.0D0.AND.NTEST.LE.NIONIP)THEN
                   PYPU=RQVNEW(Z,N1,N,PHI,1.0D0,2.0D0,TP,VDISP)/
     &                  (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                  DEXPTE(N)/DEXPTE(N1))
                   PYPD=RQVNEW(Z,N,N1,PHI,1.0D0,2.0D0,TP,VDISP)/
     &                  (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                  (EN1/EN)**2)
C***********
C  DIAGNOSTIC PRINT
       IF(DEBUG.AND.N1.LE.3.AND.N.LE.5)
     &                       write(0,*)'AT RQVNEW', '  N1=',N1,'  N=',
     &                       N,'  PYPU=',PYPU,'  PYPD=',PYPD
C***********
               ELSE
                   PYPU=0.0D0
                   PYPD=0.0D0
               ENDIF
               IF(DENIMP.GT.0.0D0.AND.NTEST.LE.NIONIP)THEN
                   PYIMPU=0.0
                   PYIMPD=0.0
                   DO 44 IMP=1,NIMP
                    IF(FRIMPA(IMP).EQ.0.0D0)GO TO 44
                    PYIMPU=PYIMPU+FRIMPA(IMP)*RQVNEW(Z,N1,N,PHI,
     &                     ZIMPA(IMP),AMIMPA(IMP),TP,VDISP)/
     &                     (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                     DEXPTE(N)/DEXPTE(N1))
                    PYIMPD=PYIMPD+FRIMPA(IMP)*RQVNEW(Z,N,N1,PHI,
     &                     ZIMPA(IMP),AMIMPA(IMP),TP,VDISP)/
     &                     (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                     (EN1/EN)**2)
C***********
C  DIAGNOSTIC PRINT
       IF(DEBUG.AND.N1.LE.3.AND.N.LE.5)
     &                       write(0,*)'AT RQVNEW', '  N1=',N1,'  N=',
     &                       N,'  PYIMPU=',PYIMPU,'  PYIMPD=',PYIMPD
C***********
   44              CONTINUE
               ELSE
                   PYIMPU=0.0D0
                   PYIMPD=0.0D0
               ENDIF
           ELSE
               IF(DENSP.GT.0.0D0.AND.NTEST.LE.NIONIP)THEN
                   PYPU=RQLNEW(Z,N1,N,PHI,1.0D0,2.0D0,TP,VDISP)/
     &                  (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                  DEXPTE(N)/DEXPTE(N1))
                   PYPD=RQLNEW(Z,N,N1,PHI,1.0D0,2.0D0,TP,VDISP)/
     &                  (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                  (EN1/EN)**2)
C***********
C  DIAGNOSTIC PRINT
       IF(DEBUG.AND.N1.LE.3.AND.N.LE.5)
     &                       write(0,*)'AT RQLNEW ', '  N1=',N1,'  N=',
     &                       N,'  PYPU=',PYPU,'  PYPD=',PYPD
C***********
               ELSE
                   PYPU=0.0D0
                   PYPD=0.0D0
               ENDIF
               IF(DENIMP.GT.0.0D0.AND.NTEST.LE.NIONIP)THEN
                   PYIMPU=0.0
                   PYIMPD=0.0
                   DO 45 IMP=1,NIMP
                    IF(FRIMPA(IMP).EQ.0.0D0)GO TO 45
                    PYIMPU=PYIMPU+FRIMPA(IMP)*RQLNEW(Z,N1,N,PHI,
     &                     ZIMPA(IMP),AMIMPA(IMP),TP,VDISP)/
     &                     (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                     DEXPTE(N)/DEXPTE(N1))
                    PYIMPD=PYIMPD+FRIMPA(IMP)*RQLNEW(Z,N,N1,PHI,
     &                     ZIMPA(IMP),AMIMPA(IMP),TP,VDISP)/
     &                     (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                     (EN1/EN)**2)
C***********
C  DIAGNOSTIC PRINT
       IF(DEBUG.AND.N1.LE.3.AND.N.LE.5)
     &                       write(0,*)'AT RQLNEW ', '  N1=',N1,'  N=',
     &                       N,'  PYIMPU=',PYIMPU,'  PYIMPD=',PYIMPD
C***********
   45              CONTINUE
               ELSE
                   PYIMPU=0.0D0
                   PYIMPD=0.0D0
               ENDIF
           ENDIF
C-----------------------------------------------------------------------
C                         SUBSTITUTE SPECIAL LOW LEVEL DATA IF AVAILABLE
C                         NB. EXCITATION AND DEEXCITATION COEFFTS.
C                             SUPPLIED SEPARATELY
C-----------------------------------------------------------------------
           IF(ILOW.GT.0.AND.N1.LE.NDLOW.AND.N.LE.NDLOW.AND.
     &                             IVDISP.EQ.1)THEN
               IF(DENSP.GT.0.0D0)THEN
                   IF(LXTBP(N1,N).EQ.1)THEN
                       PYPU=XTBP(N1,N)/
     &                      (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                      DEXPTE(N)/DEXPTE(N1))
                       PYPD=XTBP(N,N1)/
     &                      (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                      (EN1/EN)**2)
C***********
C  DIAGNOSTIC PRINT
       IF(DEBUG.AND.N1.LE.3.AND.N.LE.5)
     &                       write(0,*)'AT SUPP P', '  N1=',N1,'  N=',
     &                       N,'  PYPU=',PYPU,'  PYPD=',PYPD
C***********
                   ENDIF
               ENDIF
               IF(DENIMP.GT.0.0D0)THEN
                   IF(LXTBZ(N1,N).EQ.1)THEN
                       PYIMPU=XTBZ(N1,N)/
     &                        (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                        DEXPTE(N)/DEXPTE(N1))
                       PYIMPD=XTBZ(N,N1)/
     &                        (3.15D-7*DSQRT(1.57890D5/TP)*PHI*
     &                        (EN1/EN)**2)
C***********
C  DIAGNOSTIC PRINT
       IF(DEBUG.AND.N1.LE.3.AND.N.LE.5)
     &                       write(0,*)'AT SUPP I', '  N1=',N1,'  N=',
     &                       N,'  PYIMPU=',PYIMPU,'  PYIMPD=',PYIMPD
C***********
                   ENDIF
               ENDIF
           ENDIF
       ENDIF
C***********
C  DIAGNOSTIC PRINT
       IF(DEBUG.AND.N1.LE.3.AND.N.LE.5)
     &                       write(0,*)'AT SUM   ','  PYU=',PYU,
     &          '  PYD=',PYD,'  PYPU=',PYPU,'  PYPD=',PYPD,'  PYIMPU=',
     &          PYIMPU,'  PYIMPD=',PYIMPD,'  RHO=',RHO,'  RHOP=',RHOP,
     &          'RHOIMP=',RHOIMP,'  1=',1
C***********
       PYU=PYU*RHO+PYPU*RHOP+PYIMPU*RHOIMP
       PYD=PYD*RHO+PYPD*RHOP+PYIMPD*RHOIMP
       IF(YS-0.001)225,225,226
  225  A2=W2/(YS+0.5*YS*YS)
       GO TO 231
  226  IF(DEXPTS(N1))227,227,230
  227  IF(YS-CUT)229,228,228
  228  A2=0.0
       GO TO 231
  229  A2=W2/(DEXP(YS)-1.0)
       GO TO 231
  230  A2=W2/(DEXPTS(N1)/DEXPTS(N)-1.0)
  231  A3U=3.06998*D*D*D*PYU
       A3D=3.06998*D*D*D*PYD
       A(N1)=-A1*(A2*DEXPTE(N1)+A3U*DEXPTE(N))
       A(N)=A(N)+A1*(1.0+A2+A3D)*DEXPTE(N)
       RHS(I)=RHS(I)-A1*(DEXPTE(N)  +A2*(DEXPTE(N)-DEXPTE(N1)))
   25  CONTINUE
   26  DO 27 J=1,IMAX
   27  ARED(I,J)=0.0
       IF (DEBUG) write(0,109)I,N,A(N),RHS(I),ADS,X4
       DO 29 N=NMIN,NMAX
       J=INTER(N)
       ARED(I,J-1)=ARED(I,J-1)+AGRL(N,1)*A(N)
       ARED(I,J)=ARED(I,J)+AGRL(N,2)*A(N)
       IF(J-IMAX)28,29,29
   28  ARED(I,J+1)=ARED(I,J+1)+AGRL(N,3)*A(N)
   29  CONTINUE
   30  CONTINUE
C-----------------------------------------------------------------------
C  INITIATE CALL TO CLDLBN2 TO OBTAIN PROJECTION ONTO LOW LEVELS
C
C        BYPASS CALL IF IPOSNT.EQ.1.OR.IPOSNT.EQ.3
C-----------------------------------------------------------------------
       IF(IPOSNT.EQ.2.OR.IPOSNT.EQ.4)THEN
           Z1=Z
           ACNST=1.03928D-13*Z*ATE*DSQRT(ATE)
           A1CNST=6.60074D-24*(157890.0D0/TE)**1.5D0
           IECION=0
           IEDREC=0
           IERREC=0
           IEXREC=0
           IF(EXMEMB.NE.' ')THEN
               CALL CLDLBN2(EXMEMB,Z0,Z1,1.0D0,DENS,TE,DENSP,TP,BMENER,
     &         DENSH,W1,NMIN,NMAX,NREP,IMAX,ARED,RHS,
     &         CIONPT,DRECPT,RRECPT,XRECPT,IECION,IEDREC,IERREC,IEXREC,
     &         DVEC,ACNST,A1CNST, lpass)
           ENDIF
       ENDIF


C Write CR matrix to unit 10 (second passing file) if lpass is set

       if (iposnt.eq.4.and.lpass) then

          write(10,*)' DENS (cm-3)   BENER (eV/u)  TE (K)'
          write(10,'(1p,3e14.6)')dens, bmener, te

          write(10, '(30(i14,:))')(NREP(ii), ii=1,imax)

          fmul = ACNST / A1CNST
          do ii=1,imax
             write(10,'(i3, 2x, 1p, 30(e14.6,:))')
     &                  NREP(ii),
     &                 (fmul*ared(ii,jj)/dvec(ii), jj=1,imax)
          end do

          write(10,*)''

       endif
       
C-----------------------------------------------------------------------
C  RESUME PRINCIPAL CALCULATION
C-----------------------------------------------------------------------
       CALL MATINV(ARED,IMAX,RHS,2,DETERM)
       C=6.60074E-24*DENS*(157890.0/TE)**1.5
       DO 31 I=1,IMAX
       N=NREP(I)
       IF(I-1)601,601,600
  600  IDCN=INTER(N-1)
       CN1=RHS(IDCN-1)
       CN2=RHS(IDCN)
       IF(IMAX-IDCN)603,603,602
  602  CN3=RHS(IDCN+1)
       GO TO 604
  603  CN3=0.0
  604  CONTINUE
       IF(I-IDCN)605,606,605
  605  DELCN(I)=-CN1*AGRL(N-1,1)-CN2*AGRL(N-1,2)-CN3*(AGRL(N-1,3)-1.0)
       GO TO 601
  606  DELCN(I)=(CN2-CN1)*AGRL(N-1,1)+(CN2-CN3)*AGRL(N-1,3)
  601  CONTINUE
       DELCN(1)=0.0
       BREP(I)=1.0+RHS(I)
       DEXPBN(I)=DEXPTE(N)*BREP(I)
       EN=N
       POP(I)=C*EN*EN*DEXPBN(I)
       IF(IPOSNT.EQ.1)F2(I)=BREP(I)
       IF(IPOSNT.EQ.2)F1(I)=(BREP(I)-F2(I))/POP(1)
       IF(IPOSNT.EQ.3)F3(I)=(BREP(I)-F2(I))/(DENSH/DENS)
       IF(IPOSNT.NE.4)GOTO 31
       BNCALC(I)=F1(I)*POP(1)+F2(I)+F3(I)*(DENSH/DENS)
       BNACT(I)=BREP(I)
   31  CONTINUE
       IF(IPOSNT.EQ.2)XPOP(2)=POP(1)
       IF(IPOSNT.EQ.4)XPOP(1)=POP(1)
       IF (DEBUG) write(0,129)
       IF (DEBUG) write(0,112)(NREP(I),RHS(I),BREP(I),DEXPBN(I),
     &            POP(I),DELCN(I),I=1, IMAX)
       C=1.03928E-13*Z*ATE*DSQRT(ATE)
       ALFA=C*BREP(1)/ARED(1,1)
       if (pop(1).GT.0.0) S=ALFA/POP(1)
       IF (DEBUG) write(0,130)
       IF (DEBUG) write(0,116)ALFA,S
       IPOSS=1
       GO TO 197
  100  FORMAT(2I5,  1P,6E10.2)
  101  FORMAT(I5,5X,I5,5X,  1P,6E10.2)
  102  FORMAT(3I5)
  103  FORMAT(14I5)
  104  FORMAT(  1P,3E20.5)
  105  FORMAT(2I5,  1P,3E20.5)
  106  FORMAT(2I5,  1P,6E20.5)
  107  FORMAT(2I5,  1P,4E20.5)
  108  FORMAT(2I5,  1P,3E20.5)
  109  FORMAT(2I5,  1P,4E20.5)
  110  FORMAT(  1P,11E12.3)
  111  FORMAT(  1P,6E15.5)
  112  FORMAT(I5,  1P,5E15.5)
  113  FORMAT(I5)
  114  FORMAT(7F10.5)
  115  FORMAT(7F10.5)
  116  FORMAT(  1P,2E20.5)
  117  FORMAT('   JDENSM     JTEM      TRAD        W         Z',
     &        '2CION    CP(Y)     W1')
  118  FORMAT(15H NMIN NMAX IMAX)
  119  FORMAT(20H    NREP(I),I=1,IMAX)
  120  FORMAT(5H JCOR)
  121  FORMAT(19H    COR(J),J=1,JCOR)
  122  FORMAT(5H JMAX)
  123  FORMAT(21H    EPSIL(J),J=1,JMAX)
  124  FORMAT(19H    FIJ(J),J=1,JMAX)
  125  FORMAT(5H JDEF)
  126  FORMAT(30H    QUANTUM DEFECT(J),J=1,JDEF)
  127  FORMAT(54H      157890ZZ/TE       157890ZZ/TRAD              RHO)
  128  FORMAT('   I    N              A(N)              RHS(I)',
     &        '1       DIELEC                CHEXCH')
  129  FORMAT('    N          CN             BN        EXP(XN)BN     P',
     &        'OPULATION     C(N)-C(N-1)')
  130  FORMAT(40H        RECOMB COEFFT       IONIZN COEFT)
  131  FORMAT(20H    NE        TE    /)
  132  FORMAT(  1P,2E10.2)
  134  FORMAT(  1P,7E10.2)
  135  FORMAT(21H  DENSA(J),J=1,JDENSM)
  136  FORMAT(17H  TEA(J),J=1,JTEM)
  137  FORMAT(1H ,'  NIP       INTD      IPRS      ILOW     IONIP    NIO
     &NIP   ILPRS     IVDISP     1')
  138  FORMAT(  1P,6E12.4)
  139  FORMAT(  1P,1E12.4)
  140  FORMAT(21H    WBREP(I),I=1,IMAX)
  141  FORMAT(21H  DENPA(J),J=1,JDENSM)
  142  FORMAT(17H  TPA(J),J=1,JTEM)
  143  FORMAT(20H    NP        TP    )
  144  FORMAT(19H    WIJ(J),J=1,JMAX)
  145  FORMAT(I5,3F10.5)
  146  FORMAT(1H ,'   IMP    ZIMP      AMSS      FRAC')
  147  FORMAT(1H ,'BEAM ENER.(EV)',10X,'H DENSITY(CM-3)',10X,
     &'FLUX(CM-2SEC-1)')
  148  FORMAT(5X,1PE10.2,15X,1PE10.2,15X,1PE10.2)
 1997 FORMAT(' W1 IS SWITCHED OFF....W1 = ',  1P,1E12.2)
 1998 FORMAT(' NH IS SWITCHED OFF....NH = ',  1P,1E12.2)
 1999 FORMAT(I5,7(5X,I5),5X,F5.1,2I5)
 2000 FORMAT(8I5,F5.1,2I5)
      END
