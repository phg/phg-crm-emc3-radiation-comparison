CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas310/gbf.for,v 1.2 2004/07/06 13:58:17 whitefor Exp $ Date $Date: 2004/07/06 13:58:17 $
CX
       FUNCTION GBF(EN,U)                                               
       IMPLICIT REAL*8 (A-H,O-Z)                                       
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 FUNCTION: GBF **************************
C
C PURPOSE Calculate bound-free gaunt factor as given by Eqn. 34 in
C         Burgess and Summers, MNRAS, vol 226, p257-272 (1987).
C
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 16-01-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C VERSION: 1.2                          DATE: 13-10-99
C MODIFIED: Martin O'Mullane
C               - Annotated routine
C               - Put in double precision constants.
C               - Corrected T2 correction factor for an erroneous 25/18
C                 to 28/18 as in the paper. 
C-----------------------------------------------------------------------
C
       X=(EN*(U+1.0D0))**0.6666667D0
       T1=0.1728D0*(U-1.0D0)/X
       T2=1.55555556D0*T1*T1+1.333333D0*(0.0496D0*
     &    (U*U+1.333333D0*U+1.0D0))/(X*X)
       GBF=1.0D0/(1.0D0-1.333333D0*T1+T2)**0.75D0
       RETURN
       END
