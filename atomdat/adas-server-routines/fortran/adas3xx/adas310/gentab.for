CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas310/gentab.for,v 1.1 2004/07/06 13:58:27 whitefor Exp $ Date $Date: 2004/07/06 13:58:27 $
CX
       SUBROUTINE GENTAB(NREP,IMAX,DENS,TE,BMENER,F1,F2,F3)             
C
       IMPLICIT REAL*8(A-H,O-Z)                                        
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: GENTAB *********************
C
C PURPOSE : SEND DATA TO STREAM '10' FOR SUBSEQUENT TABLE PRODUCTION
C
C NOTES: THIS ROUTINE IS NOT YET PROPERLY ANNOTATED
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 25-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C-----------------------------------------------------------------------
C
       DIMENSION NREP(31),F1(30),F2(30),F3(30)                       
       MAXL=0                                                       
       DO 5 I=1,IMAX                                               
       IF(NREP(I).GT.20)GOTO 6                                    
    5  MAXL=MAXL+1                                               
    6  WRITE(10,104)                                            
       WRITE(10,106)DENS,TE,BMENER                             
       LMAXL1=MAXL-1                                          
       WRITE(10,105)LMAXL1                                   
       WRITE(10,105)(NREP(I),I=2,MAXL)                      
       DO 10 IL=2,MAXL                                     
   10  WRITE(10,106)F1(IL),F2(IL),F3(IL)                  
       RETURN                                            
  104  FORMAT(20X,'  LIST NO.')                         
  105  FORMAT(16I5)                                    
  106  FORMAT(1P3D10.2)                               
       END                                           
