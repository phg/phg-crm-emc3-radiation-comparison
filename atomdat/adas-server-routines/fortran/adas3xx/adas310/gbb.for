CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas310/gbb.for,v 1.2 2004/07/06 13:58:06 whitefor Exp $ Date $Date: 2004/07/06 13:58:06 $
CX
       FUNCTION GBB(EN,EN1,X2,X)                                        
C-----------------------------------------------------------------------
C                                                                      
C       PURPOSE:    TO EVALUATE THE BOUND BOUND GAUNT FACTOR USING THE
C                   EXPRESSION OUTLINED BY BURGESS.A AND SUMMERS.H.P [1].
C
C       BACKGROUND: THE BOUND BOUND GAUNT FACTOR IS USED IN THE 
C                   EVALUATION OF THE EINSTEIN A COEFFICIENT. THE
C                   EINSTEIN A COEFFICIENT IS USUALLY EXPRESSED IN 
C                   TERMS OF THE UPWARD OSCILLATOR STRENGTH [2].
C                   HOWEVER,IT IS MORE CONVIENIENT TO WRITE THE
C                   UPWARD OSCILLATOR STRENGTH IN TERMS OF WHAT IS
C                   DESCRIBED AS AN APPROXIMATE UPWARD OSCILLATOR 
C                   STRENGTH.THE BOUND BOUND GAUNT FACTOR IS A 
C                   CORRECTION TO THE APPROXIMATE OSCILLATOR 
C                   STRENGTH [3].
C
C       REFERENCES:
C                   [1] BURGESS.A & SUMMERS.H.P 
C                       THE RECOMBINATION AND LEVEL POPULATION OF IONS I 
C                       Mon.Not.R.astr.Soc.(1976),174,PP345-391.
C                       ( SEE EQUATION A5 ). 
C
C                   [2] SPENCE.J
C                       STUDIES OF CHARGE EXCHANGE RECOMBINATION IN
C                       LABORATORY FUSION PLASMAS.
C                       Phd THESIS.
C                       ( SEE P36 EQUATIONS 2.4.2.1 TO 2.4.2.3 ).
C
C                   [3] MENZEL.D.H & PEKERIS.C.L
C                       Mon.Not.R.astr.Soc.(1935),96,P77.
C
C       CALLING PROGRAM:
C
C       FUNCTION:
C               
C       INPUT : (R*8) X2        = THE RECIPROCAL OF THE SQUARE OF THE
C                                 UPPER PRINCIPAL QUANTUM NUMBER.
C       INPUT : (R*8) X         = GENERAL VARIABLE RELATED TO THE
C                                 UPPER PRINCIPAL QUANTUM NUMBER.
C				  X=EN23(N)*EN23(N2)/EN23(N11-1)
C       INPUT : (R*4) EN        = THE UPPER PRINCIPAL QUANTUM NUMBER.
C       INPUT : (R*4) EN1       = THE LOWER PRINCIPAL QUANTUM NUMBER.
C
C       OUTPUT: (R*8) GBB       = THE BOUND BOUND GAUNT FACTOR.
C
C               (R*8) G1        = GENERAL VARIABLE.
C               (R*8) G2        = GENERAL VARIABLE.
C               (R*8) G3        = GENERAL VARIABLE.
C               (R*8) T1        = GENERAL VARIABLE.
C               (R*8) T2        = GENERAL VARIABLE.
C               (R*8) T3        = GENERAL VARIABLE.
C               (R*8) T4        = GENERAL VARIABLE.
C               (R*8) X4        = THE RECIPROCAL OF THE PRINCIPAL
C                                 QUANTUM NUMBER CUBED.
C
C
C       CONTACT: HARVEY ANDERSON
C               UNIVERSITY OF STRATHCLYDE.
C               ROOM 4.13B EXT 4213
C
C       DATE:   23/12/95
C
C
C	VERSION:	1.2					DATE: 17-03-99
C	MODIFIED: HARVEY ANDERSON
C			- ADDED DOCUMENTATION
C
C-----------------------------------------------------------------------
C
        REAL*8  X2      ,       X       ,       GBB     ,       G1     ,       
     &          G2      ,       G3      ,       T1      ,       T2     ,       
     &          T3      ,       T4      ,       X4	,	EN     ,
     &		EN1
C
C-----------------------------------------------------------------------
C
       X4=X2*X2                                                         
       G1=(0.203+0.256*X2+0.257*X4)*EN                                  
       G2=0.170*EN+0.18                                                 
       G3=(0.2214+0.1554*X2+0.370*X4)*EN                               
       T1=(EN1+EN1-EN)*(EN1-EN+1.0)                                     
       T2=4.0*(EN1-1.0)*(EN-EN1-1.0)                                    
       T3=(EN1+EN1-EN+0.001)*(EN1-0.999)                                
       T4=EN-1.999                                                      
       T4=X/(T4*T4*EN)                                                  
       GBB=1.0-T4*(T1*G1+T2*G2+T3*G3)                                   
       RETURN                                                           
      END                                                               
