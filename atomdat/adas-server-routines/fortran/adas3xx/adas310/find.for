CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas310/find.for,v 1.1 2004/07/06 13:53:26 whitefor Exp $ Date $Date: 2004/07/06 13:53:26 $
CX
      SUBROUTINE FIND(ARR,VALUE,IMAX,INDEX)                             
                                                                       
C-----------------------------------------------------------------------
C                                                                      
C  ****************** FORTRAN77 SUBROUTINE: FIND **********************
C
C  PURPOSE:
C  SUBROUTINE TO FIND VALUES FROM LINE AND STORE ONLY NOT REPEATED    
C  VALUES                                                            
C                                                                   
C                                                                  
C  INPUT                                                          
C      ARR = NAME OF ARRAY TO STORE ELEMENTS                     
C      VALUE = VALUE TO BE FOUND IN LINE                        
C      IMAX = TOTAL NUMBER OF NOT REPEATED ELEMENTS IN ARRAY   
C      INDEX = COUNTER OF ELEMENT POSITION IN ARRAY           
C  OUTPUT                                                    
C       ARR = ARRAY CONTAINING NOT REPEATED VALUES READ FROM LINE      
C                                                                       
C NOTES: THIS ROUTINE IS NOT YET PROPERLY ANNOTATED
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 01-02-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER SCCS CONTROL
C
C-----------------------------------------------------------------------
      LOGICAL OVER,FOUND                                               
      REAL ARR(*),VALUE                                               
      INTEGER IMAX,INDEX                                             
C                                                                   
      OVER=.FALSE.                                                 
      FOUND=.FALSE.                                               
      INDEX=1                                     
 100  IF(.NOT.(FOUND.OR.OVER)) THEN                
           OVER=(INDEX.GT.IMAX)                     
           IF(.NOT.OVER) THEN                        
                FOUND=(ARR(INDEX).EQ.VALUE)           
                IF(.NOT.FOUND) INDEX=INDEX+1           
           ENDIF                                        
           GO TO 100                                     
      ENDIF                                               
      IF(OVER) THEN                                        
           IMAX=IMAX+1                                      
           ARR(IMAX)=VALUE                                   
      ENDIF                                                   
C                                                              
      RETURN                                                    
      END                                                        
