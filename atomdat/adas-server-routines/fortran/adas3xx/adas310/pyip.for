CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas310/pyip.for,v 1.1 2004/07/06 14:38:10 whitefor Exp $ Date $Date: 2004/07/06 14:38:10 $
CX
       SUBROUTINE PYIP(D,EM,ZCOL,PHI,CPY,WI,WJ,R,TE,INTD,PY)  
C
       IMPLICIT REAL*8 (A-H,O-Z)                            
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: PYIP ***********************
C
C PURPOSE: UNKNOWN
C
C NOTES: THIS ROUTINE IS NOT YET PROPERLY ANNOTATED
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 16-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C-----------------------------------------------------------------------
C
       DIMENSION A(4),H(4)                                  
       EIJ=D*(ZCOL+1.0)*(ZCOL+1.0)                         
       PY=0.0                                             
       IF(INTD-3)10,20,30                                
   10  A(1)=0.585786437                                 
       A(2)=3.414213562                                
       H(1)=0.853553390                               
       H(2)=0.146446609                              
       GO TO 40                                     
   20  A(1)=0.415774556                            
       A(2)=2.294280360                           
       A(3)=6.289945083                          
       H(1)=0.711093010                         
       H(2)=0.278517733                        
       H(3)=0.010389256                       
       GO TO 40                              
   30  A(1)=0.322547689                     
       A(2)=1.745761101                    
       A(3)=4.536620297                   
       A(4)=9.395070912                  
       H(1)=0.603154104                 
       H(2)=0.357418692                
       H(3)=0.038887908               
       H(4)=0.000539294              
   40  DO 50 I=1,INTD               
       EI=A(I)*TE/157890.0+EIJ     
       CALL EIQIP(EI,EIJ,EM,ZCOL,PHI,CPY,WI,WJ,R,EIQ,FLAG)          
       PY=PY+H(I)*0.552*EIQ/(8.0*PHI)       
   50  CONTINUE                            
       EM2=DSQRT(EM)                      
       PY=PY/EM2                         
       RETURN                           
       END                              
