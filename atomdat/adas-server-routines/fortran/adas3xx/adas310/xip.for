CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas310/xip.for,v 1.1 2004/07/06 15:30:18 whitefor Exp $ Date $Date: 2004/07/06 15:30:18 $
CX
       FUNCTION XIP(XI,DELTA)                                           
C
       IMPLICIT REAL*8 (A-H,O-Z)                                       
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 FUNCTION: XIP **************************
C
C PURPOSE UNKNOWN
C
C NOTES: THIS ROUTINE IS NOT YET PROPERLY ANNOTATED
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 16-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C-----------------------------------------------------------------------
C
       XM=DABS(XI)                                                    
       X1=3.142*(DELTA+0.25+0.03125/(DELTA+0.5))*DEXP(-2.0*DELTA     
     1 +1.1416*(XM+XM*XM)/(1.0+0.6*XM+3.0*XM*XM/(6.0+2.0*DELTA)))   
       X2=3.0/(XM+3.0)                                             
       X3=0.2/(XM+0.4)                                            
       XIP=X1*(1.0-X2+X2*X2+X3-2.0*X3*X3)                        
       IF(XI)1,2,2                                              
    1  XIP=XIP*DEXP(6.283*XI)                                  
    2  CONTINUE                                               
       RETURN                                                
       END                                                   
