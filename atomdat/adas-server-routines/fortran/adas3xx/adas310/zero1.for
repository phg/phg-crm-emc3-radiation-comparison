CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas310/zero1.for,v 1.1 2004/07/06 15:41:22 whitefor Exp $ Date $Date: 2004/07/06 15:41:22 $
CX
       SUBROUTINE ZERO1(A,B,VA,VB,D1,X,XI,Z,E,TIF,T2)  
C
       IMPLICIT REAL*8 (A-H,O-Z)                 
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: ZERO1 **********************
C
C PURPOSE UNKNOWN
C
C NOTES: THIS ROUTINE IS NOT YET PROPERLY ANNOTATED
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 16-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C-----------------------------------------------------------------------
C
       X=(A*VB-B*VA)/(VB-VA)                    
    9  X1=X                                    
       T=Z+E*X                                    
       D=TIF*X                                     
       V=0.5-T2*XIP(XI,D)/(T*T)                     
       IF(V)11,16,10                                 
   10     B=X                                         
       VB=V                                            
       GO TO 12                                         
   11     A=X                                            
       VA=V                                               
   12     X=0.5*(A+B)                                      
       T=Z+E*X                                              
       D=TIF*X                                               
       V=0.5-T2*XIP(XI,D)/(T*T)                               
       IF(V)14,16,13                                           
   13     B=X                                                   
       VB=V                                                      
       GO TO 15                                                   
   14     A=X                                                      
       VA=V                                                         
   15     X=(A*VB-B*VA)/(VB-VA)                                      
       IF(DABS(X1-X)/X-D1)16,16,9                                     
   16     RETURN                                                       
      END                                                               
