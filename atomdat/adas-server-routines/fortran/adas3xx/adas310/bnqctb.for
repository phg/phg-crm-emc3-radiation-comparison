CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas310/bnqctb.for,v 1.12 2007/05/24 15:27:31 mog Exp $ Date $Date: 2007/05/24 15:27:31 $
CX
       SUBROUTINE BNQCTB(Z0,Z1,NMIN,NMAX,IMAX,NREP,NBEAM,BMENA,BMFRA,
     &                   CXMEMB,IBLOCK,QTHREP,ALPHA)
C
       IMPLICIT REAL*8(A-H,O-Z)
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: BNQCTB ********************
C
C ---------------------------------------------------------------------
C  PURPOSE: CALCULATE THEORETICAL CHARGE EXCHANGE RATE COEFFICIENTS
C  FROM NEUTRAL HYDROGEN.
C
C  RATE DATA IS RETURNED TO REPRESENTATIVE N-SHELLS FOR USE BY BUNDLE-N
C  CODES.
C
C  INPUT FROM ARCHIVED DATASET IS ON UNIT 11.
C
C  THE NAME OF THE SELECTED DATASET IS CONTAINED IN: 'CXMEMB'
C
C  AND IS OPENED IN THE SUBROUTINE.
C
C  THIS VERSION USES   '1989 RESTRUCTURED DATA'   MEMBERS WITH THE
C  CHANGED L-FITTING PARAMETERS
C  THE NEW PARAMETERS ARE TRANSFERED IN   COMMON /LFIT89/
C
C  THE SUBROUTINE IS A DEVELOPMENT OF QCHEX, NEWCX2, NCHEX2 ETC.
C  ORIGINALLY WRITTEN BY J. SPENCE.  THIS VERSION ECONOMISES ON
C  SUBROUTINES.
C
C  INPUT
C      Z0=TARGET ION NUCLEAR CHARGE
C      Z1=RECOMBINING TARGET ION CHARGE
C      NMIN=LOWEST REPRESENTATIVE N-LEVEL OF TARGET
C      NMAX=HIGHEST REPRESENTATIVE N-LEVEL OF TARGET
C      IMAX=NUMBER OF REPRESENTATIVE LEVELS
C      NREP(I)=REPRESENTATIVE N-LEVELS
C      NBEAM=NUMBER OF ENERGY COMPONENTS IN NEUTRAL HYDROGEN BEAM
C      BMENA(J)=BEAM ENERGY COMPONENTS (EV/AMU)
C      BMFRA(J)=BEAM FRACTIONS IN ENERGY COMPONENTS
C      CXMEMB=DATA SET NAME OF CHARGE EXCHANGE DATA SET.
C      IBLOCK=1 SELECT UDW METHOD OR 1ST DATA BLOCK
C            =2 SELECT CCAO METHOD OR 2ND DATA BLOCK
C            =3 SELECT CTMC METHOD OR 3RD DATA BLOCK
C            =4 SELECT CCMO METHOD OR 4TH DATA BLOCK
C
C  OUTPUT
C      QTHREP(I)=MEAN RATE COEFFICIENTS FOR REPRESENTATIVE
C                N-LEVELS  (AVERAGED OVER BEAM FRACTIONS) (CM3 SEC-1)
C      ALPHA=SIZE OF 1/N**ALPHA TAIL FOR CH.EXCH X-SECT.
C
C  ********** H.P.SUMMERS, JET            13 DEC 1989  ***************
C ----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C
C UPDATE:  19/01/94 - JONATHAN NASH - TESSELLA SUPPORT SERVICES PLC
C
C          THE FOLLOWING MODIFICATIONS HAVE BEEN MADE TO THE SUBROUTINE:
C
C          1) THE COMPLETE CHARGE EXCHANGE DATA SET NAME IS NOW PASSED
C             INTO THE ROUTINE RATHER THAN JUST THE MEMBER NAME.
C
C          2) THE ROUTINE HAS BEEN UPGRADED TO READ NEW ADF01 FORMAT.
C
C  NOTES:  NO ATTEMPT HAS BEEN MADE TO RESTRUCTURE THE ROUTINE. RATHER
C          THE MINIMUM AMOUNT OF WORK TO INTEGRATE THE ROUTINE INTO
C          ADAS310 HAS BEEN COMPLETED.
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 16-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C VERSION: 1.2                          DATE: 17-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - ADDED "STATUS='UNKNOWN'" TO OPEN STATEMENT
C
C VERSION: 1.3                          DATE: 22-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - REPLACED CALLS TO NAG ROUTINE E02BBF WITH ADAS ROUTINE
C                 DXNBBF
C
C VERSION: 1.4                          DATE: 23-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - REPLACED CALLS TO NAG ROUTINE E01BAF WITH ADAS ROUTINE
C                 DXNBAF
C
C VERSION: 1.5                          DATE: 23-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - RELABELLED LOOP COUNTERS FOR LOOPS 176 AND 177
C
C VERSION: 1.6                          DATE: 24-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - RENAMED NBENG TO NBENG2 TO AVOID CONFUSION WITH
C                 OTHER NBENG IN OTHER ROUTINES
C                 REMOVED SUPERFLUOUS VARIABLES
C
C VERSION: 1.7                          DATE: 14-10-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C               - CORRECTED SECOND CALL TO DXNBAF - IT WAS USING XSA AND
C                 YSA RATHER THAN XSA AND ZSA
C
C VERSION: 1.8				DATE: 09-04-98
C MODIFIED: HARVEY ANDERSON ( UNIVERSITY OF STRATHCLYDE )
C		- CHANGED VARIBLE MXE FROM 24 TO 40.
C		- INCREASED SIZE OF ARRAYS ASSOCIATED WITH THE
C		  ROUTINES DXNBAF AND DXNBBF.
C		- REPLACED NUMERICAL VALUE WITH THE PARAMETER
C		  MXE IN THE IF STATEMENT WHICH TESTS TO ENSURE
C		  THAT THE NUMBER OF BEAM ENERGIES READ FROM
C		  INPUT FILE IS NOT GREATER THE ARRAY DIMMENSIONS
C		  OF THE RELEVANT ARRAYS.
C
C VERSION: 1.9				DATE: 23-06-98
C MODIFIED: RICHARD MARTIN
C 		-CORRECTED SCCS ERROR.
C
C VERSION: 1.10				DATE: 07-07-2004
C MODIFIED: ALLAN WHITEFORD
C 		-CHANGED CALLS FROM DXNB{A,B}F TO XXNB{A,B}F
C
C VERSION: 1.11				DATE: 16-05-07
C MODIFIED: Allan Whiteford
C	    - Updated comments as part of subroutine documentation
C             procedure.
C
C VERSION  : 1.12
C DATE     : 22-05-2007
C MODIFIED : Martin O'Mullane
C              - Remove unused m-subshell data possibility and
C                use xxdata_01 to access adf01 data.
C
C-----------------------------------------------------------------------
C
C          (I*4)  MXE       = MAXIMUM NO. OF ENERGIES.
C          (I*4)  MXN       = MAXIMUM NO. OF N SHELLS.
C          (I*4)  IZR       = ION CHARGE OF RECEIVER.
C          (I*4)  IZD       = ION CHARGE OF DONOR.
C          (I*4)  INDD      = DONOR STATE INDEX.
C          (I*4)  NBENG2    = NUMBER OF ENERGIES READ.
C          (I*4)  NMINF     = LOWEST N-SHELL FOR WHICH DATA READ.
C          (I*4)  NMAXF     = HIGHEST N-SHELL FOR WHICH DATA READ.
C
C          (L*4)  LPARMS    = FLAGS IF L-SPLITTING PARAMETERS PRESENT.
C                             .TRUE.  => L-SPLITTING PARAMETERS PRESENT.
C                             .FALSE  => L-SPLITTING PARAMETERS ABSENT.
C          (L*4)  LSETL     = FLAGS IF L-RESOLVED DATA PRESENT.
C                             .TRUE.  => L-RESOLVED DATA PRESENT.
C                             .FALSE  => L-RESOLVED DATA ABSENT.
C          (L*4)  LSETM     = FLAGS IF M-RESOLVED DATA PRESENT.
C                             .TRUE.  => M-RESOLVED DATA PRESENT.
C                             .FALSE  => M-RESOLVED DATA ABSENT.
C
C          (C*80) TITLE     = NOT SET - TITLE FOR DATA SOURCE.
C          (C*2)  SYMBR     = RECEIVER ION ELEMENT SYMBOL.
C          (C*2)  SYMBD     = DONOR ION ELMENT SYMBOL.
C
C          (I*4)  LFORMA()  = PARAMETERS FOR CALCULATING L-RES X-SEC.
C                             DIMENSION: MXE
C
C          (R*8)  BENGY()   = COLLISION ENERGIES.
C                             UNITS: EV/AMU (READ AS KEV/AMU)
C                             DIMENSION: MXE
C          (R*8)  ALPHAA()  = EXTRAPOLATION PARAMETER ALPHA.
C                             DIMENSION: MXE
C          (R*8)  XLCUTA()  = PARAMETERS FOR CALCULATING L-RES X-SEC.
C                             DIMENSION: MXE
C          (R*8)  PL2A()    = PARAMETERS FOR CALCULATING L-RES X-SEC.
C                             DIMENSION: MXE
C          (R*8)  PL3A()    = PARAMETERS FOR CALCULATING L-RES X-SEC.
C                             DIMENSION: MXE
C          (R*8)  XTOT()    = TOTAL CHARGE EXCHANGE CROSS-SECTION.
C                             UNITS: CM2
C                             DIMENSION: MXE
C
C          (R*8)  XSIGN(,)  = N-RESOLVED CHARGE EXCHANGE CROSS-SECTIONS.
C                             UNITS: CM2
C                             1ST DIMENSION: MXE
C                             2ND DIMENSION: MXN
C          (R*8)  XSIGL(,)  = L-RESOLVED CHARGE EXCHANGE CROSS-SECTIONS.
C                             UNITS: CM2
C                             1ST DIMENSION: MXE
C                             2ND DIMENSION: (MXN*(MXN+1))/2
C          (R*8)  XSIGM(,)  = M-RESOLVED CHARGE EXCHANGE CROSS-SECTIONS.
C                             UNITS: CM2
C                             1ST DIMENSION: MXE
C                             2ND DIMENSION: (MXN*(MXN+1)*(MXN+2))/6
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    MXN       , MXE
      PARAMETER( MXN = 20  , MXE = 40 )
C-----------------------------------------------------------------------
      INTEGER    IZR     , IZD     , INDD     , NBENG2  , NMINF   ,
     &           NMAXF
C-----------------------------------------------------------------------
      LOGICAL    LPARMS  , LSETL   , LSETM
C-----------------------------------------------------------------------
      CHARACTER  TITLE*80  , SYMBR*2   , SYMBD*2
C-----------------------------------------------------------------------
      INTEGER    LFORMA(MXE)
C-----------------------------------------------------------------------
      REAL*8     BENGY(MXE)   , ALPHAA(MXE)  , XLCUTA(MXE)  ,
     &           PL2A(MXE)    , PL3A(MXE)    , XTOT(MXE)
C-----------------------------------------------------------------------
      REAL*8     XSIGN(MXE,MXN)                      ,
     &           XSIGL(MXE,(MXN*(MXN+1))/2)          ,
     &           XSIGM(MXE,(MXN*(MXN+1)*(MXN+2))/6)
C-----------------------------------------------------------------------
C
       CHARACTER CXMEMB*80
       DIMENSION ZDATA(30),BMENA(6),BMFRA(6)
       DIMENSION NDATA(2),IEDATA(2)
       DIMENSION QTHREP(31),RATE(6,31),NREP(31)
       DIMENSION XSA(MXE),YSA(MXE),ZSA(MXE)
       DIMENSION WTS(MXE),B(MXE),A(MXE+4,4),DIAG(MXE+4)
       DIMENSION XKA(MXE+4),C(MXE+4),ZKA(MXE+4),ZC(MXE+4)
CX     DIMENSION WRK(160)
       DATA LCK,LWRK/28,160/
       DATA ZDATA/1.00D+00,2.00D+00,0.00D+00,4.00D+00,5.00D+00,6.00D+00,
     &            0.00D+00,8.00D+00,0.00D+00,1.00D+01,0.00D+00,0.00D+00,
     &            0.00D+00,1.40D+01,0.00D+00,0.00D+00,0.00D+00,0.00D+00,
     &            0.00D+00,0.00D+00,0.00D+00,0.00D+00,0.00D+00,0.00D+00,
     &            0.00D+00,0.00D+00,0.00D+00,0.00D+00,0.00D+00,0.00D+00/
       ALPHA=0.00D+00
       DO 50 I=1,IMAX
        QTHREP(I)=0.00D+00
   50  CONTINUE
       IZ0=Z0
       IF(Z0.EQ.ZDATA(IZ0))THEN
           IF ( CXMEMB .EQ. ' ' ) RETURN
CJ         WRITE(6,1006)CXMEMB
           OPEN(UNIT=11,FILE=CXMEMB,ERR=9999,STATUS='UNKNOWN')
C
C-----------------------------------------------------------------------
C READ SELECTED CHARGE EXCHANGE DATA.
C-----------------------------------------------------------------------
C
           call xxdata_01( 11     , mxe    , mxn    ,
     &                     symbr  , symbd  , izr    , izd    ,
     &                     indd   , nbeng2 , nminf  , nmaxf  ,
     &                     lparms , lsetl  , bengy  ,
     &                     alphaa , lforma , xlcuta , pl2a   ,
     &                     pl3a   , xtot   , xsign  , xsigl
     &                    )
C
           NDATA(1) = NMINF
           NDATA(2) = NMAXF
           IEDATA(1) = 1
           IEDATA(2) = NBENG2
C
           CLOSE(11)
C
       ENDIF
C
C-----------------------------------------------------------------------
C  MAIN LOOPS THROUGH BEAM ENERGY FRACTIONS AND REPRESENTATIVE LEVELS
C-----------------------------------------------------------------------
       NENER=IEDATA(2)-IEDATA(1)+1
       IF (NENER.GT.MXE) THEN
          WRITE(I4UNIT(-1),*)"BNQCTB ERROR: TOO MANY ENERGIES IN ",
     &                       "INPUT FILE"
           STOP
       ENDIF
       DO 80 I=1,IMAX
        N=NREP(I)
        IF(N.GE.NDATA(1))THEN
            NN=MIN0(N,NDATA(2))
            DO 71 IENER=1,NENER
             XSA(IENER)=DLOG(BENGY(IEDATA(1)+IENER-1))
             YSA(IENER)=DLOG(XSIGN(IEDATA(1)+IENER-1,NN))
             WTS(IENER)=1.0
   71       CONTINUE
            IFAIL=0
CX          CALL E01BAF(NENER,XSA,YSA,XKA,C,LCK,WRK,LWRK,IFAIL)
            DO 176, IENER=3,NENER-2
176             XKA(IENER+2) = XSA(IENER)
            CALL XXNBAF(NENER,NENER+4,XSA,YSA,WTS,XKA,B,A,DIAG,
     &                  C,SS,IFAIL)
        ENDIF
        IF(N.GT.NDATA(2))THEN
            DO 72 IENER=1,NENER
             ZSA(IENER)=DLOG(ALPHAA(IEDATA(1)+IENER-1))
   72       CONTINUE
            IFAIL=0
            DO 177, IENER=3,NENER-2
177             ZKA(IENER+2) = XSA(IENER)
CX          CALL E01BAF(NENER,XSA,ZSA,ZKA,ZC,LCK,WRK,LWRK,IFAIL)
            CALL XXNBAF(NENER,NENER+4,XSA,ZSA,WTS,ZKA,B,A,DIAG,
     &                  ZC,SS,IFAIL)
        ENDIF
        DO 75 ILOOP=1,NBEAM
         VEL=1.384D6*DSQRT(BMENA(ILOOP))
         IF(BMENA(ILOOP).LT.BENGY(IEDATA(1)))THEN
             RATE(ILOOP,I)=0.0D0
         ELSEIF(BMENA(ILOOP).GT.BENGY(IEDATA(2)))THEN
             RATE(ILOOP,I)=0.0D0
         ELSEIF(N.LT.NDATA(1))THEN
             RATE(ILOOP,I)=0.0D0
         ELSE
             XVAL=DLOG(BMENA(ILOOP))
             IFAIL=0
CX           CALL E02BBF(NENER+4,XKA,C,XVAL,SVAL,IFAIL)
             CALL XXNBBF(NENER+4, XKA, C, XVAL, SVAL, IFAIL)
             RATE(ILOOP,I)=VEL*DEXP(SVAL)
             IF(N.GT.NDATA(2))THEN
                 ZVAL=DLOG(BMENA(ILOOP))
                 IFAIL=0
CX               CALL E02BBF(NENER+4,ZKA,ZC,ZVAL,SVAL,IFAIL)
                 CALL XXNBBF(NENER+4, ZKA, ZC, ZVAL, SVAL, IFAIL)
                 ALPHA=DEXP(SVAL)
                 RATE(ILOOP,I)=RATE(ILOOP,I)*
     &           (DFLOAT(NN)/DFLOAT(N))**ALPHA
             ENDIF
         ENDIF
   75   CONTINUE
   80  CONTINUE
C-----------------------------------------------------------------------
C  COMBINE SET OF BEAM ENERGIES FRACTIONS
C-----------------------------------------------------------------------
CJ     WRITE(6,1007)
       DO 90 I=1,IMAX
        QTHREP(I)=0.00D+00
        DO 85 ILOOP=1,NBEAM
         VEL=1.384D6*DSQRT(BMENA(ILOOP))
         QTHREP(I)=QTHREP(I)+RATE(ILOOP,I)*BMFRA(ILOOP)
   85   CONTINUE
CJ      WRITE(6,1008)I,NREP(I),QTHREP(I)
   90  CONTINUE
       RETURN
C
C-----------------------------------------------------------------------
C
 9999  WRITE(6,1004)
       STOP
C
C-----------------------------------------------------------------------
C
 1000  FORMAT(1H ,'CHARGE EXCHANGE MODEL SELECTED NOT IMPLEMENTED')
 1001  FORMAT(1A80)
 1002  FORMAT(1H ,'CHARGE EXCHANGE RATES OUTSIDE ABOVE ENERGY RANGE ARE
     & SET TO ZERO')
 1003  FORMAT(1H ,1A80)
 1004  FORMAT(1H ,'CHARGE EXCHANGE DATA SET DOES NOT EXIST')
 1005  FORMAT(' THERE IS NO CHARGE EXCHANGE DATA FOR Z0 = ',1PE8.2)
 1006  FORMAT(1H ,A)
 1007  FORMAT(1H ,'    I    N        QTHREP')
 1008  FORMAT(1H ,2I5,1PD15.4)
C
C-----------------------------------------------------------------------
C
       END
