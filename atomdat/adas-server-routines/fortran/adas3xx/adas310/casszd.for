C SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas310/casszd.for,v 1.4 2008/03/26 15:56:22 allan Exp $ Date $Date: 2008/03/26 15:56:22 $
      SUBROUTINE CASSZD( IBSEL  , IZ0IN  ,
     &                   ITVAL  , TVAL   ,
     &                   BWNO   , IZ     , IZ1  ,
     &                   METI   , METF   ,
     &                   SZDA   , LTRNG  ,
     &                   TITLX  , IRCODE , DLPATH
     &                 )

      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ******************* FORTRAN77 SUBROUTINE: CASSZD ********************
C
C  PURPOSE: TO EXTRACT AND  INTERPOLATE  ZERO-DENSITY  IONIZATION  RATE-
C           COEFFICIENTS FOR GIVEN ELEMENT NUCLEAR CHARGE AND DATA-BLOCK
C           FOR AN INPUT SET OF ELECTRON TEMPERATURES (eV).
C
C           - USES THE SAME ROUTINES USED BY SSZD, EXCEPT FOR:
C
C           'CAFILE' - WHICH OPENS THE REQUESTED FILE.
C           'CACHKB' - WHICH CHECKS INPUT  VALUES  ARE  CONSISTENT  WITH
C                      THE SELECTED DATA-BLOCK 'IBSEL' AND   'IBSEL'  IS
C                      IN RANGE.
C
C            THE FIRST OF THESE FUNCTIONS IS CARRIED  OUT  IN  'ADAS502'
C            VIA ISPF PANELS USING THE ROUTINE 'E2SPF0'  -  ADAS502 DOES
C            NOT REQUIRE THE ROUTINE 'CACHKB' AS THE USER CANNOT  SELECT
C            AN INVALID VALUE FOR 'IBSEL' OR 'IBSEL'/ELEMENT COMBINATION
C
C
C  CALLING PROGRAM: NSUPH1
C
C  SUBROUTINE:
C
C  INPUT : (I*4)  IBSEL   = INDEX OF DATA-BLOCK SELECTED FOR ANALYSIS
C  INPUT : (I*4)  IZ0IN   = NUCLEAR CHARGE OF REQUIRED ELEMENT
C
C  INPUT : (I*4)  ITVAL   = NUMBER OF ELECTRON TEMPERATURE VALUES
C  INPUT : (R*8)  TVAL()  = ELECTRON TEMPERATUIRES (UNITS: EV)
C                           DIMENSION: ELECTRON TEMPERATURE INDEX
C  INPUT : (C*80) DLPATH  = PATH NAME TO THE RELEVANT DATA FILES
C                           (PASSED THROUGH TO CAFILE TO BUILD FILENAME)
C
C  OUTPUT: (R*8)  BWNO    = INPUT FILE - SELECTED DATA-BLOCK:
C                           EFFECTIVE IONIZATION POTENTIAL (cm-1).
C  OUTPUT: (I*4)  IZ      = INPUT FILE - SELECTED DATA BLOCK:
C                           IONIZING ION - INITIAL CHARGE
C  OUTPUT: (I*4)  IZ1     = INPUT FILE - SELECTED DATA BLOCK:
C                           IONIZING ION - FINAL   CHARGE
C
C  OUTPUT: (I*4)  METI    = INPUT FILE - SELECTED DATA-BLOCK:
C                           INITIAL STATE METSTABLE INDEX
C  OUTPUT: (I*4)  METF    = INPUT FILE - SELECTED DATA-BLOCK:
C                           FINAL   STATE METSTABLE INDEX
C
C  OUTPUT: (R*8)  SZDA()  = ZERO-DENSITY IONIZATION RATE-COEFFICIENTS
C                           DIMENSION: ELECTRON TEMPERATURE INDEX
C  OUTPUT: (L*4)  LTRNG() =.TRUE.  => OUTPUT 'SZDA()'  VALUE WAS INTER-
C                                     POLATED  FOR  THE  USER  ENTERED
C                                     ELECTRON TEMPERATURE 'TVAL()'.
C                          .FALSE. => OUTPUT 'SZDA()'  VALUE WAS EXTRA-
C                                     POLATED  FOR  THE  USER  ENTERED
C                                     ELECTRON TEMPERATURE 'TVAL()'.
C                           DIMENSION: ELECTRON TEMPERATURE INDEX
C
C  OUTPUT: (C*80) TITLX   = INFORMATION STRING (DSN ETC.)
C  OUTPUT: (I*4)  IRCODE  = RETURN CODE FROM SUBROUTINE:
C                           0 => NORMAL COMPLETION - NO ERROR DETECTED
C                           1 => DATA SET MEMBER FOR IONIZING ION WITH
C                                NUCLEAR CHARGE  'IZ0IN'  CAN  NOT  BE
C                                FOUND/DOES NOT EXIST.
C                           2 => DISCREPANCY BETWEEN REQUESTED CHARGES
C                                AND THOSE IN INPUT FILE.
C                           3 => THE SELECTED DATA-BLOCK 'IBSEL' IS OUT
C                                OF RANGE OR DOES NOT EXIST.
C                           4 => INVALID VALUE FOR 'IZ0IN' ENTERED.
C                                ('IZ0MIN' <= 'IZ0IN' <= 'IZ0MAX')
C                           9 => ERROR ENCOUNTERED WHEN TRYING TO OPEN
C                                INPUT DATA-SET.
C
C          (I*4)  NSTORE  = PARAMETER= MAXIMUM NUMBER  OF  DATA-BLOCKS
C                                      WHICH CAN BE READ FROM THE INPUT
C                                      DATA-SET.
C          (I*4)  NTDIM   = PARAMETER= MAXIMUM NUMBER OF ELECTRON TEMP-
C                                      ERATURES THAT CAN BE READ  FROM
C                                      AN INPUT DATA-SET DATA-BLOCK.
C          (I*4)  IZ0MIN  = PARAMETER: MIN. ALLOWED VALUE FOR 'IZ0IN'
C          (I*4)  IZ0MAX  = PARAMETER: MAX. ALLOWED VALUE FOR 'IZ0IN'
C
C          (I*4)  IZ0LST  = LAST VALUE OF 'IZ0IN' FOR  WHICH  INPUT
C                           DATA WAS READ.
C          (I*4)  IUNIT   = UNIT TO WHICH INPUT DATA SET IS ALLOCATED
C          (I*4)  NBSEL   = TOTAL NUMBER OF DATA-BLOCKS READ FROM INPUT
C                           DATA SET.
C          (I*4)  IZ0     = INPUT FILE - EMITTING ION - NUCLEAR CHARGE
C
C          (L*4)  LOPEN   = .TRUE.  => INPUT DATA SET OPEN.
C                           .FALSE. => INPUT DATA SET CLOSED.
C
C          (C*2)  ESYM    = INPUT FILE - IONIZING ION - ELEMENT SYMBOL
C          (C*3)  EXTIN   = CURRENT ADAS SOURCE DATA FILE EXTENSION
C          (C*3)  EXTLST  = ADAS SOURCE DATA FILE EXT. USED LAST TIME
C                           DATA WAS READ.
C          (C*6)  UIDIN   = CURRENT ADAS SOURCE DATA USER ID.
CX
CA         (C*80) UIDIN   = CURRENT ADAS SOURCE DATA FILE PATH
CA         (C*80) UIDLST  = ADAS SOURCE DATA FILE PATH USED LAST TIME
C                           DATA WAS READ.
C          (C*8)  GRPIN   = CURRENT ADAS SOURCE DATA GROUPNAME
C          (C*8)  GRPLST  = ADAS SOURCE DATA GROUPNAME USED LAST TIME
C                           DATA WAS READ.
CX         (C*6)  TYPIN   = CURRENT ADAS SOURCE DATA TYPENAME
CX         (C*6)  TYPLST  = ADAS SOURCE DATA TYPENAME USED LAST TIME
CA         (C*80) TYPIN   = CURRENT ADAS FILE SUBDIRECTORY( OPTIONAL)
CA         (C*80) TYPLST  = ADAS FILE SUBDIRECTORY USED LAST TIME (OPT)
C                           DATA WAS READ.
CX         (C*44) DSNREQ  = FULL MVS NAME OF DATA SET REQUESTED
CA	   (C*80) DSNREQ  = DATAFILE NAME UNDER UNIX INCLUDING PATH
C                           (MAY OR MAY NOT EXIST)
CX         (C*44) DSNAME  = FULL MVS NAME OF DATA SET INTERROGATED
CA         (C*80) DSNAME  = DATAFILE NAME UNDER UNIX INCLUDING PATH
C
C          (I*4)  ISELA() = INPUT DATA FILE: DATA-BLOCK ENTRY INDICES.
C                           DIMENSION: DATA-BLOCK INDEX
C          (I*4)  ITA()   = INPUT DATA SET-NUMBER OF ELECTRON TEMPERA-
C                           TURES.
C                           DIMENSION: DATA-BLOCK INDEX
C          (I*4)  IZOUT() = INPUT DATA FILE: IONIZING ION INITIAL CHARGE
C                           DIMENSION: DATA-BLOCK INDEX
C          (I*4)  IZ1OUT()= INPUT DATA FILE: IONIZING ION FINAL   CHARGE
C                           DIMENSION: DATA-BLOCK INDEX
C
C          (R*8)  BWNOUT()= INPUT DATA FILE: EFFECTIVE IONIZATION POT.
C                           (UNITS: cm-1).
C                           DIMENSION: DATA-BLOCK INDEX
C          (R*8)  TETA(,) = INPUT DATA SET -
C                           ELECTRON TEMPERATURES (UNITS: eV)
C                           1st DIMENSION: ELECTRON TEMPERATURE INDEX
C                           2nd DIMENSION: DATA-BLOCK INDEX
C          (R*8)  SZD(,)    =INPUT DATA SET -
C                            FULL SET OF IONIZATIONS RATE-COEFFICIENTS
C                            1st DIMENSION: ELECTRON TEMPERATURE INDEX
C                            3rd DIMENSION: DATA-BLOCK INDEX
C
C          (C*2)  CICODE()= INPUT DATA FILE - INITIAL STATE META. INDEX
C                           DIMENSION: DATA-BLOCK INDEX
C          (C*2)  CFCODE()= INPUT DATA FILE - FINAL   STATE META. INDEX
C                           DIMENSION: DATA-BLOCK INDEX
C          (C*6)  CIION() = INPUT DATA FILE - INITIAL ION
C                           DIMENSION: DATA-BLOCK INDEX
C          (C*6)  CFION() = INPUT DATA FILE - FINAL   ION
C                           DIMENSION: DATA-BLOCK INDEX
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          CAFILE     ADAS      OPEN DATA SET FOR SELECTED ELEMENT
C          XXDATA_07  ADAS      FETCH INPUT DATA FROM SELECTED DATA SET
C          CACHKB     ADAS      CHECK VALIDITY OF ELEMENT AND 'IBSEL'
C          E2SPLN     ADAS      INTERPOLATE DATA WITH ONE-WAY SPLINES
C          E2TITL     ADAS      CREATE DESCRIPTIVE TITLE FOR OUTPUT
C          XXUID      ADAS      FETCHES/SETS ADAS SOURCE DATA USER ID
C          XXSPEC     ADAS      FETCHES/SETS ADAS SOURCE DATA FILE NAME+
C
C AUTHOR:  PAUL E. BRIDEN (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/37
C          JET EXT. 6023
C
C DATE:    07/06/91
C
C UPDATE:  06/12/91 - PE BRIDEN: 'NSTORE' INCREASED FROM 10 TO 100
C
C UPDATE:  28/02/92 - PE BRIDEN: 'NSTORE' INCREASED FROM 100 TO 160
C
C UPDATE:  10/03/93 - PE BRIDEN: INTRODUCED CALL TO XXUID TO ESTABLISH
C                                IF USERID OF INPUT DATASET CHANGES
C                                BETWEEN CALLS.
C                                SAVE NAME OF LAST READ DATASET.
C                                (ADDED VARIABLES UIDIN,UIDLST,DSNREQ)
C
C UPDATE:   2/09/93 - HPS      : INTRODUCED CALL TO XXSSZD TO ESTABLISH
C                                IF USRGRP, USRTYP AND USREXT OF INPUT
C                                DATASET CHANGES BETWEEN CALLS.
C                                SAVE NAME OF LAST READ DATASET.
C                                (ADDED VARIABLES GRPIN,GRPLST,TYPIN,
C                                 TYPLST, EXTIN, EXTLST)
C
C UPDATE:   10/11/94 - L. JALOTA: MODIFIED TO RUN UNDER UNIX, SIZE OF
C				  DSNAME AND DSNREQ INCREASED TO 80 
C				  CHARACTERS
C
C UPDATE:   21/11/94 - L/ JALOTA: TIDIED UP CHARACTER LENGTHS.
C
C UNIX-IDL PORT:
C
C
C VERSION: 1.1                          DATE: 25-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - CREATED FROM SSZD.FOR WITH SPECIFIC USE IN ADAS 310
C                 AS GOAL. ALL FUNCTIONALITY IS MAINTAINED, BUT
C                 ROUTINE CALLS A NEW FILE CAFILE.FOR TO OPEN
C                 THE REQUESTED FILE IF IT IS AVAILABLE, THIS
C                 HAS ALSO NECESSITATED BRINGING IN THE VARIABLE
C                 DLPATH WHICH HOLDS THE PATH TO THE DATA FILES.
C
C
C VERSION : 1.2
C DATE    : 20-10-2003
C MODIFIED: Martin O'Mullane
C           - Extend TITLX to 120 to match e2titl routine.
C           - Save essential variable between calls.
C
C VERSION : 1.3
C DATE    : 17-05-2007
C MODIFIED: Allan Whiteford
C           - Updated comments as part of subroutine documentation
C             procedure.
C
C VERSION : 1.4
C DATE    : 26-03-2008
C MODIFIED: Allan Whiteford
C           - Changed call from E2DATA to XXDATA_07
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER     NSTORE         , NTDIM
      INTEGER     IZ0MIN         , IZ0MAX
C-----------------------------------------------------------------------
      PARAMETER(  NSTORE = 160   , NTDIM  = 24       )
      PARAMETER(  IZ0MIN =   1   , IZ0MAX = 60       )
C-----------------------------------------------------------------------
      INTEGER     IBSEL          , IZ0IN             , ITVAL       ,
     &            IZ             , IZ1               ,
     &            METI           , METF              ,
     &            IRCODE
      INTEGER     IZ0LST         , IUNIT             , NBSEL       ,
     &            IZ0
C-----------------------------------------------------------------------
      REAL*8      BWNO
C-----------------------------------------------------------------------
      LOGICAL     LOPEN
C-----------------------------------------------------------------------
      CHARACTER   ESYM*2         , EXTIN*3           , EXTLST*3     ,
     &                             UIDIN*80          , UIDLST*80    ,
     &                             GRPIN*8           , GRPLST*8     ,
     &                             TYPIN*80          , TYPLST*80    ,
     &            DSNREQ*80      , DSNAME*80         , TITLX*120    ,
     &            DLPATH*80
C-----------------------------------------------------------------------
      INTEGER     ISELA(NSTORE)             , ITA(NSTORE)          ,
     &            IZOUT(NSTORE)             , IZ1OUT(NSTORE)
C-----------------------------------------------------------------------
      REAL*8      TVAL(ITVAL)               , SZDA(ITVAL)          ,
     &            BWNOUT(NSTORE)
C-----------------------------------------------------------------------
      LOGICAL     LTRNG(ITVAL)
C-----------------------------------------------------------------------
      CHARACTER   CICODE(NSTORE)*2          , CFCODE(NSTORE)*2     ,
     &            CIION(NSTORE)*6           , CFION(NSTORE)*6
C-----------------------------------------------------------------------
      REAL*8      TETA(NTDIM,NSTORE)        , SZD(NTDIM,NSTORE)
C-----------------------------------------------------------------------
      SAVE        DSNAME      , UIDLST     , GRPLST    , TYPLST    ,
     &            EXTLST      , IZ0LST     ,
     &            IUNIT
      save        ESYM        , IZ0        ,
     &            NBSEL       , ISELA      ,
     &            IZOUT       , IZ1OUT     ,
     &            CICODE      , CFCODE     , CIION     , CFION     ,
     &            BWNOUT      , ITA        , TETA      , SZD
C-----------------------------------------------------------------------
      DATA        DSNAME /' '/   , UIDLST /' '/ , GRPLST /' '/     ,
     &            TYPLST /' '/   ,
     &            EXTLST /' '/   , IZ0LST /0/   ,
     &            IUNIT  /16/
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C
      IRCODE = 0
      LOPEN  = .FALSE.
C
C-----------------------------------------------------------------------
C ESTABLISH CURRENT ADAS SOURCE DATA UID, FILE AND FILE EXTENSION
C-----------------------------------------------------------------------
C
      UIDIN  = '?'
      CALL XXUID(UIDIN)
C
      GRPIN  = '?'
      TYPIN  = '?'
      EXTIN  = '?'
      CALL XXSSZD(GRPIN , TYPIN , EXTIN)
C
C-----------------------------------------------------------------------
C
         IF ( (IZ0IN.GE.IZ0MIN) .AND. (IZ0IN.LE.IZ0MAX) ) THEN
C
C-----------------------------------------------------------------------
C IF NEW ELEMENT ION CHARGE ENTERED OR SOURCE DATA USERID HAS CHANGED:
C - OPEN THE REQUESTED DATA SET & READ IN COMPLETE SET OF RATE DATA.
C-----------------------------------------------------------------------
C
               IF ( (IZ0LST.NE.IZ0IN) .OR.
     &              (UIDLST.NE.UIDIN) .OR.
     &              (GRPLST.NE.GRPIN) .OR.
     &              (TYPLST.NE.TYPIN) .OR.
     &              (EXTLST.NE.EXTIN)      )  THEN
C
                  CALL CAFILE(IUNIT, IZ0IN, IRCODE, DSNREQ, DLPATH)
                     IF (IRCODE.EQ.0) THEN
                        LOPEN  = .TRUE.
                        IZ0LST = IZ0IN
                        UIDLST = UIDIN
                        GRPLST = GRPIN
                        TYPLST = TYPIN
                        EXTLST = EXTIN
                        DSNAME = DSNREQ
                        CALL XXDATA_07( IUNIT  , DSNAME ,
     &                                  NSTORE , NTDIM  ,
     &                                  ESYM   , IZ0    ,
     &                                  NBSEL  , ISELA  ,
     &                                  IZOUT  , IZ1OUT ,
     &                                  CICODE , CFCODE ,
     &                                  CIION  , CFION  ,
     &                                  BWNOUT ,
     &                                  ITA    ,
     &                                  TETA   , SZD
     &                                )
                     ENDIF
               ENDIF
C
C-----------------------------------------------------------------------
C CHECK VALIDITY OF 'IBSEL' AND ENTERED ELEMENT NUCLEAR CHARGE VALUE.
C-----------------------------------------------------------------------
C
               IF ( IRCODE.EQ.0 ) THEN
                  CALL CACHKB( IUNIT , NBSEL  , IBSEL ,
     &                         IZ0IN , IZ0    ,
     &                         LOPEN , IRCODE , DLPATH
     &                       )
               ENDIF
C
C-----------------------------------------------------------------------
C 1) INTERPOLATE/EXTRAPOLATE WITH ONE DIMENSIONAL SPLINE.
C 2) CREATE TITLE FOR OUTPUT.
C-----------------------------------------------------------------------
C
               IF ( IRCODE.EQ.0 ) THEN
                  CALL E2SPLN( ITA(IBSEL)        , ITVAL         ,
     &                         BWNOUT(IBSEL)     ,
     &                         TETA(1,IBSEL)     , TVAL          ,
     &                         SZD(1,IBSEL)      , SZDA          ,
     &                         LTRNG
     &                       )
                  CALL E2TITL( IBSEL         , DSNAME        ,
     &                         CICODE(IBSEL) , CFCODE(IBSEL) ,
     &                         CIION(IBSEL)  , CFION(IBSEL)  ,
     &                         TITLX
     &                       )
C
                  BWNO = BWNOUT(IBSEL)
                  IZ   = IZOUT(IBSEL)
                  IZ1  = IZ1OUT(IBSEL)
                  READ(CICODE(IBSEL),*) METI
                  READ(CFCODE(IBSEL),*) METF
C
               ENDIF
C
         ELSE
            IRCODE = 4
         ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN
      END
