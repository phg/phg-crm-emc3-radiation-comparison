CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas310/abinew.for,v 1.2 2007/05/17 17:03:02 allan Exp $ Date $Date: 2007/05/17 17:03:02 $
CX
       FUNCTION ABINEW(OA,YA,FA,A,B,N)                                 
C
       IMPLICIT REAL*8(A-H,O-Z)                                       
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 FUNCTION: ABINEW ***********************
C
C-----------------------------------------------------------------------
C  PURPOSE: EVALUATE INNER INTEGRALS FOR RATE COEFFICIENTS.
C
C  ABINEW IS CALLED BY QH & QHE.  THE PROCEDURES ALLOW FOR PROJECTILE
C  AND TARGET  MASSES.
C
C  INPUT                                                            
C      OA=VECTOR OF CROSS-SECTIONS (CM2)                             
C      YA=VECTOR OF REDUCED RELATIVE SPEEDS. 1ST VALUE IS AT THRESHOLD
C      FA=VECTOR OF ALPHAS  (12TH VALUE IS FOR EXTRAPOLATION, PROVIDED 
C         EXPLICITLY. EXTRAPOLATION BELOW YA(1) IS BASED ON FA(1))   
C         N.B. OA(1) MUST BE NON-ZERO.                              
C             (OA,YA AND FA ARE OF FIXED LENGTH =24)               
C                                                                 
C      A=LOWER INTEGRAL LIMIT                                    
C      B=UPPER INTEGRAL LIMIT                                   
C      N=NUMBER OF CROSS-SECTIONS
C  OUTPUT                      
C      ABINEW=DEFINITE INTEGRAL.                                        
C
C  ********** H.P. SUMMERS,JET            18 FEB 1987  ****************
C  **********                        COR. 31 JUL 1990  **************** 
C  **********                             15 JUL 1991  REORDER EVAL AT 
C                                                      LABEL 50
C
C NOTES: THIS ROUTINE IS NOT YET PROPERLY ANNOTATED
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 16-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C VERSION: 1.2				DATE: 16-05-07
C MODIFIED: Allan Whiteford
C	    - Updated comments as part of subroutine documentation
C             procedure.
C
C-----------------------------------------------------------------------
       DIMENSION OA(24),YA(24),FA(24)                          
       I=0                                                    
   10  I=I+1                                                 
       IF(I.EQ.N+1)GO TO 15                                 
       IF(A.GT.YA(I))GO TO 10                              
   15  I=I-1                                              
       IF(I.EQ.0)I=1                                     
       J=0                                              
   20  J=J+1                                           
       IF(J.EQ.N+1)GO TO 25                           
       IF(B.GT.YA(J))GO TO 20                        
   25  J=J-1                                                    
       IF(J.EQ.0)J=1                                             
       IF(I.EQ.J)GO TO 50                                         
       T1=(OA(I+1)*YA(I+1)**3-OA(I)*(YA(I)/A)**(2.0D0*FA(I))*A**3)/
     &(3.0D0-2.0D0*FA(I))                                         
       T2=(OA(J)*(YA(J)/B)**(2.0D0*FA(J))*B**3-OA(J)*YA(J)**3)/    
     &(3.0D0-2.0D0*FA(J))                                           
       T3=0.0D0                                                      
       IF(I.GT.J-2)GO TO 45                                           
       DO 40 K=I+1,J-1                                   
   40  T3=T3+(OA(K+1)*YA(K+1)**3-OA(K)*YA(K)**3)/(3.0D0-2.0D0*FA(K))   
   45  ABINEW=T1+T2+T3                                    
C      WRITE(6,1000)I,J,A,B,ABINEW
C      WRITE(7,1000)I,J,A,B,ABINEW
       RETURN                                              
   50  ABINEW=OA(I)*(B**3.0D0*(YA(I)/B)**(2.0D0*FA(I))-     
     &A**3.0D0*(YA(I)/A)**(2.0D0*FA(I)))/(3.0D0-2.0D0*FA(I)) 
C      WRITE(6,1000)I,J,A,B,ABINEW
C      WRITE(7,1000)I,J,A,B,ABINEW
       RETURN                                               
C1000  FORMAT(1H ,'I=',I3,5X,'J=',I3,5X,'A=',1PD12.3,5X,'B=',1PD12.3,
C    & 5X,'ABINEW=',1PD12.3)
       END                                                   
