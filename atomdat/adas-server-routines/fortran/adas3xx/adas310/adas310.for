CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas310/adas310.for,v 1.2 2004/07/06 10:34:17 whitefor Exp $ Date $Date: 2004/07/06 10:34:17 $
CX
      PROGRAM ADAS310
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 PROGRAM: ADAS310 **********************
C
C  ORIGINAL NAME: V2B1MOD
C
C  VERSION:  (ADAS91) - SEE SC CS COMMENTS FOR VERSION NO.
C
C-----------------------------------------------------------------------
C  UNIX PORT - IMPORTANT NOTE:
C            IN THE FOLLOWING COMMENTS AND THOSE FOUND IN OTHER 
C            ROUTINES, REFERENCE IS MADE TO THE ABILITY TO RUN
C            THE ADAS 310 PROGRAM EITHER INTERACTIVELY OR IN A
C            BATCH MODE. NOTE THAT THE BATCH OPTION HAS NOT YET BEEN
C            CODED AS IT WAS FELT THAT THE INTERACTIVE VERSION
C            WOULD PROVE SUFFICIENT IN ALMOST ALL CASES.
C                     - TIM HAMMOND 08-02-96
C-----------------------------------------------------------------------
C
C  PURPOSE:  CODE TO PRODUCE THE BEAM STOPPING COEFFICIENT FILES.
C            THIS PROGRAM HAS BEEN CONSIDERABLY REWORKED FOR THE IDL
C            ADAS CONVERSION. THIS IS AN ENTIRELY NEW ROUTINE WHICH
C            SITS ABOVE THE PREVIOUS MAIN FORTRAN ROUTINE AND 
C            CONTROLS HOW IT IS CALLED. 
C            ONCE THIS FORTRAN PROGRAM HAS BEEN SPAWNED BY THE IDL
C            CONTROLLING ROUTINE adas310.pro IT IMMEDIATELY READS
C            FROM THE IDL WHETHER IT IS RUNNING IN INTERACTIVE OR
C            BATCH MODE. THIS DEPENDS ON THE VALUE OF THE PARAMETER
C            LBATCH:
C		.FALSE. - PROGRAM IS TO BE RUN INTERACTIVELY, AND
C                         SO CALCULATION AND OUTPUT OF DATA IS 
C			  PERFORMED WHILST THE USER WAITS. THIS 
C                         INVOLVES A CONSIDERABLE AMOUNT OF PIPE
C                         COMMUNICATIONS WITH THE IDL HANDLED BY
C                         THE ROUTINES CASPF2.FOR AND caspf2.pro.
C			  IN THIS CASE THE TOP LEVEL ROUTINE
C                         CALLED IS RUN310.FOR
C		.TRUE.  - PROGRAM IS RUNNING IN BATCH. THE APPROPRIATE
C                         BATCH COMMAND AND DATA FILES ARE CONSTRUCTED
C                         ENTIRELY BY THE IDL AND NO PIPE COMMUNICATIONS
C                         ARE NEEDED. IN THIS CASE ALL DATA USED BY THE
C  			  PROGRAM COMES FROM FILE RATHER THAN VIA A 
C         		  PIPE FROM THE IDL. THIS BATCH VERSION OF THE
C   			  PROGRAM IS SET TO RUN AT A SHORT TIME AFTER
C			  ITS INITIATION USING THE UNIX 'at' COMMAND.
C 
C            NOTE THAT MANY OF THE SUBROUTINES ASSOCIATED WITH ADAS310
C            HAVE YET TO BE UPGRADED INTO THEIR FINAL FORMS AND MAY
C            NOT BE AS WELL ANNOTATED AS WOULD BE DESIRABLE.
C
C  DATA   :  THE PROGRAM READS FROM AND WRITES TO A NUMBER OF DATA SETS.
C            THESE ARE LISTED TOGETHER WITH THEIR FORTAN UNIT NUMBERS.
C            NOTE THAT THE FORTRAN FILE-NAMING CONVENTIONS NO LONGER
C            APPLY BUT ARE KEPT FOR POSSIBLE FUTURE REFERENCE.
C
C            INPUT:
C
C            UNIT  DATA SET NAME           CONTENT               ROUTINE
C            ----  ----------------------  -------------------   -------
C              5   THROUGH UNIX PIPE       PRIMARY INPUT DATA
C                                          (FROM IDL SCREENS)
C              9   JETSHP.<SE>LIKE.DATA    LOW LEVEL DATA        LOWPOP
C             11   JETSHP.QCX#<BEAM>.DATA  CHARGE EXCHANGE DATA  BNQCTB
C             15   JETSHP.HLIKE.DATA       ELECTRON IMPACT DATA  NSUPH1
C                                          FOR LOW LEVELS
C             15   JETSHP.IONATOM.DATA(H)  ION IMPACT DATA       QH
C                                          FOR LOW LEVELS
C             17   USERID.BNDLEN.DATA      EXPANSION FILE        CLDLBN2
C             51   SCRATCH FILE            INPUT PARAMETERS      V2BNMOD
C
C            OUTPUT:
C
C            UNIT  DATA SET NAME           CONTENT               ROUTINE
C            ----  ----------------------  -------------------   -------
C              6   THROUGH UNIX PIPE       PRIMARY OUTPUT DATA
C                                          (TO IDL)
C              7   USER SPECIFIED          FIRST  PASSING FILE   ADAS310
C              10  USER SPECIFIED          SECOND PASSING FILE   GENTAB
C              18  USER SPECIFIED          THIRD  PASSING FILE   CLDLBN2
C              19                          DIAGNOSTIC DATA       LOWPOP
C              62  USER SPECIFIED          FOURTH PASSING FILE   CMPRSS
C
C  PROGRAM:
C
C          (L*4)  LBATCH    = FLAG FOR BATCH JOB OR NOT (TRUE = BATCH)
C
C          (I*4)   PIPEIN   = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)   PIPEOU   = PARAMETER = UNIT NUMBER FOR INPUT FROM PIPE
C          (I*4)   ONE      = PARAMETER = 1  : USED AS FLAG TO IDL
C          (I*4)   ZERO     = PARAMETER = 0  : USED AS FLAG TO IDL
C
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          XX0000     ADAS      SETS MACHINE DEPENDENT ADAS
C                               CONFIGURATION.
C	   RUN310     IDL_ADAS  RUNS AN INTERACTIVE VERSION OF ADAS310.
C          BATCH310   IDL_ADAS  RUNS A BATCH VERSION OF ADAS310.
C
C UNIX-IDL PORT:
C
C AUTHOR:  TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C
C DATE:    12TH JANUARY 1996
C
C VERSION: 1.1                          DATE: 12-01-96
C MODIFIED: TIM HAMMOND 
C               - FIRST VERSION
C
C VERSION: 1.2                          DATE: 02-04-96
C MODIFIED: TIM HAMMOND 
C               - CHANGED LOGICAL STATEMENT OF FORM VARIABLE.EQ.TRUE.
C                 TO JUST VARIABLE OR .NOT.VARIABLE AS REQUIRED. THIS
C                 STOPS A COMPILER WARNING UNDER AIX.
C
C-----------------------------------------------------------------------
      INTEGER    LOGIC
      INTEGER    PIPEIN  , PIPEOU  , ONE     , ZERO
      PARAMETER( PIPEIN=5, PIPEOU=6, ONE=1   , ZERO=0)
C-----------------------------------------------------------------------
      LOGICAL    LBATCH  
C-----------------------------------------------------------------------
C *************************** MAIN PROGRAM ****************************
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C SET MACHINE DEPENDENT ADAS CONFIGURATION VALUES.
C-----------------------------------------------------------------------
C
      CALL XX0000
C
C-----------------------------------------------------------------------
C GET PARAMETER LBATCH (IS THIS A BATCH RUN OR NOT?) FROM IDL OR FILE
C-----------------------------------------------------------------------
C
      READ( PIPEIN, *) LOGIC
      IF (LOGIC.EQ.0) THEN
          LBATCH = .FALSE.
      ELSE
          LBATCH = .TRUE.
      ENDIF
C
C-----------------------------------------------------------------------
C
      IF (LBATCH) THEN
c         CALL BATCH310
      ELSE
          CALL RUN310
      ENDIF
C
C-----------------------------------------------------------------------
C
      STOP

      END
