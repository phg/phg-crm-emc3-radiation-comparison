C SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas310/nsuph1.for,v 1.18 2013/09/29 13:11:59 mog Exp $ Date $Date: 2013/09/29 13:11:59 $
CX
       SUBROUTINE NSUPH1(TEV,EBEAM,TIEV,NIMP ,ZIMPA ,FRIMPA,AMIMPA,
     &                   ITYP1 ,ITYP2 ,ITYP3 ,ITYP4 ,ITYP5 ,ITYP6 ,
     &                   XTBE  ,XTBP  ,XTBZ  ,STBE  ,STBP  ,STBZ  ,
     &                   LXTBE ,LXTBP ,LXTBZ ,LSTBE ,LSTBP ,LSTBZ ,
     &                   PXTBE ,PXTBP ,PXTBZ ,PSTBE ,PSTBP ,PSTBZ ,
     &                   LPXTBE,LPXTBP,LPXTBZ,LPSTBE,LPSTBP,LPSTBZ,
     &                   DSLPATH, lnoion, lnocx)
C
       IMPLICIT REAL*8(A-H,O-Z)
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: NSUPH1 *********************
C-----------------------------------------------------------------------
C  PURPOSE: ACCESS SPECIFIC HIGHER QUALITY DATA FOR HYDROGEN
C
C  POPULATION STRUCTURE CALCULATION IN THE BUNDLE-N APPROXIMATION.
C
C  DATA TYPES ARE:
C
C         (1) ELECTRON IMPACT EXCITATION - SPECIFIC ION FILE IS OPENED.
C         (2) ELECTRON IMPACT IONISATION - SPECIFIC FIT IS USED.
C         (3) H+  IMPACT EXCITATION      - QHIEXDAT FILE IS OPENED.
C         (4) H+  IMPACT IONIS + CX      - QHIEXDAT FILE IS OPENED.
C         (5) ZIMP ION IMPACT EXCITATION - QHIEXDAT FILE IS OPENED.
C         (6) ZIMP ION IMPACT IONIS + CX - QHIEXDAT FILE IS OPENED.
C
C  INPUT
C      TEV      = ELECTRON TEMPERATURE (EV)
C      EBEAM    = BEAM ENERGY (EV/AMU)  USED AS A UNIFORM VELOCITY SHIFT
C                 FOR ION COLLISIONS
C      TIEV     = ION TEMPERATURE (EV)
C      NIMP     = NUMBER OF IMPURITY IONS (EXCLUDING H+)
C      ZIMPA()  = Z OF EFFECTIVE IMPURITY FOR ION COLLISIONS(EXCEPT H+)
C      FRIMPA() = FRACTION OF TOTAL IMPURITY NUMBER DENSITY (EXCL H+)
C      AMIMPA() = ATOMIC MASS NUMBER OF IMPURITY
C      ITYP1    = 0   DO NOT OBTAIN TYPE 1 DATA
C               = 1   OBTAIN TYPE 1 DATA
C      ITYP2    = 0   DO NOT OBTAIN TYPE 2 DATA
C               = 1   OBTAIN TYPE 2 DATA
C      ITYP3    = 0   DO NOT OBTAIN TYPE 3 DATA
C               = 1   OBTAIN TYPE 3 DATA
C      ITYP4    = 0   DO NOT OBTAIN TYPE 4 DATA
C               = 1   OBTAIN TYPE 1 DATA
C      ITYP5    = 0   DO NOT OBTAIN TYPE 5 DATA
C               = 1   OBTAIN TYPE 2 DATA
C      ITYP6    = 0   DO NOT OBTAIN TYPE 6 DATA
C               = 1   OBTAIN TYPE 3 DATA
C      DSLPATH  =     STRING CONTAINING PATH FOR INPUT FILE FOR UNIT 15
C      lnoion   = if .TRUE. do not include ion impact ionisation in type 4 and 6
C      lnocx    = if .TRUE. do not include charge exchange in type 4 and 6
C
C
C  OUTPUT
C      XTBE(N,N'')  = TYPE 1 RATE COEFFICIENT
C      XTBP(N,N'')  = TYPE 3 RATE COEFFICIENT
C      XTBZ(N,N'')  = TYPE 5 RATE COEFFICIENT
C      STBE(N)      = TYPE 2 RATE COEFFICIENT
C      STBP(N)      = TYPE 4 RATE COEFFICIENT
C      STBZ(N)      = TYPE 6 RATE COEFFICIENT
C      LXTBE(N,N'') = TYPE 1 MARKER  (0 =NO VALUE, 1=VALUE)
C      LXTBP(N,N'') = TYPE 3 MARKER
C      LXTBZ(N,N'') = TYPE 5 MARKER
C      LSTBE(N)     = TYPE 2 MARKER
C      LSTBP(N)     = TYPE 4 MARKER
C      LSTBZ(N)     = TYPE 6 MARKER
C      PXTBE(N)     = TYPE 1 PROJECTION MULTIPLIER
C      PXTBP(N)     = TYPE 3 PROJECTION MULTIPLIER
C      PXTBZ(N)     = TYPE 5 PROJECTION MULTIPLIER
C      PSTBE        = TYPE 2 PROJECTION MULTIPLIER
C      PSTBP        = TYPE 4 PROJECTION MULTIPLIER
C      PSTBZ        = TYPE 6 PROJECTION MULTIPLIER
C      LPXTBE(N)    = TYPE 1 PROJECTION MULTIPLIER USED ABOVE THIS N'
C      LPXTBP(N)    = TYPE 3 PROJECTION MULTIPLIER USED ABOVE THIS N'
C      LPXTBZ(N)    = TYPE 5 PROJECTION MULTIPLIER USED ABOVE THIS N'
C      LPSTBE       = TYPE 2 PROJECTION MULTIPLIER USED ABOBE THIS N
C      LPSTBP       = TYPE 4 PROJECTION MULTIPLIER USED ABOVE THIS N
C      LPSTBZ       = TYPE 6 PROJECTION MULTIPLIER USED ABOBE THIS N
C
C  ********** H.P. SUMMERS, JET                 9 MAY 1990   ***********
C  **********                                  20 JUL 1990   ***********
C  **********                                  13 AUG 1990   ***********
C  **********      NEW ELECTRON EXCIT. DATA    22 JAN 1991   ***********
C  **********      NEW ION IMPACT EXCIT. DATA   3 JUL 1991   ***********
C  **********      NEW ELEC. IMPACT ION. DATA   3 JUL 1991   ***********
C  **********      DATA EXTENSION BY ADDING     1 MAR 1992   ***********
C                  SOME INTERMEDIATE VALUES +
C                  ADDITION OF B, N, NE ION. +
C                  CHARGE EXCHANGE.
C  **********      MULTIPLE, SIMULTANEOUS      11 JAN 1994   ***********
C                  IMPURITY EXTENSION
C                  ERROR CORRECTED IN IMPURITY
C                  REDUCED MASSES
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C
C UPDATE:  19/01/94 - JONATHAN NASH - TESSELLA SUPPORT SERVICES PLC
C
C          THE FOLLOWING MODIFICATIONS HAVE BEEN MADE TO THE SUBROUTINE:
C
C          1) A PARAMETER FLAG HAS BEEN ADDED TO SWITCH ON/OFF
C             DIAGNOSTIC PRINTING (UNIT 6).
C
C  NOTES:  NO ATTEMPT HAS BEEN MADE TO RESTRUCTURE THE ROUTINE. RATHER
C          THE MINIMUM AMOUNT OF WORK TO INTEGRATE THE ROUTINE INTO
C          ADAS310 HAS BEEN COMPLETED.
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 16-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C VERSION: 1.2                          DATE: 18-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - ADDED VARIABLE DSLPATH AND CHANGED NAME OF INPUT FILE
C
C VERSION: 1.3                          DATE: 18-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - CORRECTED STRING HANDLING SYNTAX IN CONSTRUCTION OF
C                 DSNAME, COMMENTED OUT REFERENCES TO DEBUG LOGICAL
C                 VARIABLE AND INSERTED 'CALL' BEFORE XXSLEN.
C
C VERSION: 1.4                          DATE: 18-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - MODIFIED CONSTRUCTION OF DSNAME
C
C VERSION: 1.5                          DATE: 18-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - ADDED DSLPATH IN CALL TO QH.FOR
C
C VERSION: 1.6                          DATE: 22-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - REPLACED CALLS TO NAG ROUTINE E02BBF WITH ADAS ROUTINE
C                 DXNBBF
C
C VERSION: 1.7                          DATE: 23-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - REPLACED CALLS TO NAG ROUTINE E01BAF WITH ADAS ROUTINE
C                 DXNBAF
C
C VERSION: 1.8                          DATE: 08-02-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - REMOVED SUPERFLUOUS VARIABLES
C
C VERSION: 1.9                          DATE: 03-04-97
C MODIFIED: H.ANDERSON 
C		- ALTERED TO USE RESTRUCTURED ADF02 DATASET sia#h_rfm.dat
C
C VERSION: 1.10				DATE: 03/04/97
C MODIFIED: HARVEY ANDERSON.
C	    ALTERED TO USE NEW PREFERRED ADF02 DATASET sia#h_j97.dat
C
C VERSION: 1.11				DATE: 08-04-97
C MODIFIED: RICHARD MARTIN
C	    CHANGED NAME OF ADF02 FILE FROM sia#h_j97.dat TO 
C	    sia#h_j97#h.dat
C
C VERSION: 1.12				DATE: 23-02-99
C MODIFIED: HARVEY ANDERSON
C	    ADDED ADDITIONAL CODE TO ACCESS THE FUNDAMENTAL DATA
C	    FOR ARGON WHICH IS CONTAINED IN THE ADF02 TYPE FILE.
C
C
C VERSION : 1.13                        DATE: 20-10-2003
C MODIFIED: Martin O'Mullane
C           - Extend TITLX to 120 to match e2titl routine.
C
C VERSION: 1.14				DATE: 07-07-2004
C MODIFIED: Allan Whiteford
C           - Changed calls from DXNB{A,B}F TO XXNB{A,B}F
C
C VERSION: 1.15				DATE: 07-07-2004
C MODIFIED: Allan Whiteford
C           - Updated comments as part of subroutine documentation
C             procedure.
C
C VERSION : 1.16                        
C DATE    : 07-09-2009
C MODIFIED: Martin O'Mullane
C           - Add logicals, lnoion and lnocx, for model exploration
C             work.
C           - Use updated cross sections for ion impact ionisation
C             of excited n=2,3,4 and 5 levels of hydrogen.
C
C VERSION : 1.17                        
C DATE    : 18-02-2010
C MODIFIED: Martin O'Mullane
C           - Pass in as arguments lnoion and lnocx.
C           - For type 6 set stbz, rather than incorrectly setting stbp, 
C             when the lnoion and lnocx optirons are set.
C
C VERSION : 1.18               
C DATE    : 29-09-2013
C MODIFIED: Martin O'Mullane
C           - Define STRING as character*80 - for the g95 compiler.
C
C-----------------------------------------------------------------------
C
C  PARAM : (L*4)  DEBUG     = FLAGS DIAGNOSTIC PRINTING.
C                             .TRUE.  => PRINT DIAGNOSTICS.
C                             .FALSE. => DO NOT PRINT DIAGNOSTICS.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      LOGICAL    DEBUG
      PARAMETER( DEBUG = .FALSE. )
      PARAMETER (NCDIM=30,NDLOW=10)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
       CHARACTER DSNAME*80,TITLF*80,TITLX*120,DSLPATH*80,
     &           DSTEMP2*80, string*80
C
       LOGICAL LTRNG(1)
       logical lnoion, lnocx
C
       DIMENSION NA(NCDIM),N1A(NCDIM),ICODEA(NCDIM)
       DIMENSION COEFFA(NCDIM),COEFFB(NCDIM)
       DIMENSION EA(24),OA(24)
       DIMENSION TEA(8),TEALG(24),GAMA(24)
       DIMENSION XKA(28),C(28)
CX     DIMENSION WRK(160)
       DIMENSION WTS(24),B(24),A(28,4),DIAG(28)
       DIMENSION TVAL(1),SZDA(1)
       DIMENSION ZIMPA(10),FRIMPA(10),AMIMPA(10)
C
       DIMENSION XTBE(NDLOW,NDLOW),LXTBE(NDLOW,NDLOW)
       DIMENSION XTBP(NDLOW,NDLOW),LXTBP(NDLOW,NDLOW)
       DIMENSION XTBZ(NDLOW,NDLOW),LXTBZ(NDLOW,NDLOW)
       DIMENSION PXTBE(NDLOW),LPXTBE(NDLOW)
       DIMENSION PXTBP(NDLOW),LPXTBP(NDLOW)
       DIMENSION PXTBZ(NDLOW),LPXTBZ(NDLOW)
       DIMENSION STBE(NDLOW),LSTBE(NDLOW)
       DIMENSION STBP(NDLOW),LSTBP(NDLOW)
       DIMENSION STBZ(NDLOW),LSTBZ(NDLOW)
C
       DATA LCK,LWRK/28,160/
C
C-----------------------------------------------------------------------
C  ASSUME  DEUTERIUM   MASS=2
C          DEUTERONS   MASS=2
C          ZIMP IONS   MASS=2*ZIMP  UNLESS SPECIFIED ON ENTRY
C
C  COMPUTE CENTRE OF MASS ENERGIES FOR THRESHOLDS AND 'DETAILED BALANCE'
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------

       if (debug) write(0,*)'Entering nsupH1 : ', ITYP1 ,ITYP2 ,
     &                                            ITYP3 ,ITYP4 ,
     &                                            ITYP5 ,ITYP6

       AMSSRH=1.0D0
       ECMH=AMSSRH*EBEAM
C
       IPASS=0
       NCOEFF=0
       TE=11605.4D0*TEV
       ATE=157890.0D0/TE
       TELG=DLOG(TE)
CX     IF (DEBUG) WRITE(6,1002)' /JETSHP.IONATOM.DATA(H)'
C-----------------------------------------------------------------------
C  DATA TYPE (1): N=1-2, N=1-3, N=1-4, N=1-5, N=2-3, N=2-4, N=2-5
C                 N=3-4, N=3-5, N=4-5
C-----------------------------------------------------------------------
       CALL XXSLEN( DSLPATH, IFIRST, ILAST)
       IF(ITYP1.EQ.1)THEN
           IF(TE.LT.1.0D5)THEN
               DSTEMP2='/adf04/hlike/hlike_bn#l91h.dat' 
               DSNAME=DSLPATH(IFIRST:ILAST)//DSTEMP2
CX             DSNAME='/JETSHP.HLIKE.DATA(BN#L91H)'
           ELSE
               DSTEMP2='/adf04/hlike/hlike_bn#h91h.dat' 
               DSNAME=DSLPATH(IFIRST:ILAST)//DSTEMP2
CX             DSNAME='/JETSHP.HLIKE.DATA(BN#H91H)'
           ENDIF
           IF (DEBUG) WRITE(0,'(A)')DSNAME
CX         OPEN(UNIT=15,FILE=DSNAME,ACTION='READ')
           OPEN(UNIT=15,FILE=DSNAME,STATUS='UNKNOWN')
           NSKIP=7
           NTEMP=8
           READ(15,1000)(STRING,I=1,NSKIP)
           READ(15,1001)(TEA(I),I=1,NTEMP)
           DO 1 I=1,NTEMP
            TEALG(I)=DLOG(TEA(I))
            WTS(I)=1.0
    1      CONTINUE
C
    2      READ(15,1005)N1,N,(GAMA(I),I=1,NTEMP)
           IF(N1.GT.0) THEN
               NCOEFF=NCOEFF+1
               ICODEA(NCOEFF)=1
               NA(NCOEFF)=N
               N1A(NCOEFF)=N1
               DE=1.0D0/NA(NCOEFF)**2-1.0D0/N1A(NCOEFF)**2
               WN=2.0D0*NA(NCOEFF)**2
               WN1=2.0D0*N1A(NCOEFF)**2
               X=1.57890D5*DE/TE
               IF(TE.GE.TEA(NTEMP))THEN
                   GAM=GAMA(NTEMP)*EEI(X)/EEI(TE*X/TEA(NTEMP))
               ELSE
                   IFAIL=0
C
C------------------------------------------------------------------------
C UNIX PORT - REPLACED CALLS TO NAG ROUTINES E01BAF AND E02BBF WITH
C             APPROPRIATE ADAS DUPLICATE ROUTINES (SEE qh.for FOR
C             MORE INFORMATION ON DXNBAF
C------------------------------------------------------------------------
C
                   DO 176, I=3,NTEMP-2
176                    XKA(I+2) = TEALG(I)
C
CX                 CALL E01BAF(NTEMP,TEALG,GAMA,XKA,C,LCK,WRK,LWRK,IFAIL)
                   CALL XXNBAF(NTEMP,NTEMP+4,TEALG,GAMA,WTS,XKA,B,A,
     &                         DIAG,C,SS,IFAIL)
CX                 CALL E02BBF(NTEMP+4,XKA,C,TELG,GAM,IFAIL)
                   CALL XXNBBF(NTEMP+4,XKA,C,TELG,GAM,IFAIL)
               ENDIF
               LXTBE(N,N1)=1
               XTBE(N,N1)=2.17161D-8*DSQRT(X/DE)*DEXP(-X)*GAM/WN
               COEFFA(NCOEFF)=XTBE(N,N1)
C	       WRITE(0,*) 'XTBE(N,N1) IS ', N  ,   N1  , XTBE(N,N1)
               LXTBE(N1,N)=1
               XTBE(N1,N)=2.17161D-8*DSQRT(X/DE)*GAM/WN1
               COEFFB(NCOEFF)=XTBE(N1,N)
               GO TO 2
           ENDIF
           CLOSE(15)
       ENDIF
C-----------------------------------------------------------------------
C  DATA TYPE (2): N=1 ONLY
C-----------------------------------------------------------------------
       IF(ITYP2.EQ.1)THEN
C
           NCOEFF=NCOEFF+1
           ICODEA(NCOEFF)=2
           NA(NCOEFF)=1
           N1A(NCOEFF)=0
           N=1
           LPSTBE=N
           IBSEL=1
           IZ0IN=1
           ITVAL=1
           TVAL(1)=TEV
           CALL CASSZD(IBSEL , IZ0IN , ITVAL , TVAL  ,
     &                 BWNO  , IZ    , IZ1   , METI  , METF  ,
     &                 SZDA  , LTRNG ,
     &                 TITLX , IRCODE, DSLPATH)
           LSTBE(1)=1
           STBE(1)=SZDA(1)
           COEFFA(NCOEFF)=STBE(1)
           COEFFB(NCOEFF)=0.0D0
C
       ENDIF
C-----------------------------------------------------------------------
C  DATA TYPE (3): N=1-2, N=1-3, N=1-4, N=1-5
C-----------------------------------------------------------------------
       IF(ITYP3.EQ.1)THEN
C
           ISEL=21
           NCOEFF=NCOEFF+1
           ICODEA(NCOEFF)=3
           NA(NCOEFF)=1
           N1A(NCOEFF)=2
           DE=1.0D0/NA(NCOEFF)**2-1.0D0/N1A(NCOEFF)**2
           WN=2.0D0*NA(NCOEFF)**2
           WN1=2.0D0*N1A(NCOEFF)**2
           LXTBP(1,2)=1
           XTBP(1,2)=QH(EBEAM,TIEV,ISEL,ZSEL,NSEL,1,EA,OA,N,
     &     IPASS,TITLF,DSLPATH)
           COEFFA(NCOEFF)=XTBP(1,2)
           LXTBP(2,1)=1
           XTBP(2,1)=XTBP(1,2)*WN*DEXP(13.6048*DE/(TIEV+ECMH))/WN1
           COEFFB(NCOEFF)=XTBP(2,1)
C
           ISEL=22
           NCOEFF=NCOEFF+1
           ICODEA(NCOEFF)=3
           NA(NCOEFF)=1
           N1A(NCOEFF)=3
           DE=1.0D0/NA(NCOEFF)**2-1.0D0/N1A(NCOEFF)**2
           WN=2.0D0*NA(NCOEFF)**2
           WN1=2.0D0*N1A(NCOEFF)**2
           LXTBP(1,3)=1
           XTBP(1,3)=QH(EBEAM,TIEV,ISEL,ZSEL,NSEL,1,EA,OA,N,
     &     IPASS,TITLF,DSLPATH)
           COEFFA(NCOEFF)=XTBP(1,3)
           LXTBP(3,1)=1
           XTBP(3,1)=XTBP(1,3)*WN*DEXP(13.6048*DE/(TIEV+ECMH))/WN1
           COEFFB(NCOEFF)=XTBP(3,1)
C
           ISEL=23
           NCOEFF=NCOEFF+1
           ICODEA(NCOEFF)=3
           NA(NCOEFF)=1
           N1A(NCOEFF)=4
           DE=1.0D0/NA(NCOEFF)**2-1.0D0/N1A(NCOEFF)**2
           WN=2.0D0*NA(NCOEFF)**2
           WN1=2.0D0*N1A(NCOEFF)**2
           LXTBP(1,4)=1
           XTBP(1,4)=QH(EBEAM,TIEV,ISEL,ZSEL,NSEL,1,EA,OA,N,
     &     IPASS,TITLF,DSLPATH)
           COEFFA(NCOEFF)=XTBP(1,4)
           LXTBP(4,1)=1
           XTBP(4,1)=XTBP(1,4)*WN*DEXP(13.6048*DE/(TIEV+ECMH))/WN1
           COEFFB(NCOEFF)=XTBP(4,1)
C
           ISEL=24
           NCOEFF=NCOEFF+1
           ICODEA(NCOEFF)=3
           NA(NCOEFF)=1
           N1A(NCOEFF)=5
           DE=1.0D0/NA(NCOEFF)**2-1.0D0/N1A(NCOEFF)**2
           WN=2.0D0*NA(NCOEFF)**2
           WN1=2.0D0*N1A(NCOEFF)**2
           LXTBP(1,5)=1
           XTBP(1,5)=QH(EBEAM,TIEV,ISEL,ZSEL,NSEL,1,EA,OA,N,
     &     IPASS,TITLF,DSLPATH)
           COEFFA(NCOEFF)=XTBP(1,5)
           LXTBP(5,1)=1
           XTBP(5,1)=XTBP(1,5)*WN*DEXP(13.6048*DE/(TIEV+ECMH))/WN1
           COEFFB(NCOEFF)=XTBP(5,1)
       ENDIF
C-----------------------------------------------------------------------
C  DATA TYPE (4): N=1, N=2, N=3, N=4, N=5
C-----------------------------------------------------------------------
       IF(ITYP4.EQ.1)THEN
C
           ISEL=11
           T1=QH(EBEAM,TIEV,ISEL,ZIMP,NSEL,1,EA,OA,N,
     &     IPASS,TITLF,DSLPATH)
           ISEL=1
           T2=QH(EBEAM,TIEV,ISEL,ZIMP,NSEL,1,EA,OA,N,
     &     IPASS,TITLF,DSLPATH)
           NCOEFF=NCOEFF+1
           ICODEA(NCOEFF)=4
           NA(NCOEFF)=1
           N1A(NCOEFF)=0
           LSTBP(1)=1
           STBP(1)=T1+T2
           if (lnoion) stbp(1)=t2
           if (lnocx)  stbp(1)=t1 
           COEFFA(NCOEFF)=STBP(1)
C
           Z0=1
           NSEL=2
           ISEL=68
           T1=QH(EBEAM,TIEV,ISEL,Z0,NSEL,1,EA,OA,N,
     &     IPASS,TITLF,DSLPATH)
           IF (DEBUG) WRITE(0,*)'T1=',T1
           ISEL=57
           T2=QH(EBEAM,TIEV,ISEL,Z0,NSEL,1,EA,OA,N,
     &     IPASS,TITLF,DSLPATH)
           IF (DEBUG) WRITE(0,*)'T2=',T2
           NCOEFF=NCOEFF+1
           ICODEA(NCOEFF)=4
           NA(NCOEFF)=2
           N1A(NCOEFF)=0
           LSTBP(2)=1
           STBP(2)=T1+T2
           if (lnoion) stbp(2)=t2
           if (lnocx)  stbp(2)=t1 
           COEFFA(NCOEFF)=STBP(2)
C
           Z0=1
           NSEL=3
           ISEL=69
           T1=QH(EBEAM,TIEV,ISEL,Z0,NSEL,1,EA,OA,N,
     &     IPASS,TITLF,DSLPATH)
           ISEL=57
           T2=QH(EBEAM,TIEV,ISEL,Z0,NSEL,1,EA,OA,N,
     &     IPASS,TITLF,DSLPATH)
           NCOEFF=NCOEFF+1
           ICODEA(NCOEFF)=4
           NA(NCOEFF)=3
           N1A(NCOEFF)=0
           LSTBP(3)=1
           STBP(3)=T1+T2
           if (lnoion) stbp(3)=t2
           if (lnocx)  stbp(3)=t1 
           COEFFA(NCOEFF)=STBP(3)
C
           Z0=1
           NSEL=4
           ISEL=70
           T1=QH(EBEAM,TIEV,ISEL,Z0,NSEL,1,EA,OA,N,
     &     IPASS,TITLF,DSLPATH)
           ISEL=57
           T2=QH(EBEAM,TIEV,ISEL,Z0,NSEL,1,EA,OA,N,
     &     IPASS,TITLF,DSLPATH)
           NCOEFF=NCOEFF+1
           ICODEA(NCOEFF)=4
           NA(NCOEFF)=4
           N1A(NCOEFF)=0
           LSTBP(4)=1
           STBP(4)=T1+T2
           if (lnoion) stbp(4)=t2
           if (lnocx)  stbp(4)=t1 
           COEFFA(NCOEFF)=STBP(4)
C
           Z0=1
           NSEL=5
           ISEL=71
           T1=QH(EBEAM,TIEV,ISEL,Z0,NSEL,1,EA,OA,N,
     &     IPASS,TITLF,DSLPATH)
           ISEL=57
           T2=QH(EBEAM,TIEV,ISEL,Z0,NSEL,1,EA,OA,N,
     &     IPASS,TITLF,DSLPATH)
           NCOEFF=NCOEFF+1
           ICODEA(NCOEFF)=4
           NA(NCOEFF)=5
           N1A(NCOEFF)=0
           LPSTBP=5
           LSTBP(5)=1
           STBP(5)=T1+T2
           if (lnoion) stbp(5)=t2
           if (lnocx)  stbp(5)=t1 
           COEFFA(NCOEFF)=STBP(5)
       ENDIF
C-----------------------------------------------------------------------
C  DATA TYPE (5): N=1-2, N=1-3, N=1-4
C-----------------------------------------------------------------------
       IF(ITYP5.EQ.1.AND.NIMP.GT.0)THEN
C
           NCOEFF=NCOEFF+1
           ICODEA(NCOEFF)=5
           NA(NCOEFF)=1
           N1A(NCOEFF)=2
           DE=1.0D0/NA(NCOEFF)**2-1.0D0/N1A(NCOEFF)**2
           WN=2.0D0*NA(NCOEFF)**2
           WN1=2.0D0*N1A(NCOEFF)**2
           NSEL=2
           LXTBZ(1,2)=1
           LXTBZ(2,1)=1
           XTBZ(1,2)=0.0D0
           XTBZ(2,1)=0.0D0
           DO 51 IMP=1,NIMP
             IF(FRIMPA(IMP).EQ.0.0D0)GO TO 51
             ZIMP=ZIMPA(IMP)
             AMSSRZ=2.0D0*AMIMPA(IMP)/(AMIMPA(IMP)+2.0D0)
             ECMZ=AMSSRZ*EBEAM
             IF    (DABS(ZIMP-2.0D0).LE.0.2D0)THEN
                 IBSEL=25
             ELSEIF(DABS(ZIMP-3.0D0).LE.0.2D0)THEN
                 IBSEL=28
             ELSEIF(DABS(ZIMP-4.0D0).LE.0.2D0)THEN
             	 IBSEL=31
             ELSEIF(DABS(ZIMP-5.0D0).LE.0.2D0)THEN
                 IBSEL=34
             ELSEIF(DABS(ZIMP-6.0D0).LE.0.2D0)THEN
                 IBSEL=37
             ELSEIF(DABS(ZIMP-7.0D0).LE.0.2D0)THEN
                 IBSEL=40
             ELSEIF(DABS(ZIMP-8.0D0).LE.0.2D0)THEN
                 IBSEL=43
             ELSEIF(DABS(ZIMP-9.0D0).LE.0.2D0)THEN
                 IBSEL=46
             ELSEIF(DABS(ZIMP-10.0D0).LE.0.2D0)THEN
                 IBSEL=46
             ELSE
                 IBSEL=46
             ENDIF
            ISEL=IBSEL
            VAL12=FRIMPA(IMP)*QH(EBEAM,TIEV,ISEL,ZIMP,NSEL,1,EA,OA,N,
     &      IPASS,TITLF,DSLPATH)
            VAL21=VAL12*WN*DEXP(13.6048*DE/(TIEV+ECMZ))/WN1
C	    WRITE(0,*) ' EXCITATION RATE 1 2 IS', EBEAM   ,  VAL12
            XTBZ(1,2)=XTBZ(1,2)+VAL12
            XTBZ(2,1)=XTBZ(2,1)+VAL21
   51      CONTINUE
           COEFFA(NCOEFF)=XTBZ(1,2)
           COEFFB(NCOEFF)=XTBZ(2,1)
C
           NCOEFF=NCOEFF+1
           ICODEA(NCOEFF)=5
           NA(NCOEFF)=1
           N1A(NCOEFF)=3
           DE=1.0D0/NA(NCOEFF)**2-1.0D0/N1A(NCOEFF)**2
           WN=2.0D0*NA(NCOEFF)**2
           WN1=2.0D0*N1A(NCOEFF)**2
           NSEL=3
           LXTBZ(1,3)=1
           LXTBZ(3,1)=1
           XTBZ(1,3)=0.0D0
           XTBZ(3,1)=0.0D0
           DO 52 IMP=1,NIMP
             IF(FRIMPA(IMP).EQ.0.0D0)GO TO 52
             ZIMP=ZIMPA(IMP)
             AMSSRZ=2.0D0*AMIMPA(IMP)/(AMIMPA(IMP)+2.0D0)
             ECMZ=AMSSRZ*EBEAM
             IF    (DABS(ZIMP-2.0D0).LE.0.2D0)THEN
                 IBSEL=26
             ELSEIF(DABS(ZIMP-3.0D0).LE.0.2D0)THEN
                 IBSEL=29
             ELSEIF(DABS(ZIMP-4.0D0).LE.0.2D0)THEN
             	 IBSEL=32
             ELSEIF(DABS(ZIMP-5.0D0).LE.0.2D0)THEN
                 IBSEL=35
             ELSEIF(DABS(ZIMP-6.0D0).LE.0.2D0)THEN
                 IBSEL=38
             ELSEIF(DABS(ZIMP-7.0D0).LE.0.2D0)THEN
                 IBSEL=41
             ELSEIF(DABS(ZIMP-8.0D0).LE.0.2D0)THEN
                 IBSEL=44
             ELSEIF(DABS(ZIMP-9.0D0).LE.0.2D0)THEN
                 IBSEL=47
             ELSEIF(DABS(ZIMP-10.0D0).LE.0.2D0)THEN
                 IBSEL=47
             ELSE
                 IBSEL=47
             ENDIF
            ISEL=IBSEL
            VAL13=FRIMPA(IMP)*QH(EBEAM,TIEV,ISEL,ZIMP,NSEL,1,EA,OA,N,
     &      IPASS,TITLF,DSLPATH)
C	    WRITE(0,*) ' EXCITATION RATE 1 3 IS', EBEAM   ,  VAL13
            VAL31=VAL13*WN*DEXP(13.6048*DE/(TIEV+ECMZ))/WN1
            XTBZ(1,3)=XTBZ(1,3)+VAL13
            XTBZ(3,1)=XTBZ(3,1)+VAL31
   52      CONTINUE
           COEFFA(NCOEFF)=XTBZ(1,3)
           COEFFB(NCOEFF)=XTBZ(3,1)
C
           NCOEFF=NCOEFF+1
           ICODEA(NCOEFF)=5
           NA(NCOEFF)=1
           N1A(NCOEFF)=4
           DE=1.0D0/NA(NCOEFF)**2-1.0D0/N1A(NCOEFF)**2
           WN=2.0D0*NA(NCOEFF)**2
           WN1=2.0D0*N1A(NCOEFF)**2
           NSEL=4
           LXTBZ(1,4)=1
           LXTBZ(4,1)=1
           XTBZ(1,4)=0.0D0
           XTBZ(4,1)=0.0D0
           DO 53 IMP=1,NIMP
             IF(FRIMPA(IMP).EQ.0.0D0)GO TO 53
             ZIMP=ZIMPA(IMP)
             AMSSRZ=2.0D0*AMIMPA(IMP)/(AMIMPA(IMP)+2.0D0)
             ECMZ=AMSSRZ*EBEAM
             IF    (DABS(ZIMP-2.0D0).LE.0.2D0)THEN
                 IBSEL=27
             ELSEIF(DABS(ZIMP-3.0D0).LE.0.2D0)THEN
                 IBSEL=30
             ELSEIF(DABS(ZIMP-4.0D0).LE.0.2D0)THEN
             	 IBSEL=33
             ELSEIF(DABS(ZIMP-5.0D0).LE.0.2D0)THEN
                 IBSEL=36
             ELSEIF(DABS(ZIMP-6.0D0).LE.0.2D0)THEN
                 IBSEL=39
             ELSEIF(DABS(ZIMP-7.0D0).LE.0.2D0)THEN
                 IBSEL=42
             ELSEIF(DABS(ZIMP-8.0D0).LE.0.2D0)THEN
                 IBSEL=45
             ELSEIF(DABS(ZIMP-9.0D0).LE.0.2D0)THEN
                 IBSEL=48
             ELSEIF(DABS(ZIMP-10.0D0).LE.0.2D0)THEN
                 IBSEL=48
             ELSE
                 IBSEL=48
             ENDIF
             ISEL=IBSEL
            VAL14=FRIMPA(IMP)*QH(EBEAM,TIEV,ISEL,ZIMP,NSEL,1,EA,OA,N,
     &      IPASS,TITLF,DSLPATH)
C	    WRITE(0,*) ' EXCITATION RATE 1 4 IS', EBEAM   ,  VAL14
            VAL41=VAL14*WN*DEXP(13.6048*DE/(TIEV+ECMZ))/WN1
            XTBZ(1,4)=XTBZ(1,4)+VAL14
            XTBZ(4,1)=XTBZ(4,1)+VAL41
   53      CONTINUE
           COEFFA(NCOEFF)=XTBZ(1,4)
           COEFFB(NCOEFF)=XTBZ(4,1)
       ENDIF
C-----------------------------------------------------------------------
C  DATA TYPE (6): N=1, N=2, N=3, N=4, N=5
C-----------------------------------------------------------------------
       IF(ITYP6.EQ.1.AND.NIMP.GT.0)THEN
C
           NSEL=1
           NCOEFF=NCOEFF+1
           ICODEA(NCOEFF)=6
           NA(NCOEFF)=1
           N1A(NCOEFF)=0
           LSTBZ(1)=1
           STBZ(1)=0.0D0
           DO 61 IMP=1,NIMP
            IF(FRIMPA(IMP).EQ.0.0D0)GO TO 61
            ZIMP=ZIMPA(IMP)
C
            IF    (DABS(ZIMP-2.0D0).LE.0.2D0)THEN
                IBSEL=12
	    ELSEIF(DABS(ZIMP-3.0D0).LE.0.2D0)THEN
                IBSEL=13
            ELSEIF(DABS(ZIMP-4.0D0).LE.0.2D0)THEN
                IBSEL=14
            ELSEIF(DABS(ZIMP-5.0D0).LE.0.2D0)THEN
                IBSEL=15
            ELSEIF(DABS(ZIMP-6.0D0).LE.0.2D0)THEN
                IBSEL=16
            ELSEIF(DABS(ZIMP-7.0D0).LE.0.2D0)THEN
                IBSEL=17
            ELSEIF(DABS(ZIMP-8.0D0).LE.0.2D0)THEN
                IBSEL=18
	    ELSEIF(DABS(ZIMP-9.0D0).LE.0.2D0)THEN
                IBSEL=19
            ELSEIF(DABS(ZIMP-1.0D1).LE.0.2D0)THEN
                IBSEL=20
            ELSEIF(DABS(ZIMP-1.8D1).LE.0.2D0)THEN
                IBSEL=58
            ELSE
                IBSEL=55
            ENDIF
C
            ISEL=IBSEL
            T1=QH(EBEAM,TIEV,ISEL,ZIMP,NSEL,1,EA,OA,N,
     &      IPASS,TITLF,DSLPATH)
		
C
            IF    (DABS(ZIMP-2.0D0).LE.0.2D0)THEN
                IBSEL=2
            ELSEIF(DABS(ZIMP-3.0D0).LE.0.2D0)THEN
                IBSEL=3
            ELSEIF(DABS(ZIMP-4.0D0).LE.0.2D0)THEN
                IBSEL=4
            ELSEIF(DABS(ZIMP-5.0D0).LE.0.2D0)THEN
                IBSEL=5
            ELSEIF(DABS(ZIMP-6.0D0).LE.0.2D0)THEN
                IBSEL=6
            ELSEIF(DABS(ZIMP-7.0D0).LE.0.2D0)THEN
                IBSEL=7
            ELSEIF(DABS(ZIMP-8.0D0).LE.0.2D0)THEN
                IBSEL=8
	    ELSEIF(DABS(ZIMP-9.0D0).LE.0.2D0)THEN
                IBSEL=9
            ELSEIF(DABS(ZIMP-1.0D1).LE.0.2D0)THEN
                IBSEL=10
            ELSEIF(DABS(ZIMP-1.8D1).LE.0.2D0)THEN
                IBSEL=63
            ELSE
                IBSEL=56
            ENDIF
C
            ISEL=IBSEL
            T2=QH(EBEAM,TIEV,ISEL,ZIMP,NSEL,1,EA,OA,N,
     &      IPASS,TITLF,DSLPATH)

C	    WRITE(0,*) ' THE RATE FOR CX FROM THE GROUND', ebeam   ,  T2
            
            if (lnoion) then
               stbz(1)=stbz(1)+frimpa(imp)*(t2)
            elseif (lnocx) then
               stbz(1)=stbz(1)+frimpa(imp)*(t1)
            else
               STBZ(1)=STBZ(1)+FRIMPA(IMP)*(T1+T2)
            endif
   61      CONTINUE
           COEFFA(NCOEFF)=STBZ(1)
C
           NSEL=2
           NCOEFF=NCOEFF+1
           ICODEA(NCOEFF)=6
           NA(NCOEFF)=2
           N1A(NCOEFF)=0
           LSTBZ(2)=1
           STBZ(2)=0.0D0
           DO 62 IMP=1,NIMP
            IF(FRIMPA(IMP).EQ.0.0D0)GO TO 62
            ZIMP=ZIMPA(IMP)
            IF (DABS(ZIMP-1.8D1).LE.0.2D0) THEN
              ISEL = 59
            ELSE
              ISEL=53
            ENDIF
            T1=QH(EBEAM,TIEV,ISEL,ZIMP,NSEL,1,EA,OA,N,
     &      IPASS,TITLF,DSLPATH)

            IF (DABS(ZIMP-1.8D1).LE.0.2D0) THEN
              ISEL = 64
            ELSE
              ISEL = 54
            ENDIF
            T2=QH(EBEAM,TIEV,ISEL,ZIMP,NSEL,1,EA,OA,N,
     &      IPASS,TITLF,DSLPATH)

            if (lnoion) then
               stbz(2)=stbz(2)+frimpa(imp)*(t2)
            elseif (lnocx) then
               stbz(2)=stbz(2)+frimpa(imp)*(t1)
            else
               STBZ(2)=STBZ(2)+FRIMPA(IMP)*(T1+T2)
            endif
   62      CONTINUE
           COEFFA(NCOEFF)=STBZ(2)
C
           NSEL=3
           NCOEFF=NCOEFF+1
           ICODEA(NCOEFF)=6
           NA(NCOEFF)=3
           N1A(NCOEFF)=0
           LSTBZ(3)=1
           STBZ(3)=0.0D0
           DO 63 IMP=1,NIMP
            IF(FRIMPA(IMP).EQ.0.0D0)GO TO 63
            ZIMP=ZIMPA(IMP)
            IF (DABS(ZIMP-1.8D1).LE.0.2D0) THEN
              ISEL = 60
            ELSE
              ISEL = 53
            ENDIF
            T1=QH(EBEAM,TIEV,ISEL,ZIMP,NSEL,1,EA,OA,N,
     &      IPASS,TITLF,DSLPATH)

            IF (DABS(ZIMP-1.8D1).LE.0.2D0) THEN
              ISEL = 65
            ELSE
              ISEL = 54
            ENDIF
            T2=QH(EBEAM,TIEV,ISEL,ZIMP,NSEL,1,EA,OA,N,
     &      IPASS,TITLF,DSLPATH)

            if (lnoion) then
               stbz(3)=stbz(3)+frimpa(imp)*(t2)
            elseif (lnocx) then
               stbz(3)=stbz(3)+frimpa(imp)*(t1)
            else
               STBZ(3)=STBZ(3)+FRIMPA(IMP)*(T1+T2)
            endif
   63      CONTINUE
           COEFFA(NCOEFF)=STBZ(3)
C
           NSEL=4
           NCOEFF=NCOEFF+1
           ICODEA(NCOEFF)=6
           NA(NCOEFF)=4
           N1A(NCOEFF)=0
           LSTBZ(4)=1
           STBZ(4)=0.0D0
           DO 64 IMP=1,NIMP
            IF(FRIMPA(IMP).EQ.0.0D0)GO TO 64
            ZIMP=ZIMPA(IMP)
            IF (DABS(ZIMP-1.8D1).LE.0.2D0) THEN
              ISEL = 61
            ELSE
              ISEL = 53
            ENDIF
            T1=QH(EBEAM,TIEV,ISEL,ZIMP,NSEL,1,EA,OA,N,
     &      IPASS,TITLF,DSLPATH)

            IF (DABS(ZIMP-1.8D1).LE.0.2D0) THEN
              ISEL = 66
            ELSE
              ISEL = 54
            ENDIF
            T2=QH(EBEAM,TIEV,ISEL,ZIMP,NSEL,1,EA,OA,N,
     &      IPASS,TITLF,DSLPATH)

            if (lnoion) then
               stbz(4)=stbz(4)+frimpa(imp)*(t2)
            elseif (lnocx) then
               stbz(4)=stbz(4)+frimpa(imp)*(t1)
            else
               STBZ(4)=STBZ(4)+FRIMPA(IMP)*(T1+T2)
            endif
   64      CONTINUE
           COEFFA(NCOEFF)=STBZ(4)
C
           NSEL=5
           NCOEFF=NCOEFF+1
           ICODEA(NCOEFF)=6
           NA(NCOEFF)=5
           N1A(NCOEFF)=0
           LPSTBZ=5
           LSTBZ(5)=1
           STBZ(5)=0.0D0
           DO 65 IMP=1,NIMP
            IF(FRIMPA(IMP).EQ.0.0D0)GO TO 65
            ZIMP=ZIMPA(IMP)
            IF (DABS(ZIMP-1.8D1).LE.0.2D0) THEN
              ISEL = 62
            ELSE
              ISEL = 53
            ENDIF
            T1=QH(EBEAM,TIEV,ISEL,ZIMP,NSEL,1,EA,OA,N,
     &      IPASS,TITLF,DSLPATH)

            IF (DABS(ZIMP-1.8D1).LE.0.2D0) THEN
              ISEL = 67
            ELSE
              ISEL = 54
            ENDIF
            T2=QH(EBEAM,TIEV,ISEL,ZIMP,NSEL,1,EA,OA,N,
     &      IPASS,TITLF,DSLPATH)

            if (lnoion) then
               stbz(5)=stbz(5)+frimpa(imp)*(t2)
            elseif (lnocx) then
               stbz(5)=stbz(5)+frimpa(imp)*(t1)
            else
               STBZ(5)=STBZ(5)+FRIMPA(IMP)*(T1+T2)
            endif
   65      CONTINUE
           COEFFA(NCOEFF)=STBZ(5)
C
       ENDIF

       IF (DEBUG) THEN
          WRITE(0,1003)
          DO 30 I=1,NCOEFF
             WRITE(0,1004)I,ICODEA(I),NA(I),N1A(I),COEFFA(I),COEFFB(I)
   30     CONTINUE
       ENDIF

       RETURN
 1000  FORMAT(1A80)
 1001  FORMAT(16X,1P,8D8.2)
 1002  FORMAT(1H ,1A35)
 1003  FORMAT(1H ,'    I  ICODE  N   N1    COEFFA(CM**3 SEC-1)      COEF
     &FB(CM**3 SEC-1')
 1004  FORMAT(1H ,4I5,5X,1P,D12.4,13X,1P,D12.4)
 1005  FORMAT(2I4,8X,1P,8D8.2)
      END
