CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas310/run310.for,v 1.10 2010/02/19 14:49:13 mog Exp $ Date $Date: 2010/02/19 14:49:13 $
CX
      SUBROUTINE RUN310
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: RUN310*********************
C
C  CALLING PROGRAM: ADAS310
C
C  PURPOSE:  THIS IS THE TOP LEVEL SUBROUTINE FOR ADAS310
C            WHEN IT IS RUN 'INTERACTIVELY' I.E. WHEN THE USER IS
C            PREPARED TO WAIT FOR THE RESULTS TO BE CALCULATED.
C            IT OPENS ALL THE FILES AND CALLS ALL CALCULATION ROUTINES
C            AS WELL AS HANDLING PIPE COMMUNICATIONS WITH IDL.
C
c            NOTE THAT CERTAIN PARAMETERS RELATED TO THE SIZES OF
C            ARRAYS ARE SET IN THIS ROUTINE, BUT ARE ALSO SET TO IDENTICAL
C            VALUES IN THE IDL ROUTINE adas310.pro. ANY CHANGES TO THESE
C            PARAMETERS MUST BE DUPLICATED IN BOTH ROUTINES TO AVOID
C            POSSIBLE PROBLEMS.THE PARAMETERS IN QUESTION ARE PREFIXED
C            MX TO DENOTE MAXIMUM SIZES.
C
C 	     ALSO NOTE THAT SEVERAL OF THE ROUTINES CALLED FROM HERE
C            ARE NOT YET IN A FINAL FORM AND WILL NOT BE ANNOTATED AS
C            THOROUGHLY AS OTHER ADAS PROGRAMS. THIS HAS LED TO MANY
C            VARIABLES BEING USED, BUT LABELLED AS *UNKNOWN* FOR THE
C            TIME BEING.
C
C  PROGRAM:
C
C  PARAM : (I*4)   IUBTCH   = INPUT UNIT FROM JCL BATCH FILE.
C  PARAM : (I*4)   IUJCL    = OUTPUT UNIT FOR JCL FILE.
C  PARAM : (I*4)   IUTMP    = UNIT NUMBER FOR TEMPORARY SCRATCH FILE.
C  PARAM : (I*4)   IUPS1    = UNIT NUMBER FOR FIRST PASSING FILE.
C  PARAM : (I*4)   IUPS2    = UNIT NUMBER FOR SECOND PASSING FILE.
C  PARAM : (I*4)   IUPS3    = UNIT NUMBER FOR THIRD PASSING FILE.
C  PARAM : (I*4)   IUPS4    = UNIT NUMBER FOR FOURTH PASSING FILE.
C  PARAM : (I*4)   IURUN    = UNIT NUMBER FOR RUN SUMMARY FILE.
C  PARAM : (I*4)   MXIMP    = MAXIMUM NUMBER OF IMPURITIES.
C  PARAM : (I*4)   MXNRP    = MAXIMUM NUMBER OF REPRESENTATIVE N-SHELL
C                             LEVELS.
C  PARAM : (I*4)   MXDEN    = MAXIMUM NUMBER OF ELECTRON/PROTON
C                             DENSITIES.
C  PARAM : (I*4)   MXTMP    = MAXIMUM NUMBER OF ELECTRON/PROTON
C                             TEMPERATURES.
C  PARAM : (I*4)   MXENG    = MAXIMUM NUMBER OF BEAM ENERGIES IN SCAN.
C  PARAM : (I*4)   MXCOR    = *UNKNOWN*
C  PARAM : (I*4)   MXEPS    = *UNKNOWN*
C  PARAM : (I*4)   MXDEF    = *UNKNOWN*
C          (I*4)   IZ0      = NUCLEAR CHARGE OF BEAM SPECIES.
C          (I*4)   NDENS    = NUMBER OF ELECTRON/PROTON DENSITIES.
C          (I*4)   NDOUT    = NUMBER OF DENSITIES FED TO CALCULATION
C                             ROUTINES.
C          (I*4)   NTEMP    = NUMBER OF ELECTRON/PROTON TEMPERATURES.
C          (I*4)   NTOUT    = NUMBER OF TEMPERATURES FED TO CALCULATION
C                             ROUTINES.
C          (I*4)   NIP      = RANGE OF DELTA N FOR IMPACT PARAMETER
C                             XSECTS. (.LE. 4)
C          (I*4)   INTD     = ORDER OF MAXWELL QUADRATURE FOR XSECTS.
C                             (.LE. 3)
C          (I*4)   IPRS     = CONTROLS XSECTS BEYOND NIP RANGE.
C                             0 => DEFAULT TO VAN REGEMORTER XSECTS.
C                             1 => USE PERCIVAL-RICHARDS XSECTS.
C          (I*4)   ILOW     = CONTROLS ACCESS OF SPECIAL LOW LEVEL DATA.
C                             0 => NO SPECIAL LOW LEVEL DATA ACCESSED.
C                             1 => SPECIAL LOW LEVEL DATA ACCESSED.
C          (I*4)   IONIP    = CONTROLS INCLUSION OF ION IMPACT
C                             COLLISIONS.
C                             0 => NO ION IMPACT COLLISIONS INCLUDED.
C                             1 => ION IMPACT EXCITATION AND IONISATION
C                                  INCLUDED.
C          (I*4)   NIONIP   = RANGE OF DELTA N FOR ION IMPACT EXCITATION
C                             XSECTS.
C          (I*4)   ILPRS    = CONTROLS USE OF LODGE-PERCIVAL-RICHARDS
C                             XSECTS.
C                             0 => DEFAULT TO VAINSHTEIN XSECTS.
C                             1 => USE LODGE-PERCIVAL-RICHARDS XSECTS.
C          (I*4)   IVDISP   = CONTROLS USE OF BEAM ENERGY IN CALCULATION
C                             OF XSECTS.
C                             0 => ION IMPACT AT THERMAL MAXWELLIAN
C                                  ENERGIES.
C                             1 => ION IMPACT AT DISPLACED THERMAL
C                                  ENERGIES ACCORDING TO THE NEUTRAL
C                                  BEAM ENERGY PARAMETER.
C                             NB:  IF IVDISP=0 THEN SPECIAL LOW LEVEL
C                                  DATA FOR ION IMPACT IS NOT
C                                  SUBSTITUTED - ONLY VAINSHTEIN AND
C                                  LODGE ET AL. OPTIONS ARE OPEN.
C                                  ELECTRON IMPACT DATA SUBSTITUTION
C                                  DOES OCCUR.
C          (I*4)   NOSCAN   = CONTROLS MODE OF OPERATION.
C                             0 => SINGLE IMPURITY.
C                             1 => MULTIPLE IMPURITIES.
C          (I*4)   IZEFF    = NUCLEAR CHARGE OF IMPURITY.
C                             (ONLY SET IF 'NOSCAN'=0 )
C          (I*4)   NIMP     = NUMBER OF IMPURITY SPECIES
C                             (ONLY SET IF 'NOSCAN'=1 )
C          (I*4)   NMIN     = LOWEST N-SHELL.
C          (I*4)   NMAX     = HIGHEST N-SHELL.
C          (I*4)   IMAX     = NUMBER OF REPRESENTATIVE N-SHELL LEVELS.
C          (I*4)   JCOR     = *UNKNOWN*
C          (I*4)   JMAX     = *UNKNOWN*
C          (I*4)   JDEF     = NUMBER OF QUAMTUM DEFECTS.
C          (I*4)   NBENG    = NUMBER OF BEAM ENERGIES IN SCAN.
C
C          (R*8)   TS       = EXTERNAL RADIATION FIELD TEMPERATURE.
C                             UNITS: K
C          (R*8)   W        = EXTERNAL RADIATION FIELD DILUTION FACTOR
C                             (HIGHER LEVELS).
C          (R*8)   Z        = RECOMBINING ION CHARGE.
C                             ( = BEAM ION CHARGE + 1 )
C          (R*8)   CION     = MULTIPLIER OF GROUND LEVEL ELECTRON IMPACT
C                             IONISATION RATE COEFFICIENT.
C          (R*8)   CPY      = MULTIPLIER OF ELECTRON EXCITATION RATE
C                             COEFFICIENT FROM THE GOUND LEVEL.
C          (R*8)   BSIM	    = BEAM SPECIES ISOTOPE MASS.
C          (R*8)   W1       = EXTERNAL RADIATION FIELD DILUTION FACTOR
C                             FOR PHOTO-IONISATION FROM THE GROUND
C                             LEVEL.
C          (R*8)   ZEFF     = NUCLEAR CHARGE OF IMPURITY.
C                             (ONLY SET IF 'NOSCAN'=0 )
C          (R*8)   BMENER   = BEAM ENERGY.
C                             UNITS: EV/AMU
C          (R*8)   DENSH    = BEAM DENSITY.
C                             UNITS: CM-3
C          (R*8)   Z0       = NUCLEAR CHARGE OF BEAM SPECIES.
C          (R*8)   DEREF    = REFERENCE ELECTRON DENSITY.
C                             UNITS: CM-3
C          (R*8)   DPREF    = REFERENCE PROTON DENSITY.
C                             UNITS: CM-3
C          (R*8)   TEREF    = REFERENCE ELECTRON TEMPERATURE.
C                             UNITS: K
C          (R*8)   BEREF    = REFERENCE BEAM ENERGY.
C                             UNITS: EV/AMU
C          (L*4)   LFIRST   = FLAGS IF FIRST LOOP THROUGH PROGRAM.
C                             .TRUE.  => FIRST LOOP.
C                             .FALSE. => AT LEAST SECOND LOOP.
C          (L*4)   LCOMP    = FLAGS IF COMPRESSION IS TO BE PERFORMED
C                             .TRUE.  => COMPRESS DATA.
C                             .FALSE. => DO NOT COMPRESS DATA.
C
C          (C*2)   ELSYMB   = ELEMENT SYMBOL OF BEAM SPECIES.
C          (C*8)   DATE     = DATE.
C          (C*80)  DSNRUN   = RUN SUMMARY DATA SET NAME.
C          (C*80)  DSNPS1   = FIRST PASSING FILE DATA SET NAME.
C          (C*80)  DSNPS2   = SECOND PASSING FILE DATA SET NAME.
C          (C*80)  DSNPS3   = THIRD PASSING FILE DATA SET NAME.
C          (C*80)  DSNPS4   = SECOOND PASSING FILE DATA SET NAME.
C          (L*4)   LPASS1   = FLAG FOR WHETHER OR NOT 1ST PASSING FILE
C                             HAS BEEN REQUESTED
C          (L*4)   LPASS2   = FLAG FOR WHETHER OR NOT 2ND PASSING FILE
C                             HAS BEEN REQUESTED
C          (L*4)   LPASS3   = FLAG FOR WHETHER OR NOT 3RD PASSING FILE
C                             HAS BEEN REQUESTED
C          (L*4)   LPASS4   = FLAG FOR WHETHER OR NOT 4TH PASSING FILE
C                             HAS BEEN REQUESTED
C          (L*4)   LRUN     = FLAG FOR WHETHER OR NOT RUN SUMMARY FILE
C                             HAS BEEN REQUESTED
C          (C*80)  DSNEX    = FULL MVS DATA SET NAME FOR EXPANSION FILE
C                             (SUITABLE FOR DYNAMIC ALLOCATION).
C          (C*80)  DSNCX    = FULL MVS DATA SET NAME FOR CHARGE EXCHANGE
C                             DATA SET (SUITABLE FOR DYNAMIC ALLOCATION)
C          (C*24)  TITLE    = TITLE FOR RUN.
C          (C*80)  DSLPATH  = PATH FOR FILE READ BY ROUTINE NSUPH1.FOR
C                             CALLED BY V2BNMOD.FOR
C
C          (I*4)   IZIMPA() = NUCLEAR CHARGE OF IMPURITIES.
C                             (ONLY SET IF 'NOSCAN'=1 )
C                             DIMENSION: MXIMP
C          (I*4)   NREP()   = SET OF REPRESENTATIVE N-SHELL LEVELS.
C                             DIMENSION: MXNRP+1
C
C          (R*8)   ZIMPA()  = NUCLEAR CHARGE OF IMPURITIES.
C                             DIMENSION: MXIMP
C          (R*8)   DNIMPA() = DENSITY OF IMPURITIES.
C                             DIMENSION: MXIMP
C          (R*8)   AMIMPA() = ATOMIC MASS NUMBERS OF IMPURITIES.
C                             DIMENSION: MXIMP
C          (R*8)   FRIMPA() = IMPURITY FRACTIONS.
C                             DIMENSION: MXIMP
C          (R*8)   DENSA()  = ELECTRON DENSITIES.
C                             UNITS: CM-3
C                             DIMENSION: MXDEN
C          (R*8)   DENSOUT()= ELECTRON DENSITIES - SENT AS OUTPUT.
C                             UNITS: CM-3
C                             DIMENSION: MXDEN
C          (R*8)   TEA()    = ELECTRON TEMPERATURES.
C                             UNITS: K
C                             DIMENSION: MXTMP
C          (R*8)   TEAOUT() = ELECTRON TEMPERATURES - SENT AS OUTPUT.
C                             UNITS: K
C                             DIMENSION: MXTMP
C          (R*8)   DENPA()  = PROTON DENSITIES.
C                             UNITS: CM-3
C                             DIMENSION: MXDEN
C          (R*8)   DENPOUT()= PROTON DENSITIES - SENT AS OUTPUT.
C                             UNITS: CM-3
C                             DIMENSION: MXDEN
C          (R*8)   TPA()    = PROTON TEMPERATURES.
C                             UNITS: K
C                             DIMENSION: MXTMP
C          (R*8)   TPAOUT() = PROTON TEMPERATURES - SENT AS OUTPUT.
C                             UNITS: K
C                             DIMENSION: MXTMP
C          (R*8)   WBREP()  = *UNKNOWN*
C                             DIMENSION: MXNRP
C          (R*8)   COR()    = *UNKNOWN*
C                             DIMENSION: MXCOR
C          (R*8)   EPSIL()  = *UNKNOWN*
C                             DIMENSION: MXEPS
C          (R*8)   FIJ()    = *UNKNOWN*
C                             DIMENSION: MXEPS
C          (R*8)   WIJ()    = *UNKNOWN*
C                             DIMENSION: MXEPS
C          (R*8)   DEFECT() = SET OF QUANTUM DEFECT.
C                             DIMENSION: MXDEF
C          (R*8)   BMENGA() = BEAM ENERGIES IN SCAN.
C                             UNITS: EV/AMU
C                             DIMENSION: MXENG
C          (R*8)   XPOP()   = POPULATIONS OF REPRESENTATIVE LEVELS
C          (R*8)   F1()     = *UNKNOWN*
C          (R*8)   F2()     = *UNKNOWN*
C          (R*8)   F3()     = *UNKNOWN*
C          (R*8)   BNCALC() = *UNKNOWN*
C          (R*8)   BNACT()  = *UNKNOWN*
C          (R*8)   ZSUM1    = *UNKNOWN*
C          (R*8)   ZSUM2    = *UNKNOWN*
C          (R*8)   ALPHA()  = *UNKNOWN*
C          (R*8)   ALFA     = *UNKNOWN*
C          (R*8)   S        = *UNKNOWN*
C          (R*8)   HISS()   = *UNKNOWN*
C          (R*8)   DENS     = ELECTRON DENSITY (CM-3) - FROM V2BNMOD
C          (R*8)   DENSP    = PROTON/DEUTERON DENSITY  (CM-3) - FROM V2BNMOD
C          (R*8)   TE       = ELECTRON TEMPERATURE (K) - FROM V2BNMOD
C          (R*8)   TP       = PROTON/DEUTERON TEMPERATURE (K) (SAME FOR 
C                             ZIMP IONS) - FROM V2BNMOD
C          (R*8)   FLUX     = NEUTRAL BEAM FLUX  (CM-2 SEC-1) - FROM V2BNMOD
C          (R*8)   DEXPTE(I)= EXP(ATE/NREP(I)**2) FOR I'TH REPRESENTATIVE 
C                             LEVEL - FROM V2BNMOD
C
C          (I*4)   PIPEIN   = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)   PIPEOU   = PARAMETER = UNIT NUMBER FOR INPUT FROM PIPE
C          (I*4)   TWO      = PARAMETER = 2  : USED AS FLAG TO IDL
C          (I*4)   ONE      = PARAMETER = 1  : USED AS FLAG TO IDL
C          (I*4)   ZERO     = PARAMETER = 0  : USED AS FLAG TO IDL
C          (I*4)   I        = GENERAL LOOP INDEX
C          (I*4)   IMP      = LOOP INDEX
C          (I*4)   ILOGIC   = FLAG USED FOR READING OF LOGICAL VARIABLES
C          (I*4)   IRCODE   = ERROR FLAG USED WHEN OPENING FILES
C          (I*4)   JDENS    = LOOP COUNTER FOR DENSITIES
C          (I*4)   JTE      = LOOP COUNTER FOR TEMPERATURES
C          (I*4)   IPOSNT   = SEE V2BNMOD FOR EXPLANATION
C          (I*4)   NLOOPS   = TOTAL NUMBER OF TIMES TO PERFORM THE
C                             CALCULATION - THIS DEPENDS ON THE USER'S
C                             CHOICES FOR THE SCAN PARAMETERS.
C          (I*4)   NSINGLE  = FLAGs WHETHER ONLY A SINGLE CALCULATION
C                             IS REQUIRED OR NOT:
C                             0 = MORE THAN ONE CALCULATION REQUIRED
C                             1 = ONLY ONE CALCULATION REQUIRED (IMPLIES
C                                 THERE IS ONLY ONE ENERGY, ONE TEMP.
C                                 AND ONE DENSITY IN THE SCANS).
C          (I*4)   NL       = LOOP COUNTER OVER NLOOPS
C          (I*4)   IECOUNT  = NUMBER OF BEAM ENERGIES
C          (I*4)   INCOUNT  = NUMBER OF DENSITIES
C          (I*4)   ITCOUNT  = NUMBER OF TEMPERATURES
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          CAPASF     ADAS      OPENS PASSING FILES.
C          XXFLSH     IDL_ADAS  FLUSH OUT THE UNIX PIPE.
C          XXDATE     ADAS      RETURNS CURRENT DATE.
C          XFESYM     ADAS      RETURNS ELEMENT SYMBOL FOR GIVEN NUCLEAR
C                               CHARGE.
C          CATMPF     ADAS      WRITES SET OF PARAMETERS TO A TEMPORARY
C                               FILE WHICH IS THEN READ BY 'V2BNMOD'.
C          V2BNMOD    ????      PERFORMS PROGRAM'S MAIN COMPUTATION
C                               (TO BE UPGRADED).
C          CMPRSS     ????      WRITES BEAM STOPPING COEFFICENTS FILE.
C                               (TO BE UPGRADED).
C
C AUTHOR:  TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C
C VERSION: 1.1				DATE: 30-01-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C VERSION: 1.2				DATE: 08-02-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - WORKING VERSION
C
C VERSION: 1.3                          DATE: 09-02-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - CORRECTED ORDER IN WHICH OUTPUT FILENAMES ARE PASSED
C
C VERSION: 1.4                          DATE: 27-09-96
C MODIFIED: WILLIAM OSBORN + HUGH SUMMERS
C               - REMOVED MISTAKEN TEST FOR ZEFF.LE.0.0D0 IN
C		  THE CASE OF MULTIPLE IMPURITIES.
C
C VERSION: 1.5                          DATE: 27-09-96
C MODIFIED: WILLIAM OSBORN
C               - S.C.C.S. MISTAKE CORRECTED
C
C VERSION: 1.6                          DATE: 27-09-96
C MODIFIED: WILLIAM OSBORN
C     - ADDED READ OF TITLE FROM IDL
C     - REMOVED A FEW WRITE(0,*) STATEMENTS
C VERSION: 1.7
C MODIFIED: HARVEY ANDERSON
C	- ALTERED THE CALLING STRUCTURE OF V2BNMOD.FOR TO ALLOW
C	  ADDITIONAL PARAMETERS TO BE PASSED TO RUN310.
C	- REMOVED A SECTION OF CODE WHICH REPORTED TO OBTAIN
C	  MULTIPLE IMPURITY PARAMETERS. THIS SECTION OF CODE
C	  WAS REDUNDANT AFTER MODIFICATION WHERE MADE TO
C	  V2BNMOD.
C 	  DATE : 14/01/96
C	- REMOVED COUNTER NEAR LINE 631 BECAUSE UNDER CERTAIN
C	  CONDITIONS IT WAS CAUSING THE COMMUNICATION BETWEEN
C	  THE IDL AND FORTRAN TO BE OUT OF STEP. THE CONDITION
C	  WHEN THIS OCCURED WAS WHEN THERE WAS MORE TEMPERATURE
C	  VALUES THAN BEAM ENERGY VALUES. IN ALL OTHER CASES THE
C	  COUNTER WAS BEING UPDATED LATER ON IN THE CODE WHICH 
C	  WAS CORRECT, BUT BECAUSE THE COUNTER WAS BEING UPDATED
C	  AT AN EARLIER STAGE THE IDL WAS WAITING TO READ EXPECTED 
C	  DATA BUT THE FORTRAN CODE HAD FINISH,HENCE THE PROGRAM 
C	  WOULD CRASHED.
C	- ADDED ADDITIONAL CODE TO ACCOMADATE FOR THE SCENARIO WHEN 
C	  THERE IS ONLY ONE TEMPERATURE,ELECTRON AND PROTON DENSITY
C	  VALUE AND SEVERAL BEAM ENERGY VALUES. ORIGINALY THE CODE
C	  WOULD RUN FINE, HOWEVER ON CLOSE INSPECTION IT WAS OBSERVED
C	  THAT THE CORRECT REFERENCE BEAM ENERGY WOULD NOT BE 
C	  CORRECTLY EXTRACTED USING THE ROUTINE CMPRSS. THE REASON
C	  BEING THAT THE FORMAT EXPECTED BY CMPRSS HAD CHANGED
C	  UNDER THESE CONDITIONS. THIS WAS CORRECTED
C 	  DATE : 23/01/97
C	- INCREASED THE VALUE OF THE PARAMETERS MXTMP, MXENG AND
C	  MXDEN TO A VALUE OF 25.
C 	  DATE : 04/02/97
C	- WHEN THE USER ENTERED ONLY ONE TEMPERATURE AND ENERGY
C	  VALUE AND SEVERAL DENSITY VALUES. THE CORRECT REFERENCE 
C	  VALUES WOULD NOT BE EXTRACTED .THE REASON BEING THAT THE 
C         FORMAT EXPECTED BY CMPRSS.FOR HAD CHANGED UNDER THESE 
C         CONDITIONS. THIS WAS CORRECTED.
C	- IT WAS ALSO NOTICED THAT A SIMILAR SITUATION WOULD
C	  ARISE WHEN THERE WAS ONLY ONE DENSITY AND ENERGY VALUE
C	  AND SEVERAL TEMPERATURES USED. THIS WAS ALSO CORRECTED
C
C VERSION: 1.8                          DATE: 05-12-00
C MODIFIED: RICHARD MARTIN
C	INITIALISED NIMP TO 0 FOR NOSCAN.EQ.0 CASE .
C
C VERSION: 1.9                          DATE: 18-01-06
C MODIFIED: ALLAN WHITEFORD
C	FINAL SIGNAL TO IDL ONLY SENT AFTER PASSING
C       FILES HAVE BEEN CREATED
C
C VERSION : 1.10                        
C DATE    : 18-02-2010
C MODIFIED: Martin O'Mullane
C           - Get lnoion and lnocx from IDL.
C
C
C-----------------------------------------------------------------------
      INTEGER    PIPEIN       , PIPEOU       , ONE          , ZERO
      INTEGER    TWO
      INTEGER    IECOUNT      , ITCOUNT      , INCOUNT
      PARAMETER( PIPEIN=5     , PIPEOU=6     , ONE=1        , ZERO=0)
      PARAMETER( TWO=2 )
C-----------------------------------------------------------------------
      INTEGER    IUPS1        , IUPS2        , IUPS3        ,
     &           IUPS4        , MXIMP        , MXNRP        ,
     &           MXDEN        , MXTMP        , MXENG        ,
     &           MXCOR        , MXEPS        , MXDEF        ,
     &           IUTMP        , IURUN
      PARAMETER( IUPS1  = 7   , IUPS2  = 10  , IUPS3  = 18  ,
     &           IUPS4  = 62  , MXIMP  = 10  , MXNRP  = 30  ,
     &           MXDEN  = 25  , MXTMP  = 25  , MXENG  = 25  ,
     &           MXCOR  = 20  , MXEPS  = 10  , MXDEF  = 10  ,
     &           IUTMP  = 51  , IURUN  = 52)
C----------------------------------------------------------------------
      INTEGER    I       , IZ0     , NDENS   , NTEMP   ,
     &           NIP     , INTD    , IPRS    , ILOW    , IONIP   ,
     &           NIONIP  , ILPRS   , IVDISP  , IZEFF   , NOSCAN  ,
     &           NIMP    , NMIN    , NMAX    , IMAX    , JCOR    ,
     &           JMAX    , JDEF    , NBENG   , IMP     , N
      INTEGER    IZIMPA(MXIMP)  , NREP(MXNRP+1)
      INTEGER    ILOGIC  , IRCODE
      INTEGER    JTE     , JDENS   , IPOSNT  , NDOUT
      INTEGER    NLOOPS  , NSINGLE , NL      , K       , NTOUT
C-----------------------------------------------------------------------
      CHARACTER  XFESYM*2
C-----------------------------------------------------------------------
      REAL*8     TS      , W       , Z       , CION    , CPY     ,
     &           W1      , ZEFF    , BMENER  , DENSH   , Z0      ,
     &           DEREF   , DPREF   , TEREF   , BEREF   , BSIM
      REAL*8     ZIMPA(MXIMP)   , DNIMPA(MXIMP)  , AMIMPA(MXIMP)  ,
     &           FRIMPA(MXIMP)  , DENSA(MXDEN)   , TEA(MXTMP)     ,
     &           DENPA(MXDEN)   , TPA(MXTMP)     , WBREP(MXNRP)   ,
     &           COR(MXCOR)     , EPSIL(MXEPS)   , FIJ(MXEPS)     ,
     &           WIJ(MXEPS)     , DEFECT(MXDEF)  , BMENGA(MXENG)
      REAL*8     TPAOUT(MXTMP)  , TEAOUT(MXTMP)  , DENSOUT(MXDEN) ,
     &           DENPOUT(MXDEN) 
      REAL*8     F1(30)    , F2(30)    , F3(30)    , BNCALC(30)  ,
     &           BNACT(30) , XPOP(2)
      REAL*8     ZSUM1     , ZSUM2
      REAL*8     ALPHA(2)  , ALFA      , HISS(2)   , S
      REAL*8     DENS      , DENSP     , TE        , TP          ,
     &           FLUX      , XDENS     , ANSW      , EN
      REAL*8     DEXPTE(550)
C-----------------------------------------------------------------------
      CHARACTER  DATE*8       
      CHARACTER  DSNEX*80     , DSNCX*80     , TITLE*24
      CHARACTER  DSNPS1*80    , DSNPS2*80    , DSNPS3*80    ,
     &           DSNPS4*80    , DSNRUN*80
      CHARACTER  ELSYMB*2
      CHARACTER  DSLPATH*80
C-----------------------------------------------------------------------
      LOGICAL    LPASS1  , LPASS2  , LPASS3  , LPASS4 ,  LRUN
      logical    lnoion  , lnocx
C-----------------------------------------------------------------------
C *************************** BEGIN ************************************
C-----------------------------------------------------------------------
C
C
C-----------------------------------------------------------------------
C GET CURRENT DATE.
C-----------------------------------------------------------------------
C
      CALL XXDATE( DATE )
C
C-----------------------------------------------------------------------
C GET USER-DEFINED VALUES FROM IDL
C-----------------------------------------------------------------------
C FIRST GET VALUES RELATED TO THE IDL INPUT OPTIONS SCREEN 
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) IZ0
      Z0 = DFLOAT(IZ0)
      READ(PIPEIN,*) Z
      READ(PIPEIN,'(A)') DSNEX
      READ(PIPEIN,'(A)') DSNCX
      READ(PIPEIN,'(A)') DSLPATH


C
C-----------------------------------------------------------------------
C NOW GET VALUES RELATED TO THE IDL PROCESSING OPTIONS SCREEN
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) TS
      READ(PIPEIN,*) W
      READ(PIPEIN,*) CION
      READ(PIPEIN,*) CPY
      READ(PIPEIN,*) W1
      READ(PIPEIN,*) BSIM
      READ(PIPEIN,*) NIP
      READ(PIPEIN,*) INTD
      READ(PIPEIN,*) IPRS
      READ(PIPEIN,*) ILOW
      READ(PIPEIN,*) IONIP
      READ(PIPEIN,*) NIONIP
      READ(PIPEIN,*) ILPRS
      READ(PIPEIN,*) IVDISP
      READ(PIPEIN,*) NOSCAN
      IF (NOSCAN.EQ.0) THEN
          READ(PIPEIN,*) IZEFF
          ZEFF = DFLOAT(IZEFF)
	    NIMP=0
      ELSE
          READ(PIPEIN,*) NIMP
          DO 1, I=1, NIMP
              READ(PIPEIN,*) IZIMPA(I)
              ZIMPA(I) = DFLOAT(IZIMPA(I))
              READ(PIPEIN,*) AMIMPA(I)
              READ(PIPEIN,*) FRIMPA(I)
1         CONTINUE
      ENDIF
      READ(PIPEIN,*) NMIN
      READ(PIPEIN,*) NMAX
      READ(PIPEIN,*) IMAX
      DO 2, I=1, IMAX
          READ(PIPEIN,*) NREP(I)
          READ(PIPEIN,*) WBREP(I)
2     CONTINUE
      READ(PIPEIN,*) DEREF
      READ(PIPEIN,*) DPREF
      READ(PIPEIN,*) TEREF
      READ(PIPEIN,*) NDENS
      DO 3, I=1, NDENS
          READ(PIPEIN,*) DENSA(I)
          READ(PIPEIN,*) DENPA(I)
3     CONTINUE
      READ(PIPEIN,*) NTEMP
      DO 4, I=1, NTEMP
          READ(PIPEIN,*) TEA(I)
          READ(PIPEIN,*) TPA(I)
4     CONTINUE
      READ(PIPEIN,*) BEREF
      READ(PIPEIN,*) NBENG
      DO 5, I=1, NBENG
          READ(PIPEIN,*) BMENGA(I)
5     CONTINUE
      READ(PIPEIN,*) DENSH
      READ(PIPEIN,*) JCOR
      DO 6, I=1, MXCOR
          READ(PIPEIN,*) COR(I)
6     CONTINUE
      READ(PIPEIN,*) JMAX
      DO 7, I=1, MXEPS
          READ(PIPEIN,*) EPSIL(I)
          READ(PIPEIN,*) FIJ(I)
          READ(PIPEIN,*) WIJ(I)
7     CONTINUE
      READ(PIPEIN,*) JDEF
      DO 8, I=1, MXDEF
          READ(PIPEIN,*) DEFECT(I)
8     CONTINUE

C Specialized operation for BCX production

      read(pipein,*) ilogic
      if (ilogic.eq.1) then
          lnoion = .true.
      else
          lnoion = .false.
      endif

      read(pipein,*) ilogic
      if (ilogic.eq.1) then
          lnocx = .true.
      else
          lnocx = .false.
      endif
C
C
C-----------------------------------------------------------------------
C NOW GET VALUES RELATED TO THE IDL OUTPUT OPTIONS SCREEN
C-----------------------------------------------------------------------
C
      READ(PIPEIN,'(A)')TITLE
      READ(PIPEIN,*) ILOGIC
      IF (ILOGIC.EQ.1) THEN
          LRUN = .TRUE.
      ELSE
          LRUN = .FALSE.
      ENDIF
      READ(PIPEIN,*) ILOGIC
      IF (ILOGIC.EQ.1) THEN
          LPASS1 = .TRUE.
      ELSE
          LPASS1 = .FALSE.
      ENDIF
      READ(PIPEIN,*) ILOGIC
      IF (ILOGIC.EQ.1) THEN
          LPASS2 = .TRUE.
      ELSE
          LPASS2 = .FALSE.
      ENDIF
      READ(PIPEIN,*) ILOGIC
      IF (ILOGIC.EQ.1) THEN
          LPASS3 = .TRUE.
      ELSE
          LPASS3 = .FALSE.
      ENDIF
      READ(PIPEIN,*) ILOGIC
      IF (ILOGIC.EQ.1) THEN
          LPASS4 = .TRUE.
      ELSE
          LPASS4 = .FALSE.
      ENDIF
      READ(PIPEIN,'(A)') DSNRUN
      READ(PIPEIN,'(A)') DSNPS1
      READ(PIPEIN,'(A)') DSNPS2
      READ(PIPEIN,'(A)') DSNPS3
      READ(PIPEIN,'(A)') DSNPS4

C-----------------------------------------------------------------------
C Warn if specialized options are used - default should be .FALSE.
C-----------------------------------------------------------------------
       
       if (lnoion) write(0,*)'WARNING : Ion ionisation switched off'
       if (lnocx)  write(0,*)'WARNING : Charge exchange switched off'
C
C-----------------------------------------------------------------------
C OPEN OUTPUT DATA SETS
C-----------------------------------------------------------------------
C
      CALL CAPASF( IUPS1  , IUPS2  , IUPS3  , IUPS4  , IURUN ,
     &             DSNPS1 , DSNPS2 , DSNPS3 , DSNPS4 , DSNRUN, 
     &             LPASS1 , LPASS2 , LPASS3 , LPASS4 , LRUN  ,
     &             IRCODE   )
C
C-----------------------------------------------------------------------
C SEND SIGNAL TO IDL TO CONTINUE OR STOP
C-----------------------------------------------------------------------
C
      IF (IRCODE.EQ.1) THEN 
          WRITE( PIPEOU, *) ONE
          CALL XXFLSH( PIPEOU )
      ELSE
          WRITE( PIPEOU, *) ZERO
          CALL XXFLSH( PIPEOU )
      ENDIF
C
C-----------------------------------------------------------------------
C NOW DECIDE HOW MANY LOOPS TO PERFORM
C-----------------------------------------------------------------------
C
      NLOOPS = 1
      NSINGLE = 0
      NL = 1
      IF (NTEMP.GT.1) NLOOPS=NLOOPS+1
      IF (NBENG.GT.1) NLOOPS=NLOOPS+NBENG-1
C
C	MODIFIED : HARVEY ANDERSON
C	DATE:  14/01/97
C
      IF((NBENG.GT.1).AND.(NDENS.EQ.1).AND.(NTEMP.EQ.1))NLOOPS=NLOOPS+1
C
C	MODIFIED: HARVEY ANDERSON
C	DATE: 04/02/97
C
      IF((NBENG.EQ.1).AND.(NDENS.EQ.1).AND.(NTEMP.GT.1))NLOOPS=2
      IF((NBENG.EQ.1).AND.(NTEMP.EQ.1).AND.(NDENS.GT.1))NLOOPS=2
C
C-----------------------------------------------------------------------
C START OF MAIN LOOP
C-----------------------------------------------------------------------
C
      WRITE( PIPEOU, *) NLOOPS
      CALL XXFLSH( PIPEOU )
11    CONTINUE
          IF (NSINGLE.EQ.1) THEN
              DENSOUT(1) = DEREF
              DENPOUT(1) = DPREF
              NDOUT = 1
              TEAOUT(1) = TEREF
              TPAOUT(1) = TEREF
              NTOUT = 1
              BMENER = BEREF
          ELSE
C
C
C	MODIFIED: HARVEY ANDERSON
C	DATE: 14/01/97
C
	      IF ((NBENG.GT.1).AND.(NDENS.EQ.1).AND.(NTEMP.EQ.1)) THEN
		   IF (NL.LE.NBENG) THEN
                          DENSOUT(1) = DENSA(1)
                          DENPOUT(1) = DENPA(1)
                          NDOUT = NDENS
                          TEAOUT(1) = TEREF
                          TPAOUT(1) = TEREF
                          NTOUT = 1
                          BMENER = BMENGA(NL)
		   ELSE
			  DENSOUT(1) = DEREF
                      	  DENPOUT(1) = DPREF
                      	  NDOUT = 1
                          TEAOUT(1) = TEREF
                          TPAOUT(1) = TEREF
                      	  NTOUT = 1
                      	  BMENER = BEREF
		  ENDIF
	      ENDIF
C
              IF ((NBENG.GT.1).OR.(NDENS.GT.1)) THEN
                  IF (NL.LE.NBENG) THEN
                      DO 12, K=1, NDENS
                          DENSOUT(K) = DENSA(K)
                          DENPOUT(K) = DENPA(K)
12                    CONTINUE
                      NDOUT = NDENS
                      TEAOUT(1) = TEREF
                      TPAOUT(1) = TEREF
                      NTOUT = 1
                      BMENER = BMENGA(NL)
                  ELSE
                      DENSOUT(1) = DEREF
                      DENPOUT(1) = DPREF
                      NDOUT = 1
                      DO 13, K=1, NTEMP
                          TEAOUT(K) = TEA(K)
                          TPAOUT(K) = TPA(K)
13                    CONTINUE
                      NTOUT = NTEMP
                      BMENER = BEREF
                  ENDIF
              ELSE
                  DENSOUT(1) = DEREF
                  DENPOUT(1) = DPREF
                  NDOUT = 1
                  DO 14, K=1, NTEMP
                      TEAOUT(K) = TEA(K)
                      TPAOUT(K) = TPA(K)
14                CONTINUE
                  NTOUT = NTEMP
                  BMENER = BEREF
C
C MODIFIED : HARVEY ANDERSON
C DATE : 14/1/96
C
CXHA                  NL = NL + 1
              ENDIF
          ENDIF
C
C
C	MODIFIED: HARVEY ANDERSON
C	DATE: 04/02/97
C
C
	     IF ((NBENG.EQ.1).AND.(NDENS.EQ.1).AND.(NTEMP.GT.1)) THEN
                 IF (NL.LT.NLOOPS) THEN
		   DENSOUT(1) = DEREF
                   DENPOUT(1) = DPREF
		   TEAOUT(1)  = TEREF
		   TPAOUT(1)  = TEREF
                   NDOUT = 1
		 ELSE
		   DENSOUT(1) = DEREF
		   DENPOUT(1) = DPREF
		   NDOUT = 1
                   DO  K=1, NTEMP
                      TEAOUT(K) = TEA(K)
                      TPAOUT(K) = TPA(K)
		   ENDDO
                   NTOUT = NTEMP
                   BMENER = BEREF
		ENDIF
	     ENDIF

C
C
C	MODIFIED: HARVEY ANDERSON
C	DATE: 04/02/97
C
C
	     IF ((NBENG.EQ.1).AND.(NTEMP.EQ.1).AND.(NDENS.GT.1)) THEN
                 IF (NL.LT.NLOOPS) THEN
	           DO K =1,NDENS
			DENSOUT(K) = DENSA(K)
			DENPOUT(K) = DENPA(K)
		   ENDDO
		   NDOUT = NDENS
		   TEAOUT(1)  = TEREF
		   TPAOUT(1)  = TEREF
                   NDOUT = 1
		   BMENER=BMENGA(NL)
		 ELSE
		   DENSOUT(1) = DEREF
		   DENPOUT(1) = DEREF
		   NDOUT = 1
                   DO  K=1, NTEMP
                      TEAOUT(K) = TEA(K)
                      TPAOUT(K) = TPA(K)
		   ENDDO
                   NTOUT = NTEMP
                   BMENER = BEREF
		ENDIF
	     ENDIF

C
C-----------------------------------------------------------------------
C COPY INPUT PARAMETERS TO TEMPORARY FILE BEFORE CALLING
C SUBROUTINE 'V2BNMOD'. THIS FILE IS READ BY 'V2BNMOD'.
C-----------------------------------------------------------------------
C
      CALL CATMPF( IUTMP  , IZ0    , DSNEX  , DSNCX  ,
     &             NDOUT  , NTOUT  , TS     , W      ,
     &             Z      , CION   , CPY    , W1     ,
     &             NIP    , INTD   , IPRS   , ILOW   ,
     &             IONIP  , NIONIP , ILPRS  , IVDISP ,
     &             ZEFF   , NOSCAN , NIMP   , ZIMPA  ,
     &             AMIMPA , FRIMPA , DENSOUT, TEAOUT ,
     &             DENPOUT, TPAOUT , BMENER , DENSH  ,
     &             NMIN   , NMAX   , IMAX   , NREP   ,
     &             WBREP  , JCOR   , COR    , JMAX   ,
     &             EPSIL  , FIJ    , WIJ    , JDEF   ,
     &             DEFECT                              )
C
C-----------------------------------------------------------------------
C  ACQUIRE NAMES OF ELEMENT AND DATASET MEMBERS OPENED IN FORTRAN CODE.
C-----------------------------------------------------------------------
C
      ELSYMB = XFESYM( IZ0 )
      DO 101 I=1,3
          F1(I) = 0.00D+00
          F2(I) = 0.00D+00
          F3(I) = 0.00D+00
          BNCALC(I) = 0.00D+00
          BNACT(I) = 0.00D+00
  101 CONTINUE
      XPOP(1) = 0.00D+00
      XPOP(2) = 0.00D+00
C
      IF(NOSCAN.EQ.1) THEN
          ZSUM1 = 0.0
          ZSUM2 = 0.0
          DO 20 IMP=1, NIMP
              ZSUM1 = ZSUM1+ZIMPA(IMP)*FRIMPA(IMP)
	      ZSUM2 = ZSUM2+ZIMPA(IMP)**2*FRIMPA(IMP)
20        CONTINUE


      ENDIF
C
      WRITE(PIPEOU,*) NDOUT
      CALL XXFLSH( PIPEOU )
      WRITE(PIPEOU,*) NTOUT
      CALL XXFLSH( PIPEOU )


      DO 401 JDENS=1,NDOUT
          DO 400 JTE=1,NTOUT
              DO 100 I=1,4
                  IPOSNT=I
                  CALL V2BNMOD( IPOSNT , JDENS  , JTE    , NREP   ,
     &                          F1     , F2     , F3     , BNCALC ,
     &                          BNACT  , XPOP   , IMAX   , DENSH  ,
     &                          DENS   , DENSP  , TE     , TP     ,
     &                          BMENER          , FLUX   , DEXPTE , 
     &                          ALFA   , S      , DSLPATH ,NIMP   ,
     &				ZIMPA  , ZEFF   , DNIMPA ,
     &                          lnoion , lnocx  )
                  IF (IPOSNT.EQ.2) ALPHA(2)=ALFA
                  IF (IPOSNT.EQ.2) HISS(2)=S
                  IF (IPOSNT.EQ.4) ALPHA(1)=ALFA
                  IF (IPOSNT.EQ.4) HISS(1)=S
                  WRITE(PIPEOU,*) ONE
                  CALL XXFLSH( PIPEOU )		  
100           CONTINUE
C
C-----------------------------------------------------------------------
C

       WRITE(7,2001)
       WRITE(7,2045)
       WRITE(7,2045)
       WRITE(7,2045)
       WRITE(7,2050)
       WRITE(7,2045)
       WRITE(7,2045)
       WRITE(7,3002) TITLE , Z0 , Z
       WRITE(7,2045)
       WRITE(7,2061)TS,TE,TP
       WRITE(7,2062)W,DENS,DENSP
       WRITE(7,2045)
       XDENS=DENSH/DENS
       WRITE(7,2063)BMENER,DENSH,XDENS,FLUX
       WRITE(7,2045)
       WRITE(7,2045)
       WRITE(7,2071)XPOP(2),ALPHA(2),HISS(2)
       WRITE(7,2045)
       WRITE(7,2072)XPOP(1),ALPHA(1),HISS(1)
       WRITE(7,2045)
       WRITE(7,2045)
       WRITE(7,2045)
       WRITE(7,2066)
       ANSW=DEXPTE(NREP(1))*NREP(1)*NREP(1)*DENS*4.14131D-16/(TE**1.5)
       F1(1)=0.0D0
       F2(1)=ALPHA(2)/(HISS(2)*ANSW)
       F3(1)=(ALPHA(1)-ALPHA(2))/(HISS(1)*ANSW*XDENS)
       DO 102 I=1,IMAX
       N=NREP(I)
       EN=N
       ANSW=DEXPTE(N)*EN*EN*DENS*4.14131D-16/(TE**1.5)
       WRITE(7,2068)I,N,F1(I),F2(I),F3(I),BNCALC(I),BNACT(I),ANSW
  102  CONTINUE
       WRITE(7,2045)
       WRITE(7,2045)
       WRITE(7,2045)
       WRITE(7,2075)
       CALL GENTAB(NREP,IMAX,DENS,TE,BMENER,F1,F2,F3)
       WRITE(7,2045)
       WRITE(7,2045)
       WRITE(7,3003)NIP,INTD,IPRS,ILOW,IONIP,NIONIP,ILPRS,IVDISP,
     &              ZEFF,TS,W,CION,CPY,W1,ZIMPA(1),DNIMPA(1)
C
       IF(NIMP.GT.1)THEN
           DO 60 IMP=2,NIMP
             WRITE(7,3004)ZIMPA(IMP),DNIMPA(IMP)
   60      CONTINUE
       ENDIF
C
400       CONTINUE
401   CONTINUE
C
C-----------------------------------------------------------------------
C GO BACK FOR MORE IF THERE ARE MORE LOOPS TO PERFORM
C-----------------------------------------------------------------------
C
      IF (NL.LT.NLOOPS) THEN
          NL = NL + 1
          GOTO 11
      ENDIF
C
C-----------------------------------------------------------------------
C IF COMPRESS FLAG SET (4TH PASSING FILE REQUESTED) THEN COMPRESS DATA IN FIRST
C PASSING FILE INTO FOURTH PASSING FILE.
C-----------------------------------------------------------------------
C
      IF ( LPASS4 ) THEN
C
         ENDFILE( IUPS1 )
         REWIND( IUPS1 )
C
         CALL CMPRSS(IUPS1,IUPS4,DATE,IECOUNT,INCOUNT,ITCOUNT)
C
      ENDIF
      IF ( LRUN ) THEN
          WRITE(IURUN,4001) DSNRUN
          WRITE(IURUN,*) ' '
          WRITE(IURUN,4002) DSNPS1
          WRITE(IURUN,4003) DSNPS2
          WRITE(IURUN,4003) DSNPS3
          WRITE(IURUN,4003) DSNPS4
          WRITE(IURUN,*) ' '
          WRITE(IURUN,*) '   NUMBER OF BEAM ENERGIES'
          WRITE(IURUN,*) IECOUNT
          WRITE(IURUN,*) '   NUMBER OF DENSITIES'
          WRITE(IURUN,*) INCOUNT
          WRITE(IURUN,*) '   NUMBER OF TEMPERATURES '
          WRITE(IURUN,*) ITCOUNT
      ENDIF
C
C-----------------------------------------------------------------------
C CLOSE I/O FILES.
C-----------------------------------------------------------------------
C
      CLOSE( IUPS1 )
      CLOSE( IUPS2 )
      CLOSE( IUPS3 )
      CLOSE( IUPS4 )
      CLOSE( IURUN )
C
C-----------------------------------------------------------------------
C SIGNAL TO IDL THAT THE CALCULATION IS COMPLETE
C-----------------------------------------------------------------------
C
      WRITE(PIPEOU,*) TWO
      CALL XXFLSH( PIPEOU )
C
      RETURN
C
C-----------------------------------------------------------------------
C
 2000  FORMAT(8I5,F5.1,2I5)                                             
 2001  FORMAT('1')
 2002  FORMAT(1H ,1A30)
 2045  FORMAT('    ')
 2050  FORMAT(' EFFECTIVE CONTRIBUTION TABLE FOR ION PRINCIPAL QUANTUM S
     &HELL POPULATIONS IN THERMAL PLASMA')
 2061  FORMAT('    TRAD = ',1PE8.2,' K        TE = ',1PE8.2,' K
     &     TP = ',1PE8.2,' K')
 2062  FORMAT('       W = ',1PE8.2,'          NE = ',1PE8.2,' CM-3
     &     NP = ',1PE8.2,' CM-3')
 2063  FORMAT('      EH = ',1PE8.2,' EV/AMU   NH = ',1PE8.2,' CM-3
     &  NH/NE = ',1PE8.2,'   FLUX = ',1PE8.2,' CM-2 SEC-1')
 2066  FORMAT(1H ,3X,'I',4X,'N',15X,'F1',13X,'F2',13X,'F3',10X
     &,'B(CHECK)',7X,'B(ACTUAL)',6X,'NN/(BN*N+)')
 2068  FORMAT(2I5,7X,1P,7E15.5)
 2071  FORMAT(' CHARGE EXCHANGE OFF : N1/N+ = ',1PE11.5,'  RECOMB COEFF
     &= ',1PE11.5,' CM+3 SEC-1   IONIZ COEFF = ',1PE11.5,' CM+3 SEC-1')
 2072  FORMAT(' CHARGE EXCHANGE ON  : N1/N+ = ',1PE11.5,'  RECOMB COEFF
     &= ',1PE11.5,' CM+3 SEC-1   IONIZ COEFF = ',1PE11.5,' CM+3 SEC-1')
 2075  FORMAT(47X,'BN = F1*(N1/N+) + F2 + F3*(NH/NE)'/
     &47X,'N1 = POPULATION OF GROUND STATE OF ION'/
     &47X,'N+ = POPULATION OF GROUND STATE OF NEXT IONISATION STAGE'/
     &47X,'NN = POPULATION OF PRINCIPAL QUANTUM SHELL N OF ION'/
     &47X,'BN = SAHA-BOLTZMANN FACTOR FOR PRINCIPAL QUANTUM SHELL N '/
     &47X,'EH = NEUTRAL HYDROGEN BEAM ENERGY'/
     &47X,' W = RADIATION DILUTION FACTOR'/
     &47X,'Z0 = NUCLEAR CHARGE'/
     &47X,'Z1 = ION CHARGE+1')
 3000  FORMAT(E10.2,3A8)
 3002  FORMAT(1H ,2X,A,1X,' Z0 = ',1P,1E8.2,15X,' Z1 = ',1P,1E8.2)
 3003  FORMAT(1H ,'NIP   =',I3,5X,'INTD  =',I3,5X,'IPRS  =',I3,5X,
     &            'ILOW  =',I3,5X,
     &            'IONIP =',I3,5X,'NIONIP=',I3,5X,'ILPRS =',I3,5X,
     &            'IVDISP=',I3/
     &        1H ,'ZEFF  =',0P,F4.1,4X,'TS  =',1PD10.2,5X,
     &            'W   =',1PD10.2,5X,
     &            'CION  =',0P,F3.1,5X,'CPY =',0P,F3.1,5X,
     &            'W1  =',1PD10.2,5X,'ZIMP  =',0P,F4.1,1X,'(',1PD10.2,
     &            ')')
 3004  FORMAT(1H ,110X,F4.1,'(',1PD10.2,')')
  103  FORMAT(2I5,   1P,6E10.2)
  104  FORMAT(I5,3F10.5)
 4001  FORMAT(1H ,4X,'RUN SUMMARY DATA SET NAME   : ',A80)
 4002  FORMAT(1H ,4X,'PASSING FILES DATA SET NAMES: ',A80)
 4003  FORMAT(1H ,34X,A80)
C
C-----------------------------------------------------------------------
C

      END
