CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas310/cmprss.for,v 1.9 2004/07/06 12:07:17 whitefor Exp $ Date $Date: 2004/07/06 12:07:17 $
CX
      SUBROUTINE CMPRSS(IUIN,IUOUT,DATE,IECOUNT,INCOUNT,ITCOUNT)
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: CMPRSS ********************
C
C PURPOSE:
C  READ OUTPUT FROM V2BNLDN1 AND PRODUCE TABLES OF BEAM STOPPING RATES
C  AS A FUNCTION OF PLASMA DENSITY AND TEMPERATURE AND OF BEAM ENERGY
C  (FOR USE IN KS4FIT BY QHIOCH)
C
C----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C
C UPDATE:  20/01/94 - JONATHAN NASH - TESSELLA SUPPORT SERVICES PLC
C
C          THIS SUBROUTINE WAS ADAPTED FROM THE PROGRAM
C          'JETXJS.BMSTOP.FORT(COMPRESS)'.
C
C          THE FOLLOWING MODIFICATIONS HAVE BEEN MADE:
C
C          1) THE MAXIMUM NUMBER OF BEAM ENERGIES HAS BEEN INCREASED
C             FROM 13 TO 20.
C
C          2) THE UNIT NUMBERS OF THE INPUT AND OUTPUT FILES ARE PASSED
C             INTO THE ROUTINE AS ARGUMENTS.
C
C          3) THE REFERENCE ELECTRON DENSITY AND TEMPERATURE AND BEAM
C             ENERGY ARE DETERMINED RATHER THAN BEING DECLARED AS
C             PARAMETERS. THESE ASSIGMENTS ARE BASED ON KNOWING THE
C             ORDER OF THE DATA IN THE INPUT FILE.
C
C  NOTES:  NO ATTEMPT HAS BEEN MADE TO RESTRUCTURE THE ROUTINE. RATHER
C          THE MINIMUM AMOUNT OF WORK TO INTEGRATE THE ROUTINE INTO
C          ADAS310 HAS BEEN COMPLETED.
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 01-02-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER SCCS CONTROL
C
C VERSION: 1.2                          DATE: 01-02-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - ALLOWED FOR CASE WHERE ZEFF=0.0
C
C VERSION: 1.3                          DATE: 05-02-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - ADDED NUMBER OF ENERGIES, TEMPS. AND DENSITIES
C                 AS OUTPUT.
C
C VERSION: 1.4                          DATE: 05-02-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - CORRECTED SCCS KEYWORDS.
C
C VERSION: 1.5                          DATE: 08-02-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - REMOVED SUPERFLUOUS VARIABLES
C
C VERSION: 1.6                          DATE: 14-02-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - CHANGED FORMAT 1004 TO READ 97X RATHER THAN
C                 98X AS THIS WAS NOT BEING READ CORRECTLY
C
C VERSION: 1.7                          DATE: 23-09-96
C MODIFIED: WILLIAM OSBORN (TESSELLA SUPPORT SERVICES PLC)
C               - CHANGED FORMAT 1004 TO READ E12.5 RATHER THAN
C                 E11.5 AS THIS WAS NOT BEING READ CORRECTLY
C VERSION: 1.8
C MODIFIED: HARVEY ANDERSON
C DATE: 23/01/96
C	- MODIFIED THE VALUE OF MAXTE, MAXNE, AND MAXEB FROM 10
C	  TO 25.
C
C VERSION: 1.9
C MODIFIED: HARVEY ANDERSON
C DATE: 09/08/99
C	- EXTENDED THE ARRAY CONTAINING THE CHEMICAL ELEMENT
C	  SYMBOL TO INCLUDE ALL SPECIES UP TO ZN.
C
C
C-----------------------------------------------------------------------
C
C  INPUT:  (I*4)  IUIN      = UNIT NO. OF INPUT FILE.
C  INPUT:  (I*4)  IUOUT     = UNIT NO. OF OUTPUT FILE.
C  INPUT:  (C*8)  DATE      = DATE STRING.
C  OUTPUT: (I*4)  IECOUNT   = NUMBER OF BEAM ENERGIES
C  OUTPUT: (I*4)  INCOUNT   = NUMBER OF DENSITIES
C  OUTPUT: (I*4)  ITCOUNT   = NUMBER OF TEMPERATURES
C
C----------------------------------------------------------------------
C
C----------------------------------------------------------------------
      INTEGER    IUIN    , IUOUT  
C----------------------------------------------------------------------
      CHARACTER  DATE*8
C----------------------------------------------------------------------
C
C----------------------------------------------------------------------
      PARAMETER( MAXTE=25, MAXNE=25, MAXEB=25)
C
      CHARACTER LINE*132
      REAL NERAY(MAXNE),TERAY(MAXTE),EBRAY(MAXEB)
      REAL SRAY(MAXEB,MAXNE,MAXTE)
      INTEGER IEA(MAXEB),ITA(MAXTE),INA(MAXNE)
      REAL TEREF,NEREF,SREF
      REAL NE,TE,EB
      INTEGER IZ
      REAL Z
      CHARACTER*2 SPEC(30)
      DATA SPEC/'H ','HE','LI','BE','B ','C ','N ','O ','F ','NE','NA',
     &'MG','AL','SI','P ','S ','CL','AR','K ','CA','SC','TI',
     &'V ','CR','MN','FE','CO','NI','CU','ZN'/
C----------------------------------------------------------------------
C
C   S  = IONISATION COEFFICIENT
C   NE = ELECTRON DENSITY (CM -3)
C   TE = ELECTRON TEMPERATURE (EV)
C   EB = BEAM ENERGY (EV/AMU)
C   TEREF = REFERENCE TEMPERATURE FOR NE/EB MATRIX
C   NEREF = REFERENCE DENSITY FOR TE VECTOR
C   EBREF = REFERENCE BEAM ENERGY FOR TE VECTOR
C   SREF = RATE COEFFICIENT ASSOCIATED WITH THESE REFERENCE VALUES
C
C                        C.F. MAGGI 22 JULY 1991
C                        L.D. HORTON 31 JULY 91
C-----------------------------------------------------------------------
C
      ITCOUNT=0
      INCOUNT=0
      IECOUNT=0
C ---------------------------------------------------------------------
C START OF MAIN LOOP FOR PROGRAM
C ---------------------------------------------------------------------
 10   READ(IUIN,'(1A132)',END=200) LINE
      IF(LINE(30:31).EQ.'TE') THEN
        READ(LINE,1001) TE
        TE=TE/(1.16*10**4)
        CALL FIND(TERAY,TE,ITCOUNT,IT)
      ELSE
        GO TO 10
      ENDIF
C
 20   READ(IUIN,'(1A132)') LINE
      IF(LINE(30:31).EQ.'NE') THEN
        READ(LINE,1002) NE
        CALL FIND(NERAY,NE,INCOUNT,IN)
      ELSE
        GO TO 20
      ENDIF
C
 30   READ(IUIN,'(1A132)') LINE
      IF(LINE(7:8).EQ.'EH') THEN
        READ(LINE,1003) EB
        CALL FIND(EBRAY,EB,IECOUNT,IE)
      ELSE
        GO TO 30
      ENDIF
C
 40   READ(IUIN,'(1A132)') LINE
      IF(LINE(18:20).EQ.'OFF'.AND.LINE(45:56).EQ.'RECOMB COEFF')
     + THEN
        READ(LINE,1004) SRAY(IE,IN,IT)
      ELSE
        GO TO 40
      ENDIF
C
 50   READ(IUIN,'(1A132)') LINE
      IF(LINE(2:5).EQ.'ZEFF') THEN
        READ(LINE,1005) Z
        IZ = Z
      ELSE
        GO TO 50
      ENDIF
C-------------------------------------------------------------------
C  SET UP REFERENCE VALUES.
C-------------------------------------------------------------------
      IF ( ITCOUNT .EQ. 1 ) THEN
         TEREF = TE
         NEREF = NE
         EBREF = EB
      ELSE IF ( ITCOUNT .EQ. 2 ) THEN
         NEREF = NE
         EBREF = EB
      ENDIF
C-------------------------------------------------------------------
C  GO TO START OF MAIN LOOP
C-------------------------------------------------------------------
      GO TO 10
C
 200  CONTINUE
C--------------------------------------------------------------------
C
C     SORTS EBRAY,NERAY AND TERAY IN INCREASING ORDER
C
C--------------------------------------------------------------------
      CALL INITPOS(IEA,IECOUNT)
      CALL NSORT(EBRAY,IEA,IECOUNT)
      CALL INITPOS(INA,INCOUNT)
      CALL NSORT(NERAY,INA,INCOUNT)
      CALL INITPOS(ITA,ITCOUNT)
      CALL NSORT(TERAY,ITA,ITCOUNT)
C--------------------------------------------------------------------
C
C     FIND REFERENCE VALUES IN DATA
C
C--------------------------------------------------------------------
      ITREF = 0
      DO I=1,ITCOUNT
        IF (TERAY(I).EQ.TEREF) ITREF = ITA(I)
      END DO
      IF (ITREF.EQ.0) STOP 'REFERENCE TEMPERATURE NOT IN DATA!'
      IEREF = 0
      DO I=1,IECOUNT
        IF (EBRAY(I).EQ.EBREF) IEREF = IEA(I)
      END DO
      IF (IEREF.EQ.0) STOP 'REFERENCE BEAM ENERGY NOT IN DATA!'
      INREF = 0
      DO I=1,INCOUNT
        IF (NERAY(I).EQ.NEREF) INREF = INA(I)
      END DO
      IF (INREF.EQ.0) STOP 'REFERENCE DENSITY NOT IN DATA!'
      SREF = SRAY(IEREF,INREF,ITREF)
C--------------------------------------------------------------------
C
C     WRITE HEADER
C
C--------------------------------------------------------------------
      IF (IZ.GT.0) THEN
          WRITE(IUOUT,2000) IZ,SREF,SPEC(IZ),DATE
      ELSE
          WRITE(IUOUT,2000) IZ,SREF,'  ',DATE
      ENDIF
      WRITE(IUOUT,2004)
C--------------------------------------------------------------------
C
C     WRITE STOPPING RATE MATRIX AT REFERENCE TEMPERATURE
C
C--------------------------------------------------------------------
      WRITE(IUOUT,2001) IECOUNT,INCOUNT,TEREF
      WRITE(IUOUT,2004)
      WRITE(IUOUT,2002) (EBRAY(I),I=1,IECOUNT)
      WRITE(IUOUT,2002) (NERAY(I),I=1,INCOUNT)
      WRITE(IUOUT,2004)
      DO I=1,INCOUNT
        WRITE(IUOUT,2002) (SRAY(IEA(J),INA(I),ITREF),J=1,IECOUNT)
      ENDDO
      WRITE(IUOUT,2004)
C--------------------------------------------------------------------
C
C     WRITE TEMPERATURE DEPENDENT STOPPING VECTOR
C
C--------------------------------------------------------------------
      WRITE(IUOUT,2003) ITCOUNT,EBREF,NEREF
      WRITE(IUOUT,2004)
      WRITE(IUOUT,2002) (TERAY(I),I=1,ITCOUNT)
      WRITE(IUOUT,2004)
      WRITE(IUOUT,2002) (SRAY(IEREF,INREF,ITA(I)),I=1,ITCOUNT)
      WRITE(IUOUT,2004)
C
      RETURN
C
C--------------------------------------------------------------------
C
C     FORMATS
C
C--------------------------------------------------------------------
 1001 FORMAT(34X,1E8.2)
 1002 FORMAT(34X,1E8.2)
 1003 FORMAT(11X,1E8.2)
 1004 FORMAT(97X,1E12.5)
 1005 FORMAT(8X,F3.1)
 2000 FORMAT(1X,I4,1X,'/SVREF=',1PE9.3,
     +     1X,'/SPEC=',A2,1X,'/DATE=',A8,1X,'/CODE=ADAS310')
 2001 FORMAT(1X,I4,1X,I4,1X,'/TREF=',1PE9.3)
 2002 FORMAT(8(1X,1PE9.3),/,8(1X,1PE9.3))
 2003 FORMAT(1X,I4,1X,'/EREF=',1PE9.3,1X,'/NREF=',1PE9.3)
 2004 FORMAT(80('-'))
C
      END
