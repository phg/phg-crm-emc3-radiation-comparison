C SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas310/qh.for,v 1.14 2012/08/15 10:22:40 mog Exp $ Date $Date: 2012/08/15 10:22:40 $
CX
       FUNCTION QH(EPRO,TTAR,ISEL,ZSEL,NSEL,IORD,EA,OA,N,IPASS,
     &             TITLF, DSLPATH)
C
       IMPLICIT REAL*8(A-H,O-Z)
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 FUNCTION: QH ***************************
C
C  PURPOSE : FUNCTION TO EVALUATE MAXWELL AVERAGED TOTAL 
C	     IONISATION, EXCITATION AND CHARGE EXCHANGE RATE 
C	     COEFFICIENTS. THE FUNCTION ALSO RETURNS THE RAW 
C	     CROSS-SECTION DATA FOR VERIFICATION AND GRAPHING 
C	     PURPOSES. THE INCIDENT PARTICLE IS A MONOENERGETIC 
C	     BEAM AND THE TARGET IS A MAXWELL DISTRIBUTION. THE 
C	     TARGET AND PROJECTILE ROLES MAY BE REVERSED. ARBITRARY 
C	     RELATIVE SPEEDS ARE ALLOWED. THE RATE COEFFICIENT 
C	     REQUIRED IS SELECTED FROM A LIST.  THERE IS SOME
C  	     SCALED DATA. ALL X-SECTS INVOLVE HYDROGEN AS ONE OF 
C	     THE SPECIES.
C
C  INPUT
C      EPRO  = INCIDENT PARTICLE ENERGY (EV/AMU)
C      TTAR  = MAXWELL TEMPERATURE OF TARGET PARTICLES (EV)
C              IF (TTAR.LE.0) THEN ONLY RAW SOURCE VALUES ARE RETURNED
C              IN ARRAYS (EA(I),OA(I), I=1,N)
C      ISEL  = SELECTOR FOR PARTICULAR RATE COEFFT. CHOSEN FROM TABLE
C              BELOW (SEE ALSO NOTES ON DATA)
C      ZSEL  = NUCLEAR CHARGE (REQUIRED ONLY FOR PARTICULAR ISEL)
C      NSEL  = PRINC. QUANTUM NO. (REQUIRED ONLY FOR PARTICULAR ISEL
C              NB. NSEL SHOULD BE ZERO ON ENTRY OTHERWISE)
C      IORD  = 1 FOR 1ST PARTICLE INCIDENT AND MONOENERGETIC
C            = 2 FOR 2ND PARTICLE INCIDENT AND MONOENERGETIC
C      IPASS = 0 IF DATA FILE TO BE READ IN AFRESH
C            = 1 IF DATA FILE IS NOT TO BE READ IN AGAIN
C  OUTPUT
C      QH    = RATE COEFFICIENT (CM3 SEC-1)
C      EA(I) = SET OF ENERGIES (EV/AMU) FOR SELECTED SOURCE DATA
C      OA(I) = CROSS-SECTIONS (CM**2) FOR SELECTED SOURCE DATA
C      N     = NUMBER OF SOURCE DATA VALUES
C      TITLF = INFORMATION STRING
C
C
C  *********** H.P.SUMMERS,JET             20 JUL 1990  ***************
C  ***********                        COR  31 JUL 1990  ***************
C  ***********                        ADD   3 JUL 1991  ***************
C
C NOTES: THIS ROUTINE IS NOT YET PROPERLY ANNOTATED
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 18-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER SCCS CONTROL
C		- MODIFIED NAME OF INPUT FILE BY ADDING INPUT
C                 PARAMETER DSLPATH CONTAINING THE PATH TO THE
C                 FILE AND INCREASING THE LENGTH OF THE VARIABLE DSNAME
C
C VERSION: 1.2                          DATE: 18-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - MODIFIED CONSTRUCTION OF DSLPATH
C
C VERSION: 1.3                          DATE: 22-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - REPLACED CALLS TO NAG ROUTINE E02BBF WITH CALL
C                 TO ADAS REPLACEMENT ROUTINE DXNBBF
C
C VERSION: 1.4                          DATE: 23-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - REPLACED CALL TO NAG ROUTINE E01BAF WITH CALL
C                 TO ADAS REPLACEMENT ROUTINE DXNBAF (NOTE THAT
C                 THIS IS NOT A DIRECT REPLACEMENT ROUTINE BUT
C                 HOPEFULLY IT WILL NOW GIVE THE SAME RESULTS)
C
C VERSION: 1.5                          DATE: 08-02-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - REMOVED SUPERFLUOUS VARIABLES
C
C VERSION: 1.6                          DATE: 08-02-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - CORRECTED NAME "TRING" TO "STRING"
C
C VERSION: 1.7  			DATE: 26/11/96  
C MODIFIED: HARVEY ANDERSON
C		 MODIFIED FORMAT STATEMENT 1001 AND THE READ
C		 STATEMENT BEGINING ON LINE 146 TO ACCOMODATE
C		 FOR THE NEW FORMAT OF THE ADF02 FILE 
C		 /SIA#H/SIA#H_RFM#H.DAT. ALSO MODIFIED THE
C		 DSNAME ON LINE 143. ALSO SLIGHTLY MODIFIED
C		 LINE 152 AND 153 DUE TO THE NEW FORMAT OF
C		 THE FILE. ALSO MODIFIED THE SELECT NUMBERS
C		 ASSOCIATED WITH THE SCALING.
C
C VERSION: 1.8				DATE: 03-04-97
C MODIFIED: HARVEY ANDERSON
C		 MODIFIED TO USE NEW PREFERRED ADF02 FILE sia#h_j97.dat
C
C VERSION: 1.9				DATE: 08-04-97
C MODIFIED: RICHARD MARTIN
C		 CHANGED NAME OF ADF02 FILE FROM sia#h_j97.dat TO
C		 sia#h_j97#h.dat
C
C VERSION: 1.10				DATE: 08-04-97
C MODIFIED: RICHARD MARTIN
C		 CORRECTED MISTAKE MADE IN PREVIOUS MODIFICATION
C
C VERSION: 1.11				DATE: 09-08-99
C MODIFIED: HARVEY ANDERSON
C		 CHANGED NAME OF THE ADF02 FILE FROM sia#h_j97#h.dat
C		 TO sia#h_j99#h.dat.
C
C VERSION: 1.12				DATE: 20-10-2003
C MODIFIED: Martin O'Mullane
C		 Save variables read from file between calls.
C		 
C VERSION: 1.13				DATE: 07-07-2004
C MODIFIED: ALLAN WHITEFORD
C 		-CHANGED CALLS FROM DXNB{A,B}F TO XXNB{A,B}F
C
C VERSION : 1.14			
C DATE    : 25-11-2010
C MODIFIED: Martin O'Mullane / Ephrem Delabie
C		 - Incorrect energy variable was used in integration
C                  for reactions 55, 56 and 57 because the required
C                  scaling was neglected.
C
C----------------------------------------------------------------------
       PARAMETER (NSTORE=80)
       DIMENSION XA(9),WXA(9),NA(NSTORE)
       DIMENSION SIGA(24,NSTORE),ERELA(24,NSTORE)
       DIMENSION EM1A(NSTORE),EM2A(NSTORE),ALFA(NSTORE)
       DIMENSION ETHRA(NSTORE),EA(24),OA(24),YA(24),FA(24)
       DIMENSION XSA(24),YSA(24),XKA(28),C(28),WTS(24)
CX     DIMENSION WRK(160)
       CHARACTER DSNAME*80, STRING*80, TITLF*80, DSLPATH*80
       DIMENSION B(24),A(28,4),DIAG(28)
C----------------------------------------------------------------------
C  GAUSS-LAGUERRE DATA (9PT)
C----------------------------------------------------------------------
       DATA XA/0.1523222277D0,0.8072200227D0,2.0051351556D0,
     &         3.7834739733D0,6.2049567778D0,9.3729852516D0,
     &         13.4662369110D0,18.8335977889D0,26.3740718909D0/
       DATA WXA/3.36126421798D-1,4.11213980424D-1,1.99287525371D-1,
     &          4.74605627657D-2,5.59962661079D-3,3.05249767093D-4,
     &          6.59212302608D-6,4.11076933035D-8,3.29087403035D-11/
C----------------------------------------------------------------------
C  XSECT STORAGE - NA(ISEL),EM1A(ISEL),EM2A(ISEL),ALFA(ISEL),
C                           ETHRA(ISEL),ISEL
C                  ERELA(IVAL,ISEL), SIGA(IVAL,ISEL)  WHERE
C                         NA=NUMBER OF ENERGIES FOR CROSS-SECTION
C                         EM1=1ST PARTICLE MASS (AT.MASS UNITS)
C                         EM2=2ND PARTICLE MASS (AT.MASS UNITS)
C                         ALF=HIGH ENERGY SLOPE (AS EREL**-ALF)
C                         ETHR=THRESHOLD ENERGY FOR REACTION (EV)
C                         EREL=RELATIVE ENERGY (EV/AMU)
C                         SIG=X-SECT (CM2)
C  WHERE THERE ARE UP TO 24 VALUES FOR EACH X-SECT
C
C  N.B. IT ASSUMED FOR POWER LAW INTERPOLATION PURPOSES THAT THE
C  CROSS-SECTION AT ERELA(1) IS FINITE.
C----------------------------------------------------------------------
       DATA LCK,LWRK/28,160/

C----------------------------------------------------------------------
       save ntrans, na, em1a, em2a, alfa, ethra, erela, siga
C----------------------------------------------------------------------

       IF(IPASS.LE.0)THEN
C----------------------------------------------------------------------
C  OPEN FILE AND READ IN
C----------------------------------------------------------------------
CX         DSNAME='/JETSHP.IONATOM.DATA(H)'
           CALL XXSLEN(DSLPATH,IFIRST,ILAST)
           DSNAME=DSLPATH(IFIRST:ILAST)//'/adf02/sia#h/sia#h_j99#h.dat'
CX         OPEN(UNIT=15,FILE=DSNAME,ACTION='READ')
           OPEN(UNIT=15,FILE=DSNAME,STATUS='UNKNOWN')
C
           READ (15,1000)NTRANS
           DO 150 ISTORE=1,NTRANS
            READ (15,1001)NA(ISTORE),EM1A(ISTORE),EM2A(ISTORE),
     &                    ALFA(ISTORE),ETHRA(ISTORE)
            READ (15,1002)(ERELA(J,ISTORE),J=1,NA(ISTORE) )
            READ (15,1002)(SIGA(J,ISTORE),J=1, NA(ISTORE) )
  150      CONTINUE
C
           IF(ISEL.LE.0.OR.ISEL.GT.NTRANS)THEN
    7          READ(15,1004,END=8)STRING
               WRITE(6,1005)STRING
               GO TO 7
    8          IPASS=0
               CLOSE(15)
               N=0
               QH=0.0D0
               ISEL=0
               RETURN
           ELSE
               IPASS=1
               CLOSE(15)
           ENDIF
       ENDIF
C-----------------------------------------------------------------------
C  EXTRACT DATA SELECTED BY ISEL AND CONVERT IN SCALED DATA CASES
C-----------------------------------------------------------------------
       TITLF='
     &                      '
       IZSEL=ZSEL+0.001
       N=NA(ISEL)
       DO 5 I=1,N
       OA(I)=SIGA(I,ISEL)
       EA(I)=ERELA(I,ISEL)
       YA(I)=ERELA(I,ISEL)
       IF(ISEL.EQ.53)THEN
           EA(I)=EA(I)/NSEL**2
           YA(I)=EA(I)
           OA(I)=OA(I)*ZSEL**2*NSEL**4
           WRITE(TITLF,1010)ISEL,IZSEL,NSEL
           EM2A(ISEL)=2.0*ZSEL
       ELSE IF(ISEL.EQ.57)THEN
       	   EA(I)=EA(I)/NSEL**2
       	   OA(I)=OA(I)*NSEL**4
           YA(I)=EA(I)
       ELSE IF(ISEL.EQ.54)THEN
           EA(I)=EA(I)*ZSEL**0.48D0/NSEL**2
           YA(I)=EA(I)
           OA(I)=OA(I)*ZSEL**1.07*NSEL**4
           EM2A(ISEL)=2.0*ZSEL
           WRITE(TITLF,1013)ISEL,IZSEL,NSEL
       ELSE IF((ISEL.EQ.46.OR.ISEL.EQ.47).AND.IZSEL.GT.0)THEN
           EA(I)=EA(I)*IZSEL
           YA(I)=EA(I)
           OA(I)=OA(I)*IZSEL
           EM2A(ISEL)=2.0*ZSEL
           WRITE(TITLF,1019)ISEL,IZSEL
       ELSE IF(ISEL.EQ.48.AND.IZSEL.GT.0.AND.NSEL.GT.0)THEN
           EA(I)=EA(I)*IZSEL
           YA(I)=EA(I)
           OA(I)=OA(I)*IZSEL/NSEL**3
           EM2A(ISEL)=2.0*ZSEL
           WRITE(TITLF,1020)ISEL,IZSEL,NSEL
       ELSE IF( ISEL .EQ. 56 ) THEN
	   EA(I)=EA(I)*ZSEL*0.25
           OA(I)=OA(I)*(ZSEL/2)**0.89
           YA(I)=EA(I)
           EM2A(ISEL)=2.0*ZSEL
       ELSE IF( ISEL .EQ. 55 ) THEN
           EA(I)=EA(I)*ZSEL*0.228
           OA(I)=OA(I)*(ZSEL/4)*1.05
           YA(I)=EA(I)
           EM2A(ISEL)=2.0*ZSEL
       ELSE
           WRITE(TITLF,1021)ISEL
       ENDIF
    5  CONTINUE
       QH=0.0D0
       IF(TTAR.LE.0.0D0)THEN
           V=1.38377D6*DSQRT(EPRO)
C-----------------------------------------------------------------------
C  INTERPOLATE USING SPLINES (NAG ALGORITHM)
C-----------------------------------------------------------------------
           IF(EPRO.LE.EA(1))THEN
               QH=V*OA(1)
           ELSEIF(EPRO.GE.EA(N))THEN
               QH=V*OA(N)*(EA(N)/EPRO)**ALFA(ISEL)
           ELSE
               DO 175 I=1,N
                   WTS(I)=1.0
                   XSA(I)=DLOG(EA(I))
  175              YSA(I)=DLOG(OA(I))
               IFAIL=0
CA
CA---------------------------------------------------------------------
CA REPLACED CALL TO NAG ROUTINE E01BAF WITH DXNBAF INSTEAD
CA - VARIABLE WTS REPRESENTS WEIGHTS - SET TO 1.0 TO GIVE ALL DATA
CA   SAME WEIGHT
CA - VARIABLE XKA REPRESENTS THE KNOTS - THESE ARE SET AS INDICATED
CA   IN THE NAG DOCUMENTATION FOR THE ORIGINAL ROUTINE E01BAF (NOTE
CA   THAT THE ROUTINE DXNBAF IS A DIRECT REPLACEMENT FOR E02BAF NOT
CA   E01BAF AND THEREFORE THE KNOTS HAVE TO BE PRESET BEFORE THE
CA   ROUTINE IS CALLED).
CA - SS CONTAINS THE RESIDUAL SUM OF SQUARES WHEN THE ROUTINE COMPLETES
CA - B,A AND DIAG ARE (I BELIEVE) VARIABLES WHICH SHOULD ACTUALLY ONLY
CA   BE INTERIOR TO THAT ROUTINE, BUT ARE INCLUDED HERE IN ORDER TO
CA   ALLOW THE CORRECT NUMBER OF ARGUMENTS FOR THE ROUTINE TO BE USED.
CA---------------------------------------------------------------------
CA
               DO 176, I=3,N-2
176                XKA(I+2) = XSA(I)
CX             CALL E01BAF(N,XSA,YSA,XKA,C,LCK,WRK,LWRK,IFAIL)
               CALL XXNBAF(N,N+4,XSA,YSA,WTS,XKA,B,A,DIAG,C,SS,IFAIL)
               XVAL=DLOG(EPRO)
CX             CALL E02BBF(N+4,XKA,C,XVAL,SVAL,IFAIL)
               CALL XXNBBF(N+4,XKA,C,XVAL,SVAL,IFAIL)
               QH=V*DEXP(SVAL)
           ENDIF
           RETURN
       ENDIF
C-----------------------------------------------------------------------
C  COMPUTE ALPHAS AND REDUCED SPEEDS
C-----------------------------------------------------------------------
       IF(IORD.EQ.1)EMT=EM2A(ISEL)
       IF(IORD.EQ.2)EMT=EM1A(ISEL)
       V=1.38377D6*DSQRT(EPRO)
       U=1.38377D6*DSQRT(TTAR/EMT)
       VTHR=1.38377D6*DSQRT(ETHRA(ISEL)*(EM1A(ISEL)+EM2A(ISEL))/
     &(EM1A(ISEL)*EM2A(ISEL)))
       X=V/U
       XRMIN=VTHR/U
       FA(N)=ALFA(ISEL)
       DO 10 I=1,N-1
       FA(I)=-DLOG(OA(I)/OA(I+1))/DLOG(YA(I)/YA(I+1))
   10  YA(I)=1.38377D6*DSQRT(YA(I))/U
       YA(N)=1.38377D6*DSQRT(YA(N))/U
       SUM=0.0D0
       DO 100 I=1,9
       XI=XA(I)
       SXI=DSQRT(XI)
       ISWIT=1
       IF(X.LT.XRMIN)ISWIT=2
       IF(X.EQ.0.0D0)ISWIT=3
       GO TO (30,50,70),ISWIT
   30  IF(SXI.GE.0.0D0.AND.SXI.LE.X-XRMIN)F=ABI(OA,YA,FA,X-SXI,X+SXI,N)
       IF(SXI.GT.X-XRMIN.AND.SXI.LE.X+XRMIN)F=ABI(OA,YA,FA,XRMIN,X+SXI,
     & N)
       IF(SXI.GT.X+XRMIN)F=ABI(OA,YA,FA,SXI-X,X+SXI,N)
       GO TO 100
C---------------------CHANGE 31/7/90------------------------------------
C  50  XXI=XI+XRMIN**2
C-----------------------------------------------------------------------
   50  XXI=XI+(XRMIN-X)**2
       SXXI=DSQRT(XXI)
C---------------------CHANGE 31/7/90------------------------------------
C      IF(SXXI.GE.XRMIN.AND.SXXI.LE.X+XRMIN)F=ABI(OA,YA,FA,XRMIN,SXXI+X,
C    & N)
C-----------------------------------------------------------------------
       IF(SXXI.GE.XRMIN-X.AND.SXXI.LE.X+XRMIN)F=ABI(OA,YA,FA,XRMIN-X,
     & SXXI+X,N)
       IF(SXXI.GT.X+XRMIN)F=ABI(OA,YA,FA,SXXI-X,X+SXXI,N)
       GO TO 100
   70  XXI=XI+XRMIN**2
       SXXI=DSQRT(XXI)
       K=0
   75  K=K+1
       IF(K.EQ.N+1)GO TO 80
       IF(SXXI.GT.YA(K))GO TO 75
   80  K=K-1
       IF(K.EQ.0)K=1
       F=XXI*OA(K)*(YA(K)/SXXI)**(2.0D0*FA(K))
       GO TO 100
  100  SUM=SUM+F*WXA(I)
       GO TO (110,120,130),ISWIT
  110  QH=5.64190D-1*U*SUM/X
       RETURN
C---------------------CHANGE 31/7/90------------------------------------
C 120  QH=5.64190D-1*U*DEXP(-XRMIN*XRMIN)*SUM/X
C-----------------------------------------------------------------------
  120  QH=5.64190D-1*U*DEXP(-(XRMIN-X)**2)*SUM/X
       RETURN
  130  QH=1.12838D0*U*DEXP(-XRMIN*XRMIN)*SUM
       RETURN
C
 1000  FORMAT(I5)
 1001  FORMAT(13X,I2,3X,1E9.0,3X,1E9.0,3X,1E9.0,3X,1E9.0)
 1002  FORMAT(1P,6D10.3)
 1004  FORMAT(1A80)
 1005  FORMAT(1H ,1A80)
 1006  FORMAT(10I3)
 1007  FORMAT('ISEL=',I2,'  DATA SCALED FOR EXCITATION TO H(N=',
     &I1,') BY H+')
 1008  FORMAT('ISEL=',I2,'  DATA SCALED FOR EXCITATION TO H(N=',
     &I1,') BY C+6')
 1009  FORMAT('ISEL=',I2,'   DATA SCALED FOR EXCITATION TO H(N=',
     &I1,') BY Z0= ',I2)
 1010  FORMAT('ISEL=',I2,'  DATA SCALED FOR TRANSFER TO Z0=',
     &I2,'FROM H(N=',I2,')')
 1011  FORMAT('ISEL=',I2,'  DATA SCALED FOR TRANSFER TO H+  FROM H(N=',
     &I2,')')
 1012  FORMAT('ISEL=',I2,' DATA SCALED FOR TRANSFER TO C+6  FROM H(N=',
     &I2,')')
 1013  FORMAT('ISEL=',I2,'  DATA SCALED FOR TRANSFER TO Z0=',
     &I2,' FROM H(N=',I2,')')
 1015  FORMAT('ISEL=',I2,'  DATA SCALED FOR EXCITATION BY HE+2 TO H(N=',
     &         I2,')')
 1016  FORMAT('ISEL=',I2,'  DATA SCALED FOR EXCITATION BY BE+4 TO H(N=',
     &         I2,')')
 1017  FORMAT('ISEL=',I2,'  DATA SCALED FOR EXCITATION BY  C+6 TO H(N=',
     &         I2,')')
 1018  FORMAT('ISEL=',I2,'  DATA SCALED FOR EXCITATION BY  O+8 TO H(N=',
     &         I2,')')
 1019  FORMAT('ISEL=',I2,'  DATA SCALED FOR EXCITATION BY  Z0=',
     &         I2)
 1020  FORMAT('ISEL=',I2,'  DATA SCALED FOR EXCITATION BY  Z0=',
     &         I2,' TO H(N=',I2,')')
 1021  FORMAT('ISEL=',I2)
      END
