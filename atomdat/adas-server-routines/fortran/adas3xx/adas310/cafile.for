CX UNIX PORT - SCCS Info : Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas310/cafile.for,v 1.1 2004/07/06 11:58:31 whitefor Exp $ Date $Date: 2004/07/06 11:58:31 $
CX
      SUBROUTINE CAFILE( IUNIT , IZ0 , IRCODE , DSNAME , DLPATH)
C
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: CAFILE *********************
C
C  PURPOSE: TO OPEN AN IONIZATION RATE-COEFFT 'IONELEC' DATA SET
C           BY DEFAULT, OR AN ALTERNATIVE DATA SET IF REQUIRED, FOR
C           IONIZING ION WITH NUCLEAR CHARGE 'IZ0' 
C           THIS WILL BE CONNECTED TO UNIT 'IUNIT'.
C
C   DATA SET OPENED: DLPATH/adf07/<DEFADF>/<GROUP>(OPTIONAL)/<TYPE>/
C			 <GROUP_EXT>#<ELEMENT SYMBOL>'
C
C  CALLING PROGRAM: CASSZD
C
C  SUBROUTINE:
C
C  INPUT : (I*4)   IUNIT   = UNIT TO WHICH DATA SET WILL BE CONNECTED
C  INPUT : (I*4)   IZ0     = NUCLEAR CHARGE OF EMITTING ION REQUESTED
C  INPUT : (C*80) DLPATH  = PATH NAME TO THE RELEVANT DATA FILES
C
C  OUTPUT: (I*4)   IRCODE  = RETURN CODE FROM SUBROUTINE:
C                            0 => DATA SET SUCCESSFULLY CONNECTED
C                            1 => REQUESTED DATA  SET  MEMBER  DOES  NOT
C                                 EXISTS - DATA SET NOT CONNECTED.
C                            9 => REQUESTED DATA  SET  EXISTS BUT CANNOT
C                                 BE OPENED.
CX OUTPUT: (C*44)  DSNAME  = FULL MVS NAME OF OPENED DATA SET
C  OUTPUT: (C*80)  DSNAME  = NAME OF OPENED DATA SET UNDER UNIX
C
C          (I*4)   IDLEN   = LENGTH, IN BYTES, OF FIXED 'DSNAME' PREFIX
C          (I*4)   LENF1   = FIRST NON-BLANK CHR OF 'DSNAME' GROUP PART
C          (I*4)   LENF2   = LAST  NON-BLANK CHR OF 'DSNAME' GROUP PART
C          (I*4)   LENF3   = FIRST NON-BLANK CHR OF 'DSNAME' TYPE PART
C          (I*4)   LENF4   = LAST  NON-BLANK CHR OF 'DSNAME' TYPE PART
C          (I*4)   LENF5   = LAST  NON-BLANK CHR OF 'DSNAME' USERID PART
C          (I*4)   LENF6   = LAST  NON-BLANK CHR OF 'DSNAME' USERID PART
C          (I*4)   LENF7   = LAST  NON-BLANK CHR OF 'DSNAME' EXTENSION PART
C          (I*4)   LENF8   = LAST  NON-BLANK CHR OF 'DSNAME' EXTENSION PART
CA         (C*1)   HASH    = '#'   IF NON-BLANK EXT, ELSE ' '.
C          (C*2)   XFESYM  = FUNCTION - (SEE ROUTINES SECTION BELOW)
C          (C*2)   ESYM    = ELEMENT SYMBOL FOR NUCLEAR CHARGE 'IZ0'
C          (C*3)   USREXT  = ADAS SOURCE DATA FILE EXTENSION
CA         (C*80)  USERID  = ADAS SOURCE DATA USER ID
C          (C*8)   USRGRP  = ADAS SOURCE DATA GROUPNAME
CA         (C*80)  USRTYP  = ADAS SOURCE DATA TYPENAME
C	   (C*5)   DEFADF  = DEFAULT DATA DIRECTORY, I.E. ADF13
C
C          (L*4)   LEXIST  = .TRUE.  => REQUESTED  DATA  SET  EXISTS.
C                            .FALSE. => REQUESTED  DATA  SET  DOES  NOT
C                                       EXIST.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C	   XXUID      ADAS      FETCHES/SETS ADAS SOURCE DATA USER ID
C          XXSSZD     ADAS      FETCHES/SETS ADAS SOURCE DATA FILENAME
C                               AND FILE EXTENSION
C          XFESYM     ADAS      CHARACTER*2 FUNCTION -
C                               GATHERS ELEMENT SYMBOL FOR NUC. CHARGE
C	   XXSLEN     ADAS      FINDS FIRST AND LAST NON-BLANK
C				CHARACTERS IN STRING.
C
C AUTHOR:  H.P. SUMMERS
C	   K1/1/57
C	   JET EXT. 4941
C DATE:    11/10/91
C
C UPDATE:  10/03/93 - PE BRIDEN: ADDED CALL TO XXUID AND USERID VARIABLE
C                                - NOW ALLOWS ANY INPUT DATASET USER ID.
C UPDATE:   2/09/93 - HPS      : ADDED CALL TO XXSSZD AND USRGRP, USRTYP
C                                  AND USREXT NAMES
C                                - NOW ALLOWS ANY INPUT DATASET FILENAME
C                                  AND EXTENSION
C UPDATE:  23/11/93 - PEB      : CORRECT ERROR - A '.' HAD MISTAKENLY
C                                BEEN PLACED BEFORE THE MEMBER NAME IN
C                                VARIABLE DSNAME.
C
C UPDATE:  10/11/94 - L. JALOTA: MODIFIED CODE FOR RUNNING UNDER UNIX
C				 USING NEW FILE NAMING CONVENTION.
C				 "ACTION" KEYWORD IN OPEN COMMAND IS IBM
C				 SO REMOVED HERE.
C 				 ADDED DEFADF 
C UPDATE:  22/11/94 - L. JALOTA: TIDIED UP CHARACTER LENGTH DEFINITIONS
C
C UPDATE:  24/03/95 - HPS      : INTRODUCED HASH TO ELIMINATE # IN FILE IF
C                                THERE IS NO EXTENSION PART OF THE FILE NAME
C                                ALTER LOGIC TO ALLOW USRTYP, USREXT TO BE A
C                                SINGLE CHARACTER.
C
C VERSION: 1.1                          DATE: 08-02-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C-----------------------------------------------------------------------
      INTEGER     IUNIT         , IZ0            , 
     &            IRCODE        , IDLEN          ,
     &            LENF1         , LENF2          ,
     &            LENF3         , LENF4		 , 
     &		  LENF5         , LENF6          ,
     &	          LENF7	        , LENF8
      INTEGER     IF1           ,
     &            IL1           
C-----------------------------------------------------------------------
      CHARACTER   XFESYM*2      , ESYM*2         ,
     &            USERID*80     , DSNAME*80      , DEFADF*5   ,
     &            USRGRP*8      , USRTYP*80      , USREXT*3   ,
     &            HASH*1        , DLPATH*80      
C-----------------------------------------------------------------------
      LOGICAL     LEXIST
C-----------------------------------------------------------------------
C SET DEFAULT DIRECTORY.
      PARAMETER (DEFADF = 'adf07')
C
C-----------------------------------------------------------------------
C
C BUILD FIRST PART OF THE FILE NAME - NOTE THAT THE FILE IS DRAWN
C FROM THE -CENTRAL- ADAS DATA FILES.
C-----------------------------------------------------------------------
C

C
C-----------------------------------------------------------------------
C IDENTIFY ADAS SOURCE DATA USER ID, GROUPNAME , TYPENAME
C AND FILE EXTENSION
C NOTE THAT UNDER UNIX TYPENAME IS AN OPTIONAL SUBDIRECTORY.
C-----------------------------------------------------------------------
C
      USERID = '?'
      CALL XXUID(USERID)
C
      USRGRP = '?'
      USRTYP = '?'
      USREXT = '?'
      CALL XXSSZD(USRGRP , USRTYP , USREXT)
C
C-----------------------------------------------------------------------
C FIND THE FIRST AND LAST NON-BLANK CHARACTER STRINGS IN USRGRP & USRTYP
CX AND USERID AND USREXT
C-----------------------------------------------------------------------
C
      CALL XXSLEN( USRGRP , LENF1 , LENF2 )
      CALL XXSLEN( USRTYP , LENF3 , LENF4 )
      CALL XXSLEN( USERID , LENF5 , LENF6 )
      CALL XXSLEN( USREXT , LENF7 , LENF8 )
      CALL XXSLEN( DLPATH , IF1   , IL1   )
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
CX    DSNAME = USERID(LENF5:LENF6)//'/'//DEFADF//'/'//
CX   &            USRGRP(LENF1:LENF2)//'/'
      DSNAME = DLPATH(IF1:IL1)//'/adf07/ionelec/'
      IDLEN  = INDEX(DSNAME,' ') - 1
C
      IF (LENF7.EQ.0) THEN
          HASH = ' '
      ELSE
          HASH = '#'
      ENDIF
C
      IF (LENF3.EQ.0) THEN 
	 DSNAME = DSNAME(1:IDLEN)//USRGRP(LENF1:LENF2)//'_'
     &      //USREXT(LENF7:LENF8)//HASH
      ELSE 
	 DSNAME = DSNAME(1:IDLEN)//USRTYP(LENF3:LENF4)//'/'
     &      //USRGRP(LENF1:LENF2)//'_'//USREXT(LENF7:LENF8)//HASH
      ENDIF
C
      IDLEN  = INDEX(DSNAME,' ') - 1
      ESYM   = XFESYM( IZ0 )
C
         IF (IRCODE.EQ.0) THEN
C
            IF ( ESYM(2:2) .EQ. ' ' ) THEN
               DSNAME = DSNAME(1:IDLEN)//ESYM(1:1)//'.dat'
            ELSE
               DSNAME = DSNAME(1:IDLEN)//ESYM(1:2)//'.dat'
            ENDIF
C
            INQUIRE( FILE=DSNAME , EXIST=LEXIST )
C
               IF (.NOT.LEXIST) THEN
                  IRCODE = 1
               ELSE
		  OPEN(UNIT=IUNIT,FILE=DSNAME,STATUS='OLD',ERR=9999)
                  IRCODE = 0
               ENDIF
C
         ENDIF
      RETURN
C
C-----------------------------------------------------------------------
C OPEN STATEMENT ERROR HANDLING
C-----------------------------------------------------------------------
C
 9999 IRCODE = 9
      RETURN
C
C-----------------------------------------------------------------------
C
 1000 FORMAT(I1,' ')
 1001 FORMAT(I2)
C
C-----------------------------------------------------------------------
C
      END
