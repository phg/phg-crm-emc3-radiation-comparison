CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas310/initpos.for,v 1.1 2004/07/06 14:09:05 whitefor Exp $ Date $Date: 2004/07/06 14:09:05 $
CX
      SUBROUTINE INITPOS(NTA,M)                                         
C                                                                      
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: INITPOS ********************
C
C PURPOSE UNKNOWN
C
C NOTES: THIS ROUTINE IS NOT YET PROPERLY ANNOTATED
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 01-02-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER SCCS CONTROL
C
C-----------------------------------------------------------------------
C
      INTEGER NTA(*)                                                  
      DO 100 I=1,M                                                   
        NTA(I)=I                                                    
 100  CONTINUE                                                     
      RETURN                                                      
C                                                                
      END                                                       
