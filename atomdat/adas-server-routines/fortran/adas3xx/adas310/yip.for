CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas310/yip.for,v 1.1 2004/07/06 15:40:55 whitefor Exp $ Date $Date: 2004/07/06 15:40:55 $
CX
       FUNCTION YIP(XI,DELTA)                                 
C
       IMPLICIT REAL*8 (A-H,O-Z)                             
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 FUNCTION: YIP **************************
C
C PURPOSE UNKNOWN
C
C NOTES: THIS ROUTINE IS NOT YET PROPERLY ANNOTATED
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 16-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C-----------------------------------------------------------------------
C
       DIMENSION A(20),B(20)                                           
       XM=DABS(XI)                                                    
       X=XM+DELTA                                                    
       IF(X-0.1)1,1,2                                               
    1  T=DLOG(1.1229/X)                                            
       Y=T+0.25*X*X*(1.0-2.0*T*T)                                 
       GO TO 5                                                   
    2  IF(X-2.0)4,3,3                                           
    3  T=1.0/X                                                 
       Y=1.5707963*DEXP(-2.0*X)*(1.0+T*(0.25+T*(-0.09375+T*(0.0703125   
     2  -T*0.0472))))                            
       GO TO 5                                  
    4  A(1)=2.3916                             
       A(2)=1.6742                            
       A(3)=1.2583                           
       A(4)=0.9738                          
       A(5)=0.7656                         
       A(6)=0.6078                        
       A(7)=0.48561                      
       A(8)=0.38976                     
       A(9)=0.31388                    
       A(10)=0.25342                  
       A(11)=0.20501                 
       A(12)=0.16610                
       A(13)=0.13476               
       A(14)=0.10944              
       A(15)=0.08896             
       A(16)=0.07237                                                   
       A(17)=0.058903                                                 
       A(18)=0.047971                                                
       A(19)=0.039086                                               
       A(20)=0.031860                                              
       B(1)=1.0091                                                
       B(2)=0.3015                                               
       B(3)=0.1314                                              
       B(4)=0.0763                                             
       B(5)=0.0504                                            
       B(6)=0.03561                                          
       B(7)=0.02634                                         
       B(8)=0.01997                                        
       B(9)=0.01542                                       
       B(10)=0.01205                                     
       B(11)=0.00950                                    
       B(12)=0.00757                                   
       B(13)=0.00602                                  
       B(14)=0.00484                                 
       B(15)=0.00389                                
       B(16)=0.00312                               
       B(17)=0.002535                             
       B(18)=0.002047    
       B(19)=0.001659     
       B(20)=0.001344      
       J=10.0*X             
       X0=J                  
       X0=0.1*X0              
       R=10.0*(X-X0)           
       S=1.0-R                  
       Y=R*A(J+1)+S*A(J)+.16666667*(R*(R*R-1.)*B(J+1)+S*(S*S-1.)*B(J))  
       YIP=Y                     
       IF(XM-0.01)6,6,5           
    5  X=XM*XM/(XM+DELTA)          
       X2=0.125*XM*XM/(XM+3.0*DELTA)
       YIP=Y*DEXP(3.1416*XM-0.9442*X*(1.0+X*(2.14+14.2*X))/(1.0+X*(2.44 
     2  +12.8*X)))/(1.0+X2*X2)       
    6  IF(XI)7,8,8                    
    7  YIP=YIP*DEXP(6.283*XI)          
    8  RETURN                           
       END                               
