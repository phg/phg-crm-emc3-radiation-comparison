CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas310/spij3.for,v 1.1 2004/07/06 15:22:11 whitefor Exp $ Date $Date: 2004/07/06 15:22:11 $
CX
       SUBROUTINE SPIJ3(N,H,W)                                          
C
       IMPLICIT REAL*8(A-H,O-Z)                                        
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: SPIJ3 **********************
C
C PURPOSE UNKNOWN
C
C NOTES: THIS ROUTINE IS NOT YET PROPERLY ANNOTATED
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 18-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - PUT UNDER SCCS CONTROL
C
C-----------------------------------------------------------------------
C
       DIMENSION A(10),B(10),C(10),H(10),W(10,10)                     
       X=0.0                                                         
       X3=0.0                                                       
       H(N)=1.0D72                                                 
       DO 10 I=1,N                                                
       DO 12 J=1,N                                               
   12  W(I,J)=0.0                                               
       Y=1.0/H(I)                                              
       Y3=3.0*Y*Y                                             
       A(I)=X                                                
       B(I)=2.0*(X+Y)                                       
       C(I)=Y                                              
       IF(I.EQ.1)GO TO 13                                 
       W(I,I-1)=-X3                                      
   13  W(I,I)=X3-Y3                                     
       IF(I.EQ.N)GO TO 14                              
       W(I,I+1)=Y3                                    
   14  X=Y                                           
   10  X3=Y3                                        
C  SWITCH TO ZERO GRADIENT AT FIRTST POINT         
       C(1)=0.0D0                                 
       W(1,1)=0.0D0                              
       W(1,2)=0.0D0                             
       H(N)=0.0                                
       DO 20 I=2,N                            
       X=A(I)/B(I-1)                         
       B(I)=B(I)-X*C(I-1)                   
       DO 15 J=1,I                         
   15  W(I,J)=W(I,J)-X*W(I-1,J)           
   20  CONTINUE                          
       X=1.0/B(N)                       
       DO 23 I=1,N                     
   23  W(N,I)=X*W(N,I)                
       DO 30 I=2,N                   
       K=N-I+1                      
       X=1.0/B(K)                  
       DO 25 J=1,N                
   25  W(K,J)=X*(W(K,J)-C(K)*W(K+1,J))                
   30  CONTINUE                                      
       RETURN                                       
       END                                         
