CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas310/colint.for,v 1.2 2004/07/06 12:07:54 whitefor Exp $ Date $Date: 2004/07/06 12:07:54 $
CX
       SUBROUTINE COLINT(Y,Z,EN,AI)          
C
       IMPLICIT REAL*8 (A-H,O-Z)     
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: COLINT *********************
C
C PURPOSE UNKNOWN
C
C NOTES: THIS ROUTINE IS NOT YET PROPERLY ANNOTATED
C
C UNIX-IDL PORT:
C
C VERSION: 1.1                          DATE: 16-1-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - FIRST VERSION
C
C VERSION: 1.2                          DATE: 08-02-96
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C               - REPLACED 0.0 ARGUMENT IN CALL TO YIP
C                 WITH A DUMMY VARIABLE INSTEAD.
C
C-----------------------------------------------------------------------
C
       DIMENSION X(5),W(5)                                              
       J1=5                                                            
       X(1)=0.26356                                                   
       X(2)=1.41340                                                  
       X(3)=3.59643                                                 
       X(4)=7.08581                                                
       X(5)=12.64080                                              
       W(1)=0.521756                          
       W(2)=0.398667                           
       W(3)=0.0759424                           
       W(4)=0.00361176                           
       W(5)=0.00002337                            
       AI=0.0                                      
       DO 1 J=1,J1                                  
       V=X(J)                                        
       B=V/Y                                          
       B1=B+1.0                                        
       C=DSQRT(B1)                                      
       R=(1.25*EN*EN+0.25)/Z                             
       DELTA=(Z/EN)*(R+2.0*EN*EN*C/((B+2.0)*Z*Z))/(C+DSQRT(B))         
       C1=1.0/(B+2.0)                                     
       F=1.0                                               
       ZERO = 0.0
       C4=YIP(ZERO,DELTA)                                    
       C2=(C1*(B-B1*C1*DLOG(B1))+0.65343*(1.0-1.0/(B1**3))*C4/EN)*F     
       AI=AI+W(J)*C2                                         
       Q=4.0*C2/B1                                            
    1  CONTINUE                                                
       RETURN                                                   
       END                                                        
