CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas309/c9ispf.for,v 1.4 2004/07/06 11:56:55 whitefor Exp $ Date $Date: 2004/07/06 11:56:55 $
CX
      SUBROUTINE C9ISPF( LPEND, IBATCH )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C9ISPF *********************
C
C  PURPOSE: PIPE COMMS WITH IDL FOR THE PROCESSING OPTIONS OF 309.
C
C  CALLING PROGRAM: ADAS309
C
C  I/O   : (L*4)   LPEND    = FLAGS IF END OF ANALYSIS REQUESTED.
C                             .TRUE.  => END ANALYSIS OF CURRENT DATA
C                                        SETS
C                             .FALSE. => CONTINUE PANALYSIS WITH CURRENT
C                                        DATA SETS
C
C
C          (I*4)   PIPEIN - PARAMETER = UNIT NUMBER FOR INPUT FROM PIPE
C          (I*4)   LOGIC  - USED TO PIPE LOGICAL VALUES
C	   (I*4)   IBATCH - USED TO FLAG ACTION TO OTHER ROUTINES (
C			    AS LPEND CAN NOW HAVE 3 VALUES WHICH IT IS
C                           NOT POSSIBLE TO TREAT WITH A LOGICAL 
C                           VARIABLE)
C
C ROUTINES:   NONE
C
C AUTHOR:  JONATHAN NASH  (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    03/12/93
C
C Modified: Tim Hammond (Tessella Support Services plc.)
C           Preliminary UNIX port - major revisions to follow
C
C Version 1.1			21-06-95
C
C Modified: Tim Hammond (Tessella Support Services plc.)
C		- Removed references to ISPF panel IPAN.
C		- Removed other ISPF variables and features.
C		- Removed batch/no-batch flag LBTSEL
C		- Added all reads from IDL (c9ispf.pro)
C
C Version 1.2			22-06-95
C
C Modified: Tim Hammond (Tessella Support Services plc.)
C               - Added pipein parameter
C
C Version 1.3			28-06-95
C
C Modified: Tim Hammond (Tessella Support Services plc.)
C               - Rewrite of the routine - it now only reads    
C                 whether to carry on or not.
C		  The bulk of the IDL -> FORTRAN data transfer is
C		  now handled by routines under adas309b.for
C
C Version 1.4			10-07-95
C
C Modified: Tim Hammond (Tessella Support Services plc.)
C               - Added IBATCH parameter which is used to signal 
C                 'Cancel', 'Run Now', 'Run in Batch' to
C                 other routines.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    PIPEIN  , LOGIC
      INTEGER    IBATCH
      PARAMETER (PIPEIN=5)
C-----------------------------------------------------------------------
      LOGICAL    LPEND   
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C READ OUPUTS FROM PIPE
C-----------------------------------------------------------------------
C
      READ( PIPEIN, *) LOGIC
      IBATCH = LOGIC
      IF (LOGIC.EQ.1 .OR. LOGIC.EQ.2) THEN
          LPEND = .TRUE.
      ELSE
          LPEND = .FALSE.
      ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN
      END
