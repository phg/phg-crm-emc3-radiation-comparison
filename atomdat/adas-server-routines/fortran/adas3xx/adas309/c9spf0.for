CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas309/c9spf0.for,v 1.3 2004/07/06 11:57:24 whitefor Exp $ Date $Date: 2004/07/06 11:57:24 $
CX
      SUBROUTINE C9SPF0( REP    , DSFULL  )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C9SPF0 *********************
C
C  PURPOSE: PIPE COMMUNICATION WITH IDL
C
C  CALLING PROGRAM: ADAS309
C
C  SUBROUTINE:
C
C  OUTPUT: (C*3)   REP     = 'YES' => TERMINATE PROGRAM EXECUTION.
C                          = 'NO ' => CONTINUE PROGRAM EXECUTION.
C
C  OUTPUT: (C*80)  DSFULL  = INPUT DATA SET NAME (FULL UNIX PATHNAME)
C
C          (I*4)   PIPEIN   = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)   PIPEOU   = PARAMETER = UNIT NUMBER FOR INPUT TO PIPE
C
C
C ROUTINES:
C	   NONE
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    03/12/93
C
C Modified: Tim Hammond (Tessella Support Services plc.)
C           Preliminary UNIX port - major revisions to follow
C
C Date:    21/06/95		version 1.1
C 
C Modified: Tim Hammond (Tessella Support Services plc.)
C           UNIX port - reduced function of program considerably.
C           It now has the sole task of reading in the name of
C           the input file and 'cancel/done' from IDL.
C
C Date:    21/06/95		version 1.2
C
C Modified: Tim Hammond (Tessella Support Services plc.)
C           Removed reference to LDSEL in first line
C
C Date:    21/06/95             version 1.3
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      CHARACTER   REP*3         , DSFULL*80
C-----------------------------------------------------------------------
      INTEGER     PIPEIN        , PIPEOUT
      PARAMETER(  PIPEIN=5      , PIPEOUT=6)
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C  READ OUTPUTS FROM PIPE
C-----------------------------------------------------------------------
C
      READ(PIPEIN,'(A)') REP
      READ(PIPEIN,'(A)') DSFULL
C
C-----------------------------------------------------------------------
C
      RETURN
      END
