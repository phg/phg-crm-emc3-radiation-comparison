CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas309/c9wfil.for,v 1.2 2004/07/06 11:58:14 whitefor Exp $ Date $Date: 2004/07/06 11:58:14 $
CX
      SUBROUTINE C9WFIL( MXNSHL , IZ1    , NGRND  , NTOT   ,
     &                   NBOT   , NUMAX  , DENSZ  , DENS   ,
     &                   QTHEOR , FTHEOR , TBQMEP , TBQMEM ,
     &                   TBQMIP , TBQMIM , WHIGH  , WLOW
     &                 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C9WFIL *********************
C
C  PURPOSE:
C
C  CALLING PROGRAM: C9EMIS
C
C  INPUT : (I*4)  MXNSHL   = MAXIMUM VALUE OF PRINCIPAL QUANTUM NUMBER.
C  INPUT : (I*4)  IZ1      = ION CHARGE.
C  INPUT : (I*4)  NGRND    = PRINCIPAL QUANTUM NUMBER OF GROUND STATE.
C  INPUT : (I*4)  NTOT     = PRINCIPAL QUANTUM NUMBER OF HIGHEST BOUND
C                            STATE.
C  INPUT : (I*4)  NBOT     = MINIMUM PRINCIPAL QUANTUM NUMBER.
C  INPUT : (I*4)  NUMAX    = MAXIMUM UPPER PRINCIPAL QUANTUM NUMBER FOR
C                            OBSERVED SPECTRUM LINES.
C  INPUT : (R*8)  DENSZ    = PLASMA ION DENSITY.
C                            UNITS: CM-3
C  INPUT : (R*8)  DENS     = ELECTRON DENSITY.
C                            UNITS: CM-3
C  INPUT : (R*8)  QTHEOR() = MEAN CHARGE EXCHANGE OR EXCITATION RATE
C                            COEFFICIENTS FOR N-LEVELS AVERAGED OVER
C                            BEAM FRACTIONS.
C                            UNITS: CM3 SEC-1
C                            DIMENSION: REFERENCED BY N QUANTUM NUMBER.
C  INPUT : (R*8)  FTHEOR() = MEAN CHARGE EXCHANGE OR EXCITATION RATE
C                            FOR NL-LEVELS AS A FRACTION OF
C                            CORRESPONDING N-LEVEL.
C                            DIMENSION: REFERENCED BY FUNC I4IDFL(N,L).
C  INPUT : (R*8)  TBQMEP() = ELECTRON RATE COEFFT. FOR NL->NL+1.
C                            INDEX FOR NL->NL+1 TRANSITION GIVEN BY
C                            I4IDFL(N,L).
C                            DIMENSION: REFERENCED BY FUNC I4IDFL(N,L).
C  INPUT : (R*8)  TBQMEM() = ELECTRON RATE COEFFT. FOR NL+1->NL.
C                            INDEX FOR NL+1->NL TRANSITION GIVEN BY
C                            I4IDFL(N,L+1).
C                            DIMENSION: REFERENCED BY FUNC I4IDFL(N,L).
C  INPUT : (R*8)  TBQMIP() = POSITIVE ION RATE COEFFT. FOR NL->NL+1.
C                            INDEX FOR NL->NL+1 TRANSITION GIVEN BY
C                            I4IDFL(N,L).
C                            DIMENSION: REFERENCED BY FUNC I4IDFL(N,L).
C  INPUT : (R*8)  TBQMIM() = POSITIVE ION RATE COEFFT. FOR NL+1->NL.
C                            INDEX FOR NL+1->NL TRANSITION GIVEN BY
C                            I4IDFL(N,L+1).
C                            DIMENSION: REFERENCED BY FUNC I4IDFL(N,L).
C
C  OUTPUT: (R*8)  WHIGH()  =
C                            DIMENSION: REFERENCED BY L+1.
C  OUTPUT: (R*8)  WLOW(,)  =
C                            1ST DIMENSION: REFERENCED BY I4IDFL(N,L).
C                            2ND DIMENSION: REFERENCED BY L+1.
C
C  PARAM : (I*4)  MXN      = MXNSHL.
C
C          (I*4)  N       = LOOP INDEX FOR PRINCIPAL QUANTUM NUMBER.
C          (I*4)  L       = LOOP INDEX FOR ORBITAL QUANTUM NUMBER.
C          (I*4)  N1      = LOOP INDEX FOR PRINCIPAL QUANTUM NUMBER.
C          (I*4)  L1      = LOOP INDEX FOR ORBITAL QUANTUM NUMBER.
C          (I*4)  N2      = LOOP INDEX FOR PRINCIPAL QUANTUM NUMBER.
C          (I*4)  L2      = LOOP INDEX FOR ORBITAL QUANTUM NUMBER.
C          (I*4)  LP      = ARRAY INDEX = L+1.
C          (I*4)  IDL     = ARRAY INDEX.
C
C          (R*8)  A1      = LOCAL STORE FOR RETURN VALUE OF FUNC R8ATAB.
C
C          (R*8)  RHS()   = RIGHT HAND SIDE OF N LEVEL EQUATION.
C                           DIMENSION: MXN.
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          I4UNIT     ADAS      RETURNS UNIT NO. FOR OUTPUT OF MESSAGES.
C          I4IDFL     ADAS      RETURNS UNIQUE INDEX GIVEN QUANTUM
C                               NUMBERS N AND L.
C          R8ATAB     ADAS      RETURNS HYDRONIC L-RESOLVED A-VALUES.
C                               IF INPUT QUANTUM NUMBERS ARE INVALID
C                               THEN RETURNS ZERO.
C          CXWSOL     ADAS
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/81
C          JET EXT. 5183
C
C DATE:    13/10/93
C
C DATE:    30/06/95		VERSION:1.1
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C            - UNIX PORT FOR ADAS309, CREATED THIS C9WFIL FROM C8WFIL
C              WHICH COMMUNICATED VIA UNIX PIPE WITH IDL. REMOVED THIS.
C
C DATE:    10/07/95             VERSION:1.2
C MODIFIED: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C            - TIDIED CODE AND COMMENTS UP.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    I4UNIT  , I4IDFL
      REAL*8     R8ATAB
C-----------------------------------------------------------------------
      INTEGER    MXN
      PARAMETER( MXN = 20 )
C-----------------------------------------------------------------------
      INTEGER    MXNSHL  , IZ1     , NGRND   , NTOT   , NBOT    ,
     &           NUMAX
      INTEGER    N       , L       , N1      , L1     , N2      ,
     &           L2      , LP      , IDL
C-----------------------------------------------------------------------
      REAL*8     DENSZ   , DENS
      REAL*8     A1
C-----------------------------------------------------------------------
      REAL*8     QTHEOR(MXNSHL)                 ,
     &           FTHEOR((MXNSHL*(MXNSHL+1))/2)  ,
     &           TBQMEP((MXNSHL*(MXNSHL+1))/2)  ,
     &           TBQMEM((MXNSHL*(MXNSHL+1))/2)  ,
     &           TBQMIP((MXNSHL*(MXNSHL+1))/2)  ,
     &           TBQMIM((MXNSHL*(MXNSHL+1))/2)  ,
     &           WHIGH((MXNSHL*(MXNSHL+1))/2)
      REAL*8     RHS(MXN)
C-----------------------------------------------------------------------
      REAL*8     WLOW((MXNSHL*(MXNSHL+1))/2,MXNSHL)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C CHECK PARAMETER 'MXN'.
C-----------------------------------------------------------------------
C
      IF (MXN .LT. MXNSHL) THEN
         WRITE(I4UNIT(-1),1000) MXN, MXNSHL
         STOP
      ENDIF
C
C-----------------------------------------------------------------------
C INITIALISE VECTOR WHIGH.
C-----------------------------------------------------------------------
C
      DO 1 N = NUMAX+1 , NTOT
         DO 2 L = 0 , N-1
            IDL = I4IDFL( N , L )
            WHIGH(IDL) = QTHEOR(N) * FTHEOR(IDL) / QTHEOR(NUMAX)
    2    CONTINUE
    1 CONTINUE
C
C-----------------------------------------------------------------------
C INITIALISE WLOW DIAGONAL VECTORS AND ZERO OTHER COLUMNS.
C-----------------------------------------------------------------------
C
      DO 3 N = 1 , NUMAX
         DO 4 L = 0 , N-1
            IDL = I4IDFL( N , L )
            DO 5 N1 = N+1 , NUMAX
               WLOW(IDL,N1) = 0.0D0
    5       CONTINUE
            WLOW(IDL,N) = FTHEOR(IDL)
    4    CONTINUE
    3 CONTINUE
C
C-----------------------------------------------------------------------
C MAIN DOWNWARD RECURSION FROM MXNSHL TO NUMAX+1.
C-----------------------------------------------------------------------
C
      DO 6 N = MXNSHL , NUMAX+1 , -1
C
C-----------------------------------------------------------------------
C COPY WHIGH COMPONENTS INTO RHS AND ADD CASCADE CONTRIBUTIONS FROM
C LEVELS GREATER THAN N.
C-----------------------------------------------------------------------
C
         DO 7 L = 0 , N-1
C
            LP = L + 1
            RHS(LP) = WHIGH(I4IDFL( N , L ))
C
            DO 8 N1 = N+1 , MXNSHL
C
               L1 = L + 1
               A1 = R8ATAB( IZ1 , N1 , L1 , N , L )
               RHS(LP) = RHS(LP) + A1 * WHIGH(I4IDFL( N1, L1 ))
C
               L1 = L - 1
               A1 = R8ATAB( IZ1 , N1 , L1 , N , L )
               RHS(LP) = RHS(LP) + A1 * WHIGH(I4IDFL( N1 , L1 ))
C
    8       CONTINUE
    7    CONTINUE
C
C-----------------------------------------------------------------------
C SET UP AND SOLVE MATRIX M FOR LEVEL N.
C-----------------------------------------------------------------------
C
         CALL C9WSOL( MXNSHL , IZ1    , NGRND  , N      ,
     &                DENSZ  , DENS   , TBQMEP , TBQMEM ,
     &                TBQMIP , TBQMIM , RHS               )
C
C-----------------------------------------------------------------------
C COPY RESULTS INTO WHIGH
C-----------------------------------------------------------------------
C
         DO 9 L = 0, N-1
            WHIGH(I4IDFL( N , L )) = RHS(L+1)
    9    CONTINUE
C
C-----------------------------------------------------------------------
C
    6 CONTINUE
C
C-----------------------------------------------------------------------
C  ADD ALL CASCADE CONTRIBUTIONS FROM LEVELS ABOVE NUMAX INTO LEVELS
C  NGRND TO NUMAX INTO LAST COLUMN ON WLOW ARRAY. THESE ALL DEPEND ON
C  Q (CH. EX.) AT NUMAX.
C-----------------------------------------------------------------------
C
      DO 10 N = NGRND , NUMAX
         DO 11 L = 0 , N-1
C
            IDL = I4IDFL( N , L )
C
            DO 12 N1 = NUMAX+1 , MXNSHL
C
               L1 = L + 1
               A1 = R8ATAB( IZ1 , N1 , L1 , N , L )
               WLOW(IDL,NUMAX) = WLOW(IDL,NUMAX)
     &                          + A1 * WHIGH(I4IDFL( N1 , L1 ))
C
               L1 = L - 1
               A1 = R8ATAB( IZ1 , N1 , L1 , N , L )
               WLOW(IDL,NUMAX) = WLOW(IDL,NUMAX)
     &                           + A1 * WHIGH(I4IDFL( N1 , L1 ))
C
   12       CONTINUE
   11    CONTINUE
   10 CONTINUE
C
C-----------------------------------------------------------------------
C START MAIN REDUCTION LOOP DOWNWARD FROM NUMAX TO NBOT.
C-----------------------------------------------------------------------
C
      DO 13 N = NUMAX , NBOT , -1
         DO 14 N1 = N , NUMAX
C
C-----------------------------------------------------------------------
C COPY WLOW COMPONENTS INTO RHS.
C-----------------------------------------------------------------------
C
            DO 15 L = 0 , N-1
               RHS(L+1) = WLOW(I4IDFL( N , L ),N1)
   15       CONTINUE
C
C-----------------------------------------------------------------------
C SET UP AND SOLVE MATRIX M FOR LEVEL N.
C-----------------------------------------------------------------------
C
            CALL C9WSOL( MXNSHL , IZ1    , NGRND  , N      ,
     &                   DENSZ  , DENS   , TBQMEP , TBQMEM ,
     &                   TBQMIP , TBQMIM , RHS               )
C
C-----------------------------------------------------------------------
C COPY RESULT TO WLOW.
C-----------------------------------------------------------------------
C
         DO 16 L = 0 , N-1
            WLOW(I4IDFL( N , L ),N1) = RHS(L+1)
   16    CONTINUE
C
C-----------------------------------------------------------------------
C  ADD IN CASCADE CONTRIBUTIONS TO LOWER LEVELS IN APPROPRIATE COLUMNS
C  OF WLOW.
C-----------------------------------------------------------------------
C
         DO  17 N2 = NGRND , N-1
            DO 18 L2 = 0 , N2-1
C
               IDL = I4IDFL( N2 , L2 )
C
               A1 = R8ATAB( IZ1 , N , L2+1 , N2 , L2 )
               WLOW(IDL,N1) = WLOW(IDL,N1)
     &                        + A1 * WLOW(I4IDFL( N , L2+1 ),N1)
C
C
               A1 = R8ATAB( IZ1 , N , L2-1 , N2 , L2 )
               WLOW(IDL,N1) = WLOW(IDL,N1)
     &                        + A1 * WLOW(I4IDFL( N , L2-1 ),N1)
C
   18       CONTINUE
   17    CONTINUE
C
C-----------------------------------------------------------------------
C
   14  CONTINUE
   13  CONTINUE
C
C-----------------------------------------------------------------------
C
 1000 FORMAT( 1X, 32('*'), ' C9WFIL ERROR ', 32('*') //
     &        2X, 'INTERNAL PARAMETER ''MXN'' IS LESS THAN INPUT ',
     &            'ARGUMENT ''MXNSHL''.'/
     &        2X, 'MXN = ', I3 , '   MXNSHL = ', I3/
     &        2X, 'INCREASE PARAMETER ''MXN'' IN SUBROUTINE C9WFIL.' //
     &        1X, 29('*'), ' PROGRAM TERMINATED ', 29('*') )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
