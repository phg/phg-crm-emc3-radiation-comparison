CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas309/c9out0.for,v 1.1 2004/07/06 11:57:11 whitefor Exp $ Date $Date: 2004/07/06 11:57:11 $
CX
      SUBROUTINE C9OUT0( IWRITE , MXSCAN , DATE   , DSNIN  ,
     &                   DSNPAS , IPSET  , TITLE  , SYMBD  ,
     &                   IDZ0   , SYMBR  , IRZ0   , IRZ1   ,
     &                   IRZ2   , RAMSNO , TEV    , TIEV   ,
     &                   DENS   , DENSZ  , ZEFF   , BMAG   ,
     &                   BMENG  , NTU    , NTL    , ITHEOR ,
     &                   IBSTAT , IEMMS  , NBMENG , BMENGA ,
     &                   NTIEV  , TIEVA  , NDENSZ , DENSZA ,
     &                   NZEFF  , ZEFFA  , NBMAG  , BMAGA  ,
     &                   CADAS
     &                 )
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: C9OUT0 *********************
C
C  PURPOSE: WRITES TEXT OUTPUT TO FILE FOR ADAS309.
C
C  CALLING PROGRAM: ADAS309
C
C  INPUT : (I*4)  IWRITE    = UNIT NUMBER FOR OUTPUT.
C  INPUT : (I*4)  MXSCAN    = MAXIMUM NUMBER OF VALUES IN A SCAN.
C  INPUT : (C*8)  DATE      = DATE STRING.
C  INPUT : (C*80) DSNIN     = FULL INPUT DATA SET NAME.
C  INPUT : (C*80) DSNPAS    = FULL OUTPUT PASSING DATA SET NAME.
C  INPUT : (I*4)  IPSET     = INPUT PARAMETER SET NUMBER FOR CURRENT.
C  INPUT : (C*40) TITLE     = ISPF ENTERED GENERAL TITLE FOR RUN.
C  INPUT : (C*2)  SYMBD     = DONOR ELEMENT SYMBOL.
C  INPUT : (I*4)  IDZ0      = DONOR NUCLEAR CHARGE.
C  INPUT : (C*2)  SYMBR     = RECEIVER ELEMENT SYMBOL.
C  INPUT : (I*4)  IRZ0      = RECEIVER NUCLEAR CHARGE.
C  INPUT : (I*4)  IRZ1      = RECEIVER ION INITIAL CHARGE.
C  INPUT : (I*4)  IRZ2      = RECEIVER ION FINAL CHARGE.
C  INPUT : (R*8)  RAMSNO    = RECEIVER ATOMIC MASS.
C  INPUT : (R*8)  TEV       = ELECTRON TEMPERATURE.
C                             UNITS: EV
C  INPUT : (R*8)  TIEV      = ION TEMPERATURE.
C                             UNITS: EV
C  INPUT : (R*8)  DENS      = ELECTRON DENSITY.
C                             UNITS: CM-3
C  INPUT : (R*8)  DENSZ     = PLASMA ION DENSITY.
C                             UNITS: CM-3
C  INPUT : (R*8)  ZEFF      = EFFECTIVE ION CHARGE.
C  INPUT : (R*8)  BMAG      = PLASMA MAGNETIC INDUCTION.
C                             UNITS: TESLA
C  INPUT : (R*8)  BMENG     = REFERNCE VALUE FOR BEAM ENERGY.
C                             UNITS: EV/AMU
C  INPUT : (I*4)  NTU       = UPPER PRINCIPAL QUANTUM NUMBER OF
C                             TRANSITION.
C  INPUT : (I*4)  NTL       = LOWER PRINCIPAL QUANTUM NUMBER OF
C                             TRANSITION.
C  INPUT : (I*4)  ITHEOR    = CHARGE EXCHANGE MODEL OPTION.
C                             1 => USE INPUT DATA SET.
C                             2 => USE EIKONAL MODEL.
C  INPUT : (I*4)  IBSTAT    = DONOR STATE FOR EIKONAL MODEL.
C                             1 => H(1S)
C                             2 => H(2S)
C                             3 => H(2P)
C                             4 => HE(1S2)
C                             5 => HE(1S2S)
C  INPUT : (I*4)  IEMMS     = EMISSION MEASURE MODEL OPTION.
C                             1 => CHARGE EXCHANGE.
C                             2 => ELECTRON IMPACT EXCITATION.
C  INPUT : (I*4)  NBMENG    = NUMBER OF BEAM ENERGIES IN SCAN.
C  INPUT : (R*8)  BMENGA()  = BEAM ENERGIES FOR SCAN.
C                             UNITS: EV/AMU
C                             DIMENSION: MXSCAN
C  INPUT : (I*4)  NTIEV     = NUMBER OF ION TEMPERATURES IN SCAN.
C  INPUT : (R*8)  TIEVA()   = ION TEMPERATURES FOR SCAN.
C                             UNITS: EV
C                             DIMENSION: MXSCAN
C  INPUT : (I*4)  NDENSZ    = NUMBER OF ION DENSZTIES IN SCAN.
C  INPUT : (R*8)  DENSZA()  = ION DENSZTIES FOR SCAN.
C                             UNITS: CM-3
C                             DIMENSION: MXSCAN
C  INPUT : (I*4)  NZEFF     = NUMBER OF PLASMA EFFECTIVE Z IN SCAN.
C  INPUT : (R*8)  ZEFFA()   = PLASMA EFFECTIVE Z FOR SCAN.
C                             DIMENSION: MXSCAN
C  INPUT : (I*4)  NBMAG     = NUMBER OF PLASMA MAGNETIC FIELDS IN SCAN.
C  INPUT : (R*8)  BMAGA()   = PLASMA MAGNETIC FIELDS FOR SCAN.
C                             UNITS: TESLA
C                             DIMENSION: MXSCAN
C
C  I/O   : (C*80) CADAS     = ADAS HEADER: INCLUDES RELEASE, PROGRAM,
C                             TIME.
C
C          (I*4)  I         = ARRAY INDEX.
C
C          (L*4)  LFIRST    = FLAGS IF FIRST CALL OF SUBROUTINE.
C                             .TRUE.  => FIRST CALL.
C                             .FALSE. => NOT FIRST CALL.
C
C ROUTINES: NONE
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/87
C          JET EXT. 5183
C
C DATE:    03/12/93
C
C UNIX PORT: TIM HAMMOND (TESSELLA SUPPORT SERVICES PLC)
C DATE:    10/07/95			VERSION 1.1
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    IWRITE  , MXSCAN  , IDZ0    , IRZ0    ,
     &           IRZ1    , IRZ2    , NTU     , NTL     , ITHEOR  ,
     &           IBSTAT  , IEMMS   , NBMENG  , NTIEV   , NDENSZ  ,
     &           NZEFF   , NBMAG
      INTEGER    I       , IPSET
C-----------------------------------------------------------------------
      REAL*8     RAMSNO  , TEV     , TIEV    , DENS    , DENSZ   ,
     &           ZEFF    , BMAG    , BMENG
C-----------------------------------------------------------------------
      LOGICAL    LFIRST
C-----------------------------------------------------------------------
      CHARACTER  DATE*8     , DSNIN*80   , DSNPAS*80  , TITLE*40   ,
     &           SYMBR*2    , SYMBD*2
      CHARACTER  CADAS*80
C-----------------------------------------------------------------------
      REAL*8     BMENGA(MXSCAN)  , DENSZA(MXSCAN)  , TIEVA(MXSCAN)   ,
     &           ZEFFA(MXSCAN)   , BMAGA(MXSCAN)
C-----------------------------------------------------------------------
      SAVE       LFIRST
C-----------------------------------------------------------------------
      DATA       LFIRST / .TRUE. /
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C IF FIRST CALL OF ROUTINE WRITE HEADER.
C-----------------------------------------------------------------------
C
      IF (LFIRST) THEN
C        CALL XXADAS( CADAS )
         WRITE(IWRITE,1000) CADAS(2:80)
         WRITE(IWRITE,1001) DATE
         I = INDEX( DSNIN , ' ' ) - 1
         WRITE(IWRITE,1002) DSNIN(2:I)
         I = INDEX( DSNPAS , ' ' ) - 1
         WRITE(IWRITE,1003) DSNPAS(2:I)
         LFIRST = .FALSE.
      ENDIF
C
C-----------------------------------------------------------------------
C WRITE SUMMARY OF USER INPUT.
C-----------------------------------------------------------------------
C
      WRITE(IWRITE,1004) IPSET
      WRITE(IWRITE,1005) TITLE
      WRITE(IWRITE,1006) SYMBR , SYMBD , IRZ0 , IDZ0 , IRZ1 , IRZ2 ,
     &                   RAMSNO
      WRITE(IWRITE,1007) TIEV , TEV , DENSZ , DENS , ZEFF , BMAG
      WRITE(IWRITE,1008) BMENG
      WRITE(IWRITE,1009) NTU , NTL
C
      IF (ITHEOR .EQ. 1) THEN
         WRITE(IWRITE,1010) 'INPUT DATA SET'
      ELSE IF (ITHEOR .EQ. 2) THEN
         WRITE(IWRITE,1010) 'EIKONAL MODEL'
         IF (IBSTAT .EQ. 1) THEN
            WRITE(IWRITE,1011) 'H(1S)'
         ELSE IF (IBSTAT .EQ. 2) THEN
            WRITE(IWRITE,1011) 'H(2S)'
         ELSE IF (IBSTAT .EQ. 3) THEN
            WRITE(IWRITE,1011) 'H(2P)'
         ELSE IF (IBSTAT .EQ. 4) THEN
            WRITE(IWRITE,1011) 'H(1S2)'
         ELSE IF (IBSTAT .EQ. 5) THEN
            WRITE(IWRITE,1011) 'H(1S2S)'
         ENDIF
      ENDIF
C
      IF (IEMMS .EQ. 1) THEN
         WRITE(IWRITE,1012) 'CHARGE EXCHANGE'
      ELSE IF (IEMMS .EQ. 2) THEN
         WRITE(IWRITE,1012) 'ELECTRON IMPACT EXCITATION'
      ENDIF
C
      WRITE(IWRITE,1013) 'BEAM ENERGY' , 'BEAM ENERGIES' , NBMENG
      IF ( NBMENG .GT. 0 ) THEN
         WRITE(IWRITE,1014) 'BEAM ENERGIES (EV)' ,
     &                      ( BMENGA(I) , I=1,NBMENG )
      ENDIF
C
      WRITE(IWRITE,1013) 'ION TEMPERATURE' , 'ION TEMPERATURES' ,
     &                   NTIEV
      IF ( NTIEV .GT. 0 ) THEN
         WRITE(IWRITE,1014) 'ION TEMPERATURES (EV)' ,
     &                      ( TIEVA(I) , I=1,NTIEV )
      ENDIF
C
      WRITE(IWRITE,1013) 'ION DENSITY' , 'ION DENSITIES' ,
     &                   NDENSZ
      IF ( NDENSZ .GT. 0 ) THEN
         WRITE(IWRITE,1014) 'ION DENSITIES (CM-3)' ,
     &                      ( DENSZA(I) , I=1,NDENSZ )
      ENDIF
C
      WRITE(IWRITE,1013) 'PLASMA EFFECTIVE Z' , 'EFFECTIVE Z' ,
     &                   NZEFF
      IF ( NZEFF .GT. 0 ) THEN
         WRITE(IWRITE,1014) 'EFFECTIVE Z' ,
     &                      ( ZEFFA(I) , I=1,NZEFF )
      ENDIF
C
      WRITE(IWRITE,1013) 'MAGNETIC INDUCTION' , 'MAGNETIC FIELDS' ,
     &                   NBMAG
      IF ( NBMAG .GT. 0 ) THEN
         WRITE(IWRITE,1014) 'MAGNETIC FIELDS (T)' ,
     &                      ( BMAGA(I) , I=1,NBMAG )
      ENDIF
C
C-----------------------------------------------------------------------
C
 1000 FORMAT( 1H1 , A79 )
 1001 FORMAT( / 1H0 , 41( '*' ) ,
     &        '    RUN SUMMARY FOR J-RESOLVED CHARGE EXCHANGE    ' ,
     &        41( '*' ) / 1H , 41( '*' ) ,
     &        ' EFFECTIVE EMISSIVITY COEFFICENT SCANNING PROGRAM ' ,
     &        41( '*' ) / 1H , 53( '*' ) ,
     &        ' ADAS309 - DATE: ' , A8 , 1X , 53( '*' ) / )
 1002 FORMAT( / 1H0 , 'INPUT FILE         : ', A / )
 1003 FORMAT( 1H0 , 'OUTPUT PASSING FILE: ', A )
 1004 FORMAT( // 1H0 , 23( '*' ) , ' SUMMARY OF PARAMETER SET NO. ' ,
     &        I2 , 2X , 23( '*' ) / )
 1005 FORMAT( 1H0 , 19( '-' ) , 1X , A40 , 1X , 19( '-' ) // )
 1006 FORMAT( / 1H0 , 'DONOR / RECEIVER INFORMATION:' //
     &        1H , 26X , 'RECEIVER' , 6X , 'NEUTRAL DONOR' /
     &        1H , 26X , 8( '-' ) , 6X , 13 ('-') /
     &        1H , ' ELEMENT SYMBOL         =' , 6X ,A2   , 12X , A2  /
     &        1H , ' NUCLEAR CHARGE         =' , 2X ,I6   ,  8X , I6  /
     &        1H , ' RECOMBINING ION CHARGE =' , 2X ,I6   , 13X , '-' /
     &        1H , ' RECOMBINED ION CHARGE  =' , 2X ,I6   , 13X , '-' /
     &        1H , ' ATOMIC MASS NUMBER     =' , 2X ,F6.2 , 13X , '-' )
 1007 FORMAT( / 1H0 , 'PLASMA PARAMETERS:' //
     &        1H , ' ION TEMPERATURE      (EV)   = ' , 1P , D10.2 , 5X ,
     &        1H , ' ELECTRON TEMPERATURE (EV)   = ' , D10.2 , /
     &        1H , ' ION DENSITY          (CM-3) = ' , D10.2 , 5X ,
     &        1H , ' ELECTRON DENSITY     (CM-3) = ' , D10.2 , /
     &        1H , ' PLASMA EFFECTIVE Z          = ' , 0P , F6.2 , 9X ,
     &        1H , ' MAGNETIC INDUCTION   (T)    = ' , 0P , F6.2 )
 1008 FORMAT( / 1H0 , 'BEAM PARAMETERS:' //
     &        1H , ' BEAM ENERGY (EV) = ', 1P, D8.2 )
 1009 FORMAT( / 1H0 , 'TRANSITION PARAMETERS:' //
     &        1H , ' UPPER N LEVEL = ' , I2 ,
     &        '     LOWER N LEVEL = ' , I2 )
 1010 FORMAT( / 1H0 , 'CHARGE EXCHANGE MODEL : ' , A )
 1011 FORMAT( 1H  , 'DONOR STATE           : ' , A )
 1012 FORMAT( 1H  , 'EMISSION MEASURE MODEL: ' , A )
 1013 FORMAT( / 1H0 , 'VALUES FOR ' , A , ' SCAN: ' //
     &        1H , ' NUMBER OF ' , A , ' = ' , I2 / )
 1014 FORMAT( 1H , ' SET OF ' , A , / 1P, 4( 2X , 6D10.2 / ) )
C
C-----------------------------------------------------------------------
C
      RETURN
      END
