CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas309/adas309.for,v 1.8 2007/05/24 15:27:29 mog Exp $ Date $Date: 2007/05/24 15:27:29 $
CX
      PROGRAM ADAS309
C
      IMPLICIT NONE
C
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 PROGRAM: ADAS309 **********************
C
C  ORIGINAL NAME: SPSCLMS / SCLMS
C
C  VERSION:  (ADAS91) - SEE SC CS COMMENTS FOR VERSION NO.
C
C  PURPOSE:  SCANNING PROGRAM FOR L-RESOLVED EFFECTIVE EMISSIVITY RATE
C            COEFFICIENTS. FOR A GIVEN TRANSITION THE PROGRAM PERFORMS
C            ONE DIMENSIONAL SCANS IN BEAM ENERGY , ION TEMPERATURE ,
C            ION DENSITY , PLASMA EFFECTIVE Z , AND PLASMA MAGNETIC
C            INDUCTION. (AT PRESENT THE ELECTRON DENSITY IS FIXED.
C            STRICTLY IT SHOULD BE ADJUSTED WITH ION DENSITY.) THE
C            PROGRAM CAN BE RUN EITHER IN THE FOREGROUND OR IN BATCH.
C
C            THE EMISSIVITY CALCULATION IS APPLICABLE TO IMPURITIES IN
C            PLASMA TRAVERSED BY NEUTRAL BEAMS OF H OR HE. THE
C            RECOMBINED TARGET ION IS TREATED AS H-LIKE. THE MODEL
C            INCLUDES CAPTURE, N-N' LEVEL CASCADE, AND MIXING AMONG L
C            LEVELS OF SAME N BY COLLISIONS.  AN INTERNAL EIKONAL
C            APPROXIMATION IS USED FOR CAPTURE FROM EXCITED H OR HE
C            STATES, ALTHOUGH NORMALLY THE EXTERNAL DATA SET SHOULD BE
C            USED.
C
CX DATA:     THE SOURCE DATA IS CONTAINED AS MEMBERS IN A PARTITIONED
CX           DATA SET AS FOLLOWS:-
CX
CX                    'JETSHP.QCX#<DONOR>.DATA(<MEMBER>)'
CX
CX           WHERE
CX             <DONOR>    = <DONOR ELEMENT SYMBOL><DONOR CHARGE STATE>
CX             <MEMBER>   = <ID>#<RECEIVER>
CX
CX             <ID>       = 3 CHARACTER PREFIX IDENTIFYING THE SOURCE
CX                          OF THE DATA.
CX             <RECEIVER> = <RECVR ELEMENT SYMBOL><RECVR CHARGE STATE>
CX
CX
CX             E.G. 'JETSHP.QCX#H0.DATA(OLD#C6)'
C
C           THE UNITS USED IN THE DATA FILE ARE TAKEN AS FOLLOWS:
C
C           ENERGIES            : KEV / AMU
C           CROSS SECTIONS      : CM**2
C
C  PROGRAM:
C
C  PARAM : (I*4)  IUNT10    = INPUT UNIT FOR CHARGE EXCHANGE DATA SET.
C  PARAM : (I*4)  MXNENG    = MAXIMUM NO. OF ENERGIES IN DATA SET.
C  PARAM : (I*4)  MXNSHL    = MAXIMUM NUMBER OF N SHELLS.
C  PARAM : (I*4)  MXSCAN    = MAXIMUM NUMBER OF VALUES IN A SCAN.
C
C          (I*4)  IPSET     = INPUT PARAMETER SET NUMBER FOR CURRENT.
C          (I*4)  NGRND     = PRINCIPAL QUANTUM NUMBER OF GROUND STATE.
C          (I*4)  NTOT      = PRINCIPAL QUANTUM NUMBER OF HIGHEST BOUND
C                             STATE.
C          (I*4)  NBOT      = MINIMUM PRINCIPAL QUANTUM NUMBER FOR
C                             RATE TABLES.
C          (I*4)  NTOP      = MAXIMUM PRINCIPAL QUANTUM NUMBER FOR
C                             RATE TABLES.
C          (I*4)  IZR       = ION CHARGE OF RECEIVER.
C          (I*4)  IZD       = ION CHARGE OF DONOR.
C          (I*4)  INDD      = DONOR STATE INDEX.
C          (I*4)  NENRGY    = NUMBER OF ENERGIES READ FROM DATA SET.
C          (I*4)  NMINF     = LOWEST N-SHELL FOR WHICH DATA READ.
C          (I*4)  NMAXF     = HIGHEST N-SHELL FOR WHICH DATA READ.
C          (I*4)  IDZ0      = DONOR NUCLEAR CHARGE.
C          (I*4)  IRZ0      = RECEIVER NUCLEAR CHARGE.
C          (I*4)  IRZ1      = RECEIVER ION INITIAL CHARGE.
C          (I*4)  IRZ2      = RECEIVER ION FINAL CHARGE.
C          (I*4)  NBEAM     = NUMBER OF BEAM ENERGIES.
C          (I*4)  ITHEOR    = CHARGE EXCHANGE MODEL OPTION.
C                             1 => USE INPUT DATA SET.
C                             2 => USE EIKONAL MODEL.
C          (I*4)  IBSTAT    = DONOR STATE FOR EIKONAL MODEL.
C                             1 => H(1S)
C                             2 => H(2S)
C                             3 => H(2P)
C                             4 => HE(1S2)
C                             5 => HE(1S2S)
C          (I*4)  IEMMS     = EMISSION MEASURE MODEL OPTION.
C                             1 => CHARGE EXCHANGE.
C                             2 => ELECTRON IMPACT EXCITATION.
C          (I*4)  NTU       = UPPER PRINCIPAL QUANTUM NUMBER OF
C                             TRANSITION.
C          (I*4)  NTL       = LOWER PRINCIPAL QUANTUM NUMBER OF
C                             TRANSITION.
C          (I*4)  NBMENG    = NUMBER OF BEAM ENERGIES IN SCAN.
C          (I*4)  NTIEV     = NUMBER OF ION TEMPERATURES IN SCAN.
C          (I*4)  NDENSZ    = NUMBER OF ION DENSZTIES IN SCAN.
C          (I*4)  NZEFF     = NUMBER OF PLASMA EFFECTIVE Z IN SCAN.
C          (I*4)  NBMAG     = NUMBER OF PLASMA MAGNETIC FIELDS IN SCAN.
C          (I*4)  IRCODE    = RETURN CODE FROM IBM SUBROUTINE 'FILEINF'.
C          (I*4)  I         = LOOP INDEX.
C
C          (R*8)  RAMSNO    = RECEIVER ATOMIC MASS.
C          (R*8)  TEV       = ELECTRON TEMPERATURE.
C                             UNITS: EV
C          (R*8)  TIEV      = ION TEMPERATURE.
C                             UNITS: EV
C          (R*8)  DENS      = ELECTRON DENSITY.
C                             UNITS: CM-3
C          (R*8)  DENSZ     = PLASMA ION DENSITY.
C                             UNITS: CM-3
C          (R*8)  ZEFF      = EFFECTIVE ION CHARGE.
C          (R*8)  BMAG      = PLASMA MAGNETIC INDUCTION.
C                             UNITS: TESLA
C          (R*8)  BMENG     = BEAM ENERGY.
C                             UNITS: EV/AMU
C          (R*8)  ERATE     = EFFECTIVE EMISSIVITY RATE COEFFICIENT FOR
C                             REQUESTED TRANSITION.
C                             SPECTRUM LINE.
C                             UNITS: CM3 SEC-1
C          (R*8)  QEFREF    = REFERENCE VALUE FOR EFFECTIVE RATE COEFFT.
C                             UNITS: CM3 SEC-1
C          (R*8)  SCNVAL    = VALUE OF QUANTITY CURRENTLY BEING SCANNED.
C
C          (L*4)  LBATCH    = FLAGS HOW CALCULATIONS ARE TO BE EXECUTED.
C                             .TRUE.  => PROGRAM IN BATCH.
C                             .FALSE. => PROGRAM IN FOREGROUND.
C          (L*4)  OPEN10    = FLAGS IF INPUT CX DATA SET OPEN.
C                             .TRUE.  => INPUT CX DATA SET OPEN.
C                             .FALSE. => INPUT CX DATA SET CLOSED.
C          (L*4)  LPARMS    = FLAGS IF L-SPLITTING PARAMETERS PRESENT.
C                             .TRUE.  => L-SPLITTING PARAMETERS PRESENT.
C                             .FALSE  => L-SPLITTING PARAMETERS ABSENT.
C          (L*4)  LSETL     = FLAGS IF L-RESOLVED DATA PRESENT.
C                             .TRUE.  => L-RESOLVED DATA PRESENT.
C                             .FALSE  => L-RESOLVED DATA ABSENT.
C          (L*4)  LSETM     = FLAGS IF M-RESOLVED DATA PRESENT.
C                             .TRUE.  => M-RESOLVED DATA PRESENT.
C                             .FALSE  => M-RESOLVED DATA ABSENT.
C          (L*4)  LPEND     = FLAGS IF END OF ANALYSIS REQUESTED.
C                             .TRUE.  => END ANALYSIS OF CURRENT DATA
C                                        SETS
C                             .FALSE. => CONTINUE PANALYSIS WITH CURRENT
C                                        DATA SETS
C          (L*4)  LBTSEL    = FLAGS IF PROGRAM TO BE RUN IN BATCH.
C                             .TRUE.  => RUN PROGRAM IN BATCH.
C                             .FALSE. => RUN PROGRAM IN FOREGROUND.
C
C          (C*8)  DATE      = DATE.
C          (C*3)  REP       = REQUEST FOR PROGRAM TERMINATION.
C                             'YES' => TERMINATE PROGRAM EXECUTION.
C                             'NO'  => CONTINUE PROGRAM EXECUTION.
C          (C*80) DSNIN     = FILE NAME OF INPUT DATA SET.
C          (C*80) TITLEF    = NOT SET - TITLE FROM INPUT DATA SET.
C          (C*2)  SYMBR     = RECEIVER ELEMENT SYMBOL.
C          (C*2)  SYMBD     = DONOR ELEMENT SYMBOL.
C          (C*40) TITLE     = ISPF ENTERED GENERAL TITLE FOR RUN.
C          (C*80) DSNRUN    = RUN SUMMARY FILE DATA SET NAME.
C          (C*80) DSNPAS    = PASSING FILE DATA SET NAME.
C          (C*80) CADAS     = ADAS HEADER: INCLUDES RELEASE, PROGRAM,
C                             TIME.
C
C          (I*4)  LFORMA()  = PARAMETERS FOR CALCULATING L-RES X-SEC
C                             READ FROM INPUT DATA SET.
C                             DIMENSION: ENERGY INDEX
C
C          (R*8)  ENRGYA()  = COLLISION ENERGIES READ FROM INPUT DATA
C                             SET.
C                             UNITS: EV/AMU
C                             DIMENSION: ENERGY INDEX
C          (R*8)  ALPHAA()  = EXTRAPOLATION PARAMETER ALPHA READ FROM
C                             INPUT DATA SET.
C                             DIMENSION: ENERGY INDEX
C          (R*8)  XLCUTA()  = PARAMETERS FOR CALCULATING L-RES X-SEC
C                             READ FROM INPUT DATA SET.
C                             DIMENSION: ENERGY INDEX
C          (R*8)  PL2A()    = PARAMETERS FOR CALCULATING L-RES X-SEC
C                             READ FROM INPUT DATA SET.
C                             DIMENSION: ENERGY INDEX
C          (R*8)  PL3A()    = PARAMETERS FOR CALCULATING L-RES X-SEC
C                             READ FROM INPUT DATA SET.
C                             DIMENSION: ENERGY INDEX
C          (R*8)  XSECTA()  = TOTAL CHARGE EXCHANGE CROSS-SECTIONS READ
C                             FROM INPUT DATA SET.
C                             UNITS: CM2
C                             DIMENSION: ENERGY INDEX
C          (R*8)  BMENGA()  = BEAM ENERGIES FOR SCAN.
C                             UNITS: EV/AMU
C                             DIMENSION: MXSCAN
C          (R*8)  DENSZA()  = ION DENSZTIES FOR SCAN.
C                             UNITS: CM-3
C                             DIMENSION: MXSCAN
C          (R*8)  TIEVA()   = ION TEMPERATURES FOR SCAN.
C                             UNITS: EV
C                             DIMENSION: MXSCAN
C          (R*8)  ZEFFA()   = PLASMA EFFECTIVE Z FOR SCAN.
C                             DIMENSION: MXSCAN
C          (R*8)  BMAGA()   = PLASMA MAGNETIC FIELDS FOR SCAN.
C                             UNITS: TESLA
C                             DIMENSION: MXSCAN
C          (R*8)  QEFBEA()  = EFFECTIVE RATE COEFFICIENTS FOR BEAM
C                             ENERGY SCAN.
C                             UNITS: CM3 SEC-1
C                             DIMENSION: MXSCAN
C          (R*8)  QEFTIA()  = EFFECTIVE RATE COEFFICIENTS FOR ION
C                             TEMPERATURE SCAN.
C                             UNITS: CM3 SEC-1
C                             DIMENSION: MXSCAN
C          (R*8)  QEFDZA()  = EFFECTIVE RATE COEFFICIENTS FOR ION
C                             DENSITY SCAN.
C                             UNITS: CM3 SEC-1
C                             DIMENSION: MXSCAN
C          (R*8)  QEFZEA()  = EFFECTIVE RATE COEFFICIENTS FOR PLASMA
C                             EFFECTIVE Z SCAN.
C                             UNITS: CM3 SEC-1
C                             DIMENSION: MXSCAN
C          (R*8)  QEFBMA()  = EFFECTIVE RATE COEFFICIENTS FOR PLASMA
C                             MAGNETIC FIELD SCAN.
C                             UNITS: CM3 SEC-1
C                             DIMENSION: MXSCAN
C
C          (R*8)  XSECNA(,) = N-RESOLVED CHARGE EXCHANGE CROSS-SECTIONS
C                             READ FROM INPUT DATA SET.
C                             UNITS: CM2
C                             1ST DIMENSION: ENERGY INDEX
C                             2ND DIMENSION: N-SHELL
C          (R*8)  FRACLA(,) = L-RESOLVED CHARGE EXCHANGE CROSS-SECTIONS.
C                             AFTER CXDATA: ABSOLUTE VALUES (CM2).
C                             AFTER CXFRAC: FRACTION OF N-RESOLVED
C                                            DATA.
C                             1ST DIMENSION: ENERGY INDEX
C                             2ND DIMENSION: INDEXED BY I4IDFL(N,L)
C          (R*8)  FRACMA(,) = M-RESOLVED CHARGE EXCHANGE CROSS-SECTIONS.
C                             AFTER CXDATA: ABSOLUTE VALUES (CM2).
C                             AFTER CXFRAC: FRACTION OF L-RESOLVED
C                                            DATA.
C                             1ST DIMENSION: ENERGY INDEX
C                             2ND DIMENSION: INDEXED BY I4IDFM(N,L,M)
C                                            WITH M >= 0 ONLY
C          (I*4)   PIPEIN   = PARAMETER = UNIT NUMBER FOR OUTPUT TO PIPE
C          (I*4)   PIPEOU   = PARAMETER = UNIT NUMBER FOR INPUT FROM PIPE
C          (I*4)   ONE      = PARAMETER = 1  : USED AS FLAG TO IDL
C          (I*4)   ZERO     = PARAMETER = 0  : USED AS FLAG TO IDL
C          (I*4)   ISTOP    = VALUE READ FROM PIPE FOR MENU BUTTON STATE
C                             1 => PRESSED, 0 => NOT
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          -------------------------------------------------------------
C          C9SPF0     ADAS      HANDLES IDL PANEL FOR DATA SET INPUT.
C          C8CHRG     ADAS      SETS UP CHARGES AND EXTREME N LEVELS FOR
C                               DONOR AND RECEIVER.
C          C9ISPF     ADAS      HANDLES IDL PANELS FOR USER INPUT.
C          CXDATA     ADAS      READS INPUT DATA SET IN ADF01 FORMAT.
C          CXFRAC     ADAS      CONVERTS L AND M RESOLVED CROSS-SECTIONS
C                               FROM ABSOLUTE VALUES TO FRACTIONS.
C          CXEXTR     ADAS      EXTRAPOLATES N AND L RESOLVED CROSS-
C                               SECTIONS FROM INPUT DATA SET.
C          CXSETP     ADAS      SETS UP PARAMETERS IN THE SHARED POOL
C                               FOR ISPF PANEL DISPLAY.
C          XX0000     ADAS      SETS MACHINE DEPENDENT ADAS
C                               CONFIGURATION.
C          XXDATE     ADAS      RETURNS CURRENT DATE.
C          XXTEXT     ADAS      DISPLAYS DESCRIPTIVE TEXT FROM INPUT
C                               DATA SET.
C	   XXFLSH     IDL_ADAS  FLUSHES OUT THE UNIX PIPE.
C
C AUTHOR:  JONATHAN NASH (TESSELLA SUPPORT SERVICES PLC)
C          K1/0/87
C          JET EXT. 5183
C
C DATE:    03/12/93
C
C Modified: Tim Hammond (Tessella Support Services plc.)
C	    Baseline version in UNIX port - major revisions to 
C	    follow.
C
C Date:     21/06/95			Version 1.1
C
C Modified: Tim Hammond (Tessella Support Services plc.)
C           -Downgraded from main program to subroutine(called by
C	                                SHELL309.FOR)
C	    -Removed references to unit iunt05 (no longer used)
C           -Removed references to unit iunt20 (no longer used)
C	    -Increased DSNIN to 80 characters
C	    -Removed call to ZA06CS - information on whether it is
C	     a batch job or not comes from LBATCH when ADAS309 is
C	     first called.
C	    -Removed call to C9BCH0 to get input file name from 
C	     batch file - this is now read directly from c9batch.pro
C	    -Removed references to LDSEL (text preview flag)
C
C Date:     21/06/95			Version 1.2
C
C Modified: Tim Hammond (Tessella Support Services plc.)
C           -Removed all references to IPAN (ISPF panel number)
C	    -Removed additional panel called by ISPF for batch runs.
C	    -After rethinking have made adas309 back into the main
C	     program. It now runs through as with all other adas
C	     programs until the output screen is reached. At this
C	     point the user will have the option of running 
C	     interactively or in batch and the IDL will spawn a 
C	     second FORTRAN program to actually perform the 
C	     calculation either waiting for it to complete or
C	     leaving it in the background depending on what the user
C	     has specified.
C
C Date:	     22/06/95			Version 1.3
C
C Modified: Tim Hammond (Tessella Support Services plc.)
C           -Removed decisions made depending on LBATCH from early parts
C	     of code.
C
C Date:	     28/06/95			Version 1.4
C
C Modified: Tim Hammond (Tessella Support Services plc.)
C           -Redesign of adas309 has meant that this routine - adas309.for
C	     performs only the first part of the job: it gathers all the
C            data from the IDL panels and sets up the necessary data for
C	     performing the scans. The scans themselves will now be performed
C	     by a second main FORTRAN routine, adas309b.for. This will
C	     have two run modes - interactive or batch. See the comments
C	     within this routine for more information.
C
C Date:	     28/06/95			Version 1.5
C
C Modified: Tim Hammond (Tessella Support Services plc.)
C           - Increased DSNRUN and DSNPAS to CHAR*80
C	    - Removed some superfluous comments and variable defns.
C
C VERSION: 1.6                          DATE: 05-07-96
C MODIFIED: WILLIAM OSBORN
C           - ADDED MENU BUTTON CODE
C
C VERSION: 1.7				DATE: 04-09-97
C MODIFIED: HARVEY ANDERSON
C		- CHANGED THE MAXIMUM NUMBER OF ENERGIES FROM 30 TO
C		  40. THE NEW 1997 CARBON CHARGE EXCHANGE DATA OF
C		  BLIEK ET AL CONTAINED 31 COLLISION ENERGIES. 
C
C VERSION  : 1.8
C DATE     : 22-05-2007
C MODIFIED : Martin O'Mullane
C              - Remove unused m-subshell data possibility.
C
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      INTEGER    IUNT10       , ISTOP        ,
     &           MXNENG       , MXNSHL       , MXSCAN
      PARAMETER( IUNT10 = 10  ,
     &           MXNENG = 40  , MXNSHL = 20  , MXSCAN = 24    )
C-----------------------------------------------------------------------
      INTEGER    IPSET   , NGRND   , NTOT    , NBOT    , NTOP    ,
     &           IZR     , IZD     , INDD    , NENRGY  , NMINF   ,
     &           NMAXF   , IDZ0    , IRZ0    , IRZ1    , IRZ2    ,
     &           ITHEOR  , IBSTAT  , IEMMS   , NTU     ,
     &           NTL     , NBMENG  , NDENSZ  , NTIEV   , NZEFF   ,
     &           NBMAG   , IRCODE  , I       , J
      INTEGER    PIPEIN  , PIPEOU  , ONE     , ZERO, IBATCH
      PARAMETER( PIPEIN=5      , PIPEOU=6    , ONE=1       , ZERO=0)
C-----------------------------------------------------------------------
      REAL*8     RAMSNO  , TEV     , TIEV    , DENS    , DENSZ   ,
     &           ZEFF    , BMAG    , BMENG   , ERATE   , QEFREF  ,
     &           SCNVAL
C-----------------------------------------------------------------------
      LOGICAL    LBATCH  , OPEN10  , LPARMS  ,
     &           LSETL   , LSETM   , LPEND   , LBTSEL
C-----------------------------------------------------------------------
      CHARACTER  DATE*8     , REP*3      , DSNIN*80   , TITLEF*80  ,
     &           SYMBR*2    , SYMBD*2    , USERID*6   , TITLE*40   ,
     &           DSNRUN*80  , DSNPAS*80  , CADAS*80
C-----------------------------------------------------------------------
      INTEGER    LFORMA(MXNENG)
C-----------------------------------------------------------------------
      REAL*8     ENRGYA(MXNENG)  , ALPHAA(MXNENG)  , XLCUTA(MXNENG)  ,
     &           PL2A(MXNENG)    , PL3A(MXNENG)    , XSECTA(MXNENG)  ,
     &           BMENGA(MXSCAN)  , DENSZA(MXSCAN)  , TIEVA(MXSCAN)   ,
     &           ZEFFA(MXSCAN)   , BMAGA(MXSCAN)   , QEFBEA(MXSCAN)  ,
     &           QEFTIA(MXSCAN)  , QEFDZA(MXSCAN)  , QEFZEA(MXSCAN)  ,
     &           QEFBMA(MXSCAN)
C-----------------------------------------------------------------------
      REAL*8     XSECNA(MXNENG,MXNSHL)                            ,
     &           FRACLA(MXNENG,(MXNSHL*(MXNSHL+1))/2)             ,
     &           FRACMA(MXNENG,(MXNSHL*(MXNSHL+1)*(MXNSHL+2))/6)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C *************************** MAIN PROGRAM ****************************
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C SET MACHINE DEPENDENT ADAS CONFIGURATION VALUES.
C-----------------------------------------------------------------------
C
      CALL XX0000
C
C-----------------------------------------------------------------------
C GET CURRENT DATE.
C-----------------------------------------------------------------------
C
      CALL XXDATE( DATE )
C
C-----------------------------------------------------------------------
C INITIALIZE VALUES.
C-----------------------------------------------------------------------
C
      OPEN10 = .FALSE.
      LBTSEL = .FALSE.
      REP = 'NO'
      DSNPAS = ' '
      CADAS = ' '
      IPSET = 0
      NTOT = MXNSHL
      NTOP = MXNSHL
      NGRND = 1
      NBOT = 2
C
C-----------------------------------------------------------------------
C START OF CONTROL LOOP.
C-----------------------------------------------------------------------
C
    1 CONTINUE
C
C-----------------------------------------------------------------------
C IF FILE IS ACTIVE ON UNIT 10 THEN CLOSE THE UNIT.
C-----------------------------------------------------------------------
C
      IF (OPEN10) THEN
         CLOSE( IUNT10 )
         OPEN10 = .FALSE.
      ENDIF
C
C-----------------------------------------------------------------------
C GET INPUT DATA SET NAME (FROM IDL). THE IDL ROUTINE PASSING THE
C DATA IS THE STANDARD c9spf0.pro
C-----------------------------------------------------------------------
C
      CALL C9SPF0( REP , DSNIN )
C
C-----------------------------------------------------------------------
C IF PROGRAM RUN IS COMPLETED: END RUN.
C-----------------------------------------------------------------------
C
      IF (REP .EQ. 'YES') THEN
         GOTO 9999
      ENDIF
C
C-----------------------------------------------------------------------
C OPEN INPUT DATA FILE - DSNIN
C-----------------------------------------------------------------------
C
      OPEN( UNIT=IUNT10 , FILE=DSNIN , STATUS='OLD'  )
      OPEN10 = .TRUE.
C
C-----------------------------------------------------------------------
C FETCH DATA FROM SELECTED DATASET
C-----------------------------------------------------------------------
C

      call xxdata_01( iunt10 , mxneng , mxnshl ,
     &                symbr  , symbd  , izr    , izd    ,
     &                indd   , nenrgy , nminf  , nmaxf  ,
     &                lparms , lsetl  , enrgya ,
     &                alphaa , lforma , xlcuta , pl2a   ,
     &                pl3a   , xsecta , xsecna , fracla
     &              )

C
C-----------------------------------------------------------------------
C SET UP TARGET CHARGES AND LOWER PRINCIPAL QUANTUM NUMBERS.
C-----------------------------------------------------------------------
C
      CALL C8CHRG( SYMBD  , IZD    , SYMBR  , IZR    , IDZ0   ,
     &             IRZ0   , IRZ1   , IRZ2                       )
C
C-----------------------------------------------------------------------
C CONVERT L AND M RESOLVED CROSS-SECTIONS TO FRACTIONS.
C-----------------------------------------------------------------------
C
      CALL CXFRAC( MXNENG , MXNSHL , NENRGY , NMINF  , NMAXF  ,
     &             LSETL  , XSECNA , FRACLA 
     &           )
C
C-----------------------------------------------------------------------
C EXTRAPOLATE N AND L RESOLVED CROSS-SECTIONS BEYOND NMINF AND NMAXF.
C-----------------------------------------------------------------------
C
      CALL CXEXTR( MXNENG , MXNSHL , NMINF  , NMAXF  , NENRGY ,
     &             LPARMS , ALPHAA , LFORMA , XLCUTA , PL2A   ,
     &             PL3A   , XSECNA , FRACLA                     )
C
C-----------------------------------------------------------------------
C SET UP ISPF FUNCTION POOL PARAMETERS (NON-READABLE VARIABLES).
C-----------------------------------------------------------------------
C
      CALL CXSETP( 0      , 0      , SYMBD  , IDZ0   , SYMBR  ,
     &             IRZ0   , IRZ1   , IRZ2   , NGRND  , NTOT     )
C
C-----------------------------------------------------------------------
C
    2 CONTINUE
C
C-----------------------------------------------------------------------
C WRITE INFORMATION ABOUT DATA SET TO IDL 
C-----------------------------------------------------------------------
C
      LPEND = .FALSE.
C
      CALL C9ISPF( LPEND, IBATCH )
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) ISTOP
      IF(ISTOP.EQ.1) GOTO 9999
C
C-----------------------------------------------------------------------
C IF PROGRAM RUN COMPLETE THEN 
C RETURN TO INPUT DATA SET SELECTION PANEL.
C-----------------------------------------------------------------------
C
      IF ((LPEND) .AND. (IBATCH.EQ.1) ) THEN
          GOTO 1
      ENDIF
C
C-----------------------------------------------------------------------
C  IF USER HAS MOVED ONTO OUTPUT SCREEN THEN WE GET TO HERE. IF 'CANCEL'
C  IS CLICKED THEN NO OUTPUT AND RETURN TO PROCESSING SCREEN (GOTO 2)
C  IF 'RUN NOW' IS CLICKED THEN WRITE ALL REQUIRED DATA (AND THERE IS
C  QUITE A BIT) TO IDL AND THEN RETURN TO PROCESSING SCREEN.
C-----------------------------------------------------------------------
C
3     CONTINUE
      CALL C9ISPF( LPEND, IBATCH )
C
C-----------------------------------------------------------------------
C READ SIGNAL FROM IDL FOR IMMEDIATE TERMINATION (1) OR CONTINUE (0)
C-----------------------------------------------------------------------
C
      READ(PIPEIN,*) ISTOP
      IF(ISTOP.EQ.1) GOTO 9999
      IF ((LPEND) .AND. (IBATCH.EQ.1) ) THEN
          GOTO 2
      ENDIF
C
C-----------------------------------------------------------------------
C  IF WE GET TO HERE THEN RUNNING HAS BEEN SELECTED. FIRST WRITE ALL
C  DATA TO IDL ROUTINE C9SPF2.PRO WHICH THEN SPAWNS A SECOND 
C  FORTRAN PROCESS TO COMPLETE THE CALCULATION. COMMUNICATIONS WITH
C  THIS FORTRAN ROUTINE RE-COMMENCE WHEN THE CALCULATION IS COMPLETED.
C-----------------------------------------------------------------------
C
      WRITE( PIPEOU, *) MXNENG
      CALL XXFLSH( PIPEOU )
      WRITE( PIPEOU, *) MXNSHL
      CALL XXFLSH( PIPEOU )
      DO 101, I = 1, MXNENG
          WRITE( PIPEOU, *) ENRGYA(I)
101   CONTINUE
      CALL XXFLSH( PIPEOU )
      DO 102, I = 1, MXNENG
          WRITE( PIPEOU, *) ALPHAA(I)
102   CONTINUE
      CALL XXFLSH( PIPEOU )
      DO 103, I = 1, MXNENG
          DO 104, J = 1, MXNSHL
              WRITE( PIPEOU, *) XSECNA(I,J)
104       CONTINUE
          CALL XXFLSH( PIPEOU )
103   CONTINUE
      CALL XXFLSH( PIPEOU )
      DO 105, I = 1, MXNENG
          DO 106, J = 1, (MXNSHL*(MXNSHL+1)/2)
              WRITE( PIPEOU, *) FRACLA(I,J)
106       CONTINUE
          CALL XXFLSH( PIPEOU )
105   CONTINUE
      CALL XXFLSH( PIPEOU )
      WRITE( PIPEOU, *) NMINF 
      CALL XXFLSH( PIPEOU )
      WRITE( PIPEOU, *) NMAXF 
      CALL XXFLSH( PIPEOU )
      WRITE( PIPEOU, *) NENRGY
      CALL XXFLSH( PIPEOU )

      GOTO 3
C
C-----------------------------------------------------------------------
C CLOSE I/O FILES.
C-----------------------------------------------------------------------
C
 9999 CONTINUE

      CLOSE( IUNT10 )
C
      STOP
C
C-----------------------------------------------------------------------
C
      END
