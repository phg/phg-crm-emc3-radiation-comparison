      SUBROUTINE CCOUT0( OFILE  , OUNIT  , IZ     , IECOUNT , INCOUNT ,
     &			 TEREF  , EBRAY  , NERAY  , XRAY    ,
     &                   IEA    , INA    , ITREF  ,
     &			 ITCOUNT,
     &                   EBREF  , NEREF  , TERAY  ,
     &                   MAXTE  , MAXNE  , MAXEB  , 
     &			 ITA    , IEREF  , INREF  ,
     &                   DATE   , TITLE  , BMSTOP , BMEMIS  , BMDENS ,
     &			 F1     , NN     , AVALUE , NLEVEL  , LEVEL  ,
     &                   INFILE )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: CCOUT0 *********************
C
C  PURPOSE:  TO WRITE OUTPUT TO DATA FORMAT ADF21/22 SPECIFICATIONS.
C
C  CALLING PROGRAM: ADAS312
C
C  SUBROUTINE:
C
C  INPUT : (R*8)   TEREF     = THE REFERENCE TARGET TEMPERATURE (eV).
C  INPUT : (R*8)   EBREF     = THE REFERENCE NEUTRAL BEAM ENERGY
C				  (eV/amu).
C  INPUT : (R*8)   NEREF     = THE REFERENCE TARGET ELECTRON DENSITY.
C				  (CM-**3)
C  INPUT : (R*8)   TERAY()   = ARRAY CONTAINING THE TARGET TEMPERATURE.
C  INPUT : (R*8)   EBRAY()   = ARRAY CONTAINING THE NEUTRAL BEAM
C				  ENERGY ( eV/amu ).
C  INPUT : (R*8)   NERAY()   =  ARRAY CONTAINING THE TARGET DENSITY.
C  INPUT : (R*8)   XRAY()    = ARRAY CONTAINING EITHER THE BEAM STOPPING
C				  OR EMISSION COEFFICIENTS (CM**3/S).
C
C  INPUT : (R*8)   IZ        = THE EFFECTIVE NUCLEAR CHARGE OF THE
C				  TARGET PLASMA.
C  INPUT : (I*4)   ITCOUNT   = THE NUMBER OF TARGET TEMPERATURES.
C  INPUT : (I*4)   IECOUNT   = THE NUMBER OF BEAM ENERGIES.
C  INPUT : (I*4)   INCOUNT   = THE NUMBER OF TARGET DENSITIES.
C  INPUT : (I*4)   ITA()     = ARRAY CONTAINING THE INDEX VALUES
C				  OF THE TEMPERATURE ARRAY TERAY().
C  INPUT : (I*4)   ITREF     = THE INDEX VALUE AT WHICH THE REFERENCE
C				  TEMPERATURE CORRESPONDS TO IN THE ARRAY
C				  TERAY.
C  INPUT : (I*4)   IEA()     = ARRAY CONTAINING THE INDEX VALUES
C				  OF THE ENERGY ARRAY EBRAY().
C  INPUT : (I*4)   IEREF     = THE INDEX VALUE AT WHICH THE REFERENCE
C				  ENERGY CORRESPONDS TO IN THE ARRAY
C				  EBRAY().
C  INPUT : (I*4)   INA()     = ARRAY CONTAINING THE INDEX VALUES
C				  OF THE DENSITY ARRAY NERAY().
C  INPUT : (I*4)   INREF     = THE INDEX VALUE AT WHICH THE REFERENCE
C				  DENSITY CORRESPONDS TO IN THE ARRAY
C				  NERAY.
C  INPUT : (I*4)   MAXTE     = THE MAXIMUM NUMBER OF TARGET TEMPERATURES.
C  INPUT : (I*4)   MAXNE     = THE MAXIMUM NUMBER OF TARGET DENSITIES.
C  INPUT : (I*4)   MAXEB     = THE MAXIMUM NUMBER OF BEAM ENERGIES.
C  INPUT : (I*4)   NUM       = THE NUMBER OF ELEMENT SYMBOLS CONTAINED
C				  IN THE ARRAY SPEC().
C	
C
C	(CHR)	OFILE        = OUTPUT FILENAME.
C	(CHR)	SPEC()       = ARRAY CONTAING THE CHEMICAL SYMBOLS
C				  OF THE ELEMENTS UP TO THE FIRST
C				  PERIOD.
C	(CHR)	HEADER       = STRING CONTAINING THE ADAS VERSION,
C				  THE EXECUTING PROGRAM AND DATE.
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          CCFIND     ADAS      ???
C
C
C
C AUTHOR:  HARVEY ANDERSON, UNIVERSITY OF STRATHCLYDE/JET
C          JA8.08
C          TEL. 0141-553-4196
C
C
C DATE:    16/05/97
C
C UNIX-IDL PORT: H.P.SUMMERS
C
C VERSION: 1.1				DATE: 10-07-97
C MODIFIED: HUGH SUMMERS, UNIVERSITY OF STRATHCLYDE
C	    - PUT UNDER S.C.C.S. CONTROL
C
C VERSION: 1.2				DATE: 28-07-97
C MODIFIED: HARVEY ANDERSON, UNIVERSITY OF STRATHCLYDE
C	    - INCLUDED PROGRAM NAME IN THE OUTPUT FILE.
C	    - ADDED ADDITIONAL INFORMATION AT THE BOTTOM OF
C	      THE ADF21/22 TYPE FILE.
C
C VERSION: 1.3 RICHARD MARTIN		DATE: 1-11-97
C               - CORRECTED SYNTAX ERROR IN GETENV COMMAND.
C
C VERSION: 1.4 HARVEY ANDERSON	DATE: 17-03-99
C		- ADDED EXTRA OUTPUT OPTION.
C
C VERSION: 1.5 RICHARD MARTIN		DATE: 28-04-99
C	        - ADDED MISSING COMMA IN FORMAT STATEMENT 2013.
C
C VERSION: 1.6 HARVEY ANDERSON		DATE: 08/09/99
C	        - EXTENDED THE ARRAY CONTAINING THE CHEMICAL ELEMENT
C	  	  SYMBOL TO INCLUDE ALL SPECIES UP TO ZN.
C
C VERSION: 1.7 COSTANZA MAGGI           DATE: 05-02-03
C                - EXTENDED TO ALLOW WRITING BMP FILES
C
C VERSION: 1.8 Martin O'Mullane 	DATE: 06-02-2003
C		  Improve information at bottom of output file.
C
C VERSION: 1.9 Martin O'Mullane 	
C DATE   : 17-04-2019
C          Input argument data was defined as *24 rather than *8.
C
C-----------------------------------------------------------------------
      INTEGER   MAXTE       , MAXNE         , MAXEB
      INTEGER   IEREF	    , INREF         , ITREF           ,
     &		ITCOUNT	    , INCOUNT       , IECOUNT         ,
     &		OUNIT	    , IZ            , BMSTOP          ,
     &		BMEMIS	    , BMDENS        , J               ,   
     &		I           , NLEVEL        , LEVEL           ,   
     &          L1          , L2 
C-----------------------------------------------------------------------
      REAL*8    EBREF       , TEREF	    , NEREF           ,
     &		AVALUE      , F1REF         , NNREF           , 
     &          ECREF
C-----------------------------------------------------------------------
      REAL*8    EBRAY(MAXNE),	NERAY(MAXNE),	TERAY(MAXTE)  ,
     &		XRAY(MAXEB,MAXNE,MAXTE)			      ,
     &		F1(MAXEB,MAXNE,MAXTE,NLEVEL)   		      ,
     &		NN(MAXEB,MAXNE,MAXTE,NLEVEL)	          
C-----------------------------------------------------------------------
      INTEGER   IEA(MAXEB)  ,   ITA(MAXTE)  ,    INA(MAXNE)
C-----------------------------------------------------------------------
      CHARACTER DATE*8      , TITLE*24      , SPEC*2          ,
     &          OFILE*80    , INFILE*80     , XFESYM*2        , 
     &          USER*30
C-----------------------------------------------------------------------
      CALL XXNAME(USER)
      SPEC = XFESYM(IZ)
      CALL XXSTUC(SPEC) 
C-----------------------------------------------------------------------
      OPEN(UNIT=OUNIT,FILE=OFILE,STATUS='UNKNOWN')
 
      IF ( BMSTOP.EQ.1) THEN 
      	WRITE(OUNIT,2000) IZ,XRAY(IEREF,INREF,ITREF),SPEC,DATE
      ENDIF
     
      IF ( BMEMIS.EQ.1) THEN
      	F1REF = F1(IEREF,INREF,ITREF,LEVEL)
      	NNREF = NN(IEREF,INREF,ITREF,LEVEL)    
      	ECREF = NNREF * F1REF * AVALUE / NEREF
      	WRITE(OUNIT,2005) IZ,ECREF,SPEC,DATE
      ENDIF

      IF ( BMDENS.EQ.1) THEN
      	F1REF = F1(IEREF,INREF,ITREF,LEVEL)
      	NNREF = NN(IEREF,INREF,ITREF,LEVEL)    
      	ECREF = NNREF * F1REF
      	WRITE(OUNIT,2005) IZ,ECREF,SPEC,DATE
      ENDIF      
      
      WRITE(OUNIT,2004)
      WRITE(OUNIT,2001) IECOUNT,INCOUNT,TEREF
      WRITE(OUNIT,2004)
      WRITE(OUNIT,2002) (EBRAY(I),I=1,IECOUNT)
      WRITE(OUNIT,2002) (NERAY(I),I=1,INCOUNT)
      WRITE(OUNIT,2004)
      
      IF ( BMSTOP.EQ.1.) THEN 
      	DO I=1,INCOUNT
          WRITE(OUNIT,2002) (XRAY(IEA(J),INA(I),ITREF),J=1,IECOUNT)
      	ENDDO
      ENDIF

      IF ( BMEMIS.EQ.1 ) THEN
      	DO I=1,INCOUNT
        	WRITE(OUNIT,2002) ((F1(IEA(J),INA(I),ITREF,LEVEL)*
     &                         NN(IEA(J),INA(I),ITREF,LEVEL) *
     &                         AVALUE / NERAY(I)) ,J=1,IECOUNT)
      	ENDDO
      ENDIF

      IF ( BMDENS.EQ.1 ) THEN
      	DO I=1,INCOUNT
        	WRITE(OUNIT,2002) ((F1(IEA(J),INA(I),ITREF,LEVEL)*
     &                              NN(IEA(J),INA(I),ITREF,LEVEL))
     &                             ,J=1,IECOUNT)
      	ENDDO
      ENDIF 

      WRITE(OUNIT,2004)
      WRITE(OUNIT,2003) ITCOUNT,EBREF,NEREF
      WRITE(OUNIT,2004)
      WRITE(OUNIT,2002) (TERAY(I),I=1,ITCOUNT)
      WRITE(OUNIT,2004)
      IF (BMSTOP.EQ.1) THEN
      	WRITE(OUNIT,2002) (XRAY(IEREF,INREF,ITA(I)),I=1,ITCOUNT)
      	WRITE(OUNIT,2004)
      	WRITE(OUNIT,2010)
        WRITE(OUNIT,2010)
        WRITE(OUNIT,2006)
      ENDIF
      IF ( BMEMIS.EQ.1 ) THEN
         WRITE(OUNIT,2002) ((F1(IEREF,INREF,ITA(I),LEVEL)*
     &                         NN(IEREF,INREF,ITA(I),LEVEL)*
     &                         AVALUE / NEREF),I=1,ITCOUNT)
         WRITE(OUNIT,2004)
         WRITE(OUNIT,2010)
         WRITE(OUNIT,2010)
         WRITE(OUNIT,2007)
      ENDIF

      IF ( BMDENS.EQ.1 ) THEN
         WRITE(OUNIT,2002) ((F1(IEREF,INREF,ITA(I),LEVEL)*
     &                       NN(IEREF,INREF,ITA(I),LEVEL))
     &                      ,I=1,ITCOUNT)
         WRITE(OUNIT,2004)
         WRITE(OUNIT,2010)
         WRITE(OUNIT,2010)
         WRITE(OUNIT,2008)
      ENDIF
      
C Write out either the A-value or the n-level (note change of index).
      
      call xxslen(infile, L1, L2)
      WRITE(OUNIT,2012) INFILE(L1:L2)

      IF ( BMEMIS.EQ.1 ) THEN
        WRITE(OUNIT,2010)
	WRITE(OUNIT,2013) AVALUE
      ENDIF
      WRITE(OUNIT,2010)

      IF ( BMDENS.EQ.1 ) THEN
        WRITE(OUNIT,2010)
	WRITE(OUNIT,2015) level+1
      ENDIF
      WRITE(OUNIT,2010)
      
C End comments with name of producer

      WRITE(OUNIT,2020)'ADAS312', USER, DATE

      
      CLOSE(OUNIT)
      RETURN
      
     
C-----------------------------------------------------------------------
 2000 FORMAT(1X,I4,1X,'/SCREF=',1PE9.3,
     &         1X,'/SPEC=',A2,1X,'/DATE=',A8,1X,'/CODE=ADAS312')
 2001 FORMAT(1X,I4,1X,I4,1X,'/TREF=',1PE9.3)
 2002 FORMAT(8(1X,1PE9.3),/,8(1X,1PE9.3))
 2003 FORMAT(1X,I4,1X,'/EREF=',1PE9.3,1X,'/NREF=',1PE9.3)
 2004 FORMAT(80('-'))   
 2005 FORMAT(1X,I4,1X,'/ECREF=',1PE9.3,
     &         1X,'/SPEC=',A2,1X,'/DATE=',A8,1X,'/CODE=ADAS312')
 2006 FORMAT('C',2X,'ADAS file type : adf21')
 2007 FORMAT('C',2X,'ADAS file type : adf22 (bme)')
 2008 FORMAT('C',2X,'ADAS file type : adf22 (bmp)')
 2010 FORMAT('C')
 2012 FORMAT('C',2X,'Source file    : ',A) 
 2013 FORMAT('C',2X,'A-VALUE        : ',1PE9.3, ' S-1')
 2015 FORMAT('C',2X,'N-LEVEL        : ',I3)

 2020   FORMAT('C',/,
     &         'C  CODE     : ',1A7/
     &         'C  PRODUCER : ',A30/
     &         'C  DATE     : ',1A8,/,'C',/,'C',79('-'))
C-----------------------------------------------------------------------
      END
