      SUBROUTINE CCINTP( ZDATA   , YDATA   , XDATA    , SRAY    ,
     &                   ITREF   , IEREF   , INREF    ,
     &			 TVAL    , MAXEB   , MAXNE    , MAXTE   ,
     &                   EBRAY   , NERAY   , BMSTOP   , BMEMIS  ,
     &			 BMDENS  , IECOUNT , INCOUNT  ,
     &                   F1      , NN      , AVALUE   , LEVEL   ,
     &			 NLEVEL  , IEA     , INA      , ITA
     &                 )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: CCDATA *********************
C
C  PURPOSE:  TO INTERPOLATE BETWEEN THE EFFECTIVE STOPPING
C		  OR EMISSION COEFFICIENTS.
C
C  CALLING PROGRAM: ADAS312
C
C  SUBROUTINE:
C
C  INPUT : (R*8)  ZDATA(,)   = 
C  INPUT : (R*8)  YDATA()    = 
C  INPUT : (R*8)  XDATA()    = 
C  INPUT : (R*8)  SRAY(,,)   = 
C  INPUT : (I*4)  ITREF      = 
C  INPUT : (I*4)  IEREF      = 
C  INPUT : (I*4)  INREF      = 
C  INPUT : (I*4)  TVAL       = 
C  INPUT : (I*4)  MAXEB      = 
C  INPUT : (I*4)  MAXNE      = MAXIMUM NUMBER OF DENSITIES
C  INPUT : (I*4)  MAXTE      = 
C  INPUT : (R*8)  EBRAY()    = 
C  INPUT : (R*8)  NERAY()    = 
C  INPUT : (I*4)  BMSTOP     = 
C  INPUT : (I*4)  BMEMIS     =
C  INPUT : (I*4)  BMDENS     =       
C  INPUT : (I*4)  IECOUNT    = 
C  INPUT : (I*4)  INCOUNT    = 
C  INPUT : (R*8)  F1(,,,)    = 
C  INPUT : (R*8)  NN(,,,)    = 
C  INPUT : (R*8)  AVALUE     = 
C  INPUT : (I*4)  LEVEL      = 
C  INPUT : (I*4)  NLEVEL     = 
C  INPUT : (I*4)  IEA()      = 
C  INPUT : (I*4)  INA()      = 
C  INPUT : (I*4)  ITA()      = 
C
C          (I*4)  I          = GENERAL INTEGER VARIABLE
C          (I*4)  J          = GENERAL INTEGER VARIABLE
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C
C
C
C AUTHOR:  HARVEY ANDERSON, UNIVERSITY OF STRATHCLYDE/JET
C          JA8.08
C          TEL. 0141-553-4196
C
C
C DATE:    16/05/97
C
C UNIX-IDL PORT: H.P.SUMMERS
C
C VERSION: 1.1				DATE: 10-07-97
C MODIFIED: HUGH SUMMERS, UNIVERSITY OF STRATHCLYDE
C	    - PUT UNDER S.C.C.S. CONTROL
C
C VERSION: 1.2                          DATE: 05-02-03
C MODIFIED: COSTANZA MAGGI, IPP GARCHING
C           - EXTENDED TO ALLOW WRITING BMP FILES
C
C-----------------------------------------------------------------------
       INTEGER  MAXEB         , MAXNE      , MAXTE     ,	
     &          IECOUNT       , INCOUNT    , IEREF     ,
     &          INREF         , ITREF      , I         ,
     &		J             , BMSTOP     , BMEMIS    ,
     &          BMDENS        ,  
     &		TVAL          , NLEVEL     , LEVEL
C-----------------------------------------------------------------------
       INTEGER  IEA(MAXEB)    , INA(MAXNE) , ITA(MAXTE)
C-----------------------------------------------------------------------	
       REAL*8   AVALUE 	  
C-----------------------------------------------------------------------	
       REAL*8	NERAY(MAXNE)       , EBRAY(MAXEB)      ,
     &		XDATA(MAXEB)       , YDATA(MAXNE)      ,
     &          ZDATA(MAXEB,MAXNE) ,
     &		SRAY(MAXEB,MAXNE,MAXTE)                ,
     &          F1(MAXEB,MAXNE,MAXTE,NLEVEL)           ,
     &		NN(MAXEB,MAXNE,MAXTE,NLEVEL)  	  
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
	IF ( BMSTOP .EQ. 1 ) THEN
	   DO I = 1 , IECOUNT
	     DO J = 1 , INCOUNT
	       ZDATA(I,J) = SRAY(I,J,ITREF)* SRAY(IEREF,INREF,TVAL) /
     &	                    SRAY(IEREF,INREF,ITREF)
             ENDDO
           ENDDO
           
           DO I = 1 , IECOUNT
             XDATA(I) = EBRAY(I)
           ENDDO
           DO J = 1 , INCOUNT
             YDATA(J) = NERAY(J)
           ENDDO
                
        ENDIF
        IF ( BMEMIS .EQ. 1 ) THEN
        DO J = 1 , INCOUNT
          DO I = 1 , IECOUNT
             ZDATA(I,J) =(F1(IEA(I),INA(J),ITREF,LEVEL)*
     &          NN(IEA(I),INA(J),ITREF,LEVEL)*AVALUE / NERAY(J))*
     &		( F1(IEREF,INREF,ITA(TVAL),LEVEL)*
     &            NN(IEREF,INREF,ITA(TVAL),LEVEL)*AVALUE / NERAY(J))
     &		     /
     &		( F1(IEREF,INREF,ITREF,LEVEL)*
     &            NN(IEREF,INREF,ITREF,LEVEL)*AVALUE / NERAY(J))    
     		
          ENDDO
        ENDDO
             
              DO I = 1 , IECOUNT
                  XDATA(I) = EBRAY(I)
              ENDDO
              DO J = 1 , INCOUNT
                  YDATA(J) = NERAY(J)
              ENDDO
        ENDIF

        IF ( BMDENS .EQ. 1) THEN
        DO J = 1 , INCOUNT
           DO I = 1 , IECOUNT
              ZDATA(I,J) =(F1(IEA(I),INA(J),ITREF,LEVEL)*
     &                     NN(IEA(I),INA(J),ITREF,LEVEL))*
     &               ( F1(IEREF,INREF,ITA(TVAL),LEVEL)*
     &                 NN(IEREF,INREF,ITA(TVAL),LEVEL))
     &                    /
     &               ( F1(IEREF,INREF,ITREF,LEVEL)*
     &                 NN(IEREF,INREF,ITREF,LEVEL))
           ENDDO
        ENDDO

             DO I = 1 , IECOUNT
                XDATA(I) = EBRAY(I)
             ENDDO
             DO J = 1 , INCOUNT
                YDATA(J) = NERAY(J)
             ENDDO
        ENDIF     
     
        RETURN

        END
