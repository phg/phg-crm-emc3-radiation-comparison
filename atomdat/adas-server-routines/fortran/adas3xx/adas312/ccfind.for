CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas312/ccfind.for,v 1.1 2004/07/06 12:00:45 whitefor Exp $ Date $Date: 2004/07/06 12:00:45 $
CX
      SUBROUTINE CCFIND( ARR    , VALUE  , IMAX   , INDEX  )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: CCFIND *********************
C
C  PURPOSE:  TO  ISOLATE DATA FROM ADF26 DATASET
C
C  CALLING PROGRAM: CCDATA
C
C  SUBROUTINE:
C
C  INPUT : (R*8)  ARR()    = ARRAY TO BE SEARCHED
C                            1ST. DIM.:  
C  INPUT : (R*8)  VALUE    = ???
C  INPUT : (I*4)  IMAX     = NUMBER OF VALUES IN ARR
C
C  OUTPUT: (I*4)  INDEX    = LOCATED POSITION
C
C          (L*4)  FOUND    = ???
C          (C*2)  OVER     = ???
C
C
C ROUTINES:
C          NONE
C
C
C
C AUTHOR:  HARVEY ANDERSON, UNIVERSITY OF STRATHCLYDE/JET
C          JA8.08
C          TEL. 0141-553-4196
C
C
C DATE:    16/05/97
C
C UNIX-IDL PORT: H.P.SUMMERS
C
C VERSION: 1.1				DATE: 10-07-97
C MODIFIED: HUGH SUMMERS, UNIVERSITY OF STRATHCLYDE
C	    - PUT UNDER S.C.C.S. CONTROL
C
C
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      INTEGER  IMAX    , INDEX                                       
C-----------------------------------------------------------------------
      REAL*8   VALUE                                               
C-----------------------------------------------------------------------
      REAL*8   ARR(99) 
C--------------------- --------------------------------------------------
      LOGICAL  OVER    , FOUND                                               
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      OVER=.FALSE.
      FOUND=.FALSE.                                                     
      INDEX=1
C                                                         
  100 IF(.NOT.(FOUND.OR.OVER)) THEN                                     
           OVER=(INDEX.GT.IMAX)                                        
           IF(.NOT.OVER) THEN                                           
                FOUND=(ARR(INDEX).EQ.VALUE)                            
                IF(.NOT.FOUND) INDEX=INDEX+1                            
           ENDIF                                                       
           GO TO 100                                                   
      ENDIF                                                             
      IF(OVER) THEN                                                     
           IMAX=IMAX+1                                                  
           ARR(IMAX)=VALUE                                              
      ENDIF                                                             
C                                                                       
      RETURN
C                                                          
      END                                                               
