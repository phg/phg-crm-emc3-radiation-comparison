CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas312/ccaval.for,v 1.1 2004/07/06 12:00:22 whitefor Exp $ Date $Date: 2004/07/06 12:00:22 $
CX
        SUBROUTINE CCAVAL(NUPPER,NLOWER,AVALUE)      
	IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  	*********   FORTRAN77 SUBROUTINE: CCAVAL *********
C
C  	PURPOSE:  EVALUATES HYDROGENIC TRANISITON PROBABILITIES
C		  FOR TRANSITIONS BETWEEN PRINCIPAL QUANTUM SHELLS.
C
C  	CALLING PROGRAM: ADAS312
C
C	INPUT  :
C
C		(I*4)	NUPPER		: UPPER PRINCIPAL QUANTUM
C					  NUMBER.
C		(I*4)	NLOWER		: LOWER PRINCIPAL QUANTUM
C					  NUMBER.
C		
C	OUPUT :
C		(R*8)	AVALUE		: TRANSITION PROBABILITY.
C
C	GENERAL :
C
C		(R*8)	EN		: UPPER PRINCIPAL QUANTUM
C					  NUMBER.
C		(R*8)	EN1		: LOWER PRINCIPAL QUANTUM
C					  NUMBER.
C		(R*8)	X2		: GENERAL VARIABLE
C		(R*8)	X		: GENERAL VARIABLE
C		(R*8)   G		: BOUND-BOUND GAUNT FACTOR.
C
C
C 	ROUTINES:
C
C          ROUTINE         SOURCE       BRIEF DESCRIPTION
C          -------         ------       -----------------
C	    GBB		    ADAS	EVALUATES BOUND-BOUND
C					GAUNT FACTOR.
C	   
C 	CONTACT : HARVEY ANDERSON
C	    	  UNIVERSITY OF STRATHCLYDE
C	          ANDERSON@PHYS.STRATH.AC.UK
C
C 	DATE    : 11/03/99
C
C	VERSION: 1.1						DATE: 17-03-99
C	MODIFIED: HARVEY ANDERSON
C			- FIRST VERSION
C
C-----------------------------------------------------------------------

	REAL*8   AVALUE , GBB, EN , EN1 , X2 , X , G
C
	INTEGER	 NUPPER , NLOWER
C
C-----------------------------------------------------------------------
C
	EN  = NUPPER
	EN1 = NLOWER

	X2  = 1.0/(EN**2)
	X   = (1.0/(EN1**(2.0/3.0)))*((EN-1.0)/(EN-EN1))**(2.0/3.0)
	
	G = GBB(EN,EN1,X2,X)

	AVALUE = (1.57455808E10)*(1.0/(EN**3*EN1*(EN**2-EN1**2)))*G

        RETURN
	END


