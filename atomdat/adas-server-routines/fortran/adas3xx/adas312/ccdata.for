CX UNIX PORT - SCCS info: Module @(#)ccdata.for	1.2 Date 03/19/99
CX
       SUBROUTINE CCDATA( TERAY  , NERAY  , EBRAY  , N1N    , SRAY  ,
     &                    F1     , F2     , F3     , BN     ,
     &			  NN     , IZ     , INFILE , INUNIT ,
     &                    MAXNE  , MAXTE  , MAXEB  ,
     &			  INCOUNT, ITCOUNT, IECOUNT, LEVEL  ,
     &                    EBREF  , TEREF  , NEREF  , 
     &                    INA    , IEA    , ITA    , NLEVEL , FLAG  )
       IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: CCDATA *********************
C
C  PURPOSE:  TO  FETCH  DATA  FROM  BUNDLE-N POPULATION 
C            FILES OF TYPE ADF26.
C
C  CALLING PROGRAM: ADAS312
C
C  SUBROUTINE:
C
C  INPUT : (C*80) INFILE   = MVS DATA SET NAME OF DATA SET BEING READ
C  INPUT : (I*4)  INUNIT   = UNIT TO WHICH INPUT FILE IS ALLOCATED.
C  INPUT : (I*4)  MAXNE    = MAXIMUM NUMBER OF DENSITIES
C  INPUT : (I*4)  MAXTE    = MAXIMUM NUMBER OF TEMPERATURES
C  INPUT : (I*4)  MAXEB    = MAXIMUM NUMBER OF BEAM ENERGIES
C
C  OUTPUT: (R*8)  TERAY()  = TEMPERATURE SET   (EV)
C                            1ST. DIM.: TEMPERATURE INDEX
C  OUTPUT: (R*8)  NERAY()  = DENSITY SET       (CM-3)
C                            1ST. DIM.: DENSITY INDEX
C  OUTPUT: (R*8)  EBRAY()  = BEAM ENERGY SET   (EV/AMU)
C                            1ST. DIM.: BEAM ENERGY INDEX
C  OUTPUT: (R*8)  N1N(,,)  = ???
C                            1ST. DIM.: BEAM ENERGY INDEX
C                            2ND. DIM.: DENSITY INDEX 
C                            3RD. DIM.: TEMPERATURE INDEX 
C  OUTPUT: (R*8)  SRAY(,,) = COLL. RAD. IONIS COEFFT.   (CM3 S-1)
C                            1ST. DIM.: BEAM ENERGY INDEX
C                            2ND. DIM.: DENSITY INDEX 
C                            3RD. DIM.: TEMPERATURE INDEX
C  OUTPUT: (R*8)  F1(,,,)  = F1 EXPANSION FACTOR OF BN 
C                            1ST. DIM.: BEAM ENERGY INDEX
C                            2ND. DIM.: DENSITY INDEX 
C                            3RD. DIM.: TEMPERATURE INDEX
C                            4TH. DIM.: REPRES. PRINC. QUANTUM SHELL INDEX
C  OUTPUT: (R*8)  F2(,,,)  = F2 EXPANSION FACTOR OF BN 
C                            1ST. DIM.: BEAM ENERGY INDEX
C                            2ND. DIM.: DENSITY INDEX 
C                            3RD. DIM.: TEMPERATURE INDEX
C                            4TH. DIM.: REPRES. PRINC. QUANTUM SHELL INDEX
C  OUTPUT: (R*8)  F3(,,,)  = F3 EXPANSION FACTOR OF BN 
C                            1ST. DIM.: BEAM ENERGY INDEX
C                            2ND. DIM.: DENSITY INDEX 
C                            3RD. DIM.: TEMPERATURE INDEX
C                            4TH. DIM.: REPRES. PRINC. QUANTUM SHELL INDEX
C  OUTPUT: (R*8)  BN(,,,)  = BN FACTOR
C                            1ST. DIM.: BEAM ENERGY INDEX
C                            2ND. DIM.: DENSITY INDEX 
C                            3RD. DIM.: TEMPERATURE INDEX
C                            4TH. DIM.: REPRES. PRINC. QUANTUM SHELL INDEX
C  OUTPUT: (R*8)  NN(,,,)  = POPULATION CONVERSION FACTOR
C                            1ST. DIM.: BEAM ENERGY INDEX
C                            2ND. DIM.: DENSITY INDEX 
C                            3RD. DIM.: TEMPERATURE INDEX
C                            4TH. DIM.: REPRES. PRINC. QUANTUM SHELL INDEX
C  OUTPUT: (I*4)  IZ       = IMPURITY ION CHARGE
C  OUTPUT: (I*4)  INCOUNT  = NUMBER OF DENSITIES
C  OUTPUT: (I*4)  ITCOUNT  = NUMBER OF TEMPERATURES
C  OUTPUT: (I*4)  IECOUNT  = NUMBER OF BEAM ENERGIES
C  OUTPUT: (I*4)  LEVEL    = ??? APPEARS UNUSED ???
C  OUTPUT: (R*8)  EBREF    = REFERENCE BEAM ENERGY (EV/AMU)
C  OUTPUT: (R*8)  TEREF    = REFERENCE TEMPERATURE (EV)
C  OUTPUT: (R*8)  NEREF    = REFERENCE DENSITY     (CM-3)
C  OUTPUT: (I*4)  INA()    = NUMBER OF LEVELS
C                            1ST. DIM.: DENSITY INDEX 
C  OUTPUT: (I*4)  ITA()    = NUMBER OF LEVELS
C                            1ST. DIM.: TEMPERATURE INDEX 
C  OUTPUT: (I*4)  IEA()    = NUMBER OF LEVELS
C                            1ST. DIM.: BEAM ENERGY INDEX 
C  OUTPUT: (I*4)  NLEVEL   = NUMBER OF LEVELS
C
C  OUTPUT: (I*4)  FLAG()   = ARRAY INDICTAING WHETHER DATA HAS BEEN
C			     EXTRACTED FROM THE ADF26 TYPE FILE.
C
C          (I*4)  IT       = GENERAL INDEX
C          (I*4)  IN       = GENERAL INDEX
C          (I*4)  IE       = GENERAL INDEX
C          (R*8)  TE       = GENERAL REAL VARIABLE
C          (R*8)  NE       = GENERAL REAL VARIABLE
C          (R*8)  EB       = GENERAL REAL VARIABLE
C          (C*132)LINE     = GENERAL STRING
C          (C*2)  LEVELS() = PRINC. QU. SHELL STRINGS
C
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          CCFIND     ADAS      ???
C
C
C
C AUTHOR:  HARVEY ANDERSON, UNIVERSITY OF STRATHCLYDE/JET
C          JA8.08
C          TEL. 0141-553-4196
C
C
C DATE:    16/05/97
C
C UNIX-IDL PORT: H.P.SUMMERS
C
C VERSION: 1.1				DATE: 10-07-97
C MODIFIED: HUGH SUMMERS, UNIVERSITY OF STRATHCLYDE
C	    - PUT UNDER S.C.C.S. CONTROL
C
C VERSION: 1.2				DATE: 15-03-99
C MODIFIED: HARVEY ANDERSON, UNIVERSITY OF STRATHCLYDE
C	    - INTRODUCED THE ARRAY FLAG, WHICH IS EMPLOYED TO
C	      INDICATE THE DATA WHICH HAS BEEN EXTRACTED FROM
C	      THE ADF26 TYPE FILE.
C	
C VERSION: 1.3				DATE:	28-04-99
C MODIFIED: RICHARD MARTIN
C  		ADDED MISSING CARRIAGE RETURN AFTER FORMAT STATEMENT 1006
C
C-----------------------------------------------------------------------
       INTEGER	MAXTE     , MAXNE     , MAXEB    ,
     & 		INUNIT    , ITCOUNT   , INCOUNT  ,
     &		IECOUNT	  , IZ        , IT       ,
     &          IN        , IE        , LEVEL    , NLEVEL , IL   
C-----------------------------------------------------------------------
       INTEGER  INA(MAXNE)     ,IEA(MAXEB)       , ITA(MAXTE)      ,
     &		FLAG(NLEVEL)   ,REF, J, ISWITCH
C-----------------------------------------------------------------------
       REAL*8   TE        , EB        , Z         ,  
     &          EBREF     , NEREF     , TEREF     , NE
C-----------------------------------------------------------------------
       REAL*8   NERAY(MAXNE)   , TERAY(MAXTE)     , EBRAY(MAXEB)   ,
     &		SRAY(MAXEB,MAXNE,MAXTE)  , N1N(MAXEB,MAXNE,MAXTE)  ,
     &          F1(MAXEB,MAXNE,MAXTE,NLEVEL)      ,
     &	        F2(MAXEB,MAXNE,MAXTE,NLEVEL)      ,
     &          F3(MAXEB,MAXNE,MAXTE,NLEVEL)      , 
     &	        BN(MAXEB,MAXNE,MAXTE,NLEVEL)	  ,
     &          NN(MAXEB,MAXNE,MAXTE,NLEVEL)      
C-----------------------------------------------------------------------
       CHARACTER LINE*132     , INFILE*80         , LEVELS(25)*2
C-----------------------------------------------------------------------
       DATA  LEVELS/' 1',' 2',' 3',' 4',' 5',' 6',' 7',' 8',' 9','10',
     &              '11','12','13','14','15','16','17','18','19','20',
     &		    '21','22','23','24','25'/	 
C
C-----------------------------------------------------------------------
C
C		     INITIALISE THE ARRAY FLAG(NLEVEL)
C
C-----------------------------------------------------------------------
C
		DO IL = 1, NLEVEL
 		    FLAG(IL) = 0
		ENDDO
C	  
C-----------------------------------------------------------------------
C
      ITCOUNT=0                                                         
      INCOUNT=0                                                      
      IECOUNT=0
      IT = 0
      IN = 0
      IE = 0
C
C-----------------------------------------------------------------------
C
 
      OPEN(UNIT=INUNIT,FILE=INFILE,STATUS='UNKNOWN')
C
 10   READ(INUNIT,'(1A132)',END=200) LINE                          
      IF(LINE(30:31).EQ.'TE') THEN                                     
        READ(LINE,1001) TE                                
        TE=TE/(1.16*10**4)                               
        CALL CCFIND(TERAY,TE,ITCOUNT,IT)                       
      ELSE                                                           
        GO TO 10                                                      
      ENDIF                                                             
C                                                                      
 20   READ(INUNIT,'(1A132)') LINE                               
      IF(LINE(30:31).EQ.'NE') THEN                                     
        READ(LINE,1002) NE                                         
        CALL CCFIND(NERAY,NE,INCOUNT,IN)                                 
      ELSE
        GO TO 20
      ENDIF
C                                                                     
 30   READ(INUNIT,'(1A132)') LINE                                         
      IF(LINE(7:8).EQ.'EH') THEN                                       
        READ(LINE,1003) EB                                              
        CALL CCFIND(EBRAY,EB,IECOUNT,IE)                                  
      ELSE
        GO TO 30
      ENDIF
C
 40   READ(INUNIT,'(1A132)') LINE                               
      IF(LINE(18:20).EQ.'OFF'.AND.LINE(45:56).EQ.'RECOMB COEFF')       
     + THEN                                                             
        READ(LINE,1004) N1N(IE,IN,IT), SRAY(IE,IN,IT)
      ELSE                                                              
        GO TO 40                                                        
      ENDIF                                                                                                               C
      ISWITCH = 1
 50   READ(INUNIT,'(1A132)') LINE  
        IF(LINE(10:10).EQ.'N') THEN
 51	  READ(INUNIT,'(1A132)') LINE
	    DO J=2, NLEVEL
	      IF(LINE(9:10).EQ.LEVELS(J)) THEN
		REF = J-1
		ISWITCH = 0
	      ENDIF
	    ENDDO
      	  IF(ISWITCH.EQ.0.) THEN
             READ(LINE,1006) F1(IE,IN,IT,REF),F2(IE,IN,IT,REF),
     &       F3(IE,IN,IT,REF),BN(IE,IN,IT,REF),NN(IE,IN,IT,REF)
	     FLAG(REF) = 1 
	     ISWITCH = 1
	     GOTO 51
  	  ELSE
	    IF(LINE(9:10).NE.'  ') THEN
		ISWITCH = 1
	    	GOTO 51
	    ENDIF
	  ENDIF
        ELSE
	    GOTO 50
	ENDIF
C	    
 74   READ(INUNIT,'(1A132)') LINE            
      IF(LINE(2:5).EQ.'ZEFF') THEN                                      
        READ(LINE,1005) Z                                            
        IZ = Z
      ELSE                                                            
        GO TO 74                                                      
      ENDIF 
C-------------------------------------------------------------------
C  SET UP REFERENCE VALUES.
C-------------------------------------------------------------------
      IF ( ITCOUNT .EQ. 1 ) THEN
         TEREF = TE
         NEREF = NE
         EBREF = EB
      ELSE IF ( ITCOUNT .EQ. 2 ) THEN
         NEREF = NE
         EBREF = EB
      ENDIF   
C-------------------------------------------------------------------    
C  GO TO START OF MAIN LOOP                                             
C-------------------------------------------------------------------   
      GO TO 10                                                        
 200  CONTINUE
C
      CLOSE(UNIT=INUNIT)
C 
      RETURN
C-----------------------------------------------------------------------	
 1001 FORMAT(34X,1E8.2)                                                 
 1002 FORMAT(34X,1E8.2)                                                 
 1003 FORMAT(11X,1E8.2)                                                 
 1004 FORMAT(31X,1E11.5,56X,1E11.5)
 1005 FORMAT(8X,F3.1) 
 1006 FORMAT(21X,1E11.5,4X,1E11.5,4X,1E11.5,19X,1E11.5,4X,1E11.5)
C-----------------------------------------------------------------------	
       END
