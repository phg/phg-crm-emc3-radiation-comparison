CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas312/ccsort.for,v 1.1 2004/07/06 12:01:45 whitefor Exp $ Date $Date: 2004/07/06 12:01:45 $
CX
      SUBROUTINE CCSORT( XA   , IA   , N )
      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: CCSORT *********************
C
C  PURPOSE:  TO SORT AN ARRAY SO THAT XA IS INCREASING ORDER.
C
C  CALLING PROGRAM: ADAS312
C
C  SUBROUTINE:
C
C  I/O   : (R*8) XA()     = X-VALUES INITIAL THEN SORTED
C  I/O   : (I*4) IA()     = I-VALUES INITIAL THEN SORTED
C  INPUT : (I*4) N        = NUMBER OF VALUES
C
C          (I*4) N1       = GENERAL INTEGER VARIABLE
C          (I*4) I        = GENERAL INTEGER VARIABLE
C          (I*4) I1       = GENERAL INTEGER VARIABLE
C          (I*4) J        = GENERAL INTEGER VARIABLE
C          (I*4) ISWAP    = GENERAL INTEGER VARIABLE
C          (R*8) SWAP     = GENERAL REAL VARIABLE
C
C
C ROUTINES:
C          NONE
C
C
C
C AUTHOR:  HARVEY ANDERSON, UNIVERSITY OF STRATHCLYDE/JET
C          JA8.08
C          TEL. 0141-553-4196
C
C
C DATE:    16/05/97
C
C UNIX-IDL PORT: H.P.SUMMERS
C
C VERSION: 1.1				DATE: 10-07-97
C MODIFIED: HUGH SUMMERS, UNIVERSITY OF STRATHCLYDE
C	    - PUT UNDER S.C.C.S. CONTROL
C
C
C
C-----------------------------------------------------------------------
      INTEGER   ISWAP    , N     , I     , J     , I1     , N1                                              
C-----------------------------------------------------------------------
      INTEGER   IA(*)                                              
C-----------------------------------------------------------------------
      REAL*8    SWAP
C-----------------------------------------------------------------------
      REAL*8    XA(*)                                                  
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      N1=N-1
C                                                            
      DO 10 I=1,N1                                                      
        I1=I+1                                                         
        DO 5 J=I1,N                                                    
          IF(XA(I).LE.XA(J)) GO TO 5                                    
          SWAP=XA(I)                                                    
          XA(I)=XA(J)                                                   
          XA(J)=SWAP                                                   
          ISWAP=IA(I)                                                   
          IA(I)=IA(J)                                                  
          IA(J)=ISWAP                                                   
  5     CONTINUE                                                        
 10   CONTINUE                                                          
      RETURN                                                            
C                                                                       
      END                  
