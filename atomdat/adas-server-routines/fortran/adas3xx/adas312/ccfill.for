CX UNIX PORT - SCCS info: Module @(#)$Header: /home/adascvs/fortran/adas3xx/adas312/ccfill.for,v 1.1 2004/07/06 12:00:38 whitefor Exp $ Date $Date: 2004/07/06 12:00:38 $
CX
      SUBROUTINE CCFILL( NTA   , M )
      IMPLICIT NONE                                         
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 SUBROUTINE: CCFILL *********************
C
C  PURPOSE:  TO FILL AN INTEGER ARRAY WITH ITS OWN INDEX.
C
C  CALLING PROGRAM: ADAS312
C
C  SUBROUTINE:
C
C  I/O   : (I*4)  NTA()    = ARRAY TO BE FILLED
C  INPUT : (I*4)  M        = NUMBER OF ELEMENTS TO BE FILLED
C
C          (I*4)  I        = GENERAL INTEGER
C
C
C ROUTINES:
C          NONE
C
C
C AUTHOR:  HARVEY ANDERSON, UNIVERSITY OF STRATHCLYDE/JET
C          JA8.08
C          TEL. 0141-553-4196
C
C
C DATE:    16/05/97
C
C UNIX-IDL PORT: H.P.SUMMERS
C
C VERSION: 1.1				DATE: 10-07-97
C MODIFIED: HUGH SUMMERS, UNIVERSITY OF STRATHCLYDE
C	    - PUT UNDER S.C.C.S. CONTROL
C
C--------------------------------------------------------------------
      INTEGER    M      , I
C--------------------------------------------------------------------
      INTEGER    NTA(*) 
C--------------------------------------------------------------------                                                            C--------------------------------------------------------------------
      DO 100 I=1,M                                                      
        NTA(I)=I                                                       
 100  CONTINUE                                                          
      RETURN                                                            
C                                                                       
      END                                                               
