      PROGRAM ADAS312

      IMPLICIT NONE
C-----------------------------------------------------------------------
C
C  ****************** FORTRAN77 PROGRAM: ADAS312 **********************
C
C
C  VERSION:  1.0 
C
C  PURPOSE:  PRIMARILY TO EXTRACT EITHER EFFECTIVE EMISSION
C            COEFFICENTS OR EFFECTIVE BEAM STOPPING COEFFICIENTS
C            FROM THE FIRST PASSING FILE FROM ADAS310. THIS
C            ROUTINE ALSO HAS THE CAPABILITY OF EXTRACTING THE
C            POPULATION DENSITY FOR A PARTICULAR QUANTUM SHELL
C            FROM THE FIRST PASSING FILE AS WELL AS MANY OTHER
C            FEATURES.
C
C  PROGRAM:
C
C          (I*4)  MAXTE   = PARAMETER = MAXIMUM NUMBER TEMPERATURES
C          (I*4)  MAXNE   = PARAMETER = MAXIMUM NUMBER DENSITIES
C          (I*4)  MAXEB   = PARAMETER = MAXIMUM NUMBER BEAM ENERGIES
C          (I*4)  NLEVEL  = PARAMETER = MAXIMUM NUMBER QUANTUM LEVELS
C
C	  
C
C ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          I4UNIT     ADAS      FETCH UNIT NUMBER FOR OUTPUT OF MESSAGES
C          CCFIND     ADAS      ???
C	   CCAVAL     ADAS	EVALUATES N RESOLVED A-COEFFICIENTS
C
C
C
C AUTHOR:  HARVEY ANDERSON, UNIVERSITY OF STRATHCLYDE/JET
C          JA8.08
C          TEL. 0141-553-4196
C
C
C DATE:    16/05/97
C
C UNIX-IDL PORT: H.P.SUMMERS
C
C VERSION: 1.1				DATE: 10-07-97
C MODIFIED: HUGH SUMMERS, UNIVERSITY OF STRATHCLYDE
C	    - PUT UNDER S.C.C.S. CONTROL
C VERSION: 1.2				DATE: 28-07-97
C MODIFIED: HARVEY ANDERSON, UNIVERSITY OF STRATHCLYDE
C	    - CORRECTED ERROR ASSOCIATED WITH THE BEAM
C	      EMISSION COEFFICIENTS. 
C VERSION: 1.3 				DATE: 28-11-97
C MODIFIED: RICHARD MARTIN
C	    - CHANGED 'READ(PIPEIN,*) OFILE' to 'READ(PIPEIN,'(A)') OFILE'
C
C VERSION: 1.4 				DATE: 15-03-99
C MODIFIED: HARVEY ANDERSON
C	    - INTRODUCED THE ARRAY FLAG WHICH IS USED TO
C	      TO INDICATE WHAT DATA HAS BEEN EXTRACTED FROM 
C	      THE ADF26 TYPE FILE.
C	    - COMMUNICATE THE RESULTS OF THE ARRAY FLAG TO 
C	      THE IDL.
C	    - INTRODUCED THE ROUTINE CCAVAL WHICH CALCULATES
C	      THE REQUIRED A-COEFFICIENTS USING THE BOUND-
C	      BOUND GAUNT FACTOR. THE A-COEFFICIENT IS
C	      WRITTEN TO THE BOTTOM OF THE ADF21 TYPE FILE
C
C VERSION: 1.5                          DATE: 05-02-2003
C MODIFIED: COSTANZA MAGGI, IPP GARCHING
C           - EXTENDED TO ALLOW WRITING BMP FILES
C
C
C-----------------------------------------------------------------------
      INTEGER   MAXTE    , MAXNE    , MAXEB    , NLEVEL
      INTEGER   INUNIT   , OUNIT    , DATAY    , DATAN  
      INTEGER   PIPEIN   , PIPEOUT
C-----------------------------------------------------------------------
      PARAMETER ( MAXTE=25  , MAXNE=25  , MAXEB=25  , NLEVEL=25   )
      PARAMETER ( INUNIT=4  , OUNIT=7   , DATAY=1   , DATAN = 0   )
      PARAMETER ( PIPEIN=5  , PIPEOUT=6 )
C-----------------------------------------------------------------------
      INTEGER   ITREF       , I         , IEREF     , INREF       , 
     &          J           , GRAPHOUT  ,
     &          BMSTOP      , BMDENS    ,
     &          BMEMIS      , IZ        , INCOUNT   ,
     &          IECOUNT     , ITCOUNT   , LEVEL     ,
     &          OPTION      , ICANCEL   ,
     &          LOWLEV      , UPPLEV                ,
     &          IDONE       , IMENU     , TEXTOUT   ,
     &          TVAL       
C-----------------------------------------------------------------------
      REAL*8    TEREF       , NEREF     , EBREF     , AVALUE 
C-----------------------------------------------------------------------
      CHARACTER REP*3       ,  DATE*8   , INFILE*80 , OFILE*80    ,  
     &          TITLE*24
C-----------------------------------------------------------------------
      INTEGER   FLAG(NLEVEL)
C-----------------------------------------------------------------------
      INTEGER   IEA(MAXEB)  , ITA(MAXTE),INA(MAXNE)
C-----------------------------------------------------------------------
      REAL*8   NERAY(MAXNE) , TERAY(MAXTE)  , EBRAY(MAXEB)        ,
     &         SRAY(MAXEB,MAXNE,MAXTE)      ,
     &         F1(MAXEB,MAXNE,MAXTE,NLEVEL) ,
     &         F2(MAXEB,MAXNE,MAXTE,NLEVEL) ,
     &         F3(MAXEB,MAXNE,MAXTE,NLEVEL) , 
     &         BN(MAXEB,MAXNE,MAXTE,NLEVEL) ,
     &         N1N(MAXEB,MAXNE,MAXTE)       ,
     &         NN(MAXEB,MAXNE,MAXTE,NLEVEL) ,
     &         ZDATA(MAXEB,MAXNE)           ,
     &         XDATA(MAXEB)                 ,
     &         YDATA(MAXNE)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C *************************** MAIN PROGRAM ****************************
C-----------------------------------------------------------------------
C
C
C-----------------------------------------------------------------------
C SET MACHINE DEPENDENT ADAS CONFIGURATION VALUES.
C-----------------------------------------------------------------------

      CALL XX0000

C-----------------------------------------------------------------------
C GET CURRENT DATE.
C-----------------------------------------------------------------------

      CALL XXDATE( DATE )
 
 
1111	CONTINUE

C-----------------------------------------------------------------------
C GET INPUT DATA SET NAME (VIA IDL PIPE)
C-----------------------------------------------------------------------

      CALL CCSPF0( REP , INFILE )

C-----------------------------------------------------------------------
C IF PROGRAM RUN IS COMPLETED: END RUN
C-----------------------------------------------------------------------

         IF (REP.EQ.'YES') THEN
            GOTO 9999
         ENDIF

C--------------------------------------------------------------------
C	CALL ROUTINE WHICH READS THE FIRST PASSING FILE.
C--------------------------------------------------------------------

        CALL CCDATA(TERAY,NERAY,EBRAY,N1N,SRAY,F1,F2,F3,BN,
     &	NN,IZ,INFILE,INUNIT,MAXNE,MAXTE,MAXEB,INCOUNT,
     &  ITCOUNT,IECOUNT,LEVEL,EBREF,TEREF,NEREF,INA,IEA,ITA,
     &  NLEVEL,FLAG )

C--------------------------------------------------------------------  
C     SORTS EBRAY,NERAY AND TERAY IN INCREASING ORDER                
C-------------------------------------------------------------------

      CALL CCFILL(IEA,IECOUNT)                                      
      CALL CCSORT(EBRAY,IEA,IECOUNT)                                  
      CALL CCFILL(INA,INCOUNT)                                      
      CALL CCSORT(NERAY,INA,INCOUNT)                                 
      CALL CCFILL(ITA,ITCOUNT)                                       
      CALL CCSORT(TERAY,ITA,ITCOUNT)          

C-------------------------------------------------------------------
C     FIND REFERENCE VALUES IN DATA
C-------------------------------------------------------------------

      ITREF = 0
      DO I=1,ITCOUNT
        IF (TERAY(I).EQ.TEREF) ITREF = ITA(I)
      END DO
      IF (ITREF.EQ.0) STOP 'REFERENCE TEMPERATURE NOT IN DATA!'
      IEREF = 0
      DO I=1,IECOUNT
        IF (EBRAY(I).EQ.EBREF) IEREF = IEA(I)
      END DO
      IF (IEREF.EQ.0) STOP 'REFERENCE BEAM ENERGY NOT IN DATA!'
      INREF = 0
      DO I=1,INCOUNT
        IF (NERAY(I).EQ.NEREF) INREF = INA(I)
      END DO
      IF (INREF.EQ.0) STOP 'REFERENCE DENSITY NOT IN DATA!' 
          
C-------------------------------------------------------------------
C	COMMUNICATING TO IDL- SUMMARISING CONTENTS OF INPUT FILE
C-------------------------------------------------------------------

      	WRITE(PIPEOUT,*)  TEREF
      	CALL XXFLSH(PIPEOUT)
      	WRITE(PIPEOUT,*)  EBREF
      	CALL XXFLSH(PIPEOUT)
      	WRITE(PIPEOUT,*)  NEREF
      	CALL XXFLSH(PIPEOUT)
      	WRITE(PIPEOUT,*)  TERAY(1)
      	CALL XXFLSH(PIPEOUT)
      	WRITE(PIPEOUT,*)  TERAY(ITCOUNT)
      	CALL XXFLSH(PIPEOUT)
      	WRITE(PIPEOUT,*)  EBRAY(1)
      	CALL XXFLSH(PIPEOUT)
      	WRITE(PIPEOUT,*)  EBRAY(IECOUNT)
      	CALL XXFLSH(PIPEOUT)
      	WRITE(PIPEOUT,*)  NERAY(1)
      	CALL XXFLSH(PIPEOUT)
      	WRITE(PIPEOUT,*)  NERAY(INCOUNT)
      	CALL XXFLSH(PIPEOUT)
      	WRITE(PIPEOUT,*)  ITCOUNT
      	CALL XXFLSH(PIPEOUT)
      	DO I = 1, ITCOUNT
      		WRITE(PIPEOUT,*) TERAY(I)
      		CALL XXFLSH(PIPEOUT)
      	ENDDO	
	
 2222	READ(PIPEIN,*) IDONE
	READ(PIPEIN,*) ICANCEL
	READ(PIPEIN,*) IMENU
	

	IF ( IDONE .EQ. 1 ) THEN 
	    READ(PIPEIN,*) OPTION
	     IF ( OPTION .EQ. 1) THEN 
			 BMSTOP=1
			 BMEMIS=0
                         BMDENS=0
			 LEVEL=1
	     ENDIF
	     IF ( OPTION .EQ. 2) THEN
			 BMSTOP=0
			 BMEMIS=1
                         BMDENS=0
			 READ(PIPEIN,*)  LOWLEV
			 READ(PIPEIN,*)  UPPLEV
		         LEVEL = UPPLEV-1
			 IF ( FLAG(LEVEL) .NE. 1 ) THEN
			    WRITE(PIPEOUT,*) DATAN
      		            CALL XXFLSH(PIPEOUT)
			    GOTO 2222
			 ELSE
			    WRITE(PIPEOUT,*) DATAY
      		            CALL XXFLSH(PIPEOUT)
			    CALL CCAVAL(UPPLEV,LOWLEV,AVALUE)
			 ENDIF
	    ENDIF
            IF ( OPTION .EQ. 3) THEN
			 BMSTOP=0
			 BMEMIS=0
                         BMDENS=1
			 READ(PIPEIN,*)  UPPLEV
		         LEVEL = UPPLEV-1
			 IF ( FLAG(LEVEL) .NE. 1 ) THEN
			    WRITE(PIPEOUT,*) DATAN
      		            CALL XXFLSH(PIPEOUT)
			    GOTO 2222
			 ELSE
			    WRITE(PIPEOUT,*) DATAY
      		            CALL XXFLSH(PIPEOUT)
			 ENDIF
	    ENDIF
        ENDIF

	   IF ( ICANCEL .EQ. 1 ) THEN
	   	GOTO 1111
	   ENDIF

	   IF ( IMENU .EQ. 1 ) THEN
	        GOTO 9999
	   ENDIF

 3333   CONTINUE
	READ(PIPEIN,*) IDONE
	READ(PIPEIN,*) ICANCEL
	READ(PIPEIN,*) IMENU

	IF ( ICANCEL .EQ. 1 ) THEN
	     GOTO 2222
	ENDIF

	   IF ( IMENU .EQ. 1 ) THEN
	        GOTO 9999
	   ENDIF

	IF ( IDONE .EQ. 1 ) THEN
	      READ(PIPEIN,*) TEXTOUT
	      IF ( TEXTOUT .EQ. 1 ) THEN
	   	  READ(PIPEIN,'(A)') OFILE 
	   		
C-------------------------------------------------------------------
C	CALL ROUTINE WHICH WRITES OUT ADF21/22 TYPE FILE.
C-------------------------------------------------------------------

                  CALL CCOUT0(OFILE, OUNIT, IZ, IECOUNT,INCOUNT ,
     &		              TEREF, EBRAY,NERAY,SRAY,IEA,INA,ITREF  ,
     &		              ITCOUNT,EBREF,NEREF,TERAY,MAXTE,MAXNE  ,        
     &		              MAXEB,ITA,IEREF,INREF,DATE,TITLE,BMSTOP,        
     &		              BMEMIS,BMDENS,F1,NN,AVALUE,NLEVEL,LEVEL,
     &                        INFILE) 
     
              ENDIF

              READ(PIPEIN,*) GRAPHOUT
          
              IF ( GRAPHOUT .EQ. 1 ) THEN
              	
                READ(PIPEIN,*) TVAL
	   		
C-------------------------------------------------------------------
C	CALL ROUTINE WHICH SCALES EITHER EFFECTIVE EMISSION
C	OR STOPPING COEFFICICENTS.
C-------------------------------------------------------------------	

                CALL CCINTP(ZDATA,YDATA,XDATA,SRAY,ITREF,IEREF,INREF,
     &			    TVAL,MAXEB,MAXNE,MAXTE,EBRAY,NERAY,BMSTOP,
     &			    BMEMIS,BMDENS,IECOUNT,INCOUNT,F1,NN,AVALUE,
     &			    LEVEL,NLEVEL,IEA,INA,ITA)
     
                WRITE(PIPEOUT,*) IECOUNT
                CALL XXFLSH(PIPEOUT)
                WRITE(PIPEOUT,*) INCOUNT
                CALL XXFLSH(PIPEOUT)
                
                DO J = 1 ,INCOUNT
                 DO I = 1 , IECOUNT
                   WRITE(pipeout,*) ZDATA(J,I)
                   CALL XXFLSH(PIPEOUT)
              	 ENDDO
                ENDDO
                DO I = 1 , IECOUNT
              	   WRITE(PIPEOUT,*) XDATA(I)
              	   CALL XXFLSH(PIPEOUT)
                ENDDO
                DO I = 1 , INCOUNT
              	   WRITE(PIPEOUT,*) YDATA(I)
              	   CALL XXFLSH(PIPEOUT)
                ENDDO
              ENDIF
	      READ(PIPEIN,*) IDONE
	      READ(PIPEIN,*) ICANCEL
	      READ(PIPEIN,*) IMENU

	      IF ( ICANCEL .EQ. 1 ) THEN
	   	GOTO 2222
	      ENDIF

	      IF ( IMENU .EQ. 1 ) THEN
	        GOTO 9999
	      ENDIF 
	                 
              GOTO 3333
           
        ENDIF

 9999	CONTINUE
                                                                  
C--------------------------------------------------------------------  
      END
