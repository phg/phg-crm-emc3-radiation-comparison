;---------------------------------------------------------------------------
; Use of the ADAS IDL routine to plot the beam emission spectrum.
;
; 09-12-2017:
;---------------------------------------------------------------------------

PRO bes_spectrum

; Define the beam, plasma and observer

beam   = {mass : 2.0, energy : 40.0e3, te : 10.0, density : 4.27e9, dc_x : 0.0, dc_y : 0.0, dc_z : 1.0}
plasma = {mass : 2.0, te : 8e3, density : 5.0e13, zeff : 2.0}
bfield = {value : 3.3915, dc_x : 0.788, dc_y : 0.0053, dc_z : 0.6152}
efield = {value : 0.0000, dc_x : 1.000, dc_y : 0.0000, dc_z : 0.0000}
obs    = {dc_x : 0.8701, dc_y : -0.047, dc_z : 0.4905, sigma : 0.00, pi : 1.0}

; Set wavelength limits and the spectral resolution of the returned feature.
; Ask for a Doppler broadening to be applied

wave_min = 6510
wave_max = 6550
npix     = 500

adas305_get_stark,  beam           = beam,          $
                    plasma         = plasma,        $
                    bfield         = bfield,        $
                    efield         = efield,        $
                    obs            = obs,           $
                    n_lower        = 2,             $
                    n_upper        = 3,             $
                    wave_comp      = wave_comp,     $
                    emiss_comp     = emiss_comp,    $
                    wave_min       = wave_min,      $
                    wave_max       = wave_max,      $
                    npix           = npix,          $
                    emiss_doppler  = demiss,        $
                    /doppler

; Construct the wavelength vector for the feature and normalize to per-Angstrom

wave   = adas_vector(low=wave_min,high=wave_max,num=npix, /linear)
demiss = demiss / (wave[1]-wave[0])

; Plot the feature and overplot the components

plot, wave, demiss, $
      xtitle = 'Wavelength (A)', $
      ytitle = 'Column emissivity (ph cm!u-2!n s!u-1!n A!u-1!n)'

for j = 0, n_elements(wave_comp)-1 do oplot, [wave_comp[j],wave_comp[j]], [0, emiss_comp[j]]
plots, wave_comp, emiss_comp, psym=2

END
