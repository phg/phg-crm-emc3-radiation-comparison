;----------------------------------------------------------------------------
; Use an ADAS IDL routine to plot the population dependence of excited
; levels.
;
; 16-05-2018
;----------------------------------------------------------------------------

PRO population_plot

; C4+ as a function of Te

root  = getenv('ADASCENT')
adf04 = root + '/adf04/adas#6/mom97_ls#c4.dat'

te        = [30.0, 100.0]
dens      = 1.0e13
meta_list = [1]

run_adas208, adf04 = adf04,                         $
             te = te, dens = dens,                  $
             unit_te = 'ev', unit_dens = 'cm-3',    $
             meta = meta_list,                        $
             log = 'paper-208.txt',  pass_dir='./', pop=pop

population = pop.dependence
index      = indgen(pop.numlev) + 1


; Plot the population of the excited levels

plot, [0, pop.numlev], [1e-15, 1.0], /nodata, /ylog, $
      xtitle = 'Excited level index',                $
      ytitle = 'Relative population (N(i)/N(gnd))',  $
      title  = 'C!u4+!n'

oplot, index, population[*,0,0]
oplot, index, population[*,0,1], linestyle=2

ind = pop.numlev/2
xyouts, index[ind], 8.0*population[ind,0,0], 'Te = 30eV'
xyouts, index[ind], 8.0*population[ind,0,1], 'Te = 100eV'

END
