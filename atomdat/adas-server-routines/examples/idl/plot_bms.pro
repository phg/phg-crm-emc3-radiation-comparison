;----------------------------------------------------------------------------
; Use an ADAS IDL routine to plot an effective beam stopping coefficient
; and the contribution of its constituent elements.
;
; 13-02-2018
;----------------------------------------------------------------------------

PRO plot_bms

; Consider a plasma with 4% Be and 1% C impurity content

root = getenv('ADASCENT')
files = [root + '/adf21/bms10#h/bms10#h_h1.dat',  $
         root + '/adf21/bms97#h/bms97#h_be4.dat', $
         root + '/adf21/bms97#h/bms97#h_c6.dat']
element  = ['H', 'Be', 'C']
z0       = [1, 4, 6]
fraction = [0.95, 0.04, 0.01]

; Get the data as a function of beam energy for a plasma at fixed conditions

ieval   = 32
energy  = adas_vector(low=10, high=130, num=ieval)*1000.0
te_pl   = fltarr(ieval) + 2.0e3
dens_pl = fltarr(ieval) + 1e13

read_adf21, files=files, fraction=fraction, energy=energy, $
            te=te_pl, dens=dens_pl, data=bms


; Plot the total/effective coefficient

!x.omargin = [1,1]

plot, energy / 1e3, bms, $
      xtitle = 'Energy (keV/amu)', $
      ytitle = 'Beam stopping coefficient (cm!u3!n s!u-1!n)'


; Overplot the contributions of the fuel and impurities
;
; frac for a single impurity is a vector of size one due to an oddity of IDL.
;
; The appropriate density for the individual element is given by
; equation 4.4.5 of Chapter 3, section 4 of the ADAS manual (adas304).
;
; The multiplier on the individual stopping coefficient contribution
; to the total (effective) coefficient is given by equation 4.4.7.

for j = 0, n_elements(files)-1 do begin

   frac     = [1.0]
   dens_one = dens_pl * total(z0^2*fraction) / total(z0*fraction) / z0[j]
   mul      = z0[j] * fraction[j] / total(z0*fraction)

   read_adf21, files=files[j], fraction=frac, energy=energy, $
               te=te_pl, dens=dens_one, data=bms_p

   oplot, energy/ 1e3, mul * bms_p, linestyle=j+1
   xyouts, 132, mul * bms_p[ieval-1], element[j]

endfor

END
