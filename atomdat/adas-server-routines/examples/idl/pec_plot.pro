;---------------------------------------------------------------------------
; Use of ADAS IDL routine to plot photon emissivity coefficient.
;---------------------------------------------------------------------------

PRO pec_plot

; Interpolate data from adf15

root = getenv('ADASCENT')

file  = root + '/adf15/pec96#c/pec96#c_pju#c4.dat'
itval = 70
te    = adas_vector(low=10, high=1e3, num=itval)
dens  = fltarr(itval) + 1e13


read_adf15, file=file, block=50, te=te, dens=dens, data=coeff_exc
read_adf15, file=file, block=164, te=te, dens=dens, data=coeff_rec

;----------------
; Plot the figure
;----------------

xmin = min(te, max=xmax)
ymin = min([coeff_exc, coeff_rec], max=ymax)

plot_oo, [xmin, xmax], [ymin, ymax], /nodata, $
         xtitle = 'Te (eV)', $
         ytitle = 'photon emissivity coefficinet (cm!u3!n s!u-1!n)', $
         title  = 'C!u4+!n: 1s!u2!n !u1!nS!d0!n - 1s 2p !u1!nP!d1!n'

oplot, te, coeff_exc
oplot, te, coeff_rec, linestyle=2

END
