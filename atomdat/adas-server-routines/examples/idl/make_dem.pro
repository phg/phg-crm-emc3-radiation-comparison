;----------------------------------------------------------------------------
; Show the non-interactive way to run the differential emission measure (DEM)
; fitting program, adas601. This is loosely based on the example in the
; manual for adas601.
;
; The solar abundance file, sample observed intensities and G(Te) data
; are taken from the central ADAS ...adas/arch601/ store.
;
; This program produces a file in the directory from which it is run.
;
; 18-04-2019
;----------------------------------------------------------------------------


PRO make_dem

; Examples in central ADAS

files = { abundance : getenv('ADASHOME') + '/adas/arch601/abundance/abund.alan', $
          intensity : getenv('ADASHOME') + '/adas/arch601/intensity/A.dat', $
          kernel    : getenv('ADASHOME') + '/adas/arch601/kernel/goft.pmwa'}

; Choose a range and spacing of temperature

te = { num  : 100,    $
       tmin : 5.0e4, $
       tmax : 1.0e7  }

; Run the integral inversion and output the results to a file

run_adas601, files = files, te = te, smoothing = 3, weight = 3, $
             outfile = 'dem_example.dat', $
             kernel = kernel, dem = dem



; Check the results by plotting the kernel contribution functions and
; the derived DEM

!p.multi = [0, 1, 2]

xmin = min(kernel.te, max=xmax)
ymin = 1e-20
ymax = 1e-5

plot_oo, [xmin, xmax], [ymin, ymax], /nodata, $
         xtitle = 'Temperature (K)', $
         ytitle = 'G(Te)'


for j = 0, kernel.num-1 do begin
   oplot, kernel.te[0:kernel.nte[j]-1,j], kernel.goft[0:kernel.nte[j]-1,j]
endfor



xmin = min(dem.te, max=xmax)
ymin = 1e16
ymax = 1e24

plot_oo, [xmin, xmax], [ymin, ymax], /nodata, $
         xtitle = 'Temperature (K)', $
         ytitle = 'DEM'

oplot, dem.te, dem.final
oplot, dem.te, dem.prior, linestyle=1
plots, dem.tran_te, dem.tran_dem, psym=2

END
