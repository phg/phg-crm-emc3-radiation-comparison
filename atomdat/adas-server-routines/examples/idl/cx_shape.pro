;----------------------------------------------------------------------------
; Use the ADAS IDL routine, run_adas308, to calculate the components in the
; CVI n=8-7 charge exchange transition. Generate the line shape for a
; number of ion temperatures with the ADAS c5dplr routine.
;
; 08-04-2019
;----------------------------------------------------------------------------

PRO cx_shape

; State selective CX data

adf01 = getenv('ADASHOME') + '/adas/adf01/qcx#h0/qcx#h0_old#c6.dat'

; Plasma conditions - fixed plasma

dens   = 1e13
dion   = 1e13
energy = [6e5]        ; This and frac can be an array
frac   = [1]
n1req  = 8
n2req  = 7
mass   = 12.0


; Parameters for Doppler broadening

ndpix  = 1000L
npix   = ndpix
ndcomp = 20

wvmin  = 5285.0
wvmax  = 5295.0
dwave  = fltarr(ndpix)
wave   = adas_vector(low=wvmin, high=wvmax, num=ndpix, /linear)
dlam   = wave[1]-wave[0]


; Choose a number of temperatures - let Te=Tion here

tev = [50.0, 100.0, 500.0, 1000.0, 3000.0]
nte = n_elements(tev)

em_dp = fltarr(ndpix, nte)

for j = 0, nte-1 do begin

   ; calculate components

   run_adas308, adf01  = adf01,  $
                mass   = mass,   $
                te     = tev[j], $
                tion   = tev[j], $
                dens   = dens,   $
                dion   = dion,   $
                frac   = frac,   $
                energy = energy, $
                n1req  = n1req,  $
                n2req  = n2req,  $
                data   = data

   ; Doppler broaden them

   ncomp  = data.ncomp
   wvcomp = data.wave
   emcomp = data.emiss
   emiss  = fltarr(ndpix)

   c5dplr, ndpix, npix, wvmin, wvmax, ndcomp,      $
           ncomp, wvcomp, emcomp, tev[j], mass,    $
           emiss
   em_dp[*,j] = emiss / dlam

endfor


; Plot the results

xmin = wvmin
xmax = wvmax
ymin = min(em_dp, max=ymax)

plot, [xmin, xmax], [ymin, ymax], /nodata, xstyle=1, $
      xtitle = 'Wavelength (Angstrom)', $
      ytitle = 'Column emissivity (ph cm!u-2!n s!u-1!n A!u-1!n', $
      title  = 'CVI 8-7 Doppler broadened emission'

for j = 0, nte-1 do begin

   oplot, wave, em_dp[*,j], linestyle=j

   x = 5286.0
   y = (0.9-j*0.05) * ymax
   oplot, [x, x+0.5], [y, y], linestyle=j
   xyouts, x+0.7, y, string(tev[j], format='(f6.1)') + 'eV'

endfor

END
