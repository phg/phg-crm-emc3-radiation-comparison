;----------------------------------------------------------------------------
; Use the non-interactive, run_adas416, to bundle GCR neon adf11 files.
; Compare the ionization balance using the bundled data to the standard
; stage-to-stage results from run_adas405 and the diagnostic output from
; run_adas416.
;
; Plot the stage-to-stage and bundle-to-bundle ionization and recombination
; rates.
;
; This demo program will create 7 files in the directory from where
; it is run.
;
; 10-04-2019
;----------------------------------------------------------------------------

PRO test_bundling

!p.multi = [0, 2, 1]

; Generate the bundled adf11 files and diagnostic fractional abundances

script = getenv('ADASHOME') + '/adas/scripts416/partition_02_ne96.dat'

run_adas416, script=script, frac_p=fr_p, frac_c=fr_c



; For user selected parameters, generate an ionization balance
; from stage-stage data

itval = 100
te    = adas_vector(low=1, high=1e3, num=itval)
dens  = fltarr(itval) + 1e13

run_adas405, uid='adas', elem='ne', year=96, te=te, dens=dens, frac=frac_96

; and from the just produced bundled data

files = { acd : 'acd96_ne_02.dat', $
          scd : 'scd96_ne_02.dat'  }

run_adas405, uid='adas', elem='ne', year=96, te=te, dens=dens, $
             files=files, frac=frac_bdl



; Plot to compare these to the diagnostic abundances calculated by
; adas416 on the Te/density grid of the un-bundled adf11 files. Choose
; the closest density in the adf11 file to the one used for the
; run_adas405 calculation.

xmin = min(te, max=xmax)
id   = i4indf(fr_p.dens, dens[0])

n_stg = n_elements(frac_96.stage)
n_stg_bdl = n_elements(frac_bdl.stage)

plot_oo, [xmin, xmax], [0.001, 1.2], /nodata, ystyle=1, $
         xtitle = 'Te (eV)', $
         ytitle = 'fractional abundance'

for j = 0, n_stg-1 do begin
   oplot, te, frac_96.ion[*,j]
   oplot, fr_p.te, fr_p.ion[*,id,j], psym=2
endfor

for j = 0, n_stg_bdl-1 do begin
   oplot, te, frac_bdl.ion[*,j], thick=4
   oplot, fr_c.te, fr_c.ion[*,id,j], psym=6
endfor

oplot, [80, 100], [0.6, 0.6]
xyouts, 110, 0.6, 'Stage resolved'
oplot, [80, 100], [0.5, 0.5], thick=4
xyouts, 110, 0.5, 'Bundle resolved'

plots, 80, 0.4, psym=6
plots, 85, 0.4, psym=2
xyouts, 90, 0.4, 'adas416 diagnostic'



; Interpolate the ionization and recombination rates of the stages and bundles.

itval = 20
te    = adas_vector(low=1, high=1e3, num=itval)
dens  = fltarr(itval) + 1e13

scd_96  = fltarr(itval, n_stg-1)
scd_bdl = fltarr(itval, n_stg_bdl-1)
acd_96  = fltarr(itval, n_stg-1)
acd_bdl = fltarr(itval, n_stg_bdl-1)

for j = 0, n_stg-2 do begin

   is1 = j + 1

   file = getenv('ADASHOME') + '/adas/adf11/scd96/scd96_ne.dat'
   read_adf11, file=file, iz0=10, is1=is1, te=te, dens=dens, class='scd', $
            data=data_s
   scd_96[*,j] = data_s

   file = getenv('ADASHOME') + '/adas/adf11/acd96/acd96_ne.dat'
   read_adf11, file=file, iz0=10, is1=is1, te=te, dens=dens, class='acd', $
            data=data_r
   acd_96[*,j] = data_r

endfor

 for j = 0, n_stg_bdl-2 do begin

   is1 = j + 1

   file = 'scd96_ne_02.dat'
   read_adf11, file=file, iz0=10, is1=is1, te=te, dens=dens, class='scd', $
            data=data_s
   scd_bdl[*,j] = data_s

   file = 'acd96_ne_02.dat'
   read_adf11, file=file, iz0=10, is1=is1, te=te, dens=dens, class='acd', $
            data=data_r
   acd_bdl[*,j] = data_r

endfor


; Compare the stage-to-stage and bundle-to-bundle rates

xmin = min(te, max=xmax)
ymin = 1.0e-20
ymax = 1.0e-7

plot_oo, [xmin, xmax], [ymin, ymax], /nodata, $
         xtitle = 'Te (eV)', $
         ytitle = 'Rate coefficient (cm!u3!n s!u-1!n)'


for j = 0, n_stg-2 do begin
   oplot, te, scd_96[*,j]
   oplot, te, acd_96[*,j]
endfor

for j = 0, n_stg_bdl-2 do begin
   oplot, te, scd_bdl[*,j], linestyle=2, thick=4
   oplot, te, acd_bdl[*,j], linestyle=2, thick=4
endfor

oplot, [1.5, 2.5], [1e-6, 1e-6]
xyouts, 2.8, 1e-6, 'Stage-to-stage'
oplot, [1.5, 2.5], [1e-7, 1e-7], thick=4, linestyle=2
xyouts, 2.8, 1e-7, 'Bundle-to-bundle'


END
