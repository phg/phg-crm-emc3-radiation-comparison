;----------------------------------------------------------------------------
; Plot the free-free Gaunt factor as a function of wavelength and energy
; for a number of temperatures.
;
; The Gaunt factor is an evaluation of equation 17 from
; A. Burgess and H. P. Summers, Mon. Not. R. Astr. Soc., vol 226, p257 (1987)
;
; 13-02-2018
;----------------------------------------------------------------------------

FUNCTION wave_to_energy, lambda

; Helper function to convert a wavelength in Angstrom
; to a transition energy in eV.

return, 6.626070040D-34 * 2.99792458D+08 / (lambda * 1.D-10 * 1.6021766208D-19)

END


PRO plot_gff

; Define a wavelength range in Angstrom

lambda_min = 1.0
lambda_max = 6000.0
n_lambda   = 70
lambda     = adas_vector(low=lambda_min, high=lambda_max, num=n_lambda)

; A set of temperatures (eV)

te   = [50.0, 100.0, 1000.0, 10e3]
n_te = n_elements(te)

; Zeff for plasma

zeff = 1.5

; Calculate Maxwell averaged Gaunt factor. Note that both arguments
; to r8giiiav() depend on temperature so iterate over te() to
; avoid truncating the array.  

energy = wave_to_energy(lambda)
gam2   = zeff^2 * 13.6 / te

gff = fltarr(n_lambda, n_te)

for it = 0, n_te-1 do gff[*,it] =  r8giiiav(energy/te[it], gam2[it])


; Plot the results

!y.omargin = [1,1]

xmin = min(lambda, max=xmax)
ymin = min(gff, max=ymax)

plot_oi, [xmin, xmax], [ymin, ymax], /nodata, xstyle=8, $
         xtitle = 'Wavelength (Angstrom)', $
         ytitle = 'free-free Gaunt factor'

for it = 0, n_elements(te)-1 do begin
   oplot, lambda, gff[*, it], linestyle=it
   xyouts, 4e3, gff[n_lambda-1, it], 'Te = ' + string(te[it], format='(e8.2)')+'eV'
endfor

axis, xaxis=1, xrange=wave_to_energy(10.0^!x.crange), xtitle='Energy (eV)', xstyle=1, /xlog

END
