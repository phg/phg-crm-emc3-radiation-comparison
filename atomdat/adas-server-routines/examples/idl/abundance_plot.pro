;----------------------------------------------------------------------------
; Use an ADAS IDL routine to plot the the equilibrium balance of nitrogen
; in metastable resolved and stage-to-stage resolutions.
;----------------------------------------------------------------------------

PRO abundance_plot

; Central ADAS 96 GCR data as a function of temperature at a fixed density

itval = 100
te    = adas_vector(low=1, high=1e3, num=itval)
dens  = fltarr(itval) + 1e13

year  = 96
elem  = 'n'
uid   = 'adas'

run_adas405, uid=uid, elem=elem, year=year, te=te, dens=dens, $
             frac=frac, power=power

run_adas405, uid=uid, elem=elem, year=year, te=te, dens=dens, $
             frac=frac_res, power=power_res, /partial


n_stg = n_elements(frac.stage)
n_stg_res = n_elements(frac_res.stage)


; Plot as a function of temperature

!p.multi = [0, 3, 1]

xmin = min(te, max=xmax)

; abundances

plot_oo, [xmin, xmax], [0.001, 1.2], /nodata, ystyle=1, $
         xtitle = 'Te (eV)', $
         ytitle = 'fractional abundance', $
         title  = 'Stage-Stage resolution'

for j = 0, n_stg-1 do oplot, te, frac.ion[*,j], linestyle=j mod 2

plot_oo, [xmin, xmax], [0.0001, 1.2], /nodata, ystyle=1, $
         xtitle = 'Te (eV)', $
         ytitle = 'fractional abundance', $
         title  = 'Metastable resolution'

for j = 0, n_stg_res-1 do oplot, te, frac_res.ion[*,j], linestyle=j mod 2


; radiated power/ion - total and the continuum contribution

ymax = max(power.total)
ymin = 1.0e-5 * ymax

plot_oo, [xmin, xmax], [ymin, ymax], /nodata, $
         xtitle = 'Te (eV)', $
         ytitle = 'Power/ion (W cm!u3!n)', $
         title  = 'Cooling curve - totla power and continuum contribution'

oplot, te, power.total
oplot, te, power.prb, linestyle=2

!p.multi = 0

END
