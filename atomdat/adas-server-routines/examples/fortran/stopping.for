      program stopping

      implicit none
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN77 PROGRAM: stopping ********************
C
C  PURPOSE: Example code to read adf21 (BMS, BMP & BME) files and 
C           and print the effective coefficient.
C
C
C  PARAMETERS:
C
C  (I*4)   ndfiles    = Maximum number of files (10).
C  (I*4)   mxreq      = Maximum number of plasma parameters (1024).
C
C
C  VARIABLES:
C
C  (I*4)   itval      = Size of user requested TE/NE/ENERGY triplets.
C  (R*8)   te()       = Requested temperature range.
C  (R*8)   dens()     = Requested electron density range.
C  (R*8)   energy()   = Requested beam energy range.
C  (I*4)   numfiles   = Number of adf21 single element stopping files.
C  (C*132) dsfull()   = Full name of adf21 single element stopping files.
C                                      1st Dimension: numfiles
C  (R*8)   fraction(,)= Target ion fractions
C                                      1st Dimension: mxreq
C                                      2nd Dimension: ndfiles
C  (R*8)   coeff()    = Effective BMS/BMP/BME values.
C
C  ROUTINES:
C
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          CXBMS      ADAS      reads adf21/adf22 files and calculates
C                               a stopping/emission coefficient.
C
C
C  NOTES: - The parameters must be less than or equal to internal values
C           defined in cxbms.for (10 and 1024)
C         - Link with adas3xx and adaslib
C               f77 stopping.for -L/home/adas/lib -ladas3xx -ladaslib
C                                        or
C               f77 stopping.for -L/home/adas/lib64 -ladas3xx -ladaslib
C
C
C DATE    : 22-01-2013
C MODIFIED: Martin O'Mullane
C             - First version.
C
C DATE    : 05-06-2017
C MODIFIED: Martin O'Mullane
C             - Use the update hydrogen stopping coefficinets.
C
C-----------------------------------------------------------------------
      integer     ndfiles       , mxreq
C-----------------------------------------------------------------------
      parameter ( ndfiles = 10  , mxreq  = 1024  )
C-----------------------------------------------------------------------
      integer     numfiles      , itval
C-----------------------------------------------------------------------
      integer     i             , iunit
C-----------------------------------------------------------------------
      real*8      te(mxreq)     , dens(mxreq)    , energy(mxreq)  ,
     &            coeff(mxreq)
      real*8      fraction(mxreq,ndfiles)
C-----------------------------------------------------------------------
      character   dsfull(ndfiles)*132
C-----------------------------------------------------------------------

C Set conditions: 3 files - Hydrogen, Beryllium and Carbon

      numfiles  = 3

      dsfull(1) = '/home/adas/adas/adf21/bms10#h/bms10#h_h1.dat'
      dsfull(2) = '/home/adas/adas/adf21/bms97#h/bms97#h_be4.dat'
      dsfull(3) = '/home/adas/adas/adf21/bms97#h/bms97#h_c6.dat'

C Set conditions: 5 energy/te/density - units: eV/amu, eV, cm-3

      itval     = 5

      energy(1) =  10.0e3
      energy(2) =  30.0e3
      energy(3) =  50.0e3
      energy(4) =  70.0e3
      energy(5) = 100.0e3

      te(1) = 2.0e3
      te(2) = 2.0e3
      te(3) = 2.0e3
      te(4) = 2.0e3
      te(5) = 2.0e3

      dens(1) = 2.0e13
      dens(2) = 2.0e13
      dens(3) = 2.0e13
      dens(4) = 2.0e13
      dens(5) = 2.0e13

C Set conditions: Assume a constant fraction (ie impurity concentration) 
C at each energy/te/density point.

      do i = 1, itval
        fraction(i,1) = 0.95
        fraction(i,2) = 0.04
        fraction(i,3) = 0.01
      end do


C Call ADAS subroutine to assemble the effective stopping coefficient

      iunit = 67
      call cxbms(dsfull, numfiles, iunit, fraction,
     &           energy, dens, te, itval, mxreq, coeff)


C Write results to screen

      do i = 1, itval
        write(6, '(1p,4e15.4)')energy(i),te(i),dens(i),coeff(i)
      end do

C-----------------------------------------------------------------------
      end
