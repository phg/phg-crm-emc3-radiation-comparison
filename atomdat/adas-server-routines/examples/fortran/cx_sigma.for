      program cx_sigma

      implicit none
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN77 PROGRAM: cx_sigma ********************
C
C  PURPOSE: Example code to read adf01 and adf24 cross section files 
C           and to print the extracted cross sections. Also indicate
C           whether a value has been interpolated or extrapolated.
C
C
C
C  PARAMETERS:
C
C  (I*4)   mxreq      = Maximum number of plasma parameters (1024).
C
C
C  VARIABLES:
C
C  (C*80)  dsname     = Full qualified ADAS adf01/adf24 filename
C  (I*4)   ieval      = Number of energies.
C  (I*4)   ibsel      = Data block index of adf24 file
C  (R*8)   energy()   = Requested energies in eV.
C  (R*8)   coeff_01() = Total CX cross section from adf01 (cm2).
C  (R*8)   coeff_24() = Total CX cross section from adf24 (cm2).
C
C  ROUTINES:
C
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          xxdata_01  ADAS      reads all data from an adf01 file
C          xxdata_24  ADAS      reads all data from an adf24 file
C          e9spln     ADAS      interpolated adf24 onto user energies
C
C
C  NOTES:  - The adf01 and adf24 splining routines assume different
C            extrapolation behaviours. No extrapolation is preferred
C            for adf01 and the cross sections returned will be 
C            set to 0.0. For adf24 continuing the slope at
C            either end point is preferred.
C          - Link to central ADAS libraries, adas5xx and adaslib
C              gfortran cx_sigma.for -L/home/adas/lib -ladas3xx -ladas5xx -ladaslib -o cx_sigma.out
C
C
C DATE    : 06-08-2015
C MODIFIED: Martin O'Mullane
C             - First version.
C
C DATE    : 29-10-2018
C MODIFIED: Martin O'Mullane
C             - Output whether the cross-section have been interpolated.
C
C-----------------------------------------------------------------------
      integer     nstore        , nedim         , mxreq         , iunit
c-----------------------------------------------------------------------
      parameter( nstore =  250  , nedim  =  35  , mxreq  = 1024 ,
     &           iunit  =  12)
c-----------------------------------------------------------------------
      integer    ibsel          , nbsel         , ieval         , 
     &           iz0            , nsel          , lsel          ,
     &           iz             , iz1           , i
c-----------------------------------------------------------------------
      character  dsname*80      , esym*2
c-----------------------------------------------------------------------
      integer    isela(nstore)  , iea(nstore)
c-----------------------------------------------------------------------
      logical    lerng_01(mxreq)     , lerng_24(mxreq)
c-----------------------------------------------------------------------
      real*8     alpha(nstore)       , teea(nedim,nstore)  ,
     &           scx(nedim,nstore)
c-----------------------------------------------------------------------
      character  ctype(nstore)*2     , crecvr(nstore)*9    ,
     &           cfstat(nstore)*10   , cdonor(nstore)*9
C-----------------------------------------------------------------------
      real*8     energy(mxreq)       , 
     &           coeff_01(mxreq)     , coeff_24(mxreq)
C-----------------------------------------------------------------------


C Set conditions: for 5 energies (eV)

      ieval = 11

      energy(1)  =  1.0D-02
      energy(2)  =  1.0D1
      energy(3)  =  5.0D1
      energy(4)  =  1.0D2
      energy(5)  =  5.0D2
      energy(6)  =  1.0D3
      energy(7)  =  5.0D3
      energy(8)  =  10.0D3
      energy(9)  =  100.0D3
      energy(10) =  1000.0D3
      energy(11) =  1000.0D4

C Source of data (1) ADAS recommended set for CXRS analysis - adf01

      dsname = '/home/adas/adas/adf01/qcx#h0/qcx#h0_old#h1.dat'
      nsel   = 0
      lsel   = 0
      
      call cxqcx(dsname   , ieval , energy  ,
     &           nsel     , lsel  ,                
     &           coeff_01 , lerng_01
     &          )


C Source of data (2) adf24 data from ORNL

      dsname = '/home/adas/adas/adf24/scx#h0/scx#h0_ornl#h1.dat'
      ibsel  = 1

C Read in total cross section data from adf24 file

      open(unit=iunit,file=dsname)
      call xxdata_24( iunit  , dsname ,
     &                nstore , nedim  ,
     &                esym   , iz0    ,
     &                nbsel  , isela  ,
     &                iz     , iz1    ,
     &                cdonor , crecvr , cfstat , ctype ,
     &                alpha  ,
     &                iea    ,
     &                teea   , scx
     &              )
      close(iunit)

C  Interpolate onto user requested energies (work in eV).

      call e9spln( nedim          ,
     &             iea(ibsel)     ,
     &             ieval          ,
     &             teea(1,ibsel)  ,
     &             energy         ,
     &             scx(1,ibsel)   , coeff_24     ,
     &             lerng_24
     &           )


C Write results to screen
      
      write(6,*)'Energy (eV/amu)            cross section (cm2)'
      write(6,*)'                   ', 
     &          'adf01       interp  adf24       interp'
      do i = 1, ieval
        write(6, 1000)energy(i), coeff_01(i), lerng_01(i), 
     &                           coeff_24(i), lerng_24(i)
      end do

 1000 format(1p,e15.4, 2(e15.4,2x,L,2x))

C-----------------------------------------------------------------------
      end
