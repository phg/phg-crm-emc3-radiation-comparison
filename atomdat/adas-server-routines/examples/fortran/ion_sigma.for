      program ion_sigma

      implicit none
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN77 PROGRAM: ion_sigma *******************
C
C  PURPOSE: Example code to read adf02 cross section files and to print
C           the extrapolated cross section.
C
C
C  PARAMETERS:
C
C  (I*4)   mxreq      = Maximum number of plasma parameters (1024).
C
C
C  VARIABLES:
C
C  (C*132) dsname     = Full qualified ADAS adf02 filename
C  (I*4)   ieval      = Number of energies.
C  (I*4)   ibsel      = Data block index of adf02 file
C  (R*8)   energy()   = Requested energies in eV.
C  (R*8)   coeff()    = Ion impact cross section (cm2).
C
C  ROUTINES:
C
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          xxdata_02  ADAS      reads all data from an adf14 file
C          c2spln     ADAS      interpolated selected data on user
C                               requested temperatures and masses.
C
C
C  NOTES:  - Other variables and parameters required for reading the
C            adf14 dataset.
C          - Link to central ADAS libraries, adas5xx and adaslib
C              f77 ion_sigma.for -L/home/adas/lib -ladas3xx -ladaslib -o ion_sigma.out
C
C
C DATE    : 06-08-2015
C MODIFIED: Martin O'Mullane
C             - First version.
C
C-----------------------------------------------------------------------
      integer     nstore        , nedim         , mxreq         , iunit
c-----------------------------------------------------------------------
      parameter( nstore =  250  , nedim  =  35  , mxreq  = 1024 ,
     &           iunit  =  12)
c-----------------------------------------------------------------------
      integer    ibsel          , nbsel         , ieval         , i
c-----------------------------------------------------------------------
      character  dsname*132
c-----------------------------------------------------------------------
      integer    isela(nstore)  , iea(nstore)
c-----------------------------------------------------------------------
      logical    lerng(mxreq)
c-----------------------------------------------------------------------
      real*8     alpha(nstore)       , ethra(nstore)       ,
     &           teea(nedim,nstore)  ,
     &           amsa(nstore)        , ampa(nstore)        ,
     &           sia(nedim,nstore)
c-----------------------------------------------------------------------
      character  cprimy(nstore)*9    , csecdy(nstore)*9    ,
     &           ctype(nstore)*10
C-----------------------------------------------------------------------
      real*8     energy(mxreq)       , coeff(mxreq)
C-----------------------------------------------------------------------

C Source of data. ADAS recommended set and choose ionization of
C neutral hydrogen by fully-stripped helium. H0 + He2+ -> H+ + e + He2+

      dsname = '/home/adas/adas/adf02/sia#h/sia#h_j99#h.dat'
      ibsel  = 12

C Set conditions: for 5 energies (eV)

      ieval = 5

      energy(1) =  1.0D3
      energy(2) =  5.0D3
      energy(3) =  10.0D3
      energy(4) =  100.0D3
      energy(5) =  1000.0D3

C Read in data from adf14 file

      open(unit=iunit,file=dsname)
      call xxdata_02( iunit  , dsname ,
     &                nstore , nedim  ,
     &                nbsel  , isela  ,
     &                cprimy , csecdy , ctype,
     &                ampa   , amsa   , alpha  , ethra  ,
     &                iea    ,
     &                teea   , sia
     &              )
      close(iunit)

C  Interpolate onto user requested temperatures (work in eV).

      call c2spln( nedim             ,
     &             iea(ibsel)        ,
     &             ieval             ,
     &             teea(1,ibsel)     ,
     &             energy            ,
     &             sia(1,ibsel)      , coeff         ,
     &             lerng
     &           )

C Write results to screen

      do i = 1, ieval
        write(6, '(1p,3e15.4)')energy(i), coeff(i)
      end do

C-----------------------------------------------------------------------
      end
