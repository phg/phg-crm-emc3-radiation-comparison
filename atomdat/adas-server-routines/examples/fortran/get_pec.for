      program get_pec

      implicit none
C-----------------------------------------------------------------------
C
C  ********************* FORTRAN77 PROGRAM: get_pec ********************
C
C  PURPOSE: Example code to read adf15 files and print one photon
C           emissivity coefficient interpolated onto a user supplied
C           set of temperature and density pairs.
C
C
C  PARAMETERS:
C
C  (I*4)   mxreq      = Maximum number of plasma parameters (1024).
C
C
C  VARIABLES:
C
C  (C*132) dsname     = Full qualified ADAS adf15 filename
C  (I*4)   itval      = Size of user requested temperatures and densities.
C  (I*4)   ibsel      = Data block index of adf15 file
C  (R*8)   te()       = Temperatures in eV.
C  (R*8)   dens()     = Density in cm-3.
C
C  ROUTINES:
C
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          xxdata_15  ADAS      reads all data from an adf14 file
C          e3spln     ADAS      interpolated selected data on user
C                               requested temperatures and densities.
C
C
C  NOTES:  - Other variables and parameters required for reading the
C            adf15 dataset.
C          - Link to central ADAS libraries, adas5xx and adaslib
C              f77 -fno-second-underscore get_pec.for -L/home/adas/lib -ladas5xx -ladaslib -o get_pec.out
C
C
C DATE    : 18-08-2015
C MODIFIED: Martin O'Mullane
C             - First version.
C
C-----------------------------------------------------------------------
      integer   iunit   , mxreq
      integer   nstore  , ntdim   , nddim
      integer   ndptnl  , ndptn   , ndptnc , ndcnct
      integer   ndstack , ndcmt
C-----------------------------------------------------------------------
      parameter ( iunit  = 67  , mxreq  = 1024  )
      parameter ( nstore = 500 , ntdim   =  48  , nddim  =   30 )
      parameter ( ndptnl = 4   , ndptn   = 128  , ndptnc =  256 )
      parameter ( ndcnct = 100 , ndstack =  40  , ndcmt  = 4000 )
C-----------------------------------------------------------------------
      integer    iz0           , is             , is1         ,
     &           ibsel         , nbsel          ,
     &           itval         , i
      integer    nptnl         , ncnct          , ncptn_stack ,
     &           ncmt_stack
C-----------------------------------------------------------------------
      logical    lcmt          , lptn           , lres       , lsup
C-----------------------------------------------------------------------
      character  dsname*80     , esym*2
C-----------------------------------------------------------------------
      integer    isela(nstore)         ,
     &           ita(nstore)           , ida(nstore)
      integer    nptn(ndptnl)          , nptnc(ndptnl,ndptn)
      integer    iptnla(ndptnl)        , iptna(ndptnl,ndptn)
      integer    iptnca(ndptnl,ndptn,ndptnc)
      integer    icnctv(ndcnct)
      integer    isstgr(nstore)        , iszr(nstore)
      integer    ispbr(nstore)         , isppr(nstore)
C-----------------------------------------------------------------------
      real*8     teta(ntdim,nstore)    , teda(nddim,nstore)      ,
     &           wavel(nstore)         , pec(ntdim,nddim,nstore) ,
     &           pec_max(nstore)
C-----------------------------------------------------------------------
      logical    ltrng(ntdim)          , ldrng(ntdim)
C-----------------------------------------------------------------------
      character  cindm(nstore)*2       , cfile(nstore)*8         ,
     &           ctype(nstore)*8       , cwavel(nstore)*10
      character  cptn_stack(ndstack)*80,cmt_stack(ndcmt)*80
C-----------------------------------------------------------------------
      real*8     te(mxreq)             , dens(mxreq)             ,
     &           coeff(mxreq)
C-----------------------------------------------------------------------

C Source of data - 6680A line in He I.

      dsname = '/home/adas/adas/adf15/pec96#he/pec96#he_pju#he0.dat'
      ibsel  = 13

C Set conditions: for 7 temperature and density pairs. Set density to
C                 a constant but it could vary if required.

      itval = 7

      te(1) =   1.0D0
      te(2) =   3.0D0
      te(3) =   7.0D0
      te(4) =  10.0D0
      te(5) =  30.0D0
      te(6) =  50.0D0
      te(7) = 100.0D0

      dens(1) =  1.0D13
      dens(2) =  1.0D13
      dens(3) =  1.0D13
      dens(4) =  1.0D13
      dens(5) =  1.0D13
      dens(6) =  1.0D13
      dens(7) =  1.0D13


C Read in data from adf15 file

      open(unit=iunit,file=dsname)

      call xxdata_15( iunit  , dsname ,
     &                nstore , ntdim  , nddim  ,
     &                ndptnl , ndptn  , ndptnc , ndcnct ,
     &                ndstack, ndcmt ,
     &                iz0    , is     , is1    , esym   ,
     &                nptnl  , nptn   , nptnc  ,
     &                iptnla , iptna  , iptnca ,
     &                ncnct  , icnctv ,
     &                ncptn_stack     , cptn_stack      ,
     &                lres   , lptn   , lcmt   , lsup   ,
     &                nbsel  , isela  ,
     &                cwavel , cfile  , ctype  , cindm  ,
     &                wavel  , ispbr  , isppr  , isstgr , iszr  ,
     &                ita    , ida    ,
     &                teta   , teda   ,
     &                pec    , pec_max,
     &                ncmt_stack      , cmt_stack
     &              )

      close(iunit)

C  Interpolate onto user requested temperature/density pairs.

      call e3spln( ntdim             , nddim         ,
     &             ita(ibsel)        , ida(ibsel)    ,
     &             itval             ,
     &             teta(1,ibsel)     , teda(1,ibsel) ,
     &             te                , dens          ,
     &             pec(1,1,ibsel)    , coeff         ,
     &             ltrng             , ldrng
     &           )

C Write results to screen

      do i = 1, itval
        write(6, '(1p,3e15.4)')te(i),dens(i),coeff(i)
      end do

C-----------------------------------------------------------------------
      end
