      program tcx

      implicit none
C-----------------------------------------------------------------------
C
C  *********************** FORTRAN77 PROGRAM: tcx **********************
C
C  PURPOSE: Example code to read adf14 files and print the thermal 
C           CX rate coefficient.
C
C
C  PARAMETERS:
C
C  (I*4)   mxreq      = Maximum number of plasma parameters (1024).
C
C
C  VARIABLES:
C
C  (C*132) dsname     = Full qualified ADAS adf14 filename
C  (I*4)   itval      = Size of user requested temperatures.
C  (I*4)   ibsel      = Data block index of adf14 file
C  (R*8)   te_don()   = Donor temperatures in eV.
C  (R*8)   te_rec()   = Receiver temperatures in eV.
C  (R*8)   dmass      = Donor mass.
C  (R*8)   rmass      = Receiver mass.
C  (R*8)   coeff()    = Thermal CX coefficient.
C
C  ROUTINES:
C
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          xxdata_14  ADAS      reads all data from an adf14 file
C          e5spln     ADAS      interpolated selected data on user
C                               requested temperatures and masses.
C
C
C  NOTES:  - Other variables and parameters required for reading the
C            adf14 dataset.
C          - Link to central ADAS libraries, adas5xx and adaslib
C              f77 tcx.for -L/home/adas/lib -ladas5xx -ladaslib -o tcx.out
C
C
C DATE    : 30-06-2015
C MODIFIED: Martin O'Mullane
C             - First version.
C
C-----------------------------------------------------------------------
      integer     nstore        , ntrdim        , ntddim          , 
     &            ndtin         , mxreq         , iunit
c-----------------------------------------------------------------------
      parameter( nstore  =  150 , ntrdim = 24   , ntddim =  24    ,
     & 		 ndtin   = 24   , mxreq  = 1024 , iunit  =  12)
c-----------------------------------------------------------------------
      integer    ibsel          , nbsel         , itval           ,
     &           i
c-----------------------------------------------------------------------
      real*8     rmass          , dmass
c-----------------------------------------------------------------------
      character  dsname*132
c-----------------------------------------------------------------------
      integer    isela(nstore)   , irz0(nstore)         ,
     &           irz1(nstore)    , idz0(nstore)         ,
     &           itra(nstore)    , itda(nstore)
c-----------------------------------------------------------------------
      logical    lequa(nstore)                          ,
     &           ltrrng(ndtin)   , ltdrng(ndtin)
c-----------------------------------------------------------------------
      real*8     amsra(nstore)               , amsda(nstore)
      real*8     tfra(ntrdim,nstore)         , tfda(ntddim,nstore)     ,
     &           qfteqa(ntrdim,nstore)       ,
     &           qftcxa(ntddim,ntrdim,nstore)
c-----------------------------------------------------------------------
      character  cdonor(nstore)*9 , crecvr(nstore)*9    ,
     &           cfstat(nstore)*10
C-----------------------------------------------------------------------
      real*8     te_don(mxreq)    , te_rec(mxreq)       , coeff(mxreq)
C-----------------------------------------------------------------------

C Source of data

      dsname = '/home/adas/adas/adf14/tcx#h0/tcx#h0_h.dat'
      ibsel  = 1

C Set conditions: for 5 temperatures, H donor at 10.0eV and D receiver

      itval = 5

      te_rec(1) =  10.0D0
      te_rec(2) =  30.0D0
      te_rec(3) =  70.0D0
      te_rec(4) = 100.0D0
      te_rec(5) = 200.0D0
      
      te_don(1) =  10.0D0
      te_don(2) =  10.0D0
      te_don(3) =  10.0D0
      te_don(4) =  10.0D0
      te_don(5) =  10.0D0
      
      rmass = 2.0D0
      dmass = 1.0D0

C Read in data from adf14 file
      
      open(unit=iunit,file=dsname)
      call xxdata_14( iunit  , dsname ,
     &                nstore , ntrdim , ntddim ,
     &                nbsel  , isela  ,
     &                irz0   , irz1   , idz0   ,
     &                lequa  ,
     &                cdonor , crecvr , cfstat ,
     &                amsra  , amsda  ,
     &                itra   , itda   ,
     &                tfra   , tfda   ,
     &                qfteqa , qftcxa
     &              )
      close(iunit)

C  Interpolate onto user requested temperatures (work in eV).

      call e5spln( ntrdim            , ntddim            ,
     &             itra(ibsel)       , itda(ibsel)       ,
     &             itval             ,
     &             amsra(ibsel)      , amsda(ibsel)      ,
     &             rmass             , dmass             ,
     &             tfra(1,ibsel)     , tfda(1,ibsel)     ,
     &             te_rec            , te_don            ,
     &             qftcxa(1,1,ibsel) , coeff             ,
     &             ltrrng            , ltdrng
     &           )

C Write results to screen

      do i = 1, itval
        write(6, '(1p,3e15.4)')te_don(i),te_rec(i),coeff(i)
      end do

C-----------------------------------------------------------------------
      end
