      program ion_rate

      implicit none
C-----------------------------------------------------------------------
C
C  ******************** FORTRAN77 PROGRAM: ion_rate ********************
C
C  PURPOSE: Example code to read adf07 files and print the ionization
C           rate interpolated onto a user supplied set of temperatures.
C
C
C  PARAMETERS:
C
C  (I*4)   mxreq      = Dimension of user te vector (1024).
C  (I*4)   nstore     = Maximum number of blocks in adf07 file.
C  (I*4)   ntdim      = Maximum number of temperatures in adf07 file.
C  (I*4)   maxte      = Maximum number of temperatures permitted in
C                       the adf07 splining routine.
C
C
C  VARIABLES:
C
C  (C*132) dsname     = Full qualified ADAS adf07 filename
C  (I*4)   itval      = Size of user requested temperatures and densities.
C  (I*4)   ibsel      = Data block index of adf07 file
C  (R*8)   te()       = Temperatures in eV.
C
C  ROUTINES:
C
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          xxdata_07  ADAS      reads all data from an adf14 file
C          e2spln     ADAS      interpolated selected data on user
C                               requested temperatures.
C          i4numlines ADAS      number of blocks within a 1D vector
C
C
C  NOTES:  - Other variables and parameters required for reading the
C            adf07 dataset.
C          - Link to central ADAS libraries, adas5xx and adaslib
C              f77 -fno-second-underscore ion_rate.for -L/home/adas/lib -ladas5xx -ladaslib -o ion_rate.out
C
C
C DATE    : 08-08-2018
C MODIFIED: Martin O'Mullane
C             - First version.
C
C DATE    : 12-08-2018
C MODIFIED: Martin O'Mullane
C             - e2spln has an internal limit of 100 point so give
C               an example method of how to calculate more.
C
C DATE    : 20-08-2018
C MODIFIED: Martin O'Mullane
C             - L is not a valid format control for g77. Replace
C               with L5 which is acceptable to all compilers.
C
C-----------------------------------------------------------------------
      integer   iunit   , mxreq
      integer   nstore  , ntdim  , maxte
C-----------------------------------------------------------------------
      parameter ( iunit  = 67    , mxreq  = 1024 )
      parameter ( nstore = 500   , ntdim  =   35 , maxte = 100)
C-----------------------------------------------------------------------
      integer    iz0             , ibsel         , nbsel         ,
     &           itval           , i             , j             , k
      integer    ist             , ifn
      integer    i4numlines
c-----------------------------------------------------------------------
      real*8     te_low          , te_high
c-----------------------------------------------------------------------
      character  dsname*132      , esym*2
c-----------------------------------------------------------------------
      integer    isela(nstore)             , ita(nstore)         ,
     &           izout(nstore)             , iz1out(nstore)
c-----------------------------------------------------------------------
      real*8     teta(ntdim,nstore)        , szd(ntdim,nstore)   ,
     &           bwnout(nstore)
      real*8     te(mxreq)                 , coeff(mxreq)
      real*8     te_tmp(100)               , coeff_tmp(100)
c-----------------------------------------------------------------------
      logical    ltrng(mxreq)
c-----------------------------------------------------------------------
      character  cicode(nstore)*2          , cfcode(nstore)*2     ,
     &           ciion(nstore)*5           , cfion(nstore)*5
c-----------------------------------------------------------------------
      external   i4numlines
c-----------------------------------------------------------------------

C Source of data - Fe12+ from Dere, Astron. Astrophys., 466 (2007), p771.

      dsname = '/home/adas/adas/adf07/dere/dere07#fe.dat'
      ibsel  = 13

C Request 250 temperatures between 10 and 300eV

      itval   = 250
      te_low  = 10.0D0
      te_high = 300.0D0

      do i = 1, itval
         te(i) = exp(log(te_low) + float(i-1) / float(itval-1) *
     &               (log(te_high) - log(te_low)))
      end do


C Read in data from adf07 file

      open(unit=iunit,file=dsname)

      call xxdata_07( iunit  , dsname ,
     &                nstore , ntdim  ,
     &                esym   , iz0    ,
     &                nbsel  , isela  ,
     &                izout  , iz1out ,
     &                cicode , cfcode , ciion , cfion ,
     &                bwnout ,
     &                ita    ,
     &                teta   , szd
     &              )

      close(iunit)

C  Interpolate onto user requested temperatures, noting that e2spln
C  has an internal limit of maxte elements (100).

      do i = 1, i4numlines(itval, maxte)

         ist = 1 + (i-1)*maxte
         ifn = min(i*maxte, itval)

         k=1
         do j = ist, ifn
            te_tmp(k) = te(j)
            k = k + 1
         end do

         call e2spln( ita(ibsel)        , ifn-ist+1     ,
     &                bwnout(ibsel)     ,
     &                teta(1,ibsel)     , te_tmp        ,
     &                szd(1,ibsel)      , coeff_tmp     ,
     &                ltrng
     &              )

         do j = 1, ifn-ist+1
            coeff(ist+j-1) = coeff_tmp(j)
         end do

      end do

C Write results to screen

      write(6,*)'       Te (eV)   Rate (cm3/s)   Interpolated'
      do i = 1, itval
         write(6, '(1p,2e15.4,3x,L5)')te(i), coeff(i), ltrng(i)
      end do

C-----------------------------------------------------------------------
      end
