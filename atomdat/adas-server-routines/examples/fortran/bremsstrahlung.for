       program bremsstrahlung

       implicit none
C-----------------------------------------------------------------------
C
C  ************** FORTRAN77 PROGRAM: bremsstrahlung ********************
C
C  PURPOSE:   To evaluate Bremsstrahlung emission for a specified
C             wavelength range and single electron temperature.
C
C  ROUTINES:
C          ROUTINE    SOURCE    BRIEF DESCRIPTION
C          ------------------------------------------------------------
C          continuo   ADAS      for an given wavelength generate
C                               radiative reconbination and
C                               bremsstrahlung emissivity.
C
C  NOTES : - Link to central ADAS libraries, adas3xx and adaslib
C               f77 bremsstrahlung.for -L/home/adas/lib \
C                       -ladas3xx -ladaslib -o bremsstrahlung.x
C
C  DATE    : 30-03-2006
C  MODIFIED: Allan Whiteford
C             - First version.
C
C  DATE    : 23-06-2017
C  MODIFIED: Martin O'Mullane
C             - Altered to match style of examples/fortran codes.
C
C-----------------------------------------------------------------------

C-----------------------------------------------------------------------
       integer   iz0  , iz1       , n_wave    , i
       real*8    tev  , wave_min  , wave_max  ,
     &           wave , contff    , contin
C-----------------------------------------------------------------------

C consider fully stripped carbon, z0=6, z1=6

       iz0 = 6
       iz1 = 6

C specify a temperature of 3kev and request the continuum emission at
C 15 wavelengths between 1500A and 7000A

       tev      = 3000.0D0
       wave_min = 1500.0D0
       wave_max = 7000.0D0
       n_wave   = 15

C call ADAS routine and write the results to screen
C      - free-free (brmesstrahlung) emissivity (ph cm3 s-1 a-1)
C      - total (bremsstrahlung + radiative recombination)

       do i = 1, n_wave

         wave = wave_min + (i - 1) * (wave_max-wave_min) / (n_wave - 1)

         call continuo(wave   , tev    , iz0  , iz1  ,
     &                 contff , contin
     &                )


         write(6, '(f7.2,1p,2e15.4)')wave, contff, contin

       end do

       end
