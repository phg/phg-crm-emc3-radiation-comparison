#----------------------------------------------------------------------------
# Use of ADAS python routine to print an effective beam stopping coefficient
#----------------------------------------------------------------------------

import adas as adas
import numpy as np
import os
import matplotlib.pyplot as plt

# 4% Be and 1% C impurity content

root = os.getenv('ADASCENT')
files = [root + '/adf21/bms10#h/bms10#h_h1.dat',
         root + '/adf21/bms97#h/bms97#h_be4.dat',
         root + '/adf21/bms97#h/bms97#h_c6.dat']
fraction = np.asarray([0.95, 0.04, 0.01])
z0       = np.asarray([1.0, 4.0, 6.0])

# plasma conditions

ieval   = 32
energy  = adas.adas_vector(low=20, high=150, num=ieval)*1000.0
te_pl   = np.zeros(ieval) + 2.0e3
dens_pl = np.zeros(ieval) + 1e13

# total and individual contributions


bms_total = adas.read_adf21(files=files,fraction=fraction,energy=energy,te=te_pl,dens=dens_pl)

wt = (fraction * z0).sum()

bms_h  = (fraction[0] * z0[0] / wt) * adas.read_adf21(files=files[0],fraction=[1.0],energy=energy,te=te_pl,dens=dens_pl)
bms_be = (fraction[1] * z0[1] / wt) * adas.read_adf21(files=files[1],fraction=[1.0],energy=energy,te=te_pl,dens=dens_pl)
bms_c  = (fraction[2] * z0[2] / wt) * adas.read_adf21(files=files[2],fraction=[1.0],energy=energy,te=te_pl,dens=dens_pl)

# Output resuls

print('Total (effective) stopping coefficinet : ', bms_total)

fig = plt.figure(figsize=(26/2.54, 18/2.54))

box = fig.add_subplot(1,1,1)

p1  = box.plot(energy/1e3, bms_total, label='Total (effective) stopping', color='black')
p2  = box.plot(energy/1e3, bms_h, label='95% Hydrogen', color='red')
p3  = box.plot(energy/1e3, bms_be, label=' 4% Beryllium', color='green')
p4  = box.plot(energy/1e3, bms_c, label=' 1% Carbon', color='magenta')

p6  = box.set_xlabel('Beam energy (keV/amu)')
p7  = box.set_ylabel('stopping coefficient (cm$^3$ s$^{-1}$)')

p8  = box.legend(loc='upper right')

plt.draw()
plt.show()
