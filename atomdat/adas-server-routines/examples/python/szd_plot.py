#---------------------------------------------------------------------------
# Use of ADAS python routine to plot adf07 ionization rate.
#
#---------------------------------------------------------------------------

import os
import matplotlib.pyplot as plt
import adas as adas

# Interpolate data from adf07

file  = os.getenv('ADASCENT') + '/adf07/szd02#li/szd02#li_li2.dat'
itval = 203
te    = adas.adas_vector(low=1, high=1e3, num=itval)

coeff, info = adas.read_adf07(file=file, block=5, te=te)

title = info['i_ion'] + ' $\longrightarrow$ ' + info['f_ion']

#----------------
# Plot the figure
#----------------

fig = plt.figure(figsize=(26/2.54, 18/2.54))

box = fig.add_subplot(1,1,1)

p1  = box.plot(te, coeff, label='Ionization rate')

p3  = box.set_xscale('log', nonposx='clip')
p4  = box.set_yscale('log', nonposy='clip')
p5  = box.set_ylim([1e-12, 1e-7])

p7  = box.set_xlabel('Te (eV)')
p6  = box.set_ylabel('Ionization rate (cm$^3$ s$^{-1}$)')
p8  = box.set_title(title)

p9  = box.legend(loc='lower right')


#---------------------
# Put figure on screen
#---------------------

plt.draw()
plt.show()
