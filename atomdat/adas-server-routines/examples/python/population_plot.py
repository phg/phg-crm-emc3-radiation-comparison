#---------------------------------------------------------------------------
# Use of ADAS python routine to plot the population dependence of excited
# levels.
#
# 27-06-2017: this works with v2.7 and v3.5
#---------------------------------------------------------------------------

import os
import matplotlib.pyplot as plt

import numpy as np
import adas as adas

# C4+ as a function of Te

file  = os.getenv('ADASCENT') + '/adf04/adas#6/mom97_ls#c4.dat'

te   = [30.0, 100.0, 300.0]
dens = 1.0e13

meta_list = [1]

res = adas.run_adas208(adf04=file, te=te, dens=dens, meta=meta_list)

pop   = res['dependence']
nord  = res['numlev']
index = np.arange(1, nord+1)

#----------------
# Plot the figure
#----------------

fig = plt.figure(figsize=(26/2.54, 18/2.54))

box = fig.add_subplot(1,1,1)

p1  = box.plot(index, pop[:,0,0,0], label='Te = 30eV')
p2  = box.plot(index, pop[:,0,1,0], label='Te = 100eV')

p4  = box.set_yscale('log', nonposy='clip')
p5  = box.set_ylim([1e-15, 1.0])

p7  = box.set_xlabel('Excited level index')
p6  = box.set_ylabel('relative population (Ni/Ng)')
p8  = box.set_title('C${}^{4+}$')

p9  = box.legend(loc='upper right')

#---------------------
# Put figure on screen
#---------------------

plt.draw()
plt.show()
