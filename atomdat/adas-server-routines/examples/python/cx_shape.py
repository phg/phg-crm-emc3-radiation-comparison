#---------------------------------------------------------------------------
# Use the ADAS python routine, run_adas308, to calculate the components
# in the CVI n=8-7 charge exchange transition. Generate the line shape
# for a number of ion temperatures with the ADAS c5dplr routine.
#
# 08-04-2019
#---------------------------------------------------------------------------

import os
import matplotlib.pyplot as plt
import numpy as np
import adas as adas

# State selective CX data

adf01  = os.getenv('ADASCENT') + '/adf01/qcx#h0/qcx#h0_old#c6.dat'

# Plasma conditions - fixed plasma

dens   = 1e13
dion   = 1e13
energy = [6e5]        # This and frac can be an array
frac   = [1]
n1req  = 8
n2req  = 7
mass   = 12.0


# Parameters for Doppler broadening

ndpix  = 1000
npix   = ndpix
ndcomp = 20

wvmin  = 5285.0
wvmax  = 5295.0
dwave  = np.asarray(np.zeros(ndpix), float)
wave   = adas.adas_vector(low=wvmin, high=wvmax, num=ndpix, linear=True)
dlam   = wave[1]-wave[0]


# Choose a number of temperatures - let Te=Tion here

tev = [50.0, 100.0, 500.0, 1000.0, 3000.0]
nte = len(tev)

em_dp = np.asarray(np.zeros((ndpix, nte)), float)

for j in range(nte):

    # calculate components

    data = adas.run_adas308(adf01=adf01, mass=mass, te=tev[j], tion=tev[j],
                            dens=dens, dion=dion, frac=frac, energy=energy,
                            n1req=n1req, n2req=n2req)

    # Doppler broaden to line

    ncomp  = data['ncomp']
    wvcomp = data['wave']
    emcomp = data['emiss']

    emiss = adas.c5dplr(npix=npix, wvmin=wvmin, wvmax=wvmax,
                        wvcomp=wvcomp, emcomp=emcomp,
                        tev=tev[j], amss=mass)

    em_dp[:,j] = emiss / dlam


#----------------
# Plot the figure
#----------------

title = 'CVI 8-7 Doppler broadened CX emission'

fig = plt.figure(figsize=(26/2.54, 18/2.54))

box = fig.add_subplot(1,1,1)

for j in range(nte):
    p1  = box.plot(wave, em_dp[:,j], label="{:6.1f}".format(tev[j])+'eV')

p5  = box.set_ylim([em_dp.min(), 1.05*em_dp.max()])

p7  = box.set_xlabel('Wavelength ($\AA$)')
p6  = box.set_ylabel('Column emissivity (ph cm$^{-2}$ s$^{-1}$ $\AA^{-1}$)')
p8  = box.set_title(title)

p9  = box.legend(loc='upper left')


#---------------------
# Put figure on screen
#---------------------

plt.draw()
plt.show()
