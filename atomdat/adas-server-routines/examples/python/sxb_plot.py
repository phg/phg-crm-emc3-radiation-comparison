#---------------------------------------------------------------------------
# Use of ADAS python routine to plot ionization/photon coefficient.
#---------------------------------------------------------------------------

import os
import adas as adas
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.ticker as mticker

# Formatter function to label log-space axes

def log_tick_formatter(val, pos=None):
    return "{:.1e}".format(10**val) + '  '


# Consider the second S/XB coefficient from the adf13 file of Si2+

file  = os.getenv('ADASCENT') + '/adf13/sxb96#si/sxb96#si_pju#si2.dat'
block = 2

# Interpolate 2D data from the adf13 file -- set all=True for this

te   = adas.adas_vector(low=1, high=1e3, num=100)
dens = adas.adas_vector(low=1e10, high=1e15, num=50)

sxb, info = adas.read_adf13(file=file, block=block, te=te, dens=dens, all=True)


# Examine SXB as function of Te for a fixed density

te_1d   = adas.adas_vector(low=2, high=120, num=20)
dens_1d = np.zeros(20) + 1e13

sxb_1d, info = adas.read_adf13(file=file, block=block, te=te_1d, dens=dens_1d)


# Get the coefficient as archived in the file

all = adas.xxdata_13(file=file)
te_adf13   = all['te'][:,block-1]
dens_adf13 = all['dens'][:,block-1]
sxb_adf13  = all['sxb'][:,:,block-1]



#----------------
# Plot the figure
#----------------

# Calculate log10 of quantities and make a grid for te and dens (x,y)
# with z=log10(sxb), which need to be transposed.

x, y = np.meshgrid(np.log10(te), np.log10(dens))
z    = np.log10(sxb).transpose()

x_adf13, y_adf13 = np.meshgrid(np.log10(te_adf13), np.log10(dens_adf13))
z_adf13 = np.log10(sxb_adf13).transpose()


# Extablish the 3D plot

fig = plt.figure(figsize=(1.5*26/2.54, 1.5*18/2.54))
ax = fig.add_subplot(1, 1, 1, projection='3d')

plt.title('Si$^{2+}$ ($3s^2\, {}^1S - 3s^1\,4p^1\, {}^3D$) at 566$\AA$')

# Plot the 2D interpolated SXB surface

ax.plot_surface(x, y, z, rstride=4, cstride=4, zorder=0)

# Overplot sxb(te) at a constant density

ax.plot(np.log10(te_1d), np.log10(dens_1d), np.log10(sxb_1d), color='red', zorder=2,
        label='for Ne=10$^{13}$ cm$^{-3}$')

# Overplot the adf13 file values to see accuracy of the interpolation

ax.scatter(x_adf13, y_adf13, z_adf13, c = 'y', marker='o', zorder=1, label='adf13 values')

# Label the axes

ax.xaxis.set_major_formatter(mticker.FuncFormatter(log_tick_formatter))
ax.yaxis.set_major_formatter(mticker.FuncFormatter(log_tick_formatter))
ax.zaxis.set_major_formatter(mticker.FuncFormatter(log_tick_formatter))

ax.set_xlabel('\n\n\nTe (eV)')
ax.set_ylabel('\n\n\nNe (cm$^{-3}$)')
ax.set_zlabel('\n\n\nS/XB - ionizations per photon')

ax.legend()

# Change the view and realize the plot

ax.view_init(elev=25.0, azim=-120.0)

plt.show()
