#----------------------------------------------------------------------------
# Use the non-interactive, run_adas416, to bundle GCR neon adf11 files.
# Compare the ionization balance using the bundled data to the standard
# stage-to-stage results from run_adas405 and the diagnostic output from
# run_adas416.
#
# Plot the stage-to-stage and bundle-to-bundle ionization and recombination
# rates.
#
# This demo program will create 7 files in the directory from where
# it is run.
#
# 10-04-2019
#----------------------------------------------------------------------------

import os
import matplotlib.pyplot as plt
import numpy as np
import adas as adas

# Generate the bundled adf11 files and diagnostic fractional abundances

script = os.getenv('ADASHOME') + '/adas/scripts416/partition_02_ne96.dat'

fr_p, fr_c = adas.run_adas416(file=script)


# For user selected parameters, generate an ionization balance
# from stage-stage data

itval = 100
te    = adas.adas_vector(low=1, high=1e3, num=itval)
dens  = np.zeros(itval) + 1e13

frac_96, _ = adas.run_adas405(elem='ne', uid='adas', year=96, te=te, dens=dens)

# and from the just produced bundled data

files = { 'acd' : 'acd96_ne_02.dat',
          'scd' : 'scd96_ne_02.dat'  }

frac_bdl, _ = adas.run_adas405(elem='ne', uid='adas', year=96, 
                               te=te, dens=dens, files=files)


n_stg = len(frac_96['stage'])
n_stg_bdl = len(frac_bdl['stage'])


# Interpolate the ionization and recombination rates of the stages and bundles.

itval  = 20
te_c   = adas.adas_vector(low=1, high=1e3, num=itval)
dens_c = np.zeros(itval) + 1e13

scd_96  = np.zeros((itval, n_stg-1))
scd_bdl = np.zeros((itval, n_stg_bdl-1))
acd_96  = np.zeros((itval, n_stg-1))
acd_bdl = np.zeros((itval, n_stg_bdl-1))

for j in range(n_stg-1):

    is1 = j + 1

    file = os.getenv('ADASHOME') + '/adas/adf11/scd96/scd96_ne.dat'
    scd_96[:,j] = adas.read_adf11(file=file, is1=is1, 
                                  te=te_c, dens=dens_c, adf11type='scd')

    file = os.getenv('ADASHOME') + '/adas/adf11/acd96/acd96_ne.dat'
    acd_96[:,j] = adas.read_adf11(file=file, is1=is1, 
                                  te=te_c, dens=dens_c, adf11type='acd')


for j in range(n_stg_bdl-1):

    is1 = j + 1

    file = 'scd96_ne_02.dat'
    scd_bdl[:,j] = adas.read_adf11(file=file, is1=is1, 
                                   te=te_c, dens=dens_c, adf11type='scd')

    file = 'acd96_ne_02.dat'
    acd_bdl[:,j] = adas.read_adf11(file=file, is1=is1, 
                                   te=te_c, dens=dens_c, adf11type='acd')



# Plot to compare the adas405 ionization balance and the diagnostic 
# abundances (calculated by adas416 on the Te/density grid of the 
# un-bundled adf11 files). Choose the closest density in the adf11 
# file to the one used for the run_adas405 calculation.


fig = plt.figure(figsize=(26/2.54, 18/2.54))

# fractional abundance

box = fig.add_subplot(1,2,1)

xmin = te.min()
xmax = te.max()
ymin = 0.01
ymax = 1.2

id = adas.i4indf(fr_p['dens'], dens[0])

for j in range(n_stg):
    if j == 0:
        p0 = box.plot(te, frac_96['ion'][:,j], color='blue', label='Stage resolved')
        p1 = box.plot(fr_p['te'], fr_p['ion'][:,id,j], color='red', marker="o", linestyle='None')
    else:
        p0 = box.plot(te, frac_96['ion'][:,j], color='blue')
        p1 = box.plot(fr_p['te'], fr_p['ion'][:,id,j], color='red', marker="o", linestyle='None')
    
for j in range(n_stg_bdl):
    if j == 0:
        p0 = box.plot(te, frac_bdl['ion'][:,j], color='blue', linewidth=4.0, label='Bundle resolved')
        p1 = box.plot(fr_c['te'], fr_c['ion'][:,id,j], color='red', marker="o", label='adas416', linestyle='None')
    else:
        p0 = box.plot(te, frac_bdl['ion'][:,j], color='blue', linewidth=4.0)
        p1 = box.plot(fr_c['te'], fr_c['ion'][:,id,j], color='red', marker="o", linestyle='None')

p2 = box.set_xscale('log', nonposx='clip')
p3 = box.set_yscale('log', nonposy='clip')
p4 = box.set_xlim([xmin, xmax])
p5 = box.set_ylim([ymin, ymax])
p6 = box.set_xlabel('Te (eV)')
p7 = box.set_ylabel('Fractional abundance')
p8 = box.legend(loc='best')


# rate coefficients

box = fig.add_subplot(1,2,2)

xmin = te_c.min()
xmax = te_c.max()
ymin = 1.0e-20
ymax = 1.0e-6

for j in range(n_stg-1):
    if j == 0:
        p0 = box.plot(te_c, scd_96[:,j], color='blue', label='Stage-to-stage')
    else:
        p0 = box.plot(te_c, scd_96[:,j], color='blue')
    p1 = box.plot(te_c, acd_96[:,j], color='blue')

for j in range(n_stg_bdl-1):
    if j == 0:
        p2 = box.plot(te_c, scd_bdl[:,j], ':', color='red', linewidth=4.0, label='Bundle-to-bundle')
    else:
        p2 = box.plot(te_c, scd_bdl[:,j], ':', color='red', linewidth=4.0)
    p3 = box.plot(te_c, acd_bdl[:,j], ':', color='red')

p4 = box.set_xscale('log', nonposx='clip')
p5 = box.set_yscale('log', nonposy='clip')
p6 = box.set_xlim([xmin, xmax])
p7 = box.set_ylim([ymin, ymax])
p8 = box.set_xlabel('Te (eV)')
p9 = box.set_ylabel('Rate coefficient (cm$^3$ s${}^{-1}$)')
p10 = box.legend(loc='upper left')


# Put figure on screen

plt.subplots_adjust(bottom=0.1, top=0.9, wspace=0.3)
plt.draw()
plt.show()

