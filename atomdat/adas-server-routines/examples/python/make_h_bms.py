#---------------------------------------------------------------------------
# Generate the central ADAS beam stopping coefficient adf21 file for
# hydrogen using the offline run_adas310 code.
#
# This routine will produce 2 files in the directory where it is run.
#
# 17-04-2019
#---------------------------------------------------------------------------

import adas as adas

# Get beam energies, temperatures, density and reference values
# from the central ADAS adf21 file

adf21 = '/home/adas/adas/adf21/bms10#h/bms10#h_h1.dat'
all   = adas.xxdata_21(file=adf21)


# Minimal set of inputs for adas310. See documentation for all options.

adf01 = '/home/adas/adas/adf01/qcx#h0/qcx#h0_e2p#h1.dat'
adf18 = '/home/adas/adas/adf18/p310_a17/bndlen_exp#h0.dat'

dens  = all['tdens']
denp  = dens # for hydrogen
te    = all['ttemp']
tp    = te
bmeng = all['be']

tref  = all['ttref']
dref  = all['tdref']
bref  = all['beref']

izimp = [1]
amimp = [2.0]
frimp = [1.0]

# The only output from adas310 is a number of files -- ask for adf21 and adf26
 
files = {'adf21'   : 'bms_test.dat',
         'adf26'   : 'pop_test.dat'}

# Delta_n for ion impact cross sections is the only difference
# from the default values. For H we used 4 rather than 2 for
# other species. 

nionip = 4

res = adas.run_adas310(adf01=adf01, adf18=adf18,
                       izimp=izimp, amimp=amimp, frimp=frimp,
                       te=te, tp=tp, dens=dens, denp=denp, bmeng=bmeng,
                       tref=tref, dref=dref, bref=bref,
                       nionip=nionip,
                       files=files)
