#---------------------------------------------------------------------------
# Use of ADAS python routine to compare electron and ion impact cross
# sections for a transition in neutral lithium.
#---------------------------------------------------------------------------

import adas as adas
import numpy as np
import os
import matplotlib.pyplot as plt

# Electron impact cross sections from a type 1 adf04 file
# Note that the xxdata_04 routine was developed for type 3 file so
# x=energy/delta_e is returned in 'te' and the collision strength
# in 'gamma'.

file     = os.getenv('ADASCENT') + '/adf04/lilike/lilike_aug99#li0_t1.dat'
all_elec = adas.xxdata_04(file=file)

ind_elec = all_elec['lower']*100 + all_elec['upper']
x_elec   = all_elec['te']

# Ion (proton) impact cross sections from a type 1 adf06 file

file    = os.getenv('ADASCENT') + '/adf06/lilike/lilike_aug99#li0_t1.dat'
all_ion = adas.xxdata_06(file=file)
ind_ion = all_ion['lower']*100 + all_ion['upper']
x_ion   = all_ion['te']


# Find the collision strengths for the 4-1, (1s2 3p1 3P - 1s2 2s1 2S) transition

iu = 4
il = 1

omega_elec = np.squeeze(all_elec['gamma'][np.where(ind_elec == il*100 + iu)[0],:])
omega_ion  = np.squeeze(all_ion['gamma'][np.where(ind_ion == il*100 + iu)[0],:])


# Convert to cross sections in units of pi a_0^2 = 8.7972e-17, noting
# that energies in adf04/adf06 are in cm-1 and we are plotting in eV.

delta_e  = all_elec['wa'][iu-1] - all_elec['wa'][il-1]
stat_wt  = 2.0 * all_elec['xja'][il-1] + 1.0
mt       = all_ion['mass_target']
mp       = all_ion['mass_prj']
mass_red = mt * mp / (mt + mp)

sigma_elec = 8.7972e-17 * omega_elec / (stat_wt * (delta_e * x_elec / 109737.26))
sigma_ion  = 8.7972e-17 * omega_ion / (mass_red * stat_wt * (delta_e * x_ion / 109737.26))


# A comparison plot

plt.plot(x_elec*delta_e/8066.0, sigma_elec, label='electron-impact')
plt.plot(x_ion*delta_e/8066.0, sigma_ion, label='H$^+$-impact')

plt.xscale('log')
plt.yscale('log')
plt.ylim([5e-19,5e-16])
plt.xlabel('Impact energy (eV)')
plt.ylabel('Cross section (cm$^2$)')

plt.legend()
plt.title('Li$^0$: $1s^2 2s^1\,\, {}^2S - 1s^2 3p^1\,\, {}^3P$')

plt.draw()
plt.show()
