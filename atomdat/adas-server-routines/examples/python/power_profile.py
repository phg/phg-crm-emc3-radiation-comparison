#------------------------------------------------------------------------------
# Use of ADAS python routine to plot a profile of the total radiated power 
# per ion of carbon.
#
# The power profile calculated here is in W cm**3. This should be multiplied
# by the electron density and carbon density to give the local power (W/cm**3) 
#------------------------------------------------------------------------------

import os
import adas as adas
import numpy as np
import matplotlib.pyplot as plt

# Setup a profile of temperatures and density based on a radial grid 
# arbitraty units for r, temperature in eV and density in cm**-3

a    = 0.7
r    = a * (np.arange(0.0, 101.0)/50.0 - 1.0)
te   = 1.0 + 1.2e3 * (1.0 - (r/a)**2)
dens = 1.0e10 + 1.0e13 *  (1.0 - (r/a)**2)**0.5


# No transport / infinite confinement / equilibrium ionization balance

iz0   = 6
year  = 96
elem  = 'c'
uid   = 'adas'

frac_c, power_c = adas.run_adas405(elem=elem, uid=uid, year=year, dens=dens, te=te)


# If the fractional abundance is taken from a transport code the power
# must be constructed from the adf11 plt and prb coefficients.
#
# Note that the plt and prb contributions are driven by different
# ionization stages. There is no plt coefficient for the fully 
# stripped stage and no prb driven by the neutral stage.
#
#      plt runs from is1=1, iz0 (neutral to H-like)
#      prb               1, iz0 singly ionized to fully stripped  
#
# For this example we will use the equilibrium fractional abundance, 
# frac_c['ion'], calculated above.


f_plt = os.getenv('ADASCENT') + '/adf11/plt96/plt96_c.dat'
f_prb = os.getenv('ADASCENT') + '/adf11/prb96/prb96_c.dat'
power = np.zeros(r.size)

for i in range(1, iz0+1):

    is1 = i
    plt_c = adas.read_adf11(file=f_plt, adf11type='plt', is1=is1, te=te, dens=dens)
    prb_c = adas.read_adf11(file=f_prb, adf11type='prb', is1=is1, te=te, dens=dens)

    power = power + (frac_c['ion'])[:,i-1] * plt_c + (frac_c['ion'])[:,i] * prb_c


# Plot the results

f = plt.figure(figsize=(26/2.54, 18/2.54))
plt.subplots_adjust(hspace=0.001, wspace=0.4)


#  - Te and density profiles

ax1 = plt.subplot2grid((6,3), (0,0), rowspan=3)
ax1.plot(r, te/1e3)
plt.ylabel('Te (keV)')
plt.title('Plasma profiles')
ax2 = plt.subplot2grid((6,3), (3,0), rowspan=3, sharex=ax1)
ax2.plot(r, dens/1e13)
plt.ylabel('Ne (10$^{13}$ cm$^{-1}$)')

xticklabels = ax1.get_xticklabels()
plt.setp(xticklabels, visible=False)
plt.xlabel('Major radius')


#  - carbon fractional abundance profiles (don't plot the neutral)

ax1 = plt.subplot2grid((6,3), (0,1), rowspan=1)
ax1.plot(r, (frac_c['ion'])[:,0])
xticklabels = ax1.get_xticklabels()
plt.setp(xticklabels, visible=False)

for i in range(1,7):
    ax1 = plt.subplot2grid((6,3), (i-1,1), rowspan=1, sharex=ax1)
    ax1.plot(r, (frac_c['ion'])[:,i])
    ax1.text(0.5, 0.5, "C%d+" % (i), va="center", ha="center", transform = ax1.transAxes)
    xticklabels = ax1.get_xticklabels()
    if i == 1 : plt.title('Fractional abundance')
    if i != iz0 : plt.setp(xticklabels, visible=False)

plt.xlabel('Major radius')


#  - compare total power (per carbon ion) from adas405 and our calculation

ax1 = plt.subplot2grid((6,3), (0,2), rowspan=6)
ax1.plot(r, power, label='from adf11')
ax1.plot(r, power_c['total'], 'ro', label='adas405')
ax1.legend(numpoints=1, loc='upper center')

plt.ylabel('Radiated power / ion (W cm$^3$)')
plt.xlabel('Major radius')
plt.title('Radiated power')


plt.show()
