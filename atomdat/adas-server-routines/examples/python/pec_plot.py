#---------------------------------------------------------------------------
# Use of ADAS python routine to plot photon emissivity coefficient.
#
# 29-11-2015: this works with v2.6.6, v2.7.3 and 3.3.5. It fails with 3.3.2
#---------------------------------------------------------------------------

import adas as adas
import numpy as np
import os
import matplotlib.pyplot as plt

# Interpolate data from adf15

file  = os.getenv('ADASCENT') + '/adf15/pec96#c/pec96#c_pju#c4.dat'
itval = 70
te    = adas.adas_vector(low=10, high=1e3, num=itval)
dens  = np.zeros(itval) + 1e13

coeff_exc, info = adas.read_adf15(file=file, block=50, te=te, dens=dens)
coeff_rec, info = adas.read_adf15(file=file, block=164, te=te, dens=dens)


#----------------
# Plot the figure
#----------------

fig = plt.figure(figsize=(26/2.54, 18/2.54))

box = fig.add_subplot(1,1,1)

p1  = box.plot(te, coeff_exc, label='Excitation contribution')
p2  = box.plot(te, coeff_rec, label='Recombination contribution')

p3  = box.set_xscale('log', nonposx='clip')
p4  = box.set_yscale('log', nonposy='clip')
p5  = box.set_ylim([1e-15, 1e-7])

p7  = box.set_xlabel('Te (eV)')
p6  = box.set_ylabel('photon emissivity coefficinet (cm$^3$ s$^{-1}$)')
p8  = box.set_title('C${}^{4+} 1s^2 \ {}^{1}S_0 - 1s 2p \ {}^{1}P_1$')

p9  = box.legend(loc='lower right')


#---------------------
# Put figure on screen
#---------------------

plt.draw()
plt.show()
