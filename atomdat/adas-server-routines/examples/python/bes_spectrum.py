#---------------------------------------------------------------------------
# Use of ADAS phthon routine to plot beam emission spectrum
#
# 29-11-2015: this works with v2.6.6, v2.7.3 and 3.3.5. It fails with 3.3.2
#---------------------------------------------------------------------------

import adas as adas
import numpy as np
import matplotlib.pyplot as plt

# Balmer-alpha spectral feature
#
# Define beam, plasma and observer


beam   = {'mass' : 2.0, 'energy' : 40.0e3, 'te' : 20.0, 'density' : 4.27e9,
          'dc_x' : 0.0, 'dc_y' : 0.0, 'dc_z' : 1.0}
plasma = {'mass' : 2.0, 'te' : 8e3, 'dens' : 5.0e13, 'zeff' : 2.0}
bfield = {'value' : 3.3915, 'dc_x' : 0.788, 'dc_y' : 0.0053, 'dc_z' : 0.6152}
efield = {'value' : 0.0000, 'dc_x' : 1.000, 'dc_y' : 0.0000, 'dc_z' : 0.0000}
obs    = {'dc_x' : 0.8701, 'dc_y' : -0.047, 'dc_z' : 0.4905, 'sigma' : 1.00, 'pi' : 1.0}

wc, ec = adas.stark(beam=beam, plasma=plasma, bfield=bfield, efield=efield, obs=obs)

# Doppler-broadened the feature

npix   = 200
wvmin  = 6510
wvmax  = 6550.0
dwave  = np.asarray(np.zeros(npix), float)
wave   = adas.adas_vector(low=wvmin, high=wvmax, num=npix, linear=True)

emiss  = adas.c5dplr(npix=npix, wvmin=wvmin, wvmax=wvmax, 
                     wvcomp=wc, emcomp=ec, tev=beam['te'], amss=beam['mass'])

emiss = emiss / (wave[1] - wave[0])


#----------------
# Plot the figure
#----------------

fig = plt.figure(figsize=(26/2.54, 18/2.54))

ax  = fig.add_subplot(1,1,1)

p1  = ax.plot(wave, emiss, label='Broadened')
p2  = ax.plot(wc, ec, 'ro', label='Components')
p3  = ax.set_title("H Balmer-alpha (3-2)")
p4  = ax.legend(numpoints=1, loc='upper left')


#---------------------
# Put figure on screen
#---------------------

plt.draw()
plt.show()
