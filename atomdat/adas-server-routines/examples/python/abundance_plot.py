#---------------------------------------------------------------------------
# Use of ADAS python routine to plot the equilibrium balance of nitrogen
#---------------------------------------------------------------------------

import adas as adas
import numpy as np
import matplotlib.pyplot as plt

itval = 100
te    = adas.adas_vector(low=1, high=1e3, num=itval)
dens  = np.zeros(itval) + 1e13

year  = 96
elem  = 'n'
uid   = 'adas'

frac_n, power_n = adas.run_adas405(elem=elem, uid=uid, year=year, dens=dens, te=te)

nz = frac_n['ion'].shape[1]


#----------------
# Plot the figure
#----------------

fig = plt.figure(figsize=(26/2.54, 18/2.54))

# fractional abundance

box = fig.add_subplot(1,2,1)

for j in range(nz):
    p0 = box.plot(te, (frac_n['ion'])[:,j])

p1  = box.set_xscale('log', nonposx='clip')
p2  = box.set_yscale('log', nonposy='clip')
p3  = box.set_ylim([1e-3, 1.1])

p4  = box.set_xlabel('Te (eV)')
p5  = box.set_ylabel('Fractional abundance')
p6  = box.set_title('Nitrogen')

# Radiated power - total, continuum and per stage

box = fig.add_subplot(1,2,2)

p0 = box.plot(te, (power_n['total']), label='Total')
p1 = box.plot(te, (power_n['prb']), label='Recomb. + bremss.')
for j in range(nz):
    p2 = box.plot(te, (power_n['ion'])[:,j], '--')

p3  = box.set_xscale('log', nonposx='clip')
p4  = box.set_yscale('log', nonposy='clip')
p5  = box.set_ylim([1e-31, 1e-25])

p6  = box.set_xlabel('Te (eV)')
p7  = box.set_ylabel('Radiated power / ion (W cm${}^3$)')

p8  = box.legend(loc='upper right')


#---------------------
# Put figure on screen
#---------------------

plt.draw()
plt.show()
