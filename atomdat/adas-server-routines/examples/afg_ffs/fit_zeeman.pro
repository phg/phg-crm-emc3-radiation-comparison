;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS / FFS
;
; PURPOSE    :  Using FFS (Framework for Feature Synthesis) routines
;               fit a Zeeman feature (with a background and interfering
;               ordinary line) to a sample spectrum, of the multiplet
;               BeIII 1s 2s 3S - 1s 2p 3P (372.17nm).
;
;
; MODIFIED   :
;       1.1     Martin O'Mullane
;                 - First version.
;
; VERSION    :
;       1.1    04-01-2013
;-
;----------------------------------------------------------------------


PRO fit_zeeman

; Example spectrum from central ADAS

adascent = getenv('ADASCENT')
restore, adascent + '/arch603/jet_test.dat'

; Get the wavelength in nm and restrict to +/- 0.5nm about the feature
; Set the error to be square root of the counts

x = adas_vector(low=header.wavemin, high=header.wavemax, num=header.nsize,/linear)
x = x / 10.0
y = spectrum

ind = where(x GT 371.6 and x LT 372.6, count)
x   = x[ind]
y   = y[ind]
err = sqrt(y)


; Setup a ffs model defined by the MDL file 'zeeman_be2.mdl'
;
; 1. make a model object (container)
; 2. parse the MDL file
; 3. add the parsed MDL to the model
; 4. Set the independent x values in the model
; 5. make a fit object and a data structure with wavelength, intensity and error
; 6. fit the model to the data

model  = obj_new('ffs_model', modelname='zeeman_be2', /debug)
parser = obj_new('ffs_parser', file='zeeman_be2.mdl', /debug)

tmp = parser->apply(model)
tmp = model->setxdata(x)

tmp = model->pdsetup()
initialguess = (*(model->getresult())).intensity


fit  = obj_new('ffs_fit', /debug)
data = {x:x, y:y, error:err}
tmp  = fit->apply(model, data)


; Plot the data with the model fit and print the fitted parameters

res = *(model->getresult())

plot_io, x, y, yrange=[0.01,2], xtitle='Wavelength (nm)', ytitle='Intensity (au)'
oplot, res.wavelength, res.intensity,line=1



; Print out the fitted parameters

parnames = model->getparnames(/free, /full)
parvals  = model->getparvals(/free)

for j = 0, n_elements(parnames)-1 do $
    print, j, parnames[j], parvals[j], $
           format = '(i2, " : ", a20, " = ", f12.4)'

; Label graph

oplot, [371.65, 371.75], [7, 7], line=0
oplot, [371.65, 371.75], [5, 5], line=1

xyouts, 371.78, 7, 'JET spectrum'
xyouts, 371.78, 5, 'FFS (adas603) fitted spectrum'

; Plot fitted position of the ordinary line

oplot, [parvals[6],parvals[6]], [0.01, 10]
xyouts, 0.9999 * parvals[6],  1.0, 'Interfering line'

; Write the parsed model to a file in order to see all the limits

isok = parser->writedefinition(model, 'simplify_output.mdl')

stop

END
