;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS / FFS
;
; PURPOSE    :  Determine the C3+ and He+ ion temperature in a MAST
;               discharge by using FFS (Framework for Feature Synthesis)
;               and ADAS306 AFG (ADAS Feature Generation) routines to
;               fit the C IV n=6-5 and n=8-6 spectral features induced
;               by electron impact excitation measured between 465.3 nm
;               and 470.0 nm. This also includes a single Gaussian line
;               representing the He II n=4-3 transition around 468.5 nm.
;
; MODIFIED   :
;       1.1     Martin O'Mullane
;                 - First version.
;
; VERSION    :
;       1.1    19-12-2015
;-
;----------------------------------------------------------------------


PRO fit_306_feature

; Example spectrum from central ADAS

adascent = getenv('ADASCENT')
restore, adascent + '/arch603/mast_example.sav'

; The wavelength is in nm
; Set the error to be square root of the counts

x   = adas_vector(low=header.wavemin, high=header.wavemax, num=header.nsize,/linear)
y   = spectrum
err = sqrt(y)

; Setup a ffs model defined by the MDL file 'zeeman_be2.mdl'
;
; 1. make a model object (container)
; 2. parse the MDL file
; 3. add the parsed MDL to the model
; 4. Set the independent x values in the model
; 5. make a fit object and a data structure with wavelength, intensity and error
; 6. fit the model to the data


model  = obj_new('ffs_model', modelname='c3_he1_fit', /debug)
parser = obj_new('ffs_parser', file='c3_he1_fit.mdl', /debug)

tmp    = parser->apply(model)
tmp    = model->setxdata(x)

tmp    = model->pdsetup()
init   = (*(model->getresult())).intensity
xset   = (*(model->getresult())).wavelength


fit    = obj_new('ffs_fit')
data   = {x:x, y:y, error:err}
tmp    = fit->apply(model, data)

; Plot spectrum and the fit

result = *(model->getresult())

plot, x ,y, xtitle = 'Wavelength (nm)', ytitle = 'Intensity (a.u.)'
oplot, result.wavelength, result.intensity, psym=2


; Print results from the fit
;
; 1. extract parameters
; 2. calculate ion temperature from Carbon 306 feature and the He line

parnames_full = model->getparnames(/full)
parnames_free = model->getparnames(/free,/full)
parvals       = model->getparvals()
parvalserr    = fit->geterrors()

carbon_ti     = parvals[where(parnames_full eq 'a306_8_6.tion')]
carbon_ti_err = carbon_ti * sqrt(2.0 * (parvalserr[where(parnames_free eq 'doppler.fwhm')] / parvals[where(parnames_full eq 'doppler.fwhm')]))

fwhm          = parvals[where(parnames_full eq 'heII.fwhm')]
mass          = 4.003
const         = 1.09E-04
lambda        = parvals[where(parnames_full eq 'heII.pos')]
helium_ti     = fwhm^2 * mass / (const * lambda)^2
helium_ti_err = helium_ti * sqrt(2.0 * (parvalserr[where(parnames_free eq 'heII.fwhm')]/parvals[where(parnames_full eq 'heII.fwhm')]))

print, string(carbon_ti, carbon_ti_err, format='("C3+ Temperature: ",e8.2," +/- ",e8.2," eV")')
print, string(helium_ti, helium_ti_err, format='("He+ Temperature: ",e8.2," +/- ",e8.2," eV")')

stop

END
