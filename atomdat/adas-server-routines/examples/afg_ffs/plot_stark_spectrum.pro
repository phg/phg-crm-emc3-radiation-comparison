;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS / AFG
;
; PURPOSE    :  Contrast the AFG (ADAS Feature Generation) object approach
;               and the procedural ADAS way to plot the n=3-2 Stark 
;               spectrum.
;
;
; MODIFIED   :
;       1.1     Martin O'Mullane
;                 - First version.
;
; VERSION    :
;       1.1    04-01-2013
;-
;----------------------------------------------------------------------

PRO plot_stark_spectrum

;------------
; The afg way
;------------

; First make a 'stark' object

o = afg('stark', /obj)

; Look at description, default values and set some new ones

info = o->getdesc()
pars = o->getpars()

print, info.text
help, pars,/ st

; Set some conditions and accept defaults from object initialization

pars.beam_energy = 78.0   ; keV/amu

pars.obs_sigma = 1
pars.obs_pi    = 1

ok = o->setpars(pars=pars)


; Get the feature and plot it

result = o->getcalc()

plot, result.wv, result.intensity, /nodata, $
      xtitle='Wavelength (A)', ytitle = 'Intensity /ion (Photon cm!u-2!n s!u-1!n)'

for i = 0, n_elements(result.wv)-1 do oplot,[result.wv[i],result.wv[i]],[0,result.intensity[i]]


;--------------------
; The traditional way
;--------------------

; Set all parameters - nb these correspond to the afg object defaults

beam   = {mass : 2.0, energy : 78.0e3, te : 10.0, density : 1.00e10, $
          dc_x : 0.0, dc_y : 0.0, dc_z : 1.0}
plasma = {mass : 2.0, te : 4440.0, density : 2.49e13, zeff : 2.0}
bfield = {value : 3.3915, dc_x : 0.788, dc_y : 0.788, dc_z : 0.0}
efield = {value : 0.0000, dc_x : 1.000, dc_y : 1.0000, dc_z : 0.0000}
obs    = {dc_x : 0.8701, dc_y : -0.047, dc_z : 0.4905, sigma : 1.0, pi : 1.0}


adas305_get_stark,  beam           = beam,          $  
                    plasma         = plasma,        $
                    bfield         = bfield,        $
                    efield         = efield,        $
                    obs            = obs,           $  
                    n_lower        = 2,             $
                    n_upper        = 3,             $
                    wave_comp      = wave_comp,     $
                    emiss_comp     = emiss_comp,    $
                    wave_min       = wave_min,      $
                    wave_max       = wave_max,      $
                    npix           = npix,          $
                    adf22          = adf22,         $
                    emiss_doppler  = demiss
                    
; Overplot on afg feature
                
plots, wave_comp, emiss_comp, psym=6

stop
 
END
