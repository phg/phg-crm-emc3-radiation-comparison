;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS / FFS
;
; PURPOSE    :  Using FFS (Framework for Feature Synthesis) routines
;               plot a Gaussian, Lorentzian and Voigt line profile.
;
; MODIFIED   :
;       1.1     Martin O'Mullane
;                 - First version.
;
; VERSION    :
;       1.1    04-01-2013
;-
;----------------------------------------------------------------------

PRO show_line_shapes

; A 200 pixel grid centred at a wavelength at pixel 100

x = dindgen(200)

; Consider Gaussian, Lorentzian and Voigt profiles

gs = obj_new('ffs_gaussian', pos=100, fwhm=15, area=500)
lz = obj_new('ffs_lorentzian', pos=100, fwhm=20, area=500)
vg = obj_new('ffs_voigtian', pos=100.0, fwhmg=15.0, fwhml=20.0, area=1000.0)


; And a Voigt, which is a convolution of the Gaussian and Lorentzian

voigt = obj_new('ffs_model', elements=[gs,lz])


; Set the wavelength

res = gs->setxdata(x)
res = lz->setxdata(x)
res = vg->setxdata(x)

; Evaluate the shapes

res_gs    = gs->calculate()
res_lz    = lz->calculate()
res_vg    = vg->calculate()


; Plot the resulting profile shapes

plot, x, res_vg.intensity, xtitle = 'pixel', ytitle = 'Intensity'
oplot, x, res_gs.intensity, linestyle=1
oplot, x, res_lz.intensity, linestyle=2

; Label graph

oplot, [10, 25], [28, 28], line=0
oplot, [10, 25], [26, 26], line=1
oplot, [10, 25], [24, 24], line=2

xyouts, 28, 28, 'Voigt'
xyouts, 28, 26, 'Gaussian'
xyouts, 28, 24, 'Lorentzian'

stop

END
