from importlib import reload
import matplotlib.pylab as plt
import numpy as np
import amd.balmer.balmerls as bls; reload(bls)
import os
import sys
nq = 3 #epsilon
ne = 1.E13
te = 0.4
B  = 2.5
ang = 90. #theta observation angle
wmax = 5E-5 #Wavelength range (eV): frequency detuning
npts = 1000
iA = 0

woff = [0.,0.,0.,656.,486.,434.,410.,397.5]

if not(os.path.exists('./in.txt')):
    bls.in_param(nq,ne,te,B,ang,wmax,npts) #Call only if not existent

wl =[]; lsl=[]
#nql = [3,4,5,6,7]
nql = [3]
for nq in nql:
    iN,iT,iB = bls.set_bounds(ne,te,B)
    #print(iN,iT,iB)
    pn,name=bls.set_name_file(nq,iN,iT,iB,iA)
    #print(pn,name)
    w_arr,ls_arr = bls.read_file(pn,name)
    w = w_arr[0,0,0,:]
    ls = bls.ls_interpol(nq,ne,te,B,wmax,npts,w_arr,ls_arr,iN,iT,iB)
    lsl.append(ls)
    wl.append(w+woff[nq])

fig = plt.figure()
ax = fig.gca()
for i in range(len(nql)):
    ax.plot(wl[i],lsl[i],label=str('nq=%i' %nql[i]))
    wm = np.mean(wl)
plt.show(block=False)
