-----------------------------------------------------
Initial principal quantum number         :        3
Density (cm-3)                           :   1.00E+15
Temperature (eV)                         :   1.00E+00
Magnetic field (T)                       :   2.00E+00
Angle (degrees)                          :   9.00E+01
-----------------------------------------------------
Delta_omega max (eV)                     :   1.00E-03
Number of points                         :     1000
-----------------------------------------------------
