# -*- coding: utf-8 -*-
#Python to load and view ADAS data SXB/PEC

import numpy as np
import matplotlib as mp
import matplotlib.pyplot as plt
import matplotlib.gridspec as gspec
from matplotlib.backends.backend_qt4agg \
import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg \
import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
import copy
import scipy.interpolate as spi
from importlib import reload

import atomdat.adas.adf11 as adf11
import atomdat.adas.adf13 as adf13
import atomdat.adas.adf15 as adf15
import atomdat.lz as lz

import utils.colors as colors; clrs = colors.clrs()

class ADAS():
  class MplCanvas(FigureCanvas):
    """Class to represent the FigureCanvas widget"""
    def __init__(self, parent=None, form=0):
    # setup Matplotlib Figure and Axis
      self.fig = mp.figure.Figure()
    
    # initialization of the canvas
      FigureCanvas.__init__(self, self.fig)
    # set the parent widget
      self.setParent(parent)
    # we define the widget as expandable
      FigureCanvas.setSizePolicy(self,QtGui.QSizePolicy.Expanding,\
                                      QtGui.QSizePolicy.Expanding)
    # notify the system of updated policy
      FigureCanvas.updateGeometry(self)

  ##------------------------------------------------------------------------
  # Initialize and provide canvas
  ##------------------------------------------------------------------------
  def __init__(self):
    #print 'ADAS densities are in 1/cm^3'
    #self.app = QtGui.QApplication(sys.argv)
    #self.gui = mplw.GUI()
    #self.ca = mplw.MplWidget()
    reload(adf11);reload(adf13);reload(adf15);
    self.adf15 = adf15.ADF15()
    self.adf13 = adf13.ADF13()
    self.adf11 = adf11
    self.lz   = lz.LZ()
    self.adaspath = '/afs/ipp/u/flr/adas/'
    #self.ca = self.MplCanvas()
    #self.ca.navtob = NavigationToolbar(self.ca,self.ca)
    #self.ca.axes = {}
    return

  ##------------------------------------------------------------------------
  # Calculate coronal ionization balance
  ##------------------------------------------------------------------------
  def coronal(self,elem,temax=1.E4,nt=int(1.E5),pn='/afs/ipp/u/adas/adas/adf11/',fplt=False):
    #scd = self.adf11.ADF11('scd96/scd96_'+elem+'.dat',pn=pn)
    #acd = self.adf11.ADF11('acd96/acd96_'+elem+'.dat',pn=pn)
    scd = self.adf11.ADF11('scd96\\scd96_'+elem+'.dat',pn=pn)
    acd = self.adf11.ADF11('acd96\\acd96_'+elem+'.dat',pn=pn)
    fz = np.ones((nt,acd.dat.nz+1))
    ff = np.ones(nt)

    te = np.linspace(np.power(10,acd.dat.te[0]),temax,nt)
    ne = np.ones(nt)*np.power(10,acd.dat.ne[0])

    for i in range(acd.dat.nz):
      fz[:,i+1] = fz[:,i]*(np.power(10,scd.interpolate(ne,te,i,log=True)-acd.interpolate(ne,te,i,log=True)))
      ff = np.sum(fz[:,0:i+2],1)
      for j in range(i+2):
        fz[:,j] = fz[:,j]/ff

    spl = []
    for i in range(acd.dat.nz+1):
      x   = te
      y   = fz[:,i]
      yerr= np.clip(y*0.01,1.0E-30,1.0E30)
      spl.append(spi.splrep(x,y,k=1,w=1./yerr,s=0.))

    if fplt:
      fig = plt.figure()
      ax = plt.gca()
      for i in range(acd.dat.nz+1):
        fzfit = spi.splev(te,spl[i],der=0)
        ax.plot(te, fz[:,i],color=clrs.ls[i])
        ax.plot(te,fzfit,'--',color='k',alpha=0.2)
      plt.xscale('log')
      plt.show(block=False)
    return te, fz, spl
