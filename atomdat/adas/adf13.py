# -*- coding: utf-8 -*-
#Python to load and view ADAS data SXB/PEC
import matplotlib.pyplot as plt
import numpy as np
import scipy.interpolate as spi

import utils.data as data

class ADF13():

  ##------------------------------------------------------------------------
  # Initialize and provide canvas
  ##------------------------------------------------------------------------
  def __init__(self):
    self.dat = data.data()
    return

  def read(self,fn,pn='C:\\Users\\flr\\rzgshare\\work\\W7X\\python\\data\\atomdat\\adas',debug=False):
    if debug: 
      print('Read %s' %(pn+fn))
    fp = open(pn+fn,'r')
    line=[]
    line = fp.readline().split()

    self.dat.nlines=int(line[0])
    self.dat.species=(line[1]+line[2]+line[3])[1:]

    wvl=[]
    wvlunit=[]
    ndens=[]
    ntemp=[]
    filmem=[]
    ttype=[]
    indim=[]
    isel=[]
    firstpass=True

  # Read header
    for i in range(self.dat.nlines):
      line=[]
      line = fp.readline().split()
      if debug: 
        print(i, line)
      wvl.append(line[0])
      wvlunit.append(line[1])
      ndens.append(int(line[2]))
      ntemp.append(int(line[3]))
      filmem.append(line[6])
      #ttype.append(line[9])
      #indim.append(line[12])
      isel.append(int(line[-1]))
      if firstpass:
          dens=np.zeros((int(ndens[0]),int(self.dat.nlines)))
          temp=np.zeros((int(ntemp[0]),int(self.dat.nlines)))
          sxb=np.zeros((int(ndens[0]),int(ntemp[0]),int(self.dat.nlines)))
          firstpass=False

    # Read density vector
      line=[]
      for j in range(int(ndens[i]/8)):
        line.extend(fp.readline().split()) 
      if ( int(ndens[i])%8 != 0 ):
        line.extend(fp.readline().split())
      dens[0:len(line),i]=[float(x) for x in line]

    # Read temperature vector
      line=[]
      for j in range(int(ntemp[i]/8)):
        line.extend(fp.readline().split()) 
      if ( int(ntemp[i])%8 != 0 ):
        line.extend(fp.readline().split())
      temp[0:len(line),i]=[float(x) for x in line]

    # Read S|XB
      for j in range(ndens[i]):
        line=[]
        for k in range(int(ntemp[i]/8)):
          line.extend(fp.readline().split()) 
        if ( int(ntemp[i])%8 != 0 ):
          line.extend(fp.readline().split())
        sxb[j,0:len(line),i]=[float(x) for x in line]
    self.dat.comment=fp.read()
    fp.close()

  # Create structure
    self.dat.filmem=filmem
    self.dat.ttype=ttype
    self.dat.indim=indim
    self.dat.isel=isel
    self.dat.wvl=np.asarray(wvl,dtype=np.float)
    self.dat.wvlunit=wvlunit
    self.dat.ndens=ndens
    self.dat.ntemp=ntemp
    self.dat.dens=np.asarray(dens.T,dtype=np.float) *1.E6  #convert to 1/m^3
    self.dat.temp=np.asarray(temp.T,dtype=np.float)
    self.dat.sxb=np.asarray(sxb,dtype=np.float)
    return self.dat

  ##------------------------------------------------------------------------
  ##------------------------------------------------------------------------
  def plot(self,idx,ner=[0,0],ter=None,xscl='linear',yscl='linear'):
    fig = plt.figure()
    plt.title('SXB '+str('%.2f' %(self.dat.wvl[idx]/10.))+' nm')
    plt.xlabel('Temp (eV)')
    plt.ylabel('S/XB')
    for i in range(ner[0],ner[1]):
      plt.plot(self.dat.temp[idx,:],self.dat.sxb[i,:,idx], \
                   label='dens '+str('%.1e' %self.dat.dens[idx][i]))
    plt.xlim(ter)
    plt.xscale(xscl)
    plt.yscale(yscl)
    plt.legend(loc=4)
    plt.show(block=False)
    return fig

  ##------------------------------------------------------------------------
  ##------------------------------------------------------------------------
  def plott(self,nt,\
            xscl='log',yscl='log',\
            xr=None,yr=None,zr=None):
      import matplotlib as mp
      d = self.dat['dens'][nt,:]
      t = self.dat['temp'][nt,:]
      p = self.dat['sxb'][:,:,nt]
      plt.contourf(t,d,p,norm=mp.colors.LogNorm())
      plt.xscale(xscl)
      plt.yscale(yscl)
      plt.ylim(yr)
      plt.xlim(xr)
      #plt.zlim(zr)
      plt.colorbar()
      plt.show(block=False)
      return