# -*- coding: utf-8 -*-
#Python to load and view ADAS data SXB/PEC
import matplotlib.pyplot as plt
import numpy as np
import scipy.interpolate as spi

import utils.data as data
from  importlib import reload
import atomdat.adas.adfbase as adfbase; reload(adfbase)

class ADF11(adfbase.ADFBASE):
  ##------------------------------------------------------------------------
  # Initialize
  ##------------------------------------------------------------------------
  def __init__(self,fn=None,pn=''):
    super().__init__(fn,pn)
    return

  ##------------------------------------------------------------------------
  # Read adf11 (iso-nuclear masterfiles) from ADAS
  ##------------------------------------------------------------------------
  def read(self,fn,pn='C:\\Users\\flr\\rzgshare\\work\\W7X\\python\\data\\atomdat\\adas',debug=False):
    fn = pn+fn
    if debug:
      print('Read %s' %(fn))
    fp = open(fn,'r')

  # Read definition header
    line = fp.readline().split()
    self.dat.nz   = int(line[0])
    self.dat.nd   = int(line[1])
    self.dat.nt   = int(line[2])
    self.dat.zmin = int(line[3])
    self.dat.zmax = int(line[4])

    #dens=np.zeros(self.dat.nd)
    #temp=np.zeros(self.dat.nt)
    dat=np.zeros((self.dat.nd,self.dat.nt,self.dat.nz))
    self.dat.IGRD = []
    self.dat.IPRT = []
    self.dat.MH = []
    self.dat.Z = []
    self.dat.DATE = []

  # Skip line
    fp.readline()

  # Read density vector
    line=[]
    for j in range(int(self.dat.nd/8)):
      line.extend(fp.readline().split())
    if ( self.dat.nd%8 != 0 ):
      line.extend(fp.readline().split())
    self.dat.ne=np.array([float(x) for x in line]) + 6  #convert to 1/m^3
    self.dat.ner=[min(self.dat.ne),max(self.dat.ne)]

  # Read temperature vector
    line=[]
    for j in range(int(self.dat.nt/8)):
      line.extend(fp.readline().split()) 
    if ( self.dat.nt%8 != 0 ):
      line.extend(fp.readline().split())
    self.dat.te=np.array([float(x) for x in line])
    self.dat.ter=[min(self.dat.te),max(self.dat.te)]

  # Read data fields
    for i in range(self.dat.nz):
    # Read species header
      line=[]
      line = fp.readline().split('/')
      if debug:
        print(i, line)
      for j,tmp in enumerate(['IPRT','IGRD','MH','Z']):
        if '=' in line[j+1]:
          if tmp != 'MH':
            self.dat[tmp].append(int(line[j+1].split('=')[1]))
          else:
            self.dat[tmp].append(float(line[j+1].split('=')[1]))
      self.dat.DATE.append(line[4])

    # Read data field
      for j in range(self.dat.nt):
        line=[]
        for k in range(int(self.dat.nd/8)):
          line.extend(fp.readline().split())
        if ( self.dat.nd%8 != 0 ):
          line.extend(fp.readline().split())
        dat[:,j,i]=np.array([float(x)-6 for x in line])
    self.dat.dat = dat
    self.dat.comment=fp.read()
    fp.close()
    if debug:
      print('Make splines')
    self.makespline()
    return self.dat