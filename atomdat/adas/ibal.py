  # -*- coding: utf-8 -*-
#Python to load and view ADAS data SXB/PEC

import numpy as np
import matplotlib.pyplot as plt

import copy
import scipy.interpolate as spi
import scipy.linalg as scl
from importlib import reload

import atomdat.adas.adf11 as adf11
import atomdat.adas.adf13 as adf13
import atomdat.adas.adf15 as adf15
import atomdat.lz as lz

import utils.colors as colors; reload(colors); clrs = colors.clrs()
##------------------------------------------------------------------------
# Calculate coronal ionization balance
##------------------------------------------------------------------------
def coronal(elem,temax=1.E4,nt=int(5.E2),\
            fns=None,fna=None,year='96',\
            pn='/afs/ipp/u/adas/adas/adf11/',fplt=False,fsilent=False):
  #scd = self.adf11.ADF11('scd96/scd96_'+elem+'.dat',pn=pn)
  #acd = self.adf11.ADF11('acd96/acd96_'+elem+'.dat',pn=pn)
  if fns is None:
    fns = 'scd'+year+'\\scd'+year+'_'+elem+'.dat'
  if fna is None:
    fna = 'acd'+year+'\\acd'+year+'_'+elem+'.dat'
  scd = adf11.ADF11(fns,pn=pn)
  acd = adf11.ADF11(fna,pn=pn)

  fz = np.ones((nt,acd.dat.nz+1))
  ff = np.ones(nt)

  te  = np.logspace(acd.dat.te[0],np.log10(temax),nt)
  ne = np.ones(nt)*np.power(10,acd.dat.ne[0])

  for i in range(acd.dat.nz):
    fz[:,i+1] = fz[:,i]*(np.power(10,scd.interpolate(ne,te,i,log=True,silent=fsilent)-acd.interpolate(ne,te,i,log=True,silent=fsilent)))
    ff = np.sum(fz[:,0:i+2],1)
    for j in range(i+2):
      fz[:,j] = fz[:,j]/ff

  spl = []
  for i in range(acd.dat.nz+1):
    x   = te
    y   = fz[:,i]
    yerr= np.clip(y*0.01,1.0E-30,1.0E30)
    spl.append(spi.splrep(x,y,k=1,w=1./yerr,s=0.))

  if fplt:
    fig = plt.figure()
    ax = plt.gca()
    for i in range(acd.dat.nz+1):
      fzfit = spi.splev(te,spl[i],der=0)
      ax.plot(te, fz[:,i],color=clrs.ls[i])
      ax.plot(te,fzfit,'--',color='k',alpha=0.2)
    plt.xscale('log')
    plt.show(block=False)
  return te, fz, spl

##------------------------------------------------------------------------
# Calculate coronal ionization balance
##------------------------------------------------------------------------
def calc_fz(elem,nt=int(1.E2),year='96',\
        ne=None,ter=None,te=None,\
        fns=None,fna=None,fnc=None,fnt=None,fnb=None,\
        s=None,taus=0.,taua=0,\
        #s=None,taus=1.E-2,taua=1.E-4,\
        n0=0.,l0=0.1,e0=0.2\
        ,pn=None,fplt=False,debug=False,fsilent=False):
  '''
  #Transport loss terms 
  ta - to hotter regions
  ts - to colder regions
  s  - source term
  '''
  if pn is None:
    pn = 'C:\\Users\\flr\\rzgshare\\work\\W7X\\python\\data\\atomdat\\adas\\adf11\\'

  if type(year) is str:
    year = [year]*5

  if fns is None:
    fns = 'scd'+year[0]+'\\scd'+year[0]+'_'+elem+'.dat'
  if fna is None:
    fna = 'acd'+year[1]+'\\acd'+year[1]+'_'+elem+'.dat'
  if fnc is None:
    fnc = 'ccd'+year[2]+'\\ccd'+year[2]+'_'+elem+'.dat'
  if fnt is None:
    fnt = 'plt'+year[3]+'\\plt'+year[3]+'_'+elem+'.dat'
  if fnb is None:
    fnb = 'prb'+year[4]+'\\prb'+year[4]+'_'+elem+'.dat'

  scd = adf11.ADF11(fns,pn=pn)
  acd = adf11.ADF11(fna,pn=pn)
  try:
    ccd = adf11.ADF11(fnc,pn=pn)
  except:
    print('## Warning - No CCD data available !!! ##')
    ccd = scd
  prt = adf11.ADF11(fnt,pn=pn)
  prb = adf11.ADF11(fnb,pn=pn)
  
  #Provide temperatures & densities
  nz  = acd.dat.nz
  if te is None:
    if ter is None:
      ter = [acd.dat.te.min(),acd.dat.te.max()]
    te  = np.logspace(ter[0],ter[1],nt)
  nt = te.shape[0]

  if ne is None:
    ne = np.ones(te.shape)*1E17
    print('Assume coronal equilibrium (1E17 m-3)')
  if type(ne) is float:
    ne = np.ones(te.shape)*ne
  
  #Provide neutral densities for CX
  if n0 > 0.:
    n0l = np.exp(-te**e0/l0)
    n0l = n0*n0l/n0l.max()
  else:
    n0l = [n0 for i in range(nt)]
  n0 = np.array(n0l)

  #Provide source terms
  if s is None:
    s = np.array([0. for i in range(nt)])
  
  fz = np.zeros((nt,nz+1))
  lz  = np.zeros((nt,nz+1))
  lzl = np.zeros((nt,nz+1))
  
  m = make_transition_matrix(acd,scd,ccd,ne,te,n0,taus,taua,fsilent=fsilent)

  for i in range(nt):
    tmp = m[:,:,i]
    #Normalize matrix
    #norm = tmp.max()
    #tmp = tmp/norm
    #s = s/norm
    
    #Solve kernel for fractional abundance
    z = np.abs(scl.null_space(tmp))
    if z.shape[1] > 1: z = z[:,0]
    z = np.squeeze(z)
    #Normalize fractional abundance (Why did this not work with matrix?)
    fz[i,:] = z/z.sum()
    #x = np.linalg.solve(m,s)


  #Determine Lz function
  for j in range(nz):
    lzl[:,j] = prt.interpolate(ne,te,j,silent=fsilent)*fz[:,j]
    lz[:,j]  = prt.interpolate(ne,te,j,silent=fsilent)*fz[:,j]
    lz[:,j] += prb.interpolate(ne,te,j,silent=fsilent)*fz[:,j]

  #Total radiative power function
  lz[:,-1] = np.sum(lz,1)
  lzl[:,-1] = np.sum(lzl,1)

  if fplt:
    if n0[0] > 0.:
      plt.figure()
      plt.plot(te,n0)
      plt.yscale('log')
      plt.xscale('log')
      plt.ylabel('n$_0$ [m-3]')
      plt.xlabel('T$_e$ [eV]')
      plt.ylim((1.E16,1.E21))
      #plt.show(block=False)
  return te, fz, lz, lzl

lcol = ['k','b','g','y','orange','r','violet','brown','gray']
def plot_fz(te,fz,fig=None,leg=True,logy=False,yr=None,ls='-',lr=None):
    nz = fz.shape[1]
    if fig is None:
      fig = plt.figure()
      plt.plot([],[])
    ax = fig.gca()

    if lr is None:
      lr = np.arange(100)
    nr = len(lr)
    li = 0; lab=str('%s' %0)
    lab = str('%i - %i' %(0,lr[1]))
    col = clrs.resetnull()

    if nr <= 12:
      for i in range(nz):
        if i < lr[li]:
          ax.plot(te,fz[:,i],c=col,ls=ls)
        elif li < len(lr)-1:
          col = clrs.next()
          lab = str('%i - %i' %(lr[li],lr[li+1]-1))
          li += 1            
          ax.plot([],[],c=col,ls=ls,label=lab)
          ax.plot(te,fz[:,i],c=col,ls=ls)
        else:
          col = clrs.next()
          lab = str('> %i' %(lr[li]))
    else:
      for i in range(nz):
          ax.plot(te,fz[:,i],c=clrs.next(),ls=ls)
  
    if leg:
      ax.legend()
    if logy:
      ax.set_yscale('log')
    ax.set_ylim((1.E-5,1.1))
    ax.set_xlabel('T$_e$ [eV]')
    ax.set_ylabel('f$_A$')
    ax.set_xscale('log')
    #plt.show(block=False)
    if not(yr is None):
      ax.set_ylim(yr)
    return fig

def plot_lz(te,lz,fig=None,leg=True,logy=False,ls='-',lr=None):
  nz = lz.shape[1]
  if fig is None:
    fig = plt.figure()
    plt.plot([],[])
  ax = fig.gca()
  col = clrs.resetnull()

  if lr is None:
      lr = np.arange(100)
  nr = len(lr)
  li = 0; lab=str('%s' %0)
  lab = str('%i - %i' %(0,lr[1]))
  col = clrs.resetnull()

  if nr < 12:
      for i in range(nz):
        if i < lr[li]:
          ax.plot(te,lz[:,i],c=col,ls=ls)
        elif li < len(lr)-1:
          col = clrs.next()
          lab = str('%i - %i' %(lr[li],lr[li+1]-1))
          li += 1            
          ax.plot([],[],c=col,ls=ls,label=lab)
          ax.plot(te,lz[:,i],c=col,ls=ls)
        else:
          col = clrs.next()
          lab = str('> %i' %(lr[li]))
    #for i in range(nz):
    #    col = clrs.next()
    #    ax.plot(te,lz[:,i],c=col,ls=ls,label=str('%i'%i))
  else:
    for i in range(nz):
        col = clrs.next()
        ax.plot(te,lz[:,i],c=col,ls=ls)
  ax.plot(te,lz[:,-1],c='gray',ls=ls,label='Sum')

  if leg:
    ax.legend()
  if logy:
    ax.set_yscale('log')
  ax.set_xlabel('T$_e$ [eV]')
  ax.set_ylabel('L$_Z$ [Wm3]')
  ax.set_xscale('log')
  
  ax.set_ylim((1.E-35,1.E-30))
  ax.set_yscale('log')
  #plt.show(block=False)
  return fig

def make_transition_matrix(acd,scd,ccd,ne,te,n0=None,taus=0.,taua=0.,taup=0.,\
                            fsilent=False):
  '''
  taua is a decay time to lower states (transport to lower temp.)
  taus is a decay time to higher states (transport to higher temp.)
  taup is a general decay time of all ions (recycling/pumping)
  '''
  nz = acd.dat.nz+1
  nt = te.size
  m = np.zeros((nz,nz,nt))
  if taua == 0.:
    itaua = 0.
  else:
    itaua = 1./(taua+1e-60)/ne
  if taus == 0.:
    itaus = 0.
  else:
    itaus = 1./(taus+1e-60)/ne
  if taup == 0.:
    itaup = 0.
  else:
    itaup = 1./(taup+1e-60)/ne
  if n0 is None:
    n0 = np.zeros(nt)#np.ones(nt)*1E10

  #Produce matrix for population distribution calculation in steady-state
  #Be aware that the ADAS data only has the relevant charge states in the adf11
  #datasets. This implies an index shift (-1) for the acd/ccd rates (as 0 is not incl.)
  for i in range(nz):
      #m[i,i] = 0.
      if i > 0 and i < nz-1:
          m[i,i,:]   += - scd.interpolate(ne,te,i,silent=fsilent)\
                        - acd.interpolate(ne,te,i-1,silent=fsilent)\
                        - ccd.interpolate(ne,te,i-1,silent=fsilent)*n0/ne\
                        - itaus - itaua - itaup
          m[i,i-1,:] += + scd.interpolate(ne,te,i-1,silent=fsilent)\
                        + itaus
          m[i,i+1,:] += + acd.interpolate(ne,te,i,silent=fsilent)\
                        + ccd.interpolate(ne,te,i,silent=fsilent)*n0/ne\
                        + itaua
      elif i == 0:
          m[i,i,:]   += - scd.interpolate(ne,te,i,silent=fsilent)\
                        - itaus - itaup
          m[i,i+1,:] += + acd.interpolate(ne,te,i,silent=fsilent)\
                        + ccd.interpolate(ne,te,i,silent=fsilent)*n0/ne\
                        + itaua
      elif i == nz:
          m[i,i,:]   += - acd.interpolate(ne,te,i-1,silent=fsilent)\
                        - ccd.interpolate(ne,te,i-1,silent=fsilent)*n0/ne\
                        - itaua - itaup
          m[i,i-1,:] += + scd.interpolate(ne,te,i-1,silent=fsilent)\
                        + itaus

  for i in range(nt):
    m[:,:,i] *= ne[i]
  return m