# -*- coding: utf-8 -*-
#Python to load and view ADAS data SXB/PEC
import matplotlib.pyplot as plt
import numpy as np
import scipy.interpolate as spi
import matplotlib.gridspec as gspec

import utils.plot as pu
import utils.data as data

class LEVELS():
##------------------------------------------------------------------------
  # Initialize
  ##------------------------------------------------------------------------
  def __init__(self):
    self.dat = data.data()
    return

  ##------------------------------------------------------------------------
  # Read ADAS level files
  ##------------------------------------------------------------------------
  def read(self,fn,path='',v=0,debug=False):
    fn = path+fn
    if debug:
      print('Read %s' %(fn))
    fp = open(fn,'r')
    line=[]

    dat = self.dat
    dat.idx = []
    dat.config = []
    dat.term = []
    dat.j    = []
    dat.level = []
    dat.freq = []
    dat.energy = []

    #Skip header
    for i in range(4):
        line = fp.readline()
        if debug: print(line.split('|'))

    idx = 0
    while True:
        line = fp.readline()
        if line is None or line.strip() == '': break
        line = line.split('|')
        if debug: print(line)
        #if line[0].strip() != '':
        if line[1].strip() != '':
            if line[0].strip() == '':
                idx = int(-1)
            else:
                idx = int(line[0])
            config = line[1]
            term = line[2]
        if line[3].strip() == '': continue
        dat.idx.append(idx)
        dat.config.append(config)
        dat.term.append(term)
        dat.j.append(str(line[3]))
        tmp = line[4]
        if tmp.strip() != '_':
            tmp = float(tmp)
        else:
            tmp = 0.
        dat.level.append(tmp) #cm-1
        dat.freq.append(tmp*29.9792458E9) #Hz
        #dat.energy.append(float(line[4])1E2*1.986445857E-25) #cm-1 J m
        dat.energy.append(tmp*1E2*1239.841984E-9) #cm-1 eV nm
    return dat