# -*- coding: utf-8 -*-
#Python to load and view ADAS data SXB/PEC
import matplotlib.pyplot as plt
import numpy as np
import scipy.interpolate as spi

import utils.data as data
padas = 'C:\\Users\\flr\\rzgshare\\work\\W7X\\python\\data\\atomdat\\adas'

class ADFBASE():
  ##------------------------------------------------------------------------
  # Initialize
  ##------------------------------------------------------------------------
  def __init__(self,fn=None,pn=''):
    self.dat = data.data()
  # Read data
    if not fn is None:
      self.read(fn,pn=pn)
    return

  ##------------------------------------------------------------------------
  # Read adf files from ADAS
  ##------------------------------------------------------------------------
  def read(self,fn,pn=padas,debug=False):
    pass
    return

  ##------------------------------------------------------------------------
  # Make splines for interpolation
  ##------------------------------------------------------------------------
  def makespline(self, degree=1):
    '''
       Get Spline function for interpolation on log-log grid
       degree = 1 : Degree of Spline
       Linear interpolation on log-log grid is recommended
       No oscillations at grid boudary!
    '''
  # Generate grid
  # Interpolation on log-log grid is recommended (Puetti)
    te=self.dat.te
    ne=self.dat.ne
    dat=self.dat.dat

    #Generate Interpolation Spline (private)
    self.logspl = []
    for i in range(self.dat.nz ):
      self.logspl.append(spi.RectBivariateSpline(ne, te, dat[:,:,i], kx=degree, ky=degree))
    return

  ##------------------------------------------------------------------------
  # Interpolate with linear splines in log-log space
  ##------------------------------------------------------------------------
  def check_range(self,ne,te,flog=True,debug=False,silent=False):
    '''
       Check requested range and return clipped arrays
    '''
    # Convert into Log10
    if flog:
      ne = np.log10(ne)
      te = np.log10(te)

    if not(     (self.dat.ner[0] <= ne.min() and ne.max() <= self.dat.ner[1]) \
           and  (self.dat.ter[0] <= te.min() and te.max() <= self.dat.ter[1]) \
           or silent ):
      print( 'Requested ne range: %.2e - %.2e' %( np.power(10,ne.min()),np.power(10,ne.max()) ) )
      print( 'Available ne range: %.2e - %.2e' %( np.power(10,self.dat.ner[0]),np.power(10,self.dat.ner[1]) ) )
      print( 'Requested Te range: %.2e - %.2e' %( np.power(10,te.min()),np.power(10,te.max()) ) )
      print( 'Available Te range: %.2e - %.2e' %( np.power(10,self.dat.ter[0]),np.power(10,self.dat.ter[1]) ) )
      print( '  !! Warning - Out of interpolation domain !!' )

    te = np.clip(te,self.dat.ter[0],self.dat.ter[1])
    ne = np.clip(ne,self.dat.ner[0],self.dat.ner[1])
    return [ne,te]

  ##------------------------------------------------------------------------
  # Interpolate with linear splines in log-log space
  ##------------------------------------------------------------------------
  def interpolate(self,ne,te,idx,log=False,flog=True,debug=False,silent=False):
    '''
       Return interpolated values
       Assume density and temperature in usual units (not log!)
    '''
    # Test shapes
    ne, te = np.array(ne),np.array(te)
    if ne.shape[0] != te.shape[0]:
      print('## ERROR ## - Shapes of ne & Te do not match!')
      return -1

    ne,te = self.check_range(ne,te,flog=flog,silent=silent)

    res = np.zeros(len(ne))
    for i in range(len(ne)):
      #try:
        if log:
          res[i] = self.logspl[idx](ne[i],te[i])
        else:
          res[i] = np.power(10,self.logspl[idx](ne[i],te[i]))
      #except Exception:
      #  res[i] = 0.
    return np.array(res)

  ##------------------------------------------------------------------------
  # Interpolate with linear splines in log-log space
  ##------------------------------------------------------------------------
  def interpolate2D(self,ne,te,idx,log=False,flog=True,debug=False,silent=False):
    '''
       Return interpolated values
       Assume density and temperature in usual units (not log!)
    '''
    ne,te = self.check_range(ne,te,flog=flog,silent=silent)

    res = self.logspl[idx](ne,te)
    if not log:
      res = np.power(10,res)
    return res

  ##------------------------------------------------------------------------
  # Plot coefficients from adf11 structure.
  ##------------------------------------------------------------------------
  def plot(self,i0=0,i1=1,ax=None,ls='-',leg=False,log=True,xr=None):
    #plt.title('')
    #plt.xlabel('')
    if ax == None:
      plt.figure()
      ax = plt.gca()

    for i in range(i0,i1):
      for j in range(self.dat.nz):
        if log:
          ax.semilogy(np.power(10.,self.dat.te),np.power(10.,self.dat.dat[i,:,j]),ls=ls,label=str(j))
        else:
          ax.plot(np.power(10.,self.dat.te),self.dat.dat[i,:,j],ls=ls,label=str(j))

    if leg == True:
      ax.legend(loc=4)
    if xr != None:
      ax.set_xlim(xr)
    plt.draw()
    plt.show(block=False)
    return ax

