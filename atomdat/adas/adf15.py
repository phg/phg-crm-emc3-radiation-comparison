# -*- coding: utf-8 -*-
#Python to load and view ADAS data SXB/PEC
import matplotlib.pyplot as plt
import numpy as np
import scipy.interpolate as spi
import matplotlib.gridspec as gspec

import utils.plot as pu
import utils.data as data
import utils.utils as utils

class ADF15():

  ##------------------------------------------------------------------------
  # Initialize
  ##------------------------------------------------------------------------
  def __init__(self):
    self.dat = data.data()
    return

  ##------------------------------------------------------------------------
  # Read adf15 (Photon emission coefficients) from ADAS (ADAS208).
  # 
  # input:
  #  filename  - Path and filename
  #  debug     - Triggers optional terminal output
  #
  # output:
  #  adf15 structure
  ##------------------------------------------------------------------------
  def read(self,fn,pn='C:\\Users\\flr\\rzgshare\\work\\W7X\\python\\data\\atomdat\\adas',v=0,debug=False):
    fn = pn+fn
    if debug:
      print('Read %s' %(fn))
    fp = open(fn,'r')
    line=[]
    line = fp.readline().split()

    self.dat.nlines=int(line[0])
    if debug: print('No. of lines  = %i' %self.dat.nlines)
    self.dat.species=(line[1]+line[2]+line[3])[1:]

    if v == 1:
      for i in range(5):
        line = fp.readline()
        if debug: print(i, line)

    wvl=[]
    wvlunit=[]
    ndens=[]
    ntemp=[]
    filmem=[]
    ttype=[]
    indim=[]
    isel=[]
    firstpass=True

  # Read header
    for i in range(int(self.dat.nlines)):
      line=[]
      line = fp.readline()
      lines = line.split()
      if debug:
        print(i, line)

      if v ==0:
        wvl.append(float(lines[0]))
        wvlunit.append(lines[1])
        ndens.append(int(lines[2]))
        ntemp.append(int(lines[3]))
        filmem.append(lines[6])
        ttype.append(lines[9])
        indim.append(lines[12])
        isel.append(int(lines[-1]))
      elif v == 1:
        wvl.append(float(lines[0]))
        wvlunit.append('A')
        ndens.append(int(lines[1]))
        ntemp.append(int(lines[2]))
        filmem.append(line)
        if 'EXCIT' in line:
          ttype.append('E')
        elif 'RECOM' in line:
          ttype.append('R')
        else:
          ttype.append('X')
#        ttype.append(line[9])
#        indim.append(line[12])
#        isel.append(int(line[-1]))
      if firstpass:
        dens=np.zeros((ndens[0],int(self.dat.nlines)))
        temp=np.zeros((ntemp[0],int(self.dat.nlines)))
        pec=np.zeros((ndens[0],ntemp[0],int(self.dat.nlines)))
        firstpass=False

    # Read density vector
      line=[]
      for j in range(int(ndens[i]/8)):
        line.extend(fp.readline().split())
      if ( ndens[i]%8 != 0 ):
        line.extend(fp.readline().split())
      dens[0:len(line),i]=[float(x) for x in line]

    # Read temperature vector
      line=[]
      for j in range(int(ntemp[i]/8)):
        line.extend(fp.readline().split())
      if ( ntemp[i]%8 != 0 ):
        line.extend(fp.readline().split())
      temp[0:len(line),i]=[float(x) for x in line]

    # Read PEC
      for j in range(ndens[i]):
        line=[]
        for k in range(int(ntemp[i]/8)):
          line.extend(fp.readline().split())
        if ( ntemp[i]%8 != 0 ):
          line.extend(fp.readline().split())
        pec[j,0:len(line),i]=[float(x) for x in line]
    self.dat.comment=fp.read()
    fp.close()

  # Create structure
    self.dat.filmem=filmem
    self.dat.ttype=ttype
    self.dat.indim=indim
    self.dat.isel=isel
    self.dat.wvl=np.asarray(wvl,dtype=np.float)
    self.dat.ttype=np.asarray(ttype)
    self.dat.wvlunit=np.asarray(wvlunit)
    self.dat.ndens=np.asarray(ndens)
    self.dat.ntemp=np.asarray(ntemp)
    self.dat.dens=np.asarray(dens.T,dtype=np.float)*1.E6
    self.dat.temp=np.asarray(temp.T,dtype=np.float)
    self.dat.ner=[min(self.dat.dens[0]),max(self.dat.dens[0])]
    self.dat.ter=[min(self.dat.temp[0]),max(self.dat.temp[0])]
    self.dat.pec=np.asarray(pec,dtype=np.float)/1.E6 #emis [cm-3] = PEC ne[cm-3] ni[cm-3] --> emis[m-3] = PEC ne [m-3] ni [m-3] / 1E-6
    if debug:
      print('Make splines')
    self.makespline()

    return self.dat

  ##------------------------------------------------------------------------
  ##------------------------------------------------------------------------
  def plott(self,nt,\
            xscl='log',yscl='log',zscl='log',\
            xr=None,yr=None,zr=None):
      import matplotlib as mp
      d = self.dat['dens'][nt,:]
      t = self.dat['temp'][nt,:]
      p = self.dat['pec'][:,:,nt]
      if zr is None:
        zr = [p.min(),p.max()]

      vmin,vmax = zr
      p = np.clip(p,zr[0],zr[1])
      
      if zscl == 'log':
        vmin,vmax = np.log10(zr)
        p = np.log10(np.clip(p,zr[0],zr[1]))
      plt.contourf(t,d,p)#,norm=norm)#,vmin=vmin,vmax=vmax)
      plt.xscale(xscl)
      plt.yscale(yscl)
      plt.ylim(yr)
      plt.xlim(xr)
      #plt.zlim(zr)
      plt.colorbar()
      #plt.show(block=False)
      return


  ##------------------------------------------------------------------------
  # Plot individual photon emission coefficients (PEC) from adf15 structure
  # from ADAS (ADAS208).
  # 
  # input:
  #  dat1      - adf15 structure
  #  dat2      - adf15 structure
  #  nt1       - adf15 structure
  #  nt2       - adf15 structure
  #  temp      - adf15 structure
  #  dens      - adf15 structure
  #  [ca]      - Canvas (MplWidget)
  #  [tr]      - adf15 structure
  #  [dr]      - adf15 structure
  #  [logx]    - Logarithmic plot (x-axis) for density
  #  [logy]    - Logarithmic plot (y-axis) for PEC
  #  [hold]    - Hold plot for overplotting
  #  [debug]   - Triggers optional output
  #
  # output: 
  #  adf15 structure
  ##------------------------------------------------------------------------
  
  def plot(self,dat1,dat2,nt1,nt2,temp,dens,tr=[0.,100.],dr=[1E13,1E20],\
                ca = None,\
                logx=True,logy=False,hold=False,debug=False):
    nt1=nt1-1
    nt2=nt2-1
    #ca = self.ca
    if ca == None:
      fig = plt.figure()
    else:
      fig = ca.fig
    if not(hold):
      fig.clf()
    gs = gspec.GridSpec(2, 2,
                         width_ratios=[1,1],
                        height_ratios=[1,1])
    ax1 = fig.add_subplot(gs[0,0])
    ax2 = fig.add_subplot(gs[0,1])
    ax3 = fig.add_subplot(gs[1,0])
    ax4 = fig.add_subplot(gs[1,1])

    tidx1   = np.abs(dat1['temp'][nt1,:] - temp).argmin()
    tr1idx1 = np.abs(dat1['temp'][nt1,:] - tr[0]).argmin()
    tr2idx1 = np.abs(dat1['temp'][nt1,:] - tr[1]).argmin()
    didx1   = np.abs(dat1['dens'][nt1,:] - dens).argmin()
    dr1idx1 = np.abs(dat1['dens'][nt1,:] - dr[0]).argmin()
    dr2idx1 = np.abs(dat1['dens'][nt1,:] - dr[1]).argmin()
    tidx2   = np.abs(dat2['temp'][nt2,:] - temp).argmin()
    tr1idx2 = np.abs(dat2['temp'][nt2,:] - tr[0]).argmin()
    tr2idx2 = np.abs(dat2['temp'][nt2,:] - tr[1]).argmin()
    didx2   = np.abs(dat2['dens'][nt2,:] - dens).argmin()
    dr1idx2 = np.abs(dat2['dens'][nt2,:] - dr[0]).argmin()
    dr2idx2 = np.abs(dat2['dens'][nt2,:] - dr[1]).argmin()

  # Define plot vectors
    t1    = dat1['temp'][nt1,tr1idx1:tr2idx1]
    d1    = dat1['dens'][nt1,dr1idx1:dr2idx1]
    pect1 = dat1['pec'][didx1,tr1idx1:tr2idx1,nt1]
    pecd1 = dat1['pec'][dr1idx1:dr2idx1,tidx1,nt1]
    t2    = dat2['temp'][nt2,tr1idx2:tr2idx2]
    d2    = dat2['dens'][nt2,dr1idx2:dr2idx2]
    pect2 = dat2['pec'][didx2,tr1idx2:tr2idx2,nt2]
    pecd2 = dat2['pec'][dr1idx2:dr2idx2,tidx2,nt2]

    print(d1)
    print(abs(d1-d2))
  # Interpolate on common grid if necessary
    ntt   = 25
    if min(abs(t1-t2)) != 0:
      maxt  = min(t1.max(),t2.max())
      mint  = max(t1.min(),t2.min())
      t     = (np.arange(ntt)*((maxt-mint)/ntt))+mint
      pectt1 = np.interp(t,t1,pect1)
      pectt2 = np.interp(t,t2,pect2)  
      if debug:
        print( mint, maxt, t )
    else:
      t = t1
      pectt1 = pect1
      pectt2 = pect2

    if min(abs(d1-d2)) != 0:
      maxd  = min(d1.max(),d2.max())
      mind  = max(d1.min(),d2.min())
      d     = (np.arange(ntt)*((maxd-mind)/ntt))+mind
      pecdd1 = np.interp(d,d1,pecd1)
      pecdd2 = np.interp(d,d2,pecd2)
      if debug:
        print( mind, maxd, d )
    else:
      d = d1
      pecdd1 = pecd1
      pecdd2 = pecd2

    # Define maximum counts and level distance
    intres = 30.
    rdval  = 1.
    mincts = 1.E-30#max(np.floor(dat['pec'][:,:,nt1].max()/rdval)*rdval,1e-20)
    mxcts  = 1.E-10#np.ceil (dat['pec'][:,:,nt1].max()/rdval)*rdval
    if debug: 
      print( "Mincts, Maxcts", mincts, mxcts )
    levels = np.arange(mincts,mxcts,mxcts/intres)

    # Clear figure if necessary
    #ax1.hold(hold)
    #ax2.hold(hold)
    #ax3.hold(hold)
    #ax4.hold(hold)

  # Plot PECs
    rdval=rdval/10
    ax1.plot(t1,pect1)
    ax2.plot(d1,pecd1)
    #ax1.hold(True)
    #ax2.hold(True)
    ax1.plot(t2,pect2)
    ax2.plot(d2,pecd2)
    #ax1.hold(hold)
    #ax2.hold(hold)

  # Plot ratios
    ax3.plot(t,pectt1/pectt2)
    ax4.plot(d,pecdd1/pecdd2)

    if logx:
      ax2.set_xscale('log')
      ax4.set_xscale('log')
    if logy:
      ax1.set_yscale('log')
      ax2.set_yscale('log')

  # Annotate plots
    tls=10
    fts=10
    ax1.set_title('@ n_e = %7.1e / %7.1e' %(dat1['dens'][nt1,didx1],dat2['dens'][nt2,didx2]),fontsize=tls)
    ax2.set_title('@ T_e = %.1f / %.1f' %(dat1['temp'][nt1,	tidx1],dat2['temp'][nt2,tidx2]),fontsize=tls)
    ax1.tick_params(axis='both', labelsize=fts)
    ax2.tick_params(axis='both', labelsize=fts)
    ax3.tick_params(axis='both', labelsize=fts)
    ax4.tick_params(axis='both', labelsize=fts)
    ax1.set_xlabel('Temperature[eV]',fontsize=fts)
    ax1.set_ylabel('PEC [Ph m^{-3}s^{-1}]',fontsize=fts)
    ax2.set_xlabel('Electron density [m^{-3}]',fontsize=fts)
    ax2.set_ylabel('PEC [Ph m^{-3}s^{-1}]',fontsize=fts)
    ax3.set_ylabel('PEC-Ratio 1/2',fontsize=fts)
    ax3.set_xlabel('Temperature[eV]',fontsize=fts)
    ax4.set_xlabel('Electron density [m^{-3}]',fontsize=fts)
    ax4.set_ylabel('PEC-Ratio 1/2',fontsize=fts)

    #fig.colorbar(ax1)
    if ca == None:
      plt.show()
    else:
      ca.show()
      ca.draw()
    return

  ##------------------------------------------------------------------------
  # Plot individual photon emission coefficients (PEC) from adf15 structure
  # from ADAS (ADAS208).
  # 
  # input:
  #  dat1      - adf15 structure
  #  dat2      - adf15 structure
  #  nt1       - adf15 structure
  #  nt2       - adf15 structure
  #  temp      - adf15 structure
  #  dens      - adf15 structure
  #  [ca]      - Canvas (MplWidget)
  #  [tr]      - adf15 structure
  #  [dr]      - adf15 structure
  #  [target]  - Target ratio (for plot)
  #  [logx]    - Logarithmic plot (x-axis) for density
  #  [logy]    - Logarithmic plot (y-axis) for temperature
  #  [logy]    - Logarithmic plot (Colorcode) for PEC
  #  [hold]    - Hold plot for overplotting
  #  [debug]   - Triggers optional output
  #
  # output: 
  #  adf15 structure
  ##------------------------------------------------------------------------
  
  def cratio(self,dat1,dat2,nt1,nt2,t,d,tr=[0.,100.],dr=[1E10,1E21],\
                 target=None,fig=None,intres=0.2,\
                 logx=True,logy=False,logz=False,debug=False):
    from importlib import reload
    import utils.utils as utils; reload(utils)
    reload(pu)
    from matplotlib import cm
    nt1=nt1-1
    nt2=nt2-1

    hold = False
    if fig is None:
      fig = plt.figure()
      gs = gspec.GridSpec(2,3,
                         width_ratios=[2,7,.5],
                        height_ratios=[4,1])
      gs.update(top=.90,bottom=.075,left=.075,right=.9,wspace=.14,hspace=.14)

      ax1 = fig.add_subplot(gs[0,0])
      ax2 = fig.add_subplot(gs[1,1])
      ax3 = fig.add_subplot(gs[0,1])
      ax4 = fig.add_subplot(gs[0:1,2], frameon=False)
    else:
      axs = fig.get_axes()
      ax1,ax2,ax3,ax4 = axs[0],axs[1],axs[2],axs[3]
      hold = True

    tidx1   = np.abs(dat1['temp'][nt1,:] - t).argmin()
    tr1idx1 = np.abs(dat1['temp'][nt1,:] - tr[0]).argmin()
    tr2idx1 = np.abs(dat1['temp'][nt1,:] - tr[1]).argmin()
    didx1   = np.abs(dat1['dens'][nt1,:] - d).argmin()
    dr1idx1 = np.abs(dat1['dens'][nt1,:] - dr[0]).argmin()
    dr2idx1 = np.abs(dat1['dens'][nt1,:] - dr[1]).argmin()
    tidx2   = np.abs(dat2['temp'][nt2,:] - t).argmin()
    tr1idx2 = np.abs(dat2['temp'][nt2,:] - tr[0]).argmin()
    tr2idx2 = np.abs(dat2['temp'][nt2,:] - tr[1]).argmin()
    didx2   = np.abs(dat2['dens'][nt2,:] - d).argmin()
    dr1idx2 = np.abs(dat2['dens'][nt2,:] - dr[0]).argmin()
    dr2idx2 = np.abs(dat2['dens'][nt2,:] - dr[1]).argmin()

  # Define plot vectors
    t1     = dat1['temp'][nt1,tr1idx1:tr2idx1]
    d1     = dat1['dens'][nt1,dr1idx1:dr2idx1]
    t2     = dat2['temp'][nt2,tr1idx2:tr2idx2]
    d2     = dat2['dens'][nt2,dr1idx2:dr2idx2]
    pec1   = dat1['pec'] [dr1idx1:dr2idx1,tr1idx1:tr2idx1,nt1]
    pect1  = dat1['pec'] [didx1,tr1idx1:tr2idx1,nt1]
    pecd1  = dat1['pec'] [dr1idx1:dr2idx1,tidx1,nt1]
    pec2   = dat2['pec'] [dr1idx2:dr2idx2,tr1idx2:tr2idx2,nt2]
    pect2  = dat2['pec'] [didx2,tr1idx2:tr2idx2,nt2]
    pecd2  = dat2['pec'] [dr1idx2:dr2idx2,tidx2,nt2]
    vdat1  = dat1['pec'] [didx1,tidx1,nt1]
    vdat2  = dat2['pec'] [didx2,tidx2,nt2]

  # Interpolate on common grid if necessary
    [tv,pect1,pect2] = utils.interp_common_grid(t1,t2,pect1,pect2,keepx1=True,debug=debug)
    ptdat = pect1/pect2
    [dv,pect1,pect2] = utils.interp_common_grid(d1,d2,pecd1,pecd2,keepx1=True,debug=debug)
    pddat = pecd1/pecd2
    [_,pect1,pect2]  = utils.interp_common_grid(d1,d2,pec1,pec2,t1,t2,keepx1=True,debug=debug)
    pcdat  = pec1/pec2
	#Replace interpolation values < 1e-20 for log10()
    pcdat  = pcdat.clip(1e-2)

    tidxr   = np.abs(tv - t).argmin()
    didxr   = np.abs(dv - d).argmin()
    vdat1   = pec1[didxr,tidxr]
    vdat2   = pec2[didxr,tidxr]
    rat     = np.log10(vdat1/vdat2)
    if logz:
      rat     = np.log10(rat)

    if debug:
      print('T,D-Index = %i, %i' %(tidxr,didxr))
      print('PEC1,2, Ratio = %.2e, %.2e, %.2e' %(vdat1,vdat2,rat))
	
  # Plot PECs & set axes to scale
    col = 'r'
    if hold:
      col = 'b'
    ax1.plot(ptdat,tv, col)
    ax1.plot((np.arange(tv.shape[0])*1.E-5)+0.9*target,tv, 'k--')
    ax1.plot((np.arange(tv.shape[0])*1.E-5)+1.0*target,tv, 'k')
    ax1.plot((np.arange(tv.shape[0])*1.E-5)+1.1*target,tv, 'k--')
    ax2.plot(dv,pddat, col)
    ax2.plot(dv,np.ones(dv.shape[0])*0.9*target, 'k--')
    ax2.plot(dv,np.ones(dv.shape[0])*1.0*target, 'k')
    ax2.plot(dv,np.ones(dv.shape[0])*1.1*target, 'k--')
    if not hold:
      cmn = pu.det_levels(pcdat,intres=intres,nlevel=10,debug=debug,log=logz)
      cc = ax3.contourf(dv,tv,pcdat.T,cmn.levels,cmap=cm.nipy_spectral)
      #cc1 = ax3.contour(dv,tv,pcdat.T,cmn.levels,cmap=cm.nipy_spectral)
      #ax3.clabel(cc1, inline=1, fontsize=10,fmt='%.2f',colors=('k'))
    else:
      cmn = pu.det_levels(pcdat,intres=intres,nlevel=10,debug=debug,log=logz)
      cc = ax3.contour(dv,tv,pcdat.T,cmn.levels,cmap=cm.binary)
      ax3.clabel(cc, inline=1, fontsize=10,fmt='%.2f',colors=('k'))
    levels = [0.9*target,target,target,1.1*target]
    col = ('k')
    lw = (1,)
    if hold:
      col = ('w')
      lw = (1,)
    ax3.contour(dv,tv,pcdat.T,levels[1:2],\
                colors=col,linewidths=(2.5,),\
                inline=1,fontsize=9.)
    ax3.contour(dv,tv,pcdat.T,[levels[0],levels[3]],\
                colors=col,linestyles='dashed',linewidths=lw,\
                inline=1,fontsize=9.)
    ax3.plot(d,t,'k*',markersize=7.5,markeredgecolor='k')

    if logx:
      ax2.set_xscale('log')
      ax3.set_xscale('log')
    if logy:
      ax1.set_yscale('log')
      ax3.set_yscale('log')

  # Match axes properly
    ax1.set_ylim(ax3.get_ylim())
    ax2.set_xlim(ax3.get_xlim())
    ax3.set_xticklabels('')
    ax3.set_yticklabels('')
    ax4.set_xticklabels('')
    ax4.set_yticklabels('')
    ax4.set_yticks([])
    ax4.set_xticks([])
    ax4.get_xaxis().set_visible(False)
    ax4.get_yaxis().set_visible(False)

  # Annotate plots
    tls=12
    fts=10
    ax3.set_title('PEC-Ratio 1/2 - PEC=%.1e, %.1e & R=%.2f \n @ %.2em^{-3} & %.1feV @ W=%sA, %sA'\
                  %(vdat1,vdat2,rat,d,t,dat1['wvl'][nt1],dat2['wvl'][nt2]), fontsize=tls)
    ax1.tick_params(axis='both', labelsize=fts)
    ax2.tick_params(axis='both', labelsize=fts)
    ax3.tick_params(axis='both', labelsize=fts)
    ax4.tick_params(axis='both', labelsize=fts)
    ax2.set_xlabel('Electron density [m^{-3}]',fontsize=fts)
    ax1.set_ylabel('Temperature[eV]',fontsize=fts)
    if not hold:
      CBCC = fig.colorbar(cc,ax=ax4, ticks=cmn.levelsnum,fraction=1.)
      #CBCC.ax.set_ticks(intres)
      CBCC.ax.set_yticklabels(cmn.levelsstr,fontsize=fts)

    plt.draw()
    #plt.show(block=False)
    return fig

  ##------------------------------------------------------------------------
  # Plot individual photon emission coefficients (PEC) from adf15 structure
  # from ADAS (ADAS208).
  # 
  # input:
  #  dat1      - adf15 structure
  #  dat2      - adf15 structure
  #  nt1       - adf15 structure
  #  nt2       - adf15 structure
  #  t         - Electron temperature of interest
  #  d         - Electron density of interest
  #  [ca]      - Canvas (MplWidget)
  #  [tr]      - adf15 structure
  #  [dr]      - adf15 structure
  #  [logx]    - Logarithmic plot (x-axis) for density
  #  [logy]    - Logarithmic plot (y-axis) for temperature
  #  [logy]    - Logarithmic plot (Colorcode) for PEC
  #  [hold]    - Hold plot for overplotting
  #  [debug]   - Triggers optional output
  #
  # output: 
  #  adf15 structure
  ##------------------------------------------------------------------------
  
  def contour(self,dat,nt,t,d,tr=[0.,100.],dr=[1E18,1E20],\
                 target=None,ca = None,\
                 logx=True,logy=False,logz=False,hold=False,debug=False):
    nt=nt-1
    if ca == None:
      fig = plt.figure()
    else:
      fig = ca.fig
    fig.clf()
    gs = gspec.GridSpec(2,3,
                         width_ratios=[2,7,.5],
                        height_ratios=[4,1])
    gs.update(top=.90,bottom=.075,left=.075,right=.9,wspace=.14,hspace=.14)

    ax1 = fig.add_subplot(gs[0,0])
    ax2 = fig.add_subplot(gs[1,1])
    ax3 = fig.add_subplot(gs[0,1])
    ax4 = fig.add_subplot(gs[0:1,2], frameon=False)
    #gs.tight_layout(fig)

  # Find ROIs of data set
    tidx   = np.abs(dat['temp'][nt,:] - t).argmin()
    tr1idx = np.abs(dat['temp'][nt,:] - tr[0]).argmin()
    tr2idx = np.abs(dat['temp'][nt,:] - tr[1]).argmin()
    didx   = np.abs(dat['dens'][nt,:] - d).argmin()
    dr1idx = np.abs(dat['dens'][nt,:] - dr[0]).argmin()
    dr2idx = np.abs(dat['dens'][nt,:] - dr[1]).argmin()
    if debug:
      print('tidx, tr1idx,tr2idx = ', tidx,tr1idx,tr2idx)
      print('didx, dr1idx,dr2idx = ', didx,dr1idx,dr2idx)
      print('Shape(dat[pec],dat[temp],dat[dens]) = ', dat['pec'].shape,dat['temp'].shape,dat['dens'].shape)
      print( "" )

  # Define plot vectors
    tv     = dat['temp'][nt,tr1idx:tr2idx]
    dv     = dat['dens'][nt,dr1idx:dr2idx]
    pcdat  = dat['pec'][dr1idx:dr2idx,tr1idx:tr2idx,nt]
    ptdat  = dat['pec'][didx,tr1idx:tr2idx,nt]
    pddat  = dat['pec'][dr1idx:dr2idx,tidx,nt]
    vdat   = dat['pec'][didx,tidx,nt]

    # Define maximum counts and level distance
    pu.det_levels(pcdat,debug=debug,log=logz)

  # Plot PECs
    ax1.plot(ptdat,tv)
    ax1.plot((np.arange(tv.shape[0])*1E-50)+0.9*target,tv, 'k--')
    ax1.plot((np.arange(tv.shape[0])*1E-50)+1.0*target,tv, 'k')
    ax1.plot((np.arange(tv.shape[0])*1E-50)+1.1*target,tv, 'k--')
    ax2.plot(dv,pddat)
    ax2.plot(dv,np.ones(dv.shape[0])*0.9*target, 'k--')
    ax2.plot(dv,np.ones(dv.shape[0])*1.0*target, 'k')
    ax2.plot(dv,np.ones(dv.shape[0])*1.1*target, 'k--')

  # Set axes to scale
    if logz:
      cc = ax3.contourf(dv,tv,pcdat.T,cmn.levels,cmap=pl.cm.spectral,norm=mp.colors.LogNorm())
      ax3.hold(True)
      ax3.plot(d,t,'k*',markersize=7.5,markeredgecolor='k')
      ax1.set_xscale('log')
      ax2.set_yscale('log')
    else:
      cc = ax3.contourf(dv,tv,pcdat.T,cmn.levels,cmap=pl.cm.spectral)
      ax3.hold(True)
      ax3.plot(d,t,'k*',markersize=7.5,markeredgecolor='k')

    if logx:
      ax2.set_xscale('log')
      ax3.set_xscale('log')
    if logy:
      ax1.set_yscale('log')
      ax3.set_yscale('log')

  # Match axes properly
    ax1.set_ylim(ax3.get_ylim())
    ax2.set_xlim(ax3.get_xlim())
    ax3.set_xticklabels('')
    ax3.set_yticklabels('')
    ax4.set_xticklabels('')
    ax4.set_yticklabels('')
    ax4.set_yticks([])
    ax4.set_xticks([])
    ax4.get_xaxis().set_visible(False)
    ax4.get_yaxis().set_visible(False)

  # Annotate plots
    tls=12
    fts=10
    ax3.set_title('PEC - T1 -- PEC= %.1e @ %.2em^{-3}, %.1feV, %sA'\
                  %(vdat,d,t,dat['wvl'][nt]),fontsize=tls)
    ax1.tick_params(axis='both', labelsize=fts)
    ax2.tick_params(axis='both', labelsize=fts)
    ax3.tick_params(axis='both', labelsize=fts)
    ax4.tick_params(axis='both', labelsize=fts)
    ax2.set_xlabel('Electron density [m^{-3}]',fontsize=fts)
    ax1.set_ylabel('Temperature[eV]',fontsize=fts)
    CBCC = fig.colorbar(cc,ax=ax4, ticks=cmn.levelsnum,fraction=1.)
    CBCC.ax.set_yticklabels(cmn.levelsstr,fontsize=fts)

    if ca == None:
      plt.show()
    else:
      ca.show()
      ca.draw()
    return

  ##------------------------------------------------------------------------
  def print_line_index(self,wr):
    '''
       Print indices of lines in range wr
    '''
    idx = np.where((self.dat['wvl'] > wr[0]) & (self.dat['wvl'] < wr[1]))[0]
    for item in idx:
      print('%03i  - %.3f' %(item,self.dat['wvl'][item]))
    return idx

  ##------------------------------------------------------------------------
  # Make splines for interpolation
  ##------------------------------------------------------------------------
  def makespline(self,degree=1):
    '''
       Get Spline function for interpolation on log-log grid
       degree = 1 : Degree of Spline
       Linear interpolation on log-log grid is recommended
       No oscillations at grid boudary!
    '''
  # Generate grid
  # Interpolation on log-log grid is recommended (Puetti)
    te=np.log10(self.dat.temp[0])
    ne=np.log10(self.dat.dens[0])
    dat=np.log10(self.dat.pec)

    #Generate Interpolation Spline (private)
    self.logspl = []
    for i in range(self.dat.nlines):
      self.logspl.append(spi.RectBivariateSpline(ne, te, dat[:,:,i], kx=degree, ky=degree))
    return


  ##------------------------------------------------------------------------
  # Interpolate with linear splines in log-log space
  ##------------------------------------------------------------------------
  def interpolate(self,ne,te,idx,log=False,debug=False,silent=False):
    '''
       Return interpolated PEC | S/XB
    '''

  # Convert into Log10
    ne  = np.log10(ne)
    ner = np.log10(self.dat.ner)
    te = np.log10(te)
    ter = np.log10(self.dat.ter)
    #print(ne.shape,te.shape)

    if not(     (ner[0] < ne.min() and ne.max() < ner[1]) \
           and  (ter[0] < te.min() and te.max() < ter[1]) or silent):
      print( 'Requested ne range: %.2e - %.2e' %( np.power(10,ne.min()),np.power(10,ne.max()) ) )
      print( 'Available ne range: %.2e - %.2e' %( np.power(10,ner[0]),np.power(10,ner[1]) ) )
      print( 'Requested Te range: %.2e - %.2e' %( np.power(10,te.min()),np.power(10,te.max()) ) )
      print( 'Available Te range: %.2e - %.2e' %( np.power(10,ter[0]),np.power(10,ter[1]) ) )
      print( '  !! Warning - Out of interpolation domain !!' )

      np.clip(te,ter[0],ter[1],out=te)
      np.clip(ne,ner[0],ner[1],out=ne)

  # Test shapes
    if ne.shape[0] != te.shape[0]:
      print('## ERROR ## - Shapes of ne & Te do not match!')
      return -1

    res = np.zeros(len(ne))
    for i in range(len(ne)):
      #try:
        if log:
          res[i] = self.logspl[idx](ne[i],te[i])
        else:
          res[i] = np.power(10,self.logspl[idx](ne[i],te[i]))
      #except Exception:
      #  res[i] = 0.
    return np.array(res)
