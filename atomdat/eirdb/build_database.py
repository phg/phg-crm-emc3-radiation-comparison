import numpy as np
import json

#HYDHEL
#H1: sigma(E)
#H2: <sigma v>(T)
#H3: <sigma v>(E,T)
#H8: <sigma v E>(T)


#AMJUEL
#H1: sigma(E)
#H2: <sigma v>(T)
#H3: <sigma v>(E,T)
#H4: <sigma v>(n,T)
#H5: not in use
#H6: <sigma v p>(E,T)
#H7: <sigma v>(E,T)
#H8: <sigma v E>(T)
#H9: <sigma v E>(E,T)
#H10: <sigma v E>(E,T)
#H11: ?

def extract_data_block(s,lower=True,repl=[]):
    for af in ['An analytic formula is given in the text.','See text for analytic formulas.']:
        if af in s:return None
    
    if (s.count(r'\begin{verbatim}')!=1) or (s.count(r'\end{verbatim}')!=1):
        return None
    block=s.split(r'\begin{verbatim}')[1].split(r'\end{verbatim}')[0]
    if lower:block=block.lower()
    for r1,r2 in repl:block=block.replace(r1,r2)
    return block
    

def read_coefficients1D(block,varname='a',fmt='%.12e'):
    D=np.zeros(10)*np.nan
    for i in range(10):
       sc=('%s%i ' % (varname,i))
       if block.count(sc)==1:
           D[i]=float(block.split(sc)[1].split()[0])
    if ~np.isnan(D[9]):
        print('unexpected data block length')
        return []
    
    D=D[0:9]
    if np.any(np.isnan(D)):
        print('unrecognized data:')
        print(block)
        return []
    
    return D.tolist()
    

def read_coefficients2D(block,fmt='%.12e'):
    T=block.replace('t index','t-index:').split('t-index:')[1:]
    s='\n'
    for i in range(9):
     for j in range(3):
       l=T[j].split('\n')[i+1]
       s+=','.join(l.split()[1:])+','
     s+='\n'

    return np.fromstring(s.replace('d','e'),count=9*9,sep=',',dtype=float).reshape(9,9).tolist()


vnfdEnergy=[['emin','Emin',1.0,None],['emax','Emax',1.0,None],['eth','Eth',1.0,None]]
vnfdTn=[['t1min=','Tmin',1.0,0.1],['t1max=','Tmax',1.0,1e4],['n2min=','nmin',1e6,1e10],['n2max=','nmax',1e6,1e25]]


    
def read_variables(block,reac,vnfd):    
    for v,n,f,d in vnfd:
       if not (d is None):reac[n]=d
       if block.count(v)==1:
           x=float(block.split(v)[1].split()[0])*f
           reac[n]=x
    



database={'reactions':{}}


hh=open('AMdata/hydhel.tex').read()

report='HYDHEL'

headers={}
for h in hh.split('\section{H.')[1:]:
    headers[int(h[0:2])]=h

for ih in [1,2,8]:
  for S in headers[ih].split('\subsection{'):
    ll=S.split('\r\n')
    if ll[1].startswith('Reaction'):
        nam=ll[1].split()[1]
        latex=ll[1].split('$')[1]
        
        reaction={'report':report,'header':ih,'name':nam,'latex':latex}
        
        reaction['symbol']=    {1:r'$\sigma$',2:r'$\langle\sigma\cdot v\rangle$',8:r'$\langle\sigma\cdot v\cdot E\rangle$'}[ih]
        reaction['unit']=      {1:'m$^2$',    2:'m$^3$ s$^{-1}$',                8:'m$^3$ eV s$^{-1}$'}[ih]
        reaction['parameters']={1:'E',        2:'E,T',                           8:'T'}[ih]
        reaction['factor']=    {1:1e-4,       2:1e-6,                            8:1e-6}[ih]
        
        block=extract_data_block(S,repl=[[',',' ']])
        if not (block is None):
            reaction['coefficients']=read_coefficients1D(block,varname={1:'a',2:'b',8:'h'}[ih])
            read_variables(block,reaction,vnfdEnergy)
            
        database['reactions']['%s,%i,%s' % (report,ih,nam.replace('.','_'))]=reaction # the symbol . is used by json/fson to address a child element

for ih in [3]:
  for S in headers[ih].split('\subsection{'):
    ll=S.split('\r\n')
    if ll[1].startswith('Reaction'):
        nam=ll[1].split()[1]
        latex=ll[1].split('$')[1]

    reaction={'report':report,'header':ih,'name':nam,'latex':latex,'symbol':r'$\langle\sigma\cdot v\rangle$','unit':'m$^3$ s$^{-1}$','parameters':'E,T','factor':1.0e-6}

    block=extract_data_block(S,repl=[[',',' ']])
    if not (block is None):
            reaction['coefficients']=read_coefficients2D(block)
            read_variables(block,reaction,vnfdEnergy)
            
    database['reactions']['%s,%i,%s' % (report,ih,nam.replace('.','_'))]=reaction # the symbol . is used by json/fson to address a child element

report='AMJUEL'

aj=open('AMdata/amjuel.tex').read()


headers=aj.split('\section{H.')[1:]

for ih in [3,4,10,12]:
  header=headers[ih]
  for r in header.split('\subsection{')[1:]:
    l3=(' '.join(r.split('\n')[0:3])).strip()
    nam=l3.split()[1]
    print(nam)
    latex=l3.split('$')[1].strip()
    reaction={'report':report,'header':ih,'name':nam,'latex':latex}
    
    reaction['symbol']=    {3:r'$\langle\sigma\cdot v\rangle$',4:r'$\langle\sigma\cdot v\rangle$', 10:r'$\langle\sigma\cdot v\cdot E\rangle$',12:'ratio'}[ih]
    reaction['unit']=      {3:'m$^3$ s$^{-1}$',              4:'m$^3$ s$^{-1}$',                 10:'m$^3$ eV s$^{-1}$', 12:''}[ih]
    reaction['parameters']={3:'E,T',                         4:'n,T',                            10:'n,T',12:'n,T'}[ih]
    reaction['factor']=    {3:1e-6,                        4:1e-6,                               10:1e-6,12:1}[ih]
    reaction['factor_n']=  {3:1.0,                         4:1e-14,                              10:1e-14, 12:1e-14}[ih]
    
    block=''
    data=''
    block=extract_data_block(r)
    
    if not (block is None):
        reaction['coefficients']=read_coefficients2D(block)
        read_variables(block,reaction,vnfdTn)
    
    database['reactions']['%s,%i,%s' % (report,ih,nam.replace('.','_'))]=reaction # the symbol . is used by json/fson to address a child element



for ih in [2,8,11]:
  for S in headers[ih].split('\subsection{')[1:]:
    ll=S.split('\n')
    if ll[1].startswith('Reaction'):
        nam=ll[1].split()[1]
        latex=(ll[1]+ll[2]).split('$')[1]
        reaction={'report':report,'header':ih,'name':nam,'latex':latex,'parameters':'T'}

        reaction['symbol']=    {2:r'$\langle\sigma\cdot v\rangle$',8:r'$\langle\sigma\cdot v\cdot E\rangle$',11:r'$\langle E\rangle$'}[ih]
        reaction['unit']=      {2:'m$^3$ s$^{-1}$',                8:'m$^3$ eV s$^{-1}$', 11:'eV'}[ih]
        reaction['factor']=1 if ih==11 else 1e-6

        block=extract_data_block(S,repl=[[',',' '],['d','e']])
        if not (block is None):
            reaction['coefficients']=read_coefficients1D(block,varname={1:'a',2:'b',8:'h',11:'k'}[ih])
            read_variables(block,reaction,vnfdEnergy)
            
        database['reactions']['%s,%i,%s' % (report,ih,nam.replace('.','_'))]=reaction # the symbol . is used by json/fson to address a child element


json.dump(database,open('amjuel_hydhel_database.json','w'), indent=4, sort_keys=True)

