 
program eirene_database

    use fson
    implicit none
    type(fson_value), pointer :: root,reaction
    real*8, allocatable :: c(:,:)
    real*8 :: T,Tmin,Tmax,n=1.0e19,f,fy,factor,factor_n
    real*8 :: logx,logy
    integer :: ix,iy,it,Nt=30
    
    root => fson_parse("../amjuel_hydhel_database.json")
     
    call fson_get(root, "reactions.AMJUEL,4,2_1_5", reaction)

    call fson_get(reaction, "Tmin", Tmin)
    call fson_get(reaction, "Tmax", Tmax)      
    call fson_get(reaction, "factor", factor)
    call fson_get(reaction, "factor_n", factor_n)      
    
    call fson_get(reaction, "coefficients", c)
    
    write(*,fmt='(A,E10.2)') 'Density:',n
    write(*,fmt='(A)') 'Temperature [eV], Rate [m^3/s]'
    
    do it=0,Nt-1
     T=10**(log10(Tmin)+it*(log10(Tmax)-log10(Tmin))/(Nt-1.d0))
     
     logx=log(n*factor_n)
     logy=log(T)
     
     f=0.d0
     do ix=1,9
        fy=0.d0
        do iy=1,9 
          fy=fy+c(iy,ix)*logy**(iy-1)
        enddo !iy     
        f=f+fy*logx**(ix-1)
     enddo !ix
     f=exp(f)*factor
     
     write(*,fmt='(E12.4,E12.4)') T,f
    enddo !it


end program eirene_database
