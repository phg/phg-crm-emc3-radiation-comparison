import os
from copy import deepcopy
import numpy as np
from importlib import reload
import matplotlib.pylab as plt
import cnv ; reload(cnv); cv = cnv.CNV()

spath = '/toks/work/flr/SOLPS/solps5.0/src/Braams/b2/runs/HDM/28903-2400-divIId-D+N-D-096x036-TEST/testcase-2-DONLY-BC-RERUN/'
os.chdir(spath)
#from utils import data; dat = data.data(); cv.load(dat,'cnv.pik')
cv.read(); cv.prep(); cv.save()

dpath = '/afs/ipp/u/flr/python/amd/eirdb/'
#os.chdir(dpath)
import amd.eirdb.reactions_database as rdb; reload(rdb)
rdb = rdb.REACTIONS_DATABASE()
rdb.open_database(dpath+'amjuel_hydhel_database.json')


R = [\
    #H(3)/H
     'AMJUEL,12,2_1_5a',\
    #H(3)/H+
     'AMJUEL,12,2_1_8a',\
    #H(3)/H2
     'AMJUEL,12,2_2_5a',\
    #H(3)/H2+
     'AMJUEL,12,2_2_14a',\
    #H2+/H2
     'AMJUEL,12,2_0c',\
    #H(3)/H-
     'AMJUEL,12,7_2a',\
    #H-/H2
     'AMJUEL,11,7_0a',\
     #H(3)/Hll3+
     'AMJUEL,12,2_2_15a',\
     #H3+/H2/H2+/ne
     'AMJUEL,11,4_0a']
FAC = 4.410E7

#print(rdb.db['reactions'].keys())

rd = []
for item in R:
   rdb.select_reaction(item)
   rd.append(deepcopy(rdb))

nt = 200
te = np.linspace(0.2,10.,nt)
ne = np.ones(nt)*1E19
ni = np.ones(nt)*1E19
nh = np.ones(nt)*1E18
nh2 = np.ones(nt)*1E18
a0 = 4.410E7

nr = 7
rates = np.zeros((nr,nt))
for j in range(nr):
    rates[j,:] = rd[j].reaction(ne,te)
    #print(rates[j,:])

c1 = a0*rates[0,:]*nh
c2 = a0*rates[1,:]*ni
c3 = a0*rates[2,:]*nh2
c4 = a0*rates[3,:]*rates[4,:]*nh2
c5 = a0*rates[5,:]*rates[6,:]*nh2
ct = c1+c2+c3+c4+c5

fig,axs = plt.subplots(2,1,sharex=True)
axs = axs.flatten()
ax = axs[0]
ax.plot(te,ct)
ax = axs[1]
ax.plot(te,c1/ct,label='H')
ax.plot(te,c2/ct,label='H+')
ax.plot(te,c3/ct,label='H2')
ax.plot(te,c4/ct,label='H2+')
ax.plot(te,c5/ct,label='H2-')
ax.legend()
ax.set_yscale('log')
ax.set_ylim((1E-3,1E0))
plt.show(block=False)
