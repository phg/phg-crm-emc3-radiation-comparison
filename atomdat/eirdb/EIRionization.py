#Calculate ionization and recombination sources separately with AMJUEL rates
import os
from copy import deepcopy
import numpy as np
from importlib import reload
import matplotlib.pylab as plt
import cnv ; reload(cnv); cv = cnv.CNV()

spath = '/toks/work/flr/SOLPS/solps5.0/src/Braams/b2/runs/HDM/28903-2400-divIId-D+N-D-096x036-TEST/testcase-2-DONLY-BC-RERUN/'
os.chdir(spath)
#from utils import data; dat = data.data(); cv.load(dat,'cnv.pik')
cv.read(); cv.prep(); cv.save()

dpath = '/afs/ipp/u/flr/python/amd/eirdb/'
#os.chdir(dpath)
import amd.eirdb.reactions_database as rdb; reload(rdb)
rdb = rdb.REACTIONS_DATABASE()
rdb.open_database(dpath+'amjuel_hydhel_database.json')


R = [\
    #Sion_p
     'AMJUEL,4,2_1_5',\
    #Sion_E
     'AMJUEL,10,2_1_5',\
    #Srec_p
     'AMJUEL,4,2_1_8',\
    #Srec_E
     'AMJUEL,10,2_1_8',\
    #SCX
     'AMJUEL,3,3_1_8',\
    ]

#print(rdb.db['reactions'].keys())

rd = []
for item in R:
   rdb.select_reaction(item)
   rd.append(deepcopy(rdb))

nt = 200
nref = 1E20
te = np.linspace(0.1,10.,nt)
ne = np.ones(nt)*nref
ni = np.ones(nt)*nref
nh = np.ones(nt)*nref/10.
nh2 = np.ones(nt)*nref/10.

nr = 5
rates = np.zeros((nr,nt))
for j in range(nr):
    rates[j,:] = rd[j].reaction(ne,te,te)
    #print(rates[j,:])

sip = rates[0,:]*nh*ne
siE = rates[1,:]*nh*ne
srp = rates[2,:]*ni*ne
srE = rates[3,:]*ni*ne
sc  = rates[4,:]*nh*ne

fig,axs = plt.subplots(2,1,sharex=True)
axs = axs.flatten()

yl, yh = 1E20, 1E27
ax = axs[0]
ax.plot(te,sip,'r',label='S$_{ion,p}$')
ax.plot(te,srp,'b',label='S$_{rec,p}$')
tax = ax.twinx()
tax.plot(te,siE,'r--',label='S$_{ion,E}$')
tax.plot(te,srE,'b--',label='S$_{rec,E}$')
ax.legend()
ax.set_ylabel('Particle Source/Sink',labelpad=1)
tax.set_ylabel('Energy Source/Sink',labelpad=1)
ax.set_xlabel('T$_e$ [eV]',labelpad=10)
ax.set_xscale('log')
ax.set_yscale('log')
ax.set_ylim((yl,yh))
tax.set_yscale('log')
tax.set_ylim((yl,yh))
ax.text(.1,1E21,str('n$_{e,i}$ = %.2e \n n$_{H,H2}$ = %.2e' %(nref,nref/10.)))

yl, yh = 1E22, 1E27
ax = axs[1]
ax.plot(te,sc,'r',label='S$_{CX,p}$')
tax = ax.twinx()
tax.plot(te,1.5*sc*te,'r--',label='S$_{CX,E}$ - $\Delta$ E = 1.5Te')
tax.plot(te,0.15*sc*te,'r-.',label='S$_{CX,E}$ - $\Delta$ E = 0.15Te')
tax.legend()
ax.set_ylabel('Particle Source/Sink',labelpad=1)
ax.set_xlabel('E$_{lab}$ [eV]',labelpad=2)
tax.set_ylabel('Energy Source/Sink',labelpad=3)
ax.set_yscale('log')
ax.set_ylim((yl,yh))
tax.set_yscale('log')
tax.set_ylim((yl,yh))
plt.show(block=False)

te = np.clip(cv.case.pls.te.dat[1:-1,1:-1].flatten()/1.6602E-19,0.1,1E3)
ne = cv.case.pls.ne.dat[1:-1,1:-1].flatten()
ni = cv.case.pls.na.dat[1:-1,1:-1,1].flatten()
nh = cv.case.eir.da.dat[:,:,0].flatten()
nh2 = cv.case.eir.dm.dat[:,:].flatten()
nt = te.shape[0]

nr = 5
rates = np.zeros((nr,nt))
for j in range(nr):
    rates[j,:] = rd[j].reaction(ne,te,te)
    #print(rates[j,:])

sip = (rates[0,:]*nh*ne).reshape(96,36)
siE = (rates[1,:]*nh*ne).reshape(96,36)
srp = (rates[2,:]*ni*ne).reshape(96,36)
srE = (rates[3,:]*ni*ne).reshape(96,36)
scx = (rates[4,:]*nh*ne).reshape(96,36)

import qty; reload(qty)
tmp  = qty.QTY(cv.case,'eir.dm')
tmp.dat = sip
tmp.qty = 'SIP'
tmp.plot_2D(logt=True,zr=[1E20,1E27])

tmp.dat = srp
tmp.qty = 'SRP'
tmp.plot_2D(logt=True,zr=[1E20,1E27])

tmp.dat = scx
tmp.qty = 'SCX'
tmp.plot_2D(logt=True,zr=[1E20,1E27])
