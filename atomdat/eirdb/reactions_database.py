import numpy as np
import matplotlib.pyplot as plt
import sys
import json


class REACTIONS_DATABASE():
   def __init__(self,fn=None):
      self.db=None
      self.c=None
      self.latex=''
      self.verbose=True
      self.jreaction=None
      if not (fn is None):self.open_database(fn)
      self.minT=None;self.maxT=None


   def open_database(self,fn):
       self.db=json.load(open(fn))

          
   def select_reaction(self,rhn=None,report=None,header=None,name=None):
       if self.db is None: raise Exception('You need to call open_database first.')
       if (rhn is None):rhn='%s,%i,%s' % (report,int(header),name)
       
       rhn=rhn.replace('.','_') # the symbol . is used by json/fson to address a child element

       self.jreaction=self.db['reactions'][rhn]
       
       self.c=np.array(self.jreaction['coefficients'])
       
       for n in ['Tmin','Tmax','nmin','nmax','Emin','Emax','factor_n','factor','latex','symbol','unit','parameters','name','header','report']:
         setattr(self,n,self.jreaction[n] if n in list(self.jreaction.keys()) else None)
       
       return True

 
   def reaction(self,n=None,T=None,E=None):       
      if self.c is None: raise Exception('You need to call select reaction first.')
      params=self.parameters #shorter notation
      if ('n' in params) and (n is None): raise Exception('You need to specify a density.')
      if ('T' in params) and (T is None): raise Exception('You need to specify a Temperature.')
      if ('E' in params) and (E is None): raise Exception('You need to specify an energy.')
  
      #validity checks
      if 'E' in params:
        E=np.array(E)*1.0
        if not (self.Emin is None): E[E<self.Emin]=np.nan
        if not (self.Emax is None): E[E>self.Emax]=np.nan      
        
      if 'T' in params:
        T=np.array(T)*1.0
        if not (self.Tmin is None): T[T<self.Tmin]=np.nan
        if not (self.Tmax is None): T[T>self.Tmax]=np.nan      
        
      if 'n' in params:
        n=np.array(n)*1.0
        if not (self.nmin is None): n[n<self.nmin]=np.nan
        if not (self.nmax is None): n[n>self.nmax]=np.nan      
      
      if params.count(',')==0: #single-polynomial fits
         if params=='E':
            logx=np.log(E)
         elif params=='T':
            logx=np.log(T)
         else: 
            raise Exception('Parameter dependence not implemented.')  
         
         f=logx*0.0 #create an empty array of the same size than x
         for i in range(9):f+=self.c[i]*logx**i
         return np.exp(f)*self.factor     
      

      elif params.count(',')==1:#double-polynomial fits
          if params=='E,T':
              logx=np.log(E);logy=np.log(T)
          if params=='n,T':
              logx=np.log(n*self.factor_n);logy=np.log(T)
              
          f=logx*0.0 #create an empty array of the same size than x
          for ix in range(9):
            fy=logy*0.0
            for iy in range(9): 
               fy+=self.c[iy,ix]*logy**iy
            f+=fy*logx**ix

          return np.exp(f)*self.factor
          
      else: raise Exception('Only one reaction with single- or double polynomial fits are implemented so far.')

   def plot(self):
     nt = 1000
     ne = np.zeros((0,nt))
     te = np.zeros((0,nt))
     nl = [1E18,1E19,1E20,1E21]
     for i in nl:
       ne = np.vstack((ne,np.ones(nt)*i))
       te = np.vstack((te,np.linspace(0.1,1E2,1000)))
     E = te
     rates = self.reaction(ne,te,E)
     for i in range(4):
       plt.plot(te[i,:],rates[i,:],label=str(nl[i]))
     plt.legend()
     plt.xlabel('Te [eV]')
     plt.ylabel('Rate')
     plt.show(block=False)
     return

if __name__ == "__main__": 
    import argparse
    parser=argparse.ArgumentParser()
    parser.add_argument("-r","--reaction",default='AMJUEL,4,2.2.9;AMJUEL,4,2.2.5;AMJUEL,4,2.2.10;AMJUEL,4,2.2.14;AMJUEL,4,2.2.12;AMJUEL,4,2.1.5;HYDHEL,3,3.1.8',type=str)#AMJUEL,4,2.1.5,red;AMJUEL,10,2.1.8,blue
    parser.add_argument("-c","--caption",default='#reaction_name#',type=str)
    parser.add_argument("-csv","--csvfiles",default='',type=str,help='Example: AMJUEL_H10_2.1.5.csv,1e-6,red,--')
    parser.add_argument("-ne","--density",default=1e19,type=float)
    parser.add_argument("-E0","--energy_test_particle",default=1.0,type=float)
    parser.add_argument("-xmin",default=1e0,type=float)
    parser.add_argument("-xmax",default=1e3,type=float)
    parser.add_argument("-Nx",default=1000,type=float)
    parser.add_argument("-ymin",default=1e-16,type=float)
    parser.add_argument("-ymax",default=2e-13,type=float)
    parser.add_argument("-t","--title",default='cf. Stangeby Fig. 1.25',type=str)
    parser.add_argument("-o","--output",default='',type=str)
    args=parser.parse_args()

    fig,ax=plt.subplots(figsize=(12,10))
    
    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.set_xlim(args.xmin,args.xmax)
    ax.set_ylim(args.ymin,args.ymax)
    ax.set_title(args.title)

    EorT=10**np.linspace(np.log10(args.xmin),np.log10(args.xmax),args.Nx)
    E0=EorT*0.0+args.energy_test_particle # make an array with the same size as EorT of constant energy_test_particle entries
    n=EorT*0.0+args.density               # make an array with the same size as EorT of constant density entries
    
    ylabels=[]
    
    db=REACTIONS_DATABASE('amjuel_hydhel_database.json')
    
    for r in args.reaction.split(';'):
        report,header,name,col,ls,lw=(r+',,,').split(',')[0:6]
        if col=='':col=None
        if lw=='':lw=2
        if ls=='':ls='-'
    
        if db.select_reaction(report=report,header=header,name=name):
            #ylabel='%s$(%s)$ [%s]' % (db.symbol,db.parameters,db.unit)
            ylabel='%s [%s]' % (db.symbol,db.unit)
            if not (ylabel in ylabels):ylabels+=[ylabel]

            if db.parameters=='E':
                y=db.reaction(E=EorT)
            elif db.parameters=='T':
                y=db.reaction(T=EorT)
            elif db.parameters=='E,T':
                y=db.reaction(E=E0,T=EorT)
            elif db.parameters=='n,T':
                y=db.reaction(n=n,T=EorT)
            
            ax.plot(EorT,y*1.0,lw=float(lw),color=col,ls=ls,label=r'$%s$' % db.latex)
    
    if args.csvfiles.strip()!='':
     for r in args.csvfiles.split(';'):
        fn,factor,col,ls,lw=(r+',,,,').split(',')[0:5]
        factor=1.0 if factor=='' else float(factor)
        if col=='':col=None
        if lw=='':lw=4
        if ls=='':ls='--'
        
        csv=np.fromstring(''.join(open(fn).read().replace(',,',',').split('\n')[2:]),sep=',',dtype=float).reshape(-1,2)
        ax.plot(csv[:,0],csv[:,1]*factor,lw=float(lw),ls=ls,color=col)
    
    ax.legend(loc='best',fontsize=18)
    ax.set_xlabel(r'$E$, $T_e$ or $T_i$ [eV]',fontsize=20)
    ax.set_ylabel(','.join(ylabels),fontsize=20)
    
    if args.output!='':fig.savefig(args.output)        
    if sys.flags.interactive: #if python is executed with the flag -i the user is able to type commands in the python shell while the figure is opened.
      fig.show()
    else:
      if args.output=='':
         plt.show()  # the figure is opened, but no commands are acceped in the shell. python is terminated upon closing the figure
      #else: if an output is specified but not the -i parameter python terminates without graphical output

