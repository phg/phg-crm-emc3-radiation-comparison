# usage:
## show typical Hydrogen reaction rates:

    python reactions_database.py

## show specific reaction rate from the HYDHEL or AMJUEL reports:

    python reactions_database.py -ne 1e19 -r 'AMJUEL,4,2.1.5' --title 'AMJUEL,4,2.1.5'

## and compare it with the output from the web-interface (http://www.eirene.de/juniplib/eiram/):

    python reactions_database.py -ne 1e19 -r 'AMJUEL,4,2.1.5,red,-,2' -csv 'AMJUEL_H4_2.1.5.csv,1e-6,red,--,4' --title 'AMJUEL,4,2.1.5'

## plot the output into a pdf file:

    python reactions_database.py -ne 1e19 -r 'AMJUEL,4,2.1.5' --title 'AMJUEL,4,2.1.5' -o 'AMJUEL_H4_2.1.5.pdf'
    
## use within EMC3-EIRENE post processing:
    dbpath='/draco/u/tal/eirene_database/' #adapt this path acordingly
    sys.path.append(dbpath)
    from reactions_database import *

    db=reactions_database(dbpath+'amjuel_hydhel_database.json')

    e3e=emc3_reader('./make_links')  
    e3e.read_grid()
    e3e.read_fields(['ne','Te','nD','nD2'])
    
    nam='AMJUEL,10,2.1.5'
    db.select_reaction(nam)
    for zn in e3e.zone:
       zn.add_field(nam,zn.field['nD']*zn.field['ne']*db.reaction(n=zn.field['ne'],T=zn.field['Te'])*qe)
    
## fortran:

    cd fortran
    
    ./compile.sh
    
    ./eirene_database

    the fortran-json parser 'fson' originates from 
    
    https://github.com/josephalevin/fson

