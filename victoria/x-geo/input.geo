* geometry information for EMC3
* SYMMETRY -5
*--------------------------------
*** 1. grid resolution
*--------------------------------
* number of zones/blocks
1
* number of radial, poloidal and toroidal grid points
          83         401          34
*--------------------------------
*** 2. surface definitions
*--------------------------------
*** 2.1 non default surface
* radial
           0
* poloidal
           2
           0           0           1
           0          81           0          32
         400           0           1
           0          81           0          32
* toroidal
           2
           0           0           3
           0          81           0         399
          33           0           2
           0          81           0         399
*** 2.2 non transparent surface (Boundary condition must be defined)
* radial
           2
* 1: CORE BOUNDARY 0.10000E+01
           6           0           1
           0         399           0          32
* 2: VACUUM BOUNDARY 
          77           0          -1
           0         399           0          32
* poloidal
           0
* toroidal
           0
*** 2.3 plate surface (Bohm Boundary condition)
* radial
          -1
* poloidal
          -1
* toroidal
          -1
*--------------------------------
*** 3. physical cell definition
*--------------------------------
          -1
  1
* run cell check?
T
