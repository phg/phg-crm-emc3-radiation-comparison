import xemc3
import matplotlib.pyplot as plt
import xarray as xr
import numpy as np

def crossec_pcolorgrid_gabriele(ds, phi):
    # (A+1)*(B+1) = AB + A + B +1
    outR = np.empty((len(ds["z"])+1, len(ds["r"])+1))
    outZ = outR.copy()
    
    outZ[:-1,:-1] = ds["z_vert"][phi, :, :,0,0,0]  # AB
    outZ[:-1, -1] = ds["z_vert"][phi, :,-1,0,0,1]  # A
    outZ[ -1,:-1] = ds["z_vert"][phi,-1, :,0,1,0]  # B
    outZ[ -1, -1] = ds["z_vert"][phi,-1,-1,0,1,1]  # 1
    
    outR[:-1,:-1] = np.sqrt( ds["x_vert"][phi, :, :,0,0,0]**2 + ds["y_vert"][phi, :, :,0,0,0]**2 ) # AB
    outR[:-1, -1] = np.sqrt( ds["x_vert"][phi, :,-1,0,0,1]**2 + ds["y_vert"][phi, :,-1,0,0,1]**2 ) # A
    outR[ -1,:-1] = np.sqrt( ds["x_vert"][phi,-1, :,0,1,0]**2 + ds["y_vert"][phi,-1, :,0,1,0]**2 ) # B
    outR[ -1, -1] = np.sqrt( ds["x_vert"][phi,-1,-1,0,1,1]**2 + ds["y_vert"][phi,-1,-1,0,1,1]**2 ) # 1
    
    return outR.T, outZ.T

def crossec_pcolorgrid_philip(ds, phi):
    # (A+1)*(B+1) = AB + A + B +1
    outR = np.empty((len(ds["r"])+1, len(ds["theta"])+1))
    outZ = outR.copy()
    
    outR[:-1,:-1] = ds["R_bounds"][ :, :,phi,0,0,0]  # AB
    outR[:-1, -1] = ds["R_bounds"][ :,-1,phi,0,1,0]  # A
    outR[ -1,:-1] = ds["R_bounds"][-1, :,phi,1,0,0]  # B
    outR[ -1, -1] = ds["R_bounds"][-1,-1,phi,1,1,0]  # 1
    
    outZ[:-1,:-1] = ds["z_bounds"][ :, :,phi,0,0,0]  # AB
    outZ[:-1, -1] = ds["z_bounds"][ :,-1,phi,0,1,0]  # A
    outZ[ -1,:-1] = ds["z_bounds"][-1, :,phi,1,0,0]  # B
    outZ[ -1, -1] = ds["z_bounds"][-1,-1,phi,1,1,0]  # 1
    
    return outR, outZ

def approx_volume(ds_all): # APPROXIMATION
    # ds_all.R_bounds.diff(dim="delta_r").diff(dim="delta_theta").diff(dim="delta_phi").squeeze(dim=("delta_r", "delta_theta", "delta_phi"))
    drr = ds_all.R_bounds.diff(dim="delta_r").squeeze(dim="delta_r").mean(dim=("delta_theta", "delta_phi"))
    dzr = ds_all.z_bounds.diff(dim="delta_r").squeeze(dim="delta_r").mean(dim=("delta_theta", "delta_phi"))

    drtht = ds_all.R_bounds.diff(dim="delta_theta").squeeze(dim="delta_theta").mean(dim=("delta_r", "delta_phi"))
    dztht = ds_all.z_bounds.diff(dim="delta_theta").squeeze(dim="delta_theta").mean(dim=("delta_r", "delta_phi"))

    # length = r * dphi
    dlphi = ds_all.R_bounds.mean(dim=("delta_r", "delta_theta", "delta_phi")) * \
            ds_all.phi_bounds.diff(dim="delta_phi").squeeze(dim="delta_phi")  # .data[None, None, :]  dont need to do cause of xarray dims

    # vol = det ((drr, drtht, 0), (dzr, dztht, 0), (0, 0, dlphi))
    #     = det ((drr, drtht), (dzr, dztht)) * dlphi
    #     = (drr * dztht - dzr * drtht) * dlphi
    vols = (drr * dztht - dzr * drtht) * dlphi
    return vols

import ipywidgets as widgets
def combination_plot_gm(ds_gabriele, ds_emc3, gm_T, phi, foil_index=0, 
                        scale_by_volume=False, sf=lambda x: x, interactive=True, **kwargs):
    # embed matrix into coords
    ds_emc3["foil_geom_matrix"] = gm_T
    ds_emc3["volumes"] = approx_volume(ds_emc3) # (["r", "theta", "phi"], approx_volume(ds_emc3))
    
    fig, axs = plt.subplots(nrows=1, ncols=2, figsize=(10,7), **kwargs)
    axs[0].set_aspect("equal")
    axs[1].set_aspect("equal")
    fig.tight_layout()
    
    def _combination_plot(phi):
        # plot 1
        axs[0].clear()
        first_module_phi = np.minimum( phi%(2*np.pi/5), 2*np.pi/5 - phi%(2*np.pi/5) )
        phi_index = np.searchsorted(ds_emc3.phi_bounds[:,1], first_module_phi)
                   # np.clip(np.searchsorted(ds_emc3.phi_bounds[:,1], phi),
                   #          0, len(ds_emc3.phi_bounds[:,1])-1)
        try:
            R, Z = crossec_pcolorgrid_philip(ds_emc3, phi_index)
            C = ds_emc3["foil_geom_matrix"].isel(phi=phi_index, foil=foil_index).copy()
            if scale_by_volume:
                C /= ds_emc3["volumes"].isel(phi=phi_index)
            cb = axs[0].pcolormesh(R, Z, sf(C))
            # fig.colorbar(cb, ax=axs[0])
        except:
            ...

        # plot 2
        axs[1].clear()
        phi_index = np.argmin(( (ds_gabriele.phi * (np.pi/180) - phi)**2 ).data)
        R,Z = crossec_pcolorgrid_gabriele(ds_gabriele, phi_index)
        C = ds_gabriele["S"].isel(p=phi_index, foil=foil_index).T.copy()
        if scale_by_volume:
            C /= ds_gabriele["V"].isel(p=phi_index).T
        axs[1].pcolormesh(R, Z, sf(C))
    

    delta_phi = np.mean(np.diff(ds_gabriele.phi.data)) * (np.pi/180)
    all_phi = ds_gabriele.phi.data * (np.pi/180)
    if interactive:
        interactive_phi = widgets.FloatSlider(min=min(all_phi) - delta_phi/2, 
                                              max=max(all_phi) + delta_phi/2, 
                                              value=np.mean(all_phi), step=delta_phi/2)
        widgets.interact(_combination_plot, phi=interactive_phi)
    else:
        _combination_plot(phi)
        return fig, axs
    
def combination_plot_ez(ds_gabriele, ds_emc3, ez_C, phi, Ionization_index=0,
                        scale_by_volume=False, sf=lambda x: x, interactive=True, **kwargs):
    # embed matrix into coords
    ds_emc3["volumes"] = approx_volume(ds_emc3) # (["r", "theta", "phi"], approx_volume(ds_emc3))
    
    fig, axs = plt.subplots(nrows=1, ncols=2, figsize=(10,7), **kwargs)
    axs[0].set_aspect("equal")
    axs[1].set_aspect("equal")
    fig.tight_layout()
    
    def _combination_plot(phi):
        # plot 1
        axs[0].clear()
        first_module_phi = np.minimum( phi%(2*np.pi/5), 2*np.pi/5 - phi%(2*np.pi/5) )
        phi_index = np.searchsorted(ds_emc3.phi_bounds[:,1], first_module_phi)
                   # np.clip(np.searchsorted(ds_emc3.phi_bounds[:,1], phi),
                   #          0, len(ds_emc3.phi_bounds[:,1])-1)
        try:
            R, Z = crossec_pcolorgrid_philip(ds_emc3, phi_index)
            C = ds_emc3["Ez"].isel(phi=phi_index, Ionization=Ionization_index).copy()
            if scale_by_volume:
                C /= ds_emc3["volumes"].isel(phi=phi_index)
            cb = axs[0].pcolormesh(R, Z, sf(C))
            # fig.colorbar(cb, ax=axs[0])
        except:
            ...

        # plot 2
        axs[1].clear()
        phi_index = np.argmin(( (ds_gabriele.phi * (np.pi/180) - phi)**2 ).data)
        R,Z = crossec_pcolorgrid_gabriele(ds_gabriele, phi_index)
        C = ez_C.isel(p=phi_index, Ionization=Ionization_index).T.copy()
        if scale_by_volume:
            C /= ds_gabriele["V"].isel(p=phi_index).T
        axs[1].pcolormesh(R, Z, sf(C))
    

    delta_phi = np.mean(np.diff(ds_gabriele.phi.data)) * (np.pi/180)
    all_phi = ds_gabriele.phi.data * (np.pi/180)
    if interactive:
        interactive_phi = widgets.FloatSlider(min=min(all_phi) - delta_phi/2, 
                                              max=max(all_phi) + delta_phi/2, 
                                              value=np.mean(all_phi), step=delta_phi/2)
        widgets.interact(_combination_plot, phi=interactive_phi)
    else:
        _combination_plot(phi)
        return fig, axs
    